package com.github.mikephil.charting.components;

/**
 * @author chenyanming
 * @time 2019/6/21 on 17:45
 * @desc ${TODD}
 */
public interface IAxisLabelFormatter {
    String getFormattedValue(int index);
}
