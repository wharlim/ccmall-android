## CCMALL

#### 应用简介


### 第三方应用商店后台地址
-

### 项目后台地址

#### Android 应用信息
- ##### 包名
  - com.weiju.ccmall

- ##### keyStore

  - password:weijuit
  - alias : ccmall
  - password Root@cc
  - ``` text
    别名: ccmall
    创建日期: 2019年1月3日
    条目类型: PrivateKeyEntry
    证书链长度: 1
    证书[1]:
    所有者: L=gz
    发布者: L=gz
    序列号: 1c2f2f71
    生效时间: Thu Jan 03 11:05:17 CST 2019, 失效时间: Mon Dec 28 11:05:17 CST 2043
    证书指纹:
    SHA1: EB:6A:7C:09:D1:79:AB:62:06:5E:B1:C5:21:64:3C:CD:2F:E0:48:AA
    SHA256: 8E:01:6B:76:7B:91:3B:39:B7:50:49:D6:71:2F:21:A5:56:FC:5C:AB:38:E3:73:38:7B:CA:70:CF:E8:FF:C6:A1
    签名算法名称: SHA256withRSA
    主体公共密钥算法: 2048 位 RSA 密钥
    版本: 3

#### 服务器信息

- 测试环境服务器地址
  - https://testapi.weijuit.com/ccmapi/
- 正式环境服务器地址
  - https://api.create-chain.net/xdsapi/
- 微信正式环境分享地址
   - https://wx.create-chain.net/
- 微信测试环境分享地址
   - http://testwx.create-chain.net/
- 查看物流地址
    - https://m.kuaidi100.com/index_all.html?type=%s&postid=%s

#### 第三方SDK账号信息

- ##### 友盟
  - 账号：
  - 密码：
  - Android Key : 5c2ee8c5f1f55665900000b3

- ##### 微信
   - 账号:
   - 密码：
   - AppID：wx63db6cd11138635b
   - AppSecret：4c609e8ad724b38de96d509811029474
   - 支付MCH_ID:
   - 支付 key：

- ##### bugly
  - Android App ID ： 04392b505a
  - Android App Key: 5a573122-3de1-4f12-a341-b1376cf8d56c

- ##### 极光 正式
    - 账号：
    - 密码：
    - appkey：32bc8d73d826370d265df6be
    - Master Secret ：d32f732f77139939c64d70c3

- ##### 极光 测试
    - 账号：
    - 密码：
    - appkey：
    - Master Secret :


- ##### 高德地图

  - Android key
    `61d709eb80eb2732dfee27fdc67ae795`


    -

