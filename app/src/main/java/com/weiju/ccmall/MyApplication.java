package com.weiju.ccmall;

import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.alibaba.baichuan.android.trade.AlibcTradeSDK;
import com.alibaba.baichuan.android.trade.callback.AlibcTradeInitCallback;
import com.blankj.utilcode.utils.Utils;
import com.orhanobut.logger.LogLevel;
import com.orhanobut.logger.Logger;
import com.sobot.chat.SobotApi;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.beta.Beta;
import com.tencent.imsdk.TIMSdkConfig;
import com.tencent.qcloud.tim.uikit.TUIKit;
import com.tencent.qcloud.tim.uikit.config.CustomFaceConfig;
import com.tencent.qcloud.tim.uikit.config.GeneralConfig;
import com.tencent.qcloud.tim.uikit.config.TUIKitConfigs;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.weiju.ccmall.module.MainActivity;
import com.weiju.ccmall.module.matisse.ALCallbacks;
import com.weiju.ccmall.shared.manager.PushManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.BaseUrl;
import com.weiju.ccmall.shared.util.ChatroomKit;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 自定义Application
 * Created by JayChan on 2016/10/13.
 */
public class MyApplication extends MultiDexApplication {

    private static final String TAG = "MyApplication";

    private static MyApplication instance;
    public static boolean isShowNoLogin = false;

    public static boolean CAN_OPEN_LIVE = false;

    public static boolean isGongCat = false;
    public static boolean isShowPayPwd = false;

    private static Context context;

//    public int sdkAppId = 1400250517;

    public int sdkAppId = 1400248032;

    private void checkAppReplacingState() {
        if (getResources() == null) {
            Log.w(TAG, "app is replacing...kill");
            Process.killProcess(Process.myPid());
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        checkAppReplacingState();
        try {
            Class clazz = Class.forName("java.lang.Daemons$FinalizerWatchdogDaemon");
            Method method = clazz.getSuperclass().getDeclaredMethod("stop");
            method.setAccessible(true);
            Field field = clazz.getDeclaredField("INSTANCE");
            field.setAccessible(true);
            method.invoke(field.get(null));
        } catch (Throwable e) {
            e.printStackTrace();
        }


        MultiDex.install(this);
        instance = this;
        context = getApplicationContext();
        Logger.init().logLevel(BuildConfig.DEBUG ? LogLevel.FULL : LogLevel.NONE);
        if (!BuildConfig.DEBUG) {
            getSharedPreferences(BaseUrl.NETWORK_TYPE, 0).edit().putInt(BaseUrl.NETWORK_TYPE, BaseUrl.CCMALL_RELEASE_1).commit();
        }
        PushManager.registerJPushService(this);
        registerMobClickAgent();
        ServiceManager.getInstance().initFresco();
        Utils.init(this);
        initBugly();
        initUmengShare();
        initCS();
        initRongIM();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }
        registerActivityLifecycleCallbacks(new ALCallbacks());

        TUIKitConfigs configs = TUIKit.getConfigs();
        configs.setSdkConfig(new TIMSdkConfig(sdkAppId));
        configs.setCustomFaceConfig(new CustomFaceConfig());
        configs.setGeneralConfig(new GeneralConfig());
        TUIKit.init(MyApplication.getContext(), sdkAppId, configs);

        AlibcTradeSDK.asyncInit(this, new AlibcTradeInitCallback() {
            @Override
            public void onSuccess() {
                //初始化成功，设置相关的全局配置参数
                Logger.e("AlibcTradeSDK", "onSuccess");
            }

            @Override
            public void onFailure(int code, String msg) {
                //初始化失败，可以根据code和msg判断失败原因，详情参见错误说明
                Logger.e("AlibcTradeSDK", "onFailure --->" + msg);
            }
        });
    }

    private void initRongIM() {
        ChatroomKit.init(this, BuildConfig.RONG_IM_KEY);
    }

    public static Context getContext() {
        return context;
    }

    private void initCS() {
        SobotApi.initSobotSDK(this, "ef0dfc07158045d59434a84d929443ae", "");
        SobotApi.setNotificationFlag(this, true, R.drawable.logo, R.drawable.logo);

    }

    private void initUmengShare() {
        PlatformConfig.setWeixin(BuildConfig.WX_APP_ID.trim(), BuildConfig.WX_SECRET + "1111");
        UMShareAPI.get(this);
    }

    /**
     * 腾讯 bugly
     */
    private void initBugly() {
        Beta.canShowUpgradeActs.add(MainActivity.class);
        Bugly.init(getApplicationContext(), BuildConfig.BUGLY_APP_ID, false);
    }

    // 注册友盟统计
    private void registerMobClickAgent() {
        MobclickAgent.setScenarioType(this, MobclickAgent.EScenarioType.E_UM_NORMAL);
        MobclickAgent.setDebugMode(BuildConfig.DEBUG);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        MobclickAgent.onKillProcess(this);
    }


    public static MyApplication getInstance() {
        return instance;
    }
}
