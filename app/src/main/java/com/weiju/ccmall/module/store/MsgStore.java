package com.weiju.ccmall.module.store;

import com.weiju.ccmall.module.auth.event.BaseMsg;
import com.weiju.ccmall.shared.bean.MemberStore;

public class MsgStore extends BaseMsg {
    public static final int ACTION_SEND_STORE_OBJ = 1 << 0;
    private MemberStore mMemberStore;

    public MsgStore(int action) {
        super(action);
    }


    public void setMemberStore(MemberStore memberStore) {
        mMemberStore = memberStore;
    }

    public MemberStore getMemberStore() {
        return mMemberStore;
    }
}
