package com.weiju.ccmall.module.xysh.fragment;

import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.AuthChannelActivity;
import com.weiju.ccmall.module.xysh.activity.ReceiptSupportBanksActivity;
import com.weiju.ccmall.module.xysh.activity.SupportBanksActivity;
import com.weiju.ccmall.module.xysh.adapter.PayChannelAdapter;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.ReceiptChannelItem;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class PayChannelFragment extends BaseListFragment {

    private PayChannelAdapter mAdapter;
    private OnChannelClickListener mOnChannelClickListener;
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    private QueryUserBankCardResult.BankInfListBean creditCard;
    private QueryUserBankCardResult.BankInfListBean payCard;
    private QueryUserBankCardResult.BankInfListBean receiptCard;

    public static final int TYPE_REPAY = 1; // 还款
    public static final int TYPE_RECEIPT = 2; // 收款

    private int type = TYPE_REPAY;
    private int repayChannelType;

    private boolean refreshOnResume;

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        ChannelItem channel = mAdapter.getData().get(position);
        if (view != null && view.getId() == R.id.ll_supported_banks) {
            openSupportBanks(channel.channelShortName);
            return;
        }

        if (channel.openStatus == 0) { // 未认证
            refreshOnResume = true;
            if (type == TYPE_REPAY) {
                AuthChannelDialogFragment.newInstance(
                        AuthChannelActivity.AUTH_TYPE_REPAY,
                        creditCard, null, channel
                ).show(getActivity().getSupportFragmentManager());
            } if (type == TYPE_RECEIPT) {
                AuthChannelDialogFragment.newInstance(
                        AuthChannelActivity.AUTH_TYPE_RECEIPT,
                        payCard, receiptCard, channel
                ).show(getActivity().getSupportFragmentManager());
            }
        } else if (channel.openStatus == 1) { // 已认证
            //if (mOnChannelClickListener != null) {
            //     mOnChannelClickListener.onChannelClick(position, channel);
            //}
            mAdapter.setSelectChannel(channel);
        } else { // 不支持
            // ToastUtil.error("通道不支持！");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (refreshOnResume) {
            getData(true);
        }
    }

    public ChannelItem getSelectChannel() {
        return mAdapter.getSelectChannel();
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        type = getArguments().getInt("type");
        mAdapter = new PayChannelAdapter(type);
        if (type == TYPE_REPAY) {
            repayChannelType = getArguments().getInt("channelType");
        }
        creditCard = (QueryUserBankCardResult.BankInfListBean) getArguments().getSerializable("creditCard");
        payCard = (QueryUserBankCardResult.BankInfListBean) getArguments().getSerializable("payCard");
        receiptCard = (QueryUserBankCardResult.BankInfListBean) getArguments().getSerializable("receiptCard");
    }

    private void openSupportBanks(String channelId) {
        if (type == TYPE_REPAY) {
            SupportBanksActivity.start(getContext(), channelId);
        } else if (type == TYPE_RECEIPT) {
            ReceiptSupportBanksActivity.start(getContext(), channelId);
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        mRefreshLayout.setRefreshing(true);
        if (type == TYPE_REPAY) {
            APIManager.startRequest(service.getAllOnlineChannels(creditCard.cardNo, repayChannelType), new Observer<XYSHCommonResult<List<ChannelItem>>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(XYSHCommonResult<List<ChannelItem>> ret) {
                    if (ret.code == 1 || ret.success) {
                        // 下面是将不支持的通道移动到末尾
                        Iterator<ChannelItem> iterator = ret.data.iterator();
                        List<ChannelItem> unsupport = new ArrayList<>();
                        // 将已认证的通道移动到前面
                        List<ChannelItem> authenticated = new ArrayList<>();
                        while (iterator.hasNext()) {
                            ChannelItem item = iterator.next();
                            if (!item.isSupport()) {
                                iterator.remove();
                                unsupport.add(item);
                            } else if (item.openStatus == 1) {  //已认证的通道
                                iterator.remove();
                                authenticated.add(item);
                            }
                        }
                        ret.data.addAll(0, authenticated);
                        ret.data.addAll(unsupport);
                        if (!unsupport.isEmpty()) {
                            mAdapter.setFirstUnSupportChannel(unsupport.get(0));
                        }
                        mAdapter.setNewData(ret.data);
                        if (ret.data.size() > 0) {
                            ChannelItem selectChannel = ret.data.get(0);
                            if (selectChannel.openStatus == 1) {
                                mAdapter.setSelectChannel(selectChannel);
                            } else {
                                mAdapter.setSelectChannel(null);
                            }
                        } else {
                            mAdapter.setSelectChannel(null);
                        }
                    }
                    mRefreshLayout.setRefreshing(false);
                    mAdapter.loadMoreEnd(true);
                }

                @Override
                public void onError(Throwable e) {
                    ToastUtil.error(e.getMessage());
                    mRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onComplete() {

                }
            });
        } else if (type == TYPE_RECEIPT) {
            APIManager.startRequest(service.getAllOnlinePayChannels(SessionUtil.getInstance().getOAuthToken(), payCard.cardNo),
                    new Observer<XYSHCommonResult<List<ReceiptChannelItem>>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(XYSHCommonResult<List<ReceiptChannelItem>> ret) {
                            if (ret.success) {
                                List<ChannelItem> channelItems = new ArrayList<>();
                                for(ReceiptChannelItem item: ret.data) {
                                    channelItems.add(item.toChannelItem());
                                }

                                // 下面是将不支持的通道移动到末尾
                                Iterator<ChannelItem> iterator = channelItems.iterator();
                                List<ChannelItem> unsupport = new ArrayList<>();
                                // 将已认证的通道移动到前面
                                List<ChannelItem> authenticated = new ArrayList<>();
                                while (iterator.hasNext()) {
                                    ChannelItem item = iterator.next();
                                    if (!item.isSupport()) {
                                        iterator.remove();
                                        unsupport.add(item);
                                    } else if (item.openStatus == 1) {  //已认证的通道
                                        iterator.remove();
                                        authenticated.add(item);
                                    }
                                }
                                channelItems.addAll(0, authenticated);
                                channelItems.addAll(unsupport);
                                if (!unsupport.isEmpty()) {
                                    mAdapter.setFirstUnSupportChannel(unsupport.get(0));
                                }
                                mAdapter.setNewData(channelItems);
                                if (channelItems.size() > 0) {
                                    ChannelItem selectChannel = channelItems.get(0);
                                    if (selectChannel.openStatus == 1) {
                                        mAdapter.setSelectChannel(selectChannel);
                                    } else {
                                        mAdapter.setSelectChannel(null);
                                    }
                                } else {
                                    mAdapter.setSelectChannel(null);
                                }
                            }
                            mRefreshLayout.setRefreshing(false);
                            mAdapter.loadMoreEnd(true);
                        }

                        @Override
                        public void onError(Throwable e) {
                            ToastUtil.error(e.getMessage());
                            mRefreshLayout.setRefreshing(false);
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        } else {
            throw new IllegalArgumentException("type不正确: " + type);
        }
    }

    // 还款通道列表，需要提供相应银行卡
    public static PayChannelFragment newInstance(QueryUserBankCardResult.BankInfListBean creditCard, int channelType) {
        PayChannelFragment fragment = new PayChannelFragment();
        Bundle args = new Bundle();
        args.putSerializable("creditCard", creditCard);
        args.putInt("type", TYPE_REPAY);
        args.putInt("channelType", channelType);
        fragment.setArguments(args);
        return fragment;
    }

    // 收款通道列表
    public static PayChannelFragment newInstance(QueryUserBankCardResult.BankInfListBean payCard, QueryUserBankCardResult.BankInfListBean receiptCard) {
        PayChannelFragment fragment = new PayChannelFragment();
        Bundle args = new Bundle();
        args.putSerializable("payCard", payCard);
        args.putSerializable("receiptCard", receiptCard);
        args.putInt("type", TYPE_RECEIPT);
        fragment.setArguments(args);
        return fragment;
    }

    public interface OnChannelClickListener {
        void onChannelClick(int position, ChannelItem channel);
    }

    public void setOnChannelClickListener(OnChannelClickListener onChannelClickListener) {
        mOnChannelClickListener = onChannelClickListener;
    }
}
