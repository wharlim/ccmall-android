package com.weiju.ccmall.module.product;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.adapter.ProductVideoViewPagerAdapter;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Product;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.component.TagTextView;
import com.weiju.ccmall.shared.component.dialog.ProductVerifyDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/6/25 on 10:16
 * @desc 当前领取的新人专区产品
 */
public class NewerProductDetailActivity extends BaseActivity {
    @BindView(R.id.tvTagTitle)
    TagTextView mTvTagTitle;
    @BindView(R.id.tvDesc)
    TextView mTvDesc;
    @BindView(R.id.tvRetailPrice)
    TextView mTvRetailPrice;
    @BindView(R.id.tvMarketPrice)
    TextView mTvMarketPrice;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.ivBannerVideo)
    ImageView mIvBannerVideo;
    @BindView(R.id.ivBannerImage)
    ImageView mIvBannerImage;
    @BindView(R.id.tvBannerImageText)
    TextView mTvBannerImageText;
    @BindView(R.id.lyVideoPic)
    LinearLayout mLyVideoPic;
    @BindView(R.id.tv_cc)
    TextView mTvCc;
    @BindView(R.id.tvProductAuth1)
    TextView mTvProductAuth1;
    @BindView(R.id.tvProductAuth2)
    TextView mTvProductAuth2;
    @BindView(R.id.tvProductAuth3)
    TextView mTvProductAuth3;
    @BindView(R.id.topProList)
    RelativeLayout mTopProList;
    @BindView(R.id.ivCountry)
    SimpleDraweeView mIvCountry;
    @BindView(R.id.tvCountry)
    TextView mTvCountry;
    @BindView(R.id.layoutPrice)
    LinearLayout mLayoutPrice;
    @BindView(R.id.productAuthLayout)
    LinearLayout mProductAuthLayout;
    @BindView(R.id.tvCancel)
    TextView mTvCancel;
    @BindView(R.id.layoutNodata)
    LinearLayout mLayoutNodata;
    private IProductService mProductService;
    private Product mProduct;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newer_product_detail);
        ButterKnife.bind(this);

        EventBus.getDefault().register(this);

        initView();
        initData();
    }

    private void initView() {
        setTitle("新人专区");
        setLeftBlack();
    }

    private void initData() {
        ToastUtil.showLoading(NewerProductDetailActivity.this);
        mProductService = ServiceManager.getInstance().createService(IProductService.class);
        APIManager.startRequest(mProductService.getNewerSkuDetail(), new BaseRequestListener<SkuInfo>(this) {
            @Override
            public void onSuccess(SkuInfo skuInfo) {
                ToastUtil.hideLoading();
                if (!TextUtils.isEmpty(skuInfo.skuId)) {
                    mTvCancel.setVisibility(View.VISIBLE);
                    mLayoutNodata.setVisibility(View.GONE);
                    setSkuViews(skuInfo);
                    getProductInfoById(skuInfo);
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.hideLoading();
            }
        },this);
    }

    private void getProductInfoById(SkuInfo skuInfo) {
        APIManager.startRequest(mProductService.getDetailById(skuInfo.productId), new BaseRequestListener<Product>(this) {
            @Override
            public void onSuccess(Product product) {
                mProduct = product;
                setVideoViewPager(product, skuInfo);
                setProductView(product);
            }
        },this);
    }

    private void setProductView(Product mProduct) {
        if (mProduct.auths == null || mProduct.auths.size() < 1) {
            mProductAuthLayout.setVisibility(View.GONE);
        } else {
            TextView tvAuth[] = {mTvProductAuth1, mTvProductAuth2, mTvProductAuth3};
            for (int i = 0; i < mProduct.auths.size(); i++) {
                if (i > 2) {
                    break;
                }
                tvAuth[i].setVisibility(View.VISIBLE);
                tvAuth[i].setText(mProduct.auths.get(i).title);
            }
        }

        mTvTagTitle.setTags(mProduct.tags);

        if (mProduct.country != null) {
            FrescoUtil.setImage(mIvCountry, mProduct.country.flag);
            mTvCountry.setText(mProduct.country.countryName);
        }
    }


    @SuppressLint("DefaultLocale")
    private void setVideoViewPager(Product mProduct, SkuInfo skuInfo) {
        ProductVideoViewPagerAdapter adapter = new ProductVideoViewPagerAdapter(this, mProduct);
        mViewPager.setAdapter(adapter);
        final boolean isShowVideo = !StringUtils.isEmpty(mProduct.mediaUrl);
        if (!isShowVideo) {
            mIvBannerVideo.setVisibility(View.GONE);
            mIvBannerImage.setVisibility(View.GONE);
            mTvBannerImageText.setText(String.format("%d/%d", 1, null != skuInfo.images ? skuInfo.images.size() : 1));
            mTvBannerImageText.setVisibility(View.VISIBLE);
        } else {
            mTvBannerImageText.setVisibility(View.GONE);
        }
        mIvBannerVideo.setSelected(true);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (isShowVideo) {
                    if (position != 0) {
                        GSYVideoManager.onPause();
                    }
                }

                mIvBannerVideo.setSelected(position == 0);
                mIvBannerImage.setSelected(position != 0);
                if (position != 0 || !isShowVideo) {
                    int add = isShowVideo ? 0 : 1;
                    mTvBannerImageText.setVisibility(View.VISIBLE);
                    mTvBannerImageText.setText(String.format("%d/%d", position + add, skuInfo.images.size()));
                } else {
                    mTvBannerImageText.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mIvBannerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1);
            }
        });
        mIvBannerVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0);
            }
        });
    }


    private void setSkuViews(SkuInfo mSkuInfo) {
        mTvTagTitle.setText(mSkuInfo.name);
        if (StringUtils.isEmpty(mSkuInfo.desc)) {
            mTvDesc.setVisibility(View.GONE);
        } else {
            mTvDesc.setText(mSkuInfo.desc);
        }
        mTvRetailPrice.setText(MoneyUtil.centToYuan¥StrNoZero(mSkuInfo.retailPrice));
        mTvMarketPrice.setText(MoneyUtil.centToYuan¥StrNoZero(mSkuInfo.marketPrice));

        TextViewUtil.addThroughLine(mTvMarketPrice);

        if (mSkuInfo.productType == 11) {
            mTvCc.setText(Html.fromHtml(String.format("预计可返<font color =\"#f51861\">%s%%</font>算率",
                    mSkuInfo.countRateExc)));
        } else if (mSkuInfo.ticket == 0 && mSkuInfo.goldenTicket == 0) {
            mTvCc.setText(Html.fromHtml(String.format("预计可返<font color =\"#f51861\">%s%%</font>算率",
                    mSkuInfo.countRateExc)));
        } else if (mSkuInfo.ticket > 0 && mSkuInfo.goldenTicket > 0) {
            mTvCc.setText(Html.fromHtml(String.format("需要使用<font color =\"#f51861\">%1$s</font>C券，" +
                            "预计可返<font color =\"#f51861\">%2$s%%</font>算率，可用金券兑换<font color =\"#f51861\">%3$s</font>余额",
                    MoneyUtil.centToYuanStrNoZero(mSkuInfo.ticket), mSkuInfo.countRateExc,
                    MoneyUtil.centToYuanStrNoZero(mSkuInfo.goldenTicket))));
        } else if (mSkuInfo.ticket > 0) {
            mTvCc.setText(Html.fromHtml(String.format("需要使用<font color =\"#f51861\">%1$s</font>C券，预计可返<font color =\"#f51861\">%2$s%%</font>算率",
                    MoneyUtil.centToYuanStrNoZero(mSkuInfo.ticket), mSkuInfo.countRateExc)));
        } else {
            mTvCc.setText(Html.fromHtml(String.format("预计可返<font color =\"#f51861\">%1$s%%</font>算率，" +
                            "可用金券兑换<font color =\"#f51861\">%2$s</font>余额",
                    mSkuInfo.countRateExc, MoneyUtil.centToYuanStrNoZero(mSkuInfo.goldenTicket))));
        }
    }

    @OnClick(R.id.productAuthLayout)
    protected void showProductAuth() {
        if (mProduct != null && mProduct.auths != null) {
            new ProductVerifyDialog(this, mProduct.auths).show();
        }
    }

    @OnClick(R.id.tvCancel)
    protected void cancel() {
        WJDialog wjDialog = new WJDialog(NewerProductDetailActivity.this);
        wjDialog.show();

        wjDialog.setContentText("您确定放弃之前每月领购产品，换购其他产品吗");
        wjDialog.setCancelText("取消");
        wjDialog.setConfirmText("确定");
        wjDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wjDialog.dismiss();
                cancelNewerProduct();
            }
        });
    }

    private void cancelNewerProduct() {
        ToastUtil.showLoading(NewerProductDetailActivity.this);
        APIManager.startRequest(mProductService.cancelNewerSku(), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object object) {
                ToastUtil.hideLoading();
                ToastUtil.success("放弃原产品成功");
                finish();
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.hideLoading();
            }
        },this);
    }
}
