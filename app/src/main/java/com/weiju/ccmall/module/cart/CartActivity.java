package com.weiju.ccmall.module.cart;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.constant.AppTypes;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.cart
 * @since 2017-08-07
 */
public class CartActivity extends FragmentActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        CartFragment cartFragment = CartFragment.newInstance(AppTypes.CART.FROM_ACTIVITY);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.container, cartFragment);
        fragmentTransaction.commit();
    }
}
