package com.weiju.ccmall.module.xysh.activity.repayment;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class RepaymentPlanConfirmFragment extends BottomSheetDialogFragment {

    @BindView(R.id.ivBankIcon)
    ImageView ivBankIcon;
    @BindView(R.id.tvBankName)
    TextView tvBankName;
    @BindView(R.id.tvBankNo)
    TextView tvBankNo;
    @BindView(R.id.tvCommit)
    TextView tvCommit;
    @BindView(R.id.tvMinAvailable)
    TextView tvMinAvailable;
    Unbinder unbinder;

    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    String bankName;
    String cardNo;
    float available;
    String planNo;

    CountDownTimer countDownTimer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_repayment_plan_confirm, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        Bundle args = getArguments();
        if (args != null) {
            bankName = args.getString("bankName");
            cardNo = args.getString("cardNo");
            available = args.getFloat("available");
            planNo = args.getString("planNo");
            ivBankIcon.setImageResource(BankUtils.getBankIconByName(bankName));
            tvBankName.setText(bankName);
            tvBankNo.setText(String.format("(尾号%s)", BankUtils.cutBankCardNo(cardNo)));
            tvMinAvailable.setText(String.format("%.2f", available));
        }

        countDownTimer = new CountDownTimer(3000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                tvCommit.setEnabled(false);
                tvCommit.setText(String.format("我知道了，确定提交(%d秒)", millisUntilFinished/1000));
            }

            @Override
            public void onFinish() {
                tvCommit.setEnabled(true);
                tvCommit.setText("我知道了，确定提交");
            }
        }.start();
    }

    public RepaymentPlanConfirmFragment show(FragmentManager fm) {
        show(fm, "RepaymentPlanConfirmFragment");
        return this;
    }

    public static RepaymentPlanConfirmFragment newInstance(String bankName, String cardNo, float available, String planNo) {
        RepaymentPlanConfirmFragment fragment = new RepaymentPlanConfirmFragment();
        Bundle args = new Bundle();
        args.putString("bankName", bankName);
        args.putString("cardNo", cardNo);
        args.putFloat("available", available);
        args.putString("planNo", planNo);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    @OnClick(R.id.tvCommit)
    public void onViewClicked() {
        ToastUtil.showLoading(getContext());
        APIManager.startRequest(service.payBackPlanConfirm(planNo), new Observer<XYSHCommonResult<Object>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<Object> ret) {
                if (getActivity() == null) {
                    return;
                }
                ToastUtil.hideLoading();
                if (ret.success) {
                    ToastUtil.success(ret.message);
                    getActivity().finish();
                } else {
                    ToastUtil.success(ret.message);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
}
