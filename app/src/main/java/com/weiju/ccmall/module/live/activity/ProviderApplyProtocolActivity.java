package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.newRetail.bean.ActiveValueRule;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProviderApplyProtocolActivity extends BaseActivity {

    @BindView(R.id.tvProtocol)
    TextView mTvProtocol;
    @BindView(R.id.ivAgree)
    ImageView mIvAgree;
    @BindView(R.id.tvAgreementTag)
    TextView mTvAgreementTag;
    @BindView(R.id.tvNext)
    TextView mTvNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_apply_protocol);
        ButterKnife.bind(this);
        setTitle("入驻申请");
        setLeftBlack();
        String strMsg = "我已阅读并同意<font color=#0B93D9>《CClive直播供应商服务协议》</font>";
        mTvAgreementTag.setText(Html.fromHtml(strMsg));
        mTvNext.setEnabled(false);
        ILiveStoreService iLiveStoreService = ServiceManager.getInstance().createService(ILiveStoreService.class);
        APIManager.startRequest(iLiveStoreService.getTreaty(), new BaseRequestListener<ActiveValueRule>() {
            @Override
            public void onSuccess(ActiveValueRule result) {
                super.onSuccess(result);
                if (!isDestroyed()) {
                    mTvProtocol.setText(Html.fromHtml(result.content));
                }
            }
        }, this);
    }

    @OnClick({R.id.ivAgree, R.id.tvNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivAgree:
                mIvAgree.setSelected(!mIvAgree.isSelected());
                mTvNext.setEnabled(mIvAgree.isSelected());
                break;
            case R.id.tvNext:
                ProviderApplyFirstActivity.start(this);
                finish();
                break;
        }
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, ProviderApplyProtocolActivity.class);
        context.startActivity(intent);
    }

}
