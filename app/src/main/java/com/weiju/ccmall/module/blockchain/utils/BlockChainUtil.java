package com.weiju.ccmall.module.blockchain.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;

import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Locale;
import java.util.regex.Pattern;

public class BlockChainUtil {

    private final static String TransferOutHintKey = "TransferOutHint";

    /**
     * 转换ccm币
     * @param coin 币的整型值
     * @return
     */
    public static String formatCCMCoin(long coin) {
        double dc = coin / 1000000.0;
        String dcStr = String.format(Locale.CHINA,"%.6f", dc);
        int firstInvalidZeroIndex = dcStr.length();
        for (; firstInvalidZeroIndex > 0; -- firstInvalidZeroIndex) {
            if (dcStr.charAt(firstInvalidZeroIndex - 1) != '0') {
                break;
            }
        }
        return dcStr.substring(0, firstInvalidZeroIndex);
    }

    public static String formatCCMCoin(double dc) {
//        String dcStr = String.format(Locale.CHINA,"%.6f", dc);
//        int firstInvalidZeroIndex = dcStr.length();
//        for (; firstInvalidZeroIndex > 0; -- firstInvalidZeroIndex) {
//            char c = dcStr.charAt(firstInvalidZeroIndex - 1);
//            if (c != '0' && c != '.') {
//                break;
//            }
//        }
//        return dcStr.substring(0, firstInvalidZeroIndex);
        DecimalFormat decimalFormat=new DecimalFormat("#.######");
        return decimalFormat.format(dc);
    }

    public static String formatCRate(String str) {
        double dc = Double.valueOf(str);
        DecimalFormat decimalFormat=new DecimalFormat("#.######");
        return decimalFormat.format(dc);
    }

    /**
     * 转换算率
     * @param rate
     * @return
     */
    public static String formatRate(double rate) {
        return rate + "%";
    }

    // 是否有效的地址
    public static boolean isValidAddress(String address) {
        return Pattern.matches("^[1-9a-zA-HJ-NP-Z]{34}$", address);
    }

    public static void saveQrCode(Context context, Bitmap bitmap) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                try {
                    File DCIM = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
//                    File Camera = new File(DCIM, "Camera");
//                    if (!Camera.exists()) {
//                        Camera.mkdirs();
//                    } else if (Camera.isFile()) {
//                        Camera.delete();
//                        Camera.mkdirs();
//                    }

                    String fileName = "qrCode_" + System.currentTimeMillis() + ".jpg";

                    File qrCodeFile = new File(DCIM, fileName);
                    if (qrCodeFile.exists()) {
                        qrCodeFile.delete();
                    }
                    qrCodeFile.createNewFile();
                    FileOutputStream fos = new FileOutputStream(qrCodeFile);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    fos.close();
                    // 刷新图库
                    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    Uri uri = Uri.fromFile(qrCodeFile);
                    intent.setData(uri);
                    context.sendBroadcast(intent);

                    return true;
                } catch (Exception e) {
                    return false;
                }
            }

            @Override
            protected void onPostExecute(Boolean result) {
                super.onPostExecute(result);
                if (result) {
                    ToastUtil.success("二维码已保存到相册!");
                } else {
                    ToastUtil.error("二维码保存失败.");
                }
            }
        }.execute();
    }
    // 设置转出提示已读
    public static void setTransferOutHintToRead() {
        SessionUtil.getInstance().putString(TransferOutHintKey, "hasRead");
    }
    // 转出提示是否已读
    public static boolean getWhetherTransferOutHintIsRead() {
        return "hasRead".equals(SessionUtil.getInstance().getString(TransferOutHintKey));
    }
}
