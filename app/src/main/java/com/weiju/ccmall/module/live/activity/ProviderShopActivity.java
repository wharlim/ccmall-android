package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.tencent.imsdk.TIMConversation;
import com.tencent.imsdk.TIMManager;
import com.tencent.imsdk.TIMMessage;
import com.tencent.imsdk.TIMMessageListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.address.AddressListActivity;
import com.weiju.ccmall.module.auth.AuthPhoneActivity;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.SubmitStatusActivity;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.auth.model.AuthModel;
import com.weiju.ccmall.module.live.entity.StoreHomeEntity;
import com.weiju.ccmall.module.order.OrderListActivity;
import com.weiju.ccmall.module.shop.ShopActivity;
import com.weiju.ccmall.newRetail.activity.ConversationListActivity;
import com.weiju.ccmall.newRetail.activity.StoreRefundListActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.component.ItemWithIcon;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProviderShopActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.ivSwitchToLive)
    ImageView mIvSwitchToLive;
    @BindView(R.id.back)
    ImageView mBack;
    @BindView(R.id.avatarIv)
    SimpleDraweeView mAvatarIv;
    @BindView(R.id.tvShop)
    TextView mTvShop;
    @BindView(R.id.tvCashDeposit)
    TextView mTvCashDeposit;
    @BindView(R.id.tvMessage)
    ImageView mTvMessage;
    @BindView(R.id.tvMessageNumber)
    TextView mTvMessageNumber;
    @BindView(R.id.tvBalance)
    TextView mTvBalance;
    @BindView(R.id.tvSettledAmount)
    TextView mTvSettledAmount;
    @BindView(R.id.tvTodaySales)
    TextView mTvTodaySales;
    @BindView(R.id.tvTodayOrder)
    TextView mTvTodayOrder;
    @BindView(R.id.viewMoreOrderLayout)
    LinearLayout mViewMoreOrderLayout;
    @BindView(R.id.orderUnpayCmp)
    ItemWithIcon mOrderUnpayCmp;
    @BindView(R.id.orderPaidCmp)
    ItemWithIcon mOrderPaidCmp;
    @BindView(R.id.orderDispatchedCmp)
    ItemWithIcon mOrderDispatchedCmp;
    @BindView(R.id.orderServiceCmp)
    ItemWithIcon mOrderServiceCmp;
    @BindView(R.id.goodsManagementCmp)
    ItemWithIcon mGoodsManagementCmp;
    @BindView(R.id.fundsManagementCmp)
    ItemWithIcon mFundsManagementCmp;
    @BindView(R.id.storeHomeCmp)
    ItemWithIcon mStoreHomeCmp;
    @BindView(R.id.returnAddressCmp)
    ItemWithIcon mReturnAddressCmp;
    @BindView(R.id.storeFreightCmp)
    ItemWithIcon mStoreFreightCmp;
    @BindView(R.id.orderReceivedCmp)
    ItemWithIcon mOrderReceivedCmp;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;
    private User mUser;
    private StoreHomeEntity mHomeEntity;

    private IUserService mUserService;
    ILiveStoreService iLiveStoreService;
    private String htmlSmallUnit = "<small>元</small>";
    private String htmlSmallSmallUnit = "<small><small>元</small></small>";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_shop);
        ButterKnife.bind(this);
        initStatusBar();
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        iLiveStoreService = ServiceManager.getInstance().createService(ILiveStoreService.class);
        TIMManager.getInstance().addMessageListener(messageListener);
        initCmpView(mOrderPaidCmp);
        initCmpView(mOrderDispatchedCmp);
        initCmpView(mOrderReceivedCmp);
        initCmpView(mOrderServiceCmp);
//        getData();
        mRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getData();
        updateTotalUnreadMessage();
        reloadUserInfo();
    }

    private void initCmpView(ItemWithIcon cmp) {
        cmp.setLabelColor(getResources().getColor(R.color.text_gray));
    }

    private void getData() {
        APIManager.startRequest(iLiveStoreService.homePage(), new BaseRequestListener<StoreHomeEntity>(mRefreshLayout) {
            @Override
            public void onSuccess(StoreHomeEntity result) {
                super.onSuccess(result);
                mHomeEntity = result;
                FrescoUtil.setImageSmall(mAvatarIv, result.headImage);
                mTvShop.setText(result.storeName);
                mTvCashDeposit.setText(String.format("保证金 %s元", MoneyUtil.centToYuanStrNoZero(result.bondMoney)));
                mTvBalance.setText(Html.fromHtml(MoneyUtil.centToYuanStrNoZero(result.totalMoney) + htmlSmallUnit));
                mTvSettledAmount.setText(Html.fromHtml(MoneyUtil.centToYuanStrNoZero(result.waitProfitMoney) + htmlSmallSmallUnit));
                mTvTodaySales.setText(Html.fromHtml(MoneyUtil.centToYuanStrNoZero(result.dayOrderMoney) + htmlSmallSmallUnit));
                mTvTodayOrder.setText(result.dayOrderCount + "");

                mOrderPaidCmp.setBadge(result.waitShipCount);
                mOrderDispatchedCmp.setBadge(result.hasShipCount);
                mOrderServiceCmp.setBadge(result.afterSaleCount);
                mOrderServiceCmp.setBadgeRightMargin(SizeUtils.dp2px(10));
//                mOrderReceivedCmp.setBadge(result.hasReceivedCount);

            }
        }, this);
    }

    private void initStatusBar() {
        View decorView = getWindow().getDecorView();
        int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        decorView.setSystemUiVisibility(option);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void reloadUserInfo() {
        APIManager.startRequest(mUserService.getUserInfo(), new BaseRequestListener<User>(this) {
            @Override
            public void onSuccess(User user) {
                SessionUtil.getInstance().setLoginUser(user);
//                mUser = user;
            }

        }, this);
    }

    private void updateTotalUnreadMessage() {
        long totalUnreadMessage = 0;
        List<TIMConversation> conversations = TIMManager.getInstance().getConversationList();
        if (conversations != null) {
            for (TIMConversation conversation : conversations) {
                totalUnreadMessage += conversation.getUnreadMessageNum();
            }
        }

        if (totalUnreadMessage > 0) {
            mTvMessageNumber.setVisibility(View.VISIBLE);
            if (totalUnreadMessage > 99) {
                totalUnreadMessage = 99;
            }
            mTvMessageNumber.setText(String.valueOf(totalUnreadMessage));
        } else {
            mTvMessageNumber.setVisibility(View.INVISIBLE);
        }
    }

    TIMMessageListener messageListener = new TIMMessageListener() {//消息监听器
        @Override
        public boolean onNewMessages(List<TIMMessage> msgs) {//收到新消息
            updateTotalUnreadMessage();
            //消息的内容解析请参考消息收发文档中的消息解析说明
            return false; //返回true将终止回调链，不再调用下一个新消息监听器
        }
    };


    @OnClick({R.id.ivSwitchToLive, R.id.back, R.id.tvMessage, R.id.viewMoreOrderLayout, R.id.orderUnpayCmp,
            R.id.orderPaidCmp, R.id.orderDispatchedCmp, R.id.orderServiceCmp, R.id.goodsManagementCmp, R.id.fundsManagementCmp,
            R.id.storeHomeCmp, R.id.returnAddressCmp, R.id.storeFreightCmp, R.id.orderReceivedCmp,R.id.tvWithdraw,R.id.llSettle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivSwitchToLive:
                MyLivesActivity.start(this, SessionUtil.getInstance().getLiveUser(), false);
            case R.id.back:
                finish();
                break;
            case R.id.tvMessage:
                startActivity(new Intent(this, ConversationListActivity.class));
                break;
            case R.id.viewMoreOrderLayout://查看订单明细
                Intent intent = new Intent(this, OrderListActivity.class);
                intent.putExtra("mode", OrderListActivity.MODE_LIVE_STORE);
                intent.putExtra("waitShipCount", mHomeEntity.waitShipCount);
                startActivity(intent);
                break;
            case R.id.orderUnpayCmp:
//                intent = new Intent(this, OrderListActivity.class);
//                intent.putExtra("type", "wait-pay");
//                startActivity(intent);
                break;
            case R.id.orderPaidCmp://待发货
                intent = new Intent(this, OrderListActivity.class);
                intent.putExtra("type", "paid");
                intent.putExtra("mode", OrderListActivity.MODE_LIVE_STORE);
                intent.putExtra("waitShipCount", mHomeEntity.waitShipCount);
                startActivity(intent);
                break;
            case R.id.orderDispatchedCmp://待收货
                intent = new Intent(this, OrderListActivity.class);
                intent.putExtra("type", "dispatched");
                intent.putExtra("mode", OrderListActivity.MODE_LIVE_STORE);
                intent.putExtra("waitShipCount", mHomeEntity.waitShipCount);
                startActivity(intent);
                break;
            case R.id.orderReceivedCmp://已完成
                intent = new Intent(this, OrderListActivity.class);
                intent.putExtra("type", "has-received");
                intent.putExtra("mode", OrderListActivity.MODE_LIVE_STORE);
                intent.putExtra("waitShipCount", mHomeEntity.waitShipCount);
                startActivity(intent);
                break;
            case R.id.orderServiceCmp:
                StoreRefundListActivity.start(this , StoreRefundListActivity.TYPE_LIVE_SHOP);
                break;
            case R.id.goodsManagementCmp:
                mUser = SessionUtil.getInstance().getLoginUser();
                if (mUser.autoAuthStatus != AppTypes.AUTH_STATUS.SUCESS) {
                    goAuth();
//                    GoodsManagementActivity.start(this);
                } else {
                    GoodsManagementActivity.start(this);
                }
                break;
            case R.id.tvWithdraw:
                FundsManagementActivity.start(this, FundsManagementActivity.TYPE_LOAN);
                break;
            case R.id.llSettle:
            case R.id.fundsManagementCmp:   //资金管理结算页面
                FundsManagementActivity.start(this, FundsManagementActivity.TYPE_SETTLE);
                break;
            case R.id.storeHomeCmp:
                if (mHomeEntity != null) {
                    ShopActivity.start(this, mHomeEntity.storeId, true);
                }
                break;
            case R.id.returnAddressCmp:
                Intent intent1 = new Intent(this, AddressListActivity.class);
                intent1.putExtra("isReturnGoodsAddress",true);
                startActivity(intent1);
                break;
            case R.id.storeFreightCmp:
                StoreFreightActivity.start(this, true);
                break;
        }
    }

    /**
     * 实名认证
     */
    private void goAuth() {
        UserService.checkHasPassword(this, () -> {
            switch (mUser.autoAuthStatus) {
                case AppTypes.AUTH_STATUS.WAIT:
                    APIManager.startRequest(mUserService.getAuth(), new BaseRequestListener<AuthModel>(ProviderShopActivity.this) {
                        @Override
                        public void onSuccess(AuthModel model) {
                            startActivity(new Intent(ProviderShopActivity.this, SubmitStatusActivity.class));
                            MsgStatus msgStatus = new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_FAIL);
                            msgStatus.setTips(model.memberAuthBean.checkRemark);
                            EventBus.getDefault().postSticky(msgStatus);
                        }
                    }, ProviderShopActivity.this);
                    break;
                case AppTypes.AUTH_STATUS.NO_SUBMIT:
                    Intent intent2 = new Intent(ProviderShopActivity.this, AuthPhoneActivity.class);
                    SharedPreferences sharedPreferences = ProviderShopActivity.this.getSharedPreferences("authType", 0);
                    sharedPreferences.edit().putString("authType", "UserCenter").commit();
                    intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
                    startActivity(intent2);
                    break;
                default:
                    break;
            }
        });
    }


    public static void start(Context context) {
        Intent intent = new Intent(context, ProviderShopActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onRefresh() {
        getData();
        reloadUserInfo();
    }
}
