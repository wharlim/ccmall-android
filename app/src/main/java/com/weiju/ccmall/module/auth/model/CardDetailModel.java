package com.weiju.ccmall.module.auth.model;

import com.google.gson.annotations.SerializedName;

public class CardDetailModel {

    /**
     * accountId : 0da97261b1cc40afb62078123538474c
     * phone : 18520126131
     * accountType : 1
     * bankId : 1
     * bankAccount : 12345678901234567890
     * bankUser : 余建帆111
     * alipayAccount :
     * alipayUser :
     * wechatAccount :
     * wechatUser :
     * bankName : 中国建设银行
     * bankLogo : http://img.kangerys.com/G1/M00/00/0A/CqxOrVg9NgSAZb4NAAATXJD4WcM140.png
     * accountStatus : 0
     * idcardFrontImg : 身份证正面1
     * idcardBackImg : 身份证反面1
     * idcardHeadImg : 手持身份证1
     * bankcardFrontImg : 银行卡正面1
     * bankcardProvince : 省1
     * bankcardCity : 市1
     * bankcardArea : 区1
     * bankcardAddress : 支行地址1
     * bankcardCode : 020
     * checkDate :
     * checkResult :
     * remark :
     */

    @SerializedName("accountId")
    public String accountId;
    @SerializedName("phone")
    public String phone;
    @SerializedName("accountType")
    public int accountType;
    @SerializedName("bankId")
    public String bankId;
    @SerializedName("bankAccount")
    public String bankAccount;
    @SerializedName("bankUser")
    public String bankUser;
    @SerializedName("alipayAccount")
    public String alipayAccount;
    @SerializedName("alipayUser")
    public String alipayUser;
    @SerializedName("wechatAccount")
    public String wechatAccount;
    @SerializedName("wechatUser")
    public String wechatUser;
    @SerializedName("bankName")
    public String bankName;
    @SerializedName("bankLogo")
    public String bankLogo;
    @SerializedName("accountStatus")
    public int accountStatus;
    @SerializedName("idcardFrontImg")
    public String idcardFrontImg;
    @SerializedName("idcardBackImg")
    public String idcardBackImg;
    @SerializedName("idcardHeadImg")
    public String idcardHeadImg;
    @SerializedName("bankcardFrontImg")
    public String bankcardFrontImg;
    @SerializedName("bankcardProvince")
    public String bankcardProvince;
    @SerializedName("bankcardCity")
    public String bankcardCity;
    @SerializedName("bankcardArea")
    public String bankcardArea;
    @SerializedName("bankcardAddress")
    public String bankcardAddress;
    @SerializedName("bankcardCode")
    public String bankcardCode;
    @SerializedName("checkDate")
    public String checkDate;
    @SerializedName("checkResult")
    public String checkResult;
    @SerializedName("remark")
    public String remark;
    @SerializedName("bankReservedPhone")
    public String bankReservedPhone;
    @SerializedName("identityCard")
    public String identityCard;




    /**
     * account :
     * certificateType : 1
     * idNumber : 431122199311214539
     * bankNum : 6217002930104016373
     * workNumber : dac68adea0f44385ae2b84e03609a285
     * reserveMobile :
     * extraParam : dac68adea0f44385ae2b84e03609a2851548558818743
     * status : 1
     * callbackContent :
     * createAt : 2019-01-27 11:13:38.0
     * updateAt : 2019-01-27 11:13:38.0
     * finishedAt :
     * exception :
     * oldBankNum : 6217002930104016373
     * oldBankName :
     * taxRate : 100%
     */

    @SerializedName("memberId")
    public String memberId;
    @SerializedName("name")
    public String name;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("account")
    public String account;
    @SerializedName("certificateType")
    public String certificateType;
    @SerializedName("idNumber")
    public String idNumber;
    @SerializedName("bankNum")
    public String bankNum;
    @SerializedName("workNumber")
    public String workNumber;
    @SerializedName("reserveMobile")
    public String reserveMobile;
    @SerializedName("extraParam")
    public String extraParam;
    @SerializedName("status")
    public String status;
    @SerializedName("callbackContent")
    public String callbackContent;
    @SerializedName("createAt")
    public String createAt;
    @SerializedName("updateAt")
    public String updateAt;
    @SerializedName("finishedAt")
    public String finishedAt;
    @SerializedName("exception")
    public String exception;
    @SerializedName("oldBankNum")
    public String oldBankNum;
    @SerializedName("oldBankName")
    public String oldBankName;
    @SerializedName("taxRate")
    public String taxRate;
}
