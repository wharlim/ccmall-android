package com.weiju.ccmall.module.product.views;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;

public class ActivityListLayout extends LinearLayout {
    private LayoutInflater inflater;
    public ActivityListLayout(Context context) {
        super(context);
    }

    public ActivityListLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActivityListLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        inflater = LayoutInflater.from(getContext());
    }

    public void addItem(String ccmActivity) {
        TextView item = (TextView) inflater.inflate(R.layout.item_activity_list_item, this, false);
        item.setText(Html.fromHtml(ccmActivity));
        addView(item);
    }
}
