package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PlanDetail implements Serializable {

    /**
     * payBackDetail : [{"txnDt":"2020-01-17","datas":[{"tranId":"cc20200105221251160494","reason":"","repayTime":"2020-01-17 15:47:57.0","planExcuteTime":"2020-01-17 15:14:57","txnSts":"0","feeFormat":"3.36元","payBackAmt":196.64,"payStatus":"0","rePayStatus":"0","txnAmt":200}]}]
     * frozenAmt : 210
     * startDt : 2020-01-17
     * endDt : 2020-01-17
     * resCode : 0
     * payBackAmt : 200
     * cntPerDay : 1
     * payBackDetailNum : 0
     * resMsg : 还款计划详情查询成功
     * feeAmt : 3.36
     */
    // #IntelligenceRepaymentActivity.REPAY_TYPE_FULL_AUTOMATION,REPAY_TYPE_SEMI_AUTOMATION
    @SerializedName("payType")
    public int payType; // 0 全自动，1半自动

    @SerializedName("consumeAmount")
    public float consumeAmount; // 消费金额

    @SerializedName("frozenAmt")
    public float frozenAmt;
    @SerializedName("startDt")
    public String startDt;
    @SerializedName("endDt")
    public String endDt;
    @SerializedName("resCode")
    public String resCode;
    @SerializedName("payBackAmt")
    public float payBackAmt;
    @SerializedName("cntPerDay")
    public String cntPerDay;
    @SerializedName("payBackDetailNum")
    public int payBackDetailNum;
    @SerializedName("resMsg")
    public String resMsg;
    @SerializedName("feeAmt")
    public double feeAmt;
    @SerializedName("payBackDetail")
    public List<PayBackDetailBean> payBackDetail;

    public static class PayBackDetailBean implements Serializable{
        /**
         * txnDt : 2020-01-17
         * datas : [{"tranId":"cc20200105221251160494","reason":"","repayTime":"2020-01-17 15:47:57.0","planExcuteTime":"2020-01-17 15:14:57","txnSts":"0","feeFormat":"3.36元","payBackAmt":196.64,"payStatus":"0","rePayStatus":"0","txnAmt":200}]
         */

        @SerializedName("txnDt")
        public String txnDt;
        @SerializedName("datas")
        public List<PlanItem> datas;

        public static class PlanItem implements Serializable{
            /**
             * tranId : cc20200105221251160494
             * reason :
             * repayTime : 2020-01-17 15:47:57.0
             * planExcuteTime : 2020-01-17 15:14:57
             * txnSts : 0
             * feeFormat : 3.36元
             * payBackAmt : 196.64
             * payStatus : 0
             * rePayStatus : 0
             * txnAmt : 200
             */

            @SerializedName("tranId")
            public String tranId;
            @SerializedName("reason")
            public String reason;
            @SerializedName("repayTime")
            public String repayTime;
            @SerializedName("planExcuteTime")
            public String planExcuteTime;
            @SerializedName("txnSts")
            public String txnSts;
            @SerializedName("feeFormat")
            public String feeFormat;
            @SerializedName("payBackAmt")
            public float payBackAmt;
            @SerializedName("payStatus")
            public String payStatus;
            @SerializedName("rePayStatus")
            public String rePayStatus;
            @SerializedName("txnAmt")
            public float txnAmt;
            @SerializedName("payType")
            public int payType;
        }

        public static class PlanItemSummary extends PlanItem implements Serializable{
            public String repayTimeout;
            public float payMoney;
        }
    }
}
