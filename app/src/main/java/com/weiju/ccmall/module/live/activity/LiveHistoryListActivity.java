package com.weiju.ccmall.module.live.activity;

import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.adapter.LiveHomeAdapter;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.NullLoadMoreView;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/11/23 on 11:38
 * @desc ${TODD}
 */
public class LiveHistoryListActivity extends BaseListActivity {
//    @BindView(R.id.layoutTopTitle)
//    RelativeLayout mLayoutTitle;
//    @BindView(R.id.title)
//    TextView mTitle;
//    @BindView(R.id.layoutSearch)
//    FrameLayout mLayoutSearch;
//    @BindView(R.id.avatarIv)
//    ImageView mAvatarIv;

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);

    private LiveHomeAdapter mAdapter = new LiveHomeAdapter();
    private User mLoginUser;
    private ArrayList<View> mHeaderViews;

    @Override
    public String getTitleStr() {
        return "直播记录";
    }

    @Override
    public void initView() {
        super.initView();
        EventBus.getDefault().register(this);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            QMUIStatusBarHelper.translucent(LiveHistoryListActivity.this);
//            QMUIStatusBarHelper.setStatusBarLightMode(LiveHistoryListActivity.this);
//            QMUIStatusBarHelper.setStatusBarDarkMode(LiveHistoryListActivity.this);
//        }
//        getHeaderLayout().setVisibility(View.GONE);
//        int height = QMUIStatusBarHelper.getStatusbarHeight(LiveHistoryListActivity.this);
//        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mLayoutTitle.getLayoutParams();
//        layoutParams.setMargins(0, height, 0, 0);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(LiveHistoryListActivity.this, 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position >= 1 ? 1 : 2;
            }
        });
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.background));
        mAdapter.setLoadMoreView(new NullLoadMoreView());
//        mTitle.setText("直播记录");

        mLoginUser = SessionUtil.getInstance().getLoginUser();
//        mLayoutSearch.setVisibility(View.GONE);
//        mAvatarIv.setVisibility(View.GONE);
    }

    @Override
    public View getEmptyView() {
        View inflate = View.inflate(this, R.layout.live_no_data, null);
        return inflate;
    }


    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        LiveRoom item = mAdapter.getItem(position);
        if (null == item) {
            return;
        }
        switch (item.status) {
//      0=未开始，1=直播中，2=结束，3=回放 4=预告
            case 2:
            case 0:
                ToastUtil.error(item.statusStr);
                break;
            case 1:
            case 3:
                Intent intent = new Intent(LiveHistoryListActivity.this, PlayerSkinActivity.class);
                intent.putExtra("liveId", item.liveId);
                intent.putExtra("memberId", item.memberId);
                intent.putExtra("status", item.status);
                startActivity(intent);
                break;
            default:
        }

    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mService.getLiveList(null != mLoginUser ? mLoginUser.id : "", null, mCurrentPage, Constants.PAGE_SIZE), new BaseRequestListener<PaginationEntity<LiveRoom, Object>>(mRefreshLayout) {
            @Override
            public void onSuccess(PaginationEntity<LiveRoom, Object> result) {
                if (mCurrentPage == 1) {
                    mAdapter.setNewData(result.list);
                } else {
                    mAdapter.addData(result.list);
                }
                if (result.page >= result.totalPage) {
                    mAdapter.loadMoreEnd();
                } else {
                    mAdapter.loadMoreComplete();
                }

            }
        }, LiveHistoryListActivity.this);

    }

//    @Override
//    public int getContentLayoutRes() {
//        return R.layout.activity_live_home;
//    }

//    @OnClick(R.id.back)
//    protected void back() {
//        finish();
//    }

    @Override
    public List<View> getHeaderViews() {
        if (mHeaderViews == null) {
            mHeaderViews = new ArrayList<>();
            mHeaderViews.add(createHeaderView());
        }
        return mHeaderViews;
    }


    private View createHeaderView() {
        View inflate = View.inflate(this, R.layout.view_header_live_history, null);
        return inflate;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case liveRoomChange:
                mCurrentPage = 1;
                getData(true);
                break;
            case logout:
                finish();
                break;
            default:
        }
    }

}
