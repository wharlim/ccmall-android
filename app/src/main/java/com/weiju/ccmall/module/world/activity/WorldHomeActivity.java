package com.weiju.ccmall.module.world.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.world.IWorldService;
import com.weiju.ccmall.module.world.entity.CatgoryEntity;
import com.weiju.ccmall.module.world.fragment.WorldCatgoryFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/4/24.
 */
public class WorldHomeActivity extends BaseActivity {

//    @BindView(R.id.barPadding)
//    View mBarPadding;
    @BindView(R.id.ivNoData)
    ImageView mIvNoData;
    @BindView(R.id.tvNoData)
    TextView mTvNoData;
    @BindView(R.id.tvGoMain)
    TextView mTvGoMain;
    @BindView(R.id.layoutNodata)
    LinearLayout mLayoutNodata;
    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    protected List<CatgoryEntity> pages = new ArrayList<>();
    protected List<Fragment> fragments = new ArrayList<>();
    private FragmentStatePagerAdapter fragmentStatePagerAdapter;
    private IWorldService mService = ServiceManager.getInstance().createService(IWorldService.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_home);
        ButterKnife.bind(this);
        initView();
        initData();
//        initNoData();
    }

    private void initView() {
        QMUIStatusBarHelper.translucent(this);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            QMUIStatusBarHelper.translucent(this);
//            QMUIStatusBarHelper.setStatusBarLightMode(this);
//        }
//        //导航栏高度
//        int height = QMUIStatusBarHelper.getStatusbarHeight(this);
//        mBarPadding.getLayoutParams().height = height;

        fragmentStatePagerAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return pages.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return pages.get(position).categoryName;
            }
        };
        mTabLayout.setTabIndicatorFullWidth(false);
        mViewPager.setAdapter(fragmentStatePagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(pages.size());
        mViewPager.setCurrentItem(0);
    }

    private void initData() {
        APIManager.startRequest(mService.getCatgory(), new BaseRequestListener<CatgoryEntity>(this) {
            @Override
            public void onSuccess(CatgoryEntity result) {
                super.onSuccess(result);
                pages.clear();
                fragments.clear();

                String categoryId = "home";
                String categoryName = "甄选好货";
                pages.add(new CatgoryEntity(categoryId, categoryName));
                fragments.add(WorldCatgoryFragment.newInstance(categoryId));

                for (CatgoryEntity.ChildrenBean child : result.children) {
                    fragments.add(WorldCatgoryFragment.newInstance(child.categoryId));
                    pages.add(new CatgoryEntity(child.categoryId, child.categoryName));
                }

                fragmentStatePagerAdapter.notifyDataSetChanged();
            }
        }, this);

    }

    @OnClick({R.id.ivUser, R.id.llSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivUser:
                WorldUserCenterActivity.start(this);
                break;
            case R.id.llSearch:
                WorldSearchActivity.start(this);
                break;
        }
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

    public static void start(Context context) {
        context.startActivity(new Intent(context, WorldHomeActivity.class));
    }
}
