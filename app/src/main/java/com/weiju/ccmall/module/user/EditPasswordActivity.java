package com.weiju.ccmall.module.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.transferout.SimpleTextWatcher;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Chan on 2017/6/9.
 */

public class EditPasswordActivity extends BaseActivity {

    @BindView(R.id.oldPasswordEt)
    protected EditText mOldPasswordEt;

    @BindView(R.id.newPasswordEt)
    protected EditText mNewPasswordEt;

    @BindView(R.id.surePasswordEt)
    protected EditText mSurePasswordEt;
    @BindView(R.id.tvPhone)
    TextView mTvPhone;
    @BindView(R.id.editBtn)
    TextView editBtn;
    private IUserService mUserService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_password);
        ButterKnife.bind(this);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        initView();
    }

    private void initView() {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        mTvPhone.setText(ConvertUtil.maskPhone(loginUser.phone));
        initEditText(mOldPasswordEt);
        initEditText(mNewPasswordEt);
        initEditText(mSurePasswordEt);
    }

    private void initEditText(EditText editText) {
        editText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                String oldPass = mOldPasswordEt.getText().toString();
                String newPass = mNewPasswordEt.getText().toString();
                String surePass = mSurePasswordEt.getText().toString();
                if (oldPass.length() > 0 && newPass.length() > 0 && surePass.length() > 0) {
                    editBtn.setEnabled(true);
                } else {
                    editBtn.setEnabled(false);
                }
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        showHeader();
        setTitle("修改登录密码");
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_black);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @OnClick(R.id.editBtn)
    protected void onEdit() {
        String oldPass = mOldPasswordEt.getText().toString();
        String newPass = mNewPasswordEt.getText().toString();
        String surePass = mSurePasswordEt.getText().toString();
        if (Strings.isNullOrEmpty(oldPass)) {
            ToastUtil.error("请输入当前登录密码");
            mOldPasswordEt.requestFocus();
            return;
        }
        if (Strings.isNullOrEmpty(newPass)) {
            ToastUtil.error("请输入新密码");
            mNewPasswordEt.requestFocus();
            return;
        }
        if (Strings.isNullOrEmpty(surePass)) {
            ToastUtil.error("请确认新密码");
            mSurePasswordEt.requestFocus();
            return;
        }
        if (!newPass.equals(surePass)) {
            ToastUtil.error("两次密码输入不一致");
            mSurePasswordEt.requestFocus();

            return;
        }

//        APIManager.startRequest(mUserService.editPassword(StringUtil.md5(oldPass), StringUtil.md5(newPass)), new BaseRequestListener<Object>(this) {
        APIManager.startRequest(mUserService.editPassword(oldPass, newPass), new BaseRequestListener<Object>(this) {
            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("修改成功");
                EventBus.getDefault().post(new EventMessage(Event.isEditLoginPwdSuccess));
                // 清空 Token 退出登录，并跳转到登录页
//                UserService.logout();
//                startActivity(new Intent(EditPasswordActivity.this, LoginActivity.class));
                finish();
            }
        }, this);

    }
}
