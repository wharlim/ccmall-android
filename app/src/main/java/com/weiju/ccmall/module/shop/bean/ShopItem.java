package com.weiju.ccmall.module.shop.bean;

public class ShopItem {

    /**
     * storeId : 148c2e7de3dc4420a397e0539b3f8093
     * memberId : af8d785b384ad9281286275095141f15
     * sobotId : 123
     * status : 1
     * statusStr : 正常营业
     * storeName : 礼包店铺6
     * bannerImage :
     * thumbUrl :
     * contact : 马宁
     * phone : 13922119563
     * province : 河北省
     * city : 石家庄市
     * district : 长安区
     * address : 3121312312313
     * saleProductCount : 4
     * saleCount : 9
     * expressName :
     * shipAddress :
     * descScore : 0
     * expressScore : 0
     * serveScore : 0
     * wxQrCode :
     * reason :
     * negativeComment : 0
     * concernNum : 0
     * productNum : 15
     * isDefaultHead : 0
     * collectStatus : 1
     */

    public String storeId;
    public String memberId;
    public String sobotId;
    public int status;
    public String statusStr;
    public String storeName;
    public String bannerImage;
    public String thumbUrl;
    public String contact;
    public String phone;
    public String province;
    public String city;
    public String district;
    public String address;
    public int saleProductCount;
    public int saleCount;
    public String expressName;
    public String shipAddress;
    public int descScore;
    public int expressScore;
    public int serveScore;
    public String wxQrCode;
    public String reason;
    public int negativeComment;
    public int concernNum;
    public int productNum;
    public int isDefaultHead;
    public int collectStatus;
    public int fansNum;//主播粉丝数

    @Override
    public String toString() {
        return "ShopItem{" +
                "storeId='" + storeId + '\'' +
                ", memberId='" + memberId + '\'' +
                ", sobotId='" + sobotId + '\'' +
                ", status=" + status +
                ", statusStr='" + statusStr + '\'' +
                ", storeName='" + storeName + '\'' +
                ", bannerImage='" + bannerImage + '\'' +
                ", thumbUrl='" + thumbUrl + '\'' +
                ", contact='" + contact + '\'' +
                ", phone='" + phone + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", address='" + address + '\'' +
                ", saleProductCount=" + saleProductCount +
                ", saleCount=" + saleCount +
                ", expressName='" + expressName + '\'' +
                ", shipAddress='" + shipAddress + '\'' +
                ", descScore=" + descScore +
                ", expressScore=" + expressScore +
                ", serveScore=" + serveScore +
                ", wxQrCode='" + wxQrCode + '\'' +
                ", reason='" + reason + '\'' +
                ", negativeComment=" + negativeComment +
                ", concernNum=" + concernNum +
                ", productNum=" + productNum +
                ", isDefaultHead=" + isDefaultHead +
                ", collectStatus=" + collectStatus +
                '}';
    }
}
