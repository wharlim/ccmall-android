package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

public class MerchantInResult {

    /**
     * id : 165836
     * userName : 王夏亮
     * userPhone : 13711236885
     * idCardNo : 440982199002174335
     * idCardType : 1
     * debitCardNo : 6217921003904060q
     * debitBankName : 上海浦东发展银行
     * debitCardPhone : 13711236885
     * debitBankCode : 310290000013
     * creditCardNo : 4392250041436782
     * creditBankName : 招商银行
     * creditCardPhone : 13711236885
     * creditBankCode : 308584000013
     * creditCardCvv : 7VYP47kPmHbTfXVVZZQKvw==
     * creditCardExpire : JCGmX6JHLgHcvMgBegLCOA==
     * status : 0
     * channelNo : GftP3
     * channelRate : 0.0068
     * channelCharge : 2.0
     * treatyNo : null
     * merchantCode : 157819400484453
     * merchantKey : null
     * remark : 天津市-天津市-192.168.17.15
     * createTime : 2020-01-05T03:13:12.000+0000
     * tradingPlace : 天津市
     * bindId : null
     * bindNumber : null
     * acctNo : null
     * token : null
     */

    @SerializedName("id")
    public int id;
    @SerializedName("userName")
    public String userName;
    @SerializedName("userPhone")
    public String userPhone;
    @SerializedName("idCardNo")
    public String idCardNo;
    @SerializedName("idCardType")
    public int idCardType;
    @SerializedName("debitCardNo")
    public String debitCardNo;
    @SerializedName("debitBankName")
    public String debitBankName;
    @SerializedName("debitCardPhone")
    public String debitCardPhone;
    @SerializedName("debitBankCode")
    public String debitBankCode;
    @SerializedName("creditCardNo")
    public String creditCardNo;
    @SerializedName("creditBankName")
    public String creditBankName;
    @SerializedName("creditCardPhone")
    public String creditCardPhone;
    @SerializedName("creditBankCode")
    public String creditBankCode;
    @SerializedName("creditCardCvv")
    public String creditCardCvv;
    @SerializedName("creditCardExpire")
    public String creditCardExpire;
    @SerializedName("status")
    public int status;
    @SerializedName("channelNo")
    public String channelNo;
    @SerializedName("channelRate")
    public double channelRate;
    @SerializedName("channelCharge")
    public double channelCharge;
    @SerializedName("treatyNo")
    public Object treatyNo;
    @SerializedName("merchantCode")
    public String merchantCode;
    @SerializedName("merchantKey")
    public Object merchantKey;
    @SerializedName("remark")
    public String remark;
    @SerializedName("createTime")
    public String createTime;
    @SerializedName("tradingPlace")
    public String tradingPlace;
    @SerializedName("bindId")
    public Object bindId;
    @SerializedName("bindNumber")
    public Object bindNumber;
    @SerializedName("acctNo")
    public Object acctNo;
    @SerializedName("token")
    public Object token;
    /**
     * tradeNo : null
     * userId : null
     * h5Url : https://www.56zhifu.com/user/html/upopwaphtml/20200521016706.html
     */

    @SerializedName("tradeNo")
    public Object tradeNo;
    @SerializedName("userId")
    public Object userId;
    @SerializedName("h5Url")
    public String h5Url;
    @SerializedName("verifyType")
    public int verifyType;   //新通道SqSmall：1：不需要验证 2：短信验证

}
