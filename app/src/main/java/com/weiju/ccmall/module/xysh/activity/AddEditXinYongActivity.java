package com.weiju.ccmall.module.xysh.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.SelectSupportBankItem;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.bean.XyshUser;
import com.weiju.ccmall.module.xysh.fragment.XinYongHomeFragment;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.module.xysh.helper.UpdateBankNameWatcher;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.BankSelectDateDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Action;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * @author chenyanming
 * @time 2019/12/12 on 11:13
 * @desc
 */
public class AddEditXinYongActivity extends BaseActivity {

    @BindView(R.id.etCardNumber)
    EditText mEtCardNumber;
    @BindView(R.id.ivCardBig)
    SimpleDraweeView mIvCardBig;
    @BindView(R.id.tvBankName)
    TextView mTvBankName;
    @BindView(R.id.layoutCardInfo)
    RelativeLayout mLayoutCardInfo;
    @BindView(R.id.ivSacnCard)
    ImageView mIvSacnCard;
    @BindView(R.id.tvSelectBank)
    TextView mTvSelectBank;
    @BindView(R.id.ivGoSelect)
    ImageView mIvGoSelect;
    @BindView(R.id.etPhone)
    EditText mEtPhone;
    @BindView(R.id.tvBillDate)
    TextView mTvBillDate;
    @BindView(R.id.tvPayData)
    TextView mTvPayData;
    @BindView(R.id.tvSafe)
    TextView mTvSafe;
    @BindView(R.id.layoutSafe)
    RelativeLayout mLayoutSafe;
    @BindView(R.id.tvAdd)
    TextView mTvAdd;
    @BindView(R.id.layoutSelectBank)
    LinearLayout mLayoutSelectBank;
    @BindView(R.id.tvDelete)
    TextView mTvDelete;
    @BindView(R.id.ivBankIcon)
    ImageView ivBankIcon;
    @BindView(R.id.layoutSelectBill)
    LinearLayout layoutSelectBill;
    @BindView(R.id.tvTips)
    TextView tvTips;
    @BindView(R.id.tvCardholder)
    TextView tvCardholder;
    @BindView(R.id.llCardholder)
    LinearLayout llCardholder;
    @BindView(R.id.tvCardNo)
    TextView tvCardNo;
    @BindView(R.id.llCard)
    LinearLayout llCard;

    private boolean mIsEdit;
    public static final int MY_SCAN_REQUEST_CODE = 10;

    private String mBankName;
    private String mBillDate;
    private String mPayDate;

    private BankSelectDateDialog mBillDateSelectDialog;
    private BankSelectDateDialog mPayDateSelectDialog;

    // 编辑信用卡
    QueryUserBankCardResult.BankInfListBean bankInfListBean;
    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    private boolean mIsFromAChangePhone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_xinyong_card);
        ButterKnife.bind(this);
        BankUtils.downSupportBanks();

        initIntentData();
        initView();
    }

    private void initIntentData() {
        mIsEdit = getIntent().getBooleanExtra("isEdit", false);
        mIsFromAChangePhone = getIntent().getBooleanExtra("isFromAChangePhone", false);
        if (mIsEdit) {
            bankInfListBean = (QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("bankInfListBean");
        }
    }

    private void initView() {
        setTitle(mIsEdit ? "编辑信用卡" : "添加信用卡");
        setLeftBlack();
        if (mIsEdit) {
            tvTips.setVisibility(View.GONE);
            llCardholder.setVisibility(View.GONE);
            llCard.setVisibility(View.GONE);
            mLayoutSelectBank.setVisibility(View.GONE);
            mLayoutCardInfo.setVisibility(View.VISIBLE);
            mEtCardNumber.setEnabled(false);
//            mEtPhone.setEnabled(false);
            mIvSacnCard.setVisibility(View.GONE);
            mIvGoSelect.setVisibility(View.GONE);
            mLayoutSelectBank.setEnabled(false);
            mLayoutSafe.setVisibility(View.GONE);
            mTvAdd.setText("保存");
            mTvDelete.setVisibility(mIsFromAChangePhone ? View.GONE : View.VISIBLE);
            setBillTextColor();
            setPayTextColor();
            setBankTextColor();
            initWithBankInfItem();
        } else {
            XyshUser userInfo = SessionUtil.getInstance().getXyshUserInfo();
            tvCardholder.setText(userInfo.idNm);
            String str = "<font color=\"#333333\">点击投保“</font><font color=\"#D5BA80\">" +
                    "众安保险个人账户资金损失保险</font>" +
                    "<font color=\"#333333\"> ”，用卡更安全。" + "</font>";
            mTvSafe.setText(Html.fromHtml(str));
            mTvSelectBank.setTextColor(getResources().getColor(R.color.text_gray));
            mEtCardNumber.addTextChangedListener(new UpdateBankNameWatcher(this::updateBankName));
        }
    }

    private void initWithBankInfItem() {
        if (bankInfListBean == null) {
            return;
        }
        mIvCardBig.setImageResource(BankUtils.getBankIconByName(bankInfListBean.bankName));
        mTvBankName.setText(String.format("%s", bankInfListBean.bankName));
        tvCardNo.setText(String.format("%s **** **** %s", bankInfListBean.cardNo.substring(0, 4), bankInfListBean.cardNo.substring(bankInfListBean.cardNo.length() - 4)));
        mEtCardNumber.setText(bankInfListBean.cardNo);
        mBankName = bankInfListBean.bankName;
        mTvSelectBank.setText(bankInfListBean.bankName);
        mEtPhone.setText(bankInfListBean.resTel);
        mEtPhone.setSelection(mEtPhone.length());
        mBillDate = bankInfListBean.billDate;
        mTvBillDate.setText(String.format("每月%s日", mBillDate));
        mPayDate = bankInfListBean.repayDate;
        mTvPayData.setText(String.format("每月%s日", mPayDate));
    }

    private void setBankTextColor() {
        mTvSelectBank.setTextColor(getResources().getColor(R.color.text_black));

    }

    private void setBillTextColor() {
        mTvBillDate.setTextColor(getResources().getColor(R.color.text_black));
    }

    @OnClick(R.id.tvSafe)
    protected void onSafe() {
        XinYongHomeFragment.insurance(this);
    }

    private void setPayTextColor() {
        mTvPayData.setTextColor(getResources().getColor(R.color.text_black));
    }

    @OnClick(R.id.ivSacnCard)
    protected void sacnCard() {
        Intent scanIntent = new Intent(this, CardIOActivity.class)
                .putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true)//去除水印
//                .putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, true)//去除键盘
                .putExtra(CardIOActivity.EXTRA_LANGUAGE_OR_LOCALE, "zh-Hans")//设置提示为中文
                ;
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @OnClick(R.id.tvDelete)
    public void deleteCard() {
        WJDialog dialog = new WJDialog(this);
        dialog.show();
        dialog.setCancelable(true);
        dialog.setTitle("提示");
        dialog.setContentText("是否删除该信用卡?");
        dialog.setOnCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnConfirmListener(v -> {
            dialog.dismiss();
            ToastUtil.showLoading(this, true);
            APIManager.startRequest(service.deleteBankInfo(bankInfListBean.cardNo), new Observer<XYSHCommonResult<Object>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(XYSHCommonResult<Object> ret) {
                    ToastUtil.hideLoading();
                    if (ret.success) {
                        ToastUtil.success(ret.message);
                        EventBus.getDefault().post(new EventMessage(Event.bankChange));
                        finish();
                    } else {
                        ToastUtil.error(ret.message);
                    }
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });
        });
    }

    @OnClick(R.id.layoutSelectBank)
    protected void selectBank() {
//        Intent intent = new Intent(AddEditXinYongActivity.this, SelectBankActivity.class);
//        startActivityForResult(intent, Action.SELECT_BANK);
//        SelectBankActivity.start(AddEditXinYongActivity.this, Action.SELECT_BANK);
        SelectSupportBankActivity.startForSelect(this, Action.SELECT_BANK);
    }

    @OnClick(R.id.layoutSelectBill)
    protected void selevtBill() {
        if (null == mBillDateSelectDialog) {
            mBillDateSelectDialog = new BankSelectDateDialog(AddEditXinYongActivity.this, onBillConfirmListener, "", 28);
        }
        mBillDateSelectDialog.show();
    }

    @OnClick(R.id.layoutSelectPay)
    protected void selevtPay() {
        if (null == mPayDateSelectDialog) {
            mPayDateSelectDialog = new BankSelectDateDialog(AddEditXinYongActivity.this, onPayConfirmListener, "还款日", 29);
        }
        mPayDateSelectDialog.show();
    }

    @OnClick(R.id.tvAdd)
    protected void add() {
        String cardNo = mEtCardNumber.getText().toString().trim().replace(" ", "");
        String phone = mEtPhone.getText().toString().trim();
        String bankName = mBankName;

        if (TextUtils.isEmpty(cardNo) || TextUtils.isEmpty(phone) || TextUtils.isEmpty(bankName)
                || TextUtils.isEmpty(mBillDate) || TextUtils.isEmpty(mPayDate)) {
            ToastUtil.error("请填写完整信息!");
            return;
        }
        if (mIsEdit && !phone.equals(bankInfListBean.resTel)) {
            WJDialog wjDialog = new WJDialog(this);
            wjDialog.show();
            wjDialog.setTitle("确认弹窗");
            wjDialog.setContentText("修改预留手机号后，该卡在发起新交易时需要重新认证通道哦！");
            wjDialog.setOnConfirmListener(v -> {
                wjDialog.dismiss();
                checkCard(cardNo, phone, bankName);
            });
            return;
        }
        checkCard(cardNo, phone, bankName);
    }

    private void checkCard(String cardNo, String phone, String bankName) {
        BankUtils.checkCard(this, cardNo, BankUtils.CARD_CHECK_ACCEPT_CREDIT, (pass, type) -> {
            if (pass) {
                Observable<HashMap<String, String>> observable = service.newBankAdd(cardNo, phone, bankName, "credit", mBillDate, mPayDate);
                if (mIsEdit) {
                    observable = service.changeBankInfo(cardNo, phone, bankName, "0", mBillDate, mPayDate);
                }

                ToastUtil.showLoading(this, false);
                APIManager.startRequest(observable,
                        new Observer<HashMap<String, String>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(HashMap<String, String> stringStringHashMap) {
                                if ("0000".equals(stringStringHashMap.get("resCode"))) {
                                    /*Intent intent = new Intent(AddEditXinYongActivity.this, AddSuccessActivity.class);
                                    if (mIsEdit) {
                                        intent.putExtra("title", "编辑成功");
                                    }
                                    startActivity(intent);*/
                                    ToastUtil.success(mIsEdit ? "修改成功" : "添加成功");
                                    EventBus.getDefault().post(new EventMessage(Event.bankChange, phone));
                                    finish();
                                } else {
                                    String msg = stringStringHashMap.get("message");
                                    if (TextUtils.isEmpty(msg)) {
                                        msg = stringStringHashMap.get("resMsg");
                                    }
                                    ToastUtil.error(msg + "");
                                }
                                ToastUtil.hideLoading();
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {
                            }
                        });
            } else {
                ToastUtil.error("卡号不正确");
            }
        });
    }

    private BankSelectDateDialog.OnConfirmListener onBillConfirmListener = new BankSelectDateDialog.OnConfirmListener() {

        @Override
        public void confirm(Integer date) {
            mBillDate = String.format("%02d", date);
            mTvBillDate.setText(String.format("每月%s日", mBillDate));
            setBillTextColor();
        }
    };

    private BankSelectDateDialog.OnConfirmListener onPayConfirmListener = new BankSelectDateDialog.OnConfirmListener() {

        @Override
        public void confirm(Integer date) {
            mPayDate = String.format("%02d", date);
            mTvPayData.setText(String.format("每月%s日", mPayDate));
            setPayTextColor();
        }
    };


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                mEtCardNumber.setText(scanResult.cardNumber);
            }
        } else if (requestCode == Action.SELECT_BANK) {
            if (null != data) {
                SelectSupportBankItem bank = SelectSupportBankActivity.getSelectResult(data);
                if (null != bank) {
                    updateBankName(bank.bankName);
                }
            }
        }
    }

    private void updateBankName(String bankName) {
        mBankName = bankName;
        mTvSelectBank.setText(mBankName);
        mTvSelectBank.setTextColor(getResources().getColor(R.color.text_black));
        ivBankIcon.setImageResource(BankUtils.getBankIconByName(mBankName));
    }
}
