package com.weiju.ccmall.module.jkp;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.provider.MediaStore;

import com.ali.auth.third.login.callback.LogoutCallback;
import com.alibaba.baichuan.android.trade.AlibcTrade;
import com.alibaba.baichuan.android.trade.adapter.login.AlibcLogin;
import com.alibaba.baichuan.android.trade.callback.AlibcLoginCallback;
import com.alibaba.baichuan.android.trade.callback.AlibcTradeCallback;
import com.alibaba.baichuan.android.trade.model.AlibcShowParams;
import com.alibaba.baichuan.android.trade.model.AlibcTaokeParams;
import com.alibaba.baichuan.android.trade.model.OpenType;
import com.alibaba.baichuan.android.trade.model.TradeResult;
import com.alibaba.baichuan.android.trade.page.AlibcPage;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.orhanobut.logger.Logger;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.jkp.newjkp.WebViewOAuthActivity;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OutLinkUtils {
    private static OkHttpClient httpClient;
    private static Gson gson = new Gson();

    private static OkHttpClient getHttpClient() {
        if (httpClient == null) {
            synchronized (OutLinkUtils.class) {
                if (httpClient == null) {
                    httpClient = new OkHttpClient.Builder()
                            .build();
                }
            }
        }
        return httpClient;
    }

    public static void getTaoBaoProductDetail(String productId, TaoBaoProductDetailCallBack cb) {

        String url = String.format("https://h5api.m.taobao.com/h5/" +
                "mtop.taobao.detail.getdesc/6.0/?data={\"id\":\"%s\"}", productId);
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        android.os.Handler handler = new android.os.Handler();
        getHttpClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                handler.post(() -> {
                        cb.onFailure(e.getMessage());
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                handler.post(() -> {
                    try {
                        cb.onSuccess(gson.fromJson(response.body().string(), TaoBaoProductDetail.class));
                    } catch (Exception e) {
                        cb.onFailure(e.getMessage());
                    }
                });
            }
        });
    }
    interface TaoBaoProductDetailCallBack {
        void onFailure(String msg);
        void onSuccess(TaoBaoProductDetail detail);
    }

    public static class TaoBaoProductDetail {

        /**
         * api : mtop.taobao.detail.getdesc
         * v : 6.0
         * ret : ["SUCCESS::调用成功"]
         * data : {"sellerId":"2200785001192","pcDescContent":"<p><a href=\"//detail.tmall.com/item.htm?spm=a21ag.7623865.0.0.69433872QukKvx&amp;id=608499075047\" target=\"_blank\"><img src=\"//img.alicdn.com/imgextra/i1/2200785001192/O1CN01JxKL0D1KfygUwD117_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x348\"><img src=\"//img.alicdn.com/imgextra/i4/2200785001192/O1CN01Bvd3Wm1KfygrrxkXw_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1081\" /><\/a><img src=\"//img.alicdn.com/imgextra/i3/2200785001192/O1CN010JX8qb1KfygrLrtwN_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1598\"><img src=\"//img.alicdn.com/imgextra/i3/2200785001192/O1CN01cNlvHR1KfygWaXb4M_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1000\"><img src=\"//img.alicdn.com/imgextra/i2/2200785001192/O1CN01osgwbP1KfygZhzeFz_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1000\"><img src=\"//img.alicdn.com/imgextra/i1/2200785001192/O1CN01vFdCqK1KfygV26Sh0_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1000\"><img src=\"//img.alicdn.com/imgextra/i1/2200785001192/O1CN01uGDm8d1KfygSO8tLe_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1000\"><img src=\"//img.alicdn.com/imgextra/i3/2200785001192/O1CN01hhLxOG1KfygZhyAq3_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1000\"><img src=\"//img.alicdn.com/imgextra/i2/2200785001192/O1CN01qI1Ahv1KfygZi0edQ_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1000\"><img src=\"//img.alicdn.com/imgextra/i1/2200785001192/O1CN01RwjfQ61KfygVWpfBp_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1000\"><img src=\"//img.alicdn.com/imgextra/i4/2200785001192/O1CN01aMjCdZ1KfygXNKXyg_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1000\"><img src=\"//img.alicdn.com/imgextra/i3/2200785001192/O1CN014KJRsR1KfygV26nSC_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1000\"><img src=\"//img.alicdn.com/imgextra/i3/2200785001192/O1CN01xiXHok1KfygY7pEYX_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x1000\"><img src=\"//img.alicdn.com/imgextra/i3/2200785001192/O1CN01Nvn0EN1KfygY7ngvV_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x747\"><img src=\"//img.alicdn.com/imgextra/i4/2200785001192/O1CN01AIjWgN1KfygTdBNHE_!!2200785001192.jpg\" align=\"absmiddle\" size=\"790x664\" /><\/p>","itemProperties":[{"name":"净水器品牌","value":"麦可罗 "},{"name":"型号","value":"MKL-UF01 "},{"name":"材质","value":"食品级PP "},{"name":"产地","value":"中国大陆 "},{"name":"省份","value":"广东省 "},{"name":"地市","value":"深圳市 "},{"name":"分类","value":"超滤机 净水机 净水设备 "},{"name":"颜色分类","value":"标配5级直饮净水器 "},{"name":"工作原理","value":"超滤 "},{"name":"生产企业","value":"深圳市泰普环保科技有限公司 "},{"name":"滤芯","value":"活性炭 PPF棉 超滤膜 PPC "},{"name":"使用位置","value":"终端净水 "},{"name":"功效","value":"直饮 "},{"name":"是否提供安装服务","value":"否 "},{"name":"智能类型","value":"不支持智能 "},{"name":"额定出水量","value":"120L/h "},{"name":"涉水批件批准文号","value":"粤卫字水（2019）-02-第s9037号 "},{"name":"保修期","value":"12个月 "}],"anchors":[]}
         */

        @SerializedName("api")
        public String api;
        @SerializedName("v")
        public String v;
        @SerializedName("data")
        public DataBean data;
        @SerializedName("ret")
        public List<String> ret;

        public static class DataBean {
            /**
             * sellerId : 2200785001192
             * pcDescContent : <p><a href="//detail.tmall.com/item.htm?spm=a21ag.7623865.0.0.69433872QukKvx&amp;id=608499075047" target="_blank"><img src="//img.alicdn.com/imgextra/i1/2200785001192/O1CN01JxKL0D1KfygUwD117_!!2200785001192.jpg" align="absmiddle" size="790x348"><img src="//img.alicdn.com/imgextra/i4/2200785001192/O1CN01Bvd3Wm1KfygrrxkXw_!!2200785001192.jpg" align="absmiddle" size="790x1081" /></a><img src="//img.alicdn.com/imgextra/i3/2200785001192/O1CN010JX8qb1KfygrLrtwN_!!2200785001192.jpg" align="absmiddle" size="790x1598"><img src="//img.alicdn.com/imgextra/i3/2200785001192/O1CN01cNlvHR1KfygWaXb4M_!!2200785001192.jpg" align="absmiddle" size="790x1000"><img src="//img.alicdn.com/imgextra/i2/2200785001192/O1CN01osgwbP1KfygZhzeFz_!!2200785001192.jpg" align="absmiddle" size="790x1000"><img src="//img.alicdn.com/imgextra/i1/2200785001192/O1CN01vFdCqK1KfygV26Sh0_!!2200785001192.jpg" align="absmiddle" size="790x1000"><img src="//img.alicdn.com/imgextra/i1/2200785001192/O1CN01uGDm8d1KfygSO8tLe_!!2200785001192.jpg" align="absmiddle" size="790x1000"><img src="//img.alicdn.com/imgextra/i3/2200785001192/O1CN01hhLxOG1KfygZhyAq3_!!2200785001192.jpg" align="absmiddle" size="790x1000"><img src="//img.alicdn.com/imgextra/i2/2200785001192/O1CN01qI1Ahv1KfygZi0edQ_!!2200785001192.jpg" align="absmiddle" size="790x1000"><img src="//img.alicdn.com/imgextra/i1/2200785001192/O1CN01RwjfQ61KfygVWpfBp_!!2200785001192.jpg" align="absmiddle" size="790x1000"><img src="//img.alicdn.com/imgextra/i4/2200785001192/O1CN01aMjCdZ1KfygXNKXyg_!!2200785001192.jpg" align="absmiddle" size="790x1000"><img src="//img.alicdn.com/imgextra/i3/2200785001192/O1CN014KJRsR1KfygV26nSC_!!2200785001192.jpg" align="absmiddle" size="790x1000"><img src="//img.alicdn.com/imgextra/i3/2200785001192/O1CN01xiXHok1KfygY7pEYX_!!2200785001192.jpg" align="absmiddle" size="790x1000"><img src="//img.alicdn.com/imgextra/i3/2200785001192/O1CN01Nvn0EN1KfygY7ngvV_!!2200785001192.jpg" align="absmiddle" size="790x747"><img src="//img.alicdn.com/imgextra/i4/2200785001192/O1CN01AIjWgN1KfygTdBNHE_!!2200785001192.jpg" align="absmiddle" size="790x664" /></p>
             * itemProperties : [{"name":"净水器品牌","value":"麦可罗 "},{"name":"型号","value":"MKL-UF01 "},{"name":"材质","value":"食品级PP "},{"name":"产地","value":"中国大陆 "},{"name":"省份","value":"广东省 "},{"name":"地市","value":"深圳市 "},{"name":"分类","value":"超滤机 净水机 净水设备 "},{"name":"颜色分类","value":"标配5级直饮净水器 "},{"name":"工作原理","value":"超滤 "},{"name":"生产企业","value":"深圳市泰普环保科技有限公司 "},{"name":"滤芯","value":"活性炭 PPF棉 超滤膜 PPC "},{"name":"使用位置","value":"终端净水 "},{"name":"功效","value":"直饮 "},{"name":"是否提供安装服务","value":"否 "},{"name":"智能类型","value":"不支持智能 "},{"name":"额定出水量","value":"120L/h "},{"name":"涉水批件批准文号","value":"粤卫字水（2019）-02-第s9037号 "},{"name":"保修期","value":"12个月 "}]
             * anchors : []
             */

            @SerializedName("sellerId")
            public String sellerId;
            @SerializedName("pcDescContent")
            public String pcDescContent;
            @SerializedName("itemProperties")
            public List<ItemPropertiesBean> itemProperties;
            @SerializedName("anchors")
            public List<?> anchors;

            public static class ItemPropertiesBean {
                /**
                 * name : 净水器品牌
                 * value : 麦可罗
                 */

                @SerializedName("name")
                public String name;
                @SerializedName("value")
                public String value;
            }
        }
    }

    public static String getClipboard(boolean clear) {
        ClipboardManager clipboardManager = (ClipboardManager)
                MyApplication.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = clipboardManager.getPrimaryClip();
        if (clipData != null) {
            ClipData.Item item = clipData.getItemAt(0);
            if (item != null && item.getText() != null) {
                if (clear) {
                    clipboardManager.setPrimaryClip(ClipData.newPlainText(null, null));
                }
                return item.getText().toString();
            }
        }
        return "";
    }

    public static void sharedToWxCircle(Context context, String desc, ArrayList<String> imgs) {
        //分享到微信朋友圈
        ArrayList<Uri> imageUris = new ArrayList<>();
        for (String img : imgs) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                imageUris.add(FileProvider.getUriForFile(context, AppUtils.getAppPackageName(context)+".fileProvider", new File(img)));
                imageUris.add(getImageContentUri(context, new File(img)));
            } else {
                imageUris.add(Uri.fromFile(new File(img)));
            }
        }
        for (Uri uri :
                imageUris) {
//            Log.d("Seven", "share: " + uri);
        }
        Intent weChatIntent = new Intent();
//        weChatIntent.setComponent(new ComponentName("com.tencent.mm", "com.tencent.mm.ui.tools.ShareToTimeLineUI"));
        if (imageUris.size() == 0) return;
        if (imageUris.size() == 1) {
            weChatIntent.setAction(Intent.ACTION_SEND);
            weChatIntent.setType("image/*");
            weChatIntent.putExtra(Intent.EXTRA_STREAM, imageUris.get(0));
//            weChatIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
            weChatIntent.putExtra("Kdescription", desc); //分享描述
        } else {
            weChatIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
            weChatIntent.setType("image/*");
            weChatIntent.putExtra(Intent.EXTRA_STREAM, imageUris);
            weChatIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
            weChatIntent.putExtra("Kdescription", desc); //分享描述
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            weChatIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        }
        context.startActivity(Intent.createChooser(weChatIntent, "分享到："));
//        context.startActivity(weChatIntent);
    }

    public static Uri getImageContentUri(Context context, File imageFile) {
        String filePath = imageFile.getAbsolutePath();
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Images.Media._ID }, MediaStore.Images.Media.DATA + "=? ",
                new String[] { filePath }, null);
        Uri uri = null;

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
                Uri baseUri = Uri.parse("content://media/external/images/media");
                uri = Uri.withAppendedPath(baseUri, "" + id);
            }

            cursor.close();
        }

        if (uri == null) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.DATA, filePath);
            uri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        }

        return uri;
    }

    public static void openTaobao(Activity activity, String url) {
        AlibcShowParams alibcShowParams = new AlibcShowParams(OpenType.Native, false);
        //淘宝协议
        alibcShowParams.setClientType("taobao_scheme");
        AlibcTaokeParams alibcTaokeParams = new AlibcTaokeParams("57328044", "23373400", "appisvcode");
        alibcTaokeParams.pid = "mm_26632322_6858406_23810104";
        alibcTaokeParams.subPid = "mm_26632322_6858406_23810104";
        Map<String, String> exParams = new HashMap<>();

        exParams.put("isv_code", "appisvcode");
        exParams.put("alibaba", "阿里巴巴");

        /**
         * 打开电商组件, 使用默认的webview打开
         *
         * @param activity             必填
         * @param tradePage            页面类型,必填，不可为null，详情见下面tradePage类型介绍
         * @param showParams           show参数
         * @param taokeParams          淘客参数
         * @param trackParam           yhhpass参数
         * @param tradeProcessCallback 交易流程的回调，必填，不允许为null；
         * @return 0标识跳转到手淘打开了, 1标识用h5打开,-1标识出错
         */
        AlibcTrade.show(activity, new AlibcPage(url), alibcShowParams, alibcTaokeParams, exParams, new AlibcTradeCallback() {

            @Override
            public void onTradeSuccess(TradeResult tradeResult) {
                //打开电商组件，用户操作中成功信息回调。tradeResult：成功信息（结果类型：加购，支付；支付结果）
            }

            @Override
            public void onFailure(int code, String msg) {
                //打开电商组件，用户操作中错误信息回调。code：错误码；msg：错误信息
                ToastUtil.success(msg);
            }
        });
    }

    public static void aliLogin(Activity activity) {
        AlibcLogin alibcLogin = AlibcLogin.getInstance();
//        if (alibcLogin.isLogin()) {
//            WebViewOAuthActivity.start(activity);
//            return;
//        }
        WJDialog dialog = new WJDialog(activity);
        dialog.setLayoutRes(R.layout.dialog_auth_taobao);
        dialog.setExtraClickListener(new int[]{R.id.ivClose}, v-> {
            dialog.dismiss();
        });
        dialog.show();
        dialog.hideCancelBtn();
        dialog.setOnConfirmListener((v) -> {
            dialog.dismiss();
            alibcLogin.showLogin(activity, new AlibcLoginCallback() {
                @Override
                public void onSuccess() {
                    Logger.d("获取淘宝用户信息: " + AlibcLogin.getInstance().getSession());
                    WebViewOAuthActivity.start(activity);
                }

//            @Override
//            public void onSuccess(int loginResult, String openId, String userNick) {
//                // 参数说明：
//                // loginResult(0--登录初始化成功；1--登录初始化完成；2--登录成功)
//                // openId：用户id
//                // userNick: 用户昵称
//                Log.i("", "获取淘宝用户信息: " + AlibcLogin.getInstance().getSession());
//            }

                @Override
                public void onFailure(int code, String msg) {
                    // code：错误码  msg： 错误信息
                    Logger.d("获取淘宝用户信息失败: code：" + code + ",msg:" + msg);
                }
            });
        });
    }

    public static void aliLoginFromH5(Activity activity) {
        AlibcLogin alibcLogin = AlibcLogin.getInstance();
//        if (alibcLogin.isLogin()) {
//            WebViewOAuthActivity.start(activity);
//            return;
//        }
        alibcLogin.showLogin(activity, new AlibcLoginCallback() {
            @Override
            public void onSuccess() {
                Logger.d("获取淘宝用户信息: " + AlibcLogin.getInstance().getSession());
                WebViewOAuthActivity.start(activity);
            }

//            @Override
//            public void onSuccess(int loginResult, String openId, String userNick) {
//                // 参数说明：
//                // loginResult(0--登录初始化成功；1--登录初始化完成；2--登录成功)
//                // openId：用户id
//                // userNick: 用户昵称
//                Log.i("", "获取淘宝用户信息: " + AlibcLogin.getInstance().getSession());
//            }

            @Override
            public void onFailure(int code, String msg) {
                // code：错误码  msg： 错误信息
                Logger.d("获取淘宝用户信息失败: code：" + code + ",msg:" + msg);
            }
        });
    }

    public static void aliLogout(Activity activity) {
        AlibcLogin alibcLogin = AlibcLogin.getInstance();
        if (alibcLogin.isLogin()) {
            alibcLogin.logout(activity, new LogoutCallback() {
                @Override
                public void onSuccess() {
                    Logger.d("退出淘宝成功");
                }

                @Override
                public void onFailure(int i, String s) {
                    Logger.d("退出淘宝失败: code：" + i + ",msg:" + s);
                }
            });
        }
    }
}
