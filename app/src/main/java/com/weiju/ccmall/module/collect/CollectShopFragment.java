package com.weiju.ccmall.module.collect;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.collect.bean.ShopCollectItem;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICollectService;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

public class CollectShopFragment extends BaseFragment implements PageManager.RequestListener {
    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.noDataLayout)
    protected NoData mNoDataLayout;
    private ICollectService mCollectService;
    protected CollectShopAdapter collectShopAdapter;
    private PageManager mPageManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.base_list_layout, container, false);
        ButterKnife.bind(this, view);
        mCollectService = ServiceManager.getInstance().createService(ICollectService.class);
        collectShopAdapter = new CollectShopAdapter(getContext());
        mRecyclerView.setAdapter(collectShopAdapter);
        try {
            mPageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(mRecyclerView)
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .setRequestListener(this)
                    .build(getContext());
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }
        mNoDataLayout.setImgRes(R.mipmap.no_data_collect);
        mNoDataLayout.setTextView("亲，你还没有收藏店铺哦");
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        mPageManager.onRefresh();
    }

    @Override
    public void nextPage(int page) {
        APIManager.startRequest(getRequestInterface(page), new BaseRequestListener<PaginationEntity<ShopCollectItem, Object>>(mRefreshLayout) {
            @Override
            public void onSuccess(PaginationEntity<ShopCollectItem, Object> result) {
                if (page == 1) {
                    collectShopAdapter.getItems().clear();
                }
                mPageManager.setLoading(false);
                mPageManager.setTotalPage(result.totalPage);
                collectShopAdapter.addItems(result.list);
                mNoDataLayout.setVisibility(result.total > 0 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onComplete() {
                super.onComplete();
                mRefreshLayout.setRefreshing(false);
            }
        },getContext());
    }

    protected Observable<RequestResult<PaginationEntity<ShopCollectItem, Object>>>
        getRequestInterface(int page) {
        return mCollectService.getStoreList(page);
    }

    public static CollectShopFragment newInstance() {
        return new CollectShopFragment();
    }
}
