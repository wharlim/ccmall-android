package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class QueryUserBankCardResult {

    /**
     * bankInfList : [{"resTel":"13711236885","planSts":"","cardType":"1","bankName":"上海浦东发展银行","wdDefFlg":"0","cardNo":"6217921003904060","planFlag":"0"},{"resTel":"13711236885","planSts":"","cardType":"1","bankName":"浦东发展银行","wdDefFlg":"0","cardNo":"9916217921003904060","planFlag":"0"},{"resTel":"13711236885","planSts":"","cardType":"1","bankName":"6885","wdDefFlg":"0","cardNo":"9906217921003904060","planFlag":"0"},{"resTel":"13711236885","planSts":"","cardType":"1","bankName":"6885","wdDefFlg":"0","cardNo":"9996217921003904060","planFlag":"0"}]
     * code : 1
     * bankInfNum : 4
     * resCode : 0
     * resMsg : 银行卡信息查询成功
     * idNo : 440982199002174335
     * usrName : 王夏亮
     */

    @SerializedName("code")
    public int code;
    @SerializedName("bankInfNum")
    public int bankInfNum;
    @SerializedName("resCode")
    public String resCode;
    @SerializedName("resMsg")
    public String resMsg;
    @SerializedName("idNo")
    public String idNo;
    @SerializedName("usrName")
    public String usrName;
    @SerializedName("success")
    public boolean success;
    @SerializedName("bankInfList")
    public List<BankInfListBean> bankInfList;

    public static class BankInfListBean implements Serializable {
        /**
         * resTel : 13711236885
         * planSts :
         * cardType : 1
         * bankName : 上海浦东发展银行
         * wdDefFlg : 0
         * cardNo : 6217921003904060
         * planFlag : 0
         */

        @SerializedName("resTel")
        public String resTel;
        @SerializedName("planSts")
        public String planSts;

        public boolean isRunning() {
            return "1".equals(planSts);
        }
        public boolean isSuccess() {
            return "2".equals(planSts);
        }

        public boolean isFail() {
            return "3".equals(planSts);
        }

        @SerializedName("cardType")
        public String cardType;
        @SerializedName("bankName")
        public String bankName;
        @SerializedName("wdDefFlg")
        public String wdDefFlg;
        @SerializedName("cardNo")
        public String cardNo;
        @SerializedName("planFlag")
        public String planFlag;
        public boolean hasPlan() {
            return "1".equals(planFlag);
        }
        @SerializedName("billDate")
        public String billDate;
        @SerializedName("repayDate")
        public String repayDate;
        @SerializedName("totalPoint")
        public String totalPoint;
        @SerializedName("isOpen")
        public String isOpen;
    }
}
