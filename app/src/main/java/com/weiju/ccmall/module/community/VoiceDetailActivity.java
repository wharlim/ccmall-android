package com.weiju.ccmall.module.community;

import android.databinding.DataBindingUtil;
import android.databinding.ObservableField;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.blankj.utilcode.utils.ScreenUtils;
import com.squareup.picasso.Picasso;
import com.weiju.ccmall.R;
import com.weiju.ccmall.databinding.ViewHeaderVoiceDetailBinding;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;

import org.parceler.Parcel;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by bigbyto on 18/07/2017.
 */

public class VoiceDetailActivity extends CourseDetailActivity {
    private final MediaPlayer mediaPlayer = new MediaPlayer();
    private MediaSessionCompat mMediaSession;
    private PlaybackStateCompat.Builder stateBuilder;
    private static final String MY_MEDIA_ROOT_ID = "MediaSessionManager";
    private DataHandler data;
    Course mDetail;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        data = DataFactory.create(savedInstanceState, DataHandler.class);
        try {
            mMediaSession = new MediaSessionCompat(this, MY_MEDIA_ROOT_ID);
            mMediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
            stateBuilder = new PlaybackStateCompat.Builder()
                    .setActions(PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_PLAY_PAUSE
                            | PlaybackStateCompat.ACTION_SKIP_TO_NEXT | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS);
            mMediaSession.setPlaybackState(stateBuilder.build());
            mMediaSession.setCallback(sessionCb);
            mMediaSession.setActive(true);
        } catch (Exception e) {
        }

        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                updateLocMsg();
            }
        });
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void updateData(Course detail) {
        mDetail = detail;
        binding.setItem(detail);
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) binding.ivBackground.getLayoutParams();
        layoutParams.height = ScreenUtils.getScreenWidth();
        layoutParams.width = ScreenUtils.getScreenWidth();
        binding.ivBackground.setLayoutParams(layoutParams);
        Picasso.with(this).load(detail.audioBackgroup).error(R.drawable.default_image).placeholder(R.drawable.default_image).into(binding.ivBackground);
        /*Glide.with(this).load(detail.audioBackgroup).error(R.drawable.default_image).
                placeholder(R.drawable.default_image).into(binding.ivBackground);*/
        binding.tvContent.setText(android.text.Html.fromHtml(detail.intro));
        initMediaPlayer(detail.audioUrl);

        getHeaderLayout().setVisibility(View.GONE);
        initRewordMap(detail, binding.llRewardContainer);
    }

    @Override
    public void changeCommentNub(Course course) {
        binding.setItem(course);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        DataFactory.persistState(outState, data);
        super.onSaveInstanceState(outState);
    }

    private ViewHeaderVoiceDetailBinding binding;
    private boolean isTouch;

    @Override
    protected View createHeaderView() {
        binding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.view_header_voice_detail, null, false);
        binding.setData(data);
        binding.ivPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mediaPlayer.isPlaying()) {
                    binding.ivPlay.setImageResource(R.drawable.ic_play);
                    mediaPlayer.pause();
                    updatePlaybackState(PlaybackStateCompat.STATE_PAUSED);
                    updateLocMsg();
                } else {
                    binding.ivPlay.setImageResource(R.drawable.ic_suspend);
                    mediaPlayer.start();
                    updatePlaybackState(PlaybackStateCompat.STATE_PLAYING);
                    updateLocMsg();
                }
            }
        });
        initView();

        binding.seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isTouch = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                isTouch = false;
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.seekTo(seekBar.getProgress());
                    data.progress.set(DateUtils.getVoiceTime(seekBar.getProgress() / 1000 + ""));
                } else {
                    seekBar.setProgress(mediaPlayer.getCurrentPosition());
                }
            }
        });

        return binding.getRoot();
    }


    private Timer timer;

    private void initMediaPlayer(String mp3Url) {
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        try {
            mediaPlayer.setDataSource(mp3Url);
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    binding.seekbar.setMax(mediaPlayer.getDuration());
                    String duration = DateUtils.getVoiceTime(mediaPlayer.getDuration() / 1000 + "");
                    data.duration.set(duration);
                }
            });
            mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    return false;
                }
            });

            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    binding.ivPlay.setImageResource(R.drawable.ic_play);
                    data.progress.set("00:00");
                    binding.seekbar.setProgress(0);
                    addBrowseLog();
                }
            });

            mediaPlayer.prepareAsync();
        } catch (IOException e) {
            showToast(e.getMessage());
            e.printStackTrace();
        }

        timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (mediaPlayer.isPlaying() && !isTouch) {
                    int progress = mediaPlayer.getCurrentPosition() / 1000;
                    data.progress.set(DateUtils.getVoiceTime(String.valueOf(progress)));
                    binding.seekbar.setProgress(mediaPlayer.getCurrentPosition());
                }
            }
        };

        timer.scheduleAtFixedRate(task, 0, 300);
        // 不允许拖动进度条
        binding.seekbar.setEnabled(false);
    }


    @Parcel
    public static class DataHandler {
        public ObservableField<String> progress = new ObservableField<>("00:00");
        public ObservableField<String> duration = new ObservableField<>("00:00");
    }

    @Override
    protected void onPause() {
        //关屏继续播放
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        mediaPlayer.stop();
        mediaPlayer.release();
        timer.cancel();
        mMediaSession.setActive(false);
        mMediaSession.release();
        super.onDestroy();
    }

    private void initView() {
        binding.seekbar.setPadding(0, 0, 0, 0);
        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected boolean enableFullScreenToStatusBar() {
        return true;
    }

    private MediaSessionCompat.Callback sessionCb = new MediaSessionCompat.Callback() {
        @Override
        public void onPlay() {
            super.onPlay();
            mediaPlayer.start();
            updatePlaybackState(PlaybackStateCompat.STATE_PLAYING);
        }

        @Override
        public void onPause() {
            super.onPause();
            mediaPlayer.pause();
            updatePlaybackState(PlaybackStateCompat.STATE_PAUSED);
        }

        @Override
        public void onSkipToNext() {
            super.onSkipToNext();
            mediaPlayer.stop();
            updatePlaybackState(PlaybackStateCompat.STATE_STOPPED);
//            musicPlayService.handleNextPlay();
        }

        @Override
        public void onSkipToPrevious() {
            super.onSkipToPrevious();
            mediaPlayer.stop();
            updatePlaybackState(PlaybackStateCompat.STATE_STOPPED);
        }

    };


    public void updatePlaybackState(int currentState) {
        stateBuilder.setState(currentState, mediaPlayer.getCurrentPosition(), 1.0f);
        mMediaSession.setPlaybackState(stateBuilder.build());
    }

    public void updateLocMsg() {
        try {
            //同步音频信息
            MediaMetadataCompat.Builder md = new MediaMetadataCompat.Builder();
            md.putString(MediaMetadataCompat.METADATA_KEY_TITLE, mDetail.getTitle());
            md.putLong(MediaMetadataCompat.METADATA_KEY_DURATION,mediaPlayer.getDuration());
            mMediaSession.setMetadata(md.build());
        } catch (Exception e) {
        }

    }
}
