package com.weiju.ccmall.module.page

import android.net.http.SslError
import android.os.Bundle
import android.webkit.SslErrorHandler
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import com.weiju.ccmall.R
import com.weiju.ccmall.shared.basic.AgentFragment
import kotlinx.android.synthetic.main.activity_webview.*
import android.view.View
import com.just.agentweb.AgentWeb


/**
 * Created by 林先生_lcy.
 * User: lcy
 * Date: 2019/4/8
 * Time: 下午5:02
 */
class PayWebFragment : AgentFragment(){

    override fun layoutId(): Int {
        return R.layout.activity_webview
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val url = "https://ztg.zhongan.com/promote/entrance/promoteEntrance.do?" +
//                "redirectType=h5&" +
//                "promotionCode=INST190499339001&" +
//                "productCode=PRD181140121002&" +
//                "promoteCategory=single_product&" +
//                "token="
        val url = arguments?.getString("url")
        AgentWeb.with(this)
                //传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams ,第一个参数和第二个参数应该对应。
                .setAgentWebParent(layoutWebview, FrameLayout.LayoutParams(-1, -1))
                .useDefaultIndicator()// 使用默认进度条
                .setWebViewClient(getWebViewClient())
                .createAgentWeb()//
                .ready()
                .go(url)
    }

    private fun getWebViewClient(): WebViewClient {
        return object : WebViewClient(){
            override fun onReceivedSslError(view: WebView?, handler: SslErrorHandler?, error: SslError?) {
                handler?.proceed()
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                if(url?.contains("ccmapi/sandpay/ccmall") == true || url?.contains("ccmapi/chanpaynotify/toccmall") == true)
                    activity?.finish()
            }

//            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
//                Timber.e("url = $url")
//                if (url.contains("weixin://")) { // 打开微信
//                    val uri = Uri.parse(url)
//                    val intent = Intent(Intent.ACTION_VIEW, uri)
//                    try {
//                        startActivity(intent)
//                    } catch (e: Exception) {
//                        AlertDialog.Builder(context)
//                                .setMessage("未检测到微信客户端，请安装后重试。")
//                                .setPositiveButton("知道了", null).show()
//                    }
//
//                    return true
//                } else if (url.contains("wx.tenpay.com/cgi-bin/mmpayweb-bin/checkmweb")) { // 微信H5支付中间页面设置Referer
//                    val extraHeaders = HashMap<String, String>()
//                    extraHeaders["Referer"] = "https://www.zhongan.com"
//                    view.loadUrl(url, extraHeaders)
//                } else {
//                    view.loadUrl(url)
//                }
//                return true
//            }

        }
    }

}
