package com.weiju.ccmall.module.order;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.utils.StringUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.order.adapter.RefundsOrderListAdapter;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.OrderProduct;
import com.weiju.ccmall.shared.bean.RefundsOrder;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.OrderService;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.CSUtils;
import com.weiju.ccmall.shared.util.SessionUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

/**
 * <p>
 * Created by zjm on 2017/8/19.
 */
public class NewRefundsOrderListFragment extends BaseFragment {

    @BindView(R.id.noDataLayout)
    NoData mNoDataLayout;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;

    private String mOrderCode;
    private List<RefundsOrder> mDatas = new ArrayList<>();
    private RefundsOrderListAdapter mAdapter;
    private IOrderService mOrderService;

    String mNewRetail = "";

    /**
     * 查看售后记录列表
     *
     * @param orderCode 如果传入则只显示当前订单的记录 不传入显示用户所有售后订单记录
     * @return
     */
    public static NewRefundsOrderListFragment newInstance(String orderCode, String newRetail) {
        Bundle args = new Bundle();
        args.putString(Config.INTENT_KEY_ID, orderCode);
        if (!TextUtils.isEmpty(newRetail)) {
            NewRefundsOrderListFragment fragment = new NewRefundsOrderListFragment(newRetail);
            fragment.setArguments(args);
            return fragment;
        } else {
            NewRefundsOrderListFragment fragment = new NewRefundsOrderListFragment();
            fragment.setArguments(args);
            return fragment;
        }

    }

    @SuppressLint("ValidFragment")
    public NewRefundsOrderListFragment(String newRetail) {
        mNewRetail = newRetail;
    }

    public NewRefundsOrderListFragment() {
    }

    ;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.base_list_layout, container, false);
        ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    protected boolean isNeedLogin() {
        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getIntentData();
        initView();
        getData(true);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void getData(final boolean isRefresh) {
        if (mOrderService == null) {
            mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
        }
        if (isRefresh) {
            mDatas.clear();
            mAdapter.notifyDataSetChanged();
        }
        Observable<RequestResult<List<RefundsOrder>>> observable = null;
        if (TextUtils.isEmpty(mNewRetail)) {
            if (StringUtils.isEmpty(mOrderCode)) {
                int page = mDatas.size() / Constants.PAGE_SIZE + 1;
                observable = mOrderService.getOrderRefundList(
                        page, Constants.PAGE_SIZE
                ).flatMap(new Function< RequestResult<PaginationEntity<RefundsOrder, Object>>, ObservableSource<RequestResult<List<RefundsOrder>>>>() {
                    @Override
                    public ObservableSource<RequestResult<List<RefundsOrder>>> apply(RequestResult<PaginationEntity<RefundsOrder, Object>> result) throws Exception {
                        RequestResult<List<RefundsOrder>> listRequestResult = new RequestResult<>();
                        listRequestResult.code = 0;
                        listRequestResult.data = result.data.list;
                        return Observable.just(listRequestResult);
                    }
                });
            } else {
                observable = mOrderService.getOrderRefundList(mOrderCode);
            }

            APIManager.startRequest(observable, new BaseRequestListener<List<RefundsOrder>>(mRefreshLayout) {
                @Override
                public void onSuccess(List<RefundsOrder> datas) {
                    super.onSuccess(datas);
                    mDatas.addAll(datas);
                    mAdapter.notifyDataSetChanged();
                    if (datas.size() < Constants.PAGE_SIZE || !StringUtils.isEmpty(mOrderCode)) {
                        mAdapter.loadMoreEnd();
                    } else {
                        mAdapter.loadMoreComplete();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    super.onError(e);
                    mAdapter.loadMoreFail();
                }
            }, getContext());
        } else {
            if (StringUtils.isEmpty(mOrderCode)) {
                int page = mDatas.size() / Constants.PAGE_SIZE + 1;
                observable = mOrderService.getOrderRefundList(
                        page, Constants.PAGE_SIZE, 12, "onnOrder"
                ).flatMap(new Function<RequestResult<PaginationEntity<RefundsOrder, Object>>, ObservableSource<RequestResult<List<RefundsOrder>>>>() {
                    @Override
                    public ObservableSource<RequestResult<List<RefundsOrder>>> apply(RequestResult<PaginationEntity<RefundsOrder, Object>> result) throws Exception {
                        RequestResult<List<RefundsOrder>> listRequestResult = new RequestResult<>();
                        listRequestResult.code = 0;
                        listRequestResult.data = result.data.list;
                        return Observable.just(listRequestResult);
                    }
                });
            } else {
                observable = mOrderService.getOrderRefundList(mOrderCode);
            }

            APIManager.startRequest(observable, new BaseRequestListener<List<RefundsOrder>>(mRefreshLayout) {
                @Override
                public void onSuccess(List<RefundsOrder> datas) {
                    super.onSuccess(datas);
                    mDatas.addAll(datas);
                    mAdapter.notifyDataSetChanged();
                    if (datas.size() < Constants.PAGE_SIZE || !StringUtils.isEmpty(mOrderCode)) {
                        mAdapter.loadMoreEnd();
                    } else {
                        mAdapter.loadMoreComplete();
                    }
                }

                @Override
                public void onError(Throwable e) {
                    super.onError(e);
                    mAdapter.loadMoreFail();
                }
            }, getContext());
        }


    }

    private void initView() {
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setNestedScrollingEnabled(false);
        mAdapter = new RefundsOrderListAdapter(getContext(), mDatas);
        mAdapter.setSellerModel(false);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemChildClickListener(new RefundsItemChildClickListener());
        mAdapter.setEmptyView(new NoData(getContext()));
        mRecyclerView.addOnItemTouchListener(new RefundsItemClicklListener());

        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getData(false);
            }
        });
    }

    private void getIntentData() {
        mOrderCode = getArguments().getString(Config.INTENT_KEY_ID);
    }

    private void cancelRefunds(final String refundId, boolean isRefundMoney) {
        final WJDialog wjDialog = new WJDialog(getContext());
        wjDialog.show();
        wjDialog.setTitle("撤回申请");
        if (isRefundMoney) {
            wjDialog.setContentText("撤回后，该退款单将被关闭");
        } else {
            wjDialog.setContentText("撤回后，该退货单将被关闭");
        }
        wjDialog.setCancelText("取消");
        wjDialog.setConfirmText("确定");
        wjDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wjDialog.dismiss();
                OrderService.cancelRefundExt(getActivity(), refundId);
            }
        });
    }

    private void inputRefundsInfo(RefundsOrder refundsOrder) {
        String refundId = refundsOrder.apiRefundOrderBean.refundId;
        List<OrderProduct> orderProducts = refundsOrder.orderProducts;
        Intent intent = new Intent(getActivity(), RefundExpressActivity.class);
        intent.putExtra("refundId", refundId);
        if (orderProducts != null && orderProducts.size() > 0) {
            if (orderProducts.get(0).productType == 28){
                intent.putExtra("storeBean", refundsOrder.storeBean);
            }
        }
        startActivity(intent);
    }

    private void editRefunds(RefundsOrder refundsOrder) {
        if (refundsOrder.apiRefundOrderBean.isRefundMoney()) {
            OrderService.editRefundMoneyActivity(getContext(), refundsOrder.apiRefundOrderBean.orderCode, refundsOrder.apiRefundOrderBean.refundId);
        } else {
            OrderService.addOrEditRefundOrder(getContext(), refundsOrder.apiRefundOrderBean.orderCode, refundsOrder.orderProducts, refundsOrder.apiRefundOrderBean.refundId);
        }
    }

    class RefundsItemChildClickListener implements BaseQuickAdapter.OnItemChildClickListener {
        @Override
        public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
            switch (view.getId()) {
                case R.id.tvItemCS:
                    goToService(position);
                    break;
                case R.id.tvItemEdit:
                    editRefunds(mDatas.get(position));
                    break;
                case R.id.tvItemCancel:
                    RefundsOrder refundsOrder = mDatas.get(position);
                    cancelRefunds(refundsOrder.apiRefundOrderBean.refundId, refundsOrder.apiRefundOrderBean.isRefundMoney());
                    break;
                case R.id.tvItemInput:
                    inputRefundsInfo(mDatas.get(position));
                    break;
            }
        }
    }

    private void goToService(int position) {
        RefundsOrder item = mDatas.get(position);
        List<OrderProduct> orderProducts = item.orderProducts;
        if (orderProducts != null && !orderProducts.isEmpty()) {
            if (orderProducts.get(0).productType == 18) {
                // 嗨购订单,打开im聊天客服
                contactCustomer(getContext(), item.storeBean.memberId, item.storeBean.contact);
            } else if (orderProducts.get(0).productType == 28) {
                contactCustomer(getContext(), item.storeBean.memberId, item.storeBean.storeName);
            }
        } else {
            // 其他订单，打开商城客服
            CSUtils.start(getContext(), "退款退货用户");
        }
    }

    /**
     * 联系商家客服
     * @param context
     * @param memberId
     * @param nickName
     */
    private void contactCustomer(Context context, String memberId, String nickName) {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, context);
                    ChatInfo chatInfo = new ChatInfo();
                    chatInfo.setType(TIMConversationType.C2C);
                    chatInfo.setId(memberId);
                    chatInfo.setChatName(nickName);
                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.putExtra("chatInfo", chatInfo);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }


    class RefundsItemClicklListener extends OnItemClickListener {
        @Override
        public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
            Intent intent = new Intent(getContext(), RefundDetailActivity.class);
            intent.putExtra(Config.INTENT_KEY_ID, mDatas.get(position).apiRefundOrderBean.refundId);
            startActivity(intent);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(MsgStatus msgStatus) {
        switch (msgStatus.getAction()) {
            case MsgStatus.ACTION_REFUND_CHANGE:
                getData(true);
                break;
        }
    }


}
