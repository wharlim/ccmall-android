package com.weiju.ccmall.module.product.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.blankj.utilcode.utils.StringUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder;
import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
import com.squareup.picasso.Picasso;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.VideoListener;
import com.weiju.ccmall.shared.bean.Product;
import com.weiju.ccmall.shared.util.FrescoUtil;

import java.util.ArrayList;
import java.util.List;

public class ProductVideoViewPagerAdapter extends PagerAdapter {
    private ArrayList<View> mViews = new ArrayList<>();

    public ProductVideoViewPagerAdapter(Context context, Product product) {

        if (!StringUtils.isEmpty(product.mediaUrl)) {
            View video = View.inflate(context, R.layout.view_product_video, null);
            StandardGSYVideoPlayer videoView = video.findViewById(R.id.videoView);
            setVideo(context, product, videoView);
            mViews.add(video);
        }

        for (String image : product.images) {
            View inflate = View.inflate(context, R.layout.view_product_imgs, null);
            SimpleDraweeView simpleDraweeView =  inflate.findViewById(R.id.ivProduct);
            FrescoUtil.setImage(simpleDraweeView, image);
            mViews.add(simpleDraweeView);
        }
    }

    public ProductVideoViewPagerAdapter(Context context, List<String> images) {

        for (String image : images) {
            View inflate = View.inflate(context, R.layout.view_product_imgs, null);
            SimpleDraweeView simpleDraweeView =  inflate.findViewById(R.id.ivProduct);
            FrescoUtil.setImage(simpleDraweeView, image);
            mViews.add(simpleDraweeView);
        }
    }

    private void setVideo(Context context, Product product, final StandardGSYVideoPlayer videoPlayer) {
        ImageView imageView = new ImageView(context);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        if (StringUtils.isEmpty(product.coverUrl)) {
            imageView.setImageResource(R.drawable.default_image);
        } else {
            Picasso.with(context).load(product.coverUrl).into(imageView);
        }
        GSYVideoOptionBuilder gsyVideoOptionBuilder = new GSYVideoOptionBuilder();
        gsyVideoOptionBuilder
                .setIsTouchWiget(false)
                .setThumbImageView(imageView)
                .setThumbPlay(true)
                .setUrl(product.mediaUrl)
                .setCacheWithPlay(true)
                .setRotateViewAuto(true)
                .setLockLand(true)
                .setShowFullAnimation(true)
                .setNeedLockFull(true)
                .setStandardVideoAllCallBack(new VideoListener() {
                    @Override
                    public void onPrepared(String url, Object... objects) {
                        super.onPrepared(url, objects);
                        if (!videoPlayer.isIfCurrentIsFullscreen()) {
                            //静音
                            GSYVideoManager.instance().setNeedMute(false);
                        }
                    }

                    @Override
                    public void onQuitFullscreen(String url, Object... objects) {
                        super.onQuitFullscreen(url, objects);
                        //全屏不静音
                        GSYVideoManager.instance().setNeedMute(false);
                    }

                    @Override
                    public void onEnterFullscreen(String url, Object... objects) {
                        super.onEnterFullscreen(url, objects);
                        GSYVideoManager.instance().setNeedMute(false);
                    }
                }).build(videoPlayer);
        //增加title
        videoPlayer.getTitleTextView().setVisibility(View.GONE);
        //设置返回键
        videoPlayer.getBackButton().setVisibility(View.GONE);
        videoPlayer.getFullscreenButton().setVisibility(View.GONE);
    }

    @Override
    public int getCount() {
        return mViews.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    //销毁一个页卡(即ViewPager的一个item)
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(mViews.get(position));
    }

    //对应页卡添加上数据
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(mViews.get(position));//千万别忘记添加到container
        return mViews.get(position);
    }
}
