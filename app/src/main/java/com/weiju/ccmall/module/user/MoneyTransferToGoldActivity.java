package com.weiju.ccmall.module.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.DecimalEditText;
import com.weiju.ccmall.shared.component.dialog.CheckPasswordDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanmin
 * @date 2019/6/22
 * @desc
 */
public class MoneyTransferToGoldActivity extends BaseActivity {
    @BindView(R.id.moneyEt)
    protected DecimalEditText mMoneyEt;
    private long mMoney;
    private CheckPasswordDialog mCheckPasswordDialog;
    private IUserService mService = ServiceManager.getInstance().createService(IUserService.class);
    ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_money_transfer_to_gold);
        ButterKnife.bind(this);
        initIntent();
        initView();
    }

    private void initIntent() {
        mMoney = getIntent().getLongExtra("money", 0);
    }

    private void initView() {
        setTitle("转购物券");
        setLeftBlack();
        mMoneyEt.setHint(String.format("最多可转购物券%s", MoneyUtil.centToYuanStrNoZero(mMoney)));

        mMoneyEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0 && !s.toString().equals(".")) {
                    double coinD = Double.parseDouble(s.toString());
                    double coinL = coinD * 100;
                    if (coinL > mMoney) {
                        ToastUtil.error("您的购物券不足");
                        String content = s.toString().substring(0, s.length() - 1);
                        mMoneyEt.setText(content);
                        mMoneyEt.setSelection(content.length());
                        return;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @OnClick(R.id.confirmBtn)
    protected void confrim() {
        if (TextUtils.isEmpty(mMoneyEt.getText().toString())) {
            ToastUtil.error("请输入购物券");
            return;
        }
        if (mCheckPasswordDialog == null) {
            mCheckPasswordDialog = new CheckPasswordDialog(MoneyTransferToGoldActivity.this);
            mCheckPasswordDialog.setOnConfirmListener(onConfirmListener);
        }
        mCheckPasswordDialog.show();
    }

    private CheckPasswordDialog.OnConfirmListener onConfirmListener = new CheckPasswordDialog.OnConfirmListener() {
        @Override
        public void confirm(String password) {
            ToastUtil.showLoading(MoneyTransferToGoldActivity.this);
            HashMap<String, Object> params = new HashMap<>();
            params.put("transferMoney", Double.parseDouble(mMoneyEt.getText().toString()) * 100);
            params.put("password", password);
            params.put("checkNumber", "");
            params.put("transferFrom", 1);
            params.put("trsMemo", "");
            APIManager.startRequest(mService.moneyTransferToGold(APIManager.buildJsonBody(params)), new BaseRequestListener<Object>() {
                        @Override
                        public void onSuccess(Object result) {
                            ToastUtil.hideLoading();
                            EventBus.getDefault().post(new EventMessage(Event.transToGoldSuccess));
                            ToastUtil.success("转购物券成功");
                            finish();
                        }

                        @Override
                        public void onError(Throwable e) {
                            super.onError(e);
                            ToastUtil.hideLoading();
                        }
                    },MoneyTransferToGoldActivity.this
            );
        }
    };
}
