package com.weiju.ccmall.module.page;

import android.content.Context;
import android.content.Intent;
import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;

import com.blankj.utilcode.utils.AppUtils;
import com.blankj.utilcode.utils.FileUtils;
import com.blankj.utilcode.utils.LogUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.ImageViewState;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.ProductQrcodeShowActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.component.dialog.ShareDialog;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

public class WebViewShotShareActivity extends BaseActivity {

    private static final int IMG_TYPE_LOCAL = 1;
    private static final int IMG_TYPE_REMOTE = 2;

    private int imgType;

    @BindView(R.id.ivShotImg)
    SubsamplingScaleImageView ivShotImg;

    private String shareFilePath;
    private String imgUrl;
    private boolean deleteAfterShare;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_shot_share);
        ButterKnife.bind(this);
        imgType = getIntent().getIntExtra("imgType", IMG_TYPE_LOCAL);
        shareFilePath = getIntent().getStringExtra("shareFilePath");
        imgUrl = getIntent().getStringExtra("imgUrl");
        deleteAfterShare = getIntent().getBooleanExtra("deleteAfterShare", true);
        setTitle("分享");
        setLeftBlack();
//        Picasso.with(this).load(new File(shareFilePath)).into(ivShotImg);
        ivShotImg.setMinimumScaleType(SubsamplingScaleImageView.SCALE_TYPE_CUSTOM);

        ivShotImg.setMinScale(1.0F);//最小显示比例

        ivShotImg.setMaxScale(10.0F);//最大显示比例（太大了图片显示会失真，因为一般微博长图的宽度不会太宽）
        if (imgType == IMG_TYPE_LOCAL) {
            ivShotImg.setImage(ImageSource.uri(Uri.fromFile(new File(shareFilePath))),
                    new ImageViewState(1.0F, new PointF(0, 0), 0));
        } else if (imgType == IMG_TYPE_REMOTE) {
            ToastUtil.showLoading(this, true);
            Glide.with(this)
                    .downloadOnly()
                    .load(imgUrl)
                    .into(new SimpleTarget<File>() {
                        @Override
                        public void onResourceReady(@NonNull File resource, @Nullable Transition<? super File> transition) {
                            ToastUtil.hideLoading();
                            final File savedImageFile = ProductQrcodeShowActivity.getSaveFile();
                            FileUtils.copyFile(resource, savedImageFile);
                            ivShotImg.setImage(ImageSource.uri(Uri.fromFile(savedImageFile)),
                                    new ImageViewState(1.0F, new PointF(0, 0), 0));
                            shareFilePath = savedImageFile.getAbsolutePath();
                        }

                        @Override
                        public void onStop() {
                            super.onStop();
                            ToastUtil.hideLoading();
                        }
                    });
        }
    }

    @OnClick({R.id.tvSave, R.id.tvShare})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvSave:
                // 因为文件已经保存到外部sd卡，所以这里仅仅是通知系统更新
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                intent.setData(Uri.fromFile(new File(shareFilePath)));
                sendBroadcast(intent);
                ToastUtil.success("保存成功");
                break;
            case R.id.tvShare:
                if (imgType == IMG_TYPE_LOCAL) {
                    startShareLocalImg();
                } else if (imgType == IMG_TYPE_REMOTE) {
                    startShareRemoteImg();
                }
                break;
        }
    }

    private void startShareRemoteImg() {
        new ShareDialog(this, AppUtils.getAppName(MyApplication.getContext()), imgUrl, new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                ToastUtil.showLoading(WebViewShotShareActivity.this);
            }

            @Override
            public void onResult(SHARE_MEDIA share_media) {
                ToastUtil.success("分享成功");
                ToastUtil.hideLoading();
            }

            @Override
            public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                ToastUtil.error("分享出错:" + throwable.getMessage());
                ToastUtil.hideLoading();
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media) {
                ToastUtil.hideLoading();
            }
        }).show();
    }

    private void startShareLocalImg() {
        File shareOriginFile = new File(shareFilePath);
        ToastUtil.showLoading(this, true);
        Luban.with(this)
                .load(shareOriginFile)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        LogUtils.e("压缩图片开始");
                    }

                    @Override
                    public void onSuccess(File file) {
                        ToastUtil.hideLoading();
                        showShareDialog(file);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtil.error("压缩图片出错:" + e.getMessage());
                        ToastUtil.hideLoading();
                    }
                }).launch();
    }

    private void showShareDialog(final File file) {
        new ShareDialog(this, file, new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                ToastUtil.showLoading(WebViewShotShareActivity.this);
            }

            @Override
            public void onResult(SHARE_MEDIA share_media) {
                ToastUtil.success("分享成功");
                ToastUtil.hideLoading();
                file.delete();
            }

            @Override
            public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                ToastUtil.error("分享出错:" + throwable.getMessage());
                ToastUtil.hideLoading();
                file.delete();
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media) {
                ToastUtil.hideLoading();
                file.delete();
            }
        }).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (deleteAfterShare) {
            (new File(shareFilePath)).delete();
        }
    }

    public static void start(Context context, String filePath, boolean deleteAfterShare) {
        Intent intent = new Intent(context, WebViewShotShareActivity.class);
        intent.putExtra("shareFilePath", filePath);
        intent.putExtra("deleteAfterShare", deleteAfterShare);
        intent.putExtra("imgType", IMG_TYPE_LOCAL);
        context.startActivity(intent);
    }

    public static void start(Context context, String imgUrl) {
        Intent intent = new Intent(context, WebViewShotShareActivity.class);
        intent.putExtra("imgUrl", imgUrl);
        intent.putExtra("imgType", IMG_TYPE_REMOTE);
        context.startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ToastUtil.hideLoading();
    }
}
