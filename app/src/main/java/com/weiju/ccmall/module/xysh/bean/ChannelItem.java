package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class ChannelItem implements Serializable {

    /**
     * paymentChannelId : 107
     * channelName : 大额线上通道G
     * recommend : 0
     * clearingForm : 大额线上通道G
     * remind : 支持大部分主流银行
     * createDate : 1569243352000
     * channelStatus : 1
     * minimum : 200
     * maxmum : 50000
     * startTime : 9:00
     * endTime : 21:00
     * channelRate : 0.68
     * serviceCharge : 2
     * singleMinimum : 200
     * singleLimit : 20000
     * dailyLimit : 50000
     * hasRegisterUrl : http://47.110.240.229:8598/userInfo/commonHasRegister
     * registerUrl : http://47.110.240.229/app/#/newZxXiaoeBindCard?channelId=9&rate=0.0068&serviceCharge=2
     * payUrl : http://47.110.240.229/app/#/newZxXiaoeBindList?channelId=9&rate=0.0068&serviceCharge=2
     * testStatus : 1
     * channelShortName : GftP3
     * agencyCostsRate : 0.59
     * agencyCostsCharge : 1
     * transactionSwitch : 1
     * channelCcmRate : 1.8
     * splitOrder : 0
     * openStatus : 0
     * authCodeStatus : 1
     * channelType : 0
     */

    @SerializedName("paymentChannelId")
    public String paymentChannelId;
    @SerializedName("channelName")
    public String channelName;
    @SerializedName("recommend")
    public int recommend;
    @SerializedName("clearingForm")
    public String clearingForm;
    @SerializedName("remind")
    public String remind;
    @SerializedName("createDate")
    public long createDate;
    @SerializedName("channelStatus")
    public int channelStatus;
    @SerializedName("minimum")
    public int minimum;
    @SerializedName("maxmum")
    public int maxmum;
    @SerializedName("startTime")
    public String startTime;
    @SerializedName("endTime")
    public String endTime;
    @SerializedName("channelRate")
    public double channelRate;
    @SerializedName("serviceCharge")
    public float serviceCharge;
    @SerializedName("singleMinimum")
    public int singleMinimum;
    @SerializedName("singleLimit")
    public int singleLimit;
    @SerializedName("dailyLimit")
    public int dailyLimit;
    @SerializedName("hasRegisterUrl")
    public String hasRegisterUrl;
    @SerializedName("registerUrl")
    public String registerUrl;
    @SerializedName("payUrl")
    public String payUrl;
    @SerializedName("testStatus")
    public int testStatus;
    @SerializedName("channelShortName")
    public String channelShortName;
    @SerializedName("agencyCostsRate")
    public double agencyCostsRate;
    @SerializedName("agencyCostsCharge")
    public String agencyCostsCharge;
    @SerializedName("transactionSwitch")
    public String transactionSwitch;
    @SerializedName("channelCcmRate")
    public double channelCcmRate;
    @SerializedName("splitOrder")
    public int splitOrder;
    @SerializedName("openStatus")
    public int openStatus;
    @SerializedName("authCodeStatus")
    public int authCodeStatus;
    @SerializedName("channelType")
    public int channelType;
    @SerializedName("accountDate")
    public String accountDate;

    @SerializedName("h5Verification")
    public int h5Verification;

    public Boolean wasInTime;
    public boolean getWasInTime() {
        if (wasInTime != null) {
            return wasInTime;
        }
        boolean wasInTime;
        try {
            Date now = new Date();
            Date start = new Date();
            Date end = new Date();
            // 设置开始时间
            String[] p = startTime.split(":");
            start.setHours(Integer.valueOf(p[0]));
            start.setMinutes(Integer.valueOf(p[1]));
            // 设置结束时间
            p = endTime.split(":");
            end.setHours(Integer.valueOf(p[0]));
            end.setMinutes(Integer.valueOf(p[1]));

            wasInTime = now.after(start) && now.before(end);
        } catch (Exception e) {
            wasInTime = false;
        }
        this.wasInTime = wasInTime;
        return wasInTime;
    }

    public boolean isSupport() {
        return openStatus == 0 || openStatus == 1 || openStatus == -1;
    }
}
