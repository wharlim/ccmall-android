package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.utils.PhoneUtils;
import com.blankj.utilcode.utils.ToastUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.adapter.OpeningLiveAdapter;
import com.weiju.ccmall.module.live.consts.OpeningLiveType;
import com.weiju.ccmall.module.pay.PayMsg;
import com.weiju.ccmall.module.pay.PayOrderActivity;
import com.weiju.ccmall.module.user.EditPhoneActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.basic.BaseSubscriber;
import com.weiju.ccmall.shared.bean.OpeningLiveAuthInfo;
import com.weiju.ccmall.shared.bean.OpeningLiveOrder;
import com.weiju.ccmall.shared.bean.OpeningLiveRecItem;
import com.weiju.ccmall.shared.bean.OpeningLiveSearchResult;
import com.weiju.ccmall.shared.bean.OpeningLiveVipLeaderInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.ValidateUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OpeningLiveActivity extends BaseListActivity {

    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.tvLeftTime)
    TextView tvLeftTime;
    @BindView(R.id.tvOpeningDesc)
    TextView tvOpeningDesc;

    private int openingLiveType;
    private BaseActionDataProvider provider;

    private int surplusAuthorizeNum;

    private ILiveService liveService = ServiceManager.getInstance().createService(ILiveService.class);
    private OpeningLiveAdapter adapter = new OpeningLiveAdapter();

    public static void start(Context context, int openingLiveType) {
        Intent intent = new Intent(context, OpeningLiveActivity.class);
        intent.putExtra("openingLiveType", openingLiveType);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getLeftTime();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        openingLiveType = getIntent().getIntExtra("openingLiveType", OpeningLiveType.LEADER);
        switch (openingLiveType) {
            case OpeningLiveType.LEADER:
                provider = new LeaderProvider();
                break;
            case OpeningLiveType.VIP:
                provider = new VipProvider();
                break;
            case OpeningLiveType.ANCHOR:
                provider = new AnchorProvider();
                break;
        }
    }

    @Override
    public void initView() {
        super.initView();
        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH || (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    search(textView.getText().toString());
                    return true;
                }
                return false;
            }
        });
        tvOpeningDesc.setText(provider.getDesc());
    }

    private void search(String keyword) {
        if (surplusAuthorizeNum <=0) {
            ToastUtil.error("剩余次数不足!");
            return;
        }
        if (TextUtils.isEmpty(keyword)) {
            ToastUtil.error("请输入手机号");
            return;
        }
        if (!ValidateUtil.isPhone(keyword)) {
            ToastUtil.error("请输入正确的手机号");
            return;
        }
        ToastUtil.showLoading(this, true);
        APIManager.startRequest(liveService.searchMember(keyword, provider.searchType()), new BaseSubscriber<RequestResult<OpeningLiveSearchResult>>() {
            @Override
            public void onNext(RequestResult<OpeningLiveSearchResult> result) {
                ToastUtil.hideLoading();
                if (result.code == 0) {
                    showCanOpeningDialog(result.data);
                } else {
                    ToastUtils.showLongToast(result.message);
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.hideLoading();
            }
        });
    }

    private void showCanOpeningDialog(OpeningLiveSearchResult result) {
        WJDialog dialog = new WJDialog(this);
        dialog.setLayoutRes(R.layout.dialog_opening_live);
        dialog.setExtraClickListener(new int[]{R.id.ivClose}, v-> {
            dialog.dismiss();
        });
        dialog.setOnShowListener(dialog1 -> {
            ImageView iv = dialog.findViewById(R.id.civIcon);
            TextView tips = dialog.findViewById(R.id.tvTips);
            if (iv != null) {
                Glide.with(OpeningLiveActivity.this)
                        .load(Uri.parse(result.headImage))
                        .apply(RequestOptions.placeholderOf(R.mipmap.default_avatar))
                        .into(iv);
            }
            if (tips != null) {
                tips.setVisibility(View.VISIBLE);
                tips.setText(String.format("确定帮该会员%s权限吗？", provider.getTitle()));
            }
        });
        dialog.show();
        dialog.setTitle(result.nickName);
        dialog.setContentText(result.phone);
        dialog.setCancelText("取消");
        dialog.setConfirmText(provider.confirmText());
        dialog.setOnConfirmListener((v) -> {
            dialog.dismiss();
            provider.openAction(result.memberId);
        });
    }

    private void opening(String memId) {
        ToastUtil.showLoading(this, true);
        APIManager.startRequest(liveService.opening(memId, provider.searchType()), new BaseSubscriber<RequestResult<OpeningLiveSearchResult>>() {
            @Override
            public void onNext(RequestResult<OpeningLiveSearchResult> ret) {
                super.onNext(ret);
                ToastUtil.hideLoading();
                if (ret.code == 0) {
                    surplusAuthorizeNum--;
                    showOpeningResult(true, "");
                } else {
                    showOpeningResult(false, ret.message);
                }
                updateLeftTime();
                getData(true);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.hideLoading();
            }
        });
    }

    private void showOpeningResult(boolean success, String msg) {
        WJDialog dialog = new WJDialog(this);
        dialog.setLayoutRes(R.layout.dialog_opening_live);
        dialog.setExtraClickListener(new int[]{R.id.ivClose}, v-> {
            dialog.dismiss();
        });

        dialog.setOnShowListener(dialog1 -> {
            ImageView iv = dialog.findViewById(R.id.civIcon);
            int icRes = R.drawable.ic_live_opening_fail;
            if (success) {
                icRes = R.drawable.ic_live_opening_success;
            }
            if (iv != null) {
                iv.setImageResource(icRes);
            }
        });
        dialog.show();
        dialog.setTitle(success?provider.successTitle(): TextUtils.isEmpty(msg) ? "开通失败，请您稍后再试" : msg);
        dialog.setContentText(success?provider.getTitle() + String.format("名额剩余%d次", surplusAuthorizeNum):"");
        dialog.hideCancelBtn();
        dialog.setConfirmText("我知道了");
        dialog.setOnConfirmListener((v) -> {
            dialog.dismiss();
        });
    }

    @Override
    public String getTitleStr() {
        return provider.getTitle();
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    public View getEmptyView() {
        View inflate = View.inflate(this, R.layout.openinglive_no_data, null);
        return inflate;
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(liveService.getLiveAuthorizeList(mCurrentPage, Constants.PAGE_SIZE, provider.searchType()), new BaseRequestListener<PaginationEntity<OpeningLiveRecItem, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<OpeningLiveRecItem, Object> result) {
                super.onSuccess(result);
                if (mCurrentPage == 1) {
                    adapter.setNewData(result.list);
                } else {
                    adapter.addData(result.list);
                }
                if (result.page >= result.totalPage) {
                    adapter.loadMoreEnd();
                } else {
                    adapter.loadMoreComplete();
                }
                mRefreshLayout.setRefreshing(false);
            }
        }, this);
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_opening_live;
    }

    private void getLeftTime() {
        provider.getLeftTime(leftTime -> {
            surplusAuthorizeNum = leftTime;
            updateLeftTime();
        });
    }

    private void updateLeftTime() {
        tvLeftTime.setText(String.format("剩余次数：%d次", surplusAuthorizeNum));
    }

    @OnClick(R.id.ivSearch)
    public void onViewClicked() {
        search(etSearch.getText().toString());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(PayMsg msgStatus) {
        switch (msgStatus.getAction()) {
            case PayMsg.ACTION_BALANCE_SUCCEED:
                // 开通主播成功！
                surplusAuthorizeNum--;
                showOpeningResult(true, "");
                updateLeftTime();
                getData(true);
                break;
            default:
        }
    }

    interface GetLeftTimesCallback {
        void onLeftTime(int times);
    }

    class BaseActionDataProvider {
        String getTitle() {
            return "开通直播";
        }
        String getDesc() {
            return "";
        }
        String successTitle() {
            return "开通成功!";
        }
        String confirmText() {
            return "开通";
        }
        void openAction(String memberId) {
            opening(memberId);
        }
        void getLeftTime(GetLeftTimesCallback callback) {
            callback.onLeftTime(0);
        }
        String searchType() {
            return null;
        }
    }
    protected class LeaderProvider extends BaseActionDataProvider{
        @Override
        String getTitle() {
            return "开通督导";
        }

        @Override
        String getDesc() {
            return "帮助我的客户开通督导权限";
        }

        @Override
        void getLeftTime(GetLeftTimesCallback callback) {
            APIManager.startRequest(liveService.getUserAuthorizeLevelInfo(), new BaseRequestListener<OpeningLiveVipLeaderInfo>() {
                @Override
                public void onSuccess(OpeningLiveVipLeaderInfo result) {
                    super.onSuccess(result);
                    callback.onLeftTime(result.surplusSupervisorNum);
                }
            }, OpeningLiveActivity.this);
        }

        @Override
        String searchType() {
            return "supervisor";
        }
    }

    protected class VipProvider extends BaseActionDataProvider {
        @Override
        String getTitle() {
            return "开通VIP";
        }

        @Override
        String getDesc() {
            return "帮用户开通直播VIP";
        }

        @Override
        void getLeftTime(GetLeftTimesCallback callback) {
            APIManager.startRequest(liveService.getUserAuthorizeLevelInfo(), new BaseRequestListener<OpeningLiveVipLeaderInfo>() {
                @Override
                public void onSuccess(OpeningLiveVipLeaderInfo result) {
                    super.onSuccess(result);
                    callback.onLeftTime(result.surplusVipNum);
                }
            }, OpeningLiveActivity.this);
        }

        @Override
        String searchType() {
            return "vip";
        }
    }

    protected class AnchorProvider extends BaseActionDataProvider {
        @Override
        String getTitle() {
            return "开通主播";
        }

        @Override
        String getDesc() {
            return "赠送名额使用通道";
        }

        @Override
        String successTitle() {
            return "开通成功，Ta可以进行直播啦～";
        }

        @Override
        String confirmText() {
            return "去支付开通";
        }

        @Override
        void openAction(String memberId) {
            APIManager.startRequest(liveService.addAuthorizeOrder(memberId), new BaseRequestListener<OpeningLiveOrder>() {
                @Override
                public void onSuccess(OpeningLiveOrder result) {
                    Intent intent = new Intent(OpeningLiveActivity.this, PayOrderActivity.class);
                    intent.putExtra("orderCode", result.orderCode);
                    intent.putExtra("goodsType", PayOrderActivity.GOODSTYPE_ANCHOR); // 开通主播技术费
                    startActivity(intent);
                }
            }, OpeningLiveActivity.this);
        }

        @Override
        void getLeftTime(GetLeftTimesCallback callback) {
            APIManager.startRequest(liveService.getAuthorizeUserInfo(), new BaseRequestListener<OpeningLiveAuthInfo>() {
                @Override
                public void onSuccess(OpeningLiveAuthInfo result) {
                    super.onSuccess(result);
                    callback.onLeftTime(result.surplusAuthorizeNum);
                }
            }, OpeningLiveActivity.this);
        }
    }
}
