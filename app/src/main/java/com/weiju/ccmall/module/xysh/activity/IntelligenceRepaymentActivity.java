package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.blankj.utilcode.utils.SpannableStringUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.DateUtils;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.repayment.RepaymentPlanActivity;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.bean.PlanCreateItem;
import com.weiju.ccmall.module.xysh.bean.PlanDetail;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.fragment.PayChannelBottomSheetDialogFragment;
import com.weiju.ccmall.module.xysh.fragment.SelectDateDialogFragment;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.module.xysh.helper.PayBackPlanCreateHelper;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.component.dialog.ConfirmRepaymentDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static com.weiju.ccmall.shared.util.UiUtils.createSimpleTextWatcher;

/**
 * 智能还款首页
 */
public class IntelligenceRepaymentActivity extends BaseActivity {
    @BindView(R.id.ivBankCardIcon)
    SimpleDraweeView ivBankCardIcon;
    @BindView(R.id.tvCardNameAndNo)
    TextView tvCardNameAndNo;
    @BindView(R.id.tvBillDate)
    TextView tvBillDate;
    @BindView(R.id.tvPayDate)
    TextView tvPayDate;
    @BindView(R.id.tvCheckPlan)
    TextView tvCheckPlan;
    @BindView(R.id.et_repayment_money)
    EditText etRepaymentMoney;
    @BindView(R.id.et_start_money)
    EditText etStartMoney;
    @BindView(R.id.tv_tips)
    TextView tvTips;
    @BindView(R.id.tvTipsRepay)
    TextView tvTipsRepay;
    @BindView(R.id.sw_change_time_type)
    Switch swChangeTimeType;
    @BindView(R.id.tv_start_time)
    TextView tvStartTime;
    @BindView(R.id.ll_start_time)
    LinearLayout llStartTime;
    @BindView(R.id.tv_end_time)
    TextView tvEndTime;
    @BindView(R.id.tvSwitchType)
    TextView tvSwitchType;
    @BindView(R.id.ll_end_time)
    LinearLayout llEndTime;
    @BindView(R.id.ll_date_type_range)
    LinearLayout llDateTypeRange;
    @BindView(R.id.ll_time_container)
    LinearLayout llTimeContainer;
    @BindView(R.id.tv_custom_date)
    TextView tvCustomDate;
    @BindView(R.id.ll_date_type_custom)
    LinearLayout llDateTypeCustom;
    @BindView(R.id.tvTipText)
    TextView tvTipText;
    @BindView(R.id.flTipContainer)
    FrameLayout flTipContainer;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.llContent)
    LinearLayout llContent;
    @BindView(R.id.ll_use_help)
    LinearLayout ll_use_help;
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    private QueryUserBankCardResult.BankInfListBean creditCardItem;
    private QueryUserBankCardResult.BankInfListBean debitCard;
    private ChannelItem channelItem;
    public static final int REQUEST_CODE_SELECT_CARD = 1;
    public static final int REQ_CODE_SELECT_CHANNEL = 2;
    public static final int REQ_CODE_SELECT_DEBIT_CARD = 3;

    public static final int DIALOG_SELECT_START_DATE = 2;
    public static final int DIALOG_SELECT_END_DATE = 3;
    public static final int DIALOG_SELECT_CUSTOM_DATE = 3;

    public static final int REPAY_TYPE_FULL_AUTOMATION = 0;
    public static final int REPAY_TYPE_SEMI_AUTOMATION = 1;

    private String startDate;
    private String endDate;
    private String finalEndDate;
    private Set<String> customDate;
    private float launchMoney; // 启动金额
    private float averageMoney; // 平均每天还款金额
    private boolean startMoneyIsAllow;

    private int type = REPAY_TYPE_FULL_AUTOMATION;
    private static boolean isSwitchMsgShown;

    private String errorMsg = "启动资金不正确";
    private boolean hadOpenedUseHelp;
    private final float downLimit = 300f; // 启动基金不能小于300

    private PayChannelBottomSheetDialogFragment dialogFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intelligence_repayment);
        ButterKnife.bind(this);
        setTitle("填写还款信息");
        setLeftBlack();
        swChangeTimeType.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                llDateTypeCustom.setVisibility(View.VISIBLE);
                llDateTypeRange.setVisibility(View.GONE);
            } else {
                llDateTypeCustom.setVisibility(View.GONE);
                llDateTypeRange.setVisibility(View.VISIBLE);
            }
        });
        loadDefaultCreditCard();
        etRepaymentMoney.addTextChangedListener(createSimpleTextWatcher(this::onRepayChange));
        etRepaymentMoney.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                onRepayChange();
            }
        });
        etStartMoney.addTextChangedListener(createSimpleTextWatcher(this::onStartChange));
        initView();
        KeyboardVisibilityEvent.setEventListener(this, isOpen -> {
            if (isOpen) {
                if (etStartMoney.hasFocus()) {
                    scrollTo(tvTips);
                }
            }
        });

        initStartDate();
        etRepaymentMoney.post(new Runnable() {
            @Override
            public void run() {
                etRepaymentMoney.requestFocus();
            }
        });
    }

    private void initStartDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date now = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(now);
        int h = c.get(Calendar.HOUR_OF_DAY);
        if (h >= 17) {
            c.add(Calendar.DAY_OF_MONTH, 1); // +1 天
        }
        startDate = format.format(c.getTime());
        tvStartTime.setText(startDate);
    }

    private void initEndDate(String repayDate) {
        int repayDateInt = Integer.parseInt(repayDate);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date now = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(now);
        int today = c.get(Calendar.DAY_OF_MONTH);
        c.set(Calendar.DAY_OF_MONTH, repayDateInt);
        c.add(Calendar.DAY_OF_MONTH, -1);
        if (today >= repayDateInt) {
            c.add(Calendar.MONTH, 1);
        }
        finalEndDate = format.format(c.getTime());
        endDate = format.format(c.getTime());
        tvEndTime.setText(endDate);
    }

    private void scrollTo(View view) {
        int[] ctnLocation = new int[2];
        int[] tarLocation = new int[2];
        llContent.getLocationInWindow(ctnLocation);
        view.getLocationInWindow(tarLocation);
        int yInContent = tarLocation[1] - ctnLocation[1];
        scrollView.smoothScrollTo(0, yInContent + view.getHeight() - scrollView.getHeight());
    }

    private void initView() {
        // 暂时隐藏半自动切换按钮
        // tvSwitchType.setVisibility(View.VISIBLE); // 显示切换按钮
        switchTypeTo(REPAY_TYPE_FULL_AUTOMATION); // 默认切换到全自动
        onRepayChange();
    }

    private void loadDefaultCreditCard() {
        QueryUserBankCardResult.BankInfListBean creditFromPlanList =
                (QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("creditCard");
        if (creditFromPlanList != null) {
            creditCardItem = creditFromPlanList;
            initWithCreditCard(creditCardItem);
            return;
        }
        APIManager.startRequest(service.cardInfoGet(0), new Observer<QueryUserBankCardResult>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(QueryUserBankCardResult result) {
                if (isDestroyed()) {
                    return;
                }
                if (result.bankInfList == null || result.bankInfList.isEmpty()) {
                    askingWhetherAddCreditCard();
                    return;
                }
                creditCardItem = result.bankInfList.get(0);
                initWithCreditCard(creditCardItem);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    /**
     * 在没有添加信用卡的情况下，应该提示添加
     */
    private void askingWhetherAddCreditCard() {
        WJDialog dialog = new WJDialog(this);
        dialog.show();
        dialog.setContentText("当前没有信用卡，是否去添加？");
        dialog.setTitle("提示");
        dialog.setOnCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setOnConfirmListener(v -> {
            dialog.dismiss();
            BankAdminActivity.start(this);
        });
    }

    private boolean isAllZero(String s) { // 匹配 "0", "00", "000" 等
        return Pattern.matches("^0+$", s);
    }

    private void initWithCreditCard(QueryUserBankCardResult.BankInfListBean card) {
        if (card == null) {
            return;
        }
        ivBankCardIcon.setImageResource(BankUtils.getBankIconByName(card.bankName));
        tvCardNameAndNo.setText(String.format("%s（%s）", card.bankName, BankUtils.cutBankCardNo(card.cardNo)));
        tvBillDate.setText(String.format("账单日  %s日", card.billDate));
        tvPayDate.setText(String.format("还款日  %s日", card.repayDate));

        QueryUserBankCardResult.BankInfListBean item = card;
        if (TextUtils.isEmpty(item.repayDate) || TextUtils.isEmpty(item.billDate) ||
                isAllZero(item.repayDate) || isAllZero(item.billDate)) {
            WJDialog wjDialog = new WJDialog(this);
            wjDialog.show();
            wjDialog.setTitle("提示");
            wjDialog.setContentText("需要设置账单日/还款日,是否去设置？");
            wjDialog.setCancelable(false);
            wjDialog.setOnConfirmListener(v -> {
                wjDialog.dismiss();
                Intent intent = new Intent(this, AddEditXinYongActivity.class);
                intent.putExtra("isEdit", true);
                intent.putExtra("bankInfListBean", item);
                startActivity(intent);
            });
            wjDialog.setOnCancelListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    wjDialog.dismiss();
                    finish();
                }
            });
            return;
        } else {
            initEndDate(card.repayDate);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        loadDefaultCreditCard();
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, IntelligenceRepaymentActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, QueryUserBankCardResult.BankInfListBean creditCard) {
        Intent intent = new Intent(context, IntelligenceRepaymentActivity.class);
        intent.putExtra("creditCard", creditCard);
        context.startActivity(intent);
    }

    @OnClick({R.id.ll_use_help, R.id.ll_start_time, R.id.ll_end_time, R.id.tv_next, R.id.tvCheckPlan, R.id.tvSwitchType,
    R.id.flTipContainer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_use_help:
                UpdateDescActivity.start(this);
                hadOpenedUseHelp = true;
                break;
            case R.id.ll_start_time:
                showDatePicker(DIALOG_SELECT_START_DATE);
                break;
            case R.id.ll_end_time:
                showDatePicker(DIALOG_SELECT_END_DATE);
                break;
            case R.id.tv_next:
                onNext();
//                SelectRepayChannelActivity.start(this);
                break;
            case R.id.tvCheckPlan:
//                RepaymentListActivity.start(this, true, REQUEST_CODE_SELECT_CARD);
                finish();
                break;
            case R.id.tvSwitchType:
                switchTypeTo(++type % 2);
                break;
            case R.id.flTipContainer:
                flTipContainer.setVisibility(View.GONE);
                break;
        }
    }

    private float repayMoney;
    private float startMoney;

    private void onNext() {
        // 检查数据
        if (creditCardItem == null) {
            ToastUtil.error("请选择信用卡！");
            return;
        }

        try {
            repayMoney = Float.parseFloat(etRepaymentMoney.getText().toString());
            startMoney = Float.parseFloat(etStartMoney.getText().toString());
        } catch (Exception e) {
        }

        if (repayMoney < 200f) {
            ToastUtil.error("还款金额不能小于200");
            return;
        }
        if (startMoney <= 0f) {
            ToastUtil.error("请填写启动资金");
            return;
        }

        if (type == REPAY_TYPE_SEMI_AUTOMATION && !startMoneyIsAllow) {
            ToastUtil.error(errorMsg);
            return;
        }

        if (!hadOpenedUseHelp && type == REPAY_TYPE_SEMI_AUTOMATION) {
            UpdateDescActivity.start(this);
            hadOpenedUseHelp = true;
            return;
        }

        if (type == REPAY_TYPE_FULL_AUTOMATION) {
            if (swChangeTimeType.isChecked()) {
                if (customDate == null || customDate.isEmpty()) {
                    ToastUtil.error("请选择还款日期");
                    return;
                }
            } else {
                if (TextUtils.isEmpty(startDate) || TextUtils.isEmpty(endDate)) {
                    ToastUtil.error("请选择开始日期和结束日期");
                    return;
                }
            }
        }
        //SelectRepayChannelActivity.startForResult(this, creditCardItem, type, REQ_CODE_SELECT_CHANNEL);
        PayChannelBottomSheetDialogFragment.newInstance(creditCardItem, type).show(getSupportFragmentManager(), "PayChannelBottomSheetDialogFragment");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_SELECT_CARD:
                    handleSelectCardResult(data);
                    break;
                //case REQ_CODE_SELECT_CHANNEL:
                //    handleSelectChannelResult(data);
                //    break;
                case REQ_CODE_SELECT_DEBIT_CARD:
                    handleSelectDebitCardResult(data);
                    break;
            }
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof PayChannelBottomSheetDialogFragment) {
            dialogFragment = (PayChannelBottomSheetDialogFragment) fragment;
            dialogFragment.setOnChannelClickListener((position, channel) -> {
                channelItem = channel;
                if (type == REPAY_TYPE_SEMI_AUTOMATION) {
                    BankAdminActivity.startForSelectDebit(this, REQ_CODE_SELECT_DEBIT_CARD);
                } else if (type == REPAY_TYPE_FULL_AUTOMATION) {
                    doCreatePlan(null);
                }
            });
        }
    }

    private void handleSelectChannelResult(Intent data) {
        if (data == null) {
            return;
        }
        ChannelItem channel = (ChannelItem) data.getSerializableExtra("channel");
        channelItem = channel;
        if (type == REPAY_TYPE_SEMI_AUTOMATION) {
            BankAdminActivity.startForSelectDebit(this, REQ_CODE_SELECT_DEBIT_CARD);
        } else if (type == REPAY_TYPE_FULL_AUTOMATION) {
            doCreatePlan(null);
        }
    }

    private void handleSelectDebitCardResult(Intent data) {
        QueryUserBankCardResult.BankInfListBean debit = BankAdminActivity.getSelectResult(data);
        debitCard = debit;
        doCreatePlan(debit.cardNo);
    }

    private void doCreatePlan(String debitCardNo) {
        if (channelItem == null) {
            ToastUtil.error("请先选择通道！");
            return;
        }
        ToastUtil.showLoading(this);


        PayBackPlanCreateHelper.create(repayMoney,
                startMoney,
                startDate,
                endDate,
                creditCardItem.cardNo,
                channelItem.paymentChannelId,
                "",
                type,
                debitCardNo,
//                creditCardItem.billDate,
//                creditCardItem.repayDate,
                new PayBackPlanCreateHelper.CreateCallBack() {
                    @Override
                    public void created(PlanCreateItem ret) {
                        long startDateStamp = DateUtils.dateToStamp(startDate);
                        long endDateStamp = DateUtils.dateToStamp(finalEndDate);
                        long planEndDateStamp = DateUtils.dateToStamp(ret.endDate);//预计
                        if (startDateStamp > endDateStamp){
                            loadPreview(ret);
                        }else if (planEndDateStamp > endDateStamp) {
                            ConfirmRepaymentDialog mDialog = new ConfirmRepaymentDialog(IntelligenceRepaymentActivity.this);
                            mDialog.setOnConfirmListener(v -> loadPreview(ret));
                            mDialog.setOnBtnCancelListener(v -> dialogFragment.dismiss());
                            mDialog.show();
                        } else {
                            loadPreview(ret);
                        }
                    }
                });
    }

    private float getEtFloat(EditText editText) {
        String money = editText.getText().toString();
        try {
            return Float.valueOf(money);
        } catch (Exception e) {
            return 0f;
        }
    }

    private void onRepayChange() {
        float repaymentMoneyF = getEtFloat(etRepaymentMoney);
        float startMoneyF = getEtFloat(etStartMoney);
        if (type == REPAY_TYPE_FULL_AUTOMATION) {
            //tvTipsRepay.setText("还款金额不低于200.00元");
            tvTips.setText("请确保剩余额度大于启动资金，以避免计划失败造成逾期");
        } else if (type == REPAY_TYPE_SEMI_AUTOMATION) {
            updateSemiAutoTipsForRepayMoney(repaymentMoneyF);
            etStartMoney.setText("");
        }
    }

    private void onStartChange() {
        float repaymentMoneyF = getEtFloat(etRepaymentMoney);
        float startMoneyF = getEtFloat(etStartMoney);

        if (type == REPAY_TYPE_FULL_AUTOMATION) {
            //tvTipsRepay.setText("还款金额不低于200.00元");
            tvTips.setText("请确保剩余额度大于启动资金，以避免计划失败造成逾期");
        } else if (type == REPAY_TYPE_SEMI_AUTOMATION) {
            updateSemiAutoTipsForStartMoney(startMoneyF);
        }
    }

    private void updateSemiAutoTipsForRepayMoney(float repayMoney) {
        tvTipsRepay.setText("");
        if (etRepaymentMoney.getText().length() == 0) {
            // 输入空时不做任何提示.
            return ;
        }

            int billDate = 0;
        int repayDate = 0;
        try {
            billDate = Integer.valueOf(creditCardItem.billDate);
            repayDate = Integer.valueOf(creditCardItem.repayDate);
        } catch (Exception e) {
            ToastUtil.error("账单日或还款日不正确，请重新设置！");
            return;
        }
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        int today = c.get(Calendar.DAY_OF_MONTH);
        int h = c.get(Calendar.HOUR_OF_DAY);

        launchMoney = 0f;
        averageMoney = 0f;
        if (isNotInRepayDay(today, h, billDate, repayDate)) {
            // tvTipsRepay.setText("当前未出账单，请注意计划开始日期");
            tvTipsRepay.setText("");
        } else if (repayMoney < 200f) {
            tvTipsRepay.setText("还款金额不低于200.00元");
        } else if (!etRepaymentMoney.hasFocus()) {
            int repayDayCount; // 还款总天数
            if (billDate < repayDate) {
                repayDayCount = repayDate - today;
            } else {
                int maxDayOfMonth = c.getActualMaximum(Calendar.DAY_OF_MONTH);
                repayDayCount = (maxDayOfMonth - today) + repayDate;
            }
            if (h < 17) { // 今天未到17时，那么今天也算还款的时间
                repayDayCount += 1;
            }
            launchMoney = repayMoney * 0.08f;
            averageMoney = repayMoney / repayDayCount;
//            if (launchMoney < averageMoney) {
//                String msg = String.format("为了能在还款日前完成还款，建议启动资金不低于%.2f", Math.max(averageMoney, downLimit));
//                ToastUtils.showLongToast(msg);
//            } else if (averageMoney < launchMoney) {
//                String msg = String.format("为了您的用卡良好，建议启动资金不低于%.2f", Math.max(downLimit, launchMoney));
//                ToastUtils.showLongToast(msg);
//            }
        }
    }

    private void updateSemiAutoTipsForStartMoney(float startMoney) {
        tvTips.setText("");

        if (etStartMoney.getText().length() == 0) {
            // 输入空时不做任何提示
            return;
        }

        float large, little;
        if (averageMoney > launchMoney) {
            large = averageMoney;
            little = launchMoney;
        } else {
            large = launchMoney;
            little = averageMoney;
        }

        if (downLimit > little) {
            little = downLimit;
        }

        if (startMoney < little) {
            errorMsg = String.format("启动资金不得低于%.2f", little);
            tvTips.setText(errorMsg);
            startMoneyIsAllow = false;
        } else if (startMoney >= little && startMoney < large) {
            errorMsg = "资金过低，无法在还款日前全额还清账单，请留意。";
            tvTips.setText(errorMsg);
            startMoneyIsAllow = true;
        } else {
            startMoneyIsAllow = true;
        }
    }

    /**
     * 信用卡是否不处于还款时间段内
     *
     * @param today     day of month
     * @param hour      hour of day
     * @param billDate  账单日
     * @param repayDate 还款日
     * @return true 不是还款时间段
     */
    private boolean isNotInRepayDay(int today, int hour, int billDate, int repayDate) {
        if (billDate < repayDate) { // 账单日还款日在同一个月
            return today < billDate || (today > repayDate || (today == repayDate && hour > 17));
        } else { // 账单日还款日 不 在同一个月,即还款日是次月
            return today < billDate && (today > repayDate || (today == repayDate && hour > 17));
        }
    }

    private void loadPreview(PlanCreateItem planCreateItem) {
        PlanDetail planDetail = PayBackPlanCreateHelper.PlanCreateItem2PlanDetail(planCreateItem, startMoney, type);
        RepaymentPlanActivity.startForPreview(IntelligenceRepaymentActivity.this, planCreateItem, planDetail, creditCardItem,debitCard);
        finish();
    }

    private void handleSelectCardResult(Intent data) {
        if (data == null) {
            return;
        }
        creditCardItem = (QueryUserBankCardResult.BankInfListBean)
                data.getSerializableExtra("selectedCard");
        initWithCreditCard(creditCardItem);
    }

    private void showDatePicker(int reqCode) {
        String selectedDate = null;
        if (reqCode == DIALOG_SELECT_START_DATE) {
            selectedDate = startDate;
        } else if (reqCode == DIALOG_SELECT_END_DATE) {
            selectedDate = endDate;
        }
        SelectDateDialogFragment.newInstanceWithSingle(selectedDate)
                .setOnSelectListener(new SelectDateDialogFragment.OnSelectListener() {
                    @Override
                    public void onSelect(Set<String> ret) {
                        String[] retArr = ret.toArray(new String[]{});
                        if (retArr == null || retArr.length == 0) {
                            return;
                        }
                        if (reqCode == DIALOG_SELECT_START_DATE) {
                            startDate = retArr[0];
                            tvStartTime.setText(startDate);
                        } else if (reqCode == DIALOG_SELECT_END_DATE) {
                            endDate = retArr[0];
                            tvEndTime.setText(endDate);
                        }
                    }
                })
                .show(getSupportFragmentManager());
    }

    @OnClick(R.id.ll_date_type_custom)
    public void onSelectCustomDate() {
        SelectDateDialogFragment.newInstanceWithMultiple(customDate)
                .setOnSelectListener(new SelectDateDialogFragment.OnSelectListener() {
                    @Override
                    public void onSelect(Set<String> ret) {
                        customDate = ret;
                        if (!ret.isEmpty()) {
                            StringBuilder showCustomDate = new StringBuilder();
                            for (String date :
                                    ret) {
                                showCustomDate.append(date);
                                showCustomDate.append(",");
                            }
                            tvCustomDate.setText(showCustomDate.substring(0, showCustomDate.length() - 1));
                        }
                    }
                })
                .show(getSupportFragmentManager());
    }

    /**
     * 切换全自动和手动模式
     *
     * @param type #REPAY_TYPE_SEMI_AUTOMATION, #REPAY_TYPE_FULL_AUTOMATION
     */
    private void switchTypeTo(int type) {
        this.type = type;
        flTipContainer.setVisibility(View.GONE);
        if (type == REPAY_TYPE_SEMI_AUTOMATION) {
            llTimeContainer.setVisibility(View.GONE);
            tvSwitchType.setText("切换为全自动");
            ll_use_help.setVisibility(View.VISIBLE);

            int color = Color.parseColor("#E1B15A");
            if (!isSwitchMsgShown) {
                flTipContainer.setVisibility(View.VISIBLE);
                SpannableStringBuilder ssb1 = SpannableStringUtils.getBuilder("注意：")
                        .setForegroundColor(color)
                        .create();
                SpannableStringBuilder ssb2 = SpannableStringUtils.getBuilder("手动还款！")
                        .setForegroundColor(color)
                        .create();
                SpannableStringBuilder ssb3 = SpannableStringUtils.getBuilder("点击切换")
                        .setForegroundColor(color)
                        .setClickSpan(new ClickableSpan() {
                            @Override
                            public void onClick(@NonNull View widget) {
                                flTipContainer.setVisibility(View.GONE);
                                onViewClicked(tvSwitchType);
                            }

                            @Override
                            public void updateDrawState(@NonNull TextPaint ds) {
                                super.updateDrawState(ds);
                                ds.setColor(color);
                                ds.setUnderlineText(false);
                            }
                        })
                        .create();
                SpannableStringBuilder ssb = new SpannableStringBuilder();
                ssb.append(ssb1).append("本次计划消费到账储蓄卡，需自行").append(ssb2).append("若需自动还入，请").append(ssb3);
                tvTipText.setMovementMethod(LinkMovementMethod.getInstance());
                tvTipText.setText(ssb);
                isSwitchMsgShown = true;
            } else {
                flTipContainer.setVisibility(View.GONE);
            }
        } else if (type == REPAY_TYPE_FULL_AUTOMATION) {
            llTimeContainer.setVisibility(View.VISIBLE);
            tvSwitchType.setText("切换为半自动");
            ll_use_help.setVisibility(View.INVISIBLE);
        } else {
            throw new IllegalStateException("type was error: " + type);
        }
        etRepaymentMoney.setText("");
        etStartMoney.setText("");
    }
}
