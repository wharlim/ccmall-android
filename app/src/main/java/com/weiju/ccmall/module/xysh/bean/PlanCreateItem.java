package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PlanCreateItem implements Serializable {

    /**
     * cardNo : 6253624079454375
     * repayAmount : 203.36
     * payAmount : 200
     * planNo : ZXHK20200105165652056483
     * payBackTimesPerday : 1
     * startDate : 2020-01-17
     * endDate : 2020-01-17
     * minAvailableAmount : 210
     * listTranRecords : [{"excuteTime":1579190400000,"listTxnJnls":[{"tranId":"cc20200105165652056484","tmSmp":null,"corgTranId":null,"outTranId":"cc731319337431863296","usrNo":"dac68adea0f44385ae2b84e03609a285","cnlNo":"GftP3","txnMercNm":"还款","tranType":"1","tranSts":"0","planNo":"ZXHK20200105165652056483","txnAmt":200,"txnAmtList":[200],"paybackAmt":196.64,"txnRate":0.68,"feeAmt":3.36,"createTm":1579190400000,"planExcuteTime":1579247231581,"excuteTime":"2020-01-17 15:47:11","repayTime":"2020-01-17 16:23:11","txnTm":1578214612000,"rspTm":null,"txnCnt":0,"txnCardNo":"6253624079454375","clrCardNo":"6253624079454375","retCode":null,"retMsg":"","stlType":"0","rebSts":null,"capAmt":null,"corgFeeAmt":2.2,"rebCnt":0,"rebMsg":null,"isMain":null,"orderDetailCount":null,"feeFormat":"3.36元","planType":0}]}]
     */

    @SerializedName("cardNo")
    public String cardNo;
    @SerializedName("repayAmount")
    public float repayAmount;
    @SerializedName("payAmount")
    public float payAmount;
    @SerializedName("pounDage")
    public float pounDage;
    @SerializedName("planNo")
    public String planNo;
    @SerializedName("payBackTimesPerday")
    public int payBackTimesPerday;
    @SerializedName("startDate")
    public String startDate;
    @SerializedName("endDate")
    public String endDate;
    @SerializedName("minAvailableAmount")
    public float minAvailableAmount;
    @SerializedName("listTranRecords")
    public List<ListTranRecordsBean> listTranRecords;

    public static class ListTranRecordsBean implements Serializable {
        /**
         * excuteTime : 1579190400000
         * listTxnJnls : [{"tranId":"cc20200105165652056484","tmSmp":null,"corgTranId":null,"outTranId":"cc731319337431863296","usrNo":"dac68adea0f44385ae2b84e03609a285","cnlNo":"GftP3","txnMercNm":"还款","tranType":"1","tranSts":"0","planNo":"ZXHK20200105165652056483","txnAmt":200,"txnAmtList":[200],"paybackAmt":196.64,"txnRate":0.68,"feeAmt":3.36,"createTm":1579190400000,"planExcuteTime":1579247231581,"excuteTime":"2020-01-17 15:47:11","repayTime":"2020-01-17 16:23:11","txnTm":1578214612000,"rspTm":null,"txnCnt":0,"txnCardNo":"6253624079454375","clrCardNo":"6253624079454375","retCode":null,"retMsg":"","stlType":"0","rebSts":null,"capAmt":null,"corgFeeAmt":2.2,"rebCnt":0,"rebMsg":null,"isMain":null,"orderDetailCount":null,"feeFormat":"3.36元","planType":0}]
         */

        @SerializedName("excuteTime")
        public long excuteTime;
        @SerializedName("listTxnJnls")
        public List<ListTxnJnlsBean> listTxnJnls;

        public static class ListTxnJnlsBean implements Serializable {
            /**
             * tranId : cc20200105165652056484
             * tmSmp : null
             * corgTranId : null
             * outTranId : cc731319337431863296
             * usrNo : dac68adea0f44385ae2b84e03609a285
             * cnlNo : GftP3
             * txnMercNm : 还款
             * tranType : 1
             * tranSts : 0
             * planNo : ZXHK20200105165652056483
             * txnAmt : 200
             * txnAmtList : [200]
             * paybackAmt : 196.64
             * txnRate : 0.68
             * feeAmt : 3.36
             * createTm : 1579190400000
             * planExcuteTime : 1579247231581
             * excuteTime : 2020-01-17 15:47:11
             * repayTime : 2020-01-17 16:23:11
             * txnTm : 1578214612000
             * rspTm : null
             * txnCnt : 0
             * txnCardNo : 6253624079454375
             * clrCardNo : 6253624079454375
             * retCode : null
             * retMsg :
             * stlType : 0
             * rebSts : null
             * capAmt : null
             * corgFeeAmt : 2.2
             * rebCnt : 0
             * rebMsg : null
             * isMain : null
             * orderDetailCount : null
             * feeFormat : 3.36元
             * planType : 0
             */

            @SerializedName("tranId")
            public String tranId;
            @SerializedName("tmSmp")
            public Object tmSmp;
            @SerializedName("corgTranId")
            public Object corgTranId;
            @SerializedName("outTranId")
            public String outTranId;
            @SerializedName("usrNo")
            public String usrNo;
            @SerializedName("cnlNo")
            public String cnlNo;
            @SerializedName("txnMercNm")
            public String txnMercNm;
            @SerializedName("tranType")
            public String tranType;
            @SerializedName("tranSts")
            public String tranSts;
            @SerializedName("planNo")
            public String planNo;
            @SerializedName("txnAmt")
            public float txnAmt;
            @SerializedName("paybackAmt")
            public float paybackAmt;
            @SerializedName("txnRate")
            public double txnRate;
            @SerializedName("feeAmt")
            public double feeAmt;
            @SerializedName("createTm")
            public long createTm;
            @SerializedName("planExcuteTime")
            public long planExcuteTime;
            @SerializedName("excuteTime")
            public String excuteTime;
            @SerializedName("repayTime")
            public String repayTime;
            @SerializedName("txnTm")
            public long txnTm;
            @SerializedName("rspTm")
            public Object rspTm;
            @SerializedName("txnCnt")
            public int txnCnt;
            @SerializedName("txnCardNo")
            public String txnCardNo;
            @SerializedName("clrCardNo")
            public String clrCardNo;
            @SerializedName("retCode")
            public Object retCode;
            @SerializedName("retMsg")
            public String retMsg;
            @SerializedName("stlType")
            public String stlType;
            @SerializedName("rebSts")
            public Object rebSts;
            @SerializedName("capAmt")
            public Object capAmt;
            @SerializedName("corgFeeAmt")
            public double corgFeeAmt;
            @SerializedName("rebCnt")
            public int rebCnt;
            @SerializedName("rebMsg")
            public Object rebMsg;
            @SerializedName("isMain")
            public Object isMain;
            @SerializedName("orderDetailCount")
            public Object orderDetailCount;
            @SerializedName("feeFormat")
            public String feeFormat;
            @SerializedName("planType")
            public int planType;
            @SerializedName("txnAmtList")
            public List<Float> txnAmtList;
        }
    }
}
