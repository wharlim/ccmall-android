package com.weiju.ccmall.module.product;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.adapter.MyCommentAdapter;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.MyCommentModel;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.SessionUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

public class MyCommentListFragment extends BaseListFragment {

    private ArrayList<MyCommentModel> mDatas = new ArrayList();
    private MyCommentAdapter mAdapter = new MyCommentAdapter(mDatas);

    //    private MyCommentAdapter mAdapter = new MyCommentAdapter(mDatas);
    private int mType;
    private int mPage;
    private IOrderService mService;
    private User mLoginUser;
    private String classifacationType = "";


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    /**
     * @param type 待评价：0    已评价：1
     * @return
     */
    public static MyCommentListFragment newInstance(int type, String classifacationType) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        args.putString(Const.CLASSIFICATION_TYPE, classifacationType);
        MyCommentListFragment fragment = new MyCommentListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * @param type 待评价：0    已评价：1
     * @return
     */
    public static MyCommentListFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        MyCommentListFragment fragment = new MyCommentListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mType = getArguments().getInt("type");
        classifacationType = getArguments().getString(Const.CLASSIFICATION_TYPE);
    }

    @Override
    public void initData() {
        mService = ServiceManager.getInstance().createService(IOrderService.class);
        mLoginUser = SessionUtil.getInstance().getLoginUser();
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.bg_gray));
        super.initData();
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        MyCommentModel myCommentModel = mDatas.get(position);
        switch (view.getId()) {
            case R.id.tvComment:
                comment(myCommentModel);
                break;
            case R.id.tvReadComment:
                readComment(myCommentModel);
                break;
            case R.id.tvAddComment:
                addComment(myCommentModel);
                break;
            default:
                goProductActivity(myCommentModel);
                break;
        }
    }

    private void goProductActivity(MyCommentModel myCommentModel) {
        Intent intent = new Intent(getContext(), NewProductDetailActivity.class);
        intent.putExtra(Key.SKU_ID, myCommentModel.skuId);
        startActivity(intent);
    }

    /**
     * 追加评价
     *
     * @param myCommentModel
     */
    private void addComment(MyCommentModel myCommentModel) {
        Intent intent = new Intent(getContext(), OrderAddCommentActivity.class);
        intent.putExtra("orderCode", myCommentModel.orderCode);
        intent.putExtra("orderId", myCommentModel.order1Id);
        intent.putExtra("skuId", myCommentModel.skuId);
        getContext().startActivity(intent);
    }

    /**
     * 查看评价
     *
     * @param myCommentModel
     */
    private void readComment(MyCommentModel myCommentModel) {
        if(!TextUtils.isEmpty(classifacationType)){
            Intent intent = new Intent(getContext(), ProductCommentListActivity.class);
            intent.putExtra("id", myCommentModel.productId);
            intent.putExtra("order1Id", myCommentModel.order1Id);
            intent.putExtra("img", myCommentModel.productImage);
            intent.putExtra("name", myCommentModel.skuName);
            intent.putExtra("isDetail", true);
            intent.putExtra("deleteComment","deleteComment");
            startActivity(intent);
        }else{
            Intent intent = new Intent(getContext(), ProductCommentListActivity.class);
            intent.putExtra("id", myCommentModel.productId);
            intent.putExtra("order1Id", myCommentModel.order1Id);
            intent.putExtra("img", myCommentModel.productImage);
            intent.putExtra("name", myCommentModel.skuName);
            intent.putExtra("isDetail", true);
            startActivity(intent);
        }

    }

    /**
     * 跳到评价页面
     *
     * @param myCommentModel
     */
    private void comment(MyCommentModel myCommentModel) {
        Intent intent = new Intent(getContext(), OrderCommentActivity.class);
        intent.putExtra("orderCode", myCommentModel.orderCode);
        intent.putExtra("orderId", myCommentModel.order1Id);
        intent.putExtra("skuId", myCommentModel.skuId);
        getContext().startActivity(intent);
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(final boolean isRefresh) {
        if (isRefresh) {
            mPage = 1;
        } else {
            mPage++;
        }
        if (Const.NEWRETAIL_MODE.equals(classifacationType)) {
//            mAdapter = new MyCommentAdapter(mDatas, true);
            APIManager.startRequest(
                    mService.getOrderCommentList(
                            mType == 0 ? "order/getOrderWaitCommentList" : "order/getOrderCommentList",
                            mPage,
                            20,
                            mLoginUser.id, "onnOrder"),
                    new BaseRequestListener<PaginationEntity<MyCommentModel, Object>>(mRefreshLayout) {
                        @Override
                        public void onSuccess(PaginationEntity<MyCommentModel, Object> result) {
                            super.onSuccess(result);
                            if (mPage == 1) {
                                mDatas.clear();
                            }
                            mDatas.addAll(result.list);
                            mAdapter.notifyDataSetChanged();
                            if (result.page >= result.totalPage) {
                                mAdapter.loadMoreEnd(isRefresh && mDatas.size() < 5);
                            } else {
                                mAdapter.loadMoreComplete();
                            }
                        }
                    }, getContext()
            );
        } else {
            APIManager.startRequest(
                    mService.getOrderCommentList(
                            mType == 0 ? "order/getOrderWaitCommentList" : "order/getOrderCommentList",
                            mPage,
                            20,
                            mLoginUser.id),
                    new BaseRequestListener<PaginationEntity<MyCommentModel, Object>>(mRefreshLayout) {
                        @Override
                        public void onSuccess(PaginationEntity<MyCommentModel, Object> result) {
                            super.onSuccess(result);
                            if (mPage == 1) {
                                mDatas.clear();
                            }
                            mDatas.addAll(result.list);
                            mAdapter.notifyDataSetChanged();
                            if (result.page >= result.totalPage) {
                                mAdapter.loadMoreEnd(isRefresh && mDatas.size() < 5);
                            } else {
                                mAdapter.loadMoreComplete();
                            }
                        }
                    }, getContext()
            );


        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
        if (message.getEvent().equals(Event.commentFinish)) {
            getData(true);
        }
    }

}
