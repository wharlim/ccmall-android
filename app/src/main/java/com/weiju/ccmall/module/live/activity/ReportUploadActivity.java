package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveReportImageUploadItem;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.manager.UploadManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.ImageUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.zhihu.matisse.Matisse;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

public class ReportUploadActivity extends BaseActivity {

    @BindView(R.id.etReason)
    EditText etReason;
    @BindView(R.id.tvUploadImgTip)
    TextView tvUploadImgTip;
    @BindView(R.id.llImgs)
    LinearLayout llImgs;
    @BindView(R.id.tvCommit)
    TextView tvCommit;

    private String liveId;
    ArrayList<String> titleIds;

    FrameLayout[] imgs;
    List<File> selectedImgRes = new ArrayList<>();
    private int selectSession;
    private static final int REQ_SELECT_IMG = 101;
    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);

    public static void start(Context context, String liveId, ArrayList<String> titleIds) {
        Intent intent = new Intent(context, ReportUploadActivity.class);
        intent.putStringArrayListExtra("titleIds", titleIds);
        intent.putExtra("liveId", liveId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_upload);
        setTitle("举报");
        setLeftBlack();
        ButterKnife.bind(this);
        titleIds = getIntent().getStringArrayListExtra("titleIds");
        liveId = getIntent().getStringExtra("liveId");
        initImgList();
        updateImgList();
    }

    private void initImgList() {
        imgs = new FrameLayout[llImgs.getChildCount()];
        for (int i = 0; i < imgs.length; i++) {
            imgs[i] = (FrameLayout) llImgs.getChildAt(i);
            final int index = i;
            imgs[i].getChildAt(0).setOnClickListener(v -> {
                if (selectedImgRes.size() > index) {
                    // 已选,查看
                    ArrayList<String> paths = new ArrayList<>();
                    for (File f :
                            selectedImgRes) {
                        paths.add(f.getAbsolutePath());
                    }
                    ImageUtil.previewImage(this, paths, index, false);
                } else {
                    // 未选，去选择
                    this.selectSession = index;
                    UploadManager.selectImage(this, REQ_SELECT_IMG, imgs.length - selectedImgRes.size());
                }
            });
            imgs[i].getChildAt(1).setOnClickListener(v -> {
                // 删除
                selectedImgRes.remove(index);
                updateImgList();
            });
        }
    }

    private void updateImgList() {
        for (int i = 0; i < selectedImgRes.size(); i++) {
            imgs[i].setVisibility(View.VISIBLE);
            ImageView img = (ImageView) imgs[i].getChildAt(0);
            // 设置img
            Glide.with(this).load(selectedImgRes.get(i))
                    .apply(RequestOptions.bitmapTransform(new MultiTransformation(
                            new CenterCrop(),
                            new RoundedCornersTransformation(SizeUtils.dp2px(5), 0, RoundedCornersTransformation.CornerType.ALL))))
                    .into(img);
            imgs[i].getChildAt(1).setVisibility(View.VISIBLE);
        }

        for (int i = selectedImgRes.size(); i < imgs.length; i++) {
            if (i == selectedImgRes.size()) {
                imgs[i].setVisibility(View.VISIBLE);
                ImageView img = (ImageView) imgs[i].getChildAt(0);
                img.setImageResource(R.drawable.icon_report_img_add);
                imgs[i].getChildAt(1).setVisibility(View.INVISIBLE);
            } else {
                imgs[i].setVisibility(View.INVISIBLE);
            }
        }
    }

    @OnClick(R.id.tvCommit)
    public void onCommit() {
        if (selectedImgRes.isEmpty()) {
            tvUploadImgTip.setVisibility(View.VISIBLE);
            return;
        }
        uploadImgs();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case REQ_SELECT_IMG:
                    handleSelectImgResult(data);
                    break;
            }
        }
    }

    private void handleSelectImgResult(Intent data) {
        List<String> paths = Matisse.obtainPathResult(data);
        for (String path :
                paths) {
            Luban.with(getApplicationContext())
                    .load(new File(path))
                    .setCompressListener(new OnCompressListener() {
                        @Override
                        public void onStart() {

                        }

                        @Override
                        public void onSuccess(File file) {
                            Log.d("Seven", "select compress file -> " +
                                    file.getAbsolutePath() + " size -> " + (file.length() / 1024) + "Kb");
                            selectedImgRes.add(file);
                            updateImgList();
                        }

                        @Override
                        public void onError(Throwable e) {

                        }
                    })
                    .launch();
        }
    }

    private void uploadImgs() {
        MultipartBody.Builder builder = new MultipartBody.Builder()
                .setType(MultipartBody.FORM);
        for (File f :
                selectedImgRes) {
            builder.addFormDataPart("file", f.getName(), RequestBody.create(MediaType.parse("image/*"), f));
        }
        RequestBody body = builder.build();
        ToastUtil.showLoading(this);
        APIManager.startRequest(mService.uploadImages(body), new BaseRequestListener<List<LiveReportImageUploadItem>>() {
            @Override
            public void onSuccess(List<LiveReportImageUploadItem> result) {
                ArrayList<String> imgUrls = new ArrayList<>();
                for (LiveReportImageUploadItem item:
                     result) {
                    imgUrls.add(item.imgUrl);
                }
                commit(liveId, titleIds, imgUrls, etReason.getText().toString());
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.error("上传文件失败！");
                ToastUtil.hideLoading();
            }
        }, this);
    }

    private void commit(String liveId, List<String> reportTitleListIds, List<String> reportImageUrls,
                        String reportReasonDesc) {
        HashMap<String, Object> params = new HashMap<>();
        params.put("liveId", liveId);
        params.put("reportTitleListIds", reportTitleListIds);
        params.put("reportImageUrls", reportImageUrls);
        params.put("reportReasonDesc", reportReasonDesc);
        RequestBody body = RequestBody.create(MediaType.parse("application/json;charset=UTF-8"),
                new Gson().toJson(params));
        APIManager.startRequest(mService.addReport(body), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("提交成功");
                ToastUtil.hideLoading();
                // 关闭前一个页面
                EventBus.getDefault().post(new EventMessage(Event.liveReportSuccess));
                // 关闭当前页面
                finish();
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.error("提交失败");
                ToastUtil.hideLoading();
            }
        }, this);
    }
}
