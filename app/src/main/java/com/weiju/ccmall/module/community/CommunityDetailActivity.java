package com.weiju.ccmall.module.community;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.utils.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.LoginActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.CommentBottomSheetDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * @author Stone
 * @time 2018/1/5  14:10
 * @desc ${TODD}
 */

public class CommunityDetailActivity extends BasicActivity {
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private CommunityAdapter mAdapter;
    private CommunityMultiItem mHeadData;
    private int pageOffset = 1;
    ArrayList<CommunityMultiItem> dates = new ArrayList<>();
    private int clickPosition;
    private boolean mIsShareCircle;
    private ICommunityService mPageService;


    @Override
    protected int getLayoutResId() {
        return R.layout.activity_community_detail;
    }

    @Override
    protected void initViewConfig() {
        super.initViewConfig();
        ((TextView) findViewById(R.id.titleTv)).setText(getString(R.string.s_detail_title));
        mHeadData = (CommunityMultiItem) getIntent().getSerializableExtra(Constants.Extras.KEY_EXTRAS);
        mIsShareCircle = getIntent().getBooleanExtra(Constants.KEY_TYPE, false);
        mHeadData.getContent().setComments(null);
        mHeadData.needBottomLine(false);
        dates.add(mHeadData);
        mAdapter = new CommunityAdapter(dates);
        mAdapter.disableLoadMoreIfNotFullPage(mRecyclerView);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.getItemAnimator().setChangeDuration(0);
    }

    @Override
    protected void initDataNew() {
        super.initDataNew();
        mPageService = ServiceManager.getInstance().createService(ICommunityService.class);
        requestCommentData();
    }

    private void requestCommentData() {
        APIManager.startRequest(mPageService.getGroupTopicCommentList(pageOffset, 15,
                mHeadData.getContent().getTopicId()), new BaseRequestListener<PaginationEntity<MaterialVideoModule.CommentModule, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<MaterialVideoModule.CommentModule, Object> result) {
                super.onSuccess(result);
                if (pageOffset == 1 && pageOffset >= result.totalPage) {
                    mAdapter.loadMoreComplete();
                    mAdapter.setEnableLoadMore(false);
                } else if (pageOffset >= result.totalPage) {
                    mAdapter.loadMoreEnd();
                } else {
                    mAdapter.loadMoreComplete();
                }
                getCommonItems(result.list);
            }

            @Override
            public void onStart() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        },this);


    }

    private void getCommonItems(ArrayList<MaterialVideoModule.CommentModule> data) {
        ArrayList<CommunityMultiItem> dates = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            CommunityMultiItem item = new CommunityMultiItem(CommunityMultiItem.ITEM_TYPE_COMMENT,
                    null, GroupFragment.CommunityType.TYPE_GROUP);
            item.setCommontData(data.get(i));
            item.setLastCommond(i == data.size() - 1);
            dates.add(item);
        }
        mAdapter.addData(dates);
    }

    @Override
    protected void initListener() {
        super.initListener();
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                pageOffset++;
                requestCommentData();
            }
        }, mRecyclerView);
        findViewById(R.id.back_iv).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                onCommonItemClick(view, i);
            }
        });
    }

    private void onCommonItemClick(View view, int i) {
        switch (view.getId()) {
            case R.id.save_tv:
                if (mHeadData.getItemType() == CommunityMultiItem.ITEM_TYPE_LINK) {
                    forwardLinkEvent(mHeadData.getContent());
                } else {
                    saveImgEvent(mHeadData.getContent());
                }
                break;
            case R.id.forward_tv:
                if (!SessionUtil.getInstance().isLogin()) {
                    gotoLogin();
                    break;
                }
                if (mHeadData.getItemType() == CommunityMultiItem.ITEM_TYPE_LINK) {
                    forwardLinkEvent(mHeadData.getContent());
                } else if (mHeadData.getItemType() == CommunityMultiItem.ITEM_TYPE_TEXT) {
                    forWardImgEvent(mHeadData.getContent());
                }
                break;
            case R.id.like_layout:
                if (!SessionUtil.getInstance().isLogin()) {
                    gotoLogin();
                    break;
                }
                clickPosition = i;
                int likeStatus = mHeadData.getContent().getLikeStatus();
                if (likeStatus == 0) {
                    requestAddLike(mHeadData.getContent().getTopicId(), mHeadData);
                } else {
                    requestRemoveLike(mHeadData.getContent().getTopicId(), mHeadData);
                }
                break;
            case R.id.lease_msg_tv:
                if (!SessionUtil.getInstance().isLogin()) {
                    gotoLogin();
                    break;
                }
                final CommentBottomSheetDialog dialog = new CommentBottomSheetDialog(CommunityDetailActivity.this);
                dialog.setSubmitListener(new CommentBottomSheetDialog.OnSubmitListener() {
                    @Override
                    public void submit(String content) {
                        dialog.dismiss();
                        requestAddComment(content, mHeadData.getContent().getTopicId());
                    }
                });
                dialog.show("发送", "");
                break;
            case R.id.item_link_layout:
                EventUtil.compileEvent(mActivity, "link", mHeadData.getContent().getLinkUrl(), false);
                break;
            default:
                break;
        }
    }

    private void gotoLogin() {
        Intent intent = new Intent(mActivity, LoginActivity.class);
        startActivity(intent);
    }

    private void requestAddComment(String content, String topicId) {

        APIManager.startRequest(mPageService.addGroupTopicComment(topicId, content), new BaseRequestListener<MaterialVideoModule.CommentModule>() {
            @Override
            public void onStart() {
                showLoading();
            }

            @Override
            public void onSuccess(MaterialVideoModule.CommentModule result) {
                hideLoading();
                ToastUtils.showShortToast("操作成功");
                CommunityMultiItem multiItem = new CommunityMultiItem(CommunityMultiItem.ITEM_TYPE_COMMENT,
                        null, GroupFragment.CommunityType.TYPE_GROUP);
                multiItem.setCommontData(result);
                mAdapter.addData(multiItem);
                EventBus.getDefault().post(new EventMessage(Event.addCommond, result));
                String commentCount = mHeadData.getContent().getCommentCount();
                int newCommentCount = Integer.parseInt(commentCount);
                newCommentCount += 1;
                mHeadData.getContent().setCommentCount(String.valueOf(newCommentCount));
                mAdapter.notifyItemChanged(0);
            }

            @Override
            public void onError(Throwable e) {
                hideLoading();
                showError(e);
            }

            @Override
            public void onComplete() {
                hideLoading();
            }
        },this);
    }

    /**
     * 保村转发链接事件
     *
     * @param module
     */
    private void forwardLinkEvent(MaterialVideoModule module) {
        ShareDialogNew shareDialog = new ShareDialogNew(mIsShareCircle, mActivity, module.getImages(), module.getContent());
        shareDialog.setLinkType(module);
        shareDialog.show();
    }

    /**
     * 保存图片类型
     *
     * @param module
     */
    private void saveImgEvent(final MaterialVideoModule module) {
        requestPermission(new PermissionListener() {
            @Override
            public void onSuccess() {
                ToastUtils.showShortToast(getString(R.string.s_saving_text));
                ImgDownLoadUtils.savePic2Local(module.getImages(), getApplicationContext());
            }
        });
    }

    /**
     * 转发图图片类型
     *
     * @param module
     */
    private void forWardImgEvent(MaterialVideoModule module) {
        final ShareDialogNew shareDialog = new ShareDialogNew(mIsShareCircle, mActivity, module.getImages(), module.getContent());
        shareDialog.setPicType(module);
        requestPermission(new PermissionListener() {
            @Override
            public void onSuccess() {
                shareDialog.show();
            }
        });
    }

    private void requestAddLike(String topicId, final CommunityMultiItem item) {
        APIManager.startRequest(mPageService.addGroupTopicLike(topicId), new BaseRequestListener<Like>() {
            @Override
            public void onStart() {
                showLoading();
            }

            @Override
            public void onSuccess(Like result) {
                EventBus.getDefault().post(new EventMessage(Event.addSupport));
                hideLoading();
                showToast("操作成功");
                int likeCount = item.getContent().getLikeCount();
                likeCount += 1;
                item.getContent().setLikeId(result.likeId);
                item.getContent().setLikeCount(likeCount);
                item.getContent().setLikeStatus(1);
                mAdapter.notifyItemChanged(clickPosition);
            }

            @Override
            public void onError(Throwable e) {
                hideLoading();
                showError(e);
            }

            @Override
            public void onComplete() {
                hideLoading();
            }
        },this);
    }

    private void requestRemoveLike(String topicId, final CommunityMultiItem item) {
        APIManager.startRequest(mPageService.cancelGroupTopicLike(topicId, item.getContent().getLikeId()), new BaseRequestListener<Object>() {
            @Override
            public void onStart() {
                showLoading();
            }

            @Override
            public void onSuccess(Object result) {
                hideLoading();
                showToast("操作成功");
                int likeCount = item.getContent().getLikeCount();
                likeCount -= 1;
                item.getContent().setLikeCount(likeCount);
                item.getContent().setLikeStatus(0);
                mAdapter.notifyItemChanged(clickPosition);
                EventBus.getDefault().post(new EventMessage(Event.cancelSupport));
            }

            @Override
            public void onError(Throwable e) {
                hideLoading();
                showError(e);
            }

            @Override
            public void onComplete() {
                hideLoading();
            }
        },this);
    }
}
