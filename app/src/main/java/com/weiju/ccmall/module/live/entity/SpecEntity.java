package com.weiju.ccmall.module.live.entity;

import com.weiju.ccmall.shared.util.MoneyUtil;

import java.io.Serializable;

/**
 * @author Ben
 * @date 2020/4/15.
 */
public class SpecEntity implements Serializable {
    private String title;
    private Long retailPrice;//售价
    private Long marketPrice;//市场价
    private String stock;//库存(件）
    private String weight;//重量g
    private String skuId;
    private int commissionRatio;

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(String retailPrice) {
        this.retailPrice = MoneyUtil.yuanToCentLong(retailPrice);
    }

    public Long getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        this.marketPrice = MoneyUtil.yuanToCentLong(marketPrice);
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "SpecEntity{" +
                "title='" + title + '\'' +
                ", retailPrice=" + retailPrice +
                ", marketPrice=" + marketPrice +
                ", stock=" + stock +
                ", weight=" + weight +
                ", commissionRatio=" + commissionRatio +
                '}';
    }
}
