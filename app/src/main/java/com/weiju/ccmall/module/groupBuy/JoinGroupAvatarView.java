package com.weiju.ccmall.module.groupBuy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.util.FrescoUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/11/1.
 */
public class JoinGroupAvatarView extends RelativeLayout {

    @BindView(R.id.ivAvatar)
    SimpleDraweeView mIvAvatar;
    @BindView(R.id.ivTag)
    ImageView mIvTag;
    @BindView(R.id.ivAvatarNoData)
    ImageView mIvAvatarNoData;

    @SuppressLint("ResourceAsColor")
    public JoinGroupAvatarView(@NonNull Context context) {
        super(context);
        inflate(context, R.layout.view_join_group_avatar, this);
        ButterKnife.bind(this);
    }

    public void setData(GroupBuyModel.JoinMemberEntity data) {
        if (data == null) {
            mIvTag.setVisibility(GONE);
            mIvAvatar.setVisibility(GONE);
            mIvAvatarNoData.setVisibility(VISIBLE);
        } else {
            FrescoUtil.setImageSmall(mIvAvatar, data.headImage);
            mIvTag.setVisibility(data.role == 1 ? VISIBLE : GONE);
        }
    }

    public void showMore(){
        mIvTag.setVisibility(GONE);
        mIvAvatar.setVisibility(GONE);
        mIvAvatarNoData.setImageResource(R.drawable.joint_more);
        mIvAvatarNoData.setVisibility(VISIBLE);
    }

}
