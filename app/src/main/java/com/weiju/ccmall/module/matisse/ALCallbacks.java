package com.weiju.ccmall.module.matisse;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import com.zhihu.matisse.ui.MatisseActivity;

import java.lang.reflect.Field;

/**
 * Created by 林先生_lcy.
 * User: lcy
 * Date: 2019/4/2
 * Time: 下午5:48
 */
public class ALCallbacks implements Application.ActivityLifecycleCallbacks {

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if(activity instanceof MatisseActivity){
            Class<? extends Activity> aClass = MatisseActivity.class;
            try {
                Field field = aClass.getDeclaredField("mSelectedCollection");
                field.setAccessible(true);
                field.set(activity, new ISelectedItemCollection(activity));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

}
