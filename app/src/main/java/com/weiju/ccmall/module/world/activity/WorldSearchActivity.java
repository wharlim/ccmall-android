package com.weiju.ccmall.module.world.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.world.IWorldService;
import com.weiju.ccmall.module.world.adapter.WorldSearchAdapter;
import com.weiju.ccmall.module.world.entity.CatgoryItemEntity;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class WorldSearchActivity extends BaseListActivity {

    @BindView(R.id.barPadding)
    View mBarPadding;
    @BindView(R.id.keywordEt)
    protected EditText mKeywordEt;

    private String mKeyword = "";
    private List<CatgoryItemEntity> mData = new ArrayList<>();
    private WorldSearchAdapter mAdapter = new WorldSearchAdapter(mData);
    private IWorldService mService = ServiceManager.getInstance().createService(IWorldService.class);

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_world_search;
    }

    @Override
    public void initView() {
        super.initView();
        getHeaderLayout().setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            QMUIStatusBarHelper.translucent(this);
            QMUIStatusBarHelper.setStatusBarLightMode(this);
        }
        //导航栏高度
        int height = QMUIStatusBarHelper.getStatusbarHeight(this);
        mBarPadding.getLayoutParams().height = height;

        mKeywordEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH || (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    search(textView.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });

        mKeywordEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    mInputMethodManager.showSoftInput(mKeywordEt, InputMethodManager.SHOW_IMPLICIT);
                } else {
                    mInputMethodManager.hideSoftInputFromWindow(mKeywordEt.getWindowToken(), 0);
                }
            }
        });
    }

    @Override
    public String getTitleStr() {
        return null;
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        CatgoryItemEntity itemEntity = mAdapter.getData().get(position);
        WorldProductDetailActivity.start(this, itemEntity.itemId);
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        if (TextUtils.isEmpty(mKeyword)) {
            mRefreshLayout.setRefreshing(false);
            return;
        }
        APIManager.startRequest(mService.searchGoods(mKeyword, mCurrentPage, 20),
                new BaseRequestListener<PaginationEntity<CatgoryItemEntity, Object>>(this) {
                    @Override
                    public void onSuccess(PaginationEntity<CatgoryItemEntity, Object> result) {
                        super.onSuccess(result);
                        if (mCurrentPage == 1) mData.clear();
                        mData.addAll(result.list);
                        mAdapter.notifyDataSetChanged();
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && mData.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                    @Override
                    public void onComplete() {
                        super.onComplete();
                        mRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mRefreshLayout.setRefreshing(false);
                    }
                },this);
        /*new BaseObserver<PaginationEntity<CatgoryItemEntity, Object>>(this) {
                    @Override
                    public void onHandleSuccess(PaginationEntity<CatgoryItemEntity, Object> result) {
                        if (mCurrentPage == 1) mData.clear();
                        mData.addAll(result.list);
                        mAdapter.notifyDataSetChanged();
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && mData.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                });*/
    }

    protected void search(String keyword) {
        if (TextUtils.isEmpty(keyword)) {
            ToastUtil.error("请输入关键词");
            return;
        }
        mKeyword = keyword;
        mKeywordEt.setText(mKeyword);
        getData(true);
        mKeywordEt.clearFocus();
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

    @OnClick(R.id.tvSearch)
    public void onSearchClicked() {
        search(mKeywordEt.getText().toString().trim());
    }

    public static void start(Context context) {
        context.startActivity(new Intent(context, WorldSearchActivity.class));
    }
}
