package com.weiju.ccmall.module.page

//import com.bumptech.glide.request.animation.GlideAnimation
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PixelFormat
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.support.constraint.ConstraintLayout
import android.util.TypedValue
import android.view.*
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import android.widget.ImageButton
import android.widget.RelativeLayout
import android.widget.TextView
import com.blankj.utilcode.utils.AppUtils
import com.blankj.utilcode.utils.FileUtils
import com.blankj.utilcode.utils.StringUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.just.agentweb.AgentWeb
import com.weiju.ccmall.BuildConfig
import com.weiju.ccmall.R
import com.weiju.ccmall.module.face.LivenessHelper
import com.weiju.ccmall.module.xysh.fragment.CreditFragment
import com.weiju.ccmall.shared.basic.AgentFActivity
import com.weiju.ccmall.shared.basic.BaseActivity
import com.weiju.ccmall.shared.util.CSUtils
import com.weiju.ccmall.shared.util.SessionUtil
import com.weiju.ccmall.shared.util.ToastUtil
import kotlinx.android.synthetic.main.activity_webview.*
import kotlinx.android.synthetic.main.base_layout.*
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * @author JayChan <voidea></voidea>@foxmail.com>
 * @version 1.0
 * com.weiju.ccmall.module.page
 * @since 2017-07-05
 */
class WebViewActivity : BaseActivity() {

//    @BindView(R.id.layoutWebview)
//    internal var mLayoutWebview: FrameLayout? = null
//    @BindView(R.id.baseRelativeLayout)
//    internal var baseRelativeLayout: RelativeLayout? = null

    private var mWeb: AgentWeb? = null
    private var hideToolbar: Boolean = false
    private var close: TextView? = null
    private var lp: RelativeLayout.LayoutParams? = null
    private var v: Float = 0.toFloat()
    private var mConstraintLayout: ConstraintLayout? = null
    private var wmParams: WindowManager.LayoutParams? = null
    private var mWindowManager: WindowManager? = null
    private var imageButton: ImageButton? = null


    private var helper: LivenessHelper? = null
    private var isFace = false
    private var isStartingShare = false
    private lateinit var mUrl: String

    /**
     * 跳转链接拦截
     */
    private//客服
    //分享
    //纯图片分享
    //获取图片链接地址 -> 解码
    val webViewClient: WebViewClient
        get() = object : WebViewClient() {

            override fun onPageFinished(view: WebView?, url: String?) {
                if (url == "http://credit.ccmallv2.create-chain.net/customizYTH/#/NameAndIdCard") {
                    AgentFActivity.start2(this@WebViewActivity, CreditFragment::class.java)
                    view?.goBack()
                } else if (hideToolbar) {
                    val uri = Uri.parse(url)
                    if (uri.host == BuildConfig.XYSH_HOST) {
                        when (uri.fragment) {
                            "/" -> setVisibility(close, View.VISIBLE)
                            "/CashBill" -> setVisibility(close, View.VISIBLE)
                            "/RepayPlanCreditList" -> setVisibility(close, View.VISIBLE)
                            "/My" -> setVisibility(close, View.VISIBLE)
                            "/NameAndIdCard" -> {
                                setVisibility(close, View.GONE)
                                if (!isFace) {
                                    if (helper == null)
                                        helper = LivenessHelper()
                                    helper?.start(this@WebViewActivity)
                                }
                            }
                            else -> setVisibility(close, View.GONE)
                        }
//                        close?.layoutParams = lp
                    } else
                        setVisibility(close, View.GONE)
                }
                super.onPageFinished(view, url)
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                if (url.contains("weixin://")) { // 打开微信
                    val uri = Uri.parse(url)
                    val intent = Intent(Intent.ACTION_VIEW, uri)
                    try {
                        startActivity(intent)
                    } catch (e: Exception) {
                        AlertDialog.Builder(baseContext)
                                .setMessage("未检测到微信客户端，请安装后重试。")
                                .setPositiveButton("知道了", null).show()
                    }

                    return true
                } else if (url.contains("wx.tenpay.com/cgi-bin/mmpayweb-bin/checkmweb")) { // 微信H5支付中间页面设置Referer
                    val extraHeaders = HashMap<String, String>()
                    extraHeaders["Referer"] = "https://www.zhongan.com"
                    view.loadUrl(url, extraHeaders)
                    return true
                } else {
                    val parse = Uri.parse(url)
                    if ("create-chain" == parse.scheme) {
                        val host = parse.host
                        if (host != null)
                            when (host) {
                                "jump" -> {
                                    val targe = parse.getQueryParameter("targe")
                                    when (targe) {
                                        "check_liveness" -> {
                                            if (helper == null)
                                                helper = LivenessHelper()
                                            helper?.start(this@WebViewActivity)
                                        }
                                        else -> CSUtils.start(baseContext, targe, "102a49c45bb54dbbb2394becdaffbce2")
                                    }
                                }
                                "share" -> if ("image" == parse.getQueryParameter("content_type")) {
                                    toShareActivity(parse)
                                }
                            }
                        return true
                    } else {
                        if (hideToolbar && parse.host != BuildConfig.XYSH_HOST)
                            close!!.visibility = View.GONE
                        return super.shouldOverrideUrlLoading(view, url)
                    }
                }
            }

        }


    override fun onCreate(savedInstanceState: Bundle?) {
        hideToolbar = intent.getBooleanExtra("hideToolbar", false)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
//        ButterKnife.bind(this)
        if (hideToolbar)
            hideHeader()
        else
            setLeftBlack()
        mUrl = intent.getStringExtra("url")
        if (mUrl == null || mUrl.toString().trim() == "") {
            return
        }
        if (mUrl.contains("create-chain.net") && SessionUtil.getInstance().isLogin) {
            val uri = Uri.parse(mUrl)
            mUrl = uri.buildUpon()
                    .appendQueryParameter("token", SessionUtil.getInstance().oAuthToken)
                    .build()
                    .toString()
        }
        openWeb()
       // initWindown()
    }

    override fun onResume() {
        isStartingShare = false
        if (imageButton != null && mWindowManager != null) {
            imageButton!!.setVisibility(View.VISIBLE)

        }
        super.onResume()
    }

    override fun onDestroy() {
        if (imageButton != null) {
            imageButton!!.visibility = View.GONE
        }
        super.onDestroy()
    }

    private fun initWindown() {
        val showWindow = intent.getBooleanExtra("showWindow", false)
        if (showWindow) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (Settings.canDrawOverlays(this)) {
                    creartWindown()
                } else {
                    //若没有权限，提示获取.
                    val intent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
                    startActivity(intent)
                }
            } else {
                creartWindown()
            }
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun creartWindown() {
        wmParams = WindowManager.LayoutParams()
        mWindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            wmParams!!.type = WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY
        } else {
            wmParams!!.type = WindowManager.LayoutParams.TYPE_PHONE
        }
        wmParams!!.format = PixelFormat.RGBA_8888
        wmParams!!.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
        wmParams!!.x = 400
        wmParams!!.y = 800
        wmParams!!.width = 60 * 3
        wmParams!!.height = 60 * 3
        var inflater: LayoutInflater = LayoutInflater.from(this)

        mConstraintLayout = inflater.inflate(R.layout.view_window, null) as ConstraintLayout?
        imageButton = mConstraintLayout!!.findViewById(R.id.imageButton1)
        mWindowManager!!.addView(mConstraintLayout, wmParams)
        mConstraintLayout!!.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
        imageButton!!.setOnTouchListener { v, event ->
            var lastX: Int = 0
            var lastY: Int = 0
            var downX: Int = 0
            var downY: Int = 0
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    lastX = event.rawX.toInt()
                    downX = lastX
                    lastY = event.rawY.toInt()
                    downY = lastY
                }
                MotionEvent.ACTION_MOVE -> {
                    val disX = (event.rawX - lastX).toInt()
                    val disY = (event.rawY - lastY).toInt()
                    wmParams!!.x += disX
                    wmParams!!.y += disY
                    mWindowManager!!.updateViewLayout(mConstraintLayout, wmParams)
                    lastX = event.rawX.toInt()
                    lastY = event.rawY.toInt()

                }
                MotionEvent.ACTION_UP -> {
                    val x = event.rawX.toInt()
                    val y = event.rawY.toInt()
                    var upX = x - downX
                    var upY = y - downY
                    upX = Math.abs(upX)
                    upY = Math.abs(upY)
                    if (upX < 20 && upY < 20) {
                        if (!StringUtils.isEmpty(mUrl)) {
                            openWeb()
                        }
                        false
                    }
                    mWindowManager!!.updateViewLayout(mConstraintLayout, wmParams)
                }
                else -> {

                }
            }
            true
        }
    }

    private fun openWeb() {
        mWeb = AgentWeb.with(this)//传入Activity
                //传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams ,第一个参数和第二个参数应该对应。
                .setAgentWebParent(layoutWebview, FrameLayout.LayoutParams(-1, -1))
                .useDefaultIndicator()// 使用默认进度条
                .setIndicatorColor(R.color.red) // 使用默认进度条颜色
                .setReceivedTitleCallback { _, title ->
                    if (!hideToolbar)
                        setTitle(title)
                } //设置 Web 页面的 title 回调
                .setWebViewClient(webViewClient)
                .createAgentWeb()//
                .ready()
                .go(mUrl)
        val view = mWeb?.webCreator?.get()
        view?.settings?.javaScriptEnabled = true
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            view?.settings?.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            10008 -> {
                val v = mWeb?.webCreator?.get()
                if (v?.url == "http://credit.ccmallv2.create-chain.net/customizYTH/#/NameAndIdCard")
                    v.goBack()
            }
            10009 -> {
                val v = mWeb?.webCreator?.get()
                if (v?.url != "http://credit.ccmallv2.create-chain.net/customizYTH/#/NameAndIdCard")
                    AgentFActivity.start2(this, CreditFragment::class.java)
//                    v?.loadUrl("http://credit.ccmallv2.create-chain.net/customizYTH/#/NameAndIdCard")
                ToastUtil.success("人脸检测成功！")
                isFace = true
            }
            else -> mWeb!!.uploadFileResult(requestCode, resultCode, data)
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            helper?.onRequestPermissionsResult(this, requestCode, permissions, grantResults)
        }
    }

    private fun hideHeader() {
        close = TextView(this)
        close?.visibility = View.GONE
        close!!.setTextColor(-0xcccccd)
        close!!.text = "关闭"
        close!!.gravity = Gravity.CENTER
        v = resources.displayMetrics.widthPixels / 1080f
        val size = (v * 40).toInt()
        close!!.setTextSize(TypedValue.COMPLEX_UNIT_PX, size.toFloat())
        lp = RelativeLayout.LayoutParams(size * 3, (v * 125).toInt())
        lp?.leftMargin = (v * 30).toInt()
        close!!.layoutParams = lp
        close!!.setOnClickListener { finish() }
        baseRelativeLayout.addView(close, baseRelativeLayout.childCount)

        headerLayout.visibility = View.GONE
    }

    override fun onBackPressed() {
        val view = mWeb?.webCreator?.get()
        if (hideToolbar && view?.canGoBack() == true) {
            val uri = Uri.parse(view.url)
            if (uri.host == BuildConfig.XYSH_HOST) {
                when (uri.fragment) {
                    "/" -> super.onBackPressed()
                    "/CashBill" -> super.onBackPressed()
                    "/RepayPlanCreditList" -> super.onBackPressed()
                    "/My" -> super.onBackPressed()
                    else -> view.goBack()
                }
            } else {
                val list = view.copyBackForwardList()
                if (list != null && list.size > 0) {
                    val item = list.getItemAtIndex(list.currentIndex - 1)
                    if (item != null) {
                        val history = Uri.parse(item.url)
                        if (history.host == BuildConfig.XYSH_HOST)
                            view.loadUrl(intent.getStringExtra("url"))
                        else
                            view.goBack()
                    } else
                        view.loadUrl(intent.getStringExtra("url"))
                } else
                    view.loadUrl(intent.getStringExtra("url"))
            }
        } else
            super.onBackPressed()
    }

    private fun setVisibility(v: View?, visibility: Int) {
        if (v?.visibility == visibility)
            return
        v?.visibility = visibility
    }

    private fun toShareActivity(parse: Uri) {
        if (!isStartingShare) {
            isStartingShare = true
            val imageUrl = Uri.decode(parse.getQueryParameter("share_content"))
            ToastUtil.showLoading(this)
            Glide.with(this).load(imageUrl).into(object : SimpleTarget<Drawable>() {
                override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                    if (resource != null) {
                        if (resource != null) {
                            val b = Bundle()

                            val w = resource.intrinsicWidth
                            val h = resource.intrinsicHeight
                            val file = getSaveFile()

                            val config = if (resource.opacity != PixelFormat.OPAQUE)
                                Bitmap.Config.ARGB_8888
                            else
                                Bitmap.Config.RGB_565
                            // 建立对应 bitmap
                            val bitmap = Bitmap.createBitmap(w, h, config)
                            // 建立对应 bitmap 的画布
                            val canvas = Canvas(bitmap)
                            resource.setBounds(0, 0, w, h)
                            // 把 drawable 内容画到画布中
                            resource.draw(canvas)
                            saveBitmapFile(bitmap, file, false)
                            b.putString("imageUrl", imageUrl)
                            b.putString("f", file.absolutePath)
                            AgentFActivity.start(this@WebViewActivity, XyshFragment::class.java, b)
                        }
                        ToastUtil.hideLoading()
                    }
                }

                override fun onLoadFailed(errorDrawable: Drawable?) {
                    ToastUtil.hideLoading()
                }
            })


            /*Glide.with(this@WebViewActivity.applicationContext).load(imageUrl).asBitmap().into(object : SimpleTarget<Bitmap>() {
                override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {
                    if (resource != null) {
                        val b = Bundle()
                        val file = getSaveFile()
                        saveBitmapFile(resource, file, false)
                        b.putString("imageUrl", imageUrl)
                        b.putString("f", file.absolutePath)
                        AgentFActivity.start(this@WebViewActivity, XyshFragment::class.java, b)
                    }
                    ToastUtil.hideLoading()
                }

                override fun onLoadFailed(e: Exception?, errorDrawable: Drawable?) {
                    ToastUtil.hideLoading()
                }
            })*/
        }

    }

    private fun getSaveFile(): File {
        val dirPath = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).path
                + "/" + AppUtils.getAppName(this))
        val file = File(dirPath, System.currentTimeMillis().toString() + ".jpg")
        FileUtils.createOrExistsDir(dirPath)
        return file
    }

    private fun saveBitmapFile(bitmap: Bitmap, saveFile: File, isShowMsg: Boolean) {
        try {
            val bos = BufferedOutputStream(FileOutputStream(saveFile))
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
            bos.flush()
            bos.close()
            if (isShowMsg) {
                ToastUtil.success("保存成功")
                val intent = Intent()
                intent.action = Intent.ACTION_MEDIA_SCANNER_SCAN_FILE
                intent.data = Uri.fromFile(saveFile)
                sendBroadcast(intent)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

}
