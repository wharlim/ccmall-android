package com.weiju.ccmall.module.xysh.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.google.common.collect.Lists;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.adapter.SelectBankAdapter;
import com.weiju.ccmall.module.xysh.adapter.SelectBankTitleAdapter;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Bank;
import com.weiju.ccmall.shared.bean.BankItem;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.constant.Action;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import butterknife.BindView;

/**
 * @author chenyanming
 * @time 2019/6/3 on 15:04
 * @desc ${TODD}
 */
public class SelectBankActivity extends BaseListActivity {

    @BindView(R.id.recyclerViewTitle)
    protected RecyclerView mRecyclerViewTitle;

    private XyshService mXyshService = ServiceManager.getInstance().createService2(XyshService.class);
    private List<Bank> mDatas = new ArrayList<>();
    private SelectBankAdapter mAdapter = new SelectBankAdapter(new ArrayList<>());
    private String mKeyword;

    private SelectBankTitleAdapter mSelectBankTitleAdapter = new SelectBankTitleAdapter();
    private String mHotKey;


    @Override
    public void initView() {
        super.initView();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            window.setStatusBarColor(getResources().getColor(R.color.red));
//            QMUIStatusBarHelper.setStatusBarDarkMode(this);
//        }

//        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_white);
//        getHeaderLayout().makeHeaderRed();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SelectBankActivity.this);
        mRecyclerViewTitle.setLayoutManager(linearLayoutManager);
        mRecyclerViewTitle.setAdapter(mSelectBankTitleAdapter);
        mRecyclerViewTitle.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                String item = mSelectBankTitleAdapter.getItem(position);
                item = item.equals("热门") ? mHotKey : item;
                for (int i = 0; i < mAdapter.getItemCount(); i++) {
                    BankItem item1 = mAdapter.getItem(i);
                    if (null != item1 && item1.mItemType == BankItem.TITLE && item1.title.equals(item)){
                        mRecyclerView.scrollToPosition(i+2);
                    }
                }
            }
        });
    }


    @Override
    public String getTitleStr() {
        return "选择开户行";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        BankItem item = mAdapter.getItem(position);
        if (item != null && item.mItemType != BankItem.TITLE) {
            Intent intent = getIntent();
            intent.putExtra("bank", item.mItem);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser == null) {
            ToastUtil.error("请先登录");
            return;
        }
        APIManager.startRequest(mXyshService.bankCNAPS(loginUser.phone, SessionUtil.getInstance().getOAuthToken()),
                new BaseRequestListener<List<Bank>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(List<Bank> banks) {
                        mDatas = banks;
                        searchBanks();

//                        mDatas.add(new BankItem("热门银行"));
//                        for (Bank bank : banks) {
//                            if (bank.recommend == 1) {
//                                mDatas.add(new BankItem(bank, "热门银行"));
//                            }
//                        }
//                        boolean hasData = false;
//                        for (String pinyin : mPinyinList) {
//                            for (Bank bank : banks) {
//                                if (pinyin.equals(bank.pinyin.substring(0, 1))) {
//                                    hasData = true;
//                                }
//                            }
//                            if (hasData) {
//                                mDatas.add(new BankItem(pinyin));
//                                hasData = false;
//                            }
//                            for (Bank bank : banks) {
//                                if (pinyin.equals(bank.pinyin.substring(0, 1))) {
//                                    mDatas.add(new BankItem(bank, pinyin));
//                                }
//                            }
//                        }
                        //
                    }
                },this);
    }

    private void searchBanks() {
        if (null == mDatas || mDatas.isEmpty()) {
            return;
        }
        mHotKey = "热门银行";
        LinkedHashMap<String, List<BankItem>> map = new LinkedHashMap<>();
        map.put(mHotKey, new ArrayList<>());
        for (Bank bank : mDatas) {
            String pingYin = bank.pinyin.toUpperCase();
            if (!TextUtils.isEmpty(mKeyword) && !(bank.bankName.contains(mKeyword) || pingYin.contains(mKeyword))) {
                continue;
            }
            String letter = pingYin.substring(0, 1);
            if (!map.containsKey(letter)) {
                map.put(letter, new ArrayList<>());
            }
            map.get(letter).add(new BankItem(bank, letter));
            if (bank.recommend == 1) {
                map.get(mHotKey).add(new BankItem(bank, letter));
            }
        }

        List<BankItem> bankItems = new ArrayList<>();
        List<BankItem> hotKeyBankItems = map.get(mHotKey);
        if (null != hotKeyBankItems && hotKeyBankItems.size() > 0) {
            bankItems.add(new BankItem(mHotKey));
            bankItems.addAll(hotKeyBankItems);
        }

        Set<String> letters = map.keySet();
        letters.remove(mHotKey);
        ArrayList<String> letterList = Lists.newArrayList(letters);
        Collections.sort(letterList, String::compareToIgnoreCase);
        for (String letter : letterList) {
            BankItem bankItem = new BankItem(letter);
            bankItems.add(bankItem);
            bankItems.addAll(map.get(letter));
        }
//        for (Map.Entry<String, List<BankItem>> entry : map.entrySet()) {
//            BankItem bankItem = new BankItem(entry.getKey());
//            bankItems.add(bankItem);
//            bankItems.addAll(entry.getValue());
//        }
        mAdapter.setNewData(bankItems);

        if (null != hotKeyBankItems && hotKeyBankItems.size() > 0) {
            letterList.add(0, "热门");
        }
        mSelectBankTitleAdapter.setNewData(letterList);

    }


    @Override
    public List<View> getHeaderViews() {
        LinearLayout layoutHead = (LinearLayout) View.inflate(this, R.layout.view_select_bank_head, null);
        final EditText etSearch = layoutHead.findViewById(R.id.etSearch);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mKeyword = s.toString();
                searchBanks();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        List<View> views = new ArrayList<>();
        views.add(layoutHead);
        return views;
    }

    @Override
    public boolean isLoadMore() {
        return false;
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_select_bank;
    }

    public static void start(Activity context, int reqCode) {
        Intent intent = new Intent(context, SelectBankActivity.class);
        context.startActivityForResult(intent, reqCode);
    }

    public static Bank obtainResult(Intent data) {
        return (Bank) data.getSerializableExtra("bank");
    }
}
