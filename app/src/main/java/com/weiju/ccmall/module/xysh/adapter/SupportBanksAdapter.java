package com.weiju.ccmall.module.xysh.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.SupportBanksItem;

public class SupportBanksAdapter extends BaseQuickAdapter<SupportBanksItem, BaseViewHolder> {
    public SupportBanksAdapter() {
        super(R.layout.item_support_bank);
    }

    @Override
    protected void convert(BaseViewHolder helper, SupportBanksItem item) {
        helper.setText(R.id.tv_bank_name, item.bankName);
        helper.setText(R.id.tv_transfer_limit_one_time, 1000 + "元");
        helper.setText(R.id.tv_transfer_limit_one_day, 5000 + "元");
    }
}
