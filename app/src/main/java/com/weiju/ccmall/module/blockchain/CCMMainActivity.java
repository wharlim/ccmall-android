package com.weiju.ccmall.module.blockchain;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.beans.BlockChainInfo;
import com.weiju.ccmall.module.blockchain.computingpower.ComputingPowerActivity;
import com.weiju.ccmall.module.blockchain.fragments.ComputePowerFragment;
import com.weiju.ccmall.module.blockchain.fragments.DetailFragment;
import com.weiju.ccmall.module.blockchain.fragments.RankingFragment;
import com.weiju.ccmall.module.blockchain.transferccm.TransferCCMActivity;
import com.weiju.ccmall.module.blockchain.transferin.TransferInActivity;
import com.weiju.ccmall.module.blockchain.transferout.TransferOutActivity;
import com.weiju.ccmall.module.blockchain.utils.BlockChainUtil;
import com.weiju.ccmall.module.page.WebViewCommonActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.IBlockChain;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CCMMainActivity extends BaseActivity {

    IBlockChain blockChain;
    User user;

    @BindView(R.id.tv_ccm)
    TextView tvCcm;
    @BindView(R.id.tv_computing_power)
    TextView tvComputingPower;
    @BindView(R.id.tv_computing_rate)
    TextView tvComputingRate;
    @BindView(R.id.tv_active_val)
    TextView tvActiveVal;
    @BindView(R.id.computing_power_container)
    FrameLayout computingPowerContainer;
    @BindView(R.id.detail_container)
    FrameLayout detailContainer;
    @BindView(R.id.ranking_container)
    FrameLayout rankingContainer;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    ComputePowerFragment computePowerFragment;
    DetailFragment detailFragment;
    RankingFragment rankingFragment;

    BlockChainInfo chainInfo;
    @BindView(R.id.tv_transfer_out)
    TextView tvTransferOut;
    @BindView(R.id.llDetail)
    LinearLayout llDetail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ccm_main);
        ButterKnife.bind(this);
        setTitle("CCM");
        setLeftBlack();
        blockChain = ServiceManager.getInstance().createService(IBlockChain.class);
        user = SessionUtil.getInstance().getLoginUser();
        if (user == null) {
            finish();
            return;
        }
        getBlockChainInfo(true);

        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                if (i == 0) {
                    return ComputePowerFragment.newInstance();
                } else if (i == 1) {
                    return DetailFragment.newInstance();
                } else if (i == 2) {
                    return RankingFragment.newInstance();
                }
                throw new IllegalArgumentException("无效参数: " + i);
            }

            @Override
            public int getCount() {
//                return 3;
                return 2; // 隐藏排行榜
            }
        });
        // 首先显示算力
        onViewClicked(computingPowerContainer);
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof ComputePowerFragment) {
            computePowerFragment = (ComputePowerFragment) fragment;
        } else if (fragment instanceof DetailFragment) {
            detailFragment = (DetailFragment) fragment;
        } else if (fragment instanceof RankingFragment) {
            rankingFragment = (RankingFragment) fragment;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getBlockChainInfo(false);
    }

    @Override
    protected void onStart() {
        super.onStart();
        MyApplication.isShowPayPwd = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.isShowPayPwd = false;
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, CCMMainActivity.class);
        context.startActivity(intent);
    }


    private void getBlockChainInfo(boolean showLoading) {
        if (showLoading) {
            ToastUtil.showLoading(this);
        }
        APIManager.startRequest(blockChain.getAccountInfo(), new BaseRequestListener<BlockChainInfo>() {
            @Override
            public void onSuccess(BlockChainInfo result) {
                super.onSuccess(result);
                tvCcm.setText(BlockChainUtil.formatCCMCoin(result.bi));
                tvComputingRate.setText(BlockChainUtil.formatCCMCoin(result.accountCcm.availableRate) + "%");
                tvComputingPower.setText(BlockChainUtil.formatCCMCoin(result.accountCcm.sumCcm));
                chainInfo = result;
//                tvTransferOut.setVisibility(result.isForbiddenTransferCCM ? View.GONE:View.VISIBLE);
            }

            @Override
            public void onComplete() {
                super.onComplete();
                ToastUtil.hideLoading();
            }
        }, this);
    }

    @OnClick({R.id.tv_transfer_in, R.id.tv_transfer_out, R.id.tv_all_computing_power,
            R.id.computing_power_container, R.id.detail_container,
            R.id.ranking_container, R.id.tv_transfer_ccm, R.id.tvDetail})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_transfer_in:
                if (chainInfo == null) {
                    return;
                }
                TransferInActivity.start(this, chainInfo.address);
                break;
            case R.id.tv_transfer_out:
                checkCcmRollOut();
                break;
            case R.id.tv_transfer_ccm:
                TransferCCMActivity.start(this);
                break;
            case R.id.tv_all_computing_power:
                ComputingPowerActivity.start(this);
                break;
            case R.id.computing_power_container:
                setTabState(computingPowerContainer, true, R.mipmap.computing_power_selected);
                setTabState(detailContainer, false, R.mipmap.detail_unselect);
                setTabState(rankingContainer, false, R.mipmap.ranking_unselect);
                viewPager.setCurrentItem(0, true);
                llDetail.setVisibility(View.GONE);
                break;
            case R.id.detail_container:
                setTabState(computingPowerContainer, false, R.mipmap.computing_power_unselect);
                setTabState(detailContainer, true, R.mipmap.detail_selected);
                setTabState(rankingContainer, false, R.mipmap.ranking_unselect);
                viewPager.setCurrentItem(1, true);
                llDetail.setVisibility(View.VISIBLE);
                break;
            case R.id.ranking_container:
                setTabState(computingPowerContainer, false, R.mipmap.computing_power_unselect);
                setTabState(detailContainer, false, R.mipmap.detail_unselect);
                setTabState(rankingContainer, true, R.mipmap.ranking_selected);
                viewPager.setCurrentItem(2, true);
                llDetail.setVisibility(View.GONE);
                break;
            case R.id.tvDetail:
                WebViewCommonActivity.start(this, String.format("%sblock/public?address=%s", BuildConfig.WECHAT_URL, chainInfo.address), false, false);
                break;
        }
    }

    private void checkCcmRollOut() {
        APIManager.startRequest(blockChain.checkCcmRollOut(), new BaseRequestListener<Object>(this) {
            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.error(e.getMessage());
            }

            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                UserService.checkHasPayPwd(CCMMainActivity.this, new UserService.HasPasswordListener() {
                    @Override
                    public void onHasPassword() {
                        TransferOutActivity.start(CCMMainActivity.this);
                    }
                });
            }
        }, this);
    }

    private void setTabState(FrameLayout tabContainer, boolean selected, @DrawableRes int drawableLeft) {
        tabContainer.setSelected(selected);
        TextView tab = (TextView) tabContainer.getChildAt(0);
        tab.setSelected(selected);
        tab.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(drawableLeft), null, null, null);
    }
}
