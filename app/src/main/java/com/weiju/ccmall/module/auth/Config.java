package com.weiju.ccmall.module.auth;

import com.weiju.ccmall.BuildConfig;


public class Config {
    /**
     * 是否开启会员会员折扣功能
     */
    public static final boolean IS_DISCOUNT = false;
    public static final boolean IS_DEBUG = BuildConfig.DEBUG;
    public static final int ERROR_CODE = -1;
    public static final String INTENT_KEY_TYPE_NAME = "intenttypekey";
    public static final String INTENT_KEY_ID = "id";
    public static final String KEY_EVENTBUS_TAG = "eventbus_tag";
    public static final int REQUEST_CODE_CHOOSE_IMAGE_SELECT = 0x998;

    public static final String BUGLY_APP_ID = "04392b505a";


    public static final class USER {
        public static final String SP_NAME = "usersp";
        public static final String MODEL = "model";

        public static final int INTENT_KEY_TYPE_REGISTER = 10000;
        public static final int INTENT_KEY_TYPE_FIND_PASSWORD = 10001;
        /**
         * 实名认证提交成功
         */
        public static final int INTENT_KEY_TYPE_AUTH_IDENTITY_SUBMIT_SUCCESS = 10002;
        public static final int INTENT_KEY_TYPE_AUTH_PHONE = 10003;
        public static final int INTENT_KEY_TYPE_AUTH_CARD = 10004;
        /**
         * 验证提现
         */
        public static final int INTENT_KEY_TYPE_AUTH_DEAL = 10004 * 2;
        /**
         * 验证提现
         */
        public static final int INTENT_KEY_TYPE_AUTH_DEAL_ALI = 10004 * 3;
        /**
         * 实名认证 审核失败
         */
        public static final int INTENT_KEY_TYPE_AUTH_IDENTITY_FAIL = 10005;
        /**
         * 实名认证 审核成功
         */
        public static final int INTENT_KEY_TYPE_AUTH_IDENTITY_SUCCESS = 10006;
        /**
         * 实名认证 审核中
         */
        public static final int INTENT_KEY_TYPE_AUTH_IDENTITY_WAIT = 10007;

        /**
         * 银行卡 提交成功
         */
        public static final int INTENT_KEY_TYPE_AUTH_CARD_SUBMIT_SUCCESS = 102;
        /**
         * 银行卡 审核失败
         */
        public static final int INTENT_KEY_TYPE_AUTH_CARD_FAIL = 105;
        /**
         * 银行卡 审核成功
         */
        public static final int INTENT_KEY_TYPE_AUTH_CARD_SUCCESS = 106;
        /**
         * 银行卡 审核中
         */
        public static final int INTENT_KEY_TYPE_AUTH_CARD_WAIT = 107;
    }


    public static final class NET_MESSAGE {
        public static final String NO_LOGIN = "请先登录";
    }
}
