package com.weiju.ccmall.module.live.adapter;

import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.util.FrescoUtil;

import java.util.List;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
public class LiveHorizontalUserAdapter extends BaseQuickAdapter<LiveUser, BaseViewHolder> {
    public LiveHorizontalUserAdapter() {
        super(R.layout.item_live_horizonta_user);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveUser item) {
        SimpleDraweeView simpleDraweeView = helper.getView(R.id.ivAvatar);
        FrescoUtil.setImage(simpleDraweeView, item.headImage);
    }
}
