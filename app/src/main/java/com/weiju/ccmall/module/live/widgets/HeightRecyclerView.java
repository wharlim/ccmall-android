package com.weiju.ccmall.module.live.widgets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class HeightRecyclerView extends RecyclerView {
    public HeightRecyclerView(Context context) {
        super(context);
    }

    public HeightRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HeightRecyclerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}