package com.weiju.ccmall.module.order;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.order.adapter.OrderItemAdapter;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.OrderProduct;
import com.weiju.ccmall.shared.bean.RefundsOrder;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.common.ImageAdapter;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.OrderService;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.CSUtils;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * <p>
 * Created by zjm on 2017/11/24 .
 */
public class RefundDetailActivity extends BaseActivity {

    @BindView(R.id.tvRefundType1)
    TextView mTvRefundType1;
    @BindView(R.id.tvRefundType2)
    TextView mTvRefundType2;
    @BindView(R.id.layoutRefundType)
    LinearLayout mLayoutRefundType;
    @BindView(R.id.tvStoreName)
    TextView mTvStoreName;
    @BindView(R.id.tvStorePhone)
    TextView mTvStorePhone;
    @BindView(R.id.tvStoreAddress)
    TextView mTvStoreAddress;
    @BindView(R.id.tvStoreExpress)
    TextView mTvStoreExpress;
    @BindView(R.id.layoutStore)
    LinearLayout mLayoutStore;
    @BindView(R.id.tvRefundReason)
    TextView mTvRefundReason;
    @BindView(R.id.tvRefundMoney)
    TextView mTvRefundMoney;
    @BindView(R.id.tvRefundRemark)
    TextView mTvRefundRemark;
    @BindView(R.id.rvRefundImages)
    RecyclerView mRvRefundImages;
    @BindView(R.id.tvRefundDate)
    TextView mTvRefundDate;
    @BindView(R.id.tvRefundCode)
    TextView mTvRefundCode;
    @BindView(R.id.tvRefundOrderCode)
    TextView mTvRefundOrderCode;
    @BindView(R.id.layoutRefundInfo)
    LinearLayout mLayoutRefundInfo;
    @BindView(R.id.rvProduct)
    RecyclerView mRvProduct;
    @BindView(R.id.tvItemCS)
    TextView mTvItemCS;
    @BindView(R.id.tvItemEdit)
    TextView mTvItemEdit;
    @BindView(R.id.tvItemInput)
    TextView mTvItemInput;
    @BindView(R.id.tvItemCancel)
    TextView mTvItemCancel;
    @BindView(R.id.bottomLayout)
    LinearLayout mBottomLayout;
    @BindView(R.id.tvStoreInputExpress)
    TextView mTvStoreInputExpress;
    @BindView(R.id.layoutProduct)
    LinearLayout mLayoutProduct;
    @BindView(R.id.tvRefundTagTitle)
    TextView mTvRefundTagTitle;
    @BindView(R.id.tvRefundTagReason)
    TextView mTvRefundTagReason;
    @BindView(R.id.tvRefundTagMoney)
    TextView mTvRefundTagMoney;
    @BindView(R.id.tvRefundTagRemark)
    TextView mTvRefundTagRemark;
    @BindView(R.id.tvEditExpress)
    TextView mTvEditExpress;

    private String mRefundId;
    private String mMemberId;
    private int mMode;
    private IOrderService mService;
    private List<OrderProduct> products = new ArrayList<>();
    private RefundsOrder mRefundOrder;
    private ImageAdapter mImageAdapter;
    private OrderItemAdapter mProductAdapter;
    private RefundsOrder.ApiRefundOrderBeanEntity mRefundOrderBean;
    private int mProductType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund_detail);
        ButterKnife.bind(this);

        getIntentData();
        initView();
        initData();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initData() {
        mService = ServiceManager.getInstance().createService(IOrderService.class);
        APIManager.startRequest(mService.getRefundDetail(mRefundId, mMemberId), new BaseRequestListener<RefundsOrder>(this) {

            @Override
            public void onSuccess(RefundsOrder result) {
                super.onSuccess(result);
                mRefundOrder = result;
                mRefundOrderBean = result.apiRefundOrderBean;
                List<OrderProduct> orderProducts = result.orderProducts;
                if (orderProducts != null && orderProducts.size() > 0) {
                    OrderProduct orderProduct = orderProducts.get(0);
                    mProductType = orderProduct.productType;
                }
                setRefundTypeLayout();
                setRefundStoreLayout();
                setRefundInfoLayout();
                setProductLayout();
                setBottomBtns();
                setTypeView();
            }
        }, this);
    }

    private void setTypeView() {
        if (mRefundOrderBean.refundType == 2 || mRefundOrderBean.refundType == 4) {
            setTitle("退款详情");
            mTvRefundTagTitle.setText("退款信息");
            mTvRefundTagReason.setText("退款原因：");
            mTvRefundTagMoney.setText("申请退款金额：");
            mTvRefundTagRemark.setText("退款说明：");
        } else {
            setTitle("退货详情");
            mTvRefundTagTitle.setText("退货信息");
            mTvRefundTagReason.setText("退货原因：");
            mTvRefundTagMoney.setText("申请退货金额：");
            mTvRefundTagRemark.setText("退货说明：");
        }
    }


    private void initView() {
        setTitle("退货详情");
        setLeftBlack();

        mRvProduct.setLayoutManager(new LinearLayoutManager(this));
        mRvProduct.setNestedScrollingEnabled(false);
        mProductAdapter = new OrderItemAdapter(this, products);
        mProductAdapter.setDetailModel(true);
        mProductAdapter.setRefundModel(true);
        mRvProduct.setAdapter(mProductAdapter);
        mRvProduct.addItemDecoration(new ListDividerDecoration(this));

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);
        gridLayoutManager.setAutoMeasureEnabled(true);
        gridLayoutManager.setSmoothScrollbarEnabled(false);
        mRvRefundImages.setLayoutManager(gridLayoutManager);
        mImageAdapter = new ImageAdapter(this);
        mRvRefundImages.setAdapter(mImageAdapter);
    }

    private void getIntentData() {
        mRefundId = getIntent().getStringExtra(Config.INTENT_KEY_ID);
        mMemberId = getIntent().getStringExtra("memberId");
        mMode = getIntent().getIntExtra("mode", OrderListActivity.MODE_BUYER);

    }

    private void setRefundTypeLayout() {
        mTvRefundType1.setText(mRefundOrder.apiRefundOrderBean.mePromptDetail);
        mTvRefundType2.setVisibility(View.VISIBLE);
        switch (mRefundOrderBean.refundStatus) {
            case -3:
            case -4:
                mTvRefundType2.setText("拒绝原因：" + mRefundOrderBean.sellerRemark);
                break;
            case 4:
                mTvRefundType2.setText("实际退款金额：" + ConvertUtil.centToCurrency(this, mRefundOrderBean.refundMoney));
                break;
            default:
                mTvRefundType2.setVisibility(View.GONE);
                break;
        }
    }

    private void setRefundStoreLayout() {
        RefundsOrder.StoreBeanEntity storeBean = mRefundOrder.storeBean;
        if (mRefundOrderBean.isRefundMoney() || mRefundOrderBean.refundStatus == 0 || ((mRefundOrderBean.refundStatus == -2 || mRefundOrderBean.refundStatus == -4) && StringUtils.isEmpty(mRefundOrderBean.refundGoodsExpressCode))) {
            mLayoutStore.setVisibility(View.GONE);
            return;
        }
        mTvStoreName.setText(String.format("商家姓名： %s", storeBean.contact));
        mTvStorePhone.setText(String.format("商家电话： %s", storeBean.phone));
        mTvStoreAddress.setText(storeBean.province + storeBean.city + storeBean.district + storeBean.address);

        if (mRefundOrder.apiRefundOrderBean.refundStatus == 1) {
            mTvStoreInputExpress.setVisibility(View.VISIBLE);
            mTvStoreExpress.setVisibility(View.GONE);
        } else {
            mTvStoreInputExpress.setVisibility(View.GONE);
            mTvStoreExpress.setVisibility(View.VISIBLE);

            String expressName = mRefundOrder.apiRefundOrderBean.refundGoodsExpressName;
            final String expressCode = mRefundOrder.apiRefundOrderBean.refundGoodsExpressCode;
            String format = String.format("%s <font color=\"#3333ff\">%s</font>", expressName, expressCode);
            mTvStoreExpress.setText(Html.fromHtml(format));
            mTvStoreExpress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    OrderService.checkExpress(RefundDetailActivity.this, expressCode, mRefundOrder.apiRefundOrderBean.expressType);
                }
            });

            if (mRefundOrderBean.refundStatus == 2) {
                mTvEditExpress.setVisibility(View.VISIBLE);
            }
        }
    }

    private void setRefundInfoLayout() {
        RefundsOrder.ApiRefundOrderBeanEntity bean = mRefundOrder.apiRefundOrderBean;
        mTvRefundReason.setText(bean.refundReason);
        mTvRefundMoney.setText(ConvertUtil.centToCurrency(this, bean.applyRefundMoney));
        mTvRefundRemark.setText(bean.refundRemark);
        mImageAdapter.setItems(bean.refundGoodsImage);
        mTvRefundDate.setText(bean.createDate);
        mTvRefundCode.setText(bean.refundCode);
        mTvRefundOrderCode.setText(bean.orderCode);
    }

    private void setProductLayout() {
        if (mRefundOrder.orderProducts.size() == 0) {
            mLayoutProduct.setVisibility(View.GONE);
        } else {
            mProductAdapter.removeAllItems();
            mProductAdapter.addItems(mRefundOrder.orderProducts);
        }
    }

    private void setBottomBtns() {
        if (mMode == OrderListActivity.MODE_MY_GIFT) {
            mBottomLayout.setVisibility(View.GONE);
            return;
        }
        for (int i = 0; i < mBottomLayout.getChildCount(); i++) {
            mBottomLayout.getChildAt(i).setVisibility(View.GONE);
        }
        if (mProductType == 28) {
            mTvItemCS.setText("联系店主");
        }
        mTvItemCS.setVisibility(View.VISIBLE);
        if (mRefundOrderBean.isRefundMoney()) {
            switch (mRefundOrderBean.refundStatus) {
                case 0:
                    mTvItemEdit.setVisibility(View.VISIBLE);
                    mTvItemCancel.setVisibility(View.VISIBLE);
                    break;
//                case -1:
//                    mTvItemCS.setVisibility(View.GONE);
//                    break;
                default:
            }
        } else {
            switch (mRefundOrderBean.refundStatus) {
                case 0:
                    mTvItemEdit.setVisibility(View.VISIBLE);
                    mTvItemCancel.setVisibility(View.VISIBLE);
                    break;
                case 1:
                case 2:
                    mTvItemCancel.setVisibility(View.VISIBLE);
                    break;
                default:
                    break;
            }
        }
    }

    private void editRefunds(RefundsOrder refundsOrder) {
        if (refundsOrder.apiRefundOrderBean.isRefundMoney()) {
            OrderService.editRefundMoneyActivity(this, refundsOrder.apiRefundOrderBean.orderCode, refundsOrder.apiRefundOrderBean.refundId);
        } else {
            OrderService.addOrEditRefundOrder(this, refundsOrder.apiRefundOrderBean.orderCode, refundsOrder.orderProducts, refundsOrder.apiRefundOrderBean.refundId);
        }
    }

    private void cancelRefunds(final String refundId) {
        final WJDialog wjDialog = new WJDialog(this);
        wjDialog.show();
        wjDialog.setTitle("撤回申请");
        if (mRefundOrderBean.isRefundMoney()) {
            wjDialog.setContentText("撤回后，该退款单将被关闭");
        } else {
            wjDialog.setContentText("撤回后，该退货单将被关闭");
        }
        wjDialog.setCancelText("取消");
        wjDialog.setConfirmText("确定");
        wjDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wjDialog.dismiss();
                OrderService.cancelRefundExt(RefundDetailActivity.this, refundId);
            }
        });
    }

    private void inputRefundsInfo(RefundsOrder refundsOrder) {
        String refundId = refundsOrder.apiRefundOrderBean.refundId;
        List<OrderProduct> orderProducts = refundsOrder.orderProducts;
        Intent intent = new Intent(this, RefundExpressActivity.class);
        intent.putExtra("refundId", refundId);
        if (orderProducts != null && orderProducts.size() > 0) {
            if (orderProducts.get(0).productType == 28){
                intent.putExtra("storeBean", refundsOrder.storeBean);
            }
        }
        startActivity(intent);
    }

    @OnClick({R.id.tvStoreInputExpress, R.id.tvEditExpress, R.id.tvItemCS, R.id.tvItemEdit, R.id.tvItemCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvStoreInputExpress:
            case R.id.tvEditExpress:
                inputRefundsInfo(mRefundOrder);
                break;
            case R.id.tvItemCS:
                goToService();
                break;
            case R.id.tvItemEdit:
                editRefunds(mRefundOrder);
                break;
            case R.id.tvItemCancel:
                cancelRefunds(mRefundOrder.apiRefundOrderBean.refundId);
                break;
        }
    }

    private void goToService() {
        if (mRefundOrder == null) {
            return;
        }
        final RefundsOrder item = mRefundOrder;
        if (mProductType == 18) {
            // 嗨购订单,打开im聊天客服
            contactCustomer(this, item.storeBean.memberId, item.storeBean.contact);
        } else if (mProductType == 28) {
            // 嗨购订单,打开im聊天客服
            contactCustomer(this, item.storeBean.memberId, item.storeBean.storeName);
        } else {
            // 其他订单，打开商城客服
            CSUtils.start(this, "退款退货用户");
        }
    }

    /**
     * 联系商家客服
     *
     * @param context
     * @param memberId
     * @param nickName
     */
    private void contactCustomer(Context context, String memberId, String nickName) {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, context);
                    ChatInfo chatInfo = new ChatInfo();
                    chatInfo.setType(TIMConversationType.C2C);
                    chatInfo.setId(memberId);
                    chatInfo.setChatName(nickName);
                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.putExtra("chatInfo", chatInfo);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(MsgStatus msgStatus) {
        switch (msgStatus.getAction()) {
            case MsgStatus.ACTION_REFUND_CHANGE:
                initData();
                break;
        }
    }

    @OnClick({R.id.tvCopyRefundCode, R.id.tvCopyRefundOrderCode})
    public void onCopyViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvCopyRefundCode:
                copy(mTvRefundCode.getText().toString());
                break;
            case R.id.tvCopyRefundOrderCode:
                copy(mTvRefundOrderCode.getText().toString());
                break;
        }
    }


    private void copy(String content) {
        if (TextUtils.isEmpty(content)) return;
        ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData mClipData = ClipData.newPlainText("Label", content);
        cm.setPrimaryClip(mClipData);
        ToastUtil.success("复制成功");
    }

}
