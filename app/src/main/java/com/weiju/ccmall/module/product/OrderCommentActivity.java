package com.weiju.ccmall.module.product;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.body.AddCommentBody;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.UploadResponse;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.common.ImageUploadAdapter;
import com.weiju.ccmall.shared.component.StarField;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.manager.UploadManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.zhihu.matisse.Matisse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.order
 * @since 2017-07-19
 */
public class OrderCommentActivity extends BaseActivity {


    @BindView(R.id.thumbIv)
    SimpleDraweeView mThumbIv;
    @BindView(R.id.titleTv)
    TextView mTitleTv;
    @BindView(R.id.contentEt)
    EditText mContentEt;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.descStarField)
    StarField mDescStarField;
    @BindView(R.id.expressStarField)
    StarField mExpressStarField;
    @BindView(R.id.serviceStarField)
    StarField mServiceStarField;
    @BindView(R.id.submitBtn)
    TextView mSubmitBtn;

    private String mOrderCode;
    private String mOrderId;
    private String mSkuId;
    private IProductService mProductService;
    private static final int REQUEST_CODE_CHOOSE_PHOTO = 1;
    private ImageUploadAdapter mImageUploadAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_comment);
        ButterKnife.bind(this);
        showHeader();
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            mOrderCode = intent.getExtras().getString("orderCode");
            mOrderId = intent.getExtras().getString("orderId");
            mSkuId = intent.getExtras().getString("skuId");
        }
        if (mOrderCode == null || mOrderCode.isEmpty() || mOrderId == null || mOrderId.isEmpty() || mSkuId == null || mSkuId.isEmpty()) {
            ToastUtil.error("参数错误");
            finish();
            return;
        }
        setTitle("评价");
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_black);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mProductService = ServiceManager.getInstance().createService(IProductService.class);
        loadSkuInfo();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);
        gridLayoutManager.setAutoMeasureEnabled(true);
        gridLayoutManager.setSmoothScrollbarEnabled(false);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mImageUploadAdapter = new ImageUploadAdapter(this, 9);
        mRecyclerView.setAdapter(mImageUploadAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    private void uploadImage(final Uri uri) {
        UploadManager.uploadImage(this, uri, new BaseRequestListener<UploadResponse>(this) {
            @Override
            public void onSuccess(UploadResponse result) {
                mImageUploadAdapter.addItem(result.url);
            }
        });
    }

    private void loadSkuInfo() {
        APIManager.startRequest(mProductService.getSkuById(mSkuId), new BaseRequestListener<SkuInfo>(this) {
            @Override
            public void onSuccess(SkuInfo result) {
                FrescoUtil.setImageSmall(mThumbIv, result.thumb);
                mTitleTv.setText(result.name);
            }
        },this);
    }

    @OnClick(R.id.submitBtn)
    public void submit() {
        String content = mContentEt.getText().toString();
        ArrayList<String> urls = new ArrayList<>();
        for (int i = 0; i < mImageUploadAdapter.getItemCount(); i++) {
            if (mImageUploadAdapter.getItemViewType(i) == 1) {
                urls.add(mImageUploadAdapter.getItems().get(i));
            }
        }
        if (StringUtils.isEmpty(content)) {
            ToastUtil.error("请输入评价内容");
            return;
        }

        AddCommentBody addCommentBody = new AddCommentBody(
                mOrderCode,
                mOrderId,
                content,
                urls,
                mDescStarField.getValue() * 10,
                mExpressStarField.getValue() * 10,
                mServiceStarField.getValue() * 10
        );
        APIManager.startRequest(mProductService.saveProductComment(addCommentBody), new BaseRequestListener<Object>(this) {

            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("发表成功");
                finish();
//                EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_COMMENT_ORDER));
                EventMessage eventMessage = new EventMessage(Event.commentFinish, mOrderId);
                EventBus.getDefault().post(eventMessage);
            }
        },this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_CHOOSE_PHOTO:
                    List<Uri> uris = Matisse.obtainResult(data);
                    LogUtils.e("拿到图片" + uris.get(0).getPath());
                    uploadImage(uris.get(0));
                    break;
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void imageUploadHandler(EventMessage message) {
        if (message.getEvent().equals(Event.selectImage2Upload)) {
            UploadManager.selectImage(this, REQUEST_CODE_CHOOSE_PHOTO, 1);
        }
    }
}
