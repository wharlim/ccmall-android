package com.weiju.ccmall.module.pickUp.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.pickUp.fragments.PickUpUsedListFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/8/1 on 18:17
 * @desc ${TODD}
 */
public class PickUpUsedListActivity extends BaseActivity {
    @BindView(R.id.tv_used)
    TextView tvUsed;
    @BindView(R.id.tv_given)
    TextView tvGiven;
    @BindView(R.id.tv_received)
    TextView tvReceived;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickup_used_list);
        ButterKnife.bind(this);
        setLeftBlack();
        setTitle("提货券使用记录");
        setUpViewPage();
    }

    private void setUpViewPage() {
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                switch (i) {
                    case 0:
                        return PickUpUsedListFragment.newInstance(PickUpUsedListFragment.TYPE_USED);
                    case 1:
                        return PickUpUsedListFragment.newInstance(PickUpUsedListFragment.TYPE_GIVING);
                    case 2:
                        return PickUpUsedListFragment.newInstance(PickUpUsedListFragment.TYPE_RECEIVED);
                }
                throw new IllegalArgumentException("index 错误");
            }

            @Override
            public int getCount() {
                return 3;
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                tvUsed.setSelected(false);
                tvGiven.setSelected(false);
                tvReceived.setSelected(false);
                switch (i) {
                    case 0:
                        tvUsed.setSelected(true);
                        break;
                    case 1:
                        tvGiven.setSelected(true);
                        break;
                    case 2:
                        tvReceived.setSelected(true);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });
        tvUsed.setSelected(true);
        viewPager.setCurrentItem(0);
    }

    @OnClick({R.id.tv_used, R.id.tv_given, R.id.tv_received})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_used:
                viewPager.setCurrentItem(0, true);
                break;
            case R.id.tv_given:
                viewPager.setCurrentItem(1, true);
                break;
            case R.id.tv_received:
                viewPager.setCurrentItem(2, true);
                break;
        }
    }

    //    private PickUpUsedAdapter mAdapter = new PickUpUsedAdapter();
//    private IPickUpService mService = ServiceManager.getInstance().createService(IPickUpService.class);
//
//    @Override
//    public String getTitleStr() {
//        return "已用提货券列表";
//    }
//
//    @Override
//    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
//
//    }
//
//    @Override
//    public BaseQuickAdapter getAdapter() {
//        return mAdapter;
//    }
//
//    @Override
//    public void getData(boolean isRefresh) {
//        APIManager.startRequest(mService.getUsedList(mCurrentPage, 15),
//                new BaseRequestListener<PaginationEntity<PickUp, Object>>(mRefreshLayout) {
//                    @Override
//                    public void onSuccess(PaginationEntity<PickUp, Object> result) {
//                        if (mCurrentPage == 1) {
//                            mAdapter.setNewData(result.list);
//                        } else {
//                            mAdapter.addData(result.list);
//                        }
//                        if (result.page >= result.totalPage) {
//                            mAdapter.loadMoreEnd(isRefresh && result.list.size() < 5);
//                        } else {
//                            mAdapter.loadMoreComplete();
//                        }
//                    }
//                },this);
//    }
}
