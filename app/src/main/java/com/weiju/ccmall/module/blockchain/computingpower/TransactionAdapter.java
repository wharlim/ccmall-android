package com.weiju.ccmall.module.blockchain.computingpower;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.beans.AllCCm;
import com.weiju.ccmall.module.blockchain.utils.BlockChainUtil;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.VH>{
    List<AllCCm.AccountTransctionBean.ContentBean> content;
    // 传递数据
    public TransactionAdapter(List<AllCCm.AccountTransctionBean.ContentBean> content) {
        this.content = content;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return VH.newInstance(LayoutInflater.from(viewGroup.getContext()), viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull VH vh, int i) {
        vh.bindView(content.get(i));
    }

    @Override
    public int getItemCount() {
        return content.size();
    }

    public static class VH extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_transaction_address)
        TextView tvTransactionAddress;
        @BindView(R.id.tv_transaction_time)
        TextView tvTransactionTime;
        @BindView(R.id.tv_from_address)
        TextView tvFromAddress;
        @BindView(R.id.tv_to_address)
        TextView tvToAddress;
        @BindView(R.id.tv_transaction_ccm)
        TextView tvTransactionCcm;

        VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindView(AllCCm.AccountTransctionBean.ContentBean contentBean) {
            tvTransactionAddress.setText(contentBean.txHash);
            tvFromAddress.setText(contentBean.from);
            tvToAddress.setText(contentBean.to);
            tvTransactionTime.setText(BlockAdapter.dateFormat.format(new Date(Long.valueOf(contentBean.txTimestamp))));
            tvTransactionCcm.setText(contentBean.amount + "CCM");
        }

        public static VH newInstance(LayoutInflater inflater, ViewGroup parent) {
            View view = inflater.inflate(R.layout.item_computing_power_transaction, parent, false);
            return new VH(view);
        }
    }
}
