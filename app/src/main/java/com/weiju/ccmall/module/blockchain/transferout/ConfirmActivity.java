package com.weiju.ccmall.module.blockchain.transferout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.events.TransferOutSuccess;
import com.weiju.ccmall.module.blockchain.utils.BlockChainUtil;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBlockChain;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ConfirmActivity extends BaseActivity {

    @BindView(R.id.et_receiver_address)
    EditText etReceiverAddress;
    @BindView(R.id.et_transfer_coin)
    EditText etTransferCoin;
    @BindView(R.id.et_authcode)
    EditText etAuthcode;
    @BindView(R.id.tv_get_authcode)
    TextView tvGetAuthcode;
    @BindView(R.id.tv_commit)
    TextView tvCommit;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.et_password)
    EditText etPassword;

    String phone;
    String toAddress;
    double transferCoin;

    IBlockChain blockChain;

    CountDownTimer timer;

    private void startTimer() {
        if (timer != null) {
            timer.cancel();
        }
        timer = new CountDownTimer(60 * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                long left = millisUntilFinished / 1000;
                tvGetAuthcode.setText("重新发送(" + left + ")");
            }

            @Override
            public void onFinish() {
                tvGetAuthcode.setText("重新发送");
                tvGetAuthcode.setEnabled(true);

            }
        };
        timer.start();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_out_confirm);
        ButterKnife.bind(this);
        User user = SessionUtil.getInstance().getLoginUser();
        phone = user.phone;
        String hint = "";
        try {
            hint = phone.substring(0, 3) + "****" + phone.substring(7, 11);
        } catch (Exception e) {
            hint = "无效手机号";
        }
//        etAuthcode.setHint(hint);
        tvPhone.setText(hint);
        Intent intent = getIntent();
        toAddress = intent.getStringExtra("toAddress");
        transferCoin = intent.getDoubleExtra("transferCoin", 0);
        etReceiverAddress.setText(toAddress);
        etTransferCoin.setText(BlockChainUtil.formatCCMCoin(transferCoin));
        blockChain = ServiceManager.getInstance().createService(IBlockChain.class);
        setTitle("确认信息");
        setLeftBlack();

        etAuthcode.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                String authCode = s.toString();
                tvCommit.setEnabled(authCode.length() > 0);
            }
        });
    }

    @OnClick({R.id.tv_get_authcode, R.id.tv_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_get_authcode:
                getAuthCode();
                break;
            case R.id.tv_commit:
                transaction();
                break;
        }
    }

    private void getAuthCode() {
        ToastUtil.showLoading(this);
        APIManager.startRequest(blockChain.sendTransCode(phone), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                if (isFinishing()) {
                    return;
                }
                tvGetAuthcode.setEnabled(false);
                etAuthcode.setHint("短信验证码");
                startTimer();
            }

            @Override
            public void onComplete() {
                ToastUtil.hideLoading();
            }
        }, this);
    }

    private void transaction() {
        String amount = etTransferCoin.getText().toString().trim();
        String checkNumber = etAuthcode.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        if (StringUtils.isEmpty(password)) {
            ToastUtil.error("请输入支付密码");
            return;
        }
        String encodePwd = Base64.encodeToString(password.getBytes(), Base64.NO_WRAP);
        ToastUtil.showLoading(this);
        APIManager.startRequest(blockChain.transferAccounts(toAddress, amount, checkNumber, encodePwd), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                if (isFinishing()) {
                    return;
                }
                CommitSuccessActivity.start(ConfirmActivity.this, toAddress);
                EventBus.getDefault().post(new TransferOutSuccess());
                finish();
            }

            @Override
            public void onComplete() {
                ToastUtil.hideLoading();
            }
        }, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (timer != null) {
            timer.cancel();
        }
    }

    public static void start(Context context, String toAddress, double transferCoin) {
        Intent intent = new Intent(context, ConfirmActivity.class);
        intent.putExtra("toAddress", toAddress);
        intent.putExtra("transferCoin", transferCoin);
        context.startActivity(intent);
    }
}
