package com.weiju.ccmall.module.pickUp.adapter;

import android.graphics.Color;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.PickUp;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

/**
 * @author chenyanming
 * @time 2019/8/1 on 17:43
 * @desc ${TODD}
 */
public class PickUpDetailAdapter extends BaseQuickAdapter<PickUp, BaseViewHolder> {

    private int selectIndex = -1;
    private boolean showUseNow = false;

    public PickUpDetailAdapter() {
        super(R.layout.item_pick_up_detail);
    }

    @Override
    protected void convert(BaseViewHolder helper, PickUp item) {
        FrescoUtil.setImage(helper.getView(R.id.ivAvatar), item.iconUrl);
        helper.setText(R.id.tvTitle, item.title);
        helper.setText(R.id.tvMoney, MoneyUtil.centToYuan¥StrNoZero(item.cost));
        helper.setText(R.id.tvGoodsCode, String.format("SN:%s", item.goodsCode));
        helper.setText(R.id.tvDate, String.format("过期时间:%s", item.expireDate));
        if (TextUtils.isEmpty(item.outMemberPhone)) {
            helper.setVisible(R.id.tvFrom, false);
        } else {
            helper.setVisible(R.id.tvFrom, true);
            helper.setText(R.id.tvFrom, item.outMemberPhone + " 赠送");
        }
        helper.setVisible(R.id.tvUse, showUseNow);
        helper.addOnClickListener(R.id.tvUse);

        if (item.isSelected) {
            helper.setBackgroundRes(R.id.ll_item_content, R.drawable.img_pick_up_item_bg2_selected);
            helper.setTextColor(R.id.tvGoodsCode, Color.WHITE);
            helper.setTextColor(R.id.tvDate, Color.WHITE);
            helper.setTextColor(R.id.tvFrom, Color.WHITE);
        } else {
            helper.setBackgroundRes(R.id.ll_item_content, R.drawable.img_pick_up_item_bg2);
            helper.setTextColor(R.id.tvGoodsCode, Color.parseColor("#666666"));
            helper.setTextColor(R.id.tvDate, mContext.getResources().getColor(R.color.text_black));
            helper.setTextColor(R.id.tvFrom, Color.parseColor("#666666"));
        }
    }

    public void setShowUseNow(boolean show) {
        this.showUseNow = show;
    }
}
