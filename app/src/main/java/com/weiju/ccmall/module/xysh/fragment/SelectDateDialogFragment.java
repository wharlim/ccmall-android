package com.weiju.ccmall.module.xysh.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.weiju.ccmall.R;

import org.threeten.bp.Clock;
import org.threeten.bp.Duration;
import org.threeten.bp.LocalDate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class SelectDateDialogFragment extends BottomSheetDialogFragment {

    @BindView(R.id.tvConfirm)
    TextView tvConfirm;
    @BindView(R.id.flBtnContainer)
    FrameLayout flBtnContainer;
    @BindView(R.id.calendarView)
    MaterialCalendarView calendarView;
    Unbinder unbinder;

    public static final int SELECTION_MODE_SINGLE = 1;
    public static final int SELECTION_MODE_MULTIPLE = 2;

    private int selectionMode = SELECTION_MODE_SINGLE;

    // 格式为 yyyy-MM-dd
    private Set<String> selectResult = new HashSet<>();

    private OnSelectListener mOnSelectListener;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_select_date, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        Bundle args = getArguments();
        if (args != null) {
            selectionMode = args.getInt("selectMode");
            String[] selectedDates = args.getStringArray("selectedDates");
            if (selectedDates != null) {
                Collections.addAll(selectResult, selectedDates);
            }
        }
        if (selectionMode == SELECTION_MODE_SINGLE) {
            flBtnContainer.setVisibility(View.GONE);
            calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_SINGLE);
        } else if (selectionMode == SELECTION_MODE_MULTIPLE) {
            flBtnContainer.setVisibility(View.VISIBLE);
            calendarView.setSelectionMode(MaterialCalendarView.SELECTION_MODE_MULTIPLE);
        } else {
            throw new IllegalArgumentException("selectMode不正确，必须是1或者2");
        }

        for (String dateStr :
                selectResult) {

            CalendarDay date = CalendarDay.from(LocalDate.parse(dateStr));
            calendarView.setDateSelected(date, true);
        }
        calendarView.setCurrentDate(CalendarDay.today());
        calendarView.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
//        calendarView.newState().setMinimumDate(LocalDate.now())
//                .commit();

        Date now = new Date();
//        now.setTime(now.getTime() + 7 * 3600 * 1000L);
        calendarView.newState().setMinimumDate(LocalDate.now(Clock.offset(Clock.systemDefaultZone(), Duration.ofMillis(7 * 3600 * 1000L))))
                .commit();

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {

                selectResult.clear();
                for (CalendarDay d :
                        calendarView.getSelectedDates()) {
                    String dateFormat = String.format("%04d-%02d-%02d", d.getYear(), d.getMonth(), d.getDay());
                    selectResult.add(dateFormat);
                }
                
                if (selectionMode == SELECTION_MODE_SINGLE) {
                    if (mOnSelectListener != null) {
                        mOnSelectListener.onSelect(selectResult);
                    }
                    dismiss();
                }
            }
        });
    }

    public void show(FragmentManager fm) {
        show(fm, "SelectDateDialogFragment");
    }

    public static SelectDateDialogFragment newInstance(int selectMode, Set<String> selectedDates) {
        SelectDateDialogFragment fragment = new SelectDateDialogFragment();
        Bundle args = new Bundle();
        args.putInt("selectMode", selectMode);
        if (selectedDates != null) {
            args.putStringArray("selectedDates", selectedDates.toArray(new String[]{}));
        }
        fragment.setArguments(args);
        return fragment;
    }

    public static SelectDateDialogFragment newInstanceWithSingle(String selectedDate) {
        Set<String> set = new HashSet<>();
        if (!TextUtils.isEmpty(selectedDate)) {
            set.add(selectedDate);
        }
        return newInstance(SELECTION_MODE_SINGLE, set);
    }

    public static SelectDateDialogFragment newInstanceWithMultiple(Set<String> selectedDates) {
        return newInstance(SELECTION_MODE_MULTIPLE, selectedDates);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.tvConfirm)
    public void onViewClicked() {
        if (mOnSelectListener != null) {
            mOnSelectListener.onSelect(selectResult);
        }
        dismiss();
    }

    public interface OnSelectListener {
        void onSelect(Set<String> ret);
    }

    public SelectDateDialogFragment setOnSelectListener(OnSelectListener onSelectListener) {
        mOnSelectListener = onSelectListener;
        return this;
    }
}
