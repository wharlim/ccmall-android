package com.weiju.ccmall.module.xysh.activity;

import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.adapter.HelpAdapter;
import com.weiju.ccmall.shared.basic.BaseListActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenyanming
 * @time 2019/12/16 on 14:10
 * @desc
 */
public class HelpListActivity extends BaseListActivity {
    private HelpAdapter mAdapter = new HelpAdapter();

    @Override
    public void initView() {
        super.initView();
        View footerView = getLayoutInflater().inflate(R.layout.layout_help_footer, mRecyclerView, false);
        mAdapter.addFooterView(footerView);

    }

    @Override
    public String getTitleStr() {
        return "帮助中心";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        Intent intent = new Intent(HelpListActivity.this,HelpDetailActivity.class);
        startActivity(intent);
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        mRefreshLayout.setRefreshing(false);
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            list.add("关于信用生活");
        }
        mAdapter.setNewData(list);
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public boolean isLoadMore() {
        return false;
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_help_list;
    }
}
