package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReceiptOrder implements Serializable {

    /**
     * orderId : 2019122116385177900013
     * tmSmp : 2019-12-21T08:38:52.000+0000
     * payOrderId : Q725878969122951168
     * repayOrderId : null
     * channelNo : GftQPay
     * usrNo : 1817b784802443d4b28b171bb03436ed
     * oemId : ccpay
     * txnMercNm : M2019122100509569
     * tranType : null
     * orderStatus : 5
     * planNo : null
     * txnAmt : 100
     * repayAmt : 97.32
     * txnRate : 0.68
     * feeAmt : 2.68
     * txnCnt : 0
     * executeDate : 2019-12-20T16:00:00.000+0000
     * executeTime : null
     * payTime : 2019-12-21T08:38:52.000+0000
     * repayTime : null
     * txnCardNo : 6225780604948706
     * clrCardNo : 6214831200670559
     * retCode : 0
     * retMsg : success
     * stlType : null
     * rebSts : 0
     * capAmt : 0
     * orgFeeAmt : 0
     * checkStatus : null
     * remark : GftQPay
     */

    @SerializedName("orderId")
    public String orderId;
    @SerializedName("tmSmp")
    public String tmSmp;
    @SerializedName("payOrderId")
    public String payOrderId;
    @SerializedName("repayOrderId")
    public Object repayOrderId;
    @SerializedName("channelNo")
    public String channelNo;
    @SerializedName("usrNo")
    public String usrNo;
    @SerializedName("oemId")
    public String oemId;
    @SerializedName("txnMercNm")
    public String txnMercNm;
    @SerializedName("tranType")
    public Object tranType;
    @SerializedName("orderStatus")
    public int orderStatus;
    @SerializedName("planNo")
    public Object planNo;
    @SerializedName("txnAmt")
    public double txnAmt;
    @SerializedName("repayAmt")
    public double repayAmt;
    @SerializedName("txnRate")
    public double txnRate;
    @SerializedName("feeAmt")
    public double feeAmt;
    @SerializedName("txnCnt")
    public int txnCnt;
    @SerializedName("executeDate")
    public String executeDate;
    @SerializedName("executeTime")
    public Object executeTime;
    @SerializedName("payTime")
    public String payTime;
    @SerializedName("repayTime")
    public Object repayTime;
    @SerializedName("txnCardNo")
    public String txnCardNo;
    @SerializedName("clrCardNo")
    public String clrCardNo;
    @SerializedName("retCode")
    public String retCode;
    @SerializedName("retMsg")
    public String retMsg;
    @SerializedName("stlType")
    public Object stlType;
    @SerializedName("rebSts")
    public String rebSts;
    @SerializedName("capAmt")
    public int capAmt;
    @SerializedName("orgFeeAmt")
    public int orgFeeAmt;
    @SerializedName("checkStatus")
    public Object checkStatus;
    @SerializedName("remark")
    public String remark;

    public String getOrderStatus() {
        if (orderStatus == 5 || orderStatus == 1) {
            return "进行中";
        } else if (orderStatus == 6) {
            return "失败";
        } else if (orderStatus == 7) {
            return "成功";
        } else {
            return "";
        }
    }
}
