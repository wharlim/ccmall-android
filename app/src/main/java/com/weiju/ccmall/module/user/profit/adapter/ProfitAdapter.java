package com.weiju.ccmall.module.user.profit.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Balance;
import com.weiju.ccmall.shared.util.ConvertUtil;

import java.util.Locale;

/**
 * @author chenyanmin
 * @date 2019/6/22
 */
public class ProfitAdapter extends BaseQuickAdapter<Balance, BaseViewHolder> {
    public ProfitAdapter() {
        super(R.layout.item_balance);
    }

    @Override
    protected void convert(BaseViewHolder helper, Balance item) {
        helper.setText(R.id.itemTimeTv, String.format("时间：%s", item.createDate));
        helper.setText(R.id.itemTypeTv, String.format("类型：%s", item.typeStr));
        helper.setText(R.id.itemStatusTv, String.format("状态：%s", item.statusStr));
        helper.setText(R.id.itemProfitTv, item.freezeProfitMoney < 0 ? ConvertUtil.cent2yuanNoZero(item.freezeProfitMoney) :
                String.format(Locale.CHINA, "+%s", ConvertUtil.cent2yuanNoZero(item.freezeProfitMoney)));
        helper.setTextColor(R.id.itemProfitTv,mContext.getResources().getColor(item.freezeProfitMoney < 0? R.color.green:R.color.red));
    }
}
