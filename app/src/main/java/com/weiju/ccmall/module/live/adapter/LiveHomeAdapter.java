package com.weiju.ccmall.module.live.adapter;

import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blankj.utilcode.utils.SizeUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Picasso;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.util.FrescoUtil;

/**
 * @author chenyanming
 * @time 2019/11/21 on 11:55
 * @desc ${TODD}
 */
public class LiveHomeAdapter extends BaseQuickAdapter<LiveRoom, BaseViewHolder> {

    public LiveHomeAdapter() {
        super(R.layout.item_live_home);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveRoom item) {
        SimpleDraweeView view = helper.getView(R.id.ivAvatar);
        RequestOptions options = new RequestOptions().placeholder(R.drawable.default_image).error(R.drawable.default_image);
        Glide.with(mContext).load(item.liveImage).apply(options).into(view);
        CardView layoutProduct = helper.getView(R.id.layoutLive);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) layoutProduct.getLayoutParams();
        if (helper.getPosition() % 2 == 0) {
            //右边
            layoutParams.rightMargin = SizeUtils.dp2px(10);
            layoutParams.leftMargin = SizeUtils.dp2px(5);

        } else {
            // 左边
            layoutParams.rightMargin = SizeUtils.dp2px(5);
            layoutParams.leftMargin = SizeUtils.dp2px(10);
        }
        layoutParams.bottomMargin = SizeUtils.dp2px(10);
        layoutProduct.setLayoutParams(layoutParams);

//        helper.setVisible(R.id.ivLiving, item.status == 1);
        helper.setText(R.id.tvOnLive, item.popularity + "");
        helper.setText(R.id.tvNickName, item.nickName);
        helper.setVisible(R.id.llOnlineView, item.status == 1 || item.status == 3);
        helper.setText(R.id.tvTitle, item.title);
        // 直播状态
        if (item.status == 1 || item.status == 3 || item.status == 4) {
            helper.setVisible(R.id.tvLiveState, true);
            helper.setText(R.id.tvLiveState, item.getStatusStr());
            helper.setBackgroundRes(R.id.tvLiveState, item.getStatusBg());
        } else {
            helper.setVisible(R.id.tvLiveState, false);
        }
    }


}
