package com.weiju.ccmall.module.xysh.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.AuthChannelActivity;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class AuthChannelDialogFragment extends DialogFragment {

    @BindView(R.id.iv_channel_icon)
    SimpleDraweeView ivChannelIcon;
    @BindView(R.id.tv_channel_name)
    TextView tvChannelName;
    Unbinder unbinder;

    private int authType = AuthChannelActivity.AUTH_TYPE_REPAY;

    QueryUserBankCardResult.BankInfListBean creditCard;
    QueryUserBankCardResult.BankInfListBean debitCard;
    ChannelItem channel;
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        Bundle args = getArguments();
        authType = args.getInt("authType", AuthChannelActivity.AUTH_TYPE_REPAY);
        creditCard = (QueryUserBankCardResult.BankInfListBean) args.getSerializable("creditCard");
        debitCard = (QueryUserBankCardResult.BankInfListBean) args.getSerializable("debitCard");
        channel = (ChannelItem) args.getSerializable("channel");

        View view = inflater.inflate(R.layout.fragment_auth_channel, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    private void initView() {
        tvChannelName.setText(channel.channelName);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.iv_close, R.id.tv_auth_now})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_close:
                dismiss();
                break;
            case R.id.tv_auth_now:
                if (authType == AuthChannelActivity.AUTH_TYPE_REPAY) {
                    versionCheck(getContext());
                } else if (authType == AuthChannelActivity.AUTH_TYPE_RECEIPT) {
                    AuthChannelActivity.start(getContext(), creditCard, debitCard, channel);
                }
                dismiss();
                break;
        }
    }

    private void versionCheck(Context context){
        ToastUtil.showLoading(context);
        APIManager.startRequest(service.versionCheck(channel.channelShortName), new Observer<XYSHCommonResult<Object>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<Object> ret) {
                ToastUtil.hideLoading();
                if (ret.code == 1) {
                    AuthChannelActivity.start(context, creditCard, channel);
                } else {
                    ToastUtil.error(ret.message);
                }
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.hideLoading();
                ToastUtil.error(e.getMessage());
            }

            @Override
            public void onComplete() {
                ToastUtil.hideLoading();
            }
        });
    }

    public AuthChannelDialogFragment show(FragmentManager fm) {
        show(fm, "AuthChannelDialogFragment");
        return this;
    }

    public static AuthChannelDialogFragment newInstance(
            int authType,
            QueryUserBankCardResult.BankInfListBean creditCard,
            QueryUserBankCardResult.BankInfListBean debitCard,
            ChannelItem channel) {
        AuthChannelDialogFragment fragment = new AuthChannelDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable("creditCard", creditCard);
        args.putSerializable("channel", channel);
        args.putInt("authType", authType);
        if (debitCard != null) {
            args.putSerializable("debitCard", debitCard);
        }
        fragment.setArguments(args);
        return fragment;
    }
}
