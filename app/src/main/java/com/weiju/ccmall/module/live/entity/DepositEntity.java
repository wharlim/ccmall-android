package com.weiju.ccmall.module.live.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/4/18.
 */
public class DepositEntity {

    /**
     * orderId : 2afed951370a4ba98ff49008401849e0
     * orderCode : 0211587433234688
     */

    @SerializedName("orderId")
    public String orderId;
    @SerializedName("orderCode")
    public String orderCode;
}
