package com.weiju.ccmall.module.blockchain.beans;

import com.google.gson.annotations.SerializedName;

public class BlockChainInfo {

    /**
     * serviceFee : 0.08
     * bi : 0
     * accountCcm : {"accountId":"","contribution":0,"sumCcm":0,"availableRate":0,"memberId":""}
     */

    @SerializedName("serviceFee")
    public double serviceFee;
    @SerializedName("bi")
    public double bi;
    @SerializedName("address")
    public String address;
    @SerializedName("accountCcm")
    public AccountCcmBean accountCcm;
    @SerializedName("isForbiddenTransferCCM")
    public boolean isForbiddenTransferCCM;

    public static class AccountCcmBean {
        /**
         * accountId :
         * contribution : 0
         * sumCcm : 0
         * availableRate : 0
         * memberId :
         */

        @SerializedName("accountId")
        public String accountId;
        @SerializedName("contribution")
        public double contribution;
        @SerializedName("sumCcm")
        public double sumCcm;
        @SerializedName("availableRate")
        public double availableRate;
        @SerializedName("memberId")
        public String memberId;
    }
}
