package com.weiju.ccmall.module.world.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.world.IWorldService;
import com.weiju.ccmall.module.world.activity.WorldOrderDetailActivity;
import com.weiju.ccmall.module.world.activity.WorldProductDetailActivity;
import com.weiju.ccmall.module.world.adapter.WorldOrderListAdapter;
import com.weiju.ccmall.module.world.entity.OrderEntity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.OrderService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static com.weiju.ccmall.shared.Constants.PAGE_SIZE;

public class WorldOrderListFragment extends BaseListFragment {

    private List<OrderEntity> mDatas = new ArrayList<>();
    private WorldOrderListAdapter mAdapter = new WorldOrderListAdapter(mDatas);
    private IWorldService mService = ServiceManager.getInstance().createService(IWorldService.class);

    private int mCurrentPage = 1;
    private Page mPage;

    public static WorldOrderListFragment newInstance(Page page) {
        Bundle args = new Bundle();
        args.putSerializable("page", page);
        WorldOrderListFragment fragment = new WorldOrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mPage = (Page) getArguments().get("page");
    }

    @Override
    public void initView() {
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.bg_gray));
        super.initView();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) getData(true);
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        OrderEntity orderEntity = mDatas.get(position);
        switch (view.getId()) {
            case R.id.tvCheckLogistics:
                OrderService.checkExpress(getContext(), orderEntity.expressCode, orderEntity.expressId);
                break;
            case R.id.tvConfirmReceipt:
                OrderService.finishWorldOrder(getActivity(), orderEntity.merchantOrderNo);
                break;
            case R.id.tvBuyAgain:
                WorldProductDetailActivity.start(getContext(), orderEntity.itemId);
                break;
            default:
                WorldOrderDetailActivity.start(getContext(), orderEntity.merchantOrderNo);
                break;
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        if (mPage == null || !getUserVisibleHint()) {
            return;
        }
        if (isRefresh) {
            mCurrentPage = 1;
        } else {
            mCurrentPage++;
        }
        getOrderList(isRefresh);
    }

    private void getOrderList(boolean isRefresh) {
        Observable<RequestResult<PaginationEntity<OrderEntity, Object>>> observable = TextUtils.isEmpty(mPage.id) ? mService.getAllOrderList(mCurrentPage, PAGE_SIZE)
                : mService.getOrderList(mPage.id, mCurrentPage, PAGE_SIZE);
        APIManager.startRequest(observable, new BaseRequestListener<PaginationEntity<OrderEntity, Object>>(mRefreshLayout) {
            @Override
            public void onSuccess(PaginationEntity<OrderEntity, Object> result) {
                super.onSuccess(result);
                if (isRefresh) {
                    mDatas.clear();
                }
                mDatas.addAll(result.list);
                mAdapter.notifyDataSetChanged();
                if (result.list.size() < Constants.PAGE_SIZE) {
                    mAdapter.loadMoreEnd();
                } else {
                    mAdapter.loadMoreComplete();
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mAdapter.loadMoreFail();
            }

        }, getContext());

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
        if (message.getEvent().equals(Event.finishOrder)) {
            if (mAdapter == null) {
                return;
            }
            getData(true);
        }
    }

}
