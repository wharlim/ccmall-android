package com.weiju.ccmall.module.blockchain.utils;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;

public class TemplateWebUtil {

    private static final String TAG = "TemplateWebUtil";
    public static boolean loadChart(WebView webView, String templateId, String params, String name) {
        try {
            setUpWebView(webView);
            File tmpDir = new File(getDownloadDir(webView.getContext()), templateId);

            webView.loadDataWithBaseURL(
                    "file://" + tmpDir.getAbsolutePath() + "/",
                    loadStringFromFile(new File(tmpDir.getAbsolutePath() + "/chart.html")),
                    "text/html",
                    "utf-8",
                    null
            );
            Log.e(TAG, "loadChart , " + params);
            new Handler().postDelayed(() -> {
                webView.loadUrl("javascript:loadData("+params+", '" + name + "')");
            }, 500);
            return true;
        } catch (Exception e) {
            Log.e(TAG, "loadChart error, " + e.getMessage());
            return false;
        }
    }

    public static File getDownloadDir(Context context) {
        return new File(context.getFilesDir(), "template");
    }


    private static String loadStringFromFile(File f){
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            byte[] data = new byte[fis.available()];
            fis.read(data);
            fis.close();
            return new String(data, StandardCharsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void setUpWebView(WebView webView) {
        WebSettings settings = webView.getSettings();
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccess(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowContentAccess(true);
        settings.setDomStorageEnabled(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);//支持内容重新布局
    }
}
