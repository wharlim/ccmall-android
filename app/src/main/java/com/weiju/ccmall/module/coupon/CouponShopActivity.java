package com.weiju.ccmall.module.coupon;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;

/**
 * 某一张优惠券可用的店铺列表
 */
public class CouponShopActivity extends BaseActivity {
    // 优惠券id
    public static final String KEY_COUPON_ID = "couponId";
    private String mCouponId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_shop);
        setTitle("店铺满减");
        setLeftBlack();
        mCouponId = getIntent().getStringExtra(KEY_COUPON_ID);
        initCouponShopFragment();
    }

    private void initCouponShopFragment() {
        CouponShopFragment fragment = CouponShopFragment.newInstance(mCouponId);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.fl_container, fragment).commit();
    }

    public static void start(Context context, String couponId) {
        Intent intent = new Intent(context, CouponShopActivity.class);
        intent.putExtra(KEY_COUPON_ID, couponId);
        context.startActivity(intent);
    }
}
