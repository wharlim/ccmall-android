package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.bean.ApplyCreditCardItem;
import com.weiju.ccmall.module.xysh.bean.ApplyCreditCardUrlResult;
import com.weiju.ccmall.module.xysh.bean.ApplyCreditCommonResult;
import com.weiju.ccmall.module.xysh.bean.SelectSupportBankItem;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * @author chenyanming
 * @time 2019/12/18 on 11:04
 * @desc
 */
public class ApplyCreaditCardActivity extends BaseActivity {

    @BindView(R.id.etPhone)
    EditText mEtPhone;
    @BindView(R.id.etIdCard)
    EditText mEtIdCard;
    @BindView(R.id.etName)
    EditText mEtName;
    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    User loginUser;
    String code;
    ApplyCreditCardItem mApplyCreditCardItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_creadit_card);
        ButterKnife.bind(this);
        code = getIntent().getStringExtra("code");
        mApplyCreditCardItem = (ApplyCreditCardItem) getIntent().getSerializableExtra("ApplyCreditCardItem");
        loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null) {
            mEtPhone.setText(loginUser.phone);
            mEtIdCard.setText(loginUser.identityCard);
            mEtName.setText(loginUser.userName);
        } else {
            ToastUtil.error("请先登录!");
            finish();
        }
        initView();
    }

    public static void start(Context context, String code, ApplyCreditCardItem item) {
        Intent intent = new Intent(context, ApplyCreaditCardActivity.class);
        intent.putExtra("code", code);
        intent.putExtra("ApplyCreditCardItem", item);
        context.startActivity(intent);
    }

    private void initView() {
        setTitle("申请信用卡");
        setLeftBlack();
    }


    @OnClick(R.id.tvAdd)
    protected void add() {
        if (TextUtils.isEmpty(mEtPhone.getText().toString().trim())) {
            ToastUtil.error("请输入手机号");
            return;
        }
        if (TextUtils.isEmpty(mEtIdCard.getText().toString().trim())) {
            ToastUtil.error("请输入身份证号码");
            return;
        }

        if (TextUtils.isEmpty(mEtName.getText().toString().trim())) {
            ToastUtil.error("请输入身份证上姓名");
            return;
        }

        applyCredit();
    }

    private void applyCredit() {
        String idCard = mEtIdCard.getText().toString().trim();
        String phone = mEtPhone.getText().toString().trim();
        String name = mEtName.getText().toString().trim();
        APIManager.startRequest(service.applyCreditCard(
                BankUtils.sAppToken,
                loginUser.phone,
                code,
                mApplyCreditCardItem.name,
                "CCPAY",
                idCard,
                phone,
                name,
                mApplyCreditCardItem.stationBankCardChannelId,
                mApplyCreditCardItem.stationChannelId
        ), new Observer<ApplyCreditCommonResult<List<ApplyCreditCardUrlResult>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ApplyCreditCommonResult<List<ApplyCreditCardUrlResult>> ret) {
                if (ret.isSuccess()) {
                    if (!ret.result.isEmpty()) {
                        Intent intent = new Intent(ApplyCreaditCardActivity.this, WebViewJavaActivity.class);
                        intent.putExtra("url", ret.result.get(0).url);
                        startActivity(intent);
                    }
                } else {
                    ToastUtil.error(ret.message+"");
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
}
