package com.weiju.ccmall.module.transfer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.SubmitStatusActivity;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.VouchersTransfer;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.CaptchaBtn;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.enums.AccountType;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICaptchaService;
import com.weiju.ccmall.shared.service.contract.IPickUpService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.transfer
 * @since 2017-08-03
 */
public class StepThirdActivity extends BaseActivity {

    @BindView(R.id.tvPhone)
    TextView mTvPhone;
    @BindView(R.id.captchaEt)
    EditText mCaptchaEt;
    @BindView(R.id.captchaBtn)
    CaptchaBtn mCaptchaBtn;
    @BindView(R.id.passwordEt)
    EditText mPasswordEt;
    @BindView(R.id.confirmBtn)
    TextView mConfirmBtn;
    private User mPayee;
    private double mMoney;
    private String mRemark;
    private User mUser;
    private ICaptchaService mCaptchaService;
    private IUserService mUserService;
    private IPickUpService mPickUpService;
    private AccountType mAccountType;
    private int mType;
    private String mGoodsCodes;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avtivity_transfer_step_third);
        ButterKnife.bind(this);
        showHeader();
        setLeftBlack();
        getIntentData();
        setData();
        initViewByType();
    }

    private void setData() {
        mCaptchaService = ServiceManager.getInstance().createService(ICaptchaService.class);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        mPickUpService = ServiceManager.getInstance().createService(IPickUpService.class);

        mUser = SessionUtil.getInstance().getLoginUser();
        StringBuilder phoneSB = new StringBuilder(mUser.phone);
        String phone = phoneSB.replace(3, 7, "****").toString();
        mTvPhone.setText(phone);
    }

    private void getIntentData() {
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            mType = intent.getExtras().getInt(Config.INTENT_KEY_TYPE_NAME, AppTypes.TRANSFER.MONEY);
            mPayee = (User) intent.getExtras().getSerializable("payee");
            mMoney = intent.getExtras().getDouble("money");
            mRemark = intent.getExtras().getString("remark", "");
            mGoodsCodes = intent.getExtras().getString("goodsCodes", "");
        }
        if (mPayee == null) {
            ToastUtil.error("参数错误");
            finish();
        }
        mAccountType = (AccountType) getIntent().getSerializableExtra("AccountType");
    }

    private void initViewByType() {
        if (mType == AppTypes.TRANSFER.MONEY) {
            setTitle("转账");
        } else if (mType == AppTypes.TRANSFER.PICKUP_COUPON) {
            setTitle("提货券赠送");
            mCaptchaBtn.setBackgroundResource(R.drawable.btn_pickup_common);
            mConfirmBtn.setBackgroundResource(R.drawable.btn_pickup_common);
            mConfirmBtn.setText("确认赠送");
        }
    }

    private void doTransfer() {
        APIManager.startRequest(
                mUserService.doTransfer(
                        mPayee.phone,
                        Long.valueOf((long) ((mMoney + 0.005) * 100)),
                        mRemark,
                        mPasswordEt.getText().toString(),
                        mCaptchaEt.getText().toString()
                ), new BaseRequestListener<Object>(this) {

                    @Override
                    public void onSuccess(Object result) {
                        finish();

                        Intent intent = new Intent(StepThirdActivity.this, SubmitStatusActivity.class);
                        startActivity(intent);
                        MsgStatus msgStatus = new MsgStatus(AppTypes.TRANSFER.TRANSFER_MONEY_SUCESS);
                        msgStatus.setTips(mPayee.nickname + "已收到你的转账");
                        msgStatus.setMoney(mMoney);
                        EventBus.getDefault().postSticky(msgStatus);

                        EventBus.getDefault().post(new EventMessage(Event.transferSuccess));
                    }
                },this
        );
    }

    private void doTransferShopMoney() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("transferMoney", Long.valueOf((long) ((mMoney + 0.005) * 100)));
        params.put("password", mPasswordEt.getText().toString());
        params.put("checkNumber", mCaptchaEt.getText().toString());
        params.put("trsMemo", mRemark);
        params.put("transferFrom", 1);
        params.put("payeeMemberId", mPayee.id);
        APIManager.startRequest(
                mUserService.transferGoldTransfer(APIManager.buildJsonBody(params))
                ,
                new BaseRequestListener<Object>(this) {
                    @Override
                    public void onSuccess(Object result) {
                        finish();

                        Intent intent = new Intent(StepThirdActivity.this, SubmitStatusActivity.class);
                        startActivity(intent);
                        MsgStatus msgStatus = new MsgStatus(AppTypes.TRANSFER.TRANSFER_MONEY_SUCESS);
                        msgStatus.setTips(mPayee.nickname + "已收到你的转账");
                        msgStatus.setMoney(mMoney);
                        EventBus.getDefault().postSticky(msgStatus);

                        EventBus.getDefault().post(new EventMessage(Event.transferSuccess));

                    }
                },this
        );
    }

    private void doTransferPickUpCoupon() {
        APIManager.startRequest(
                mPickUpService.transfer(
                        mPayee.phone,
                        mPasswordEt.getText().toString(),
                        mCaptchaEt.getText().toString(),
                        mGoodsCodes
                ), new BaseRequestListener<VouchersTransfer>(this) {

                    @Override
                    public void onSuccess(VouchersTransfer result) {
                        finish();
                        Intent intent = new Intent(StepThirdActivity.this, SubmitStatusActivity.class);
                        startActivity(intent);
                        MsgStatus msgStatus = new MsgStatus(AppTypes.TRANSFER.PICKUP_COUPON);
                        if (result.data.successCount > 0) {
                            msgStatus.setTips(mPayee.nickname + "已收到你赠送的" + result.data.successCount + "张提货劵");
                        } else {
                            msgStatus.setTips("提货劵赠送失败，请稍后重新赠送");
                        }
                        msgStatus.setVouchersTransfer(result);
                        EventBus.getDefault().postSticky(msgStatus);

                        EventBus.getDefault().post(new EventMessage(Event.transferSuccess));
                    }
                },this
        );
    }

    @OnClick(R.id.captchaBtn)
    protected void getCaptcha() {
        String token = StringUtil.md5(Constants.API_SALT + mUser.phone);
        APIManager.startRequest(mCaptchaService.getCaptchaForCheck(token, mUser.phone), new BaseRequestListener<Object>(this) {
            @Override
            public void onSuccess(Object result) {
                mCaptchaBtn.start();
            }
        },this);
    }

    @OnClick(R.id.confirmBtn)
    public void confirm() {
        String captcha = mCaptchaEt.getText().toString();
        if (Strings.isNullOrEmpty(captcha)) {
            ToastUtil.error("请输入验证码");
            mCaptchaEt.requestFocus();
            return;
        }
        String password = mPasswordEt.getText().toString();
        if (Strings.isNullOrEmpty(password)) {
            ToastUtil.error("请输入密码");
            mPasswordEt.requestFocus();
            return;

        }
        if (mAccountType.equals(AccountType.Balance)) {
            doTransfer();
        } else if (mAccountType.equals(AccountType.ShopMoney)) {
            doTransferShopMoney();
        } else if (mAccountType.equals(AccountType.PickUpCoupon)) {
            doTransferPickUpCoupon();
        }
    }


}
