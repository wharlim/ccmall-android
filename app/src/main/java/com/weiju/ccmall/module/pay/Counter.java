package com.weiju.ccmall.module.pay;

import android.util.Log;

/**
 * 限流策略控制类
 */
public class Counter {

    private static final String TAG = "Counter";

    private long requestTime;

    private Counter(long reqTime) {
        this.requestTime = reqTime;
    }

    public static Counter startReq() {
        return new Counter(System.currentTimeMillis());
    }

    public boolean shouldReq() {
        long delta = System.currentTimeMillis() - this.requestTime;
        Log.d(TAG, "deltaTime -> " + delta);
        return delta > 1000;
    }
}
