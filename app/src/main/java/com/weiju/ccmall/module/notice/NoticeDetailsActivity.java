package com.weiju.ccmall.module.notice;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.NoticeDetailsModel;
import com.weiju.ccmall.shared.bean.RulesModel;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.INotesService;
import com.weiju.ccmall.shared.util.WebViewUtil;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NoticeDetailsActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView mWebView;
    @BindView(R.id.tvData)
    TextView mTvData;
    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    private String mId;
    private boolean mIsRules;
    private boolean mIsPrivacyPolicy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_details);
        ButterKnife.bind(this);
        getIntentData();
        initView();
        setData();
    }

    private void setData() {
        INotesService service = ServiceManager.getInstance().createService(INotesService.class);
        if (mIsRules) {
            setTitle("注册协议");
            APIManager.startRequest(
                    service.getRules(),
                    new BaseRequestListener<RulesModel>(this) {
                        @Override
                        public void onSuccess(RulesModel result) {
                            mTvTitle.setVisibility(View.GONE);
                            mTvData.setVisibility(View.GONE);
                            WebViewUtil.loadDataToWebView(mWebView, result.html);
                        }
                    },this
            );
        } else if (mIsPrivacyPolicy) {
            setTitle("隐私政策");
            APIManager.startRequest(service.getPrivacyAgreement(),
                    new BaseRequestListener<RulesModel>(this) {
                        @Override
                        public void onSuccess(RulesModel result) {
                            mTvTitle.setVisibility(View.GONE);
                            mTvData.setVisibility(View.GONE);
                            WebViewUtil.loadDataToWebView(mWebView, result.html);
                        }
                    }, this);
        } else {
            APIManager.startRequest(
                    service.getNotes(mId),
                    new BaseRequestListener<NoticeDetailsModel>(this) {
                        @Override
                        public void onSuccess(NoticeDetailsModel result) {
                            mTvTitle.setText(result.title);
                            mTvData.setText(result.createDate);
                            WebViewUtil.loadDataToWebView(mWebView, result.content);
                        }
                    },this
            );
        }

    }

    private void initView() {
        setTitle("公告详情");
        setLeftBlack();
    }

    private void getIntentData() {
        mId = getIntent().getStringExtra("id");
        mIsRules = getIntent().getBooleanExtra("isRules", false);
        mIsPrivacyPolicy = getIntent().getBooleanExtra("isPrivacyPolicy", false);
    }

}
