package com.weiju.ccmall.module.challenge.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.challenge.manager.ChallengeManager;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Challenge;
import com.weiju.ccmall.shared.bean.ScoreModel;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.EditChallengePKScoreDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.contracts.OnValueChangeLister;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IChallengeService;
import com.weiju.ccmall.shared.service.contract.IPointService;
import com.weiju.ccmall.shared.util.CountDownRxUtils;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/6/26 on 17:12
 * @desc pk详情
 */
public class ChallengePKDetailActivity extends BaseActivity {

    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.tvTime)
    TextView mTvTime;
    @BindView(R.id.tvCancelChallenge)
    TextView mTvChallenge;
    @BindView(R.id.ivChallengeBg)
    ImageView mIvChallengeBg;
    @BindView(R.id.layoutPkResult)
    LinearLayout mLayoutPkResult;
    @BindView(R.id.ivBg)
    ImageView mIvBg;
    @BindView(R.id.layoutPk)
    RelativeLayout mLayoutPk;
    @BindView(R.id.layoutChallenge)
    FrameLayout mLayoutChallenge;
    @BindView(R.id.ivMyAvatar)
    SimpleDraweeView mIvMyAvatar;
    @BindView(R.id.tvMyName)
    TextView mTvMyName;
    @BindView(R.id.tvMyScore)
    TextView mTvMyScore;
    @BindView(R.id.ivPKAvatar)
    SimpleDraweeView mIvPKAvatar;
    @BindView(R.id.tvPkName)
    TextView mTvPkName;
    @BindView(R.id.tvPkScore)
    TextView mTvPkScore;
    @BindView(R.id.ivWinnAvatar)
    SimpleDraweeView mIvWinnAvatar;
    @BindView(R.id.tvWinnName)
    TextView mTvWinnName;
    @BindView(R.id.tvWinnScore)
    TextView mTvWinnScore;
    @BindView(R.id.layoutWinner)
    RelativeLayout mLayoutWinner;
    @BindView(R.id.tvPkType)
    TextView mTvPkType;
    @BindView(R.id.tvPkStartTime)
    TextView mTvPkStartTime;
    @BindView(R.id.tvPkEndTime)
    TextView mTvPkEndTime;
    @BindView(R.id.tvPkGetScore)
    TextView mTvPkGetScore;
    @BindView(R.id.layoutDraw)
    FrameLayout mLayoutDraw;
    @BindView(R.id.ivNoUser)
    ImageView mIvNoUser;
    @BindView(R.id.layoutPKUser)
    LinearLayout mLayoutPKUser;
    @BindView(R.id.tvNoUser)
    TextView mTvNoUser;

    private String mActivityId;

    private IChallengeService mService = ServiceManager.getInstance().createService(IChallengeService.class);
    private Challenge mChallenge;
    private EditChallengePKScoreDialog mEditChallengePKScoreDialog;
    private ScoreModel mScoreModel;
    private int mMinScore;
    private int mMaxScore;
    private boolean mIsHis;
    private String mChallengeId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_pk_detail);
        ButterKnife.bind(this);
        initIntent();
        initView();
        initData();
        EventBus.getDefault().register(this);

    }

    private void initIntent() {
        mActivityId = getIntent().getStringExtra("activityId");
        mMinScore = getIntent().getIntExtra("minScore", 10);
        mMaxScore = getIntent().getIntExtra("maxScore", 200);
        mIsHis = getIntent().getBooleanExtra("isHis", false);
        mChallengeId = getIntent().getStringExtra("challengeId");
    }

    private void initData() {
        IPointService pointService = ServiceManager.getInstance().createService(IPointService.class);
        ToastUtil.showLoading(ChallengePKDetailActivity.this);
        if (mIsHis) {
            APIManager.startRequest(mService.getChallenge(mChallengeId),
                    new BaseRequestListener<Challenge>(this) {
                        @Override
                        public void onSuccess(Challenge result) {
                            mChallenge = result;
                            setData();
                        }
                    },this);
        } else {
            APIManager.startRequest(pointService.getScore(), new BaseRequestListener<ScoreModel>() {
                @Override
                public void onSuccess(ScoreModel result) {
                    super.onSuccess(result);
                    mScoreModel = result;
                }
            },this);
            APIManager.startRequest(mService.checkChallenge(mActivityId),
                    new BaseRequestListener<Challenge>(this) {
                        @Override
                        public void onSuccess(Challenge result) {
                            mChallenge = result;
                            setData();
                        }
                    },this);
        }
    }

    private void setData() {
        if (mChallenge.activityStatus != 1 && !mIsHis) {
            // 还没开始挑战，挑战成功，挑战失败
            mTvTime.setText("发起挑战");
            mLayoutChallenge.setVisibility(View.VISIBLE);
            mTvTitle.setText(getResources().getString(R.string.challeng_item_pk_tip, mChallenge.lifeCycle, mChallenge.getTypeString(), "奖励"));
        } else if (mIsHis) {
            // 从历史记录进来，挑战完成
            if (mChallenge.pkStatus == 2 && !TextUtils.isEmpty(mChallenge.pkDetail.memberId) &&
                    !TextUtils.isEmpty(mChallenge.pkDetail.pkMemberId) && TextUtils.isEmpty(mChallenge.pkDetail.winMemberId)) {
                //平局
                mLayoutDraw.setVisibility(View.VISIBLE);
            } else if (!TextUtils.isEmpty(mChallenge.pkDetail.memberId) && TextUtils.isEmpty(mChallenge.pkDetail.pkMemberId)) {
                //无人挑战
                mTvPkGetScore.setVisibility(View.GONE);
                mIvNoUser.setVisibility(View.VISIBLE);
                mLayoutPKUser.setVisibility(View.GONE);
                mTvNoUser.setVisibility(View.VISIBLE);
            } else {
                //有胜出者
                mLayoutWinner.setVisibility(View.VISIBLE);
                boolean isWinner = mChallenge.pkDetail.winMemberId.equals(mChallenge.pkDetail.memberId);
                FrescoUtil.setImage(mIvWinnAvatar, isWinner ? mChallenge.pkDetail.headImage : mChallenge.pkDetail.pkHeadImage);
                mTvWinnName.setText(isWinner ? mChallenge.pkDetail.nickName : mChallenge.pkDetail.nickName);
                int score = isWinner ? mChallenge.pkDetail.endPoint - mChallenge.pkDetail.startPoint
                        : mChallenge.pkDetail.pkEndPoint - mChallenge.pkDetail.pkStartPoint;
                mTvWinnScore.setText(mChallenge.type == 2 || mChallenge.type == 3 ? MoneyUtil.centToYuanStrNoZero(score) : String.valueOf(score));
            }
            setPkData();
            mLayoutPk.setVisibility(View.VISIBLE);
            mTvTitle.setVisibility(View.GONE);
            mTvPkType.setText(String.format("巅峰对决：%s贡献值较高获胜", mChallenge.getTypeString()));
            mTvPkStartTime.setText(String.format("开始时间：%s", mChallenge.startDate));
            mTvPkEndTime.setText(String.format("结束时间：%s", mChallenge.endDate));
            mTvPkGetScore.setText(Html.fromHtml(String.format("获胜奖励：获得<font color =\"#ffac25\">%s</font>幸运积分", mChallenge.pkDetail.pkScore * 2)));
        } else if (mChallenge.pkStatus == 0) {
            //开启挑战，无人应战
            mLayoutChallenge.setVisibility(View.VISIBLE);
            mTvChallenge.setVisibility(View.VISIBLE);
            mTvTime.setText("等待挑战");
            mTvTitle.setText(getResources().getString(R.string.challeng_item_pk_tip,
                    mChallenge.lifeCycle, mChallenge.getTypeString(), String.format("%s幸运积分", mChallenge.pkDetail.pkScore * 2)));
        } else if (mChallenge.pkStatus == 1) {
            //对决中
            mLayoutChallenge.setVisibility(View.VISIBLE);
            mTvTitle.setVisibility(View.INVISIBLE);
            CountDownRxUtils.textViewCountDown(mTvTime, mChallenge.expiresDate, "距离结束还剩\n%s", "%s : %s : %s");
            mLayoutPkResult.setVisibility(View.GONE);
            mIvBg.setVisibility(View.GONE);
            mLayoutPk.setVisibility(View.VISIBLE);
            setPkData();
        }
    }

    private void setPkData() {
        FrescoUtil.setImage(mIvMyAvatar, mChallenge.pkDetail.headImage);
        FrescoUtil.setImage(mIvPKAvatar, mChallenge.pkDetail.pkHeadImage);
        mTvMyName.setText(mChallenge.pkDetail.nickName);
        mTvPkName.setText(mChallenge.pkDetail.pkNickName);
        int myScore = mChallenge.pkDetail.endPoint - mChallenge.pkDetail.startPoint;
        int pkScore = mChallenge.pkDetail.pkEndPoint - mChallenge.pkDetail.pkStartPoint;
        mTvMyScore.setText(mChallenge.type == 2 || mChallenge.type == 3 ? MoneyUtil.centToYuanStrNoZero(myScore) : String.valueOf(myScore));
        mTvPkScore.setText(mChallenge.type == 2 || mChallenge.type == 3 ? MoneyUtil.centToYuanStrNoZero(pkScore) : String.valueOf(pkScore));
    }


    private void initView() {
        setTitle("巅峰对决");
        setLeftBlack();
    }

    private OnValueChangeLister onValueChangeLister = new OnValueChangeLister() {
        @Override
        public void changed(int value) {
            if (mScoreModel.totalScore < value) {
                ChallengeManager.showNoScoreDialog(ChallengePKDetailActivity.this);
            } else {
                APIManager.startRequest(mService.getPkChallengeList(mActivityId, value, value),
                        new BaseRequestListener<List<Challenge>>(ChallengePKDetailActivity.this) {
                            @Override
                            public void onSuccess(List<Challenge> result) {
                                if (result.size() > 0) {
                                    WJDialog wjDialog = new WJDialog(ChallengePKDetailActivity.this);
                                    wjDialog.show();
                                    wjDialog.setContentText("已为您匹配到可挑战的队友");
                                    wjDialog.setCancelText("发起挑战");
                                    wjDialog.setConfirmText("前往挑战");
                                    wjDialog.setConfirmTextColor(R.color.red);
                                    wjDialog.setOnCancelListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            ChallengeManager.addPkChallenge(ChallengePKDetailActivity.this, mActivityId, value);
                                            wjDialog.dismiss();
                                        }
                                    });
                                    wjDialog.setOnConfirmListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            wjDialog.dismiss();
                                            Intent intent = new Intent(ChallengePKDetailActivity.this, ChallengePkListActivity.class);
                                            intent.putExtra("activityId", mActivityId);
                                            intent.putExtra("pkScore", value);
                                            startActivity(intent);
                                            finish();
                                        }
                                    });
                                } else {
                                    ChallengeManager.addPkChallenge(ChallengePKDetailActivity.this, mActivityId, value);
                                }
                            }
                        },ChallengePKDetailActivity.this);
            }

        }
    };

    @OnClick(R.id.layoutChallenge)
    public void challenge(View view) {
        if (null != mChallenge && mChallenge.activityStatus != 1) {
            if (null == mEditChallengePKScoreDialog) {
                mEditChallengePKScoreDialog = new EditChallengePKScoreDialog(ChallengePKDetailActivity.this,
                        mMinScore, mMaxScore);
                mEditChallengePKScoreDialog.setOnChangeListener(onValueChangeLister);
            }
            mEditChallengePKScoreDialog.show();
        }
    }

    @OnClick(R.id.tvCancelChallenge)
    protected void cancelChallenge() {
        WJDialog wjDialog = new WJDialog(ChallengePKDetailActivity.this);
        wjDialog.show();
        wjDialog.setContentText("再等一下就有人来挑战啦，确定要取消吗？");
        wjDialog.setCancelText("取消挑战");
        wjDialog.setConfirmText("继续等待");
        wjDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wjDialog.dismiss();
            }
        });
        wjDialog.setOnCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ToastUtil.showLoading(ChallengePKDetailActivity.this);
                APIManager.startRequest(mService.cancelPkChallenge(mChallenge.challengeId),
                        new BaseRequestListener<Object>(ChallengePKDetailActivity.this) {
                            @Override
                            public void onSuccess(Object result) {
                                wjDialog.dismiss();
                                EventBus.getDefault().post(new EventMessage(Event.challengeTrans));
                                ToastUtil.success("取消成功");
                                finish();
                            }
                        },ChallengePKDetailActivity.this);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(EventMessage message) {
        switch (message.getEvent()) {
            case countDownComplete:
                initData();
                break;
            default:
        }
    }
}
