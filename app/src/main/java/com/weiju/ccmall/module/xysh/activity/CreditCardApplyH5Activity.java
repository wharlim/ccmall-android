package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;

import com.blankj.utilcode.utils.StringUtils;
import com.orhanobut.logger.Logger;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.component.H5WebView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Ben
 * @date 2020/6/10.
 */
public class CreditCardApplyH5Activity extends BaseActivity {

    @BindView(R.id.webView)
    H5WebView webView;
    @BindView(R.id.pb)
    ProgressBar mPb;
    @BindView(R.id.vShade)
    View vShade;
    private String mUrl;
    private String mTitle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card_apply_h5);
        ButterKnife.bind(this);
        mUrl = "https://interacts.hq.vidata.com.cn/h5/card-platform/index.html?source=18849";
        setLeftBlack();
        if (StringUtils.isEmpty(mUrl)) {
            return;
        }
        Logger.d("mUrl:" + mUrl);
        openWeb();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        webView.destroy();
    }

    private void openWeb() {
        webView.setProgressbar(mPb);
        webView.setWebViewChangeTitleCallback(title -> {
            setTitle(title);
            if (TextUtils.isEmpty(mTitle)) {
                mTitle = title;
            }
            if (title.equals(mTitle)) {
                vShade.setVisibility(View.VISIBLE);
            } else {
                vShade.setVisibility(View.GONE);
            }
        });
        webView.loadUrl(mUrl);
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) {
            webView.goBack();
        } else {
            super.onBackPressed();
        }
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, CreditCardApplyH5Activity.class);
//        intent.putExtra("url", url);
        context.startActivity(intent);
    }
}
