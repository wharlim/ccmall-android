package com.weiju.ccmall.module.message.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.balance.BalanceListActivity;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.module.lottery.WinnerListActivity;
import com.weiju.ccmall.module.message.ExchangeTicketActivity;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.user.BindPhoneActivity;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.ActivityCheckResult;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.Message;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.enums.AccountType;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IInstantService;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MessageAdapter extends BaseAdapter<Message, MessageAdapter.MessageViewHolder> {

    private IInstantService instantService;
    private ILiveService liveService = ServiceManager.getInstance().createService(ILiveService.class);

    public MessageAdapter(Context context) {
        super(context);
        instantService = ServiceManager.getInstance().createService(IInstantService.class);
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MessageViewHolder(layoutInflater.inflate(R.layout.item_message_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        final Message message = items.get(position);

        holder.itemTitleTv.setText(message.title);
        holder.itemTimeTv.setText(message.createDate);
        if (message.isImageMessage()) {
            holder.itemContentTv.setVisibility(View.GONE);
            holder.itemThumbIv.setVisibility(View.VISIBLE);
            FrescoUtil.setImageSmall(holder.itemThumbIv, message.thumb);
        } else {
            holder.itemContentTv.setVisibility(View.VISIBLE);
            holder.itemThumbIv.setVisibility(View.GONE);
            holder.itemContentTv.setText(message.getContent());
        }
        if (message.hasDetail()) {
            holder.itemMoreLayout.setVisibility(View.VISIBLE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewMessageDetail(message);
                }
            });
            holder.itemMoreLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewMessageDetail(message);

                }
            });
        } else {
            holder.itemMoreLayout.setOnClickListener(null);
            holder.itemView.setOnClickListener(null);
            holder.itemMoreLayout.setVisibility(View.GONE);
        }
        if (position == items.size() - 1) {
            RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) holder.itemView.getLayoutParams();
            int margin = ConvertUtil.dip2px(10);
            layoutParams.setMargins(0, 0, 0, margin);
            holder.itemView.setLayoutParams(layoutParams);
        }
    }

    private WJDialog wjDialog;

    private void showDialog(String msg) {
        if (wjDialog != null && wjDialog.isShowing()) {
            wjDialog.dismiss();
        }
        wjDialog = new WJDialog(context);
        wjDialog.show();
        wjDialog.setTitle("提示");
        wjDialog.setContentText(msg);
        wjDialog.setConfirmText("确定");
        wjDialog.hideCancelBtn();
        wjDialog.setOnConfirmListener(v -> {
            wjDialog.dismiss();
        });
    }

    private void viewMessageDetail(Message message) {
        if (message.type == 16) {
            APIManager.startRequest(instantService.checkSeckillOrderRelation(), new BaseRequestListener<ActivityCheckResult>() {
                @Override
                public void onSuccess(ActivityCheckResult result) {
                    super.onSuccess(result);
                    if (context != null && !((Activity) context).isDestroyed()) {
                        if (result.isOk()) {
                            ExchangeTicketActivity.start(context);
                        } else {
                            showDialog(result.message);
                        }
                    }
                }
            }, context);
        } else if (message.type == 17){
            String url = message.infoId;
            if (TextUtils.isEmpty(url)) {
                return ;
            }
            if (url.contains("?")) {
                url += "&";
            } else {
                url += "?";
            }
            url += ("token=" + SessionUtil.getInstance().getOAuthToken());
            Intent intent = new Intent(context, WebViewJavaActivity.class);
            intent.putExtra("url", url);
            intent.putExtra("hideToolbar", true);
            intent.putExtra("showWindow", true);
            context.startActivity(intent);
        } else if (Arrays.asList(41, 44, 45, 48).contains(message.type)) {
            // 订单详情
            EventUtil.viewOrderDetail(context, message.infoId, false);
        } else if (Arrays.asList(46, 47, 51).contains(message.type)) {
            if (StringUtils.isEmpty(message.refundId)) {
                EventUtil.viewOrderDetail(context, message.infoId, false);
            } else {
                EventUtil.viewRefundDetail(context, message.refundId, false);
            }
        } else if (message.type == 50) {
            // 收入
            Intent intent2 = new Intent(context, BalanceListActivity.class);
            intent2.putExtra("type", AccountType.Profit);
            context.startActivity(intent2);

        } else if (message.type == 70) {
            // 余额

            Intent intent2 = new Intent(context, BalanceListActivity.class);
            intent2.putExtra("type", AccountType.Balance);
            context.startActivity(intent2);

        } else if (message.type == 71) {
            // 中奖纪录
            context.startActivity(new Intent(context, WinnerListActivity.class));
        } else if (message.type == 98) {
            // 直播
            openLive(message.infoId);
        }
    }

    class MessageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemTitleTv)
        protected TextView itemTitleTv;
        @BindView(R.id.itemTimeTv)
        protected TextView itemTimeTv;
        @BindView(R.id.itemThumbIv)
        protected SimpleDraweeView itemThumbIv;
        @BindView(R.id.itemContentTv)
        protected TextView itemContentTv;
        @BindView(R.id.itemMoreLayout)
        protected LinearLayout itemMoreLayout;

        MessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private void openLive(String liveId) {
        APIManager.startRequest(liveService.getLive(liveId), new BaseRequestListener<LiveRoom>() {
            @Override
            public void onSuccess(LiveRoom result) {
                LiveManager.checkRoomPassword(context, result);
            }
        }, context);
    }
}
