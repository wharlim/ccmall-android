package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProviderApplyStatusActivity extends BaseActivity {

    @BindView(R.id.ivStatus)
    ImageView mIvStatus;
    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.tvContent)
    TextView mTvContent;
    @BindView(R.id.tvBack)
    TextView mTvBack;
    private boolean mIsApplyFail;
    private String mMchInId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_apply_status);
        ButterKnife.bind(this);
        setTitle("入驻申请");
        setLeftBlack();
        initView();
    }

    private void initView() {
        mMchInId = getIntent().getStringExtra("mchInId");
        mIsApplyFail = !TextUtils.isEmpty(mMchInId);
        if (mIsApplyFail) {
            String reason = getIntent().getStringExtra("reason");
            mIvStatus.setImageResource(R.drawable.ic_provider_apply_fail);
            mTvTitle.setText("审核失败");
            mTvContent.setText(String.format("失败原因：%s", reason));
            mTvBack.setText("返回修改");
        } else {
            mIvStatus.setImageResource(R.drawable.ic_provider_apply_wait);
            mTvTitle.setText("等待审核");
            mTvContent.setText("已提交申请，请耐心等待平台人员处理");
            mTvBack.setText("返回");
        }
    }

    @OnClick(R.id.tvBack)
    public void onViewClicked() {
        if (mIsApplyFail) ProviderApplyFirstActivity.start(this, mMchInId);
        finish();
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, ProviderApplyStatusActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, String mchInId, String reason) {
        Intent intent = new Intent(context, ProviderApplyStatusActivity.class);
        intent.putExtra("mchInId", mchInId);
        intent.putExtra("reason", reason);
        context.startActivity(intent);
    }

}
