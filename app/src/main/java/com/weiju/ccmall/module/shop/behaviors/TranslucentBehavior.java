package com.weiju.ccmall.module.shop.behaviors;

import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class TranslucentBehavior extends CoordinatorLayout.Behavior<Toolbar> {

    private int total;

    public TranslucentBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, Toolbar child, View dependency) {
        return dependency instanceof TextView;
    }

    /**
     * 必须要加上  layout_anchor，对方也要layout_collapseMode才能使用
     */
    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, Toolbar child, View dependency) {

        if (total == 0) {
            total = Math.round((dependency.getMeasuredHeight() - child.getMeasuredHeight()) / 2.0f);
        }

        int percent = Math.round(dependency.getY() * 100 / total);
        if (percent > 100) {
            percent = 100;
        }

        // 计算alpha通道值
        float alpha = percent / 100.0f * 255;


        //设置背景颜色
        child.setBackgroundColor(Color.argb((int) alpha, 245,24,97));

        return true;
    }
}
