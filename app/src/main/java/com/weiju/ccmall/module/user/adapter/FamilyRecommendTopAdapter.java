package com.weiju.ccmall.module.user.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.weiju.ccmall.module.user.viewholders.FamilyRecommendHolder;
import com.weiju.ccmall.shared.bean.Family;

import java.util.ArrayList;
import java.util.List;

public class FamilyRecommendTopAdapter extends RecyclerView.Adapter<FamilyRecommendHolder> {

    private ArrayList<Family.DatasEntity> datas = new ArrayList<>();

    public void setDatas(List<Family.DatasEntity> datas) {
        this.datas.clear();
        this.datas.addAll(datas);
        notifyDataSetChanged();
    }

    public ArrayList<Family.DatasEntity> getDatas() {
        return this.datas;
    }

    @NonNull
    @Override
    public FamilyRecommendHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return FamilyRecommendHolder.newInstance(viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull FamilyRecommendHolder familyRecommendHolder, int i) {
        familyRecommendHolder.bindView(datas.get(i));
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }
}
