package com.weiju.ccmall.module.deal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.ToastUtils;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.AuthPhoneActivity;
import com.weiju.ccmall.module.auth.AuthPhoneActivity2;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.auth.model.CardDetailModel;
import com.weiju.ccmall.module.live.activity.BindAlipayActivity;
import com.weiju.ccmall.module.live.entity.AlipayAccountEntity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.MyStatus;
import com.weiju.ccmall.shared.bean.ProfitData;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBalanceService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

public class DealFirstActivity extends BaseActivity {

    @BindView(R.id.etMoney)
    EditText mEtMoney;
    @BindView(R.id.tvAll)
    TextView mTvAll;
    @BindView(R.id.tvSubmit)
    TextView mTvSubmit;
    @BindView(R.id.tvTypeTitle)
    TextView tvTypeTitle;
    @BindView(R.id.tvAccount)
    TextView tvAccount;
    @BindView(R.id.tvTips)
    TextView tvTips;

    private IBalanceService mBalanceService;
    private ProfitData mProfitData;
    private MyStatus mMyStatus;
    private IUserService mUserService;
    private CardDetailModel mCardDetailModel;
    private AlipayAccountEntity mAlipayUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal_first);
        ButterKnife.bind(this);
        initView();
        if (mAlipayUser == null) {
            initData();
        } else {
            initAlipayData();
        }
        EventBus.getDefault().register(this);
    }

    private void initAlipayData() {
        mEtMoney.setHint("最多可提现" + mAlipayUser.totalMoney + "元");
        tvTypeTitle.setText("到账支付宝");
        tvTypeTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_alipay, 0, 0, 0);
        tvTips.setText("无需手续费，我们会在1～3个工作日内审核打款");
        tvAccount.setText(mAlipayUser.formatAlipayAccount);
        tvAccount.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_edit, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initData() {
        mBalanceService = ServiceManager.getInstance().createService(IBalanceService.class);
        APIManager.startRequest(mBalanceService.get(), new BaseRequestListener<ProfitData>(this) {

            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(ProfitData profitData) {
                mProfitData = profitData;
                mEtMoney.setHint("可提现余额 " + ConvertUtil.cent2yuan(profitData.availableMoney));
            }
        }, this);

        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(mUserService.getMyStatus(), new BaseRequestListener<MyStatus>(this) {

            @Override
            public void onSuccess(MyStatus myStatus) {
                mMyStatus = myStatus;
            }
        }, this);
        Observable<RequestResult<CardDetailModel>> gongCatCard = MyApplication.isGongCat ?
                mUserService.getGongCatCard(SessionUtil.getInstance().getOAuthToken()) :
                mUserService.getCard();
        APIManager.startRequest(gongCatCard, new BaseRequestListener<CardDetailModel>(this) {

            @Override
            public void onSuccess(CardDetailModel model) {
                mCardDetailModel = model;
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.error(e.getMessage());
                mCardDetailModel = new CardDetailModel();
                mCardDetailModel.status = "-1";
            }
        }, this);
    }

    private void initView() {
        setLeftBlack();
        setTitle("申请提现");
        mAlipayUser = (AlipayAccountEntity) getIntent().getSerializableExtra("alipayUser");
    }

    @OnClick(R.id.tvAll)
    public void onMTvAllClicked() {
        if (mAlipayUser == null) {
            mEtMoney.setText(mProfitData.availableMoney * 1.0f / 100 + "");
        } else {
            mEtMoney.setText(mAlipayUser.totalMoney);
        }
    }

    @OnClick(R.id.tvAccount)
    public void onEditClicked() {
        BindAlipayActivity.start(this, mAlipayUser);
    }

    @OnClick(R.id.tvSubmit)
    public void onMTvSubmitClicked() {
        if (mAlipayUser != null) {
            alipaySubmit();
            return;
        }
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser == null || mMyStatus == null) {
            ToastUtils.showShortToast("等待数据");
            return;
        }

        if (MyApplication.isGongCat) {
            int status = -1;
            if (!TextUtils.isEmpty(mCardDetailModel.status)) {
                status = Integer.parseInt(mCardDetailModel.status);
            }
            if (status != AppTypes.CARD_STATUS_GONGCAT.SUCESS) {
                switch (status) {
                    case AppTypes.CARD_STATUS_GONGCAT.NO_SUBMIT:
                        showErrorDialog();
                        break;
                    case AppTypes.CARD_STATUS_GONGCAT.WAIT:
                        ToastUtils.showShortToast("我们正在审核您的银行卡，请稍后再试");
                        break;
                    case AppTypes.CARD_STATUS_GONGCAT.FAIL:
                        showErrorDialog();
                        break;
                    default:
                        break;
                }
            } else {
                addDeal();
            }
        } else {
            if (loginUser.autoAuthStatus != AppTypes.AUTH_STATUS.SUCESS) {
                ToastUtils.showShortToast("请先实名认证");
            } else if (mMyStatus.bindBankStatus != AppTypes.CARD_STATUS.SUCESS) {
                switch (mMyStatus.bindBankStatus) {
                    case AppTypes.CARD_STATUS.NO_SUBMIT:
                        showErrorDialog();
                        break;
                    case AppTypes.CARD_STATUS.WAIT:
                        ToastUtils.showShortToast("我们正在审核您的银行卡，请稍后再试");
                        break;
                    case AppTypes.CARD_STATUS.FAIL:
                        showErrorDialog();
                        break;
                    default:
                        break;
                }
            } else {
                addDeal();
            }
        }

    }

    private void alipaySubmit() {
        String money = mEtMoney.getText().toString();
        if (StringUtils.isEmpty(money)) {
            ToastUtil.error("请填写提现金额");
            return;
        }
        long money2Long = ConvertUtil.stringMoney2Long(money);
        if (money2Long < 10000 || money2Long > 5000000) {
            ToastUtil.error("单笔提现限额为100元~50000元");
            return;
        }

        Intent intent = new Intent(this, AuthPhoneActivity.class);
        intent.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_DEAL_ALI);
        intent.putExtra("applyAccountId", mAlipayUser.accountId);
        intent.putExtra("applyMoney", money2Long);
        startActivity(intent);
    }

    private void showErrorDialog() {
        WJDialog wjDialog = new WJDialog(DealFirstActivity.this);
        wjDialog.show();
        wjDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DealFirstActivity.this, MyApplication.isGongCat ? AuthPhoneActivity2.class : AuthPhoneActivity.class);
                intent.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_CARD);
                startActivity(intent);
            }
        });
        if (MyApplication.isGongCat) {
            int status = -1;
            if (!TextUtils.isEmpty(mCardDetailModel.status)) {
                status = Integer.parseInt(mCardDetailModel.status);
            }
            switch (status) {
                case AppTypes.CARD_STATUS.FAIL:
                    wjDialog.setContentText("您的银行卡信息未通过审核，请重新提交");
                    wjDialog.setConfirmText("去查看");
                    break;
                case AppTypes.CARD_STATUS.NO_SUBMIT:
                    wjDialog.setConfirmText("去绑定");
                    wjDialog.setContentText("请先绑定银行卡");
                    break;
                default:
                    break;
            }
        } else {
            switch (mMyStatus.bindBankStatus) {
                case AppTypes.CARD_STATUS.FAIL:
                    wjDialog.setContentText("您的银行卡信息未通过审核，请重新提交");
                    wjDialog.setConfirmText("去查看");
                    break;
                case AppTypes.CARD_STATUS.NO_SUBMIT:
                    wjDialog.setConfirmText("去绑定");
                    wjDialog.setContentText("请先绑定银行卡");
                    break;
                default:
                    break;
            }
        }

    }

    private void addDeal() {
        // 提现
        if (StringUtils.isEmpty(mEtMoney.getText().toString())) {
            ToastUtil.error("请填写提现金额");
            return;
        }


        long money = MyApplication.isGongCat ? Long.parseLong(mEtMoney.getText().toString()) : ConvertUtil.stringMoney2Long(mEtMoney.getText().toString());
//        if (money%10!=0){
//            ToastUtil.error("请输入10的倍数");
//            return;
//        }
        Intent intent = new Intent(this, AuthPhoneActivity.class);
        intent.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_DEAL);
        startActivity(intent);


        //     WithdrawDo withdrawDo = new WithdrawDo("1.0",SessionUtil.getInstance().getOAuthToken(),money,"");
//        Observable<RequestResult<Object>> observable = mBalanceService.addDeal(withdrawDo.toMap());

        Observable<RequestResult<Object>> observableGongCat = mBalanceService.addDealGongCat(SessionUtil.getInstance().getOAuthToken(), money, "");
        Observable<RequestResult<Object>> observable = mBalanceService.addDeal(money, mCardDetailModel.accountId);
        EventBus.getDefault().postSticky(MyApplication.isGongCat ? observableGongCat : observable);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(MsgStatus status) {
        switch (status.getAction()) {
            case MsgStatus.ACTION_DEAL_SUCESS:
                finish();
                break;
            default:
                break;
        }
    }

    public static void start(Context context, AlipayAccountEntity alipayUser) {
        Intent intent = new Intent(context, DealFirstActivity.class);
        intent.putExtra("alipayUser", alipayUser);
        context.startActivity(intent);
    }
}
