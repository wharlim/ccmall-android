package com.weiju.ccmall.module.live.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.util.FrescoUtil;

public class MyHistoryLivesAdapter extends BaseQuickAdapter<LiveRoom, BaseViewHolder> {
    public MyHistoryLivesAdapter() {
        super(R.layout.item_my_history_lives);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveRoom item) {
        // 状态
        helper.setText(R.id.tvState, item.getStatusStr());
        // 直播封面
        SimpleDraweeView avatar = helper.getView(R.id.avatarIv);
        FrescoUtil.setImageSmall(avatar, item.liveImage);
        // 标题和时间
        helper.setText(R.id.tvTitle, item.title);
        if (item.isForecast()) {
            helper.setText(R.id.tvTime, item.forecastTime);
        } else {
            helper.setText(R.id.tvTime, item.startTime);
        }
        // 开始按钮事件
        helper.addOnClickListener(R.id.tvUp);
        helper.addOnClickListener(R.id.tvDown);
        helper.addOnClickListener(R.id.tvDel);
        helper.addOnClickListener(R.id.tvEnd);
        helper.addOnClickListener(R.id.tvShare);
        helper.addOnClickListener(R.id.tvStartLive);

        // 按钮显示状态
        helper.setVisible(R.id.tvUp, false);
        helper.setVisible(R.id.tvDown, false);
        helper.setVisible(R.id.tvEnd, false);
        helper.setVisible(R.id.tvDel, false);
        helper.setVisible(R.id.tvShare, true);
        helper.setVisible(R.id.tvStartLive, false);
        if (item.isForecast()) {
            helper.setVisible(R.id.tvDel, true);
            helper.setVisible(R.id.tvStartLive, true);
        } else if (item.status == 1) {
            helper.setVisible(R.id.tvEnd, true);
        } else if (item.status == 3 || item.status == 2) {
            helper.setVisible(R.id.tvDel, true);
            // 上架下架
            if (item.onlineStatus == 0) {
                helper.setVisible(R.id.tvUp, true);
                helper.setVisible(R.id.tvDown, false);
            } else {
                helper.setVisible(R.id.tvUp, false);
                helper.setVisible(R.id.tvDown, true);
            }
        }
    }
}
