package com.weiju.ccmall.module.jkp.newjkp.widgets;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.listener.OnItemClickListener;
import com.blankj.utilcode.utils.SizeUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.jkp.newjkp.entity.BannerEntity;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.shared.util.CarouselUtil;
import com.weiju.ccmall.shared.util.CarshReportUtils;

import java.util.ArrayList;
import java.util.List;

public class BannerElement extends LinearLayout implements OnItemClickListener {

    private ConvenientBanner convenientBanner;
    private List<BannerEntity.Bean> mDataList;
    private Context mContext;

    public BannerElement(Context context, List<BannerEntity.Bean> beans) {
        super(context);
        mContext = context;
        try {
            View view = inflate(getContext(), R.layout.el_carousel_element, this);
            convenientBanner = view.findViewById(R.id.imagesLayout);
            LayoutParams layoutParams;
            layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, SizeUtils.dp2px(85));
            layoutParams.topMargin = SizeUtils.dp2px(7);
            layoutParams.leftMargin = SizeUtils.dp2px(7);
            layoutParams.rightMargin = SizeUtils.dp2px(7);
            convenientBanner.setLayoutParams(layoutParams);
            mDataList = beans;
            List<String> img = new ArrayList<>();
            for (BannerEntity.Bean bean : mDataList) {
                img.add(bean.image);
            }
            CarouselUtil.initCarousel(convenientBanner, img, R.drawable.icon_page_indicator_focused);
            convenientBanner.setOnItemClickListener(this);
            convenientBanner.setCanLoop(img.size() > 1);
            convenientBanner.setPointViewVisible(img.size() > 1);
        } catch (Exception e) {
            CarshReportUtils.post(e);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (null != convenientBanner) {
            convenientBanner.stopTurning();
        }
    }

    @Override
    public void onItemClick(int position) {
        BannerEntity.Bean data = mDataList.get(position);
        Intent intent = new Intent(mContext, WebViewJavaActivity.class);
        intent.putExtra("url", data.target);
        mContext.startActivity(intent);
    }
}
