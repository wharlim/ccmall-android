package com.weiju.ccmall.module.groupBuy.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.util.FrescoUtil;

/**
 * @author chenyanming
 * @time 2019/8/15 on 11:04
 * @desc ${TODD}
 */
public class SuperMatchItemAdapter extends BaseQuickAdapter<User, BaseViewHolder> {
    public SuperMatchItemAdapter() {
        super(R.layout.item_super_match_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, User item) {
        helper.setText(R.id.tvName, String.format("%s(%s)", item.nickname, item.phone));
        FrescoUtil.setImageSmall(helper.getView(R.id.ivAvatar), item.avatar);
        helper.setVisible(R.id.ivTag, item.role == 1);
    }
}
