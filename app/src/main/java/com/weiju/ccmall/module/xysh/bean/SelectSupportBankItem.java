package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SelectSupportBankItem implements Serializable {

    /**
     * bankName : 交通银行
     * bankAbbr : COMM
     */

    @SerializedName("bankName")
    public String bankName;
    @SerializedName("bankAbbr")
    public String bankAbbr;
}
