package com.weiju.ccmall.module.user;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.adapter.MyContributionSortTagAdapter;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.bean.Page;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanmin
 * @date 2019/6/23
 * @desc
 */
public class MyContributionSortListActivity extends BaseActivity {
    @BindView(R.id.recyclerViewTag)
    protected RecyclerView mRecyclerViewTag;
    @BindView(R.id.tvWeekSort)
    protected TextView mTvWeekSort;
    @BindView(R.id.tvMonthSort)
    protected TextView mTvMonthSort;
    @BindView(R.id.tvTotalSort)
    protected TextView mTvTotalSort;
    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;

    private String mDateType = "week";
    private String mLastDateType = "week";

    private boolean mIsCCM;

    private List<Page> mPages = new ArrayList<>();

    private MyContributionSortTagAdapter mMyContributionSortTagAdapter = new MyContributionSortTagAdapter();
    //贡献值排行榜列表Fragment
    protected List<BaseFragment> mFragments = new ArrayList<>();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contribution_sort_list);
        ButterKnife.bind(this);
        initInent();
        initView();
    }

    private void initInent() {
        mIsCCM = getIntent().getBooleanExtra("isCCM", false);
    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.setStatusBarColor(getResources().getColor(R.color.contribution_sort_title_bg));
            QMUIStatusBarHelper.setStatusBarDarkMode(this);
        }
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_white);
        getHeaderLayout().makeHeaderColor(R.color.contribution_sort_title_bg);
        getHeaderLayout().setTitleImage(mIsCCM ? R.drawable.ic_ccm_sort_title : R.drawable.ic_contribution_sort_title);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(MyContributionSortListActivity.this, 5);
        mRecyclerViewTag.setLayoutManager(gridLayoutManager);

        mRecyclerViewTag.setAdapter(mMyContributionSortTagAdapter);
//        mTagList.add("PSP");
//        mTagList.add("TSP");
//        mTagList.add("TSE");
//        mTagList.add("TSL");
//        mTagList.add("PCP");
        if (mIsCCM) {
            mPages.add(new Page("ccm", "CCM"));
            mRecyclerViewTag.setVisibility(View.GONE);
        } else {
            mPages.add(new Page("psp", "PSP"));
            mPages.add(new Page("tsp", "TSP"));
            mPages.add(new Page("tse", "TSE"));
            mPages.add(new Page("tsl", "TSL"));
            mPages.add(new Page("pcp", "PCP"));
        }
        mMyContributionSortTagAdapter.setNewData(mPages);

        mRecyclerViewTag.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                mMyContributionSortTagAdapter.setSelectPosition(position);
                mViewPager.setCurrentItem(position);
            }
        });

        mTvWeekSort.setSelected(true);
        mTvMonthSort.setSelected(false);
        mTvTotalSort.setSelected(false);

        for (Page page : mPages) {
            MyContributionSortFragment myContributionSortFragment = new MyContributionSortFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("page", page);
            myContributionSortFragment.setArguments(bundle);
            mFragments.add(myContributionSortFragment);
        }
        initViewPager();
    }

    private void initViewPager() {
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mPages.get(position).name;
            }
        });
        mViewPager.setOffscreenPageLimit(mFragments.size());

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                mMyContributionSortTagAdapter.setSelectPosition(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @OnClick({R.id.tvWeekSort, R.id.tvMonthSort, R.id.tvTotalSort})
    public void dateSelect(View view) {
        switch (view.getId()) {
            case R.id.tvWeekSort:
                mDateType = "week";
                mTvWeekSort.setSelected(true);
                mTvMonthSort.setSelected(false);
                mTvTotalSort.setSelected(false);
                break;
            case R.id.tvMonthSort:
                mDateType = "month";
                mTvWeekSort.setSelected(false);
                mTvMonthSort.setSelected(true);
                mTvTotalSort.setSelected(false);
                break;
            case R.id.tvTotalSort:
                mDateType = "total";
                mTvWeekSort.setSelected(false);
                mTvMonthSort.setSelected(false);
                mTvTotalSort.setSelected(true);
                break;
            default:
        }
        if (!mDateType.equals(mLastDateType)) {

        }


    }
}
