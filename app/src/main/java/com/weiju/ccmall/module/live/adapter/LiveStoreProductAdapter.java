package com.weiju.ccmall.module.live.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

import java.util.List;

public class LiveStoreProductAdapter extends BaseQuickAdapter<SkuInfo, BaseViewHolder> {


    public LiveStoreProductAdapter(@Nullable List<SkuInfo> data) {
        super(R.layout.item_live_store_product, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SkuInfo item) {
        FrescoUtil.setImageSmall(helper.getView(R.id.itemThumbIv), item.thumb);
        helper.setText(R.id.tvTitle, item.name);
        helper.setText(R.id.tvItemPrice, MoneyUtil.centToYuan¥StrNoZero(item.retailPrice));
        helper.setText(R.id.tvCCM, String.format("奖%s%%算率", item.countRateExc));
    }

}
