package com.weiju.ccmall.module.xysh.activity.quick_pay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.BankAdminActivity;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.ReceiptOrder;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SelectCardActivity extends BaseActivity {
    @BindView(R.id.tvTransferAmt)
    TextView tvTransferAmt;
    @BindView(R.id.tvServiceCharge)
    TextView tvServiceCharge;
    @BindView(R.id.tvPayCard)
    TextView tvPayCard;
    @BindView(R.id.llPayCardContainer)
    LinearLayout llPayCardContainer;
    @BindView(R.id.tvReceiptCard)
    TextView tvReceiptCard;
    @BindView(R.id.llReceiptCardContainer)
    LinearLayout llReceiptCardContainer;
    @BindView(R.id.tv_next)
    TextView tvNext;

    private static final int REQ_SELECT_CREDIT = 1;
    private static final int REQ_SELECT_DEBIT = 2;
    @BindView(R.id.etAuthCode)
    EditText etAuthCode;
    @BindView(R.id.tvSendAuthCode)
    TextView tvSendAuthCode;
    @BindView(R.id.llAuthCodeContainer)
    LinearLayout llAuthCodeContainer;

    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    private QueryUserBankCardResult.BankInfListBean payCard; // 扣款
    private QueryUserBankCardResult.BankInfListBean receiptCard; // 到账

    private ChannelItem channel;
    private boolean isChannelAuthed;
    private float transferAmt;

    private String orderId;

    private CountDownTimer timer;
    private boolean couldSendAuthCode = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_pay_card);
        channel = (ChannelItem) getIntent().getSerializableExtra("channel");
        transferAmt = getIntent().getFloatExtra("transferAmt", 0f);
        ButterKnife.bind(this);
        initView();
        setLeftBlack();
        setTitle("选择扣款卡");
        loadDefaultPayCard();
        loadDefaultReceiptCard();

//        onViewClicked(tvNext); // 自动点击下一步
    }

    private void initView() {
        setChannelAuthOrNot(true);
        tvTransferAmt.setText(String.format("%.2f元", transferAmt));
        tvServiceCharge.setText(String.format("%.2f元", transferAmt * channel.channelRate / 100 + channel.serviceCharge));
    }

    private void loadDefaultPayCard() {
        setPayCard((QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("payCard"));
        //APIManager.startRequest(service.cardInfoGet(0), new Observer<QueryUserBankCardResult>() {
        //    @Override
        //    public void onSubscribe(Disposable d) {
        //
        //    }
        //
        //    @Override
        //    public void onNext(QueryUserBankCardResult ret) {
        //        if (ret.code == 1 && ret.bankInfList.size() > 0) {
        //            setPayCard(ret.bankInfList.get(0));
        //        }
        //    }
        //
        //    @Override
        //    public void onError(Throwable e) {
        //
        //    }
        //
        //    @Override
        //    public void onComplete() {
        //
        //    }
        //});
    }

    private void loadDefaultReceiptCard() {
        setReceiptCard((QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("receiptCard"));
        //APIManager.startRequest(service.cardInfoGet(1), new Observer<QueryUserBankCardResult>() {
        //    @Override
        //    public void onSubscribe(Disposable d) {
        //
        //    }
        //
        //    @Override
        //    public void onNext(QueryUserBankCardResult ret) {
        //        if (ret.code == 1 && ret.bankInfList.size() > 0) {
        //            setReceiptCard(ret.bankInfList.get(0));
        //        }
        //    }
        //
        //    @Override
        //    public void onError(Throwable e) {
        //
        //    }
        //
        //    @Override
        //    public void onComplete() {
        //
        //    }
        //});
    }

    private void setPayCard(QueryUserBankCardResult.BankInfListBean card) {
        payCard = card;
        tvPayCard.setText(String.format("%s(%s)", card.bankName, BankUtils.cutBankCardNo(card.cardNo)));
    }

    private void setReceiptCard(QueryUserBankCardResult.BankInfListBean card) {
        receiptCard = card;
        tvReceiptCard.setText(String.format("%s(%s)", card.bankName, BankUtils.cutBankCardNo(card.cardNo)));
    }

    public static void start(Context context, ChannelItem channelItem, float transferAmt, QueryUserBankCardResult.BankInfListBean payCard,
                             QueryUserBankCardResult.BankInfListBean receiptCard) {
        Intent intent = new Intent(context, SelectCardActivity.class);
        intent.putExtra("channel", channelItem);
        intent.putExtra("transferAmt", transferAmt);
        intent.putExtra("payCard", payCard);
        intent.putExtra("receiptCard", receiptCard);
        context.startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == REQ_SELECT_CREDIT) {
                setPayCard(BankAdminActivity.getSelectResult(data));
                setChannelAuthOrNot(false);
            } else if (requestCode == REQ_SELECT_DEBIT) {
                setReceiptCard(BankAdminActivity.getSelectResult(data));
            }
        }
    }

    @OnClick({R.id.llPayCardContainer, R.id.llReceiptCardContainer, R.id.tv_next, R.id.tvSendAuthCode})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llPayCardContainer:
                //BankAdminActivity.startForSelectCredit(this, REQ_SELECT_CREDIT);
                break;
            case R.id.llReceiptCardContainer:
                //BankAdminActivity.startForSelectDebit(this, REQ_SELECT_DEBIT);
                break;
            case R.id.tv_next:
                confirm();
                break;
            case R.id.tvSendAuthCode:
                pay();
                break;
        }
    }

    private void setChannelAuthOrNot(boolean auth) {
        if (auth) {
            llAuthCodeContainer.setVisibility(View.VISIBLE);
            // 已认证
            tvNext.setText("确认");
            isChannelAuthed = true;
        } else {
            llAuthCodeContainer.setVisibility(View.GONE);
            // 已认证
            tvNext.setText("下一步");
            isChannelAuthed = false;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAuthSuccess(EventMessage ev) {
        if (ev.getEvent() == Event.authChannelSuccess) {
            setChannelAuthOrNot(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        if (timer != null) {
            timer.cancel();
        }
    }

    private void pay() {
        if (!couldSendAuthCode) {
            return;
        }
        if (payCard == null) {
            ToastUtil.error("请选择扣款卡！");
            return;
        }
        if (receiptCard == null) {
            ToastUtil.error("请选择到账卡！");
            return;
        }
        Observable<XYSHCommonResult<String>> req = service.gftqPay_pay(
                transferAmt,
                payCard.cardNo,
                receiptCard.cardNo,
                SessionUtil.getInstance().getXyshUserInfo().usrNo,
                "ccpay"
        );
        if (channel.h5Verification == 1) {
            req = service.ctfPay(
                    transferAmt,
                    payCard.cardNo,
                    receiptCard.cardNo,
                    SessionUtil.getInstance().getXyshUserInfo().usrNo,
                    "ccpay"
            );
        }
        ToastUtil.showLoading(this);
        couldSendAuthCode = false;
        APIManager.startRequest(req, new Observer<XYSHCommonResult<String>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<String> ret) {
                ToastUtil.hideLoading();
                if (ret.code == 0) {
                    orderId = ret.data;
                    ToastUtil.success("验证码发送成功！");
                    if (timer != null) {
                        timer.cancel();
                    }
                    timer = new CountDownTimer(60000, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            tvSendAuthCode.setText(String.format("%ds", millisUntilFinished/1000));
                        }

                        @Override
                        public void onFinish() {
                            couldSendAuthCode = true;
                        }
                    }.start();
                }else {
                    couldSendAuthCode = true;
                    ToastUtil.error(ret.msg);
                }
            }

            @Override
            public void onError(Throwable e) {
                couldSendAuthCode = true;
                ToastUtil.hideLoading();
                ToastUtil.error(e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void confirm() {
        if (TextUtils.isEmpty(orderId)) {
            ToastUtil.error("请发送验证码!");
            return;
        }
        String authCode = etAuthCode.getText().toString().trim();
        if (TextUtils.isEmpty(authCode)) {
            ToastUtil.error("请输入验证码!");
            return;
        }
        ToastUtil.showLoading(this, true);
        tvNext.setEnabled(false);
        Observable<XYSHCommonResult<ReceiptOrder>> observable = service.payConfirm(orderId, authCode);
        if (channel.h5Verification == 1) {
            observable = service.ctfPayConfirm(orderId, authCode);
        }
        APIManager.startRequest(observable, new Observer<XYSHCommonResult<ReceiptOrder>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<ReceiptOrder> ret) {
                ToastUtil.hideLoading();
                if (ret.code == 0) {
                    ToastUtil.success("扣款成功！");
                    QuickPayOrderDetailActivity.start(SelectCardActivity.this, payCard, receiptCard, ret.data, channel);
                    finish();
                } else {
                    ToastUtil.success("扣款失败！");
                }
            }

            @Override
            public void onError(Throwable e) {
                tvNext.setEnabled(true);
                ToastUtil.success("扣款失败！");
                ToastUtil.hideLoading();
            }

            @Override
            public void onComplete() {
                tvNext.setEnabled(true);
            }
        });
    }
}
