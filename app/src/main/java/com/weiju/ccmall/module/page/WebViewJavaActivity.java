package com.weiju.ccmall.module.page;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebBackForwardList;
import android.webkit.WebHistoryItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.AppUtils;
import com.blankj.utilcode.utils.FileUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.ChromeClientCallbackManager;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.AuthPhoneActivity;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.face.LivenessHelper;
import com.weiju.ccmall.module.jkp.OutLinkUtils;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.module.xysh.fragment.CreditFragment;
import com.weiju.ccmall.newRetail.activity.CommunityActivity;
import com.weiju.ccmall.shared.basic.AgentFActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ShareUtils;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.WebViewUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

//import com.bumptech.glide.request.animation.GlideAnimation;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.mmbs.module.page
 * @since 2017-07-05
 */
public class WebViewJavaActivity extends BaseActivity {

    private static final String TAG = "WebViewJavaActivity";

    @BindView(R.id.layoutWebview)
    FrameLayout mLayoutWebview;
    @BindView(R.id.imageHome)
    ImageView mImageHome;
    @BindView(R.id.baseRelativeLayout)
    RelativeLayout baseRelativeLayout;
    private AgentWeb mWeb;
    private String mUrl;
    private TextView close;
    private float v = 0.0f;
    private RelativeLayout.LayoutParams lp;
    private boolean hideToolbar;

    private boolean isFace = false;
    private boolean mIsJKP;
    private LivenessHelper helper;
    private boolean isStartingShare;

    private boolean isXYSH; // 是否信用生活的web页面
    private boolean isTestScrollShot; // 是否测试滚动截图

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            android.webkit.WebView.enableSlowWholeDocumentDraw();
        }
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);
        mUrl = getIntent().getStringExtra("url");
        if (mUrl.contains("create-chain.net") && SessionUtil.getInstance().isLogin()) {
            Uri uri = Uri.parse(mUrl);
            mUrl = uri.buildUpon()
                    .appendQueryParameter("token", SessionUtil.getInstance().getOAuthToken())
                    .build()
                    .toString();
        }
        hideToolbar = getIntent().getBooleanExtra("hideToolbar", false);
        mIsJKP = getIntent().getBooleanExtra("isJKP", false);
        isXYSH = getIntent().getBooleanExtra("isXYSH", false);
        isTestScrollShot = getIntent().getBooleanExtra("isTestScrollShot", false);
        Log.d(TAG, "mUrl: " + mUrl);
        Log.d(TAG, "hideToolbar: " + hideToolbar);
        Log.d(TAG, "mIsJKP: " + mIsJKP);
        if (hideToolbar) {
            hideHeader();
        } else {
            setLeftBlack();
        }
        if (StringUtils.isEmpty(mUrl)) {
            return;
        }
        openWeb(mUrl);
        showHomeBtnOrNot();
    }

    @SuppressLint("ResourceType")
    private void hideHeader() {
        close = new TextView(this);
        close.setVisibility(View.GONE);
        close.setTextColor(getResources().getColor(R.color.white));
        close.setText("关闭");
        close.setGravity(Gravity.CENTER);
        v = getResources().getDisplayMetrics().widthPixels / 1080f;
        int size = (int) (v * 40);
        close.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) size);
        lp = new RelativeLayout.LayoutParams(size * 3, (int) (v * 125));
        lp.setMargins((int) (v * 30), 0, 0, 0);
        close.setLayoutParams(lp);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        baseRelativeLayout.addView(close, baseRelativeLayout.getChildCount());
        getHeaderLayout().setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        WebView webView = mWeb.getWebCreator().get();
        Uri uri = Uri.parse(webView.getUrl());

        if (hideToolbar && webView.canGoBack()) {
            String url = webView.getUrl();
            int i = url.indexOf("//");
            if (i != -1) {
                url = url.substring(i + 2, url.length());
            } /*else {
                int i1 = url.indexOf("https://");
                if (i1 != -1) {
                    url = url.substring(i1, url.length());
                }
            }*/
            if (url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/CashBill") || url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/") || url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/RepayPlanCreditList") || url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/My")) {
                finish();
                return;
            }
            if (uri.getHost().equals(BuildConfig.XYSH_HOST)) {
                switch (uri.getFragment()) {
                    case "/":
                    case "/CashBill":
                    case "/RepayPlanCreditList":
                    case "/My":
                        super.onBackPressed();
                        break;
                    default:
                        webView.goBack();
                }
            } else {
                WebBackForwardList list = webView.copyBackForwardList();
                if (list != null && list.getSize() > 0) {
                    WebHistoryItem itemAtIndex = list.getItemAtIndex(list.getCurrentIndex() - 1);
                    if (itemAtIndex != null) {
                        if (uri.getHost().equals(BuildConfig.XYSH_HOST)) {
                            webView.loadUrl(getIntent().getStringExtra("url"));
                        } else {
                            webView.goBack();
                        }

                    } else {
                        webView.loadUrl(getIntent().getStringExtra("url"));
                    }
                } else {
                    webView.loadUrl(getIntent().getStringExtra("url"));
                }
            }
        } else {
            super.onBackPressed();
        }
    }

    /**
     * 信用生活返回首页按钮
     */
    private void showHomeBtnOrNot() {
        boolean showWindow = getIntent().getBooleanExtra("showWindow", false);
        mImageHome.setVisibility(showWindow ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onResume() {
        isStartingShare = false;
        super.onResume();
        ToastUtil.hideLoading();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWeb != null) {
            mWeb.clearWebCache();
            mWeb.destroy();
        }
    }

    @OnClick(R.id.imageHome)
    protected void home() {
        if (isTestScrollShot) {
            scrollScreenShot();
            return;
        }
        if (isXYSH) {
            finish();
        } else {
            openWeb(mUrl);
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void openWeb(String url) {
        if (mWeb != null) {
            mWeb.getLoader().loadUrl(url);
            return;
        }
        //传入Activity
//传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams ,第一个参数和第二个参数应该对应。
// 使用默认进度条
// 使用默认进度条颜色
//设置 Web 页面的 title 回调
//
        mWeb = AgentWeb.with(this)//传入Activity
                .setAgentWebParent(mLayoutWebview, new FrameLayout.LayoutParams(-1, -1))//传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams ,第一个参数和第二个参数应该对应。
                .useDefaultIndicator()// 使用默认进度条
                .setIndicatorColor(R.color.red) // 使用默认进度条颜色
                .setReceivedTitleCallback(new ChromeClientCallbackManager.ReceivedTitleCallback() {
                    @Override
                    public void onReceivedTitle(WebView view, String title) {
                        if (!hideToolbar) {
                            setTitle(title);
                        }
                    }
                }) //设置 Web 页面的 title 回调
                .setWebViewClient(webViewClient)
                .createAgentWeb()//
                .ready()
                .go(url);

        //  js调 java
        mWeb.getJsInterfaceHolder().addJavaObject("app", new AndroidInterface());
        if (mIsJKP) {
            mWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidJKPInterface());
        }
    }

    class AndroidJKPInterface {

        @JavascriptInterface
        public void onClickTakeToken() {
            UserService.checkIsLogin(WebViewJavaActivity.this, new UserService.IsLoginListener() {
                @Override
                public void isLogin() {
                }
            });
        }

        @JavascriptInterface
        public void onClickTakeCoupon(final String url) {
            UserService.checkIsLogin(WebViewJavaActivity.this, new UserService.IsLoginListener() {
                @Override
                public void isLogin() {

                }
            });
        }

        @JavascriptInterface
        public void onClickCopy(String content) {
        }

        @JavascriptInterface
        public void onClickShare(String title, String tkPwd, String img) {
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        WebView webView = mWeb.getWebCreator().get();
        switch (resultCode) {
            case 10008:
                if (webView.getUrl().equals("http://credit.ccmallv2.create-chain.net/customizYTH/#/NameAndIdCard")) {
                    webView.goBack();
                }
                break;
            case 10009:
                if (!webView.getUrl().equals("http://credit.ccmallv2.create-chain.net/customizYTH/#/NameAndIdCard")) {
                    AgentFActivity.start2(WebViewJavaActivity.this, CreditFragment.class, null);
                }
                ToastUtil.success("人脸检测成功！");
                isFace = true;
                break;
            default:
                mWeb.uploadFileResult(requestCode, resultCode, data);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            helper.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        }
    }

    private WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (url.equals("http://credit.ccmallv2.create-chain.net/customizYTH/#/NameAndIdCard")) {
                AgentFActivity.start2(WebViewJavaActivity.this, CreditFragment.class, null);
                view.goBack();
            } else if (hideToolbar) {
                Uri uri = Uri.parse(url);
                if (uri.getHost().equals(BuildConfig.XYSH_HOST) && uri.getFragment() != null) {
                    switch (uri.getFragment()) {
                        case "/":
                        case "/CashBill":
                        case "/RepayPlanCreditList":
                        case "/My":
                            setVisibility(close, View.VISIBLE);
                            break;
                        case "/NameAndIdCard":
                            setVisibility(close, View.GONE);
                            if (!isFace) {
                                if (helper == null) {
                                    helper = new LivenessHelper();
                                }
                                helper.start(WebViewJavaActivity.this);
                            }
                            break;
                        default:
                            setVisibility(close, View.GONE);
                    }
                }
            } else {
                setVisibility(close, View.GONE);
            }
            super.onPageFinished(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (url.contains("weixin://")) {
                Uri uri = Uri.parse(url);
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(intent);
                } catch (Exception e) {
                    new AlertDialog.Builder(getBaseContext())
                            .setMessage("未检测到微信客户端，请安装后重试。")
                            .setPositiveButton("知道了", null).show();
                }
                return true;
            } else if (url.contains("wx.tenpay.com/cgi-bin/mmpayweb-bin/checkmweb")) {
                HashMap<String, String> extraHeaders = new HashMap<>();
                extraHeaders.put("Referer", "https://www.zhongan.com");
                view.loadUrl(url, extraHeaders);
                return true;
            } else {
                Uri uri = Uri.parse(url);
                Log.d("Seven", "uri -> " + uri);
                if ("create-chain".equals(uri.getScheme())) {
                    String host = uri.getHost();
                    if (host != null) {
                        switch (host) {
                            case "jump":
                                String targe = uri.getQueryParameter("targe");
                                switch (targe) {
                                    case "goods": {
                                        String skuid = uri.getQueryParameter("skuid");
                                        Intent intent = new Intent(WebViewJavaActivity.this, NewProductDetailActivity.class);
                                        intent.putExtra(Key.SKU_ID, skuid);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                        break;
                                    case "check_liveness":
                                        if (helper == null) {
                                            helper = new LivenessHelper();
                                        }
                                        helper.start(WebViewJavaActivity.this);
                                        break;
                                    case "authentication":
                                        Intent intent2 = new Intent(WebViewJavaActivity.this, AuthPhoneActivity.class);
                                        SharedPreferences sharedPreferences = WebViewJavaActivity.this.getSharedPreferences("authType", 0);
                                        sharedPreferences.edit().putString("authType", "UserCenter").commit();
                                        intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
                                        startActivity(intent2);
                                        break;
                                    case "product_detail":
                                        String skuId = uri.getQueryParameter("sku_id");
                                        if (skuId == null || skuId.isEmpty()) {
                                            break;
                                        } else {
                                            Intent intent = new Intent(getBaseContext(), NewProductDetailActivity.class);
                                            intent.putExtra(Key.SKU_ID, skuId);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }

                                        break;
                                    case "customPage":
                                        String pageId = uri.getQueryParameter("pageId");
                                        if (pageId == null || pageId.isEmpty()) {
                                            break;
                                        }
                                        Intent intent = new Intent(getBaseContext(), CustomPageActivity.class);
                                        intent.putExtra("pageId", pageId);
                                        startActivity(intent);
                                        break;
                                    case "community":
                                        startActivity(new Intent(getBaseContext(), CommunityActivity.class));
                                        break;
                                    case "otherApp":
                                        Intent intentByPackageName = getAppOpenIntentByPackageName("com.taobao.taobao");
                                        if (intentByPackageName == null) {
                                            ToastUtil.error("检查到您手机没有安装淘宝，请安装后使用该功能");
                                        } else {
                                            startActivity(intentByPackageName);
                                        }
                                        break;
                                    case "taobao_authorize":    //淘宝授权
                                        OutLinkUtils.aliLoginFromH5(WebViewJavaActivity.this);
                                        break;
                                    default:
                                        break;
//                                        CSUtils.start(getBaseContext(), targe, "102a49c45bb54dbbb2394becdaffbce2");
                                }
                                break;
                            case "share": {
                                String target = uri.getQueryParameter("targe");
                                if ("scrollScreenShot".equals(target)) {
                                    // 滚动截屏
                                    scrollScreenShot();
                                    break;
                                }
                                if ("publicImg".equals(target)) {
                                    String imgUrl = uri.getQueryParameter("imgUrl");
                                    if (TextUtils.isEmpty(imgUrl)) {
                                        ToastUtil.error("链接为空!");
                                        break;
                                    }
                                    WebViewShotShareActivity.start(WebViewJavaActivity.this,
                                            imgUrl);
                                    break;
                                }
                                if ("image".equals(uri.getQueryParameter("content_type"))) {
                                    toShareActivity(uri);
                                    break;
                                }
                                if ("wechat".equals(target)) {
                                    ShareUtils.showShareDialog(WebViewJavaActivity.this, OutLinkUtils.getClipboard(false));
                                    break;
                                }
                            }
                                break;

                            default:
                                break;
                        }
                    }
                    return true;
                } else {
                    if (!hideToolbar && uri.getHost().equals(BuildConfig.XYSH_HOST)) {
                        close.setVisibility(View.GONE);
                    }
                    return super.shouldOverrideUrlLoading(view, url);
                }
            }
        }

        private void toShareActivity(Uri parse) {
            if (!isStartingShare) {
                isStartingShare = true;
                final String imageUrl = Uri.decode(parse.getQueryParameter("share_content"));
                ToastUtil.showLoading(WebViewJavaActivity.this);
                Glide.with(WebViewJavaActivity.this).load(imageUrl).into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        if (resource != null) {
                            Bundle bundle = new Bundle();
                            File saveFile = getSaveFile();
                            int w = resource.getIntrinsicWidth();
                            int h = resource.getIntrinsicHeight();

                            // 取 drawable 的颜色格式
                            Bitmap.Config config = resource.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
                                    : Bitmap.Config.RGB_565;
                            // 建立对应 bitmap
                            Bitmap bitmap = Bitmap.createBitmap(w, h, config);
                            // 建立对应 bitmap 的画布
                            Canvas canvas = new Canvas(bitmap);
                            resource.setBounds(0, 0, w, h);
                            // 把 drawable 内容画到画布中
                            resource.draw(canvas);

//                            BitmapFactory.decodeResource(resource, R.drawable.icon);
                            saveBitmapFile(bitmap, saveFile, false);
                            bundle.putString("imageUrl", imageUrl);
                            bundle.putString("f", saveFile.getAbsolutePath());
                            AgentFActivity.start(WebViewJavaActivity.this, XyshFragment.class, bundle);
                        }
                        ToastUtil.hideLoading();
                        isStartingShare = false;
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        ToastUtil.hideLoading();
                        isStartingShare = false;
                    }
                });
//                Picasso.with(WebViewJavaActivity.this).load(imageUrl).into(new SimpleDraweeView(Bitmap));
                /*Glide.with(WebViewJavaActivity.this).load(imageUrl).asBitmap().into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        if (resource != null) {
                            Bundle bundle = new Bundle();
                            File saveFile = getSaveFile();
                            saveBitmapFile(resource, saveFile, false);
                            bundle.putString("imageUrl", imageUrl);
                            bundle.putString("f", saveFile.getAbsolutePath());
                            AgentFActivity.start(WebViewJavaActivity.this, XyshFragment.class, bundle);
                        }
                        ToastUtil.hideLoading();
                        isStartingShare = false;

                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        ToastUtil.hideLoading();
                        isStartingShare = false;
                    }
                });*/
            }
        }
    };

    public Intent getAppOpenIntentByPackageName(String packageName){
        String mainAct = null;
        PackageManager pkgMag = getPackageManager();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED|Intent.FLAG_ACTIVITY_NEW_TASK);

        @SuppressLint("WrongConstant") List<ResolveInfo> list = pkgMag.queryIntentActivities(intent, PackageManager.GET_ACTIVITIES);
        for (int i = 0; i < list.size(); i++) {
            ResolveInfo info = list.get(i);
            if (info.activityInfo.packageName.equals(packageName)) {
                mainAct = info.activityInfo.name;
                break;
            }
        }
        if (TextUtils.isEmpty(mainAct)) {
            return null;
        }
        intent.setComponent(new ComponentName(packageName, mainAct));
        return intent;
    }

    private void saveBitmapFile(Bitmap bitmap, File file, boolean isShow) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
            if (isShow) {
                ToastUtil.success("保存成功");
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                intent.setData(Uri.fromFile(file));
                sendBroadcast(intent);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File getSaveFile() {
        String dirPath = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM).getPath() + "/" + AppUtils.getAppName(this);

        File file = new File(dirPath, System.currentTimeMillis() + ".jpg");
        FileUtils.createOrExistsDir(dirPath);
        return file;
    }

    private void setVisibility(View view, int visibility) {
        if (view == null || view.getVisibility() == visibility) {
            return;
        }
        view.setVisibility(visibility);
    }

    private void scrollScreenShot() {
//        ToastUtil.showLoading(this, true);
        File file = WebViewUtil.shotWebView(mWeb.getWebCreator().get());
//        Log.d("Seven", "file -> " + file.getAbsolutePath());
        if (file != null) {
            WebViewShotShareActivity.start(WebViewJavaActivity.this,
                    file.getAbsolutePath(), true);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case taobaoAuthorize:
                openWeb(mUrl);
                break;
        }
    }

    class AndroidInterface {
        @JavascriptInterface
        public void setTitle(final String titleStr) {
            setTitle(titleStr);
            getHeaderLayout().setOnRightClickListener(null);
            getHeaderLayout().setRightText("");
        }
    }
}
