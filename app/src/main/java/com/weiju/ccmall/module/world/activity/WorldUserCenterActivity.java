package com.weiju.ccmall.module.world.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/4/25.
 */
public class WorldUserCenterActivity extends BaseActivity {
    @BindView(R.id.sdvUserIcon)
    SimpleDraweeView sdvUserIcon;
    @BindView(R.id.tvNickName)
    TextView tvNickName;
    private User mUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_user_center);
        ButterKnife.bind(this);
        QMUIStatusBarHelper.translucent(this);
        initView();
    }

    private void initView() {
        if (SessionUtil.getInstance().isLogin()) {
            mUser = SessionUtil.getInstance().getLoginUser();
            FrescoUtil.setImage(sdvUserIcon, mUser.avatar);
            tvNickName.setText(mUser.nickname);
        }

    }

    @OnClick({R.id.tvAllOrder, R.id.tvNotDelivery, R.id.tvDelivered, R.id.tvComplete, R.id.ivBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvAllOrder:
                WorldOrderListActivity.start(this, "");
                break;
            case R.id.tvNotDelivery:
                WorldOrderListActivity.start(this, "3");
                break;
            case R.id.tvDelivered:
                WorldOrderListActivity.start(this, "4");
                break;
            case R.id.tvComplete:
                WorldOrderListActivity.start(this, "5");
                break;
            case R.id.ivBack:
                finish();
                break;
        }
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, WorldUserCenterActivity.class);
        context.startActivity(intent);
    }
}
