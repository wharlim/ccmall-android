package com.weiju.ccmall.module.xysh.fragment

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.qmuiteam.qmui.util.QMUIStatusBarHelper
import com.tbruyelle.rxpermissions.RxPermissions
import com.weiju.ccmall.R
import com.weiju.ccmall.shared.basic.AgentFActivity
import com.weiju.ccmall.shared.basic.AgentFragment
import com.weiju.ccmall.shared.util.*
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.PicassoEngine
import com.zhihu.matisse.internal.entity.CaptureStrategy
import kotlinx.android.synthetic.main.fragment_credit.*
import top.zibin.luban.Luban
import top.zibin.luban.OnCompressListener
import java.io.File

/**
 * Created by 林先生_lcy.
 * User: lcy
 * Date: 2019/3/30
 * Time: 上午11:51
 * 实名认证页
 */
class CreditFragment : AgentFragment() {

    private var ID_faceUrl: String? = null
    private var ID_conUrl: String? = null

    override fun layoutId(): Int {
        return R.layout.fragment_credit
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        QMUIStatusBarHelper.setStatusBarDarkMode(activity)
        QMUIStatusBarHelper.translucent(activity, resources.getColor(R.color.red))
        activity?.title = "实名认证"
        val toolbar = activity?.findViewById<Toolbar>(R.id.toolbar)
        toolbar?.setBackgroundColor(resources.getColor(R.color.red))
        toolbar?.setNavigationIcon(R.drawable.ioco)
        toolbar?.findViewById<TextView>(R.id.toolbarTitle)?.setTextColor(-1)
        super.onViewCreated(view, savedInstanceState)
        ID_face.setOnClickListener {
            showDiagram( R.drawable.example_diagram_o, 1080)
        }
        ID_con.setOnClickListener {
            showDiagram(R.drawable.example_diagram_t, 1090)
        }
        init()
        next.setOnClickListener {
            when {
                ID_faceUrl == null -> ToastUtil.error("请上传身份证正面！")
                ID_conUrl == null -> ToastUtil.error("请上传身份证反面！")
                TextUtils.isEmpty(name.text) -> ToastUtil.error("请填写姓名！")
                TextUtils.isEmpty(ID.text) -> ToastUtil.error("请填写身份正号！")
                else -> {
                    saveInfo()
                    AgentFActivity.start2(this, PictureFragment::class.java)
                }
            }
        }

//        if (SessionUtil.getInstance().getString("diagram_xysh") == null) {
//            showHIdeDiagram(View.VISIBLE)
//            diagram_bg.setOnClickListener { }
//            var index = 0
//            diagram_text.setOnClickListener {
//                if (index == 0) {
//                    diagram.setBackgroundResource(R.drawable.example_diagram_t)
//                    index++
//                } else {
//                    SessionUtil.getInstance().putString("diagram_xysh", "1")
//                    showHIdeDiagram(View.GONE)
//                }
//            }
//        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == 1015)
            activity?.finish()
        else if (data != null) {
            val array = data.getSerializableExtra("extra_result_selection_path") as? ArrayList<String>?
            if (array != null && array.isNotEmpty()) {
                if (requestCode == 1080) {
                    scale(array[0])
                    Glide.with(this).load(array[0]).into(ID_face)
                } else if (requestCode == 1090) {
                    scale(array[0], 1)
                    Glide.with(this).load(array[0]).into(ID_con)
                }
            }
        }
    }

    fun getID_FACE(): String {
        return ID_faceUrl!!
    }

    fun getID_CON(): String {
        return ID_conUrl!!
    }


    fun getName(): String {
        return name.text.toString()
    }


    fun getID_No(): String {
        return ID.text.toString()
    }

    private fun init() {
        val util = SessionUtil.getInstance()
        val n = util.getString("usrName")
        if (n != null)
            name.setText(n)
        val idNo = util.getString("idNo")
        if (idNo != null)
            ID.setText(idNo)
        ID_faceUrl = util.getString("idfrontpic")
        if (ID_faceUrl != null)
            Glide.with(this).load(ID_faceUrl).into(ID_face)
        ID_conUrl = util.getString("idbackpic")
        if (ID_conUrl != null)
            Glide.with(this).load(ID_conUrl).into(ID_con)
    }

    private fun saveInfo() {
        val util = SessionUtil.getInstance()
        util.putString("usrName", getName())
        util.putString("idNo", getID_No())
        util.putString("idfrontpic", ID_faceUrl)
        util.putString("idbackpic", ID_conUrl)
    }

    private fun selectImg(requestCode: Int) {
        val rxPermissions = RxPermissions(activity!!)
        rxPermissions.request(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        ).subscribe { aBoolean ->
            if (aBoolean!!) {
                Matisse.from(this)
                        .choose(MimeType.ofImage())
                        .countable(true)
                        .maxSelectable(1)
                        .capture(true)
                        .captureStrategy(CaptureStrategy(true, "com.weiju.ccmall.fileProvider"))
                        .imageEngine(PicassoEngine())
                        .forResult(requestCode)
            } else {
                PermissionsUtils.goPermissionsSetting(activity)
                ToastUtil.error("无法获得必要的权限")
            }
        }
    }

    /**
     * 压缩
     */
    private fun scale(path: String, type: Int = 0) {
        Luban.with(context?.applicationContext)
                .load(File(path))                              // 忽略不压缩图片的大小
                .setCompressListener(object : OnCompressListener {
                    override fun onSuccess(file: File?) {
                        if (type == 0)
                            ID_faceUrl = file?.absolutePath
                        else
                            ID_conUrl = file?.absolutePath
                        hideLoading()
                    }

                    override fun onError(e: Throwable?) {
                        hideLoading()
                    }

                    override fun onStart() {
                        showLoading()
                    } //设置回调

                }).launch()    //启动压缩
    }

    private fun showHideDiagram(isVisibility: Int) {
        diagram_bg.visibility = isVisibility
        diagram.visibility = isVisibility
        diagram_text.visibility = isVisibility
    }

    private fun showDiagram(resId: Int, requestCode: Int) {
        diagram.setBackgroundResource(resId)
        showHideDiagram(View.VISIBLE)
        diagram_bg.setOnClickListener { }
        diagram_text.setOnClickListener {
            showHideDiagram(View.GONE)
            selectImg(requestCode)
        }
    }

}
