package com.weiju.ccmall.module.transfer.fragments;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.pickUp.activity.PickUpProductListActivity;
import com.weiju.ccmall.module.pickUp.adapter.PickUpDetailAdapter;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.PickUp;
import com.weiju.ccmall.shared.bean.PickUpEx;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IPickUpService;

import java.util.List;

public class PickUpSelectListFragment extends BaseListFragment {

    private PickUpDetailAdapter mAdapter = new PickUpDetailAdapter();
    private IPickUpService mService = ServiceManager.getInstance().createService(IPickUpService.class);
    private int mCurrentPage = 1;

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        PickUp item = mAdapter.getItem(position);
        if (null == item) {
            return;
        }
        switch (view.getId()) {
            case R.id.tvUse:
                Intent intent = new Intent(getContext(), PickUpProductListActivity.class);
                intent.putExtra("vouchersId", item.vouchersId);
                startActivity(intent);
                break;
            default:
                item.isSelected = !item.isSelected;
                mAdapter.notifyDataSetChanged();
                break;
        }
    }

    @Override
    public void initView() {
        super.initView();
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.window_background));
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        mAdapter.setShowUseNow(false);
        return mAdapter;
    }

    public String getSelectedGoodsCodes() {
        List<PickUp> data = mAdapter.getData();
        String goodsCodes = "";
        for (int i = 0; i < data.size(); i++) {
            PickUp pickUp = data.get(i);
            if (pickUp.isSelected) {
                if (TextUtils.isEmpty(goodsCodes)) goodsCodes = pickUp.goodsCode;
                else goodsCodes += "," + pickUp.goodsCode;
            }
        }
        if (TextUtils.isEmpty(goodsCodes)) {
            return null;
        }
        return goodsCodes;
    }

    @Override
    public void getData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
        }
        APIManager.startRequest(mService.queryList(mCurrentPage, 15),
                new BaseRequestListener<PaginationEntity<PickUp, PickUpEx>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<PickUp, PickUpEx> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && result.list.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, getContext());
    }

    public static PickUpSelectListFragment newInstance() {
        PickUpSelectListFragment fragment = new PickUpSelectListFragment();
        return fragment;
    }
}
