package com.weiju.ccmall.module.world.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.address.AddressListActivity;
import com.weiju.ccmall.module.auth.AuthPhoneActivity;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.SubmitStatusActivity;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.auth.model.AuthModel;
import com.weiju.ccmall.module.world.IWorldService;
import com.weiju.ccmall.module.world.entity.CatgoryItemEntity;
import com.weiju.ccmall.module.world.entity.FreightEntity;
import com.weiju.ccmall.module.world.entity.OrderEntity;
import com.weiju.ccmall.module.world.entity.OrderRequestBodyEntity;
import com.weiju.ccmall.module.world.widgets.RealNameTipsDialogFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Address;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.PayTypeLayout;
import com.weiju.ccmall.shared.constant.Action;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.IAddressService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.WePayUtils;

import org.greenrobot.eventbus.EventBus;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorldPayActivity extends BaseActivity {
    @BindView(R.id.selectAddressTv)
    TextView selectAddressTv;
    @BindView(R.id.addressArrowIv)
    ImageView addressArrowIv;
    @BindView(R.id.phoneTv)
    TextView phoneTv;
    @BindView(R.id.contactsTv)
    TextView contactsTv;
    @BindView(R.id.addressDetailTv)
    TextView addressDetailTv;
    @BindView(R.id.addressInfoLayout)
    RelativeLayout addressInfoLayout;
    @BindView(R.id.addressLayout)
    LinearLayout addressLayout;
    @BindView(R.id.sdvGoodsIcon)
    SimpleDraweeView sdvGoodsIcon;
    @BindView(R.id.tvGoodsName)
    TextView tvGoodsName;
    @BindView(R.id.tvGoodsDesc)
    TextView tvGoodsDesc;
    @BindView(R.id.tvGoodsPrice)
    TextView tvGoodsPrice;
    @BindView(R.id.tvGoodsCount)
    TextView tvGoodsCount;
    @BindView(R.id.layoutPayType)
    PayTypeLayout layoutPayType;
    @BindView(R.id.lyPayType)
    LinearLayout lyPayType;
    @BindView(R.id.tvTotal)
    TextView tvTotal;
    @BindView(R.id.tvSubtotal)
    TextView tvSubtotal;
    @BindView(R.id.confirmBtn)
    TextView confirmBtn;
    @BindView(R.id.tvFreight)
    TextView tvFreight;
    @BindView(R.id.tvRate)
    TextView tvRate;
    @BindView(R.id.llRate)
    LinearLayout llRate;
    @BindView(R.id.tvCCM)
    TextView tvCCM;

    private Address mAddress;
    private IAddressService mAddressService;
    private IWorldService mService = ServiceManager.getInstance().createService(IWorldService.class);

    private IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
    private CatgoryItemEntity mProduct;

    private long mFreight = 0L;//邮费
    private RealNameTipsDialogFragment mTipsDialogFragment;

    private String mOrderCode;
    private Long mTaxFee;//税费

    public static void start(Context context, CatgoryItemEntity product) {
        Intent intent = new Intent(context, WorldPayActivity.class);
        intent.putExtra("product", product);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world__pay);
        ButterKnife.bind(this);
        setTitle("结算付款");
        setLeftBlack();
        initView();
        initData();

    }

    private void initView() {
        mProduct = (CatgoryItemEntity) getIntent().getExtras().get("product");
        if (mProduct != null) {
            List<String> mainImages = mProduct.mainImages;
            if (mainImages.size() > 0) {
                FrescoUtil.setImageSmall(sdvGoodsIcon, mainImages.get(0));
            }
            tvGoodsName.setText(mProduct.skuName);
            tvGoodsDesc.setText(mProduct.attr);
            tvGoodsPrice.setText(MoneyUtil.centToYuan¥Str(mProduct.price));
            tvGoodsCount.setText(MessageFormat.format("x{0}", mProduct.quantity));
            initTotalView();
        }

        layoutPayType.setOnlyWeChat();
        initCCM();
    }

    private void initCCM() {
        double countRateExc = mProduct.countRateExc * mProduct.quantity;
        if (countRateExc > 100) {
            countRateExc = 100;
        }
        tvCCM.setText(Html.fromHtml(String.format("预计可返<font color=#8B57F4>%s%%</font>算率", countRateExc)));
    }

    private void initTotalView() {
        String money = MoneyUtil.centToYuan¥Str(mProduct.price * mProduct.quantity + mFreight + (mTaxFee == null ? 0 : mTaxFee));
        tvSubtotal.setText(money);
        tvTotal.setText(money);
    }

    private void initData() {
        mAddressService = ServiceManager.getInstance().createService(IAddressService.class);
        loadDefaultAddress();
    }

    private void loadDefaultAddress() {
        APIManager.startRequest(mAddressService.getDefaultAddress(), new BaseRequestListener<Address>(this) {
            @Override
            public void onSuccess(Address result) {
                mAddress = result;
                initAddressView();
                loadFreight();
            }

            @Override
            public void onComplete() {
            }
        }, this);
    }


    private void loadFreight() {
        if (mAddress == null) {
            return;
        }
        APIManager.startRequest(mService.getPostFee(mAddress.addressId, mProduct.itemId, mProduct.quantity), new BaseRequestListener<FreightEntity>(this) {
            @Override
            public void onSuccess(FreightEntity result) {
                super.onSuccess(result);
                mFreight = MoneyUtil.yuanStrToCentLong(result.postFee);
                mTaxFee = result.taxFee;
                if (result.tax > 0) {
                    double tax = MoneyUtil.keepTwoDecimals(result.tax * 100);
                    tvRate.setText(MessageFormat.format("{0}%（估：{1}）", tax, MoneyUtil.centToYuan¥Str(mTaxFee)));
                    llRate.setVisibility(View.VISIBLE);
                } else {
                    llRate.setVisibility(View.GONE);
                }
                if (mFreight > 0) {
                    tvFreight.setText(MoneyUtil.centToYuan¥Str(mFreight));
                } else {
                    tvFreight.setText("包邮");
                }
                initTotalView();
            }

        }, this);
    }

    private void initAddressView() {
        if (mAddress == null) {
            selectAddressTv.setVisibility(View.VISIBLE);
            addressInfoLayout.setVisibility(View.GONE);
        } else {
            selectAddressTv.setVisibility(View.GONE);
            addressInfoLayout.setVisibility(View.VISIBLE);

            contactsTv.setText(String.format("收货人：%s", mAddress.contacts));
            phoneTv.setText(mAddress.phone);
            addressDetailTv.setText(String.format("收货地址：%s", mAddress.getFullAddress()));
        }
    }

    private void showRealNameTips() {
        if (mTipsDialogFragment == null){
            mTipsDialogFragment = RealNameTipsDialogFragment.newInstance();
            mTipsDialogFragment.setOnConfirmListener(() -> {
                OrderRequestBodyEntity bodyEntity = new OrderRequestBodyEntity();
                bodyEntity.addressId = mAddress.addressId;
                bodyEntity.originType = 1;
                List<OrderRequestBodyEntity.Item> items = new ArrayList<>();
                OrderRequestBodyEntity.Item item = new OrderRequestBodyEntity.Item();
                item.itemId = mProduct.itemId;
                item.quantity = mProduct.quantity;
                items.add(item);
                bodyEntity.item = items;
                APIManager.startRequest(mService.addOrder(bodyEntity), new BaseRequestListener<OrderEntity>(this) {
                    @Override
                    public void onSuccess(OrderEntity result) {
                        super.onSuccess(result);
                        ToastUtil.success("下单成功");
                        mOrderCode = result.merchantOrderNo;
                        WePayUtils.payByWeChatMini(WorldPayActivity.this, result.merchantOrderNo);
                    }
                }, this);
            });
        }
        mTipsDialogFragment.show(getSupportFragmentManager(), "realNameTipsDialogFragment");
    }

    @OnClick({R.id.addressLayout, R.id.confirmBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addressLayout:
                Intent intent = new Intent(this, AddressListActivity.class);
                intent.putExtra("action", Key.SELECT_ADDRESS);
                startActivityForResult(intent, Action.SELECT_ADDRESS);
                break;
            case R.id.confirmBtn:
                if (mAddress == null || mAddress.addressId == null || mAddress.addressId.isEmpty()) {
                    ToastUtil.error("请选择收货地址");
                    return;
                }
                if (mTaxFee == null){
                    ToastUtil.error("网络异常，请重试");
                    return;
                }
                User user = SessionUtil.getInstance().getLoginUser();
                if (user.autoAuthStatus != AppTypes.AUTH_STATUS.SUCESS) {
                    goAuth(user);
//                    showRealNameTips();
                } else {
                    showRealNameTips();
                }
                break;
        }
    }
    /**
     * 实名认证
     * @param user
     */
    private void goAuth(User user) {
        UserService.checkHasPassword(this, () -> {
            switch (user.autoAuthStatus) {
                case AppTypes.AUTH_STATUS.WAIT:
                    APIManager.startRequest(mUserService.getAuth(), new BaseRequestListener<AuthModel>(WorldPayActivity.this) {
                        @Override
                        public void onSuccess(AuthModel model) {
                            startActivity(new Intent(WorldPayActivity.this, SubmitStatusActivity.class));
                            MsgStatus msgStatus = new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_FAIL);
                            msgStatus.setTips(model.memberAuthBean.checkRemark);
                            EventBus.getDefault().postSticky(msgStatus);
                        }
                    }, WorldPayActivity.this);
                    break;
                case AppTypes.AUTH_STATUS.NO_SUBMIT:
                    Intent intent2 = new Intent(WorldPayActivity.this, AuthPhoneActivity.class);
                    SharedPreferences sharedPreferences = WorldPayActivity.this.getSharedPreferences("authType", 0);
                    sharedPreferences.edit().putString("authType", "UserCenter").commit();
                    intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
                    startActivity(intent2);
                    break;
                default:
                    break;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == Action.SELECT_ADDRESS) {
            Address address = (Address) data.getExtras().get("address");
            if (address != null) {
                mAddress = address;
                initAddressView();
                loadFreight();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(mOrderCode)) {
            EventBus.getDefault().post(new EventMessage(Event.worldAddOrderSuccess));
            WorldOrderDetailActivity.start(this, mOrderCode);
            finish();
        }
    }

    /*@Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(PayMsg msgStatus) {
        switch (msgStatus.getAction()) {
            case PayMsg.ACTION_WXPAY_SUCCEED:
                EventBus.getDefault().post(new EventMessage(Event.paySuccess));
            case PayMsg.ACTION_WXPAY_FAIL:
                ToastUtil.error(msgStatus.message);
//                EventUtil.viewOrderDetail(this, mOrderCode, false);
//                getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
//                finish();
                break;
            default:
        }
    }*/
}
