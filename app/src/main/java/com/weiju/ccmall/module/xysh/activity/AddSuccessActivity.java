package com.weiju.ccmall.module.xysh.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/13 on 10:12
 * @desc
 */
public class AddSuccessActivity extends BaseActivity {

    @BindView(R.id.tvDesc)
    TextView mTvDesc;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xinyong_add_success);
        ButterKnife.bind(this);
        String title = getIntent().getStringExtra("title");
        setTitle(TextUtils.isEmpty(title) ? "添加成功" : title);
        setLeftBlack();
        mTvDesc.setText(TextUtils.isEmpty(title) ? "添加成功" : title);
    }

    @OnClick(R.id.tvCommit)
    protected void commit() {
        finish();
    }


}
