package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.adapter.CardDoctorHistoryAdapter;
import com.weiju.ccmall.module.xysh.bean.CardDoctorHistoryItem;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CardDoctorHistoryActivity extends BaseListActivity {

    private CardDoctorHistoryAdapter mAdapter;
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mAdapter = new CardDoctorHistoryAdapter();
        super.onCreate(savedInstanceState);
    }

    @Override
    public String getTitleStr() {
        return "历史评测结果";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        CardDoctorHistoryItem item = mAdapter.getData().get(position);
        Intent intent = new Intent(this, WebViewJavaActivity.class);
        intent.putExtra("url", item.productResult);
        startActivity(intent);
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_card_doctor_history;
    }

    @Override
    public void getData(boolean isRefresh) {
//        List<String> list = new ArrayList<>();
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        mAdapter.setNewData(list);
//        mRefreshLayout.setRefreshing(false);
        APIManager.startRequest(service.getCreditCardAppraisalByUserNo(
                SessionUtil.getInstance().getOAuthToken()
        ), new Observer<XYSHCommonResult<List<CardDoctorHistoryItem>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<List<CardDoctorHistoryItem>> ret) {
                if (ret.success) {
                    mAdapter.setNewData(ret.data);
                    mAdapter.loadMoreEnd(true);
                    mRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public static void start(Context context) {
        Intent activity = new Intent(context, CardDoctorHistoryActivity.class);
        context.startActivity(activity);
    }
}
