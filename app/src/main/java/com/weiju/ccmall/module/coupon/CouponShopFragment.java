package com.weiju.ccmall.module.coupon;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weiju.ccmall.module.collect.CollectShopFragment;
import com.weiju.ccmall.module.collect.bean.ShopCollectItem;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICouponService;

import static com.weiju.ccmall.module.coupon.CouponShopActivity.KEY_COUPON_ID;

import io.reactivex.Observable;

/**
 * 某一张优惠券可用的店铺列表
 */
public class CouponShopFragment extends CollectShopFragment {

    private ICouponService mCouponService;
    private String mCouponId;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        mCouponService = ServiceManager.getInstance().createService(ICouponService.class);
        mCouponId = getArguments().getString(KEY_COUPON_ID);
        mNoDataLayout.setTextView("亲，该优惠券暂时无可用店铺哦");
        collectShopAdapter.setShowCollectState(false);
        return v;
    }

    @Override
    protected Observable<RequestResult<PaginationEntity<ShopCollectItem, Object>>>
        getRequestInterface(int page) {
        return mCouponService.getStoreList(mCouponId, page);
    }

    public static CouponShopFragment newInstance(String mCouponId) {
        Bundle args = new Bundle();
        args.putString(KEY_COUPON_ID, mCouponId);
        CouponShopFragment fragment = new CouponShopFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
