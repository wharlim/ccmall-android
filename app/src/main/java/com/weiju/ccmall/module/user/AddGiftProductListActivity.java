package com.weiju.ccmall.module.user;

import android.content.Intent;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.utils.KeyboardUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.module.product.adapter.ProductAdapter;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/11/6 on 10:33
 * @desc ${TODD}
 */
public class AddGiftProductListActivity extends BaseListActivity {

    @BindView(R.id.cleanBtn)
    protected ImageView mCleanBtn;
    @BindView(R.id.keywordEt)
    protected EditText mKeywordEt;

    private ProductAdapter mAdapter = new ProductAdapter();
    private String mKeyword;
    private IProductService mFootService = ServiceManager.getInstance().createService(IProductService.class);

    @Override
    public void initView() {
        super.initView();
        EventBus.getDefault().register(this);
        mAdapter.isAdd(true);
        mKeywordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mCleanBtn.setVisibility(charSequence.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mKeywordEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH || (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    search(textView.getText().toString());
                    return true;
                }
                return false;
            }
        });
    }

    private void search(String keyword) {
        if (TextUtils.isEmpty(keyword)) {
            ToastUtil.error("请输入查找内容");
            return;
        }
        mKeyword = keyword;
        mCurrentPage = 1;
        getData(true);
        KeyboardUtils.hideSoftInput(AddGiftProductListActivity.this);
    }

    @OnClick(R.id.cleanBtn)
    protected void cleanKeyword() {
        mKeywordEt.setText("");
    }

    @Override
    public String getTitleStr() {
        return "添加礼包";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        SkuInfo item = mAdapter.getItem(position);
        if (null != item) {
            Intent intent = new Intent(AddGiftProductListActivity.this, NewProductDetailActivity.class);
            intent.putExtra("type", NewProductDetailActivity.TYPE_ADD_GIFT);
            intent.putExtra(Key.SKU_ID, item.skuId);
            startActivity(intent);
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mFootService.getProductPackages(mKeyword, mCurrentPage, Constants.PAGE_SIZE),
                new BaseRequestListener<PaginationEntity<SkuInfo, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && result.list.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, AddGiftProductListActivity.this);
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_add_gift_product;
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(EventMessage message) {
        switch (message.getEvent()) {
            case joinProduct:
            case delectProduct:
                mCurrentPage = 1;
                getData(true);
                break;
            default:
        }
    }
}
