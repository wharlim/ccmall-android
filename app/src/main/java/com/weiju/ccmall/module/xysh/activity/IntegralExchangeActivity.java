package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.SelectSupportBankItem;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.module.xysh.helper.UpdateBankNameWatcher;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.Bank;
import com.weiju.ccmall.shared.constant.Action;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * 积分兑换
 */
public class IntegralExchangeActivity extends BaseActivity {

    @BindView(R.id.etBankCardId)
    EditText etBankCardId;
    @BindView(R.id.tvBankName)
    TextView tvBankName;
    @BindView(R.id.etPhone)
    EditText etPhone;
    @BindView(R.id.tvConfirm)
    TextView tvConfirm;

    String bankName;
    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    private static final int REQ_SELECT_DEBIT = 1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_integral_exchange);
        ButterKnife.bind(this);
        setTitle("积分兑换");
        setLeftBlack();
        etBankCardId.addTextChangedListener(new UpdateBankNameWatcher(this::updateBankName));
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, IntegralExchangeActivity.class);
        context.startActivity(intent);
    }

    @OnClick({R.id.ll_select_bank, R.id.tvConfirm, R.id.ivSelectDebit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_select_bank:
                SelectSupportBankActivity.startForSelect(this, Action.SELECT_BANK);
                break;
            case R.id.tvConfirm:
                confirm();
                break;
            case R.id.ivSelectDebit:
                BankAdminActivity.startForSelectDebit(this, REQ_SELECT_DEBIT);
                break;
        }
    }

    private void updateBankName(String bankName) {
        this.bankName = bankName;
        tvBankName.setText(bankName);
    }

    private void confirm() {
        String cardNo = etBankCardId.getText().toString().trim();
        if (TextUtils.isEmpty(cardNo)) {
            ToastUtil.error("请输入储蓄银行卡");
            return;
        }
        if (TextUtils.isEmpty(bankName)) {
            ToastUtil.error("请选择银行");
            return;
        }
        if (!UiUtils.checkETPhone(etPhone)) {
            return;
        }
        String phone = etPhone.getText().toString().trim();

        BankUtils.checkCard(this, cardNo, BankUtils.CARD_CHECK_ACCEPT_DEBIT, (pass, t) -> {
            if (pass) {
                ToastUtil.showLoading(this, true);
                APIManager.startRequest(service.getIntegralUrl(cardNo, bankName, phone), new Observer<XYSHCommonResult<String>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(XYSHCommonResult<String> ret) {
                        ToastUtil.hideLoading();
                        if (ret.code == 0) {
//                            ToastUtil.success("兑换成功！");
//                            finish();
                            Intent intent = new Intent(IntegralExchangeActivity.this, WebViewJavaActivity.class);
                            intent.putExtra("url", ret.data);
                            startActivity(intent);
                        } else {
                            ToastUtil.error(ret.msg);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
            } else {
                ToastUtil.error("卡号不正确");
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == Action.SELECT_BANK && data != null) {
                SelectSupportBankItem bank = SelectSupportBankActivity.getSelectResult(data);
                updateBankName(bank.bankName);
            } else if (requestCode == REQ_SELECT_DEBIT && data != null) {
                QueryUserBankCardResult.BankInfListBean ret = BankAdminActivity.getSelectResult(data);
                etBankCardId.setText(ret.cardNo);
            }
        }
    }
}
