package com.weiju.ccmall.module.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.blankj.utilcode.utils.AppUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.notice.NoticeDetailsActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Chan on 2017/6/10.
 */

public class AboutUsActivity extends BaseActivity {
    @BindView(R.id.tvName)
    TextView mTvName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus_layout);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        showHeader();
        setTitle("关于我们");
        setLeftBlack();
        mTvName.setText(AppUtils.getAppName(this) + "  v" + AppUtils.getAppVersionName(this));
    }

    @OnClick(R.id.llPrivacyPolicy)
    public void onViewClicked() {
        Intent intent = new Intent(this, NoticeDetailsActivity.class);
        intent.putExtra("isPrivacyPolicy", true);
        startActivity(intent);
    }
}
