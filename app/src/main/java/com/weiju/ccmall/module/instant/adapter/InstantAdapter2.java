package com.weiju.ccmall.module.instant.adapter;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.component.InstantProgressBar;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import java.util.List;

public class InstantAdapter2 extends BaseQuickAdapter<SkuInfo, BaseViewHolder> {

    public InstantAdapter2(@Nullable List<SkuInfo> data) {
        super(R.layout.item_instant_product, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SkuInfo item) {
        helper.getView(R.id.layoutRoot).setBackgroundColor(Color.TRANSPARENT);
        SimpleDraweeView ivImage = helper.getView(R.id.ivImg);
        FrescoUtil.setSkuImgSmallMask(ivImage, item);
        helper.setText(R.id.tvTitle, item.name);
        helper.setText(R.id.tvRetailPrice, MoneyUtil.centToYuan¥StrNoZero(item.retailPrice));
        helper.setText(R.id.tvMarketPrice, MoneyUtil.centToYuan¥StrNoZero(item.marketPrice));
        helper.setText(R.id.tvCCM, String.format("奖%s%%算率", item.countRateExc));

        TextView tvMarketPrice = helper.getView(R.id.tvMarketPrice);
        TextViewUtil.addThroughLine(tvMarketPrice);

        long startTime = 0;
        if (!StringUtils.isEmpty(item.sellBegin)) {
            startTime = TimeUtils.string2Millis(item.sellBegin);
        }
        long endTime = 0;
        if (!StringUtils.isEmpty(item.sellEnd)) {
            endTime = TimeUtils.string2Millis(item.sellEnd);
        }

        long currentTime = System.currentTimeMillis();
        TextView tvGo = helper.getView(R.id.tvGo);
        if (currentTime < startTime) {
            tvGo.setText("即将开抢");
            tvGo.setSelected(false);
        } else if (currentTime > endTime) {
            tvGo.setText("已结束");
            tvGo.setSelected(true);
        } else {
            tvGo.setSelected(false);
            tvGo.setText("马上抢");
            helper.addOnClickListener(R.id.tvGo);
        }

        InstantProgressBar progressBar = helper.getView(R.id.progressBar);
        if (item.quantity == 0) {
            progressBar.setProgress(0);
        } else if (item.sales >= item.quantity || item.stock == 0) {
            LogUtils.e("总数" + item.quantity + "  当前销量" + item.sales);
            progressBar.setProgress(100);
            if (!tvGo.isSelected()) {
                tvGo.setText("抢光了");
                tvGo.setSelected(true);
            }
        } else {
            progressBar.setProgress((int) (100 * item.sales / item.quantity));
        }

        helper.getView(R.id.ivBanjia).setVisibility(View.GONE);

    }
}
