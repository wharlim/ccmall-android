package com.weiju.ccmall.module.face;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.linkface.liveness.LFLivenessSDK;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.face.util.LFMediaPlayer;
import com.weiju.ccmall.module.face.util.LFProtoBufUtil;
import com.weiju.ccmall.module.face.util.LFReturnResult;
import com.weiju.ccmall.module.face.util.LFSensorManager;
import com.weiju.ccmall.module.face.widget.CircleTimeView;
import com.weiju.ccmall.module.face.widget.LFGifView;
import com.weiju.ccmall.module.face.widget.LinkfaceAlertDialog;
import com.weiju.ccmall.module.face.widget.TimeViewContoller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@SuppressLint("NewApi")
public class LivenessActivity extends Activity implements Constants{
    private static final String TAG = "LivenessActivity";

    /**
     * 加载库文件出现错误
     */
    public static final int RESULT_CREATE_HANDLE_ERROR = 1001;

    /**
     * 无法访问摄像头，没有权限或摄像头被占用
     */
    public static final int RESULT_CAMERA_ERROR_NOPRERMISSION_OR_USED = 2;

    /**
     * 内部错误
     */
    public static final int RESULT_INTERNAL_ERROR = 3;

    /**
     * 包名绑定错误
     */
    public static final int RESULT_SDK_INIT_FAIL_APPLICATION_ID_ERROR = 4;

    /**
     * sdk初始化失败,模型加载失败
     */
    public static final int RESULT_SDK_INIT_FAIL_OUT_OF_DATE = 5;

    /**
     * license过期
     */
    public static final int RESULT_SDK_INIT_FAIL_LICENSE_OUT_OF_DATE = 6;

    /**
     * license读取失败
     */
    public static final int RESULT_FAILED_NO_LIC_PATH = 7;

    /**
     * 传入保存结果的文件路径
     */
    public static String EXTRA_RESULT_PATH = "com.linkface.liveness.resultPath";

    /**
     * 传入活体检测的动作序列
     */
    public static final String EXTRA_MOTION_SEQUENCE = "com.linkface.liveness.motionSequence";

    /**
     * 获取info信息
     */
    public static final String EXTRA_INFO = "com.linkface.liveness.info";

    /**
     * 是否打开语音提示
     */
    public static final String SOUND_NOTICE = "soundNotice";

    /**
     *  输出类型
     */
    public static final String OUTTYPE = "outType";

    /**
     * 设置复杂度
     */
    public static final String COMPLEXITY = "complexity";

    /**
     * 返回加密结果
     * */
    public static final String KEY_DETECT_RESULT = "key_detect_result";

    /**
     * 设置是否返回图片结果
     */
    public static final String KEY_DETECT_IMAGE_RESULT = "key_detect_image_result";

    /**
     * 设置是否返回video结果,只有video模式才会返回
     */
    public static final String KEY_DETECT_VIDEO_RESULT = "key_detect_video_result";

    /**
     * 设置是否返回protobuf结果
     * */
    public static final String KEY_DETECT_PROTO_BUF_RESULT = "key_detect_proto_buf_result";

    public static final String LIVENESS_FILE_NAME = "livenessResult";
    public static final String LIVENESS_VIDEO_NAME = "livenessVideoResult.mp4";

    public static String SEQUENCE_JSON = "sequence_json";

    public static String OUTPUT_TYPE = "";

    private static final int CURRENT_ANIMATION = -1;

    private LFGifView mGvView;
    private TextView mNoteTextView;
    private ViewGroup mVGBottomDots;
    private View mAnimFrame;
    private CircleTimeView mTimeView;
    private RelativeLayout mWaitDetectView;
    private LinearLayout mLlytTitle;

    private Context mContext;
    private LinkfaceAlertDialog mDialog;
    private boolean mIsStart = false, mSoundNoticeOrNot = true;
    private FaceOverlapFragment mFragment;
    private TimeViewContoller mTimeViewContoller;
    private LFMediaPlayer mMediaPlayer = new LFMediaPlayer();
    private String[] mDetectList = null;
    private LFLivenessSDK.LFLivenessMotion[] mMotionList = null;
    private LFSensorManager mSensorManger;
    private int mCurrentDetectStep = 0;

    private FaceOverlapFragment.OnLivenessCallBack mLivenessListener = new FaceOverlapFragment.OnLivenessCallBack() {
        @Override
        public void onLivenessDetect(final int value, final int status, byte[] livenessEncryptResult,
                                     byte[] videoResult, LFLivenessSDK.LFLivenessImageResult[] imageResult) {
            Log.i(TAG, "onLivenessDetect" + "***value***" + value);
            onLivenessDetectCallBack(value, status, livenessEncryptResult, videoResult, imageResult);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.linkface_activity_liveness);
        mContext = this;
        mSensorManger = new LFSensorManager(this);
        Bundle bundle = getIntent().getExtras();
        mDetectList = getDetectActionOrder(bundle.getString(LivenessActivity.EXTRA_MOTION_SEQUENCE));
        mMotionList = getMctionOrder(bundle.getString(LivenessActivity.EXTRA_MOTION_SEQUENCE));
        EXTRA_RESULT_PATH = bundle.getString(LivenessActivity.EXTRA_RESULT_PATH);

        if (EXTRA_RESULT_PATH == null) {
            EXTRA_RESULT_PATH = Environment
                    .getExternalStorageDirectory().getAbsolutePath()
                    + File.separator
                    + "liveness" + File.separator;
        }
        File livenessFolder = new File(EXTRA_RESULT_PATH);
        if (!livenessFolder.exists()) {
            livenessFolder.mkdirs();
        }
        OUTPUT_TYPE = bundle.getString(Constants.OUTTYPE);
        mSoundNoticeOrNot = bundle.getBoolean(LivenessActivity.SOUND_NOTICE);

        ImageButton backBtn = (ImageButton) findViewById(R.id.linkface_return_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                setResult(10008);
                finish();
            }
        });
        mVGBottomDots = (ViewGroup) findViewById(R.id.viewGroup);
        if (mDetectList.length >= 1) {
            for (int i = 0; i < mDetectList.length; i++) {
                TextView tvBottomCircle = new TextView(this);
                tvBottomCircle.setBackgroundResource(R.drawable.drawable_liveness_detect_bottom_cicle_bg_selector);
                tvBottomCircle.setEnabled(true);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(dp2px(8),
                        dp2px(8));
                layoutParams.leftMargin = dp2px(8);
                mVGBottomDots.addView(tvBottomCircle, layoutParams);
            }
        }
        mAnimFrame = findViewById(R.id.anim_frame);
        mAnimFrame.setVisibility(View.INVISIBLE);
        initView();
        mWaitDetectView = (RelativeLayout) findViewById(R.id.wait_time_notice);
        mWaitDetectView.setVisibility(View.VISIBLE);
        mLlytTitle = (LinearLayout) findViewById(R.id.noticeLinearLayout);
        mLlytTitle.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
        mSensorManger.unregisterListener(mSensorEventListener);
        mMediaPlayer.stop();

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        if (isDialogShowing()) {
            mMediaPlayer.stop();
        }

        mSensorManger.registerListener(mSensorEventListener);
        if (mIsStart) {
            hideDialog();
            showDialog(Constants.ERROR_DETECT_FAIL);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mIsStart = true;
        Log.i(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        if (mFragment != null) {
            mFragment.registerLivenessDetectCallback(null);
            mFragment = null;
        }
        if (mTimeViewContoller != null) {
            mTimeViewContoller.setCallBack(null);
            mTimeViewContoller = null;
        }
    }

    private void initView() {
        mGvView = (LFGifView) findViewById(R.id.id_gv_play_action);
        mNoteTextView = (TextView) findViewById(R.id.noteText);
        mTimeView = (CircleTimeView) findViewById(R.id.time_view);
        mTimeViewContoller = new TimeViewContoller(mTimeView);
        mFragment = (FaceOverlapFragment) getFragmentManager()
                .findFragmentById(R.id.overlapFragment);
        mFragment.registerLivenessDetectCallback(mLivenessListener);

        ImageView ivMask = (ImageView) findViewById(R.id.image_mask);
        ivMask.setImageBitmap(readBitMap(this, R.drawable.linkface_mask_background));
    }

    /**
     * 以最省内存的方式读取本地资源的图片
     *
     * @param context
     * @param resId
     * @return
     */
    public static Bitmap readBitMap(Context context, int resId) {
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        //获取资源图片
        InputStream is = context.getResources().openRawResource(resId);
        return BitmapFactory.decodeStream(is, null, opt);
    }

    private void startAnimation(int animation) {
        if (animation != CURRENT_ANIMATION) {
            mGvView.setMovieResource(animation);
            if (isDialogShowing()) {
                return;
            }
        }
        mTimeViewContoller.start();
        mTimeViewContoller.setCallBack(new TimeViewContoller.CallBack() {
            @Override
            public void onTimeEnd() {
                if (null == mFragment) {
                    return;
                }
                mFragment.onTimeEnd();
            }
        });
    }

    private void setLivenessState(boolean pause) {
        if (null == mFragment) {
            return;
        }
        if (pause) {
            mFragment.stopLiveness();
        } else {
            mFragment.startLiveness();
        }
    }

    private void onLivenessDetectCallBack(final int value, final int status, final byte[] livenessEncryptResult, final byte[] videoResult, final LFLivenessSDK.LFLivenessImageResult[] imageResult) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCurrentDetectStep = status + 1;
                if (value == LFLivenessSDK.LFLivenessMotion.BLINK.getValue()) {
                    updateUi("请眨眨眼", R.raw.raw_liveness_detect_blink, status + 1);
                } else if (value == LFLivenessSDK.LFLivenessMotion.MOUTH.getValue()) {
                    updateUi("请张张嘴", R.raw.raw_liveness_detect_mouth, status + 1);
                } else if (value == LFLivenessSDK.LFLivenessMotion.NOD.getValue()) {
                    updateUi("请点点头", R.raw.raw_liveness_detect_nod, status + 1);
                } else if (value == LFLivenessSDK.LFLivenessMotion.YAW.getValue()) {
                    updateUi("请摇摇头", R.raw.raw_liveness_detect_yaw, status + 1);
                } else if (value == Constants.LIVENESS_SUCCESS) {
                    updateTheLastStepUI(mVGBottomDots);
                    saveFinalEncrytFile(livenessEncryptResult, videoResult, imageResult);
                } else if (value == Constants.LIVENESS_TRACKING_MISSED) {
                    showDialog(Constants.ERROR_ACTION_GET_FAIL);
                    saveFile(livenessEncryptResult, EXTRA_RESULT_PATH, LIVENESS_FILE_NAME);
                } else if (value == Constants.LIVENESS_TIME_OUT) {
                    showDialog(Constants.ERROR_ACTION_TIME_OUT);
                    saveFile(livenessEncryptResult, EXTRA_RESULT_PATH, LIVENESS_FILE_NAME);
                } else if (value == Constants.DETECT_BEGIN_WAIT) {
                    showDetectWaitUI();
                } else if (value == Constants.DETECT_END_WAIT) {
                    removeDetectWaitUI();
                }
            }
        });
    }

    private void showDetectWaitUI() {
        mWaitDetectView.setVisibility(View.VISIBLE);
        mIsStart = true;
        if (mTimeViewContoller != null) {
            mTimeViewContoller.setCallBack(null);
        }
        mLlytTitle.setVisibility(View.INVISIBLE);
    }

    private void removeDetectWaitUI() {
        mWaitDetectView.setVisibility(View.GONE);
        setLivenessState(false);
        mLlytTitle.setVisibility(View.VISIBLE);
        mAnimFrame.setVisibility(View.VISIBLE);
        if (mMotionList == null || mMotionList.length <= 0) {
            return;
        }
        onLivenessDetectCallBack(mMotionList[0].getValue(), 0, null, null, null);
    }

    private void updateTheLastStepUI(ViewGroup viewGroup) {
        mMediaPlayer.release();
    }

    private void updateUi(String stringId, int animationId, int number) {
        mNoteTextView.setText(stringId);
        if (animationId != 0) {
            startAnimation(animationId);
        }
        if (number - 2 >= 0) {
            View childAt = mVGBottomDots.getChildAt(number - 2);
            childAt.setEnabled(false);
        }
        playSoundNotice(number);
    }

    private void playSoundNotice(int step) {

        if (step > 0) {
            if (mDetectList[step - 1]
                    .equalsIgnoreCase("BLINK")) {
                if (mSoundNoticeOrNot) {
                    mMediaPlayer.setMediaSource(mContext, "linkface_notice_blink.mp3", true);
                }
            } else if (mDetectList[step - 1]
                    .equalsIgnoreCase("NOD")) {
                if (mSoundNoticeOrNot) {
                    mMediaPlayer.setMediaSource(mContext, "linkface_notice_nod.mp3", true);
                }
            } else if (mDetectList[step - 1]
                    .equalsIgnoreCase("MOUTH")) {
                if (mSoundNoticeOrNot) {
                    mMediaPlayer.setMediaSource(mContext, "linkface_notice_mouth.mp3", true);
                }
            } else if (mDetectList[step - 1]
                    .equalsIgnoreCase("YAW")) {
                if (mSoundNoticeOrNot) {
                    mMediaPlayer.setMediaSource(mContext, "linkface_notice_yaw.mp3", true);
                }
            }
        }
    }

    public void saveFinalEncrytFile(byte[] livenessEncryptResult, byte[] videoResult, LFLivenessSDK.LFLivenessImageResult[] imageResult) {
        Intent intent = new Intent();

        LFReturnResult returnResult = new LFReturnResult();
        boolean isReturnImage = getIntent().getBooleanExtra(KEY_DETECT_IMAGE_RESULT, false);
        boolean isReturnProtoBuf = getIntent().getBooleanExtra(KEY_DETECT_PROTO_BUF_RESULT, false);

        if (isReturnImage) {
            returnResult.setImageResults(imageResult);
        }

        if(isReturnProtoBuf && livenessEncryptResult != null && livenessEncryptResult.length > 0) {
            LFProtoBufUtil.setProtoBuf(livenessEncryptResult);
        }

        if (videoResult != null) {
            String fileAbsolutePath = saveFile(videoResult, EXTRA_RESULT_PATH, LIVENESS_VIDEO_NAME);
            boolean isReturnVideo = getIntent().getBooleanExtra(KEY_DETECT_VIDEO_RESULT, false);
            if (isReturnVideo) {
                returnResult.setVideoResultPath(fileAbsolutePath);
            }
        }
        intent.putExtra(KEY_DETECT_RESULT, returnResult);

        if (livenessEncryptResult != null) {
//            LivenessUtils.saveFile(livenessEncryptResult, EXTRA_RESULT_PATH, LIVENESS_FILE_NAME);
            setResult(10009, intent);
            finish();
        }
    }

    private void showDialog(String message) {
        if (isDialogShowing()) {
            return;
        }
        if (mDetectList.length >= 1) {
            for (int i = 0; i < mDetectList.length; i++) {
                View childAt = mVGBottomDots.getChildAt(i);
                if (childAt != null) {
                    childAt.setEnabled(true);
                }
            }
        }

        hideTimeContoller();
        hideIndicateView();
        mDialog = new LinkfaceAlertDialog(mContext).builder().setCancelable(false).
                setTitle(message).setNegativeButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                onErrorHappen(10008);
            }
        }).setPositiveButton("确定", new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                showIndicateView();
                if (mFragment != null) {
                    mFragment.registerLivenessDetectCallback(mLivenessListener);
                }
                restartAnimationAndLiveness();
            }
        });
        if (((Activity) mContext).isFinishing()) {
            return;
        }
        mDialog.show();
        mMediaPlayer.release();
    }

    @Override
    public void finish() {
        if(mDialog != null && mDialog.isShowing())
            mDialog.dismiss();
        super.finish();
    }

    private void hideDialog() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    private void hideIndicateView() {
        if (mGvView != null) {
            mGvView.setVisibility(View.GONE);
        }
        if (mVGBottomDots != null) {
            mVGBottomDots.setVisibility(View.GONE);
        }
        if (mNoteTextView != null) {
            mNoteTextView.setVisibility(View.GONE);
        }
    }

    private void showIndicateView() {
        if (mGvView != null) {
            mGvView.setVisibility(View.VISIBLE);
        }
        if (mVGBottomDots != null) {
            mVGBottomDots.setVisibility(View.VISIBLE);
        }
        if (mNoteTextView != null) {
            mNoteTextView.setVisibility(View.VISIBLE);
        }
    }

    private void hideTimeContoller() {
        if (mTimeViewContoller != null) {
            mTimeViewContoller.hide();
        }
    }

    private void restartAnimationAndLiveness() {
        setLivenessState(false);
        deleteFiles(EXTRA_RESULT_PATH);
        if (mDetectList.length >= 1) {
            View childAt = mVGBottomDots.getChildAt(0);
            childAt.setEnabled(false);
        }
        startAnimation(CURRENT_ANIMATION);
        mMediaPlayer.release();
        playSoundNotice(mCurrentDetectStep);
    }

    private boolean isDialogShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    public void onErrorHappen(int resultCode) {
        setResult(resultCode);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putString(Constants.RESULT, "活体检测被取消!");
        intent.putExtras(bundle);
        setResult(10008, intent);
        finish();
    }

    SensorEventListener mSensorEventListener = new SensorEventListener() {

        @Override
        public void onAccuracyChanged(Sensor arg0, int arg1) {
        }

        @Override
        public void onSensorChanged(SensorEvent event) {
            mFragment.addSequentialInfo(event.sensor.getType(), event.values);
        }
    };

    private int dp2px(float dpValue) {
        int densityDpi = this.getResources().getDisplayMetrics().densityDpi;
        return (int) (dpValue * (densityDpi / 160));
    }

    private String[] getDetectActionOrder(String input) {
        String[] splitStrings = input.split("\\s+");
        return splitStrings;
    }

    private  LFLivenessSDK.LFLivenessMotion[] getMctionOrder(String input) {
        String[] splitStrings = input.split("\\s+");
        LFLivenessSDK.LFLivenessMotion[] detectList = new LFLivenessSDK.LFLivenessMotion[splitStrings.length];
        for (int i = 0; i < splitStrings.length; i++) {
            if (splitStrings[i].equalsIgnoreCase(BLINK)) {
                detectList[i] = LFLivenessSDK.LFLivenessMotion.BLINK;
            } else if (splitStrings[i].equalsIgnoreCase(NOD)) {
                detectList[i] = LFLivenessSDK.LFLivenessMotion.NOD;
            } else if (splitStrings[i].equalsIgnoreCase(MOUTH)) {
                detectList[i] = LFLivenessSDK.LFLivenessMotion.MOUTH;
            } else if (splitStrings[i].equalsIgnoreCase(YAW)) {
                detectList[i] = LFLivenessSDK.LFLivenessMotion.YAW;
            }
        }
        return detectList;
    }

    private void deleteFiles(String folderPath) {
        File dir = new File(folderPath);
        if (dir == null || !dir.exists() || !dir.isDirectory() || dir.listFiles() == null)
            return;
        for (File file : dir.listFiles()) {
            if (file.isFile())
                file.delete();
        }
    }

    private String saveFile(byte[] bfile, String filePath,String fileName) {
        String fileAbsolutePath = null;
        if (bfile == null) {
            return fileAbsolutePath;
        }
        BufferedOutputStream bos = null;
        FileOutputStream fos = null;
        try {
            File dir = new File(filePath);
            if(!dir.exists()&&dir.isDirectory()){
                dir.mkdirs();
            }
            File file = new File(filePath + File.separator + fileName);
            fileAbsolutePath = file.getAbsolutePath();
            fos = new FileOutputStream(file);
            bos = new BufferedOutputStream(fos);
            bos.write(bfile);
        } catch (Exception e) {
            e.printStackTrace();
            fileAbsolutePath =  null;
        } finally {
            if (bos != null) {
                try {
                    bos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return fileAbsolutePath;
    }
}
