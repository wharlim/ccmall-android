package com.weiju.ccmall.module.community;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;

import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;


public class MyCustomGSYVideoPlayer extends StandardGSYVideoPlayer {
    private boolean enableSeekBar = false;
    private boolean isComplete;

    public MyCustomGSYVideoPlayer(Context context, Boolean fullFlag) {
        super(context, fullFlag);
    }

    public MyCustomGSYVideoPlayer(Context context) {
        super(context);
    }

    public MyCustomGSYVideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setEnableSeekBar(boolean enableSeekBar) {
//            mProgressBar.setEnabled(enableSeekBar);
        this.enableSeekBar = enableSeekBar;
        if (mProgressBar != null) {
            mProgressBar.setEnabled(enableSeekBar);
        }
    }

    @Override
    protected void init(Context context) {
        super.init(context);
        if (mProgressBar != null) {
            mProgressBar.setEnabled(enableSeekBar);
        }
    }

    @Override
    protected void setProgressAndTime(int progress, int secProgress, int currentTime, int totalTime) {
        super.setProgressAndTime(progress, secProgress, currentTime, totalTime);
        if (progress >= 94 && !isComplete) {
            isComplete = true;
            if (getContext() instanceof VideoDetailActivity) {
                ((VideoDetailActivity)getContext()).addBrowseLog();
            }
        }
        if (progress <= 4) {
            isComplete = false;
        }
    }

    @Override
    public void onVideoResume() {
        super.onVideoResume();
        isComplete = false;
    }

    @Override
    public void onVideoReset() {
        super.onVideoReset();
        isComplete = false;
    }
}
