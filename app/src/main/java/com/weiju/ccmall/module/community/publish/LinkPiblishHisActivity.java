package com.weiju.ccmall.module.community.publish;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.ICommunityService;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.TopicLibraryModel;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/8/28.
 */
public class LinkPiblishHisActivity extends BaseListActivity {

    private LinkHisQuickAdapter mAdapter = new LinkHisQuickAdapter();
    private View mHeadView;
    private ICommunityService mService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public String getTitleStr() {
        return "官方发布记录";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        TopicLibraryModel data = (TopicLibraryModel) adapter.getData().get(position);
        if (data.status == 2) {
            Intent intent = new Intent(this, PublishPicActivity.class);
            intent.putExtra(Constants.KEY_IS_EDIT, true);
            intent.putExtra("type", data.type == 1 ? PublishPicActivity.TYPE_LINKE_IMAGE : PublishPicActivity.TYPE_LINKE);
            intent.putExtra("data", data);
            startActivity(intent);
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void initData() {
        mService = ServiceManager.getInstance().createService(ICommunityService.class);
        super.initData();
    }

    @Override
    public void getData(final boolean isRefresh) {
        APIManager.startRequest(
                mService.getMyMaterialLibraryList(mCurrentPage, 15),
                new BaseRequestListener<PaginationEntity<TopicLibraryModel, HistoryExtra>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<TopicLibraryModel, HistoryExtra> result) {
                        super.onSuccess(result);
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && result.list.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                        setHeadeData(result.ex);
                    }
                },this
        );
    }

    @Override
    public List<View> getHeaderViews() {
        mHeadView = getLayoutInflater().inflate(R.layout.publish_head_layout, mRecyclerView, false);
        return Arrays.asList(mHeadView);
    }

    private void setHeadeData(HistoryExtra ex) {
        CircleImageView headerUserIv = (CircleImageView) mHeadView.findViewById(R.id.header_user_iv);
        TextView headerUserName = (TextView) mHeadView.findViewById(R.id.header_user_name);
        TextView headMaterialNum = (TextView) mHeadView.findViewById(R.id.head_material_num);
        TextView headVideoNum = (TextView) mHeadView.findViewById(R.id.header_video_num);

        Glide.with(this).load(ex.getHeadImage()).into(headerUserIv);
        headerUserName.setText(ex.getName());
        headMaterialNum.setText("图文 " + ex.getImageCount());
        headVideoNum.setText("链接 " + ex.getLinkCount());
        mAdapter.setHeaderView(mHeadView);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void needRefresh(EventMessage eventMessage) {
        if (eventMessage.getEvent() == Event.PUBLISH_EDIT_FINISH) {
            mCurrentPage = 1;
            getData(true);
        }
    }
}
