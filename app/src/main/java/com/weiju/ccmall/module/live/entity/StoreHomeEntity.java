package com.weiju.ccmall.module.live.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/4/17.
 */
public class StoreHomeEntity {

    /**
     * memberId : 58fa3195a380409cb48e725e3e6717fd
     * storeName :
     * headImage : http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJrptJd4DQulLgIfuicLJ5UibYvXIy8xPeHuyUwicLNveLvibaA8ibPtaSyxWaFO43SEEPkbuoGTohcDDQ/132
     * profitMoney : 0
     * totalMoney : 0
     * bondMoney : 0
     * waitProfitMoney : 10600
     * dayOrderMoney : 0
     * dayOrderCount : 0
     * waitShipCount : 1
     * hasShipCount : 0
     * "hasReceivedCount": 0
     */

    @SerializedName("memberId")
    public String memberId;
    @SerializedName("storeName")
    public String storeName;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("profitMoney")
    public long profitMoney;
    @SerializedName("totalMoney")
    public long totalMoney;
    @SerializedName("bondMoney")
    public long bondMoney;
    @SerializedName("waitProfitMoney")
    public long waitProfitMoney;
    @SerializedName("dayOrderMoney")
    public long dayOrderMoney;
    @SerializedName("dayOrderCount")
    public long dayOrderCount;
    @SerializedName("waitShipCount")
    public int waitShipCount;
    @SerializedName("hasShipCount")
    public int hasShipCount;
    @SerializedName("hasReceivedCount")
    public int hasReceivedCount;
    @SerializedName("afterSaleCount")
    public int afterSaleCount;
    /**
     * storeId : 123456
     */

    @SerializedName("storeId")
    public String storeId;
}
