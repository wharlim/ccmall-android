package com.weiju.ccmall.module.collect;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.collect.bean.ShopCollectItem;
import com.weiju.ccmall.module.collect.views.ShopGoodItemView;
import com.weiju.ccmall.module.shop.ShopActivity;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CollectShopAdapter extends BaseAdapter<ShopCollectItem, CollectShopAdapter.ViewHolder> {

    private IProductService iProductService;
    private boolean showCollectState = true;

    public CollectShopAdapter(Context context) {
        super(context);
        iProductService = ServiceManager.getInstance().createService(IProductService.class);
    }

    @NonNull
    @Override
    public CollectShopAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_shop_collect, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CollectShopAdapter.ViewHolder viewHolder, int i) {
        ShopCollectItem item = items.get(i);
        viewHolder.bind(item, i);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.shopIcon)
        SimpleDraweeView shopIcon;
        @BindView(R.id.shopName)
        TextView shopName;
        @BindView(R.id.collectIcon)
        ImageView collectIcon;
        @BindView(R.id.collectText)
        TextView collectText;
        @BindView(R.id.collectContainer)
        LinearLayout collectContainer;
        @BindView(R.id.good1)
        ShopGoodItemView good1;
        @BindView(R.id.good2)
        ShopGoodItemView good2;
        @BindView(R.id.good3)
        ShopGoodItemView good3;
        ShopGoodItemView[] goods;

        private int collectedTextColor;
        private int notCollectTextColor;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            collectedTextColor = itemView.getContext().getResources().getColor(R.color.red);
            notCollectTextColor = itemView.getContext().getResources().getColor(R.color.grayDark);

            goods = new ShopGoodItemView[]{good1, good2, good3};
        }

        public void bind(ShopCollectItem item, int pos) {
            itemView.setOnClickListener(v -> {
                ShopActivity.start(itemView.getContext(), item.storeId);
            });
            FrescoUtil.setImageSmall(shopIcon, item.thumbUrl);
            shopName.setText(item.storeName);
            if (item.collectStatus > 0) {
//                collectIcon.setImageResource(R.mipmap.shop_followed);
//                collectText.setTextColor(collectedTextColor);
                collectText.setText("已收藏");
            } else {
//                collectIcon.setImageResource(R.mipmap.shop_not_follow);
//                collectText.setTextColor(notCollectTextColor);
                collectText.setText("收藏");
            }
            collectContainer.setOnClickListener(v -> {
                if (item.collectStatus > 0) {
                    WJDialog wjDialog = new WJDialog(context);
                    wjDialog.show();
                    wjDialog.setContentText("确定取消该收藏？");
                    wjDialog.setCancelText("取消");
                    wjDialog.setConfirmText("确定");
                    wjDialog.setOnCancelListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            wjDialog.dismiss();
                        }
                    });
                    wjDialog.setOnConfirmListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            wjDialog.dismiss();
                            // 取消收藏
                            delCollectStore(pos);
                        }
                    });
                } else {
                    // 收藏
                    collectStore(pos);
                }
            });
            for (ShopGoodItemView v: goods) {
                v.setVisibility(View.INVISIBLE);
            }
            int i = 0;
            for (SkuInfo skuInfo: item.productList) {
                ShopGoodItemView v = goods[i];
                v.setVisibility(View.VISIBLE);
                v.setSkuInfo(skuInfo);
                if (i >= 2) {
                    break;
                }
                ++i;
            }

            collectContainer.setVisibility(showCollectState ? View.VISIBLE : View.INVISIBLE);
        }
    }

    private void delCollectStore(int position) {
        APIManager.startRequest(iProductService.delCollectStore(items.get(position).storeId), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                if (((Activity)context).isDestroyed()) {
                    return ;
                }
                ToastUtil.success("取消收藏成功!");
                items.remove(position);
                notifyDataSetChanged();
            }
        }, context);
    }

    private void collectStore(int position) {
        APIManager.startRequest(iProductService.collectStore(items.get(position).storeId), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                if (((Activity)context).isDestroyed()) {
                    return ;
                }
                ToastUtil.success("收藏店铺成功!");
                items.get(position).collectStatus = 1;
                notifyItemChanged(position);
            }
        }, context);
    }

    public void setShowCollectState(boolean showCollectState) {
        this.showCollectState = showCollectState;
    }
}
