package com.weiju.ccmall.module.xysh.activity;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.AuthIdentitySuccssdActivity;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.adapter.CreditCardAdapter;
import com.weiju.ccmall.module.xysh.adapter.PopupSelectAdapter;
import com.weiju.ccmall.module.xysh.bean.ApplyCreditCardItem;
import com.weiju.ccmall.module.xysh.bean.ApplyCreditCommonResult;
import com.weiju.ccmall.module.xysh.bean.TagListItem;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.PrestoreType;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.bean.BankItem;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PopupWindowManage;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * @author chenyanming
 * @time 2019/12/17 on 17:04
 * @desc
 */
public class CreditCardApplyListActivity extends BaseListActivity {

    @BindView(R.id.lineSelect)
    View mLineSelect;
    @BindView(R.id.tvBank)
    TextView mTvBank;
    @BindView(R.id.ivBank)
    ImageView mIvBank;

    @BindView(R.id.tvType)
    TextView mTvType;
    @BindView(R.id.ivType)
    ImageView mIvType;

    @BindView(R.id.tvThem)
    TextView mTvThem;
    @BindView(R.id.ivThem)
    ImageView mIvThem;

    @BindView(R.id.viewYinying)
    View mViewYinying;

    private CreditCardAdapter mAdapter = new CreditCardAdapter();
    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    private List<ApplyCreditCommonResult.BankItem> allBanks = new ArrayList<>();
    private List<TagListItem> allTags = new ArrayList<>();
    private List<TagListItem> allTopics = new ArrayList<>();

    private String bankId = "";
    private String tagId = "";
    private String topicsId = "";

    //银行选择窗弹窗
    private PopupWindowManage mBankWindowManage;
    private View mBankPopupView;
    private PopupSelectAdapter mBankSelectAdapter;

    //等级选择弹窗
    private PopupWindowManage mTypeWindowManage;
    private View mTypePopupView;
    private PopupSelectAdapter mTypeSelectAdapter;

    //主题选择弹窗
    private PopupWindowManage mThemWindowManage;
    private View mThemPopupView;
    private PopupSelectAdapter mThemSelectAdapter;


    @Override
    public void initView() {
        super.initView();
        EventBus.getDefault().register(this);
        getHeaderLayout().setRightText("进度查询");
        getHeaderLayout().setRightTextColor(R.color.text_black);
        getHeaderLayout().setOnRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(bankId)) {
                    ToastUtil.error("请选择银行!");
                    return;
                }
                ApplyCreditCommonResult.BankItem bankSelected = null;
                for (ApplyCreditCommonResult.BankItem bank :
                        allBanks) {
                    if (bankId.equals(bank.id)) {
                        bankSelected = bank;
                    }
                }
                if (bankSelected == null || TextUtils.isEmpty(bankSelected.searchUrl)) {
                    ToastUtil.error("当前银行无申请");
                    return;
                }
                Intent intent = new Intent(CreditCardApplyListActivity.this, WebViewJavaActivity.class);
                intent.putExtra("url", bankSelected.searchUrl);
                startActivity(intent);
            }
        });
    }

    private String getCodeByStationChannelId(String stationChannelId) {
        for (ApplyCreditCommonResult.BankItem item : allBanks) {
            if (stationChannelId.equals(item.stationChannelId)) {
                return item.code;
            }
        }
        return "";
    }

    private void getBankSelectableList() {
        APIManager.startRequest(service.getBankList(), new Observer<ApplyCreditCommonResult<List<ApplyCreditCommonResult.BankItem>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ApplyCreditCommonResult<List<ApplyCreditCommonResult.BankItem>> ret) {
                if (ret.isSuccess()) {
                    allBanks.clear();
                    allBanks.addAll(ret.result);
                    creatBankPopup();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void initData() {
        super.initData();
        getBankSelectableList();
        APIManager.startRequest(service.getBankTags(), new Observer<ApplyCreditCommonResult<List<TagListItem>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ApplyCreditCommonResult<List<TagListItem>> ret) {
                if (ret.isSuccess()) {
                    allTags.clear();
                    allTags.addAll(ret.result);
                    creatTypePopup();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
        APIManager.startRequest(service.getBankTopics(), new Observer<ApplyCreditCommonResult<List<TagListItem>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ApplyCreditCommonResult<List<TagListItem>> ret) {
                if (ret.isSuccess()) {
                    allTopics.clear();
                    allTopics.addAll(ret.result);
                    creatThemPopup();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

        getBankList();
    }

    private void getBankList() {
        APIManager.startRequest(service.getBankAisle(
                BankUtils.sAppToken,
                topicsId, tagId, bankId, "1"
        ), new Observer<ApplyCreditCommonResult<List<ApplyCreditCardItem>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ApplyCreditCommonResult<List<ApplyCreditCardItem>> ret) {
                if (ret.isSuccess()) {
                    mAdapter.setNewData(ret.result);
                    mAdapter.loadMoreEnd(true);
                    mRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }


    private void creatThemPopup() {
        mThemWindowManage = PopupWindowManage.getInstance(CreditCardApplyListActivity.this);
        mThemWindowManage.setYinYing(mViewYinying);
        mThemWindowManage.setOutsideTouchable(false);
        mThemWindowManage.setPopupWindowDismissListener(mThemPopupDismissListener);

        mThemPopupView = View.inflate(CreditCardApplyListActivity.this, R.layout.view_popup_select, null);
        RecyclerView recyclerView = mThemPopupView.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CreditCardApplyListActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        List<PrestoreType> list = new ArrayList<>();
        list.add(new PrestoreType("全部主题", ""));
//        list.add(new PrestoreType("主题1", "0"));
//        list.add(new PrestoreType("主题2", "1"));
//        list.add(new PrestoreType("主题3", "2"));
//        list.add(new PrestoreType("主题4", "3"));
//        list.add(new PrestoreType("主题5", "3"));
//        list.add(new PrestoreType("主题6", "3"));
        for (TagListItem item : allTopics) {
            list.add(new PrestoreType(item.name, item.id));
        }
        mThemSelectAdapter = new PopupSelectAdapter();
        mThemSelectAdapter.setNewData(list);
        recyclerView.setAdapter(mThemSelectAdapter);
        mThemSelectAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mThemSelectAdapter.setSelectPosition(position);
                mThemWindowManage.dismiss();
                PrestoreType item = mThemSelectAdapter.getItem(position);
                if (null != item) {
                    mTvThem.setText(item.name);
                    topicsId = item.value;
                    getData(true);
                }
            }
        });
    }

    private void creatTypePopup() {
        mTypeWindowManage = PopupWindowManage.getInstance(CreditCardApplyListActivity.this);
        mTypeWindowManage.setYinYing(mViewYinying);
        mTypeWindowManage.setOutsideTouchable(false);
        mTypeWindowManage.setPopupWindowDismissListener(mTypePopupDismissListener);

        mTypePopupView = View.inflate(CreditCardApplyListActivity.this, R.layout.view_popup_select, null);
        RecyclerView recyclerView = mTypePopupView.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CreditCardApplyListActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        List<PrestoreType> list = new ArrayList<>();
        list.add(new PrestoreType("全部等级", ""));
//        list.add(new PrestoreType("等级1", "0"));
//        list.add(new PrestoreType("等级2", "1"));
//        list.add(new PrestoreType("等级3", "2"));
//        list.add(new PrestoreType("等级4", "3"));
//        list.add(new PrestoreType("等级5", "3"));
//        list.add(new PrestoreType("等级6", "3"));
        for (TagListItem item : allTags) {
            list.add(new PrestoreType(item.name, item.id));
        }
        mTypeSelectAdapter = new PopupSelectAdapter();
        mTypeSelectAdapter.setNewData(list);
        recyclerView.setAdapter(mTypeSelectAdapter);
        mTypeSelectAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mTypeSelectAdapter.setSelectPosition(position);
                mTypeWindowManage.dismiss();
                PrestoreType item = mTypeSelectAdapter.getItem(position);
                if (null != item) {
                    mTvType.setText(item.name);
                    tagId = item.value;
                    getData(true);
                }
            }
        });
    }

    private void creatBankPopup() {
        mBankWindowManage = PopupWindowManage.getInstance(CreditCardApplyListActivity.this);
        mBankWindowManage.setYinYing(mViewYinying);
        mBankWindowManage.setOutsideTouchable(false);
        mBankWindowManage.setPopupWindowDismissListener(mBankPopupDismissListener);

        mBankPopupView = View.inflate(CreditCardApplyListActivity.this, R.layout.view_popup_select, null);
        RecyclerView recyclerView = mBankPopupView.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CreditCardApplyListActivity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        List<PrestoreType> list = new ArrayList<>();
//        list.add(new PrestoreType("全部银行", "0"));
//        list.add(new PrestoreType("中国建设银行", "1"));
//        list.add(new PrestoreType("招商银行", "2"));
//        list.add(new PrestoreType("中国建设银行", "3"));
//        list.add(new PrestoreType("农业银行", "3"));
//        list.add(new PrestoreType("广州农商银行", "3"));
//        list.add(new PrestoreType("广发银行", "3"));
//        list.add(new PrestoreType("光大银行", "3"));
//        list.add(new PrestoreType("中国工商银行", "3"));
        list.add(new PrestoreType("全部银行", ""));
        for (ApplyCreditCommonResult.BankItem bankItem: allBanks){
            list.add(new PrestoreType(bankItem.name, bankItem.id));
        }


        mBankSelectAdapter = new PopupSelectAdapter();
        mBankSelectAdapter.setNewData(list);
        recyclerView.setAdapter(mBankSelectAdapter);
        mBankSelectAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mBankSelectAdapter.setSelectPosition(position);
                mBankWindowManage.dismiss();
                PrestoreType item = mBankSelectAdapter.getItem(position);
                if (null != item) {
                    mTvBank.setText(item.name);
                    bankId = item.value;
                    getData(true);
                }
            }
        });

    }


    private PopupWindowManage.PopupWindowDismissListener mBankPopupDismissListener = new PopupWindowManage.PopupWindowDismissListener() {
        @Override
        public void dismiss() {
            mIvBank.setSelected(false);
        }
    };

    private PopupWindowManage.PopupWindowDismissListener mTypePopupDismissListener = new PopupWindowManage.PopupWindowDismissListener() {
        @Override
        public void dismiss() {
            mIvType.setSelected(false);
        }
    };

    private PopupWindowManage.PopupWindowDismissListener mThemPopupDismissListener = new PopupWindowManage.PopupWindowDismissListener() {
        @Override
        public void dismiss() {
            mIvThem.setSelected(false);
        }
    };

    @Override
    public String getTitleStr() {
        return "信用卡申请";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        ApplyCreditCardItem item = mAdapter.getItem(position);
        ApplyCreaditCardActivity.start(this, getCodeByStationChannelId(item.stationChannelId), item);
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
//        List<String> list = new ArrayList<>();
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        mAdapter.setNewData(list);
//        mRefreshLayout.setRefreshing(false);
        getBankList();
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_credit_card;
    }

    @OnClick(R.id.layoutBank)
    protected void selectBank() {
        if (null != mTypeWindowManage && mTypeWindowManage.isShowing()) {
            mTypeWindowManage.dismiss();
        }
        if (null != mThemWindowManage && mThemWindowManage.isShowing()) {
            mThemWindowManage.dismiss();
        }


        if (null == mBankWindowManage) {
            return;
        }

        mBankWindowManage.showWindow(mLineSelect, mBankPopupView);
        mIvBank.setSelected(true);
    }

    @OnClick(R.id.layoutType)
    protected void selectType() {
        if (null != mBankWindowManage && mBankWindowManage.isShowing()) {
            mBankWindowManage.dismiss();
        }
        if (null != mThemWindowManage && mThemWindowManage.isShowing()) {
            mThemWindowManage.dismiss();
        }


        if (null == mTypeWindowManage) {
            return;
        }

        mTypeWindowManage.showWindow(mLineSelect, mTypePopupView);
        mIvType.setSelected(true);
    }

    @OnClick(R.id.layoutThem)
    protected void selectThem() {
        if (null != mBankWindowManage && mBankWindowManage.isShowing()) {
            mBankWindowManage.dismiss();
        }
        if (null != mTypeWindowManage && mTypeWindowManage.isShowing()) {
            mTypeWindowManage.dismiss();
        }


        if (null == mThemWindowManage) {
            return;
        }

        mThemWindowManage.showWindow(mLineSelect, mThemPopupView);
        mIvThem.setSelected(true);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getBankSelectableList();
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void stickEvent(EventMessage message) {
        switch (message.getEvent()) {
            case applyCard:
//                ToastUtil.success("刷新成功");
                getData(true);
                getBankSelectableList();
                break;
            default:
        }
    }

}
