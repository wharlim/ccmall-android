package com.weiju.ccmall.module.blockchain.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseFragment;

public class RankingFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ranking, container, false);
    }

    public static RankingFragment newInstance() {
        Bundle args = new Bundle();
        RankingFragment fragment = new RankingFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
