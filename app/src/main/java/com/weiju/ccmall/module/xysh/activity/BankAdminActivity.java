package com.weiju.ccmall.module.xysh.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.fragment.BankAdminFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.manager.ServiceManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/11 on 18:12
 * @desc
 */
public class BankAdminActivity extends BaseActivity {

    @BindView(R.id.tvXinYong)
    TextView mTvXinYong;
    @BindView(R.id.tvChuXu)
    TextView mTvChuXu;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    private boolean selectCredit;
    private boolean selectDebit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_admin);
        ButterKnife.bind(this);
        selectCredit = getIntent().getBooleanExtra("selectCredit", false);
        selectDebit = getIntent().getBooleanExtra("selectDebit", false);
        initView();
    }

    private void initView() {
        setTitle("银行卡管理");
        setLeftBlack();
        mTvXinYong.setSelected(true);
        if (selectCredit) {
            mTvChuXu.setVisibility(View.GONE);
        } else if (selectDebit) {
            mTvXinYong.setVisibility(View.GONE);
            mTvChuXu.setSelected(true);
        }
        initViewPager();
    }

    private void initViewPager() {
        ArrayList<BaseFragment> fragments = new ArrayList<>();

        if (selectCredit) {
            fragments.add(BankAdminFragment.newInstance(0, true));
            setTitle("选择信用卡");
        } else if (selectDebit) {
            fragments.add(BankAdminFragment.newInstance(1, true));
            setTitle("选择储蓄卡");
        } else {
            fragments.add(BankAdminFragment.newInstance(0, false));
            fragments.add(BankAdminFragment.newInstance(1, false));
        }

        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }
        };
        mViewPager.setAdapter(fragmentPagerAdapter);
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setCurrentItem(0);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (i == 0) {
                    mTvXinYong.setSelected(true);
                    mTvChuXu.setSelected(false);
                } else {
                    mTvChuXu.setSelected(true);
                    mTvXinYong.setSelected(false);
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @OnClick(R.id.tvXinYong)
    protected void xinyong() {
        if (mTvXinYong.isSelected()) {
            return;
        }
        mTvChuXu.setSelected(false);
        mTvXinYong.setSelected(true);
        mViewPager.setCurrentItem(0);
    }

    @OnClick(R.id.tvChuXu)
    protected void chuxu() {
        if (mTvChuXu.isSelected()) {
            return;
        }
        mTvChuXu.setSelected(true);
        mTvXinYong.setSelected(false);
        mViewPager.setCurrentItem(1);
    }

    public static void start(Context context) {
        context.startActivity(new Intent(context, BankAdminActivity.class));
    }

    public static void startForSelectCredit(Activity context, int reqCode) {
        Intent bankAdminActivity = new Intent(context, BankAdminActivity.class);
        bankAdminActivity.putExtra("selectCredit", true);
        context.startActivityForResult(bankAdminActivity, reqCode);
    }

    public static void startForSelectDebit(Activity context, int reqCode) {
        Intent bankAdminActivity = new Intent(context, BankAdminActivity.class);
        bankAdminActivity.putExtra("selectDebit", true);
        context.startActivityForResult(bankAdminActivity, reqCode);
    }

    public static QueryUserBankCardResult.BankInfListBean getSelectResult(Intent data) {
        return (QueryUserBankCardResult.BankInfListBean) data.getSerializableExtra("card");
    }
}
