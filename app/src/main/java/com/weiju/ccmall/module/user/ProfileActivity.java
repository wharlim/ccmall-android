package com.weiju.ccmall.module.user;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.ToastUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.address.AddressListActivity;
import com.weiju.ccmall.module.auth.AuthPhoneActivity;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.user.model.UpMemberModel;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.basic.BaseSubscriber;
import com.weiju.ccmall.shared.bean.HasPasswordModel;
import com.weiju.ccmall.shared.bean.UploadResponse;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.manager.UploadManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.CSUtils;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.zhihu.matisse.Matisse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;

public class ProfileActivity extends BaseActivity {

    @BindView(R.id.invitationTv)
    TextView mInvitationTv;
    @BindView(R.id.avatarLayout)
    LinearLayout mAvatarLayout;
    @BindView(R.id.nicknameLayout)
    LinearLayout mNicknameLayout;
    @BindView(R.id.phoneLayout)
    LinearLayout mPhoneLayout;
    @BindView(R.id.tvPasswordTips)
    TextView mTvPasswordTips;
    @BindView(R.id.passwordLayout)
    LinearLayout mPasswordLayout;
    @BindView(R.id.invitationLayout)
    LinearLayout mInvitationLayout;
    @BindView(R.id.addressLayout)
    LinearLayout mAddressLayout;
    @BindView(R.id.layoutCS)
    LinearLayout mLayoutCS;
    @BindView(R.id.aboutUsLayout)
    LinearLayout mAboutUsLayout;
    @BindView(R.id.logoutBtn)
    TextView mLogoutBtn;
    @BindView(R.id.tvPassword)
    TextView mTvPassword;
    @BindView(R.id.tvPayPassword)
    TextView tvPayPassword;
    @BindView(R.id.tvPayPasswordTips)
    TextView tvPayPasswordTips;
    private IUserService mUserService;

    @BindView(R.id.avatarIv)
    protected SimpleDraweeView mAvatarIv;

    @BindView(R.id.nicknameTv)
    protected TextView mNicknameTv;

    @BindView(R.id.phoneTv)
    protected TextView mPhoneTv;

    private User mUser;
    private boolean isNoUp;
    private UpMemberModel mUpMemberModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_layout);

        ButterKnife.bind(this);

        initData();
    }

    private void initData() {
        mUserService = ServiceManager.getInstance().createService(IUserService.class);

        execute(mUserService.getUserInfo(), new BaseSubscriber<User>() {
            @Override
            public void onNext(@NonNull User user) {
                super.onNext(user);
                setUserData(user);
                SessionUtil.getInstance().setLoginUser(user);
            }
        });
//        BaseSubscriber<UpMemberModel> baseSubscriber = new BaseSubscriber<UpMemberModel>() {
//            @Override
//            public void onNext(UpMemberModel result) {
//                super.onNext(result);
//                mInvitationTv.setText(result.nickName);
//            }
//        };
//        baseSubscriber.setProgressDialog(getProgressDialog());
//        execute(mUserService.getUpMember(), baseSubscriber);

        APIManager.startRequest(mUserService.hasPayPwd(), new BaseRequestListener<HasPasswordModel>(this) {
            @Override
            public void onSuccess(HasPasswordModel model) {
                super.onSuccess(model);
                if (model.status) {
                    tvPayPassword.setText("修改支付密码");
                    tvPayPasswordTips.setText("");
                } else {
                    tvPayPassword.setText("设置支付密码");
                    tvPayPasswordTips.setText("未设置");
                }
            }
        }, this);

        APIManager.startRequest(mUserService.hasPassowrd(), new BaseRequestListener<HasPasswordModel>(this) {
            @Override
            public void onSuccess(HasPasswordModel model) {
                super.onSuccess(model);
                if (model.status) {
                    mTvPasswordTips.setText("");
                    mTvPassword.setText("修改登录密码");
                } else {
                    mTvPassword.setText("设置登录密码");
                    mTvPasswordTips.setText("未设置");
                }
            }
        }, this);

        APIManager.startRequest(mUserService.getUpMember(), new BaseRequestListener<UpMemberModel>(this) {
            @Override
            public void onSuccess(UpMemberModel result) {
                mUpMemberModel = result;
                mInvitationTv.setText(result.nickName);
            }

            @Override
            public void onError(Throwable e) {

            }
        }, this);

    }

    private void setUserData(User user) {
        mUser = user;

        FrescoUtil.setImageSmall(mAvatarIv, user.avatar);
        mNicknameTv.setText(user.nickname);
        if (StringUtils.isEmpty(user.phone)) {
            mPhoneTv.setText("未绑定");
        } else {
            mPhoneTv.setText(StringUtil.maskPhone(user.phone));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        showHeader();
        setTitle("我的资料");
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_black);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        MyApplication.isShowPayPwd = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        MyApplication.isShowPayPwd = false;
    }

    @OnClick(R.id.logoutBtn)
    protected void logout() {
        APIManager.startRequest(mUserService.logout(), new BaseRequestListener<Object>(this) {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);

                UserService.logout(ProfileActivity.this);
//                getSharedPreferences(Const.READ_HIGOU_NEED_TO_KNOW, 0).edit().putBoolean(Const.READ_HIGOU_NEED_TO_KNOW, false);
                ProfileActivity.this.finish();
            }

        }, this);
    }

    @OnClick(R.id.avatarLayout)
    protected void changeAvatar() {
        UploadManager.selectImage(this);
    }

    private void uploadImage(Uri uri) {
        UploadManager.uploadImage(this, uri, new BaseRequestListener<UploadResponse>(this) {
            @Override
            public void onSuccess(UploadResponse result) {
                updateAvatar(result.url);
            }
        });
    }

    private void updateAvatar(final String imageUrl) {
        APIManager.startRequest(mUserService.updateAvatar(imageUrl), new BaseRequestListener<Object>(this) {
            @Override
            public void onSuccess(Object result) {
                EventBus.getDefault().post(new EventMessage(Event.updateAvatar, imageUrl));
                FrescoUtil.setImageSmall(mAvatarIv, imageUrl);
                User loginUser = SessionUtil.getInstance().getLoginUser();
                loginUser.avatar = imageUrl;
                SessionUtil.getInstance().setLoginUser(loginUser);
                // 更新im头像
                LoginActivity.updateIMUserProfile();
            }
        }, this);
    }

    @OnClick(R.id.nicknameLayout)
    protected void editNickname() {
        if (mUser == null) {
            return;
        }
        Intent intent = new Intent(this, EditNicknameActivity.class);
        intent.putExtra("nickname", mUser.nickname);
        startActivity(intent);
    }

    @OnClick(R.id.phoneLayout)
    protected void editPhone() {
        if (mUser == null) {
            return;
        }
        if (StringUtils.isEmpty(mUser.phone)) {
            startActivity(new Intent(this, BindPhoneActivity.class));
        } else {
            UserService.checkHasPassword(this, new UserService.HasPasswordListener() {
                @Override
                public void onHasPassword() {
                    Intent intent = new Intent(ProfileActivity.this, EditPhoneActivity.class);
                    intent.putExtra("phone", mUser.phone);
                    startActivity(intent);
                }
            });
        }
    }

    @OnClick(R.id.passwordLayout)
    protected void editPassword() {
        if (mUser == null) {
            return;
        }
        if (StringUtils.isEmpty(mUser.phone)) {
            ToastUtils.showShortToast("请先绑定手机");
            startActivity(new Intent(this, BindPhoneActivity.class));
            return;
        }
        APIManager.startRequest(mUserService.hasPassowrd(), new BaseRequestListener<HasPasswordModel>(this) {
            @Override
            public void onSuccess(HasPasswordModel model) {
                super.onSuccess(model);
                if (model.status) {
                    PasswordOptionActivity.start(ProfileActivity.this, false);
                } else {
                    startActivity(new Intent(ProfileActivity.this, SetPasswordActivity.class));
                }
            }
        }, this);
    }

    @OnClick(R.id.llPayPassword)
    protected void editPayPassword() {
        if (mUser == null) {
            return;
        }
        if (TextUtils.isEmpty(mUser.phone)) {
            ToastUtils.showShortToast("请先绑定手机");
            startActivity(new Intent(this, BindPhoneActivity.class));
            return;
        }
        if (mUser.autoAuthStatus != AppTypes.AUTH_STATUS.SUCESS) {
            Intent intent2 = new Intent(this, AuthPhoneActivity.class);
            SharedPreferences sharedPreferences = getSharedPreferences("authType", 0);
            sharedPreferences.edit().putString("authType", "UserCenter").commit();
            intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
            startActivity(intent2);
            MyApplication.isShowPayPwd = true;
            return;
        }

        APIManager.startRequest(mUserService.hasPayPwd(), new BaseRequestListener<HasPasswordModel>(this) {
            @Override
            public void onSuccess(HasPasswordModel model) {
                super.onSuccess(model);
                if (model.status) {
                    PasswordOptionActivity.start(ProfileActivity.this, true);
                } else {
                    SetPayPasswordActivity.start(ProfileActivity.this);
                }
            }
        }, this);
    }

    @OnClick(R.id.invitationLayout)
    protected void onClickInvitation() {
        if (TextUtils.isEmpty(SessionUtil.getInstance().getLoginUser().referrerMemberId)) {
            startActivity(new Intent(this, InvitationEditActivity.class));
        } else if (mUpMemberModel == null) {
            ToastUtil.error("上级不存在");
        } else {
            startActivity(new Intent(this, InvitationActivity.class));
        }
    }

    @OnClick(R.id.aboutUsLayout)
    protected void onClickAboutUs() {
        startActivity(new Intent(this, AboutUsActivity.class));
    }


    @OnClick(R.id.addressLayout)
    protected void viewAddressList() {
        startActivity(new Intent(this, AddressListActivity.class));
    }

    @OnClick(R.id.layoutCS)
    public void goCS() {
        CSUtils.start(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Config.REQUEST_CODE_CHOOSE_IMAGE_SELECT && resultCode == RESULT_OK) {
            List<Uri> uris = Matisse.obtainResult(data);
            LogUtils.e("拿到图片" + uris.get(0).getPath());
            uploadImage(uris.get(0));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatusMsg(MsgStatus message) {
        switch (message.getAction()) {
            case MsgStatus.ACTION_EDIT_PHONE:
                finish();
                break;
            case MsgStatus.ACTION_USER_CHANGE:
                initData();
                break;
            default:
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(EventMessage message) {
        if (message.getEvent().equals(Event.goMain) || message.getEvent().equals(Event.goToLogin)) {
            finish();
        } else if (message.getEvent().equals(Event.isEditLoginPwdSuccess)) {
            logout();
        }
    }
}
