package com.weiju.ccmall.module.user.viewholders;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.ChangeCustomerNoteNameActivity;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Family;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FamilyRecommendHolder extends BaseViewHolder {
    @BindView(R.id.avatarIv)
    SimpleDraweeView avatarIv;
    @BindView(R.id.UserLevel)
    TextView UserLevel;
    @BindView(R.id.noteName)
    TextView noteName;
    @BindView(R.id.nickName)
    TextView nickName;

    private Family.DatasEntity recommend;

    public FamilyRecommendHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public static FamilyRecommendHolder newInstance(ViewGroup parent) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_family_recommend, parent, false);
        return new FamilyRecommendHolder(v);
    }

    public void bindView(Family.DatasEntity item) {
        this.recommend = item;
        FrescoUtil.setImageSmall(avatarIv, item.headImage);
        UserLevel.setText(item.memberTypeStr);
        noteName.setText(TextUtils.isEmpty(item.noteName) ? item.nickName : item.noteName);
        if (TextUtils.isEmpty(item.noteName)) {
            nickName.setVisibility(View.INVISIBLE);
        } else {
            nickName.setVisibility(View.VISIBLE);
            nickName.setText("昵称:" + item.nickName);
        }
    }

    @OnClick({R.id.noteBtn, R.id.chatBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.noteBtn:
                ChangeCustomerNoteNameActivity.start((Activity) view.getContext(),
                        recommend.memberId, recommend.noteName);
                break;
            case R.id.chatBtn:
                contactCustomer(view.getContext(), recommend);
                break;
        }
    }

    private void contactCustomer(Context context, Family.DatasEntity item) {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null && item != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, context);
                    ChatInfo chatInfo = new ChatInfo();
                    chatInfo.setType(TIMConversationType.C2C);
                    chatInfo.setId(item.memberId);
                    chatInfo.setChatName(TextUtils.isEmpty(item.noteName) ? item.nickName : item.noteName);
                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.putExtra("chatInfo", chatInfo);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }
}
