package com.weiju.ccmall.module.xysh.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;

import java.util.List;

/**
 * @author chenyanming
 * @time 2019/6/4 on 16:37
 * @desc ${TODD}
 */
public class SelectBankTitleAdapter extends BaseQuickAdapter<String,BaseViewHolder> {
    public SelectBankTitleAdapter() {
        super(R.layout.item_select_bank_title);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tvTitle,item);
    }
}
