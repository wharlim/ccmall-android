package com.weiju.ccmall.module.community;

import android.text.TextUtils;

import com.blankj.utilcode.utils.TimeUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by bigbyto on 30/06/2017.
 */

public class DateUtils {
    public static String getYM(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);

        return String.format("%s%s", year, month);
    }

    public static String getYMR(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        return String.format("%s-%s-%s", year, month, day);
    }

    public static String parseYM(int ym) {
        String str = String.valueOf(ym);
        String year = str.substring(0, 4);
        String month = str.substring(4, str.length());

        return String.format("%s年%s月", year, month);
    }

    public static String getVoiceTime(String timeStr) {
        if (TextUtils.isEmpty(timeStr)) {
            return "00:00";
        }

        int time = 0;
        try {
            time = Integer.parseInt(timeStr);
        } catch (Exception e) {
            e.printStackTrace();
            return "00:00";
        }

        int ONE_MINUTE = 60;
        int minute = time / ONE_MINUTE;
        int sec = (time - minute * ONE_MINUTE) % ONE_MINUTE;

        String minuteStr = "";
        String secStr = "";
        if (minute < 10) {
            minuteStr = "0" + minute;
        } else {
            minuteStr += minute;
        }

        if (sec < 10) {
            secStr = "0" + sec;
        } else {
            secStr += sec;
        }

        return minuteStr + ':' + secStr;
    }

    public static String getDateString(String dateStr) {
        if (TextUtils.isEmpty(dateStr)) {
            return "";
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = null;
        try {
            date = format.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            return "1980-01-01";
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        Date date2 = new Date();

        long delta = System.currentTimeMillis() - date.getTime();
        int ONE_DAY = 86400000;
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        calendar.setTime(date2);
        int today = calendar.get(Calendar.DAY_OF_MONTH);

        if (delta < ONE_DAY && day == today) {
            return String.format(Locale.getDefault(), "%02d:%02d", hours, minute);
        } else if (delta <= ONE_DAY * 2) {
            return "昨天";
        } else if (delta > ONE_DAY * 2 && delta <= ONE_DAY * 3) {
            return "前天";
        } else {
            return String.format(Locale.getDefault(), "%d-%02d-%02d", year, month + 1, day);
        }
    }

    public static Date parseDateString(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date;
        try {
            date = format.parse(dateString);
        } catch (Exception e) {
            date = new Date();
            e.printStackTrace();
        }

        return date;
    }

    public static int getYearFromDateString(String dateString) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parseDateString(dateString));

        return calendar.get(Calendar.YEAR);
    }

    public static int getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar.get(Calendar.YEAR);
    }

    public static boolean isThisYear(Date date) {
        int year = getYear(date);
        int thisYear = getYear(new Date());

        return year == thisYear;
    }

    public static String getYMD(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return format.format(date);
    }

    public static String getCurrentTime() {
        Calendar time = Calendar.getInstance();
        time.setTime(new Date());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(time.getTime());
    }

    public static String getDateBefore(int day) {
        Calendar now = Calendar.getInstance();
        now.setTime(new Date());
        now.set(Calendar.DATE, now.get(Calendar.DATE) - day + 1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(now.getTime());
    }

    public static String getDayDateBefore(Date date, int day) {
        Calendar now = Calendar.getInstance();
        now.setTime(date);
        now.set(Calendar.DATE, now.get(Calendar.DATE) - day + 1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(now.getTime());
    }

    public static String getDayDateAfter(Date date, int day) {
        Calendar now = Calendar.getInstance();
        now.setTime(date);
        now.set(Calendar.DATE, now.get(Calendar.DATE) + day - 1);
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(now.getTime());
    }

    public static Date parseDateString2(String dateString) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date date;
        try {
            date = format.parse(dateString);
        } catch (Exception e) {
            date = new Date();
            e.printStackTrace();
        }

        return date;
    }

    /**
     * 计算当前日期
     *
     * @return
     */
    public static int[] getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        return new int[]{calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH)};
    }

    public static int longOfTwoDate(Date first, Date second) throws ParseException {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(first);
        int cnt = 0;
        while (calendar.getTime().compareTo(second) != 0) {
            calendar.add(Calendar.DATE, 1);
            cnt++;
        }
        cnt++;
        return cnt;
    }

    public static String getStandDate(Date dt) {
        DateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
        return dFormat.format(dt);
    }

    public static String getMonthDay(String createDate) {
        Date date = TimeUtils.string2Date(createDate);
        return  TimeUtils.date2String(date, "M月d日");


    }

    private static final long ONE_MINUTE = 60;
    private static final long ONE_HOUR = 3600;
    private static final long ONE_DAY = 86400;
    private static final long ONE_MONTH = 2592000;
    private static final long ONE_YEAR = 31104000;

    /**
     * 距离今天多久
     * @param date
     * @return
     *
     */
    public static String fromToday(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        long time = date.getTime() / 1000;
        long now = new Date().getTime() / 1000;
        long ago = now - time;
        if (ago <= ONE_MINUTE) {
            return ago + "秒前";
        }
        else if (ago <= ONE_HOUR)
            return ago / ONE_MINUTE + "分钟前";
        else if (ago <= ONE_DAY)
            return ago / ONE_HOUR + "小时" + (ago % ONE_HOUR / ONE_MINUTE)
                    + "分钟前";
        else if (ago <= ONE_DAY * 2)
            return "昨天" + calendar.get(Calendar.HOUR_OF_DAY) + "点"
                    + calendar.get(Calendar.MINUTE) + "分";
        else if (ago <= ONE_DAY * 3)
            return "前天" + calendar.get(Calendar.HOUR_OF_DAY) + "点"
                    + calendar.get(Calendar.MINUTE) + "分";
        else if (ago <= ONE_MONTH) {
            long day = ago / ONE_DAY;
            return day + "天前" + calendar.get(Calendar.HOUR_OF_DAY) + "点"
                    + calendar.get(Calendar.MINUTE) + "分";
        } else if (ago <= ONE_YEAR) {
            long month = ago / ONE_MONTH;
            long day = ago % ONE_MONTH / ONE_DAY;
            return month + "个月" + day + "天前"
                    + calendar.get(Calendar.HOUR_OF_DAY) + "点"
                    + calendar.get(Calendar.MINUTE) + "分";
        } else {
            long year = ago / ONE_YEAR;
            int month = calendar.get(Calendar.MONTH) + 1;// JANUARY which is 0 so month+1
            return year + "年前" + month + "月" + calendar.get(Calendar.DATE)
                    + "日";
        }

    }

    /*
     * 将时间转换为时间戳
     */
    public static long dateToStamp(String time){
//        String stap;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = simpleDateFormat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long ts = date.getTime();//获取时间的时间戳
//        stap = String.valueOf(ts);
        return ts;
    }
}
