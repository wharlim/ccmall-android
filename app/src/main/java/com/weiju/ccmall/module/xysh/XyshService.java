package com.weiju.ccmall.module.xysh;

import com.weiju.ccmall.module.xysh.bean.ApplyCreditCardItem;
import com.weiju.ccmall.module.xysh.bean.ApplyCreditCardUrlResult;
import com.weiju.ccmall.module.xysh.bean.ApplyCreditCommonResult;
import com.weiju.ccmall.module.xysh.bean.CardDoctorHistoryItem;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.bean.MerchantInResult;
import com.weiju.ccmall.module.xysh.bean.PlanCreateItem;
import com.weiju.ccmall.module.xysh.bean.PlanDetail;
import com.weiju.ccmall.module.xysh.bean.PlanForCard;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.ReceiptChannelItem;
import com.weiju.ccmall.module.xysh.bean.ReceiptOrder;
import com.weiju.ccmall.module.xysh.bean.ReceiptSupportBankEntity;
import com.weiju.ccmall.module.xysh.bean.ReceivablesBill;
import com.weiju.ccmall.module.xysh.bean.SNCode;
import com.weiju.ccmall.module.xysh.bean.SelectSupportBankItem;
import com.weiju.ccmall.module.xysh.bean.SupportBanksItem;
import com.weiju.ccmall.module.xysh.bean.TagListItem;
import com.weiju.ccmall.module.xysh.bean.TodayPayStatus;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.bean.XyshUser;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.bean.Bank;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by 林先生_lcy.
 * User: lcy
 * Date: 2019/4/3
 * Time: 上午9:18
 */
public interface XyshService {

    @Multipart
    @POST("api/userPersonRelauth")
    Observable<XYSHCommonResult<Object>> userPersonRelauth(@Query("usrTel") String usrTel,
                                                        @Query("usrName") String usrName,
                                                        @Query("idNo") String idNo,
                                                        @Query("token") String token,
                                                        @Part MultipartBody.Part idhandlepic,
                                                        @Part MultipartBody.Part livingbody,
                                                        @Part MultipartBody.Part idfrontpic,
                                                        @Part MultipartBody.Part idbackpic);

    @GET("api/getArea")
    Observable<RequestResult<List<String>>> getArea(@Query("token") String token,
                                                    @Query("prov") String prov,
                                                    @Query("city") String city);

    @FormUrlEncoded
    @POST("api/userBankRelauth")
    Observable<XYSHCommonResult<Object>> userBankRelauth(@Field("bankNumber") String bankNumber,
                                                      @Field("bankNm") String bankNm,
                                                      @Field("usrTel") String usrTel,
                                                      @Field("cardNo") String cardNo,
                                                      @Field("resTel") String resTel,
                                                      @Field("token") String token,
                                                      @Field("prov") String prov,
                                                      @Field("city") String city,
                                                      @Field("zone") String zone);

    @FormUrlEncoded
    @POST("api/getUserInfo")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<XyshUser>> userPersonCenter(
            @Field("token") String token
    );

    @FormUrlEncoded
    @POST("api/bankCNAPS")
    Observable<RequestResult<List<Bank>>> bankCNAPS(@Field("usrTel") String usrTel, @Field("token") String token);

    // 获取 银行卡管理列表，0 为信用卡，1 为储蓄卡
    @FormUrlEncoded
    @POST("api/cardInfoGet")
    Observable<QueryUserBankCardResult> cardInfoGet(@Field("cardType") int cardType);

    // 更新银行卡信息 defaultFlag 0 为信用卡，1 为储蓄卡
    @FormUrlEncoded
    @POST("api/new/changeBankInfo")
    Observable<HashMap<String, String>> changeBankInfo(@Field("bankCardNo") String bankCardNo,
                                       @Field("phone") String phone,
                                       @Field("bankName") String bankName,
                                       @Field("defaultFlag") String defaultFlag,
                                       @Field("billDate") String billDate,
                                       @Field("repayDate") String repayDate);
    // 新增银行卡信息 defaultFlag 0 为信用卡，1 为储蓄卡
    @FormUrlEncoded
    @POST("/api/new/newBankAdd")
    Observable<HashMap<String, String>> newBankAdd(@Field("bankCardNo") String bankCardNo,
                                                       @Field("phone") String phone,
                                                       @Field("bankName") String bankName,
                                                       @Field("bankCardType") String bankCardType,
                                                       @Field("billDate") String billDate,
                                                       @Field("repayDate") String repayDate);

    @FormUrlEncoded
    @POST("api/new/deleteBankInfo")
    Observable<XYSHCommonResult<Object>> deleteBankInfo(@Field("bankCardNo") String bankCardNo);

    @FormUrlEncoded
    @POST("api/paymentChannel/getPaymentChannelList")
    Observable<XYSHCommonResult<List<ChannelItem>>> getAllOnlineChannels(
            @Field("creditCardNo") String creditCardNo,
            @Field("channelType") int channelType);

    // 获取通道支持银行
    @GET("repay/channel/bank/support/{channelShortName}")
    Observable<XYSHCommonResult<List<SupportBanksItem>>> supportBank(@Path("channelShortName") String channelShortName);

    // 通道入网
    @FormUrlEncoded
    @POST("repay/authChannel/merchantIn")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<MerchantInResult>> merchantIn(
            @Field("channelNo") String channelNo,
            @Field("creditCardNo") String creditCardNo,
            @Field("cardCvv") String cardCvv,
            @Field("cardExpire") String cardExpire,
            @Field("province") String province,
            @Field("city") String city,
            @Field("county") String county,
            @Field("address") String address,
            @Field("versionNo") String versionNo);

    // 申请绑卡
    @FormUrlEncoded
    @POST("repay/authChannel/sendSMS")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<Object>> authChannelSendSMS(
            @Field("channelNo") String channelNo,
            @Field("creditCardNo") String creditCardNo);

    // 确认绑卡
    @FormUrlEncoded
    @POST("repay/authChannel/cardOpen")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<Object>> authChannelCardOpen(
            @Field("channelNo") String channelNo,
            @Field("creditCardNo") String creditCardNo,
            @Field("smsCode") String smsCode);

    // 创建计划
    @FormUrlEncoded
    @POST("api/repayPlan/payPlanCreate")
    Observable<XYSHCommonResult<PlanCreateItem>> payBackPlanCreate(
            @Field("payBackAmount") double payBackAmount, // 还款金额
            @Field("frozenAmount") double frozenAmount, // 周转金
            @Field("startDate") String startDate, // 还款开始时间(全自动必填)
            @Field("endDate") String endDate, // 还款结束时间(全自动必填)
            @Field("cardNo") String cardNo,
            @Field("repaymentChannelId") String repaymentChannelId,
            @Field("areaCode") String areaCode,
            @Field("planType") int planType, // planType=0:全自动，planType=1:半自动
            @Field("debitCardNo") String debitCardNo // 到账卡(半自动必填)
//            @Field("billDate") String billDate, // 账单日(半自动必填)
//            @Field("repayDate") String repayDate // 还款日(半自动必填)
    );

    // 确认计划
    @FormUrlEncoded
    @POST("api/repayPlan/payBackPlanConfirm")
    Observable<XYSHCommonResult<Object>> payBackPlanConfirm(
            @Field("planNo") String planNo);

    // 查询银行卡的还款计划
    @GET("api/profit/newPayBackPlanStatusQuery")
    Observable<XYSHCommonResult<PlanForCard.PlanForCardContainer>> newPayBackPlanStatusQuery(
            @Query("cardNo") String cardNo, @Query("tranSts") String tranSts,
            @Query("page") int page, @Query("limit") int limit);

    // 查询还款计划详情
    @FormUrlEncoded
    @POST("api/payBackPlanListNew")
    Observable<PlanDetail> payBackPlanList(
            @Field("usrTel") String usrTel,
            @Field("planNo") String planNo);

    // 查询收款账单
    @GET("api/getTxnJnlVos")
    Observable<XYSHCommonResult<List<ReceivablesBill>>> getTxnJnlVos(@Query("month") String month,
                                                                     @Query("year") String year);

    //绑定SN码查看
    @GET("api/sn/getBindData")
    Observable<XYSHCommonResult<SNCode.SNCodeResult>> getBindData();


    // 确认绑定SN码
    @FormUrlEncoded
    @POST("api/sn/bindSnCode")
    Observable<XYSHCommonResult<Object>> bindSnCode(
            @Field("snCode") String snCode);

    @FormUrlEncoded
    @POST("api/integral/getIntegralUrl")
    Observable<XYSHCommonResult<String>> getIntegralUrl(
            @Field("bankCardNo") String bankCardNo,
            @Field("bankName") String bankName,
            @Field("phoneNo") String phoneNo);

    // 终止计划
    @FormUrlEncoded
    @POST("api/terminatePayBackPlan")
    Observable<XYSHCommonResult<Object>> terminatePayBackPlan(
            @Field("planNo") String planNo);

    // 获取支持的银行列表
    @GET("api/supportBankList/getSupportBankList")
    Observable<XYSHCommonResult<List<SelectSupportBankItem>>> getSupportBankList();
    // 信用卡申请，全部银行
    @GET("apply/applycard/getBankList?appToken=" + BankUtils.sAppToken)
    Observable<ApplyCreditCommonResult<List<ApplyCreditCommonResult.BankItem>>> getBankList();
    // 信用卡申请，全部等级
    @GET("apply/applycard/getBankTags?appToken=" + BankUtils.sAppToken)
    Observable<ApplyCreditCommonResult<List<TagListItem>>> getBankTags();
    // 信用卡申请，全部主题
    @GET("apply/applycard/getBankTopics?appToken=" + BankUtils.sAppToken)
    Observable<ApplyCreditCommonResult<List<TagListItem>>> getBankTopics();
    // 信用卡申请 银行列表
    @GET("apply/applycard/getBankAisle")
    Observable<ApplyCreditCommonResult<List<ApplyCreditCardItem>>> getBankAisle(
            @Query("appToken") String appToken,
            @Query("topicIds") String topicIds,
            @Query("tagIds") String tagIds,
            @Query("bankIds") String bankIds,
            @Query("current") String current
    );
    @FormUrlEncoded
    @POST("apply/applycard/applyCreditCard")
    Observable<ApplyCreditCommonResult<List<ApplyCreditCardUrlResult>>> applyCreditCard(
            @Field("appToken") String appToken,
            @Field("appUserId") String appUserId,
            @Field("bankCode") String bankCode,
            @Field("bankName") String bankName,
            @Field("clientNo") String clientNo,
            @Field("idCard") String idCard,
            @Field("mobile") String mobile,
            @Field("name") String name,
            @Field("stationBankCardChannelId") String stationBankCardChannelId,
            @Field("stationChannelId") String stationChannelId
    );

    // 信用卡评测
    @FormUrlEncoded
    @POST("api/product/creditCardAppraisalSendMsg")
    Observable<XYSHCommonResult<String>> creditCardAppraisalSendMsg(
            @Field("bankCard") String bankCard,
            @Field("mobile") String mobile,
            @Field("name") String name,
            @Field("idCard") String idCard,
            @Field("token") String token
    );
    // 信用卡评测 - 验证短信
    @FormUrlEncoded
    @POST("api/product/msgVerify")
    Observable<XYSHCommonResult<String>> msgVerify(
            @Field("identifyingCode") String identifyingCode, // 短信验证码
            @Field("serialNumber") String serialNumber,
            @Field("token") String token
    );

    // 卡医生(信用卡评测短信确认-返回第三方URL)
    @FormUrlEncoded
    @POST("api/product/getAppraisalResult")
    Observable<XYSHCommonResult<String>> getAppraisalResult(
            @Field("identifyingCode") String identifyingCode, // 短信验证码
            @Field("serialNumber") String serialNumber,
            @Field("bankCard") String bankCard,
            @Field("mobile") String mobile,
            @Field("orderNo") String orderNo,
            @Field("name") String name,
            @Field("idCard") String idCard,
            @Field("token") String token
    );

    @GET("api/creditCardAppraisal/getCreditCardAppraisalByUserNo")
    Observable<XYSHCommonResult<List<CardDoctorHistoryItem>>> getCreditCardAppraisalByUserNo(
            @Query("token") String token
    );
    // 获取所有收款通道
    @FormUrlEncoded
    @POST("api/paymentChannel/getAllOnlinePayChannels")
    Observable<XYSHCommonResult<List<ReceiptChannelItem>>> getAllOnlinePayChannels(@Field("token") String token, @Field("creditCardNo") String creditCardNo);

    // 获取收款通道支持银行
    @GET("api/payCommon/getSupportBank")
    Observable<XYSHCommonResult<List<ReceiptSupportBankEntity>>> getSupportBank(
            @Query("token") String token,
            @Query("channelId") String channelId,
            @Query("type") String type // 可为null
    );

    // 国付通大额Q收款通道 是否认证
    @GET("repay/gftqPay/isOpen")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<Object>> isOpen(
            @Query("masterChannelNo") String masterChannelNo,
            @Query("creditCardNo") String creditCardNo
    );

    @GET("repay/ctfPay/isOpen")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<MerchantInResult>> isCtfOpen(
            @Query("channelNo") String channelNo,
            @Query("creditCardNo") String creditCardNo
    );

    /**
     * @param channelNo 通道编号
     * @param debitCardNo   到账卡
     * @param creditCardNo 信用卡卡号
     * @return
     */
    @GET("repay/ctfPay/bindCheck")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<Object>> bindCheck(
            @Query("channelNo") String channelNo,
            @Query("debitCardNo") String debitCardNo,
            @Query("creditCardNo") String creditCardNo
    );

    // 国付通大额Q收款通道 入网
    @FormUrlEncoded
    @POST("repay/gftqPay/merchantIn")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<MerchantInResult>> gftqPay_merchantIn(
            @Field("debitCardNo") String debitCardNo,
            @Field("creditCardNo") String creditCardNo,
            @Field("cardCvv") String cardCvv,
            @Field("cardExpire") String cardExpire,
            @Field("province") String province,
            @Field("city") String city,
            @Field("county") String county,
            @Field("address") String address
    );

    @FormUrlEncoded
    @POST("repay/ctfPay/merchantIn")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<MerchantInResult>> ctfPay_merchantIn(
            @Field("debitCardNo") String debitCardNo,
            @Field("creditCardNo") String creditCardNo,
            @Field("cardCvv") String cardCvv,
            @Field("cardExpire") String cardExpire,
            @Field("province") String province,
            @Field("city") String city
    );

    // 国付通大额Q收款通道 申请绑卡接口
    @FormUrlEncoded
    @POST("repay/gftqPay/sendSMS")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<Object>> gftqPay_SendSMS(
            @Field("creditCardNo") String creditCardNo);

    // 国付通大额Q收款通道 确认绑卡接口
    @FormUrlEncoded
    @POST("repay/gftqPay/cardOpen")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<Object>> gftqPay_cardOpen(
            @Field("creditCardNo") String creditCardNo,
            @Field("smsCode") String smsCode
    );

    // 申请扣款接口
    @POST("repay/gftqPay/pay")
    Observable<XYSHCommonResult<String>> gftqPay_pay(
            @Query("payMoney") float payMoney,
            @Query("payBankCard") String payBankCard,
            @Query("intoBankCard") String intoBankCard,
            @Query("userId") String userId,
            @Query("oemId") String oemId
    );

    @POST("repay/ctfPay/pay")
    Observable<XYSHCommonResult<String>> ctfPay(
            @Query("payMoney") float payMoney,
            @Query("payBankCard") String payBankCard,
            @Query("intoBankCard") String intoBankCard,
            @Query("userId") String userId,
            @Query("oemId") String oemId
    );

    // 确认扣款接口
    @POST("repay/gftqPay/payConfirm")
    Observable<XYSHCommonResult<ReceiptOrder>> payConfirm(
            @Query("orderId") String orderId,
            @Query("smCode") String smCode
    );

    @POST("repay/ctfPay/payConfirm")
    Observable<XYSHCommonResult<ReceiptOrder>> ctfPayConfirm(
            @Query("orderId") String orderId,
            @Query("smCode") String smCode
    );

    // 获取还款状态
    @FormUrlEncoded
    @POST("api/getTodayPayStatus")
    Observable<XYSHCommonResult<TodayPayStatus>> getTodayPayStatus(
            @Field("planNo") String planNo,
            @Field("planExcuteTime") String planExcuteTime
    );

    @FormUrlEncoded
    @POST("api/paymentChannel/versionCheck")
    @Headers({
            "format: formatJson"
    })
    Observable<XYSHCommonResult<Object>> versionCheck(@Field("channelNo") String channelNo);
}
