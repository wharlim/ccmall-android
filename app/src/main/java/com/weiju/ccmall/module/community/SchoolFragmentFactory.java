package com.weiju.ccmall.module.community;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.models.CoursePlate;
import com.weiju.ccmall.shared.util.ConvertUtil;

import java.util.List;


/**
 * @author Stone
 * @time 2018/4/17  17:23
 * @desc ${TODD}
 */

public class SchoolFragmentFactory extends FragmentFactory {
    private List<CoursePlate.CourseCatsBean> mCourseCats;
    public SchoolFragmentFactory(Context context, List<CoursePlate.CourseCatsBean> mCourseCats) {
        super(context);
        this.mCourseCats = mCourseCats;
    }

    @Override
    public String[] getTabTitle() {
//        return mContext.getResources().getStringArray(R.array.arr_school_str);
        String[] tabs = new String[mCourseCats.size()];
        for (int i = 0; i < mCourseCats.size(); i++) {
            tabs[i] = mCourseCats.get(i).title;
        }
        return tabs;
    }

    @Override
    public Fragment getFragment(int position, Fragment fragment) {
//        switch (position) {
//            case 0:
//                fragment = CourseFragment.newInstance(2, true, "", false);
//                break;
//            case 1:
//                fragment = CourseFragment.newInstance(1, true, "", false);
//                break;
//            case 2:
//                fragment = CourseFragment.newInstance(3, true, "", false);
//                break;
//            default:
//                break;
//        }
//        return fragment;
        return CourseFragment.newInstance(mCourseCats.get(position).type, true, "", false, mCourseCats.get(position).plateId);
    }

    @Override
    public int getTabBottomLineWidth() {
        return ConvertUtil.dip2px(70);
    }

}
