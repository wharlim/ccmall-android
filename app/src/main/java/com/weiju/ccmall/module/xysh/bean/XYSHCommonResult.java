package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

public class XYSHCommonResult<T> {

    /**
     * code : 0
     * resCode : 0000
     * success : true
     * message : 删除成功!
     * data : null
     */

    @SerializedName("code")
    public int code;
    @SerializedName("resCode")
    public String resCode;
    @SerializedName("resMsg")
    public String resMsg;
    @SerializedName("success")
    public boolean success;
    @SerializedName("message")
    public String message;
    @SerializedName("msg")
    public String msg;
    @SerializedName("data")
    public T data;
}
