package com.weiju.ccmall.module.blockchain.computingpower;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.beans.AllCCm;
import com.weiju.ccmall.module.community.DateUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BlockAdapter extends RecyclerView.Adapter<BlockAdapter.VH>{

    List<AllCCm.AccountBlockBean.BlockBean> blocks;
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    // 传递数据
    public BlockAdapter(List<AllCCm.AccountBlockBean.BlockBean> blocks) {
        this.blocks = blocks;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return VH.newInstance(LayoutInflater.from(viewGroup.getContext()), viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull VH vh, int i) {
        vh.bindView(blocks.get(i));
    }

    @Override
    public int getItemCount() {
        return blocks.size();
    }

    public static class VH extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_block_no)
        TextView tvBlockNo;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_block_address)
        TextView tvBlockAddress;
        @BindView(R.id.tv_block_state)
        TextView tvBlockState;
        @BindView(R.id.tv_block_reward)
        TextView tvBlockReward;

        VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bindView(AllCCm.AccountBlockBean.BlockBean blockBean) {
            AllCCm.AccountBlockBean.BlockBean.BlockHeaderBean.RawDataBean rawDataBean = blockBean.blockHeader.rawData;
            tvBlockNo.setText("#" + rawDataBean.number);
            tvBlockAddress.setText(rawDataBean.witnessAddress);
//            tvBlockReward.setText("");
//            tvTime.setText(rawDataBean.timestamp);
            long timestamp = Long.valueOf(rawDataBean.timestamp);
//            tvTime.setText(dateFormat.format(new Date(timestamp)));
            tvTime.setText(DateUtils.fromToday(new Date(timestamp)));
        }

        public static VH newInstance(LayoutInflater inflater, ViewGroup parent) {
            View view = inflater.inflate(R.layout.item_computing_power_block, parent, false);
            return new VH(view);
        }
    }
}
