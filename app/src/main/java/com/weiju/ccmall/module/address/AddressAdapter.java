package com.weiju.ccmall.module.address;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.bean.Address;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.address
 * @since 2017-06-10
 */
public class AddressAdapter extends BaseAdapter<Address, AddressAdapter.ViewHolder> {

    private boolean isSelectAddress = false;
    private boolean mIsLottery;
    private boolean mIsReturnGoodsAddress;

    AddressAdapter(Context context, boolean isSelectAddress, boolean isLottery, boolean isReturnGoodsAddress) {
        super(context);
        this.isSelectAddress = isSelectAddress;
        mIsLottery = isLottery;
        mIsReturnGoodsAddress = isReturnGoodsAddress;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_address, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setAddress(items.get(position));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.contactsTv)
        protected TextView mContactsTv;
        @BindView(R.id.phoneTv)
        protected TextView mPhoneTv;
        @BindView(R.id.detailTv)
        protected TextView mDetailTv;
        @BindView(R.id.editBtn)
        protected ImageView mEditBtn;
        @BindView(R.id.addressDefaultIv)
        protected ImageView mDefaultIv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void setAddress(final Address address) {
            mContactsTv.setText(address.contacts);
            mPhoneTv.setText(address.phone);
            mDetailTv.setText(address.getFullAddress());
            mDefaultIv.setVisibility(address.isDefault ? View.VISIBLE : View.GONE);
            if (isSelectAddress) {
                mEditBtn.setVisibility(View.GONE);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mPhoneTv.getLayoutParams();
                layoutParams.rightMargin = SizeUtils.dp2px(15);
            } else {
                mEditBtn.setVisibility(View.VISIBLE);
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mPhoneTv.getLayoutParams();
                layoutParams.rightMargin = SizeUtils.dp2px(0);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!isSelectAddress) {
                        Intent intent = new Intent(context, AddressFormActivity.class);
                        intent.putExtra("action", Key.EDIT_ADDRESS);
                        intent.putExtra("addressId", address.addressId);
                        intent.putExtra("isReturnGoodsAddress", mIsReturnGoodsAddress);
                        context.startActivity(intent);
                    } else {
                        if (mIsLottery) {
                            final WJDialog dialog = new WJDialog(context);
                            dialog.show();
                            dialog.setTitle("选择该收货地址？");
                            dialog.setContentText("选择后将不可更改");
                            dialog.setConfirmText("确定");
                            dialog.setCancelText("取消");
                            dialog.setOnConfirmListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                    EventBus.getDefault().post(new EventMessage(Event.selectLotteryAddress, address));
                                }
                            });
                        } else {
                            EventBus.getDefault().post(new EventMessage(Event.selectAddress, address));
                        }
                    }
                }
            });
        }
    }
}
