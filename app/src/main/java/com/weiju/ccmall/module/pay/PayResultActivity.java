package com.weiju.ccmall.module.pay;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderListActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 支付结果页面
 * <p>
 *
 *
 *
 * @author zjm
 * @date 2018/9/2
 */
public class PayResultActivity extends BaseActivity {

    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.ivLogo)
    ImageView mIvLogo;
    @BindView(R.id.tvTips)
    TextView mTvTips;
    @BindView(R.id.tvOrder)
    TextView mTvOrder;
    @BindView(R.id.payDownView)
    PayResultCountDownView mPayDownView;
    @BindView(R.id.tvFinishRed)
    TextView mTvFinishRed;
    @BindView(R.id.tvFinish)
    TextView mTvFinish;
    private String mOrderCode;
    private IOrderService mService;

    /**
     * 倒计时结束了
     */
    private boolean mIsFinish = false;
    /**
     * 倒计时开始了
     */
    private boolean mIsStart = false;
    /**
     * 请求延迟间隔 (毫秒)
     */
    private final long DELAY_MILLIS = 3000;
    /**
     * 总倒计时时间（秒）
     */
    private final int COUNT_DOWN_TIME = 10;
    /**
     * 开始倒计时
     */
    private final int HANDLER_WHAT_START_DOWN = 2;
    /**
     * 发送网络请求检查订单状态
     */
    private final int HANDLER_WHAT_CHECK_PAY = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_result);
        ButterKnife.bind(this);
        getIntentData();
        initView();
        mService = ServiceManager.getInstance().createService(IOrderService.class);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        LogUtils.e("支付结果  onRestart");
        mHandler.sendEmptyMessage(HANDLER_WHAT_START_DOWN);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LogUtils.e("支付结果  onStart");
    }

    @Override
    protected void onPause() {
        super.onPause();
        LogUtils.e("支付结果  onPause");
        mHandler.removeMessages(HANDLER_WHAT_START_DOWN);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtils.e("支付结果  onResume");
        // 如果500毫秒后没有被暂停，说明是真启动，开启倒计时
        mHandler.sendEmptyMessageDelayed(HANDLER_WHAT_START_DOWN, 500);
    }


    private void startDownTime() {
        if (mIsStart) {
            return;
        }
        mIsStart = true;
        mPayDownView.setCountdownTime(COUNT_DOWN_TIME);
        mPayDownView.startCountDown();
        LogUtils.e("支付结果  倒计时开始");
        mPayDownView.setListener(new PayResultCountDownView.OnCountDownFinishListener() {
            @Override
            public void countDownFinished() {
                LogUtils.e("倒计时结束");
                if (!mIsFinish) {
                    setPayFail();
                    mIsFinish = true;
                    mHandler.removeCallbacksAndMessages(null);
                }
            }
        });

        mHandler.sendEmptyMessageDelayed(1, DELAY_MILLIS);
    }

    private void initView() {
        setTitle("支付结果");
        setLeftBlack();

        mTvTitle.setText("查询中");
        mTvTips.setText(R.string.s_pay_result_loding);
        mTvOrder.setVisibility(View.GONE);
        mIvLogo.setVisibility(View.GONE);
    }

    private void getIntentData() {
        mOrderCode = getIntent().getStringExtra("orderCode");
    }

    private void setPaySucceed() {
        mTvTitle.setText("支付成功");
        mIvLogo.setVisibility(View.VISIBLE);
        mIvLogo.setImageResource(R.drawable.ic_user_auth_submit_succeed);
        mPayDownView.setVisibility(View.GONE);
        mTvTips.setText("您已成功支付，可查看订单详情");
        mTvTips.setTextSize(12);
        mTvOrder.setVisibility(View.VISIBLE);

        mTvFinishRed.setVisibility(View.GONE);
        mTvFinish.setVisibility(View.VISIBLE);
        mTvOrder.setVisibility(View.VISIBLE);
    }

    private void setPayFail() {
        mTvTitle.setText("没有查询结果");
        mIvLogo.setVisibility(View.VISIBLE);
        mIvLogo.setImageResource(R.drawable.ic_user_auth_submit_fail);
        mTvTips.setTextSize(12);
        mTvTips.setText(R.string.s_pay_result_tips);
        mTvOrder.setVisibility(View.VISIBLE);
        mPayDownView.setVisibility(View.GONE);

        mTvFinishRed.setVisibility(View.GONE);
        mTvFinish.setVisibility(View.VISIBLE);
        mTvOrder.setVisibility(View.VISIBLE);
    }

    /**
     * 查询支付结果
     */
    @SuppressLint("CheckResult")
    private void checkPayResult() {
        APIManager.startRequest(
                mService.checkOrderPayStatus(mOrderCode),
                new BaseRequestListener<Object>() {
                    @Override
                    public void onSuccess(Object result) {
                        super.onSuccess(result);
                        mIsFinish = true;
                        mHandler.removeCallbacksAndMessages(null);
                        setPaySucceed();
                        LogUtils.e("支付结果  请求成功");
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.e("支付结果  请求失败");
                        mHandler.sendEmptyMessageDelayed(HANDLER_WHAT_CHECK_PAY, DELAY_MILLIS);
                    }
                },this
        );
    }

    @SuppressLint("HandlerLeak")
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case HANDLER_WHAT_CHECK_PAY:
                    if (!mIsFinish) {
                        mHandler.removeMessages(HANDLER_WHAT_CHECK_PAY);
                        checkPayResult();
                    }
                    break;
                case HANDLER_WHAT_START_DOWN:
                    startDownTime();
                    break;
                default:
            }
        }
    };

    @OnClick(R.id.tvOrder)
    public void onViewClicked() {
        Intent intent = new Intent(this, OrderListActivity.class);
        intent.putExtra("type", "all");
        startActivity(intent);
    }

    @OnClick({R.id.tvFinishRed, R.id.tvFinish})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvFinishRed:
            case R.id.tvFinish:
                finish();
                break;
            default:
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
    }
}
