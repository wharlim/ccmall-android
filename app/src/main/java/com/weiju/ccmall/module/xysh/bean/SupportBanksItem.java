package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

public class SupportBanksItem {

    /**
     * channelNo : Paf
     * bankCode : 102100099996
     * bankName : 工商银行
     * debitCardFlag : 1
     * creditCardFlag : 1
     * createTime : 2018-09-11T04:18:10.000+0000
     * remark : null
     */

    @SerializedName("channelNo")
    public String channelNo;
    @SerializedName("bankCode")
    public String bankCode;
    @SerializedName("bankName")
    public String bankName;
    @SerializedName("debitCardFlag")
    public int debitCardFlag;
    @SerializedName("creditCardFlag")
    public int creditCardFlag;
    @SerializedName("createTime")
    public String createTime;
    @SerializedName("remark")
    public Object remark;
    @SerializedName("single")
    public int single;
    @SerializedName("currentDay")
    public int currentDay;
}
