package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.adapter.ReportAdapter;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveReportTypesItem;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReportActivity extends BaseActivity {

    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.tvNext)
    TextView tvNext;

    private String liveId;

    ReportAdapter adapter = new ReportAdapter();
    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);
    @BindView(R.id.tvSelectTip)
    TextView tvSelectTip;

    public static void start(Context context, String liveId) {
        Intent intent = new Intent(context, ReportActivity.class);
        intent.putExtra("liveId", liveId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        setTitle("举报");
        liveId = getIntent().getStringExtra("liveId");
        setLeftBlack();
        ButterKnife.bind(this);
        initReportTypes();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initReportTypes() {
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter ad, View view, int position) {
//                Log.d("Seven", "onItemClice " + position);
                adapter.setSelectedItem(adapter.getItem(position));
            }
        });
        rvList.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        rvList.setAdapter(adapter);
        ToastUtil.showLoading(this);
        APIManager.startRequest(mService.getAllReportTitle(), new BaseRequestListener<List<LiveReportTypesItem>>() {
            @Override
            public void onSuccess(List<LiveReportTypesItem> result) {
                adapter.setNewData(result);
                ToastUtil.hideLoading();
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.hideLoading();
            }
        }, this);
    }

    @OnClick(R.id.tvNext)
    public void onNext() {
        if (adapter.getSelectedItem() == null) {
            tvSelectTip.setVisibility(View.VISIBLE);
            return;
        }
        ReportUploadActivity.start(this, liveId,
                new ArrayList<>(Arrays.asList(adapter.getSelectedItem().reportTitleListId)));
    }

    @Subscribe
    public void onReportSuccess(EventMessage ev) {
        if (ev.getEvent() == Event.liveReportSuccess) { // 举报成功
            finish();
        }
    }
}
