package com.weiju.ccmall.module.xysh.helper;

import android.app.Activity;
import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.bean.SelectSupportBankItem;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Pattern;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class BankUtils {

    private static List<SelectSupportBankItem> sSupportBanks = new ArrayList<>();
    private static XyshService xyshService = ServiceManager.getInstance().createService2(XyshService.class);
    public static void downSupportBanks() {
        APIManager.startRequest(xyshService.getSupportBankList(), new Observer<XYSHCommonResult<List<SelectSupportBankItem>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<List<SelectSupportBankItem>> ret) {
                if (ret.success) {
                    sSupportBanks.clear();
                    sSupportBanks.addAll(ret.data);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public static List<SelectSupportBankItem> getSupportBanks() {
        return sSupportBanks;
    }

    public static SelectSupportBankItem getSupportBankByAbbr(String abbr) {
        SelectSupportBankItem ret = null;
        for (SelectSupportBankItem i: sSupportBanks) {
            if (i.bankAbbr.equals(abbr)) {
                ret = i;
                break;
            }
        }
        return ret;
    }

    public static int getBankIconByName(String bankName) {
        if(TextUtils.isEmpty(bankName)) {
            return R.drawable.ic_common_bank;
        } else if (bankName.contains("北京")) {
            return R.drawable.ic_bank_of_beijing;
        } else if (bankName.contains("工商")) {
            return R.drawable.ic_industrial_and_commercial_bank_of_china;
        } else if (bankName.contains("光大")) {
            return R.drawable.ic_china_everbright_bank;
        } else if (bankName.contains("广发")) {
            return R.drawable.ic_china_guangfa_bank;
        } else if (bankName.contains("广州")) {
            return R.drawable.ic_bank_of_guangzhou;
        } else if (bankName.contains("杭州")) {
            return R.drawable.ic_bank_of_hangzhou;
        } else if (bankName.contains("恒丰")) {
            return R.drawable.ic_hengfeng_bank;
        } else if (bankName.contains("华瑞")) {
            return R.drawable.ic_shanghai_huarui_bank;
        } else if (bankName.contains("华夏")) {
            return R.drawable.ic_hua_xia_bank;
        } else if (bankName.contains("建设")) {
            return R.drawable.ic_china_construction_bank_corporation;
        } else if (bankName.contains("交通")) {
            return R.drawable.ic_bank_of_communications;
        } else if (bankName.contains("民生")) {
            return R.drawable.ic_china_minsheng_bank;
        } else if (bankName.contains("宁波")) {
            return R.drawable.ic_bank_of_ningbo;
        } else if (bankName.contains("农业")) {
            return R.drawable.ic_agricultural_bank_of_china;
        } else if (bankName.contains("平安")) {
            return R.drawable.ic_ping_an_bank;
        } else if (bankName.contains("浦发")) {
            return R.drawable.ic_spd_bank;
        } else if (bankName.contains("上海")) {
            return R.drawable.ic_bank_of_shanghai;
        } else if (bankName.contains("潍坊")) {
            return R.drawable.ic_bank_of_weifang;
        } else if (bankName.contains("兴业")) {
            return R.drawable.ic_industrial_bank;
        } else if (bankName.contains("邮储")) {
            return R.drawable.ic_postal_savings_bank_of_china;
        } else if (bankName.contains("招商")) {
            return R.drawable.ic_china_merchants_bank;
        } else if (bankName.contains("浙商")) {
            return R.drawable.ic_china_zheshang_bank;
        } else if (bankName.contains("中国")) {
            return R.drawable.ic_bank_of_china;
        } else if (bankName.contains("中信")) {
            return R.drawable.ic_citic_bank;
        } else {
            return R.drawable.ic_common_bank;
        }
    }
    public static String formatBankCardNo(String bankCardNo) {
        int len = bankCardNo.length();
        int partCount = (len+3) / 4;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < partCount - 1; i++) {
            sb.append("**** ");
        }
        sb.append(bankCardNo.substring((partCount-1) * 4));
        return sb.toString();
    }

    public static String cutBankCardNo(String bankCardNo) {
        return bankCardNo.substring(bankCardNo.length() - 4);
    }

    public interface OnCheckCardResultListener {
        void onCheckResult(boolean pass, String type);
    }

    public static abstract class OnCheckCardBankResultListener implements OnCheckCardResultListener {
        public abstract void onCheckResult(boolean pass, String type, String bank);

        @Override
        public void onCheckResult(boolean pass, String type) {
            // no op
        }
    }

    public static final String sAppToken = "Gvh5HfD2TCmtAlGnh7pQAg==";

    public static final int CARD_CHECK_ACCEPT_DEBIT = 1;
    public static final int CARD_CHECK_ACCEPT_CREDIT = 2;
    public static final int CARD_CHECK_ACCEPT_DEBIT_CREDIT = 3;
    private static OkHttpClient httpClient = new OkHttpClient();
    // 调用第三方接口检查银行卡是否合法
    public static void checkCard(Activity context, String cardNo, int acceptType, OnCheckCardResultListener listener) {
        String url = String.format("https://ccdcapi.alipay.com/validateAndCacheCardInfo.json?" +
                "cardNo=%s&cardBinCheck=true", cardNo);
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        android.os.Handler handler = new android.os.Handler();
//        ToastUtil.showLoading(context, true);
        httpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                ToastUtil.hideLoading();
                handler.post(() -> {
                    if (listener instanceof OnCheckCardBankResultListener) {
                        ((OnCheckCardBankResultListener)listener).onCheckResult(false, null, null);
                    } else {
                        listener.onCheckResult(false, null);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                ToastUtil.hideLoading();
                try {
                    String body = response.body().string();
                    JSONObject jsonObject = JSON.parseObject(body);
                    String cardType = jsonObject.getString("cardType");
                    String bank = jsonObject.getString("bank");
                    if ("DC".equals(cardType)) {
                        handler.post(() -> {
                            if (listener instanceof OnCheckCardBankResultListener) {
                                ((OnCheckCardBankResultListener)listener).onCheckResult(acceptType == CARD_CHECK_ACCEPT_DEBIT ||
                                        acceptType == CARD_CHECK_ACCEPT_DEBIT_CREDIT, "DC", bank);
                            } else {
                                listener.onCheckResult(acceptType == CARD_CHECK_ACCEPT_DEBIT ||
                                        acceptType == CARD_CHECK_ACCEPT_DEBIT_CREDIT, "DC");
                            }
                        });
                    } else if ("CC".equals(cardType)) {
                        handler.post(() -> {
                            if (listener instanceof OnCheckCardBankResultListener) {
                                ((OnCheckCardBankResultListener)listener).onCheckResult(acceptType == CARD_CHECK_ACCEPT_CREDIT ||
                                        acceptType == CARD_CHECK_ACCEPT_DEBIT_CREDIT, "CC", bank);
                            } else {
                                listener.onCheckResult(acceptType == CARD_CHECK_ACCEPT_CREDIT ||
                                        acceptType == CARD_CHECK_ACCEPT_DEBIT_CREDIT, "CC");
                            }
                        });
                    } else {
                        handler.post(() -> {
                            if (listener instanceof OnCheckCardBankResultListener) {
                                ((OnCheckCardBankResultListener)listener).onCheckResult(false, null, null);
                            } else {
                                listener.onCheckResult(false, null);
                            }
                        });
                    }
                } catch (Exception e) {
                    handler.post(() -> {
                        if (listener instanceof OnCheckCardBankResultListener) {
                            ((OnCheckCardBankResultListener)listener).onCheckResult(false, null, null);
                        } else {
                            listener.onCheckResult(false, null);
                        }
                    });
                }
            }
        });
    }

    // 检查信用卡有效期是否正确
    public static boolean checkPeriodOfValidity(String date) {
        if (!Pattern.matches("[0-9]{4}", date)) {
            return false;
        }
        int month = Integer.valueOf(date.substring(0,2));
        int year = Integer.valueOf(date.substring(2));
        if (month <= 0 || month > 12) {
            // ToastUtil.error("有效期月份格式不正确！");
            return false;
        }
        int currentYear = Calendar.getInstance().get(Calendar.YEAR) % 100;
        int currentMonth = Calendar.getInstance().get(Calendar.MONTH) + 1;
        if (year < currentYear) {
            // ToastUtil.error("有效期年份不能为过去年份！");
            return false;
        }

        if (year == currentYear) {
            if (currentMonth > month) {
                // ToastUtil.error("信用卡已过期！");
                return false;
            }
        }

        if (year > currentYear + 10) {
            return false;
        }

        if (year == currentYear + 10) {
            if (month > currentMonth) {
                // ToastUtil.error("信用卡已过期！");
                return false;
            }
        }
        return true;
    }
}
