package com.weiju.ccmall.module.blockchain.transferin;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.utils.BlockChainUtil;
import com.weiju.ccmall.module.product.ProductQrcodeShowActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.util.BitmapUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bingoogolapple.qrcode.core.BGAQRCodeUtil;
import cn.bingoogolapple.qrcode.zxing.QRCodeEncoder;

public class TransferInActivity extends BaseActivity {

    @BindView(R.id.iv_qr_code)
    ImageView ivQrCode;
    @BindView(R.id.tv_address)
    TextView tvAddress;

    String address;
    Bitmap bAddress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        address = getIntent().getStringExtra("address");
        setContentView(R.layout.activity_transfer_in);
        ButterKnife.bind(this);
        setTitle("转入");
        setLeftBlack();
        generateQrCode(address);
        tvAddress.setText(address);
    }

    @OnClick({R.id.tv_save_code, R.id.tv_copy_addr})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_save_code:
                BlockChainUtil.saveQrCode(this, bAddress);
                break;
            case R.id.tv_copy_addr:
                copyAddress();
                break;
        }
    }

    public static void start(Context context, String address) {
        Intent intent = new Intent(context, TransferInActivity.class);
        intent.putExtra("address", address);
        context.startActivity(intent);
    }

    private void generateQrCode(String address) {
        ToastUtil.showLoading(this);
        new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                Bitmap logo = BitmapFactory.decodeResource(getResources(), R.drawable.logo);
                return QRCodeEncoder.syncEncodeQRCode(
                        address,
                        BGAQRCodeUtil.dp2px(TransferInActivity.this, 130),
                        Color.BLACK,
                        logo);
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                ToastUtil.hideLoading();
                bAddress = bitmap;
                if (bitmap != null) {
                    ivQrCode.setImageBitmap(bitmap);
                } else {
                    Toast.makeText(TransferInActivity.this, "生成二维码失败", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    private void copyAddress() {
        ClipboardManager clipboardManager = (ClipboardManager)getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.setPrimaryClip(ClipData.newPlainText(null, address));
        ToastUtil.success("地址已复制!");
    }
}
