package com.weiju.ccmall.module.jkp.newjkp.widgets;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blankj.utilcode.utils.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.jkp.newjkp.adapter.ImageAdapter;
import com.weiju.ccmall.module.jkp.newjkp.entity.BannerEntity;
import com.weiju.ccmall.module.page.WebViewJavaActivity;

import java.util.List;

public class ImagesElement extends LinearLayout implements BaseQuickAdapter.OnItemClickListener {

    private final RecyclerView mListRv;
    private ImageAdapter mImageAdapter;
    private List<BannerEntity.Bean> mDataList;
    private Context mContext;

    public ImagesElement(Context context, List<BannerEntity.Bean> beans) {
        super(context);
        mContext = context;
        mDataList = beans;
        View view = inflate(getContext(), R.layout.el_product_layout, this);
        view.setBackgroundColor(Color.parseColor("#ececf2"));
        mListRv = view.findViewById(R.id.eleListRv);
        mListRv.setScrollContainer(false);
        mListRv.requestDisallowInterceptTouchEvent(true);
        mListRv.setNestedScrollingEnabled(false);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mListRv.getLayoutParams();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        gridLayoutManager.setAutoMeasureEnabled(true);
        gridLayoutManager.setSmoothScrollbarEnabled(false);
        mListRv.setLayoutManager(gridLayoutManager);

        int margin = SizeUtils.dp2px(7);
        layoutParams.setMargins(margin, 0, margin, 0);
        mListRv.setLayoutParams(layoutParams);

        mImageAdapter = new ImageAdapter();
        mImageAdapter.setNewData(mDataList);
        mListRv.setAdapter(mImageAdapter);
        mImageAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        BannerEntity.Bean bean = mDataList.get(position);
        Intent intent = new Intent(mContext, WebViewJavaActivity.class);
        intent.putExtra("url", bean.target);
        mContext.startActivity(intent);
    }
}
