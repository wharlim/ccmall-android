package com.weiju.ccmall.module.auth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.ToastUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.model.AuthModel;
import com.weiju.ccmall.module.auth.model.body.SubmitAuthBody;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.AutoAuthStatus;
import com.weiju.ccmall.shared.bean.UploadResponse;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.contracts.RequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.manager.UploadManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ActivityControl;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.zhihu.matisse.Matisse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import mtopsdk.common.util.StringUtils;

public class AuthIdentityActivity extends BaseActivity {

    @BindView(R.id.etName)
    EditText mEtName;
    @BindView(R.id.etIdNumber)
    EditText mEtIdNumber;
    @BindView(R.id.IvIdCard1)
    SimpleDraweeView mIvIdCard1;
    @BindView(R.id.IvIdCard2)
    SimpleDraweeView mIvIdCard2;
    @BindView(R.id.IvIdCard3)
    SimpleDraweeView mIvIdCard3;
    @BindView(R.id.tvSubmit)
    TextView mTvSubmit;

    private static final int REQUEST_CODE_CHOOSE_PHOTO_CARD1 = 1;
    private static final int REQUEST_CODE_CHOOSE_PHOTO_CARD2 = 2;
    private static final int REQUEST_CODE_CHOOSE_PHOTO_CARD3 = 3;
    @BindView(R.id.ivDeleteCard1)
    ImageView mIvDeleteCard1;
    @BindView(R.id.ivDeleteCard2)
    ImageView mIvDeleteCard2;
    @BindView(R.id.ivDeleteCard3)
    ImageView mIvDeleteCard3;

    private com.weiju.ccmall.module.auth.model.AuthModel mModel;
    private String mImgUrl1;
    private String mImgUrl2;
    private String mImgUrl3;
    private String mUrl = "auth/add";
    private boolean mIsEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_identity);

        ButterKnife.bind(this);
        setTitle("实名认证");
        setLeftBlack();
        initData();
    }

    private void initData() {
//        mIsEdit=true;
//        mIsEdit = getIntent().getBooleanExtra("isEdit", false);
//        if (mIsEdit) {


        mUrl = "auth/edit";
        IUserService service = ServiceManager.getInstance().createService(IUserService.class);

        APIManager.startRequest(service.getUserInfo(), new Observer<RequestResult<User>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(RequestResult<User> userRequestResult) {
                User user = userRequestResult.data;
                if (userRequestResult.isSuccess()) {

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            APIManager.startRequest(service.getStatus(), new Observer<RequestResult<AutoAuthStatus>>() {
                                @Override
                                public void onSubscribe(Disposable d) {

                                }

                                @Override
                                public void onNext(RequestResult<AutoAuthStatus> autoAuthStatusRequestResult) {
                                    AutoAuthStatus autoAuthStatus = autoAuthStatusRequestResult.data;
                                    if (1 == autoAuthStatus.autoAuthStatus) {
                                        initDatas();
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onComplete() {

                                }
                            });
                        }
                    }).start();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });

            /*APIManager.startRequest(service.getUserInfo(), new Observer<RequestResult<User>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(RequestResult<User> authModelRequestResult) {
                    *//*AuthModel authModel=User.data;
                    if(authModelRequestResult.isSuccess()){
                        if(authModel.memberAuthBean.getAutoAuthStatus()==2){
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    APIManager.startRequest(service.getStatus(), new Observer<RequestResult<AutoAuthStatus>>() {
                                        @Override
                                        public void onSubscribe(Disposable d) {

                                        }

                                        @Override
                                        public void onNext(RequestResult<AutoAuthStatus> autoAuthStatusRequestResult) {
                                            AutoAuthStatus autoAuthStatus=autoAuthStatusRequestResult.data;
                                            if(0==autoAuthStatus.autoAuthStatus){
                                                initData(authModel);
                                            }
                                        }

                                        @Override
                                        public void onError(Throwable e) {

                                        }

                                        @Override
                                        public void onComplete() {

                                        }
                                    });
                                }


                            }).start();


                        }*//*

                    }
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {

                }


            });
*/


            /*APIManager.startRequest(service.getStatus(), new Observer<RequestResult<AutoAuthStatus>>() {


                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(RequestResult<AutoAuthStatus> autoAuthStatusRequestResult) {
                    AutoAuthStatus autoAuthStatus=autoAuthStatusRequestResult.data;
                    if(autoAuthStatusRequestResult.isSuccess()){
                        if(1==autoAuthStatus.autoAuthStatus){
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    IUserService service = ServiceManager.getInstance().createService(IUserService.class);
                                    APIManager.startRequest(service.getAuth(), new BaseRequestListener<AuthModel>(AuthIdentityActivity.this) {
                                        @Override
                                        public void onSuccess(com.weiju.ccmall.module.auth.model.AuthModel model) {
                                            mModel = model;
                                            mEtName.setText(model.memberAuthBean.userName);
                                            mEtIdNumber.setText(model.memberAuthBean.getIdentityCard());
                                            mImgUrl1 = model.memberAuthBean.getIdcardFrontImg();
                                            mImgUrl2 = model.memberAuthBean.getIdcardBackImg();
                                            mImgUrl3 = model.memberAuthBean.getIdcardHeadImg();
                                            FrescoUtil.setImageSmall(mIvIdCard1, mImgUrl1);
                                            FrescoUtil.setImageSmall(mIvIdCard2, mImgUrl2);
                                            FrescoUtil.setImageSmall(mIvIdCard3, mImgUrl3);
                                            mIvDeleteCard1.setVisibility(View.VISIBLE);
                                            mIvDeleteCard2.setVisibility(View.VISIBLE);
                                            mIvDeleteCard3.setVisibility(View.VISIBLE);
                                        }

                                        @Override
                                        protected void onE(Throwable e) {
                                            super.onE(e);
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            super.onError(e);
                                        }
                                    });
                                }
                            }).start();
                        }
                    }


                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });*/

    }

    public void initDatas() {
        IUserService service = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(service.getAuth(), new RequestListener<AuthModel>() {
            @Override
            public void onStart() {

            }

            @Override
            public void onSuccess(AuthModel model) {
                mModel = model;
                mEtName.setText(model.memberAuthBean.userName);
//                mEtIdNumber.setText(model.memberAuthBean.getIdentityCard());
                mImgUrl1 = model.memberAuthBean.getIdcardFrontImg();
                mImgUrl2 = model.memberAuthBean.getIdcardBackImg();
                mImgUrl3 = model.memberAuthBean.getIdcardHeadImg();
                FrescoUtil.setImageSmall(mIvIdCard1, mImgUrl1);
                FrescoUtil.setImageSmall(mIvIdCard2, mImgUrl2);
                FrescoUtil.setImageSmall(mIvIdCard3, mImgUrl3);
                mIvDeleteCard1.setVisibility(View.VISIBLE);
                mIvDeleteCard2.setVisibility(View.VISIBLE);
                mIvDeleteCard3.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        }, this);


    }


    @OnClick({R.id.ivDeleteCard1, R.id.ivDeleteCard2, R.id.ivDeleteCard3, R.id.tvSubmit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivDeleteCard1:
                mIvIdCard1.setImageURI("");
                mImgUrl1 = "";
                mIvDeleteCard1.setVisibility(View.GONE);
                break;
            case R.id.ivDeleteCard2:
                mIvIdCard2.setImageURI("");
                mImgUrl2 = "";
                mIvDeleteCard2.setVisibility(View.GONE);
                break;
            case R.id.ivDeleteCard3:
                mIvIdCard3.setImageURI("");
                mImgUrl3 = "";
                mIvDeleteCard3.setVisibility(View.GONE);
                break;
            case R.id.tvSubmit:
                if (StringUtils.isEmpty(mEtName.getText().toString())) {
                    ToastUtils.showShortToast("姓名不能为空");
                    return;
                } else if (StringUtils.isEmpty(mEtIdNumber.getText().toString().trim())) {
                    ToastUtils.showShortToast("身份证不能为空");
                    return;
                } else if (StringUtils.isEmpty(mImgUrl1)) {
                    ToastUtils.showShortToast("证件正面照片不能为空");
                    return;
                } else if (StringUtils.isEmpty(mImgUrl2)) {
                    ToastUtils.showShortToast("证件背面照片不能为空");
                    return;
                } else if (StringUtils.isEmpty(mImgUrl3)) {
                    ToastUtils.showShortToast("手持证件照片不能为空");
                    return;
                }
                submitAuth();
                break;
            default:
        }
    }


    /**
     * 提交实名认证资料
     */
    private void submitAuth() {
        goSubmitSucceedActivity();
        /*IUserService service = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(
                service.submitAuth(mUrl, createBody().toMap()),
                new BaseRequestListener<Object>(this) {
                    @Override
                    public void onSuccess(Object result) {
                        super.onSuccess(result);
                        goSubmitSucceedActivity();
                    }
                }
        );*/


    }

    private com.weiju.ccmall.module.auth.model.body.SubmitAuthBody createBody() {
        return new SubmitAuthBody(
                mImgUrl1,
                mImgUrl2,
                mImgUrl3,
                mEtName.getText().toString().trim(),
                mEtIdNumber.getText().toString().trim(),
                ""
        );
    }

    private void goSubmitSucceedActivity() {
        /*User loginUser = SessionUtil.getInstance().getLoginUser();
        loginUser.autoAuthStatus = AppTypes.AUTH_STATUS.WAIT;
        loginUser.authStatusStr = "审核中";
        SessionUtil.getInstance().setLoginUser(loginUser);*/

        /*startActivity(new Intent(this, SubmitStatusActivity.class));
        EventBus.getDefault().postSticky(new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_SUBMIT_SUCCESS));
        EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_USER_CHANGE));*/

        Intent intent = new Intent(this, AuthCardActivity.class);
        ActivityControl.getInstance().add(this);
        intent.putExtra("isEdit", true);
        intent.putExtra("AuthBody", createBody());
        startActivity(intent);


//        finish();
    }


    private void selectImage(final int request) {
        UploadManager.selectImage(this, request, 1);
    }

    private void updateImage(final int request, final Uri uri) {
        UploadManager.uploadImage(this, uri, new BaseRequestListener<UploadResponse>(this) {
            @Override
            public void onSuccess(UploadResponse result) {
                switch (request) {
                    case REQUEST_CODE_CHOOSE_PHOTO_CARD1:
                        mIvIdCard1.setImageURI(uri.toString());
                        mImgUrl1 = result.url;
                        mIvDeleteCard1.setVisibility(View.VISIBLE);
                        break;
                    case REQUEST_CODE_CHOOSE_PHOTO_CARD2:
                        mIvIdCard2.setImageURI(uri.toString());
                        mImgUrl2 = result.url;
                        mIvDeleteCard2.setVisibility(View.VISIBLE);
                        break;
                    case REQUEST_CODE_CHOOSE_PHOTO_CARD3:
                        mIvIdCard3.setImageURI(uri.toString());
                        mImgUrl3 = result.url;
                        mIvDeleteCard3.setVisibility(View.VISIBLE);
                        break;
                    default:
                }
            }
        });
    }

    @OnClick({R.id.IvIdCard1, R.id.IvIdCard2, R.id.IvIdCard3})
    public void onClick2(View view) {
        switch (view.getId()) {
            case R.id.IvIdCard1:
                selectImage(REQUEST_CODE_CHOOSE_PHOTO_CARD1);
                break;
            case R.id.IvIdCard2:
                selectImage(REQUEST_CODE_CHOOSE_PHOTO_CARD2);
                break;
            case R.id.IvIdCard3:
                selectImage(REQUEST_CODE_CHOOSE_PHOTO_CARD3);
                break;
            default:
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_CHOOSE_PHOTO_CARD1:
                case REQUEST_CODE_CHOOSE_PHOTO_CARD2:
                case REQUEST_CODE_CHOOSE_PHOTO_CARD3:
                    List<Uri> uris = Matisse.obtainResult(data);
                    LogUtils.e("拿到图片" + uris.get(0).getPath());
                    updateImage(requestCode, uris.get(0));
                    break;
                default:
            }
        }
    }

}
