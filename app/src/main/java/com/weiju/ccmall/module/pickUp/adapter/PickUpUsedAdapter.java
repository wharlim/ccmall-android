package com.weiju.ccmall.module.pickUp.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.PickUp;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import static com.weiju.ccmall.module.pickUp.fragments.PickUpUsedListFragment.TYPE_USED;
import static com.weiju.ccmall.module.pickUp.fragments.PickUpUsedListFragment.TYPE_GIVING;
import static com.weiju.ccmall.module.pickUp.fragments.PickUpUsedListFragment.TYPE_RECEIVED;

/**
 * @author chenyanming
 * @time 2019/8/1 on 18:19
 * @desc ${TODD}
 */
public class PickUpUsedAdapter extends BaseQuickAdapter<PickUp, BaseViewHolder> {

    private int mType;

    public PickUpUsedAdapter(int type) {
        super(R.layout.item_pick_up_used);
        this.mType = type;
    }

    @Override
    protected void convert(BaseViewHolder helper, PickUp item) {
        FrescoUtil.setImage(helper.getView(R.id.ivAvatar), item.iconUrl);
        helper.setText(R.id.tvTitle, item.title);
        helper.setText(R.id.tvMoney, MoneyUtil.centToYuan¥StrNoZero(item.cost));
        helper.setText(R.id.tvGoodsCode, String.format("SN: %s", item.goodsCode));
        helper.setText(R.id.tvDate, String.format("过期时间: %s", item.expireDate));
        switch (mType) {
            case TYPE_USED:
                helper.setText(R.id.tvOrderId, String.format("使用订单号: %s", item.orderCode));
                helper.setText(R.id.tvTransferDate, "");
                break;
            case TYPE_GIVING:
                helper.setText(R.id.tvOrderId, String.format("好友: %s", item.inMemberPhone));
                helper.setText(R.id.tvTransferDate, item.transferDate);
                break;
            case TYPE_RECEIVED:
                helper.setText(R.id.tvOrderId, String.format("来自好友: %s", item.outMemberPhone));
                helper.setText(R.id.tvTransferDate, item.transferDate);
                break;
        }
    }
}
