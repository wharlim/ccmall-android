package com.weiju.ccmall.module.xysh.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.module.xysh.adapter.SelectSupportBankAdapter;
import com.weiju.ccmall.module.xysh.bean.SelectSupportBankItem;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseListActivity;

public class SelectSupportBankActivity extends BaseListActivity {

    private SelectSupportBankAdapter mAdapter = new SelectSupportBankAdapter();

    @Override
    public String getTitleStr() {
        return "选择开户行";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        SelectSupportBankItem item = mAdapter.getData().get(position);
        Intent data = new Intent();
        data.putExtra("bank", item);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        mAdapter.setNewData(BankUtils.getSupportBanks());
        mAdapter.loadMoreEnd(true);
        mRefreshLayout.setRefreshing(false);
    }

    public static void startForSelect(Activity context, int reqCode) {
        Intent activity = new Intent(context, SelectSupportBankActivity.class);
        context.startActivityForResult(activity, reqCode);
    }

    public static SelectSupportBankItem getSelectResult(Intent data) {
        return (SelectSupportBankItem) data.getSerializableExtra("bank");
    }
}
