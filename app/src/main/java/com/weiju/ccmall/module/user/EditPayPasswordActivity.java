package com.weiju.ccmall.module.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.utils.RegexUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.transferout.SimpleTextWatcher;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/6/23.
 */
public class EditPayPasswordActivity extends BaseActivity {

    @BindView(R.id.oldPasswordEt)
    protected EditText mOldPasswordEt;

    @BindView(R.id.newPasswordEt)
    protected EditText mNewPasswordEt;

    @BindView(R.id.surePasswordEt)
    protected EditText mSurePasswordEt;
    @BindView(R.id.tvPhone)
    TextView mTvPhone;
    @BindView(R.id.editBtn)
    TextView editBtn;
    private IUserService mUserService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_pay_password);
        ButterKnife.bind(this);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        initView();
    }

    private void initView() {
        setTitle("修改支付密码");
        setLeftBlack();
        User loginUser = SessionUtil.getInstance().getLoginUser();
        mTvPhone.setText(ConvertUtil.maskPhone(loginUser.phone));
        initEditText(mOldPasswordEt);
        initEditText(mNewPasswordEt);
        initEditText(mSurePasswordEt);
    }

    private void initEditText(EditText editText) {
        editText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                String oldPass = mOldPasswordEt.getText().toString();
                String newPass = mNewPasswordEt.getText().toString();
                String surePass = mSurePasswordEt.getText().toString();
                if (oldPass.length() > 0 && newPass.length() > 0 && surePass.length() > 0) {
                    editBtn.setEnabled(true);
                } else {
                    editBtn.setEnabled(false);
                }
            }
        });
    }

    @OnClick(R.id.editBtn)
    protected void onEdit() {
        String oldPass = mOldPasswordEt.getText().toString();
        String newPass = mNewPasswordEt.getText().toString();
        String surePass = mSurePasswordEt.getText().toString();
        if (TextUtils.isEmpty(oldPass)) {
            ToastUtil.error("请输入当前支付密码");
            mOldPasswordEt.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(newPass)) {
            ToastUtil.error("请输入新密码");
            mNewPasswordEt.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(surePass)) {
            ToastUtil.error("请确认新密码");
            mSurePasswordEt.requestFocus();
            return;
        }
        if (newPass.length() < 6) {
            ToastUtil.error("支付密码必须为6位数字");
            mNewPasswordEt.requestFocus();
            return;
        }
        if (isSimplePwd(newPass)) {
            ToastUtil.error("当前密码过于简单，请重新设置");
            mNewPasswordEt.requestFocus();
            return;
        }
        if (!newPass.equals(surePass)) {
            ToastUtil.error("两次密码输入不一致");
            mSurePasswordEt.requestFocus();
            return;
        }
        String encodeOldPass = Base64.encodeToString(oldPass.getBytes(), Base64.NO_WRAP);
        String encodeNewPass = Base64.encodeToString(newPass.getBytes(), Base64.NO_WRAP);
        APIManager.startRequest(mUserService.editPayPassword(encodeOldPass, encodeNewPass), new BaseRequestListener<Object>(this) {
            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("修改成功");
                finish();
            }
        }, this);

    }

    private boolean isSimplePwd(String pwd) {
        if (RegexUtils.isMatch("(?:(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){5}|(?:9(?=8)|8(?=7)|7(?=6)|6(?=5)|5(?=4)|4(?=3)|3(?=2)|2(?=1)|1(?=0)){5})\\d"
                , pwd)) {
            return true;
        } else return RegexUtils.isMatch("([\\d])\\1{2,}", pwd);
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, EditPayPasswordActivity.class);
        context.startActivity(intent);
    }
}
