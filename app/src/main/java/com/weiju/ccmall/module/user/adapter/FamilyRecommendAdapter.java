package com.weiju.ccmall.module.user.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.viewholders.FamilyRecommendHolder;
import com.weiju.ccmall.shared.bean.Family;

import java.util.List;

public class FamilyRecommendAdapter extends BaseQuickAdapter<Family.DatasEntity, FamilyRecommendHolder> {
    public FamilyRecommendAdapter(@Nullable List<Family.DatasEntity> data) {
        super(R.layout.item_family_recommend, data);
    }

    @Override
    protected void convert(FamilyRecommendHolder helper, Family.DatasEntity item) {
        helper.bindView(item);
    }
}
