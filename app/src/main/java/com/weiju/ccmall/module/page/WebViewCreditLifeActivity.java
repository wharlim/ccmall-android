package com.weiju.ccmall.module.page;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebBackForwardList;
import android.webkit.WebHistoryItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.ToastUtils;
import com.just.agentweb.AgentWeb;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.face.LivenessHelper;
import com.weiju.ccmall.module.pay.PayMsg;
import com.weiju.ccmall.module.pay.PayOrderActivity;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.module.xysh.activity.RealNameAuthActivity;
import com.weiju.ccmall.module.xysh.fragment.CreditFragment;
import com.weiju.ccmall.newRetail.activity.CommunityActivity;
import com.weiju.ccmall.shared.basic.AgentFActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.util.CSUtils;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewCreditLifeActivity extends BaseActivity {

    private static final String TAG = "WebViewCL";

    @BindView(R.id.layoutWebview)
    FrameLayout mLayoutWebview;
    @BindView(R.id.baseRelativeLayout)
    RelativeLayout baseRelativeLayout;
    private AgentWeb mWeb;
    private String mUrl;
    private TextView close;
    private float v = 0.0f;
    private RelativeLayout.LayoutParams lp;
    private boolean hideToolbar;
    private int mGrade;
    private LivenessHelper helper;
    private int fromPage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            WebView.enableSlowWholeDocumentDraw();
        }
        setContentView(R.layout.activity_webview);
        ButterKnife.bind(this);
        mUrl = getIntent().getStringExtra("url");
        hideToolbar = getIntent().getBooleanExtra("hideToolbar", false);
        mGrade = getIntent().getIntExtra("grade", 0);
        fromPage = getIntent().getIntExtra("fromPage", 0);
        String usrTel = getIntent().getStringExtra("usrTel");
        if (mUrl.contains("create-chain.net") && SessionUtil.getInstance().isLogin()) {
//            Uri uri = Uri.parse(mUrl);
//            mUrl = uri.buildUpon()
//                    .appendQueryParameter("token", SessionUtil.getInstance().getOAuthToken())
//                    .appendQueryParameter("grade", String.valueOf(mGrade))
//                    .build()
//                    .toString();
            mUrl = mUrl + "?token=" + SessionUtil.getInstance().getOAuthToken() + "&phone=" + usrTel + "&grade=" + mGrade;
        }
        Log.d(TAG, "mUrl: " + mUrl);
        Log.d(TAG, "hideToolbar: " + hideToolbar);
        Log.d(TAG, "grade: " + mGrade);
        Log.d(TAG, "usrTel: " + usrTel);
        if (hideToolbar) {
            hideHeader();
        } else {
            setLeftBlack();
        }
        if (StringUtils.isEmpty(mUrl)) {
            return;
        }
        openWeb(mUrl);
    }

    @SuppressLint("ResourceType")
    private void hideHeader() {
        close = new TextView(this);
        close.setVisibility(View.GONE);
        close.setTextColor(getResources().getColor(R.color.white));
        close.setText("关闭");
        close.setGravity(Gravity.CENTER);
        v = getResources().getDisplayMetrics().widthPixels / 1080f;
        int size = (int) (v * 40);
        close.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) size);
        lp = new RelativeLayout.LayoutParams(size * 3, (int) (v * 125));
        lp.setMargins((int) (v * 30), 0, 0, 0);
        close.setLayoutParams(lp);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        baseRelativeLayout.addView(close, baseRelativeLayout.getChildCount());
        getHeaderLayout().setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        WebView webView = mWeb.getWebCreator().get();
        Uri uri = Uri.parse(webView.getUrl());

        if (hideToolbar && webView.canGoBack()) {
            String url = webView.getUrl();
            int i = url.indexOf("//");
            if (i != -1) {
                url = url.substring(i + 2, url.length());
            } /*else {
                int i1 = url.indexOf("https://");
                if (i1 != -1) {
                    url = url.substring(i1, url.length());
                }
            }*/
            if (url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/CashBill") || url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/") || url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/RepayPlanCreditList") || url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/My")) {
                finish();
                return;
            }
            if (uri.getHost().equals(BuildConfig.XYSH_HOST)) {
                switch (uri.getFragment()) {
                    case "/":
                    case "/CashBill":
                    case "/RepayPlanCreditList":
                    case "/My":
                        super.onBackPressed();
                        break;
                    default:
                        webView.goBack();
                }
            } else {
                WebBackForwardList list = webView.copyBackForwardList();
                if (list != null && list.getSize() > 0) {
                    WebHistoryItem itemAtIndex = list.getItemAtIndex(list.getCurrentIndex() - 1);
                    if (itemAtIndex != null) {
                        if (uri.getHost().equals(BuildConfig.XYSH_HOST)) {
                            webView.loadUrl(getIntent().getStringExtra("url"));
                        } else {
                            webView.goBack();
                        }

                    } else {
                        webView.loadUrl(getIntent().getStringExtra("url"));
                    }
                } else {
                    webView.loadUrl(getIntent().getStringExtra("url"));
                }
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWeb != null) {
            mWeb.destroy();
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void openWeb(String url) {
        if (mWeb != null) {
            mWeb.getLoader().loadUrl(url);
            return;
        }
        //传入Activity
//传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams ,第一个参数和第二个参数应该对应。
// 使用默认进度条
// 使用默认进度条颜色
//设置 Web 页面的 title 回调
//
        mWeb = AgentWeb.with(this)//传入Activity
                .setAgentWebParent(mLayoutWebview, new FrameLayout.LayoutParams(-1, -1))//传入AgentWeb 的父控件 ，如果父控件为 RelativeLayout ， 那么第二参数需要传入 RelativeLayout.LayoutParams ,第一个参数和第二个参数应该对应。
                .useDefaultIndicator()// 使用默认进度条
                .setIndicatorColor(R.color.red) // 使用默认进度条颜色
                .setReceivedTitleCallback((view, title) -> {
                    if (!hideToolbar) {
                        setTitle(title);
                    }
                }) //设置 Web 页面的 title 回调
                .setWebViewClient(webViewClient)
                .createAgentWeb()//
                .ready()
                .go(url);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            helper.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
        }
    }

    private WebViewClient webViewClient = new WebViewClient() {
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (url.equals("http://credit.ccmallv2.create-chain.net/customizYTH/#/NameAndIdCard")) {
                AgentFActivity.start2(WebViewCreditLifeActivity.this, CreditFragment.class, null);
                view.goBack();
            } else if (hideToolbar) {
                Uri uri = Uri.parse(url);
                if (uri.getHost().equals(BuildConfig.XYSH_HOST) && uri.getFragment() != null) {
                    switch (uri.getFragment()) {
                        case "/":
                        case "/CashBill":
                        case "/RepayPlanCreditList":
                        case "/My":
                            setVisibility(close, View.VISIBLE);
                            break;
                        default:
                            setVisibility(close, View.GONE);
                    }
                }
            } else {
                setVisibility(close, View.GONE);
            }
            super.onPageFinished(view, url);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Uri uri = Uri.parse(url);
            Log.d(TAG, "shouldOverrideUrlLoading uri -> " + uri);
            if ("create-chain".equals(uri.getScheme())) {
                String host = uri.getHost();
                if (host != null) {
                    switch (host) {
                        case "jump":
                            String targe = uri.getQueryParameter("targe");
                            switch (targe) {
                                case "goods": {
                                    String skuid = uri.getQueryParameter("skuid");
                                    Intent intent = new Intent(WebViewCreditLifeActivity.this, NewProductDetailActivity.class);
                                    intent.putExtra(Key.SKU_ID, skuid);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                                break;
                                case "check_liveness":
                                    if (helper == null) {
                                        helper = new LivenessHelper();
                                    }
                                    helper.start(WebViewCreditLifeActivity.this);
                                    break;
                                case "authentication":
//                                    Intent intent2 = new Intent(WebViewCreditLifeActivity.this, AuthPhoneActivity.class);
//                                    SharedPreferences sharedPreferences = WebViewCreditLifeActivity.this.getSharedPreferences("authType", 0);
//                                    sharedPreferences.edit().putString("authType", "UserCenter").commit();
//                                    intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
//                                    startActivity(intent2);
                                    RealNameAuthActivity.start(WebViewCreditLifeActivity.this);
                                    break;
                                case "TransitionMatrix":
                                    ToastUtil.success("系统正在处理中，请稍后查看。");
                                    finish();
                                    break;
                                case "product_detail":
                                    String skuId = uri.getQueryParameter("sku_id");
                                    if (skuId == null || skuId.isEmpty()) {
                                        break;
                                    } else {
                                        Intent intent = new Intent(getBaseContext(), NewProductDetailActivity.class);
                                        intent.putExtra(Key.SKU_ID, skuId);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }

                                    break;
                                case "customPage":
                                    String pageId = uri.getQueryParameter("pageId");
                                    if (pageId == null || pageId.isEmpty()) {
                                        break;
                                    }
                                    Intent intent = new Intent(getBaseContext(), CustomPageActivity.class);
                                    intent.putExtra("pageId", pageId);
                                    startActivity(intent);
                                    break;
                                case "community":
                                    startActivity(new Intent(getBaseContext(), CommunityActivity.class));
                                    break;
                                case "upgrade":
                                    String orderCode = uri.getQueryParameter("orderCode");
                                    LogUtils.d("orderCode:" + orderCode);
                                    if (TextUtils.isEmpty(orderCode)) {
                                        break;
                                    } else {
                                        Intent intentGrade = new Intent(getBaseContext(), PayOrderActivity.class);
                                        intentGrade.putExtra("orderCode", orderCode);
                                        intentGrade.putExtra("goodsType", PayOrderActivity.GOODSTYPE_CREDIT_LIFE_UPGRADE);
                                        startActivity(intentGrade);
                                    }
                                    break;
                                default:
                                    CSUtils.start(getBaseContext(), targe, "102a49c45bb54dbbb2394becdaffbce2");
                            }
                            break;

                        default:
                            break;
                    }
                }
                return true;
            } else {
                if (!hideToolbar && uri.getHost().equals(BuildConfig.XYSH_HOST)) {
                    close.setVisibility(View.GONE);
                }
                return super.shouldOverrideUrlLoading(view, url);
            }
        }

    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(PayMsg msgStatus) {
        switch (msgStatus.getAction()) {
            case PayMsg.ACTION_ALIPAY_SUCCEED:
            case PayMsg.ACTION_WEBPAY_SUCCEED:
            case PayMsg.ACTION_WXPAY_SUCCEED:
            case PayMsg.ACTION_BALANCE_SUCCEED:
                EventBus.getDefault().post(new EventMessage(Event.creditLifeUpgradeSuccess, fromPage));
                ToastUtil.success("系统正在处理中，请稍后查看。");
                finish();
                break;
            case PayMsg.ACTION_WXPAY_FAIL:
            case PayMsg.ACTION_ALIPAY_FAIL:
                ToastUtils.showShortToast(msgStatus.message);
                break;
            case PayMsg.ACTION_WEBPAY_FAIL:
                finish();
                break;
            default:
        }
    }
    private void setVisibility(View view, int visibility) {
        if (view == null || view.getVisibility() == visibility) {
            return;
        }
        view.setVisibility(visibility);
    }

    /**
     *
     * @param context
     * @param url
     * @param hideToolbar
     * @param grade 会员等级
     * @param fromPage 1：信用生活首页，2：信用生活个人中心
     */
    public static void start(Context context, String url, boolean hideToolbar, int grade, int fromPage, String usrTel) {
        Intent intent = new Intent(context, WebViewCreditLifeActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("hideToolbar", hideToolbar);
        intent.putExtra("grade", grade);
        intent.putExtra("fromPage", fromPage);
        intent.putExtra("usrTel", usrTel);
        context.startActivity(intent);
    }
}
