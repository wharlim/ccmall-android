package com.weiju.ccmall.module.user;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderListFragment;
import com.weiju.ccmall.module.user.adapter.MyContributionSortAdapter;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.ContributionSort;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/6/24 on 14:26
 * @desc ${TODD}
 */
public class MyContributionSortFragment extends BaseListFragment {

    @BindView(R.id.tvWeekSort)
    protected TextView mTvWeekSort;
    @BindView(R.id.tvMonthSort)
    protected TextView mTvMonthSort;
    @BindView(R.id.tvTotalSort)
    protected TextView mTvTotalSort;
    @BindView(R.id.layoutDate)
    protected LinearLayout mLayoutDate;

    private String mDateType = "week";
    private String mLastDateType = "week";

    private MyContributionSortAdapter mAdapter = new MyContributionSortAdapter();
    private Page mPage;
    private IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
    private String mUrl;

    public static OrderListFragment newInstance(String page) {
        Bundle args = new Bundle();
        args.putSerializable("page", page);
        OrderListFragment fragment = new OrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mPage = (Page) getArguments().get("page");
    }

    @Override
    public void initView() {
        super.initView();
        mTvWeekSort.setSelected(true);
        mTvMonthSort.setSelected(false);
        mTvTotalSort.setSelected(false);

        mUrl = "stat/getContributionSortList";

        switch (mPage.id) {
            case "tse":
            case "tsl":
                mLayoutDate.setVisibility(View.GONE);
                mDateType = null;
                mUrl = "stat/getTseOrTslSortList";
                break;
            default:
        }
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {


    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mUserService.getContributionSortList(mUrl, mPage.id, mDateType),
                new BaseRequestListener<List<ContributionSort>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(List<ContributionSort> result) {
                        super.onSuccess(result);
                        mAdapter.setNewData(result);
                    }
                },getContext());
    }

    @Override
    public boolean isLoadMore() {
        return false;
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.fragment_my_contribution_sort_list;
    }

    @OnClick({R.id.tvWeekSort, R.id.tvMonthSort, R.id.tvTotalSort})
    public void dateSelect(View view) {
        if ("tse".equals(mPage.id) || "tsl".equals(mPage.id)) {
            return;
        }
        switch (view.getId()) {
            case R.id.tvWeekSort:
                mDateType = "week";
                mTvWeekSort.setSelected(true);
                mTvMonthSort.setSelected(false);
                mTvTotalSort.setSelected(false);
                break;
            case R.id.tvMonthSort:
                mDateType = "month";
                mTvWeekSort.setSelected(false);
                mTvMonthSort.setSelected(true);
                mTvTotalSort.setSelected(false);
                break;
            case R.id.tvTotalSort:
                mDateType = "total";
                mTvWeekSort.setSelected(false);
                mTvMonthSort.setSelected(false);
                mTvTotalSort.setSelected(true);
                break;
            default:
        }
        if (!mDateType.equals(mLastDateType)) {
            mLastDateType = mDateType;
            getData(true);
        }


    }
}
