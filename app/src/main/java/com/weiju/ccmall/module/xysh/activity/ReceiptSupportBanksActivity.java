package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.fragment.ReceiptSupportBanksFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReceiptSupportBanksActivity extends BaseActivity {

    @BindView(R.id.tvPayCard)
    TextView tvPayCard;
    @BindView(R.id.tvReceiptCard)
    TextView tvReceiptCard;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    private String channelId;

    public static void start(Context context, String channelId) {
        Intent intent = new Intent(context, ReceiptSupportBanksActivity.class);
        intent.putExtra("channelId", channelId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_support_banks);
        ButterKnife.bind(this);
        setTitle("查看支持银行");
        setLeftBlack();
        channelId = getIntent().getStringExtra("channelId");
        initViewPage();
    }

    private void initViewPage() {
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                switch (i) {
                    case 0:
                        return ReceiptSupportBanksFragment.newInstance(ReceiptSupportBanksFragment.TYPE_PAY, channelId);
                    case 1:
                        return ReceiptSupportBanksFragment.newInstance(ReceiptSupportBanksFragment.TYPE_RECEIPT, channelId);
                    default:
                        throw new IllegalArgumentException("i超出范围:" +i);
                }
            }

            @Override
            public int getCount() {
                return 2;
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                setTabSelect(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        tvPayCard.setSelected(true);
    }

    private void setTabSelect(int i) {
        switch (i) {
            case 0:
                tvPayCard.setSelected(true);
                tvReceiptCard.setSelected(false);
                break;
            case 1:
                tvPayCard.setSelected(false);
                tvReceiptCard.setSelected(true);
                break;
            default:
                throw new IllegalArgumentException("i超出范围:" +i);
        }
    }

    private int getTabIndex(View indexView) {
        if (indexView == tvPayCard) {
            return 0;
        } else if (indexView == tvReceiptCard) {
            return 1;
        }
        return 0;
    }

    @OnClick({R.id.tvPayCard, R.id.tvReceiptCard})
    public void onViewClicked(View view) {
        int index = getTabIndex(view);
        viewPager.setCurrentItem(index);
        setTabSelect(index);
//        switch (view.getId()) {
//            case R.id.tvPayCard:
//                tvPayCard.setSelected(true);
//                tvReceiptCard.setSelected(false);
//                viewPager.setCurrentItem(0);
//                break;
//            case R.id.tvReceiptCard:
//                tvPayCard.setSelected(false);
//                tvReceiptCard.setSelected(true);
//                viewPager.setCurrentItem(1);
//                break;
//        }
    }
}
