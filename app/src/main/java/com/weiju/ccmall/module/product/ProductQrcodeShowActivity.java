package com.weiju.ccmall.module.product;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.utils.AppUtils;
import com.blankj.utilcode.utils.FileUtils;
import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.Product;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.component.dialog.ShareDialog;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bingoogolapple.qrcode.core.BGAQRCodeUtil;
import cn.bingoogolapple.qrcode.zxing.QRCodeEncoder;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

public class ProductQrcodeShowActivity extends BaseActivity {

    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.tvDesc)
    TextView mTvDesc;
    @BindView(R.id.ivImg)
    SimpleDraweeView mIvImg;
    @BindView(R.id.ivQrCode)
    SimpleDraweeView mIvQrCode;
    @BindView(R.id.layoutQrCode)
    FrameLayout mLayoutQrCode;
    @BindView(R.id.tvTagPrice)
    TextView mTvTagPrice;
    @BindView(R.id.tvProducePrice)
    TextView mTvProducePrice;
    @BindView(R.id.tvMarketPrice)
    TextView mTvMarketPrice;
    @BindView(R.id.layoutContent)
    LinearLayout mLayoutContent;
    @BindView(R.id.tvSave)
    TextView mTvSave;
    @BindView(R.id.tvShare)
    TextView mTvShare;

    private AsyncTask<Void, Void, Bitmap> mExecute;
    private String mId;
    private SkuInfo mSkuInfo;
    private Product mSpuInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_qrcode_show);
        ButterKnife.bind(this);

        getIntentData();
        initView();
        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ToastUtil.hideLoading();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ToastUtil.hideLoading();
        mExecute.cancel(true);
    }

    private void initData() {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        String link = BuildConfig.WECHAT_URL + "p/" + mSkuInfo.skuId + "/" + loginUser.invitationCode;
        createEnglishQRCodeWithLogo(link);
        mTvName.setText(mSkuInfo.name);
        mTvDesc.setText(mSkuInfo.desc);
        if (StringUtils.isEmpty(mSkuInfo.desc)) {
            mTvDesc.setVisibility(View.GONE);
        }
        FrescoUtil.setImage(mIvImg, mSkuInfo.thumb);
        mTvMarketPrice.setText("原价：" + MoneyUtil.centToYuan¥Str(mSkuInfo.marketPrice));
        TextViewUtil.addThroughLine(mTvMarketPrice);

        String tagPrice = "会员价：";
        long price = mSkuInfo.retailPrice;
        if (mSpuInfo.isInstant()) {
            // 秒杀
            tagPrice = "秒杀价：";
            price = mSkuInfo.retailPrice;
        } else if (mSpuInfo.extType == 2) {
            // 团购
            tagPrice = "团购价：";
            price = mSpuInfo.getGroupEntity(mSkuInfo.skuId).groupPrice;
        }
        mTvTagPrice.setText(tagPrice);
        mTvProducePrice.setText(MoneyUtil.centToYuan¥Str(price));
    }

    private void initView() {
        setTitle("分享");
        setLeftBlack();
    }

    private void getIntentData() {
        Intent intent = getIntent();
        mSkuInfo = (SkuInfo) intent.getSerializableExtra("sku");
        mSpuInfo = (Product) intent.getSerializableExtra("spu");
    }

    @OnClick(R.id.tvSave)
    public void onMTvSaveClicked() {
        ToastUtil.showLoading(this);
        Bitmap viewBitmap = getViewBitmap(mLayoutContent);
        File savedImageFile = getSaveFile();
        saveBitmapFile(viewBitmap, savedImageFile, true);
        ToastUtil.hideLoading();
    }

    @OnClick(R.id.tvShare)
    public void onMTvShareClicked() {
        ToastUtil.showLoading(this);
        Bitmap viewBitmap = getViewBitmap(mLayoutContent);
        final File savedImageFile = getSaveFile();
        saveBitmapFile(viewBitmap, savedImageFile, false);
        Luban.with(this)
                .load(savedImageFile)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        LogUtils.e("压缩图片开始");
                    }

                    @Override
                    public void onSuccess(File file) {
                        savedImageFile.delete();
                        ToastUtil.hideLoading();
                        showShareDialog(file);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtil.error("压缩图片出错:" + e.getMessage());
                        ToastUtil.hideLoading();
                    }
                }).launch();
    }

    private void showShareDialog(final File file) {
        new ShareDialog(this, file, new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                ToastUtil.showLoading(ProductQrcodeShowActivity.this);
            }

            @Override
            public void onResult(SHARE_MEDIA share_media) {
                ToastUtil.success("分享成功");
                ToastUtil.hideLoading();
                file.delete();
            }

            @Override
            public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                ToastUtil.error("分享出错:" + throwable.getMessage());
                ToastUtil.hideLoading();
                file.delete();
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media) {
                ToastUtil.hideLoading();
                file.delete();
            }
        }).show();
    }

    public static File getSaveFile() {
        String dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath()
                + "/" + AppUtils.getAppName(MyApplication.getContext());
        File file = new File(dirPath, System.currentTimeMillis() + ".jpg");
        FileUtils.createOrExistsDir(dirPath);
        return file;
    }

    public static void saveBitmapFile(Bitmap bitmap, final File saveFile, boolean isShowMsg) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(saveFile));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
            if (isShowMsg) {
                ToastUtil.success("保存成功");
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                intent.setData(Uri.fromFile(saveFile));
                MyApplication.getContext().sendBroadcast(intent);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 查看图片
     *
     * @param saveFile
     */
    private void viewImage(File saveFile) {
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setDataAndType(Uri.fromFile(saveFile), "image/*");
        startActivity(intent);
    }

    private Bitmap getViewBitmap(View v) {
        v.clearFocus();
        v.setPressed(false);
        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);
        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            return null;
        }
        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);
        return bitmap;
    }

    private void createEnglishQRCodeWithLogo(final String linkUrl) {
        ToastUtil.showLoading(this);
        mExecute = new AsyncTask<Void, Void, Bitmap>() {
            @Override
            protected Bitmap doInBackground(Void... params) {
                return QRCodeEncoder.syncEncodeQRCode(linkUrl, BGAQRCodeUtil.dp2px(ProductQrcodeShowActivity.this, 100));
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                ToastUtil.hideLoading();
                if (bitmap != null) {
                    mIvQrCode.setImageBitmap(bitmap);
                } else {
                    Toast.makeText(ProductQrcodeShowActivity.this, "生成二维码失败", Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }
}


