package com.weiju.ccmall.module.live.adapter;

import android.text.Html;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

import java.util.List;

public class ChooseLiveGoodsAdapter extends BaseQuickAdapter<SkuInfo, BaseViewHolder> {

    public ChooseLiveGoodsAdapter(List<SkuInfo> data) {
        super(R.layout.item_choose_live_goods, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SkuInfo item) {
        FrescoUtil.setImageSmall(helper.getView(R.id.ivItemThumb), item.thumb);
        helper.getView(R.id.ivItemSelector).setSelected(item.isSelected);

        helper.setText(R.id.tvName, item.name);
        helper.setText(R.id.tvItemPrice, MoneyUtil.centToYuan¥StrNoZero(item.retailPrice));
        helper.setText(R.id.tvCCM, Html.fromHtml("预计可返<big><font color ='#EE196B'>" + item.countRateExc +
                "%</font></big>算率，预计赚<big><font color ='#EE196B'>" + item.getAnchorDistributionProfit() + "</font></big>元"));
    }

}
