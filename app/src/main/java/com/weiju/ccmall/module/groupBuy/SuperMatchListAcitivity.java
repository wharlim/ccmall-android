package com.weiju.ccmall.module.groupBuy;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.groupBuy.adapter.SuperMatchAdapter;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SuperMatch;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;

/**
 * @author chenyanming
 * @time 2019/8/15 on 10:43
 * @desc ${TODD}
 */
public class SuperMatchListAcitivity extends BaseListActivity {

    private SuperMatchAdapter mAdapter = new SuperMatchAdapter();
    private IUserService mService = ServiceManager.getInstance().createService(IUserService.class);

    private String mGroupCode;

    @Override
    public void getIntentData() {
        super.getIntentData();
        mGroupCode = getIntent().getStringExtra("groupCode");
    }

    @Override
    public String getTitleStr() {
        return "拼团成功的列表";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    public void initView() {
        super.initView();
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.background));
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mService.getSuperMatchList(mCurrentPage, 15, mGroupCode),
                new BaseRequestListener<PaginationEntity<SuperMatch, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<SuperMatch, Object> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                },this);
    }
}
