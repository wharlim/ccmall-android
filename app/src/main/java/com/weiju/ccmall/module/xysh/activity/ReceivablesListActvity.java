package com.weiju.ccmall.module.xysh.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.adapter.PopupSelectAdapter;
import com.weiju.ccmall.module.xysh.adapter.ReceivablesAdapter;
import com.weiju.ccmall.module.xysh.bean.ReceivablesBill;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.PrestoreType;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.component.PrestoreDatePopupView;
import com.weiju.ccmall.shared.component.StatusPopupView;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PopupWindowManage;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * @author chenyanming
 * @time 2019/12/16 on 15:50
 * @desc
 */
public class ReceivablesListActvity extends BaseListActivity {


    @BindView(R.id.viewYinying)
    View mViewYinying;
    @BindView(R.id.tvDate)
    TextView mTvDate;
    @BindView(R.id.tvStatus)
    TextView mTvStatus;
    @BindView(R.id.lineSelect)
    View mLineSelect;

    private ReceivablesAdapter mAdapter = new ReceivablesAdapter();

    //日期选择窗弹窗
    private PopupWindowManage mDateWindowManage;
    private PrestoreDatePopupView mPrestoreDatePopupView;

    //状态选择窗弹窗
    private PopupWindowManage mStatusWindowManage;
    private StatusPopupView mStatusPopupView;
    private View mStatusListPopupView;
    private PopupSelectAdapter mReceivableStatusAdapter;

    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    private String month;
    private String year;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        month = String.valueOf(calendar.get(Calendar.MONTH) + 1);
        year = String.valueOf(calendar.get(Calendar.YEAR));
        super.onCreate(savedInstanceState);
    }

    @Override
    public void initView() {
        super.initView();
        creatDatePopup();
//        creatStatusListPopup();
        mTvStatus.setVisibility(View.GONE);
    }

    private void creatStatusListPopup() {
        mStatusWindowManage = PopupWindowManage.getInstance(ReceivablesListActvity.this);
        mStatusWindowManage.setYinYing(mViewYinying);

        mStatusListPopupView = View.inflate(ReceivablesListActvity.this, R.layout.view_popup_select, null);
        RecyclerView recyclerView = mStatusListPopupView.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ReceivablesListActvity.this);
        recyclerView.setLayoutManager(linearLayoutManager);
        List<PrestoreType> list = new ArrayList<>();
        list.add(new PrestoreType("全部状态", "0"));
        list.add(new PrestoreType("执行中", "1"));
        list.add(new PrestoreType("执行成功", "2"));
        list.add(new PrestoreType("执行失败", "3"));
        mReceivableStatusAdapter = new PopupSelectAdapter();
        mReceivableStatusAdapter.setNewData(list);
        recyclerView.setAdapter(mReceivableStatusAdapter);
        mReceivableStatusAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                mReceivableStatusAdapter.setSelectPosition(position);
                mStatusWindowManage.dismiss();
                PrestoreType item = mReceivableStatusAdapter.getItem(position);
                if (null != item) {
                    mTvStatus.setText(item.name);
                }
            }
        });


    }

    private void creatStatusPopup() {
        mStatusWindowManage = PopupWindowManage.getInstance(ReceivablesListActvity.this);
        mStatusWindowManage.setYinYing(mViewYinying);

        mStatusPopupView = new StatusPopupView(ReceivablesListActvity.this);
        mStatusPopupView.setSelectListener(new StatusPopupView.SelectListener() {
            @Override
            public void onSelectType(String type, String text) {
                mStatusWindowManage.dismiss();
                mTvStatus.setText(text);
            }
        });
    }

    private void creatDatePopup() {
        mTvDate.setText(year + "年" + month + "月");
        mDateWindowManage = PopupWindowManage.getInstance(ReceivablesListActvity.this);
        mDateWindowManage.setYinYing(mViewYinying);

        mPrestoreDatePopupView = new PrestoreDatePopupView(this);
        mPrestoreDatePopupView.setSelectListener(new PrestoreDatePopupView.SelectListener() {
            @Override
            public void onSelectDate(String yearAndMonth, String text) {
                mDateWindowManage.dismiss();
                mTvDate.setText(text);
//                Log.d("Seven", "yearAndMonth -> " + yearAndMonth + " text -> " + text);
                String[] splits = yearAndMonth.split("-");
                year = splits[0];
                month = splits[1];
                getData(true);
            }
        });
    }


    @Override
    public String getTitleStr() {
        return "收款账单";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        Intent intent = new Intent(ReceivablesListActvity.this, ReceivablesDetailActivity.class);
        intent.putExtra("ReceivablesBill", mAdapter.getData().get(position));
        startActivity(intent);
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
//        List<String> list = new ArrayList<>();
//        for (int i = 0; i < 20; i++) {
//            list.add("到帐卡-交通银行（2397）");
//        }
//        mAdapter.setNewData(list);
//        mRefreshLayout.setRefreshing(false);
        APIManager.startRequest(service.getTxnJnlVos(
                month, year
        ), new Observer<XYSHCommonResult<List<ReceivablesBill>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<List<ReceivablesBill>> ret) {
                mAdapter.setNewData(ret.data);
                mAdapter.loadMoreEnd(true);
                mRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_receivable_list;
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @OnClick(R.id.tvDate)
    protected void selectDate() {
        mDateWindowManage.showWindow(mLineSelect, mPrestoreDatePopupView);
    }

    @OnClick(R.id.tvStatus)
    protected void selectStatus() {
        mStatusWindowManage.showWindow(mLineSelect, mStatusListPopupView);
    }
}
