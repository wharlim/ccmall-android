package com.weiju.ccmall.module.transfer;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.AuthPhoneActivity;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.transfer.fragments.PickUpSelectListFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.DecimalEditText;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.enums.AccountType;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.ValidateUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.transfer
 * @since 2017-08-03
 */
public class StepSecondActivity extends BaseActivity {

    @BindView(R.id.avatarIv)
    protected SimpleDraweeView mAvatarIv;
    @BindView(R.id.nameTv)
    protected TextView mNameTv;
    @BindView(R.id.phoneTv)
    protected TextView mPhoneTv;
    @BindView(R.id.moneyEt)
    protected DecimalEditText mMoneyEt;
    @BindView(R.id.remarkEt)
    protected EditText mRemarkEt;
    @BindView(R.id.tvTips)
    TextView mTvTips;
    @BindView(R.id.confirmBtn)
    TextView mConfirmBtn;

    @BindView(R.id.ll_inputType_num)
    LinearLayout mInputTypeNum;
    @BindView(R.id.ll_inputType_select)
    LinearLayout mInputTypeSelect;
    @BindView(R.id.fl_select_container)
    FrameLayout flSelectContainer;

    PickUpSelectListFragment selectListFragment;

    private int mType;
    private User mPayee;
    private AccountType mAccountType;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_step_second);
        ButterKnife.bind(this);
        getIntentData();
        initView();
        EventBus.getDefault().register(this);
    }

    private void getIntentData() {
        Intent intent = getIntent();
        mType = intent.getIntExtra(Config.INTENT_KEY_TYPE_NAME, AppTypes.TRANSFER.MONEY);
        mPayee = (User) intent.getExtras().getSerializable("payee");
        mAccountType = (AccountType) getIntent().getSerializableExtra("AccountType");
    }

    private void initView() {
        showHeader();
        setLeftBlack();
        mInputTypeNum.setVisibility(View.GONE);
        mInputTypeSelect.setVisibility(View.GONE);
        switch (mType) {
            case AppTypes.TRANSFER.MONEY:
                setTitle("转帐到对方帐户");
                mInputTypeNum.setVisibility(View.VISIBLE);
                break;
            case AppTypes.TRANSFER.SCORE:
                setTitle("转到对方账户");
                mTvTips.setText("转增数额");
                mConfirmBtn.setText("转增");
                mMoneyEt.setInputType(InputType.TYPE_CLASS_NUMBER);
                mMoneyEt.setHint("0");
                mInputTypeNum.setVisibility(View.VISIBLE);
                break;
            case AppTypes.TRANSFER.PICKUP_COUPON:
                setTitle("赠送到对方账户");
                mConfirmBtn.setText("下一步");
                mConfirmBtn.setBackgroundResource(R.drawable.btn_pickup_common);
                mInputTypeSelect.setVisibility(View.VISIBLE);
                initSelectList();
                break;
        }
        FrescoUtil.setImageSmall(mAvatarIv, mPayee.avatar);
        mNameTv.setText(String.format("%s（%s）", mPayee.nickname, mPayee.userName));
        mPhoneTv.setText(mPayee.phone);
    }

    private void initSelectList() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fl_select_container, PickUpSelectListFragment.newInstance());
        transaction.commit();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof PickUpSelectListFragment) {
            this.selectListFragment = (PickUpSelectListFragment) fragment;
        }
    }

    private void transferMoney() {
        String moneyStr = mMoneyEt.getText().toString();
        if (moneyStr.isEmpty()) {
            ToastUtil.error("请填写转帐金额");
            return;
        }
        if (!ValidateUtil.isMoney(moneyStr)) {
            ToastUtil.error("转帐金额格式不正确");
            return;
        }
        double money = Double.parseDouble(moneyStr);
        if (1 > money || money > 1000000) {
            ToastUtil.error("金额只能为 1 到 1000000 之间");
            return;
        }
        String remark = mRemarkEt.getText().toString();
        Intent intent = new Intent(this, StepThirdActivity.class);
        intent.putExtra(Config.INTENT_KEY_TYPE_NAME, mType);
        intent.putExtra("payee", mPayee);
        intent.putExtra("money", money);
        intent.putExtra("remark", remark);
        intent.putExtra("AccountType", mAccountType);
        startActivity(intent);
    }

    private void transferScore() {
        String moneyStr = mMoneyEt.getText().toString();
        if (StringUtils.isEmpty(moneyStr)) {
            ToastUtil.error("请填写转账数额");
        } else {
            long money = Long.parseLong(moneyStr);
            if (money == 0) {
                ToastUtil.error("转赠金额不能为0");
            } else {
                Intent intent = new Intent(this, AuthPhoneActivity.class);
                intent.putExtra(Config.INTENT_KEY_TYPE_NAME, AppTypes.TRANSFER.SCORE);
                intent.putExtra("phone", mPayee.phone);
                intent.putExtra("transferScore", money);
                intent.putExtra("remark", mRemarkEt.getText().toString());
                String userName = StringUtil.maskName(mPayee.userName);
                intent.putExtra("statusTips", String.format("%s(%s)已收到你的转赠", mPayee.nickname, userName));
                startActivity(intent);
            }
        }
    }

    private void transferPickUpCoupon() {
        String goodsCodes = selectListFragment.getSelectedGoodsCodes();
        if (goodsCodes == null) {
            ToastUtil.error("请选择赠送的提货券");
        } else {
            Intent intent = new Intent(this, StepThirdActivity.class);
            intent.putExtra("payee", mPayee);
            intent.putExtra(Config.INTENT_KEY_TYPE_NAME, mType);
            intent.putExtra("goodsCodes", goodsCodes);
            intent.putExtra("AccountType", mAccountType);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.confirmBtn)
    protected void onSubmit() {
        switch (mType) {
            case AppTypes.TRANSFER.MONEY:
                transferMoney();
                break;
            case AppTypes.TRANSFER.SCORE:
                transferScore();
                break;
            case AppTypes.TRANSFER.PICKUP_COUPON:
                transferPickUpCoupon();
                break;
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void transferHandler(EventMessage message) {
        if (message.getEvent().equals(Event.transferSuccess)) {
            finish();
        }
    }
}
