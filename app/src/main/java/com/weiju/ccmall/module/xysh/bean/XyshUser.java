package com.weiju.ccmall.module.xysh.bean;

import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class XyshUser {


    /**
     * usrLoginpwd : null
     * city : 广州市
     * refNm : null
     * headUrl : null
     * tmSmp : 1570956753000
     * uCreateTime : 1551089186000
     * idNm : 王夏亮
     * rlAuthFlag : 1
     * idNo : 440982199002174335
     * usrPaypwd : null
     * usrSts : 1
     * lastLoginIp : null
     * payAmount : null
     * usrNo : 5a85b44c6fe023eda3d844a03af2f50d
     * handPw : null
     * usrTel : 13711236885
     * prov : 广东省
     * channelId : ccpay
     * email : null
     * usrLvl : 0
     * area : 东山区
     * payBackAmount : null
     * salt : null
     * headImage : https://static.create-chain.net/ccmall/6e/d8/fb/a4f9b672687a483d88fea83ab4bc3a53.jpg
     * usrLvlFlag : 0
     * sessionId : null
     * inRefNm2 : null
     * inRefNm1 : null
     * handOpenFlag : null
     * lastLoginTm : null
     * errPwdCnt : 0
     * lastDevice : null
     */

    @SerializedName("usrLoginpwd")
    public Object usrLoginpwd;
    @SerializedName("city")
    public String city;
    @SerializedName("refNm")
    public Object refNm;
    @SerializedName("headUrl")
    public Object headUrl;
    @SerializedName("tmSmp")
    public long tmSmp;
    @SerializedName("uCreateTime")
    public long uCreateTime;
    @SerializedName("idNm")
    public String idNm;
    @SerializedName("rlAuthFlag")
    public String rlAuthFlag;
    @SerializedName("idNo")
    public String idNo;
    @SerializedName("usrPaypwd")
    public Object usrPaypwd;
    @SerializedName("usrSts")
    public String usrSts;
    @SerializedName("lastLoginIp")
    public Object lastLoginIp;
    @SerializedName("payAmount")
    public Object payAmount;
    @SerializedName("usrNo")
    public String usrNo;
    @SerializedName("handPw")
    public Object handPw;
    @SerializedName("usrTel")
    public String usrTel;
    @SerializedName("prov")
    public String prov;
    @SerializedName("channelId")
    public String channelId;
    @SerializedName("email")
    public Object email;
    @SerializedName("usrLvl")
    public int usrLvl;  //用户等级
    @SerializedName("area")
    public String area;
    @SerializedName("payBackAmount")
    public Object payBackAmount;
    @SerializedName("salt")
    public Object salt;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("usrLvlFlag")
    public int usrLvlFlag;
    @SerializedName("sessionId")
    public Object sessionId;
    @SerializedName("inRefNm2")
    public Object inRefNm2;
    @SerializedName("inRefNm1")
    public Object inRefNm1;
    @SerializedName("handOpenFlag")
    public Object handOpenFlag;
    @SerializedName("lastLoginTm")
    public Object lastLoginTm;
    @SerializedName("errPwdCnt")
    public int errPwdCnt;
    @SerializedName("lastDevice")
    public Object lastDevice;
    /**
     * remark : 更低费率,更多会员特权,立即升级!
     * nickName : null
     */

    @SerializedName("remark")
    public String remark;
    @SerializedName("usrLvlName")
    public String usrLvlName;
    @SerializedName("nickName")
    public Object nickName;

    public boolean hasRealNameAuth() {
        return "1".equals(rlAuthFlag);
    }

    public static XyshUser fromJson(@Nullable String json) {
        if (json == null || json.isEmpty()) {
            return null;
        }
        return new Gson().fromJson(json, XyshUser.class);
    }

}
