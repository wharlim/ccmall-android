package com.weiju.ccmall.module.user;

import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.groupBuy.JoinGroupActivity;
import com.weiju.ccmall.module.user.adapter.SuperGroupAdapter;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SuperGroup;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.decoration.SpacesItemDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * @author chenyanming
 * @time 2019/8/14 on 11:21
 * @desc 我的超级拼团
 */
public class MySuperGroupListActivity extends BaseListActivity {
    private SuperGroupAdapter mAdapter = new SuperGroupAdapter();
    private IUserService mService = ServiceManager.getInstance().createService(IUserService.class);

    @Override
    public String getTitleStr() {
        return "超级拼团";
    }

    @Override
    public void initView() {
        super.initView();
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.white));
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(ConvertUtil.dip2px(10)));
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        SuperGroup item = mAdapter.getItem(position);
        if (null == item) {
            return;
        }
        if (item.activityBean.status == 1 && item.productBean.status == 1 &&
                TimeUtils.getNowTimeDate().getTime() < TimeUtils.string2Millis(item.activityBean.endDate) && item.productBean.stock > 0) {
            Intent intent = new Intent(MySuperGroupListActivity.this, JoinGroupActivity.class);
            intent.putExtra(Config.INTENT_KEY_TYPE_NAME, JoinGroupActivity.TYPE_HOST);
            intent.putExtra(Config.INTENT_KEY_ID, item.groupInfo.groupCode);
            intent.putExtra("isSuperGroup", true);
            startActivity(intent);
        } else {
            EventUtil.viewProductDetail(MySuperGroupListActivity.this, item.productBean.skuId, false);
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mService.getSuperGroupList(mCurrentPage, 15),
                new BaseRequestListener<PaginationEntity<SuperGroup, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<SuperGroup, Object> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                },this);
    }

    @Override
    public View getEmptyView() {
        View inflate = View.inflate(this, R.layout.cmp_no_data, null);
        TextView noDataTip = inflate.findViewById(R.id.noDataLabel);
        noDataTip.setText("还没有超级拼团订单，赶紧去拼团吧");
        TextView goMain = inflate.findViewById(R.id.tvReRefresh);
        goMain.setVisibility(View.VISIBLE);
        goMain.setText("去逛逛");
        goMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                EventBus.getDefault().post(new EventMessage(Event.viewHome));
            }
        });
        return inflate;
    }
}
