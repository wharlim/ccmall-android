package com.weiju.ccmall.module.product;

import android.os.Bundle;

import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.basic.BaseTopTabActivity;
import com.weiju.ccmall.shared.bean.CommentCount;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

/**
 * 评价中心
 */
public class MyCommentListActivity extends BaseTopTabActivity {

    private String[] mTitle = {"待评价", "已评价"};

    private String classifacationType = "";

    @Override
    public void initView() {
        setTitle("评价中心");
        setLeftBlack();
    }

    @Override
    public String[] getTitles() {
        return mTitle;
    }

    @Override
    public List<BaseFragment> getFragments() {
        ArrayList<BaseFragment> baseFragments = new ArrayList<>();
        if (!"".equals(classifacationType)) {
            baseFragments.add(MyCommentListFragment.newInstance(0, classifacationType));
            baseFragments.add(MyCommentListFragment.newInstance(1, classifacationType));
        } else {
            baseFragments.add(MyCommentListFragment.newInstance(0));
            baseFragments.add(MyCommentListFragment.newInstance(1));
        }

        return baseFragments;
    }

    @Override
    public void initData() {

        IOrderService service = ServiceManager.getInstance().createService(IOrderService.class);
        super.initData();
        classifacationType = getIntent().getStringExtra(Const.CLASSIFICATION_TYPE);
        if (Const.NEWRETAIL_MODE.equals(classifacationType)) {
            APIManager.startRequest(
                    service.getOrderCommentCount("onnOrder"),
                    new BaseRequestListener<CommentCount>() {
                        @Override
                        public void onSuccess(CommentCount result) {
                            super.onSuccess(result);
                            mTitle[0] = "待评价(" + result.waitCount + ")";
                            mTitle[1] = "已评价(" + result.laterCount + ")";
                            if (mNavigatorAdapter == null) {
                                MyCommentListActivity.super.initView();
                            } else {
                                mNavigatorAdapter.notifyDataSetChanged();
                            }
                        }
                    },this
            );
        } else {
            APIManager.startRequest(
                    service.getOrderCommentCount(),
                    new BaseRequestListener<CommentCount>() {
                        @Override
                        public void onSuccess(CommentCount result) {
                            super.onSuccess(result);
                            mTitle[0] = "待评价(" + result.waitCount + ")";
                            mTitle[1] = "已评价(" + result.laterCount + ")";
                            if (mNavigatorAdapter == null) {
                                MyCommentListActivity.super.initView();
                            } else {
                                mNavigatorAdapter.notifyDataSetChanged();
                            }
                        }
                    },this
            );
        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    int test = 1;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
        if (message.getEvent().equals(Event.commentFinish)) {
            initData();
        }
    }
}
