package com.weiju.ccmall.module.xysh.activity.repayment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.shop.views.Utils;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.IntelligenceRepaymentActivity;
import com.weiju.ccmall.module.xysh.bean.PlanDetail;
import com.weiju.ccmall.module.xysh.bean.TodayPayStatus;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class RepaymentOneDayFragment extends BaseListFragment {

    @BindView(R.id.tvTotalCount)
    public TextView tvTotalCount;
    @BindView(R.id.ivComplete)
    public ImageView ivComplete;
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    boolean isPreview;

    private RepaymentOneDayAdapter mAdapter;
    private List<PlanDetail.PayBackDetailBean.PlanItem> data = new ArrayList<>();
    private String date;
    private String planNo;
    private int payType;
    @Override
    public void getIntentData() {
        super.getIntentData();
        isPreview = getArguments().getBoolean("isPreview");
        mAdapter = new RepaymentOneDayAdapter(isPreview);
    }

    @Override
    public void initView() {
        super.initView();
        mRefreshLayout.setEnabled(false);
        mRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
                outRect.set(0, 0, 0, Utils.dpToPx(15));
            }
        });
        mRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int h = mRecyclerView.getHeight();
                if (h >= 0) {
                    mRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    ivComplete.getLayoutParams().height = h;
                    ivComplete.requestLayout();
                }
            }
        });
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (view.getId() == R.id.tvCopy) {
            ClipboardManager clipboardManager = (ClipboardManager)getContext().getSystemService(Context.CLIPBOARD_SERVICE);
            clipboardManager.setPrimaryClip(ClipData.newPlainText(null, data.get(position).tranId));
            ToastUtil.success("复制成功!");
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        mAdapter.setNewData(data);
        mRefreshLayout.setRefreshing(false);
        mAdapter.loadMoreEnd(true);
        int count = 0;
        for (PlanDetail.PayBackDetailBean.PlanItem item :
                data) {
            if (!(item instanceof PlanDetail.PayBackDetailBean.PlanItemSummary)) {
                count ++;
            }
        }
        tvTotalCount.setText(String.format("本日交易%d笔", count));
        getTodayPayStatus();
    }

    private void getTodayPayStatus() {
        // 预览，全自动，不需要获取这个状态
        if (isPreview || payType == IntelligenceRepaymentActivity.REPAY_TYPE_FULL_AUTOMATION) {
            return;
        }
        APIManager.startRequest(service.getTodayPayStatus(planNo, date), new Observer<XYSHCommonResult<TodayPayStatus>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<TodayPayStatus> ret) {
                if (isAdded() && ret.success) {
                    boolean isComplete = ret.data.todayStatus == 0;
                    ivComplete.setVisibility(isComplete ? View.VISIBLE : View.GONE);
                    PlanDetail.PayBackDetailBean.PlanItemSummary summaryItem =
                            (PlanDetail.PayBackDetailBean.PlanItemSummary) data.get(data.size()-1);
                    summaryItem.payMoney = ret.data.todayAmount;
                    mAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public RepaymentOneDayFragment setData(List<PlanDetail.PayBackDetailBean.PlanItem> data,
                                           String date,
                                           String planNo,
                                           int payType) {
        this.data = data;
        this.date = date;
        this.planNo = planNo;
        this.payType = payType;
        return this;
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.fragment_repayment_one_day;
    }

    public static RepaymentOneDayFragment newInstance(boolean isPreview) {
        RepaymentOneDayFragment fragment = new RepaymentOneDayFragment();
        Bundle args = new Bundle();
        args.putBoolean("isPreview", isPreview);
        fragment.setArguments(args);
        return fragment;
    }
}
