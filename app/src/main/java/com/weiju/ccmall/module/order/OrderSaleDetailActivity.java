package com.weiju.ccmall.module.order;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.adapter.SaleViewHolder;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.OrderProfit;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderSaleDetailActivity extends BaseActivity {

    @BindView(R.id.payState)
    TextView payState;
    @BindView(R.id.orderPrice)
    TextView orderPrice;
    @BindView(R.id.type)
    TextView type;
    @BindView(R.id.intoTime)
    TextView intoTime;
    @BindView(R.id.orderCustomer)
    TextView orderCustomer;
    @BindView(R.id.orderTime)
    TextView orderTime;
    @BindView(R.id.orderNo)
    TextView orderNo;
    @BindView(R.id.copyNoBtn)
    TextView copyNoBtn;
    @BindView(R.id.orderType)
    TextView orderType;
    @BindView(R.id.orderState)
    TextView orderState;
    @BindView(R.id.questionBtn)
    ImageView questionBtn;
    @BindView(R.id.contactBtn)
    TextView contactBtn;
    @BindView(R.id.isGiveUpRefund)
    TextView isGiveUpRefund;

    private IOrderService orderService;
    private OrderProfit order;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_sale_detail);
        ButterKnife.bind(this);

        getHeaderLayout().setTitle("销售订单详情");
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_black);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        orderService = ServiceManager.getInstance().createService(IOrderService.class);

        BaseRequestListener<OrderProfit> listener = new BaseRequestListener<OrderProfit>() {
            @Override
            public void onSuccess(OrderProfit result) {
                super.onSuccess(result);
                if (!isDestroyed()) {
                    initData(result);
                }
            }
        };
        APIManager.startRequest(orderService.getOrderProfit(
                getIntent().getStringExtra("orderCode")), listener, this);
    }

    public static void start(Context context, String orderCode) {
        Intent intent = new Intent(context, OrderSaleDetailActivity.class);
        intent.putExtra("orderCode", orderCode);
        context.startActivity(intent);
    }

    @OnClick({R.id.copyNoBtn, R.id.questionBtn, R.id.contactBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.copyNoBtn:
                if (order != null && copy(order.orderMain.orderCode)) {
                    ToastUtil.success("已复制到剪切板!");
                }
                break;
            case R.id.questionBtn:
                showTip();
                break;
            case R.id.contactBtn:
                contactCustomer();
                break;
        }
    }

    private void initData(OrderProfit order) {
        this.order = order;
        payState.setText(SaleViewHolder.profitStatusStr(order.memberProfitBean.status));
        orderPrice.setText(String.format(Locale.getDefault(), "+%.2f", order.memberProfitBean.profitMoney/(100.0)));
        type.setText("销售提成");
        intoTime.setText(TextUtils.isEmpty(order.memberProfitBean.unfreezeDate) ? "-" : order.memberProfitBean.unfreezeDate);
        orderCustomer.setText(order.userName + " " + order.orderMain.phone);
        orderTime.setText(order.orderMain.createDate);
        orderNo.setText(order.orderMain.orderCode);
        orderType.setText("嗨购订单");
        orderState.setText(order.orderMain.orderStatusStr);
        contactBtn.setVisibility(TextUtils.isEmpty(order.orderMain.memberId) ? View.INVISIBLE : View.VISIBLE);
        isGiveUpRefund.setText(
                order.orderMain.giveUpRefund == 0 ? "客户未操作过「快速升级」" : "客户已确认「快速升级」"
        );
    }

    private boolean copy(String copyStr) {
        try {
            //获取剪贴板管理器
            ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            // 创建普通字符型ClipData
            ClipData mClipData = ClipData.newPlainText("Label", copyStr);
            // 将ClipData内容放到系统剪贴板里。
            cm.setPrimaryClip(mClipData);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void contactCustomer() {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null && order != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, OrderSaleDetailActivity.this);
                    ChatInfo chatInfo = new ChatInfo();
                    chatInfo.setType(TIMConversationType.C2C);
                    chatInfo.setId(order.orderMain.memberId);
                    chatInfo.setChatName(order.orderMain.nickName);
                    Intent intent = new Intent(OrderSaleDetailActivity.this, ChatActivity.class);
                    intent.putExtra("chatInfo", chatInfo);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });
        }
    }

    private void showTip() {
        final WJDialog dialog = new WJDialog(this);
        dialog.show();
        dialog.setTitle("提示");
        dialog.setContentText("您需升级成C1用户，并且客户在未收货时提前「快速升级」或已「确认收货」，提成才能解冻");
        dialog.setConfirmText("我明白了");
        dialog.hideCancelBtn();
    }
}
