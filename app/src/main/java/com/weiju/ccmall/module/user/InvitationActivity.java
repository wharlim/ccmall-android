package com.weiju.ccmall.module.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.model.UpMemberModel;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Chan on 2017/6/10.
 */

public class InvitationActivity extends BaseActivity {

    @BindView(R.id.avatarIv)
    protected SimpleDraweeView mAvatarIv;

    @BindView(R.id.nicknameTv)
    protected TextView mNicknameTv;

    @BindView(R.id.phoneTv)
    protected TextView mPhoneTv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    private void initData() {
        IUserService service = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(service.getUpMember(), new BaseRequestListener<UpMemberModel>(this) {
            @Override
            public void onSuccess(UpMemberModel result) {
                mAvatarIv.setImageURI(result.headImage);
                mNicknameTv.setText(result.nickName);
                StringBuilder phoneSB = new StringBuilder(result.phone);
                String phone = phoneSB.replace(3, 7, "****").toString();
                mPhoneTv.setText(phone);
            }
        }, this);
    }

    private void initView() {
        setTitle("我的邀请人");
        setLeftBlack();
    }

}
