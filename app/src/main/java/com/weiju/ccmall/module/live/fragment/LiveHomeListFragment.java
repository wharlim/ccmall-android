package com.weiju.ccmall.module.live.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.listener.OnItemClickListener;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.module.live.activity.LiveSearchActivity;
import com.weiju.ccmall.module.live.adapter.LiveHomeAdapter;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.NullLoadMoreView;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.CarouselUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LiveHomeListFragment extends BaseListFragment {

    @BindView(R.id.layoutSearch)
    protected View layoutSearch;

    public static final int LIVE_LIST_TYPE_RECOMMEND = 0;
    public static final int LIVE_LIST_TYPE_FOCUS = 1;

    private ArrayList<View> mHeaderViews;
    private ViewHolder mHeaderViewHolder;
    private LiveHomeAdapter mAdapter = new LiveHomeAdapter();
    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);
    private int mCurrentPage = 1;
    private int mLiveListType = LIVE_LIST_TYPE_RECOMMEND;

    public static LiveHomeListFragment newInstance(int liveListType) {
        LiveHomeListFragment fragment = new LiveHomeListFragment();
        Bundle args = new Bundle();
        args.putInt("liveListType", liveListType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mLiveListType = getArguments().getInt("liveListType", LIVE_LIST_TYPE_RECOMMEND);
    }

    @Override
    public void initView() {
        super.initView();
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position >= 1 ? 1 : 2;
            }
        });
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setBackgroundColor(Color.parseColor("#EFEFEF"));
        mAdapter.setLoadMoreView(new NullLoadMoreView());
        layoutSearch.setVisibility(mLiveListType == LIVE_LIST_TYPE_RECOMMEND ? View.VISIBLE: View.GONE);
        EventBus.getDefault().register(this);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View getEmptyView() {
        View inflate = View.inflate(getContext(), R.layout.live_no_data, null);
        return inflate;
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        LiveRoom item = mAdapter.getItem(position);
        if (null == item) {
            return;
        }
        LiveManager.checkRoomPassword(getContext(), item);
    }



    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.fragment_live_home_list;
    }

    @Override
    public void getData(boolean isRefresh) {
        if (isRefresh) {
            if (mLiveListType == LIVE_LIST_TYPE_RECOMMEND) {
                APIManager.startRequest(mService.getLiveRecommend(), new BaseRequestListener<List<LiveRoom>>() {
                    @Override
                    public void onSuccess(List<LiveRoom> result) {
                        mHeaderViewHolder.setData(result);
                    }
                }, getContext());
            } else if (mLiveListType == LIVE_LIST_TYPE_FOCUS) {
                mHeaderViewHolder.convenientBanner.setVisibility(View.GONE);
            }
            mCurrentPage = 1;
        }
        APIManager.startRequest(mService.getLiveListWithTab(null, null, mCurrentPage, Constants.PAGE_SIZE, mLiveListType+1), new BaseRequestListener<PaginationEntity<LiveRoom, Object>>(mRefreshLayout) {
            @Override
            public void onSuccess(PaginationEntity<LiveRoom, Object> result) {
                if (mCurrentPage == 1) {
                    mAdapter.setNewData(result.list);
                } else {
                    mAdapter.addData(result.list);
                }
                if (result.page >= result.totalPage) {
                    mAdapter.loadMoreEnd();
                } else {
                    mAdapter.loadMoreComplete();
                }
                mCurrentPage++;
            }
        }, getContext());

    }

    @Override
    public ArrayList<View> getHeaderViews() {
        if (mHeaderViews == null) {
            mHeaderViews = new ArrayList<>();
            mHeaderViews.add(createHeaderView());
        }
        return mHeaderViews;
    }

    private View createHeaderView() {
        View inflate = View.inflate(getContext(), R.layout.view_header_live, null);
        mHeaderViewHolder = new ViewHolder(inflate, getContext());
        return inflate;
    }

    @OnClick(R.id.layoutSearch)
    protected void search() {
        startActivity(new Intent(getContext(), LiveSearchActivity.class));
    }

    @Override
    public void onResume() {
        super.onResume();
//        getData(true);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && mLiveListType == LIVE_LIST_TYPE_FOCUS) {
            UiUtils.checkUserLogin(getContext());
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case logout:
            case loginSuccess:
                getData(true);
                break;
            default:
        }
    }

    static class ViewHolder {

        @BindView(R.id.imagesLayout)
        ConvenientBanner convenientBanner;
        private Context mContext;

        ViewHolder(View view, Context context) {
            ButterKnife.bind(this, view);
            mContext = context;
        }


        private void setData(List<LiveRoom> list) {
            if (null == list || list.size() == 0) {
                convenientBanner.setVisibility(View.GONE);
                return;
            }

            convenientBanner.setVisibility(View.VISIBLE);
            CarouselUtil.initCarousel(convenientBanner, list, R.drawable.icon_page_indicator_focused);
            convenientBanner.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    LiveRoom liveRecommend = list.get(position);
                    liveRecommend.status = liveRecommend.liveStatus;
                    LiveManager.checkRoomPassword(mContext, liveRecommend);
                }
            });
        }
    }
}
