package com.weiju.ccmall.module.blockchain.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class TemplateDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "template.db";
    private static final int VERSION = 1;

    private static final String CREATE_TABLE_TEMPLATE = "create table if not exists template(" +
            "_id integer primary key," +
            "templateId text unique," +
            "templateName text," +
            "remark text," +
            "fileUrl text," +
            "status int," +
            "createDate text," +
            "updateDate text," +
            "deleteFlag int," +
            "localState int default 0 )";

    public TemplateDatabaseHelper(@Nullable Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TEMPLATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
