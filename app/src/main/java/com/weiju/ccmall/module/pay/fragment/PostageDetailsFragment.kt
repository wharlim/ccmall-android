package com.weiju.ccmall.module.pay.fragment

import android.os.Bundle
import android.view.View
import com.weiju.ccmall.R
import com.weiju.ccmall.shared.basic.AgentFragment
import kotlinx.android.synthetic.main.fragment_postage_details.*

class PostageDetailsFragment : AgentFragment(){

    override fun layoutId(): Int {
        return R.layout.fragment_postage_details
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.title = "商品总价详情"
        super.onViewCreated(view, savedInstanceState)
//        val total = arguments?.getLong("totalPrice") ?: return
//        val mFreight = "${arguments?.getLong("mFreight")}".toBigDecimal()
//        val shippingPrice = "${arguments?.getLong("shippingPrice")}".toBigDecimal()
//        val retailP = ("$total".toBigDecimal().subtract(mFreight).subtract(shippingPrice)).divide("100".toBigDecimal()).toString()
//        details.text = "商品总价${"$total".toBigDecimal().divide("100".toBigDecimal())}元 = " +
//                "商品价格${retailP}元 + 物流费用（${mFreight.divide("100".toBigDecimal())}元 + ${shippingPrice.divide("100".toBigDecimal())}元）"


        val mFreight = arguments?.getLong("mFreight") ?: return
        details.text = "商家承担运费成本${"$mFreight".toBigDecimal().divide("100".toBigDecimal())}元" +
                "（说明：包邮商品运费由卖家全额承担；不包邮商品运费由买家全额或部分承担，商家承担其余运费）"


    }
}
