package com.weiju.ccmall.module.auth.model.body;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.util.ConvertUtil;

import org.json.JSONException;

import java.util.Map;

/**
 * @author chenyanming
 * @time 2019/2/14 on 16:37
 * @desc
 */
public class WithdrawDo {

    //            @Field("version") String version,
//            @Field("token") String version,
//            @Field("amount") long token,
//            @Field("remark") String remark

    @SerializedName("version")
    public String version;
    @SerializedName("token")
    public String token;
    @SerializedName("amount")
    public long amount;
    @SerializedName("remark")
    public String remark;


    public WithdrawDo(String version, String token, long amount, String remark) {
        this.version = version;
        this.token = token;
        this.amount = amount;
        this.remark = remark;
    }

    public Map<String, String> toMap() {
        Map<String, String> map = null;
        try {
            map = ConvertUtil.objToMap(this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return map;
    }
}
