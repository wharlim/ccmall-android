package com.weiju.ccmall.module.user;

import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.module.product.adapter.ProductAdapter;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/11/5 on 15:29
 * @desc ${TODD}
 */
public class MyGiftProductListActivity extends BaseListActivity {

    private ProductAdapter mAdapter = new ProductAdapter();
    private IProductService mFootService = ServiceManager.getInstance().createService(IProductService.class);

    @Override
    public void initData() {
        super.initData();
        EventBus.getDefault().register(this);
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_my_gift_product;
    }

    @Override
    public String getTitleStr() {
        return "我的礼包";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        SkuInfo item = mAdapter.getItem(position);
        if (null != item) {
            Intent intent = new Intent(MyGiftProductListActivity.this, NewProductDetailActivity.class);
            intent.putExtra("type", NewProductDetailActivity.TYPE_MY_GIFT);
            intent.putExtra("status", item.status);
            intent.putExtra("memberSkuId", item.memberSkuId);
            intent.putExtra(Key.SKU_ID, item.skuId);
            startActivity(intent);
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @OnClick(R.id.tvAddVIP)
    protected void addVIP() {
        startActivity(new Intent(MyGiftProductListActivity.this, AddGiftProductListActivity.class));
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mFootService.getMyPackages(mCurrentPage, Constants.PAGE_SIZE),
                new BaseRequestListener<PaginationEntity<SkuInfo, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && result.list.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, MyGiftProductListActivity.this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(EventMessage message) {
        switch (message.getEvent()) {
            case joinProduct:
            case delectProduct:
                mCurrentPage = 1;
                getData(true);
                break;
            default:
        }
    }
}
