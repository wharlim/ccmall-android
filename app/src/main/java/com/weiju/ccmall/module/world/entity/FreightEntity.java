package com.weiju.ccmall.module.world.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/5/7.
 */
public class FreightEntity {
    @SerializedName("postFee")
    public String postFee;
    @SerializedName("taxFee")
    public long taxFee;
    @SerializedName("tax")
    public double tax;
}
