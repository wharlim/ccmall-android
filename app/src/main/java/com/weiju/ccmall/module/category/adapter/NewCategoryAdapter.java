package com.weiju.ccmall.module.category.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Category;
import com.weiju.ccmall.shared.util.FrescoUtil;

import java.util.List;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/11/30.
 */
public class NewCategoryAdapter extends BaseQuickAdapter<Category, BaseViewHolder> {

    public NewCategoryAdapter(@Nullable List<Category> data) {
        super(R.layout.item_category, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Category item) {
        helper.setText(R.id.itemTitleTv, item.name);
        FrescoUtil.loadRvItemImg(helper, R.id.itemThumbIv, item.icon);
    }
}
