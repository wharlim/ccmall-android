package com.weiju.ccmall.module.challenge.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.weiju.ccmall.module.challenge.activity.ChallengePKDetailActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Challenge;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IChallengeService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

/**
 * @author chenyanming
 * @time 2019/6/27 on 14:46
 * @desc ${TODD}
 */
public class ChallengeManager {
    private static IChallengeService mService = ServiceManager.getInstance().createService(IChallengeService.class);

    /**
     * 参加挑战
     *
     * @param activity
     * @param activityId
     * @param pkScore
     */
    public static void addPkChallenge(Activity activity, String activityId, int pkScore) {
        ToastUtil.showLoading(activity);
        APIManager.startRequest(mService.addPkChallenge(activityId, pkScore),
                new BaseRequestListener<Challenge>(activity) {
                    @Override
                    public void onSuccess(Challenge result) {
                        EventBus.getDefault().post(new EventMessage(Event.challengeTrans));
                        Intent intent = new Intent(activity, ChallengePKDetailActivity.class);
                        intent.putExtra("activityId", activityId);
                        activity.startActivity(intent);
                        activity.finish();
                    }
                },activity);
    }

    public static void joinPkChallenge(Activity activity, String pkChallengeId, String pkMemberId) {
        ToastUtil.showLoading(activity);
        APIManager.startRequest(mService.joinPkChallenge(pkChallengeId, pkMemberId),
                new BaseRequestListener<Challenge>(activity) {
                    @Override
                    public void onSuccess(Challenge result) {
                        EventBus.getDefault().post(new EventMessage(Event.challengeTrans));
                        Intent intent = new Intent(activity, ChallengePKDetailActivity.class);
                        intent.putExtra("activityId", result.activityId);
                        activity.startActivity(intent);
                        activity.finish();
                    }
                },activity);
    }

    public static void showNoScoreDialog(Context context) {
        WJDialog wjDialog = new WJDialog(context);
        wjDialog.show();
        wjDialog.setContentText("您的积分不足，可获取更多积分后再发起活动");
        wjDialog.hideCancelBtn();
        wjDialog.setConfirmText("确认");
    }
}
