package com.weiju.ccmall.module.xysh.helper;

import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.bean.PlanCreateItem;
import com.weiju.ccmall.module.xysh.bean.PlanDetail;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class PayBackPlanCreateHelper {
    private static XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    private static double payBackAmount;
    private static double frozenAmount;
    private static String startDate;
    private static String endDate;
    private static String cardNo;
    private static String repaymentChannelId;
    private static String areaCode;
    private static int planType;
    private static String debitCardNo;
    private static String billDate;
    private static String repayDate;
    public static void reCreate(CreateCallBack cb) {
        if (payBackAmount == 0) {
            cb.created(null);
            return;
        }
        create(payBackAmount,
                frozenAmount,
                startDate,
                endDate,
                cardNo,
                repaymentChannelId,
                areaCode,
                planType,
                debitCardNo,
//                billDate,
//                repayDate,
                cb);
    }
    public static void create(
            double payBackAmount, // 还款金额
            double frozenAmount, // 周转金
            String startDate, // 还款开始时间(全自动必填)
            String endDate, // 还款结束时间(全自动必填)
            String cardNo,
            String repaymentChannelId,
            String areaCode,
            int planType, // planType=0:全自动，planType=1:半自动
            String debitCardNo, // 到账卡(半自动必填)
//            String billDate, // 账单日(半自动必填)
//            String repayDate, // 还款日(半自动必填)
            CreateCallBack cb
    ) {
        APIManager.startRequest(service.payBackPlanCreate(
                payBackAmount,
                frozenAmount,
                startDate,
                endDate,
                cardNo,
                repaymentChannelId,
                areaCode,
                planType,
                debitCardNo
//                billDate,
//                repayDate
        ), new Observer<XYSHCommonResult<PlanCreateItem>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<PlanCreateItem> ret) {
                ToastUtil.hideLoading();
                if (ret.success) {
                    PayBackPlanCreateHelper.payBackAmount = payBackAmount;
                    PayBackPlanCreateHelper.frozenAmount = frozenAmount;
                    PayBackPlanCreateHelper.startDate = startDate;
                    PayBackPlanCreateHelper.endDate = endDate;
                    PayBackPlanCreateHelper.cardNo = cardNo;
                    PayBackPlanCreateHelper.repaymentChannelId = repaymentChannelId;
                    PayBackPlanCreateHelper.areaCode = areaCode;
                    PayBackPlanCreateHelper.planType = planType;
                    PayBackPlanCreateHelper.debitCardNo = debitCardNo;
                    PayBackPlanCreateHelper.billDate = billDate;
                    PayBackPlanCreateHelper.repayDate = repayDate;
                    //RepaymentPlanActivity.start(IntelligenceRepaymentActivity.this, true, ret.data, creditCardItem);
                    cb.created(ret.data);
                } else {
                    ToastUtil.error(ret.message);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
                ToastUtil.hideLoading();
            }
        });
    }

    public interface CreateCallBack {
        void created(PlanCreateItem ret);
    }

    public static PlanDetail PlanCreateItem2PlanDetail(PlanCreateItem planCreateItem, float startMoney, int type) {
        PlanDetail planDetail = new PlanDetail();
        planDetail.payBackDetail = new ArrayList<>();
        planDetail.startDt = planCreateItem.startDate;
        planDetail.endDt = planCreateItem.endDate;
        planDetail.consumeAmount = planCreateItem.repayAmount;
        planDetail.feeAmt = planCreateItem.pounDage;
        planDetail.frozenAmt = startMoney;
        planDetail.payType = type;

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        for (PlanCreateItem.ListTranRecordsBean day : planCreateItem.listTranRecords
        ) {
            PlanDetail.PayBackDetailBean newDay = new PlanDetail.PayBackDetailBean();
            planDetail.payBackDetail.add(newDay);
            newDay.txnDt = df.format(new Date(day.excuteTime));
            newDay.datas = new ArrayList<>();

            for (PlanCreateItem.ListTranRecordsBean.ListTxnJnlsBean dayItem :
                    day.listTxnJnls) {
                PlanDetail.PayBackDetailBean.PlanItem newDayItem = new PlanDetail.PayBackDetailBean.PlanItem();
                newDay.datas.add(newDayItem);
                newDayItem.tranId = dayItem.tranId;
                newDayItem.txnSts = dayItem.tranSts;
                newDayItem.planExcuteTime = dayItem.excuteTime;
                newDayItem.repayTime = dayItem.repayTime;
                newDayItem.txnAmt = dayItem.txnAmt;
                newDayItem.payBackAmt = dayItem.paybackAmt;
                newDayItem.payType = dayItem.planType;
            }
        }

        return planDetail;
    }
}
