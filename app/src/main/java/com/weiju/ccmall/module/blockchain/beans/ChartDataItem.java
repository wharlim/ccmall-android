package com.weiju.ccmall.module.blockchain.beans;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChartDataItem {

    /**
     * values : [{"ccm":0,"day":"2019-10-18"}]
     * name : 算力
     * templateId : 02d9f54bd0154832b5cdddde5c479756
     */

    @SerializedName("name")
    public String name;
    @SerializedName("templateId")
    public String templateId;
    @SerializedName("values")
    public List<ValuesBean> values;

    public static class ValuesBean {
        /**
         * ccm : 0
         * day : 2019-10-18
         */

        @SerializedName("ccm")
        public double ccm;
        @SerializedName("day")
        public String day;
    }
}
