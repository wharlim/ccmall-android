package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

public class CardDoctorHistoryItem {

    /**
     * orderNo : cc733857900308467712
     * productId : 1
     * productResult : https://api.hihippo.cn/order/godetail/PU1216284735834468353/26a78cf1ef91aa63ad4f799509a702e75095602f
     * orderStatus : 1
     * productOrderId : a03ab049d71543b5
     * userName : 王夏亮
     * userId : 5a85b44c6fe023eda3d844a03af2f50d
     * productName : 信用卡测评
     * property2 : 招商银行信用卡中心
     * property1 : 4392250041436782
     * property4 : null
     * payType : 1
     * property3 : null
     * oemId : ccpay
     * productPrice : 19.8
     * createDate : 2020-01-12 17:04:12.0
     */

    @SerializedName("orderNo")
    public String orderNo;
    @SerializedName("productId")
    public String productId;
    @SerializedName("productResult")
    public String productResult;
    @SerializedName("orderStatus")
    public int orderStatus;
    @SerializedName("productOrderId")
    public String productOrderId;
    @SerializedName("userName")
    public String userName;
    @SerializedName("userId")
    public String userId;
    @SerializedName("productName")
    public String productName;
    @SerializedName("property2")
    public String property2;
    @SerializedName("property1")
    public String property1;
    @SerializedName("property4")
    public Object property4;
    @SerializedName("payType")
    public String payType;
    @SerializedName("property3")
    public Object property3;
    @SerializedName("oemId")
    public String oemId;
    @SerializedName("productPrice")
    public double productPrice;
    @SerializedName("createDate")
    public String createDate;
}
