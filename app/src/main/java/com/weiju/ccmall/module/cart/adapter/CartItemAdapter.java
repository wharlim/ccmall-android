package com.weiju.ccmall.module.cart.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.bean.CartItem;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.NumberField;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.contracts.OnValueChangeLister;
import com.weiju.ccmall.shared.manager.CartManager;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.cart.adapter
 * @since 2017-06-19
 */
class CartItemAdapter extends BaseAdapter<CartItem, CartItemAdapter.ViewHolder> {

    public CartItemAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_cart_item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setCartItem(items.get(position));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemSelectorIv)
        protected ImageView mItemSelectorIv;
        @BindView(R.id.itemThumbIv)
        protected SimpleDraweeView mItemThumbIv;
        @BindView(R.id.itemTitleTv)
        protected TextView mItemTitleTv;
        @BindView(R.id.itemPropertyTv)
        protected TextView mItemPropertyTv;
        @BindView(R.id.itemPriceTv)
        protected TextView mItemPriceTv;
        @BindView(R.id.itemNumberField)
        protected NumberField mItemNumberField;
        @BindView(R.id.itemPresentLayout)
        protected LinearLayout mItemPresentLayout;
        @BindView(R.id.recyclerView)
        protected RecyclerView mRecyclerView;
        @BindView(R.id.itemActivityLayout)
        protected LinearLayout mItemActivityLayout;
        @BindView(R.id.rvActivity)
        protected RecyclerView mRvActivity;
        @BindView(R.id.tvCc)
        protected TextView mTvCc;
        @BindView(R.id.tvLiveTag)
        protected TextView tvLiveTag;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setCartItem(final CartItem cartItem) {
//            if (cartItem.equals(itemView.getTag())) {
//                mItemSelectorIv.setSelected(cartItem.isSelected);
//                return;
//            }
//            itemView.setTag(cartItem);
            FrescoUtil.setImageSmall(mItemThumbIv, cartItem.thumb);
            tvLiveTag.setVisibility(cartItem.isLiveStoreProduct() ? View.VISIBLE : View.GONE);
            mItemTitleTv.setText(cartItem.name);
            mItemPropertyTv.setText(cartItem.properties);
            mItemPriceTv.setText(ConvertUtil.centToCurrency(context, cartItem));
            mItemNumberField.setValues(cartItem.amount, 1, cartItem.stock);
            mItemSelectorIv.setSelected(cartItem.isSelected);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventUtil.viewProductDetail(context, cartItem.skuId, false);
                }
            });
            mItemNumberField.setOnChangeListener(new OnValueChangeLister() {
                @Override
                public void changed(int value) {
                    if (value != cartItem.amount) {
                        cartItem.amount = value;
                        CartManager.updateCartItem(context, cartItem.skuId, value);
                        EventBus.getDefault().post(new EventMessage(Event.selectCartItem));
                    }
                }
            });
            mItemSelectorIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LogUtils.e("发送一个  selectCartItem");

                    cartItem.isSelected = !cartItem.isSelected;
                    mItemSelectorIv.setSelected(cartItem.isSelected);
                    EventBus.getDefault().post(new EventMessage(Event.selectCartItem));
                }
            });

            if (cartItem.presents != null && cartItem.presents.size() > 0) {
                mItemPresentLayout.setVisibility(View.VISIBLE);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                linearLayoutManager.setAutoMeasureEnabled(true);
                mRecyclerView.setLayoutManager(linearLayoutManager);
                mRecyclerView.setHasFixedSize(true);
                CartItemPresentAdapter cartItemPresentAdapter = new CartItemPresentAdapter(context);
                cartItemPresentAdapter.setHasStableIds(true);
                cartItemPresentAdapter.setItems(cartItem.presents);
                mRecyclerView.setAdapter(cartItemPresentAdapter);
            } else {
                mItemPresentLayout.setVisibility(View.GONE);
            }


            if (cartItem.activityTag != null && cartItem.activityTag.size() > 0) {
                mItemActivityLayout.setVisibility(View.VISIBLE);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                linearLayoutManager.setAutoMeasureEnabled(true);
                mRvActivity.setLayoutManager(linearLayoutManager);
                mRvActivity.setHasFixedSize(true);
                CartItemActivityAdapter adapter = new CartItemActivityAdapter(context);
                adapter.setHasStableIds(true);
                adapter.setItems(cartItem.activityTag);
                adapter.setCartItem(cartItem);
                mRvActivity.setAdapter(adapter);
            } else {
                mItemActivityLayout.setVisibility(View.GONE);
            }
            if (cartItem.productType == 11){
                mTvCc.setText(Html.fromHtml(String.format("需使用<font color =\"#f51861\">%1$s</font>金券，预计可返<font color =\"#f51861\">%2$s%%</font>算率",
                        MoneyUtil.centToYuanStrNoZero(cartItem.goldenTicket), cartItem.countRateExc)));
            }else if (cartItem.ticket == 0 && cartItem.goldenTicket==0) {
                mTvCc.setText(Html.fromHtml(String.format("预计可返<font color =\"#f51861\">%s%%</font>算率",
                        cartItem.countRateExc)));
            } else if (cartItem.ticket > 0 && cartItem.goldenTicket>0){
                mTvCc.setText(Html.fromHtml(String.format("需要使用<font color =\"#f51861\">%1$s</font>C券，" +
                                "预计可返<font color =\"#f51861\">%2$s%%</font>算率，可用金券兑换<font color =\"#f51861\">%3$s</font>余额",
                        MoneyUtil.centToYuanStrNoZero(cartItem.ticket), cartItem.countRateExc,
                        MoneyUtil.centToYuanStrNoZero(cartItem.goldenTicket))));
            }else if (cartItem.ticket > 0){
                mTvCc.setText(Html.fromHtml(String.format("需要使用<font color =\"#f51861\">%1$s</font>C券，" +
                                "预计可返<font color =\"#f51861\">%2$s%%</font>算率",
                        MoneyUtil.centToYuanStrNoZero(cartItem.ticket), cartItem.countRateExc)));
            }else {
                mTvCc.setText(Html.fromHtml(String.format("预计可返<font color =\"#f51861\">%1$s%%</font>算率，" +
                                "可用金券兑换<font color =\"#f51861\">%2$s</font>余额", cartItem.countRateExc,
                        MoneyUtil.centToYuanStrNoZero(cartItem.goldenTicket))));
            }
        }

        private void setPresentsList(RecyclerView rvPresents, List<SkuInfo> presents) {
            CartPresentsAdapter cartPresentsAdapter = new CartPresentsAdapter(presents);
            rvPresents.setLayoutManager(new LinearLayoutManager(rvPresents.getContext()));
            rvPresents.setAdapter(cartPresentsAdapter);
        }
    }
}
