package com.weiju.ccmall.module.product.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

/**
 * @author chenyanming
 * @time 2019/11/5 on 15:30
 * @desc ${TODD}
 */
public class ProductAdapter extends BaseQuickAdapter<SkuInfo, BaseViewHolder> {
    private boolean mIsAdd;

    public void isAdd(boolean isAdd) {
        mIsAdd = isAdd;
    }

    public ProductAdapter() {
        super(R.layout.item_product);
    }

    @Override
    protected void convert(BaseViewHolder helper, SkuInfo item) {
        if (mIsAdd) {
            item.stock = 1;
        }
        FrescoUtil.setSkuImgSmallMask(helper.getView(R.id.itemThumbIv), item);
        helper.setText(R.id.itemTitleTv, item.name);
        helper.setText(R.id.itemMoneyTv, MoneyUtil.centToYuan¥StrNoZero(item.retailPrice));
    }
}
