package com.weiju.ccmall.module.community;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.blankj.utilcode.utils.ToastUtils;
import com.esafirm.rxdownloader.RxDownloader;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import zlc.season.rxdownload2.RxDownload;
import zlc.season.rxdownload2.entity.DownloadStatus;

import static android.os.Environment.DIRECTORY_DCIM;

/**
 * @author Stone
 * @time 2018/1/4  16:00
 * @desc ${TODD}
 */

public class ImgDownLoadUtils {
    private static String sPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath() + "/Camera";

    public static void savePic2Local(List<String> picList, Context context) {
        WeakReference<Context> contextWeakReference = new WeakReference<>(context);
        context = contextWeakReference.get();
        if (context == null) {
            return;
        }
        final RxDownloader mRxDownload = new RxDownloader(context);
        Single<List<String>> listSingle = Observable.fromIterable(picList).flatMap(new Function<String, ObservableSource<String>>() {
            @Override
            public ObservableSource<String> apply(String imgUrl) throws Exception {
                String fileName = imgUrl.substring(imgUrl.lastIndexOf("/") + 1);
                return mRxDownload.download(imgUrl, fileName, DIRECTORY_DCIM, "*/*", false);
            }
        }).toList();

        final Context finalContext = contextWeakReference.get();
        listSingle.observeOn(Schedulers.newThread()).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<List<String>>() {
            @Override
            public void accept(List<String> strings) throws Exception {
                if (finalContext != null) {
                    ToastUtils.showShortToast("保存成功");
                    scanFiles(finalContext);
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
            }
        });
    }

    public static void saveVideo2Local(String downloadUrl, final Context context) {
        if (context == null || com.blankj.utilcode.utils.StringUtils.isEmpty(downloadUrl)) {
            ToastUtils.showShortToast("参数异常");
            return;
        }
        final File localFile = getLocalFile(downloadUrl);
        if (com.blankj.utilcode.utils.FileUtils.isFileExists(localFile)) {
            ToastUtils.showShortToast("保存成功");
            return;
        }
        RxDownload
                .getInstance(context)
                .download(downloadUrl, localFile.getName(), sPath)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<DownloadStatus>() {
                    @Override
                    public void onSubscribe(Disposable d) {
                        ToastUtils.showShortToast("开始下载");
                    }

                    @Override
                    public void onNext(DownloadStatus downloadStatus) {
                        if (downloadStatus.getPercentNumber() == 100) {
                            ToastUtils.showShortToast("下载完成");
                            senScanFileBroadcast(context, localFile);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtils.showShortToast("下载失败");
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private static File getLocalFile(String downloadUrl) {
        String fileName = downloadUrl.substring(downloadUrl.lastIndexOf("/") + 1);
        com.blankj.utilcode.utils.FileUtils.createOrExistsDir(sPath);
        File file = new File(sPath, fileName);
        return file;
    }

    private static void scanFiles(Context finalContext) {
        MediaScanner mediaScanner = new MediaScanner(finalContext);
        String[] filearr = new String[]{Environment.getExternalStoragePublicDirectory(DIRECTORY_DCIM).getPath()};
        String[] typeArr = new String[]{"*/*"};
        mediaScanner.scanFiles(filearr, typeArr);
    }

    /**
     * 发送广播通知系统有新的文件
     *
     * @param context
     * @param saveFile 新保存的文件
     */
    private static void senScanFileBroadcast(Context context, File saveFile) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(Uri.fromFile(saveFile));
        context.sendBroadcast(intent);
    }

}
