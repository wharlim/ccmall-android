package com.weiju.ccmall.module.page

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.blankj.utilcode.utils.AppUtils
import com.blankj.utilcode.utils.FileUtils
import com.blankj.utilcode.utils.LogUtils
import com.facebook.drawee.generic.RootDrawable
import com.just.agentweb.AgentWeb
import com.umeng.socialize.UMShareListener
import com.umeng.socialize.bean.SHARE_MEDIA
import com.weiju.ccmall.R
import com.weiju.ccmall.module.community.BaseFragment
import com.weiju.ccmall.shared.component.dialog.ShareDialog
import com.weiju.ccmall.shared.util.FrescoUtil
import com.weiju.ccmall.shared.util.ToastUtil
import kotlinx.android.synthetic.main.base_layout.*
import kotlinx.android.synthetic.main.fragment_xysh_share.*
import top.zibin.luban.Luban
import top.zibin.luban.OnCompressListener
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

class XyshFragment : BaseFragment() {

    private var bitmap: Bitmap? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var contentView = view
        if (contentView == null) {
            contentView = inflater.inflate(R.layout.base_layout, container, false)
            val layout = contentView?.findViewById<FrameLayout>(R.id.baseContentLayout)
            inflater.inflate(R.layout.fragment_xysh_share, layout, true)
        }
        val parent = contentView?.parent as? ViewGroup?
        parent?.removeView(contentView)
        return contentView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        headerLayout.setTitle("分享")
        headerLayout.setLeftDrawable(R.mipmap.icon_back_black);
        headerLayout.setOnLeftClickListener { activity?.finish() }
        loadImg()
        tvShare.setOnClickListener {
//            if (shareImg.drawable == null)
//                ToastUtil.error("等待数据")
//            else
                share()
        }
        tvSave.setOnClickListener {
            ToastUtil.showLoading(context)
            val viewBitmap = getBitmap()
            if (viewBitmap != null) {
                val savedImageFile = getSaveFile()
                saveBitmapFile(viewBitmap, savedImageFile, true)
            }
            ToastUtil.hideLoading()
        }
    }

    override fun onStop() {
        super.onStop()
        ToastUtil.hideLoading()
    }

    private fun loadImg() {
        val url = arguments?.getString("imageUrl")
        if (url != null)
            FrescoUtil.setImage(shareImg, url)
//        webView.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.SINGLE_COLUMN
//        webView.loadUrl(url)
        AgentWeb.with(this)
                .setAgentWebParent(webView, FrameLayout.LayoutParams(-1, -1))
                .closeDefaultIndicator()
                .createAgentWeb()
                .ready()
                .go(url)
    }

    private fun share() {
//        val viewBitmap = getBitmap()
//        val savedImageFile = getSaveFile()
//        if (viewBitmap == null)
//            return

//        saveBitmapFile(viewBitmap, savedImageFile, false)
        val savedImagePath= arguments?.getString("f") ?: return
        val savedImageFile = File(savedImagePath)
        ToastUtil.showLoading(context)
        Luban.with(context)
                .load(savedImageFile)
                .setCompressListener(object : OnCompressListener {
                    override fun onStart() {
                        LogUtils.e("压缩图片开始")
                    }

                    override fun onSuccess(file: File) {
                        savedImageFile.delete()
                        ToastUtil.hideLoading()
                        showShareDialog(file)
                    }

                    override fun onError(e: Throwable) {
                        ToastUtil.error("压缩图片出错:" + e.message)
                        ToastUtil.hideLoading()
                    }
                }).launch()
    }

    private fun showShareDialog(file: File) {
        ShareDialog(activity, file, object : UMShareListener {
            override fun onStart(share_media: SHARE_MEDIA) {
                ToastUtil.showLoading(context)
            }

            override fun onResult(share_media: SHARE_MEDIA) {
                ToastUtil.success("分享成功")
                ToastUtil.hideLoading()
                file.delete()
            }

            override fun onError(share_media: SHARE_MEDIA, throwable: Throwable) {
                ToastUtil.error("分享出错:" + throwable.message)
                ToastUtil.hideLoading()
                file.delete()
            }

            override fun onCancel(share_media: SHARE_MEDIA) {
                ToastUtil.hideLoading()
                file.delete()
            }
        }).show()
    }

    private fun getBitmap(): Bitmap? {
        val drawable = shareImg.drawable
        if (bitmap == null && drawable is RootDrawable) {
            val d = drawable.current ?: return null
            if (d.intrinsicWidth > 0 && d.intrinsicHeight > 0) {
                shareImg.isDrawingCacheEnabled = true
                val t = shareImg.drawingCache
                bitmap = Bitmap.createBitmap(d!!.intrinsicWidth, d.intrinsicHeight, Bitmap.Config.ARGB_8888)
                val c = Canvas(bitmap!!)
                d.setBounds(0, 0, d.intrinsicWidth, d.intrinsicHeight)
                d.draw(c)
                shareImg.setImageBitmap(t)
            }
        }

        return bitmap
    }

    private fun getSaveFile(): File {
        val dirPath = (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).path
                + "/" + AppUtils.getAppName(context))
        val file = File(dirPath, System.currentTimeMillis().toString() + ".jpg")
        FileUtils.createOrExistsDir(dirPath)
        return file
    }

    private fun saveBitmapFile(bitmap: Bitmap, saveFile: File, isShowMsg: Boolean) {
        try {
            val bos = BufferedOutputStream(FileOutputStream(saveFile))
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
            bos.flush()
            bos.close()
            if (isShowMsg) {
                ToastUtil.success("保存成功")
                val intent = Intent()
                intent.action = Intent.ACTION_MEDIA_SCANNER_SCAN_FILE
                intent.data = Uri.fromFile(saveFile)
                activity?.sendBroadcast(intent)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }
}
