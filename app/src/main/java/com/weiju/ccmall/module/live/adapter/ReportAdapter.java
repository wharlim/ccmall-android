package com.weiju.ccmall.module.live.adapter;

import android.support.v7.widget.AppCompatRadioButton;
import android.widget.RadioButton;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.common.collect.Lists;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.LiveReportTypesItem;

import java.util.ArrayList;
import java.util.Arrays;

public class ReportAdapter extends BaseQuickAdapter<LiveReportTypesItem, BaseViewHolder> {

    private LiveReportTypesItem selectedItem;

    public ReportAdapter() {
        super(R.layout.item_live_report);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveReportTypesItem item) {
        AppCompatRadioButton rb = helper.getView(R.id.radioBtn);
        rb.setChecked(selectedItem == item);
        rb.setText(item.reportTitle);
    }

    public void setSelectedItem(LiveReportTypesItem item) {
        selectedItem = item;
        notifyDataSetChanged();
    }

    public LiveReportTypesItem getSelectedItem() {
        return selectedItem;
    }
}
