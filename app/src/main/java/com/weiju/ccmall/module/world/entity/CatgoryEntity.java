package com.weiju.ccmall.module.world.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Ben
 * @date 2020/4/24.
 */
public class CatgoryEntity {

    @SerializedName("categoryId")
    public String categoryId;
    @SerializedName("categoryName")
    public String categoryName;
    @SerializedName("children")
    public List<ChildrenBean> children;

    public CatgoryEntity(String categoryId, String categoryName) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
    }

    public static class ChildrenBean {
        /**
         * categoryId : 0
         * categoryName :
         * children : []
         */

        @SerializedName("categoryId")
        public String categoryId;
        @SerializedName("categoryName")
        public String categoryName;
    }
}
