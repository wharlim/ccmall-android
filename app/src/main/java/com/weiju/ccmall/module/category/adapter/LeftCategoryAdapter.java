package com.weiju.ccmall.module.category.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.bean.Category;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;

import org.greenrobot.eventbus.EventBus;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.category.adapter
 * @since 2017-06-17
 */
public class LeftCategoryAdapter extends BaseAdapter<Category, LeftCategoryAdapter.ViewHolder> {

    public LeftCategoryAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_left_category, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Category category = items.get(position);
        holder.mItemLabelTv.setText(category.name);
        holder.mItemLabelTv.setSelected(category.isSelected);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().postSticky(new EventMessage(Event.changeCategory, category.id));
            }
        });
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView mItemLabelTv;

        public ViewHolder(View itemView) {
            super(itemView);
            mItemLabelTv = (TextView) itemView.findViewById(R.id.itemLabelTv);
        }
    }
}
