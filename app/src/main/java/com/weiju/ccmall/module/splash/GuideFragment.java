package com.weiju.ccmall.module.splash;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.blankj.utilcode.utils.SPUtils;
import com.bumptech.glide.Glide;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.MainActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.util.FrescoUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class GuideFragment extends BaseFragment {


    @BindView(R.id.ivImage)
    SimpleDraweeView mIvImage;
    Unbinder unbinder;
    @BindView(R.id.ivText)
    ImageView mIvText;
    @BindView(R.id.ivGoMain)
    ImageView mIvGoMain;

    public static GuideFragment newInstance(String imageUrl, boolean isEnd) {
        Bundle args = new Bundle();
        args.putString("imageUrl", imageUrl);
        args.putBoolean("isEnd", isEnd);
        GuideFragment fragment = new GuideFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guide, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String imageUrl = getArguments().getString("imageUrl");
        boolean isEnd = getArguments().getBoolean("isEnd");
        Glide.with(this).load(imageUrl).into(mIvImage);
//        FrescoUtil.setImage(mIvImage, imageUrl);
        if (isEnd) {
//            mIvGoMain.setVisibility(View.VISIBLE);
            mIvImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onViewClicked();
                }
            });
        }
    }

    private void startAnima() {
        AlphaAnimation alphaAnimation = (AlphaAnimation) AnimationUtils.loadAnimation(getContext(), R.anim.guide_anim);
        mIvText.startAnimation(alphaAnimation);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.ivGoMain)
    public void onViewClicked() {
        SPUtils spUtils = new SPUtils(GuideActivity.class.getName());
        spUtils.putBoolean("ontStart", true);
        startActivity(new Intent(getContext(), MainActivity.class));
        getActivity().finish();
    }
}
