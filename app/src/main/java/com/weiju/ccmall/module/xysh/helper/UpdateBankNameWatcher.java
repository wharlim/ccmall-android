package com.weiju.ccmall.module.xysh.helper;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import com.weiju.ccmall.module.xysh.bean.SelectSupportBankItem;

public class UpdateBankNameWatcher implements TextWatcher {

    private OnGotBankNameListener mOnGotBankNameListener;

    public UpdateBankNameWatcher(OnGotBankNameListener onGotBankNameListener) {
        mOnGotBankNameListener = onGotBankNameListener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String cardNo = s.toString().trim();
        if (cardNo.length() < 15) {
            return;
        }
        BankUtils.checkCard(null, cardNo, BankUtils.CARD_CHECK_ACCEPT_DEBIT_CREDIT, new BankUtils.OnCheckCardBankResultListener() {

            @Override
            public void onCheckResult(boolean pass, String type, String bankAbbr) {
                if (pass) {
                    SelectSupportBankItem bank = BankUtils.getSupportBankByAbbr(bankAbbr);
                    if (bank != null) {
                        mOnGotBankNameListener.onGotBankName(bank.bankName);
                    }
                }
            }
        });
    }

    public interface OnGotBankNameListener {
        void onGotBankName(String bankName);
    }
}
