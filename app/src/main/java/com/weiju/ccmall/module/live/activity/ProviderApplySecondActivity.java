package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.OptionsPickerView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.entity.ApplyInfoEntity;
import com.weiju.ccmall.module.live.entity.ShopApplyEntity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Category;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICategoryService;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProviderApplySecondActivity extends BaseActivity {

    @BindView(R.id.tvShopName)
    TextView mTvShopName;
    @BindView(R.id.tvProductCategory)
    TextView mTvProductCategory;

    private ICategoryService mCategoryService;
    private OptionsPickerView mPvOptions;

    private ArrayList<Category> mCategories;
    private ShopApplyEntity mEntity;

    ILiveStoreService iLiveStoreService;
    private Category mCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_apply_second);
        ButterKnife.bind(this);
        setTitle("入驻申请");
        setLeftBlack();

        iLiveStoreService = ServiceManager.getInstance().createService(ILiveStoreService.class);
        mCategoryService = ServiceManager.getInstance().createService(ICategoryService.class);
        mEntity = (ShopApplyEntity) getIntent().getSerializableExtra("shopApplyEntity");

        mTvShopName.setText(mEntity.getShopName());
        mPvOptions = new OptionsPickerView.Builder(this, (options1, options2, options3, v) -> {
            mCategory = mCategories.get(options1);
            mTvProductCategory.setText(mCategory.name);
        })
                .setDividerColor(getResources().getColor(R.color.line_color))
                .setTitleBgColor(getResources().getColor(R.color.white))
                .setBgColor(getResources().getColor(R.color.white))
                .setCancelColor(getResources().getColor(R.color.text_gray))
                .setSubmitColor(getResources().getColor(R.color.red))
                .setSubmitText("完成")
                .setSubCalSize(15)
                .setTextColorCenter(getResources().getColor(R.color.default_text_color)) //设置选中项文字颜色
                .setContentTextSize(18)
                .setOutSideCancelable(false)// default is true
                .build();
    }

    private void initCategory() {
        APIManager.startRequest(mCategoryService.getTopCategory(1), new BaseRequestListener<PaginationEntity<Category, Object>>(this) {
            @Override
            public void onSuccess(PaginationEntity<Category, Object> result) {
                mCategories = result.list;
                if (mCategories == null || mCategories.isEmpty()) {
                    return;
                }
                mPvOptions.setPicker(mCategories);
                mPvOptions.show();

            }
        }, this);
    }


    @OnClick({R.id.tvSubmitAudit, R.id.llProductCategory})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llProductCategory:
                initCategory();
                break;
            case R.id.tvSubmitAudit:
                String productCategory = mTvProductCategory.getText().toString();
                if (productCategory.isEmpty()) {
                    ToastUtil.error("请选择主营类目");
                    return;
                }
                HashMap<String, Object> params = new HashMap<>();
                params.put("categoryId", mCategory.id);
                params.put("storeName", mEntity.getShopName());
                params.put("provinceName", mEntity.getProvinceName());
                params.put("cityName", mEntity.getCityName());
                params.put("districtName", mEntity.getDistrictName());
                params.put("addr", mEntity.getAddr());
                params.put("phone", mEntity.getPhone());
                params.put("mail", mEntity.getMail());
                APIManager.startRequest(iLiveStoreService.signApply(params), new BaseRequestListener<ApplyInfoEntity>() {
                    @Override
                    public void onSuccess(ApplyInfoEntity result) {
                        super.onSuccess(result);
                        EventBus.getDefault().post(new EventMessage(Event.signApplySuccess));
                        ProviderApplyStatusActivity.start(ProviderApplySecondActivity.this);
                        finish();
                    }
                }, this);

                break;
        }

    }

    public static void start(Context context, ShopApplyEntity shopApplyEntity) {
        Intent intent = new Intent(context, ProviderApplySecondActivity.class);
        intent.putExtra("shopApplyEntity", shopApplyEntity);
        context.startActivity(intent);
    }

}
