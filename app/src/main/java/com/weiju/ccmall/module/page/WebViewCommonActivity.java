package com.weiju.ccmall.module.page;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebBackForwardList;
import android.webkit.WebHistoryItem;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.activity.QuickPaymentsActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.component.H5WebView;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.util.SessionUtil;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.mmbs.module.page
 * @since 2017-07-05
 */
public class WebViewCommonActivity extends BaseActivity {

    private static final String TAG = "WebViewCommonActivity";

    @BindView(R.id.webView)
    H5WebView webView;
    @BindView(R.id.baseRelativeLayout)
    RelativeLayout baseRelativeLayout;
    @BindView(R.id.pb)
    ProgressBar mPb;

    private String mUrl;
    private boolean mIsJKP;
    private boolean hideToolbar;
    private boolean isAuthChannel;

    private TextView close;
    private float v = 0.0f;
    private RelativeLayout.LayoutParams lp;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_common);
        ButterKnife.bind(this);
        mUrl = getIntent().getStringExtra("url");
        hideToolbar = getIntent().getBooleanExtra("hideToolbar", false);
        isAuthChannel = getIntent().getBooleanExtra("isAuthChannel", false);
        mIsJKP = getIntent().getBooleanExtra("isJKP", false);
        if (mUrl.contains("create-chain.net") && SessionUtil.getInstance().isLogin()) {
            Uri uri = Uri.parse(mUrl);
            mUrl = uri.buildUpon()
                    .appendQueryParameter("token", SessionUtil.getInstance().getOAuthToken())
                    .build()
                    .toString();
        }
        Log.d(TAG, "mUrl: " + mUrl);
        if (hideToolbar) {
            hideHeader();
        } else {
            setLeftBlack();
        }
        if (StringUtils.isEmpty(mUrl)) {
            return;
        }
        if (isAuthChannel) {
            mHeaderLayout.setOnLeftClickListener(v -> showBackDialog());
        }
        openH5Web(mUrl);
    }

    @SuppressLint("ResourceType")
    private void hideHeader() {
        close = new TextView(this);
        close.setVisibility(View.GONE);
        close.setTextColor(getResources().getColor(R.color.white));
        close.setText("关闭");
        close.setGravity(Gravity.CENTER);
        v = getResources().getDisplayMetrics().widthPixels / 1080f;
        int size = (int) (v * 40);
        close.setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) size);
        lp = new RelativeLayout.LayoutParams(size * 3, (int) (v * 125));
        lp.setMargins((int) (v * 30), 0, 0, 0);
        close.setLayoutParams(lp);
        close.setOnClickListener(v -> finish());
        baseRelativeLayout.addView(close, baseRelativeLayout.getChildCount());
        getHeaderLayout().setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        Uri uri = Uri.parse(webView.getUrl());
        if (isAuthChannel) {
            showBackDialog();
        } else if (webView.canGoBack()) {
            String url = webView.getUrl();
            int i = url.indexOf("//");
            if (i != -1) {
                url = url.substring(i + 2, url.length());
            } /*else {
                int i1 = url.indexOf("https://");
                if (i1 != -1) {
                    url = url.substring(i1, url.length());
                }
            }*/
            if (url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/CashBill") || url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/") || url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/RepayPlanCreditList") || url.equals("credit.ccmallv2.create-chain.net//customizYTH/#/My")) {
                finish();
                return;
            }
            if (uri.getHost().equals(BuildConfig.XYSH_HOST)) {
                switch (uri.getFragment()) {
                    case "/":
                    case "/CashBill":
                    case "/RepayPlanCreditList":
                    case "/My":
                        super.onBackPressed();
                        break;
                    default:
                        webView.goBack();
                }
            } else {
                WebBackForwardList list = webView.copyBackForwardList();
                if (list != null && list.getSize() > 0) {
                    WebHistoryItem itemAtIndex = list.getItemAtIndex(list.getCurrentIndex() - 1);
                    if (itemAtIndex != null) {
                        if (uri.getHost().equals(BuildConfig.XYSH_HOST)) {
                            webView.loadUrl(getIntent().getStringExtra("url"));
                        } else {
                            webView.goBack();
                        }

                    } else {
                        webView.loadUrl(getIntent().getStringExtra("url"));
                    }
                } else {
                    webView.loadUrl(getIntent().getStringExtra("url"));
                }
            }
        } else {
            super.onBackPressed();
        }
    }

    private void showBackDialog() {
        WJDialog wjDialog = new WJDialog(this);
        wjDialog.show();
        wjDialog.setTitle("提示");
        wjDialog.setContentText("如已开通，请点击[返回商户]进行数据更新");
        wjDialog.setConfirmText("已点击");
        wjDialog.setOnConfirmListener(v -> {
            wjDialog.dismiss();
            Intent intent = new Intent(WebViewCommonActivity.this, QuickPaymentsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            WebViewCommonActivity.this.startActivity(intent);
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        webView.destroy();
    }

    private void openH5Web(String url) {
        webView.setWebViewChangeTitleCallback(title -> {
            if (!hideToolbar) {
                setTitle(title);
            }
        });
        webView.setProgressbar(mPb);
        //  js调 java
        webView.addJavascriptInterface(new AndroidInterface(), "app");
        if (mIsJKP) {
            webView.addJavascriptInterface(new AndroidJKPInterface(), "android");
        }
        webView.loadUrl(url);
    }

    class AndroidJKPInterface {

        @JavascriptInterface
        public void onClickTakeToken() {
            UserService.checkIsLogin(WebViewCommonActivity.this, new UserService.IsLoginListener() {
                @Override
                public void isLogin() {
                }
            });
        }

        @JavascriptInterface
        public void onClickTakeCoupon(final String url) {
            UserService.checkIsLogin(WebViewCommonActivity.this, new UserService.IsLoginListener() {
                @Override
                public void isLogin() {

                }
            });
        }

        @JavascriptInterface
        public void onClickCopy(String content) {
        }

        @JavascriptInterface
        public void onClickShare(String title, String tkPwd, String img) {
        }
    }

    class AndroidInterface {
        @JavascriptInterface
        public void setTitle(final String titleStr) {
            setTitle(titleStr);
            getHeaderLayout().setOnRightClickListener(null);
            getHeaderLayout().setRightText("");
        }
    }

    public static void start(Context context, String url, boolean hideToolbar, boolean isJKP) {
        Intent intent = new Intent(context, WebViewCommonActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("hideToolbar", hideToolbar);
        intent.putExtra("isJKP", isJKP);
        context.startActivity(intent);
    }

    public static void start(Context context, String url, boolean isAuthChannel) {
        Intent intent = new Intent(context, WebViewCommonActivity.class);
        intent.putExtra("url", url);
        intent.putExtra("isAuthChannel", isAuthChannel);
        context.startActivity(intent);
    }
}
