package com.weiju.ccmall.module.xysh.activity.repayment;

import android.text.Html;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.util.MultiTypeDelegate;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.activity.IntelligenceRepaymentActivity;
import com.weiju.ccmall.module.xysh.bean.PlanDetail;

public class RepaymentOneDayAdapter extends BaseQuickAdapter<PlanDetail.PayBackDetailBean.PlanItem, BaseViewHolder> {
    private boolean mIsPreview;
    public RepaymentOneDayAdapter(boolean isPreview) {
        super(R.layout.item_replay_plan);
        mIsPreview = isPreview;
        MultiTypeDelegate<PlanDetail.PayBackDetailBean.PlanItem> multiTypeDelegate = new MultiTypeDelegate<PlanDetail.PayBackDetailBean.PlanItem>() {
            @Override
            protected int getItemType(PlanDetail.PayBackDetailBean.PlanItem planItem) {
                if (planItem instanceof PlanDetail.PayBackDetailBean.PlanItemSummary) {
                    return 1;
                }
                return 0;
            }
        };
        multiTypeDelegate.registerItemType(0, R.layout.item_replay_plan);
        multiTypeDelegate.registerItemType(1, R.layout.item_replay_plan_summary);
        setMultiTypeDelegate(multiTypeDelegate);
    }

    @Override
    protected void convert(BaseViewHolder helper, PlanDetail.PayBackDetailBean.PlanItem item) {
        if (item instanceof PlanDetail.PayBackDetailBean.PlanItemSummary) {
            PlanDetail.PayBackDetailBean.PlanItemSummary sItem =
                    (PlanDetail.PayBackDetailBean.PlanItemSummary) item;
            helper.setText(R.id.tvRepayTimeout, sItem.repayTimeout);
            helper.setText(R.id.tvPayMoney, String.format("%.2f元", sItem.payMoney));
//            for (PlanDetail.PayBackDetailBean.PlanItem i: getData()) {
//                if ("6".equals(i.rePayStatus)) {
//                    helper.setText(R.id.tvRepayState, i.reason);
//                }
//            }
        } else {
            helper.setText(R.id.tvNo, item.tranId);
            helper.setVisible(R.id.tvState, !mIsPreview);
            helper.setVisible(R.id.tvPayMoneySemi, false);
            int redColor = helper.itemView.getResources().getColor(R.color.color_text_red);
            if (!mIsPreview && item.payType == IntelligenceRepaymentActivity.REPAY_TYPE_FULL_AUTOMATION) {
                helper.setVisible(R.id.tvFailReason, false);
                String state = "失败";
                helper.setBackgroundRes(R.id.tvState, R.drawable.bg_gold_stroke);
                helper.setTextColor(R.id.tvState, helper.itemView.getResources().getColor(R.color.text_gold));
                if ("0".equals(item.txnSts)) {
                    state = "未进行";
                } else if ("1".equals(item.txnSts)) {
                    state = "进行中";
                } else if ("2".equals(item.txnSts)) {
                    state = "交易成功";
                } else {
                    String reason = TextUtils.isEmpty(item.reason) ? "交易失败" : item.reason;
                    helper.setVisible(R.id.tvFailReason, true);
                    helper.setText(R.id.tvFailReason, Html.fromHtml("<span style=\"color:#F7263C\">失败原因：</span>" + reason));
                }
                helper.setText(R.id.tvState, state);
            } else if (item.payType == IntelligenceRepaymentActivity.REPAY_TYPE_SEMI_AUTOMATION) {
                helper.setVisible(R.id.tvState, false);
                helper.setVisible(R.id.tvPayMoneySemi, true);

                int color;
                // 还款状态：rePayStatus=0：等待还款，5：执行中，6：还款失败，7：还款成功
                if ("7".equals(item.rePayStatus)) {
                    // 成功
                    color = mContext.getResources().getColor(R.color.text_gray);

                } else {
                    // 未进行/进行中/失败
                    color = mContext.getResources().getColor(R.color.text_black);
                }
                helper.setTextColor(R.id.tvPayMoneySemi, color);
                helper.setTextColor(R.id.tvPayTime, color);
                helper.setTextColor(R.id.tvPayMoney, color);
                helper.setTextColor(R.id.tvPayLabel, color);

                if ("6".equals(item.rePayStatus)) {
                    helper.setText(R.id.tvPayLabel, item.reason);
                    helper.setTextColor(R.id.tvPayLabel, redColor);
                } else {
                    helper.setText(R.id.tvPayLabel, "消费");
                }
            }
            helper.setText(R.id.tvPayTime, item.planExcuteTime);
            helper.addOnClickListener(R.id.tvCopy);
            if (item.payType == IntelligenceRepaymentActivity.REPAY_TYPE_SEMI_AUTOMATION) {
                helper.setVisible(R.id.tvRepayTime, false);
                ((View)helper.getView(R.id.tvRepayMoney).getParent()).setVisibility(View.GONE);
                helper.setText(R.id.tvPayMoneySemi, String.format("%.2f元", item.txnAmt));
                helper.setText(R.id.tvPayMoney, String.format("到账%.2f元", item.payBackAmt));
            } else {
                helper.setVisible(R.id.tvRepayTime, true);
                ((View)helper.getView(R.id.tvRepayMoney).getParent()).setVisibility(View.VISIBLE);
                helper.setText(R.id.tvPayMoney, String.format("%.2f元", item.txnAmt));
                helper.setText(R.id.tvRepayTime, item.repayTime);
                helper.setText(R.id.tvRepayMoney, String.format("%.2f元", item.payBackAmt));
            }
        }
    }
}
