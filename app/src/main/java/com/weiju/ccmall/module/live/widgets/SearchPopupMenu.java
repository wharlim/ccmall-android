package com.weiju.ccmall.module.live.widgets;

import android.content.Context;
import android.os.Build;
import android.support.v4.widget.PopupWindowCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.util.ConvertUtil;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchPopupMenu extends PopupWindow {
    private OnMenuClickListener mOnMenuClickListener;
    private String[] title = new String[]{"主播昵称", "手机号", "直播标题"};

    public SearchPopupMenu(Context context, OnMenuClickListener onMenuClickListener) {
        super(ConvertUtil.dip2px(120), ViewGroup.LayoutParams.WRAP_CONTENT);
        View contentView = LayoutInflater.from(context).inflate(R.layout.menu_live_search, null);
        ButterKnife.bind(this, contentView);
        setContentView(contentView);
        mOnMenuClickListener = onMenuClickListener;
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setElevation(10);
        }
        setOutsideTouchable(true);
        setFocusable(true);
    }

    @OnClick({R.id.llNickName, R.id.llPhone, R.id.llTitle})
    public void onViewClicked(View view) {
        dismiss();
        if (mOnMenuClickListener != null) {
            mOnMenuClickListener.onMenuClick(getMenuIndex(view.getId()), title[getMenuIndex(view.getId())]);
        }
    }

    private int getMenuIndex(int menuId) {
        switch (menuId) {
            case R.id.llNickName:
                return 0;
            case R.id.llPhone:
                return 1;
            case R.id.llTitle:
                return 2;
        }
        return -1;
    }

    public interface OnMenuClickListener {
        void onMenuClick(int menuIndex, String title);
    }
}
