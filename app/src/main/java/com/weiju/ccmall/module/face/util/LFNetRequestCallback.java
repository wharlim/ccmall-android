package com.weiju.ccmall.module.face.util;


import com.weiju.ccmall.shared.util.Timber;

/**
 * Copyright (c) 2017-2018 LINKFACE Corporation. All rights reserved.
 */

public abstract class LFNetRequestCallback {
    private String TAG = LFNetRequestCallback.class.getSimpleName();

    public abstract void completed(String response);

    /**
     * @param httpStatusCode
     * @param error
     */
    public void failed(int httpStatusCode, String error) {
        String result = "Code" + httpStatusCode + ";error:" + error;
        Timber.INSTANCE.e(result);
    }
}