package com.weiju.ccmall.module.face.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Copyright (c) 2017-2018 LINKFACE Corporation. All rights reserved.
 */

public class LFGetRequestUtils {

    public static final String TAG = LFGetRequestUtils.class.getSimpleName();
    public static String urlPath;
    public static final ExecutorService executorService = Executors
            .newFixedThreadPool(5);

    public static void getRequest(String getUrl, LFNetRequestCallback callback) {
        urlPath = getUrl;
        getSyn(callback);
    }

    public static void getSyn(final LFNetRequestCallback callback) {
        if (urlPath == null || "".equals(urlPath)) {
            sendFailResult(callback, 404, "URL无效");
            return;
        }

        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlPath);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    dealRequestResponse(conn, callback);
                } catch (Exception e) {
                    sendFailResult(callback, 404, "网络请求失败");
                }
            }
        });

    }


    private static void dealRequestResponse(HttpURLConnection conn, final LFNetRequestCallback callback) throws IOException {
        if (conn != null) {
            int code = conn.getResponseCode();
            if (code == 200) {
                InputStream is = conn.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                StringBuffer sb = new StringBuffer();
                char[] buf = new char[1024];
                int len = 0;
                while ((len = reader.read(buf)) != -1) {
                    sb.append(new String(buf, 0, len));
                }
                String result = sb.toString();
                if (result != null) {
                    sendSuccessResult(callback, result);
                } else {
                    sendFailResult(callback, 0, "");
                }
            } else {
                String result = conn.getResponseMessage();
                sendFailResult(callback, code, result);
            }
        } else {
            sendFailResult(callback, 0, "");
        }
    }

    public static <T> void sendFailResult(final LFNetRequestCallback callback, final int errorCode, final String errorString) {
        if (callback != null) {
            callback.failed(errorCode, errorString);
        }
    }

    public static void sendSuccessResult(final LFNetRequestCallback callback, final String response) {

        if (callback != null) {
            callback.completed(response);
        }
    }
}
