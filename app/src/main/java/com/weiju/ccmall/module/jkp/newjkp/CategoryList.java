package com.weiju.ccmall.module.jkp.newjkp;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/4/10.
 */
public class CategoryList {

    /**
     * cid : 1
     * main_name : 女装
     * data : []
     */

    @SerializedName("cid")
    public String cid;
    @SerializedName("mainName")
    public String mainName;
}
