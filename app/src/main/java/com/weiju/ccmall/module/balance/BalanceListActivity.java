package com.weiju.ccmall.module.balance;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.orhanobut.logger.Logger;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.AuthPhoneActivity;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.SubmitStatusActivity;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.auth.model.AuthModel;
import com.weiju.ccmall.module.auth.model.CardDetailModel;
import com.weiju.ccmall.module.deal.DealFirstActivity;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.transfer.StepFirstActivity;
import com.weiju.ccmall.module.user.MoneyTransferToGoldActivity;
import com.weiju.ccmall.module.user.MyContributionSortListActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.AutoAuthStatus;
import com.weiju.ccmall.shared.bean.Balance;
import com.weiju.ccmall.shared.bean.CommonExtra;
import com.weiju.ccmall.shared.bean.DayProfit;
import com.weiju.ccmall.shared.bean.GongMaoAuth;
import com.weiju.ccmall.shared.bean.MyStatus;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.enums.AccountType;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.IBalanceService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ActivityControl;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Chan on 2017/6/17.
 *
 * @author Chan
 * @package com.weiju.ccmall.module.balance
 * @since 2017/6/17 下午4:37
 */

public class BalanceListActivity extends BaseActivity implements PageManager.RequestListener {

    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.noDataLayout)
    protected View mNoDataLayout;
    @BindView(R.id.bottomLayout)
    LinearLayout mBottomLayout;
    @BindView(R.id.transferTv)
    TextView mTransferTv;
    @BindView(R.id.withdrawalTv)
    TextView mWithdrawalTv;
    @BindView(R.id.transferGoldTv)
    TextView mTransferGoldTv;
    @BindView(R.id.tvExch)
    TextView mTvExch;

    private IBalanceService mBalanceService;
    private BalanceAdapter mBalanceAdapter;
    private PageManager mPageManager;
    private AccountType type;
    private int mStatus = -1;
    private MyStatus mMyStatus;

    private int autoAuthStatus;

    private IUserService mService=ServiceManager.getInstance().createService(IUserService.class);

    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
    private CommonExtra mBalanCommonExtra;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_list_layout);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.setStatusBarColor(getResources().getColor(R.color.red));
            QMUIStatusBarHelper.setStatusBarDarkMode(this);
        }

        mBalanceService = ServiceManager.getInstance().createService(IBalanceService.class);
        mBalanceAdapter = new BalanceAdapter(this);
        mRecyclerView.setAdapter(mBalanceAdapter);
        try {

            mPageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(mRecyclerView)
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .setRequestListener(this)
                    .build(this);
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }

        EventBus.getDefault().register(this);
        initIntent();
        initView();
        reloadMyAuthInfo();
//        reloadMyStatus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        reloadMyStatus();
    }

    private void initIntent() {
        try {
            this.type = (AccountType) getIntent().getSerializableExtra("type");
        } catch (ClassCastException e) {
            Logger.e("获取类型失败", e);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        mPageManager.onRefresh();
    }

    private void initView() {
        showHeader();
        setTitle(type.getName());
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_white);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getHeaderLayout().makeHeaderRed();
        mBalanceAdapter.setType(type);

        switch (type) {
            case Balance:
                mBottomLayout.setVisibility(View.VISIBLE);
                break;
            case GoldenTicket:
                mBottomLayout.setVisibility(View.VISIBLE);
                mTransferTv.setVisibility(View.GONE);
                mWithdrawalTv.setVisibility(View.GONE);
                mTransferGoldTv.setVisibility(View.GONE);
                mTvExch.setVisibility(View.VISIBLE);
                break;
            case ShopMoney:
                mBottomLayout.setVisibility(View.VISIBLE);
                mWithdrawalTv.setVisibility(View.GONE);
                mTransferGoldTv.setVisibility(View.GONE);
                break;
            case Profit:
                mBalanceAdapter.setGetDayProfitOnClickListener(onClickListener);
                reloadDayProfitStatus();
                break;
            case CCM:
                getHeaderLayout().setRightText("排行榜");
                getHeaderLayout().setOnRightClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(BalanceListActivity.this, MyContributionSortListActivity.class);
                        intent.putExtra("isCCM", true);
                        startActivity(intent);
                    }
                });
                break;

            default:
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ToastUtil.showLoading(BalanceListActivity.this);
            APIManager.startRequest(mBalanceService.getDayProfit(), new BaseRequestListener<DayProfit>() {
                @Override
                public void onSuccess(DayProfit dayProfit) {
                    ToastUtil.hideLoading();
                    DayProfit dayProfit1 = mBalanceAdapter.getDayProfit();
                    if (dayProfit1 != null) {
                        dayProfit1.status = dayProfit.status;
                        mBalanceAdapter.setDayProfit(dayProfit1);
                    }
                    ToastUtil.success(dayProfit.msg);
                }

                @Override
                public void onError(Throwable e) {
                    super.onE(e);
                    ToastUtil.hideLoading();
                }
            },BalanceListActivity.this);
        }
    };

    private void reloadDayProfitStatus() {
        APIManager.startRequest(mBalanceService.getDayProfitStatus(), new BaseRequestListener<DayProfit>() {
            @Override
            public void onSuccess(DayProfit dayProfit) {
                mBalanceAdapter.setDayProfit(dayProfit);
            }
        },this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Override
    public void nextPage(final int page) {
        APIManager.startRequest(getObaservable(page), new BaseRequestListener<PaginationEntity<Balance, CommonExtra>>(this) {
            @Override
            public void onSuccess(PaginationEntity<Balance, CommonExtra> result) {
                if (page == 1) {
                    mBalanceAdapter.getItems().clear();
                    mBalanCommonExtra = result.ex;
                }
                mPageManager.setLoading(false);
                mPageManager.setTotalPage(result.totalPage);
                mBalanceAdapter.setHeaderData(result.ex);
                mBalanceAdapter.addItems(result.list);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onComplete() {
                super.onComplete();
                mRefreshLayout.setRefreshing(false);
            }
        },this);
    }

    private Observable<RequestResult<PaginationEntity<Balance, CommonExtra>>> getObaservable(int page) {
        switch (type) {
            case Balance:
                return mBalanceService.getBalanceList(page);
            case CCM:
                return mBalanceService.getCoinList(page);
            case CB:
                return mBalanceService.getTicketlist(page);
            case Profit:
                return mBalanceService.getProfitList(page);
            case ShopMoney:
                return mBalanceService.getGoldList(page);
            case GoldenTicket:
                return mBalanceService.getGoldTicketList(page);
            case PSP:
                return mBalanceService.getContributionList(page, "psp");
            case TSP:
                return mBalanceService.getContributionList(page, "tsp");
            case PCP:
                return mBalanceService.getContributionList(page, "pcp");
            default:
                return mBalanceService.getTicketlist(page);
        }
    }

    @OnClick(R.id.transferTv)
    public void onMTransferTvClicked() {
        // 转账
        UserService.checkHasPassword(this, new UserService.HasPasswordListener() {
            @Override
            public void onHasPassword() {
                Intent intent = new Intent(BalanceListActivity.this, StepFirstActivity.class);
                intent.putExtra("AccountType", type);
                startActivity(intent);
            }
        });
    }

    @OnClick(R.id.tvExch)
    public void exch() {
        Intent intent = new Intent(BalanceListActivity.this, ExchGoldenTicketActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.transferGoldTv)
    protected void transferGold() {
        UserService.checkHasPassword(this, new UserService.HasPasswordListener() {
            @Override
            public void onHasPassword() {
                Intent intent = new Intent(BalanceListActivity.this, MoneyTransferToGoldActivity.class);
                intent.putExtra("money", mBalanCommonExtra.availableMoney);
                startActivity(intent);
            }
        });
    }

    @OnClick(R.id.withdrawalTv)
    public void onMWithdrawalTvClicked() {
        // 提现
        UserService.checkHasPassword(this, new UserService.HasPasswordListener() {
            @Override
            public void onHasPassword() {


                if(2==autoAuthStatus){
                    startActivity(new Intent(BalanceListActivity.this, DealFirstActivity.class));
                }else if(1==autoAuthStatus){
                    startActivity(new Intent(BalanceListActivity.this, SubmitStatusActivity.class));
                    MsgStatus msgStatus = new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_FAIL);
//                                        msgStatus.setTips(autoAuthStatusRequestResult.data.);
                    EventBus.getDefault().postSticky(msgStatus);
                    return;
                }else if(0==autoAuthStatus){
                    Intent intent2 = new Intent(BalanceListActivity.this, AuthPhoneActivity.class);
                    SharedPreferences sharedPreferences=getSharedPreferences("authType",0);
                    sharedPreferences.edit().putString("authType","balance").commit();
                    ActivityControl.getInstance().add(BalanceListActivity.this);
                    intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
                    startActivity(intent2);
                    return;
                }

                startActivity(new Intent(BalanceListActivity.this, DealFirstActivity.class));

                if (MyApplication.isGongCat) {
                    if (mStatus == AppTypes.CARD_STATUS_GONGCAT.SUCESS) {
                        startActivity(new Intent(BalanceListActivity.this, DealFirstActivity.class));
                    } else {
                        ToastUtil.error("未绑定银行卡，无法提现");
                    }
                } else {




                }

            }
        });
    }

    private void getGongMallAuth() {
        ToastUtil.showLoading(BalanceListActivity.this);
        APIManager.startRequest(mUserService.getGongMallAuth(), new BaseRequestListener<GongMaoAuth>() {
            @Override
            public void onSuccess(GongMaoAuth model) {
                ToastUtil.hideLoading();
                Intent intent = new Intent(BalanceListActivity.this, WebViewJavaActivity.class);
                intent.putExtra("url", model.url);
                startActivity(intent);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.hideLoading();
            }
        },this);
    }

    /**
     * 绑定银行卡
     */
    private void bindCard() {
        switch (mMyStatus.bindBankStatus) {
            case AppTypes.CARD_STATUS.NO_SUBMIT:
                Intent intent2 = new Intent(BalanceListActivity.this, AuthPhoneActivity.class);
                SharedPreferences sharedPreferences=getSharedPreferences("authType",0);
                sharedPreferences.edit().putString("authType","balance").commit();
                intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_CARD);
                startActivity(intent2);
                break;
            case AppTypes.CARD_STATUS.WAIT:
                startActivity(new Intent(BalanceListActivity.this, SubmitStatusActivity.class));
                EventBus.getDefault().postSticky(new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_CARD_WAIT));
                break;
            case AppTypes.CARD_STATUS.FAIL:
                APIManager.startRequest(mUserService.getCard(), new BaseRequestListener<CardDetailModel>(mRefreshLayout) {
                    @Override
                    public void onSuccess(CardDetailModel model) {
                        startActivity(new Intent(BalanceListActivity.this, SubmitStatusActivity.class));
                        MsgStatus msgStatus = new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_CARD_FAIL);
                        msgStatus.setTips(model.checkResult);
                        EventBus.getDefault().postSticky(msgStatus);
                    }
                },this);
                break;
            default:
                break;
        }
    }

    /**
     * 实名认证
     */
    private void goAuth() {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        switch (loginUser.autoAuthStatus) {
            case AppTypes.AUTH_STATUS.FAIL:
                APIManager.startRequest(mUserService.getAuth(), new BaseRequestListener<AuthModel>(mRefreshLayout) {
                    @Override
                    public void onSuccess(AuthModel model) {
                        startActivity(new Intent(BalanceListActivity.this, SubmitStatusActivity.class));
                        MsgStatus msgStatus = new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_FAIL);
                        msgStatus.setTips(model.memberAuthBean.checkRemark);
                        EventBus.getDefault().postSticky(msgStatus);
                    }
                },this);
                break;
            case AppTypes.AUTH_STATUS.NO_SUBMIT:
                Intent intent2 = new Intent(BalanceListActivity.this, AuthPhoneActivity.class);
                SharedPreferences sharedPreferences=getSharedPreferences("authType",0);
                sharedPreferences.edit().putString("authType","balance").commit();
                intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
                startActivity(intent2);
                break;
            default:
                break;
        }
    }

    private void reloadMyAuthInfo() {
        Observable<RequestResult<CardDetailModel>> gongCatCard = MyApplication.isGongCat ?
                mUserService.getGongCatCard(SessionUtil.getInstance().getOAuthToken()) :
                mUserService.getCard();
        APIManager.startRequest(gongCatCard, new BaseRequestListener<CardDetailModel>() {
            @Override
            public void onSuccess(CardDetailModel model) {
                if (!TextUtils.isEmpty(model.status)) {
                    mStatus = Integer.parseInt(model.status);
                }
            }

            @Override
            public void onError(Throwable e) {

            }
        },this);

    }

    private void reloadMyStatus() {
        APIManager.startRequest(mUserService.getMyStatus(), new BaseRequestListener<MyStatus>(mRefreshLayout) {
            @Override
            public void onSuccess(MyStatus myStatus) {
                mMyStatus = myStatus;
            }
        },this);

        APIManager.startRequest(mService.getStatus(), new Observer<RequestResult<AutoAuthStatus>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(RequestResult<AutoAuthStatus> autoAuthStatusRequestResult) {
                AutoAuthStatus autoAuthStatus=autoAuthStatusRequestResult.data;
                if (autoAuthStatusRequestResult.isSuccess()) {
                    BalanceListActivity.this.autoAuthStatus=autoAuthStatus.autoAuthStatus;
            }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(MsgStatus status) {
        switch (status.getAction()) {
            case MsgStatus.ACTION_DEAL_SUCESS:
                mPageManager.onRefresh();
                break;
            default:
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void transferHandler(EventMessage message) {
        switch (message.getEvent()) {
            case transferSuccess:
            case exchSuccess:
                mPageManager.onRefresh();
                break;
            default:
        }

    }
}
