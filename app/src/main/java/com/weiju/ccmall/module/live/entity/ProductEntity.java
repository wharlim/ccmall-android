package com.weiju.ccmall.module.live.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/4/18.
 */
public class ProductEntity {

    /**
     * productId : b3ae73968a8944f98452cfac3e250320
     * storeId : 0bf82e68134e4388ad06fe5c776f6065
     * brandId : 3d3168a18c8c4816b357e33c8e068e73
     * productName : 直播开店1
     * minPrice : 0
     * maxPrice : 0
     * unitMesure :
     * unitPack :
     * unitRatio :
     * saleCount : 0
     * status : 1
     * sellBegin :
     * sellEnd :
     * countryId : 4e54696e2397470d9faf9dd2f35e2574
     * content : <img src="https://static.create-chain.net/ccmall/6d/af/b5/8d8a66599ecf4680a86a508b0be7cf04.jpg" alt="" />
     * coverUrl :
     * mediaUrl :
     * hasPresent : 0
     * hasCoupon : 0
     * type : 28
     * extType : 0
     * viewCount : 0
     * isShippingFree : 1
     * freeQuantity : 2
     * buyDayQuantity : 0
     * createDate : 2019-09-29 14:29:11
     * isCross : 0
     * categoryId :
     * categoryName :
     * productSource : 0
     * intro : 产品特点
     蕴含北美金缕梅、黄芩等多种植物精华，净化肌肤毛孔同时为肌肤提供充足水分和营养。有助于改善因肌肤缺水引发的干枯、粗糙、干纹等状况。
     新疆、内蒙、西藏不包邮加20元邮费
     * syncStatus : 0
     * warehouseId :
     * serviceFee :
     * virtual : 0
     * xibeiFreeShipMoneyStatus : 0
     * keyWord :
     * promptId :
     * materialCode : 01.02.000011
     * cache_flag : 0
     * thumbUrl : https://static.create-chain.net/ccmall/8c/6e/c8/812ae7a1117f468caeba750eecac6a29.jpg
     * bannerImage : ["https://static.create-chain.net/ccmall/36/5a/a3/53e68a2f87ab4ee884b74e4b9ceb328d.png","https://static.create-chain.net/ccmall/c7/ca/ec/7819cf24436a4e7e8c0a69e215b64918.jpg"]
     * retailPrice : 3800
     * stock : 50
     */

    @SerializedName("productId")
    public String productId;
    @SerializedName("storeId")
    public String storeId;
    @SerializedName("brandId")
    public String brandId;
    @SerializedName("productName")
    public String productName;
    @SerializedName("minPrice")
    public int minPrice;
    @SerializedName("maxPrice")
    public int maxPrice;
    @SerializedName("unitMesure")
    public String unitMesure;
    @SerializedName("unitPack")
    public String unitPack;
    @SerializedName("unitRatio")
    public String unitRatio;
    @SerializedName("saleCount")
    public int saleCount;
    @SerializedName("status")
    public int status;
    @SerializedName("sellBegin")
    public String sellBegin;
    @SerializedName("sellEnd")
    public String sellEnd;
    @SerializedName("countryId")
    public String countryId;
    @SerializedName("content")
    public String content;
    @SerializedName("coverUrl")
    public String coverUrl;
    @SerializedName("mediaUrl")
    public String mediaUrl;
    @SerializedName("hasPresent")
    public int hasPresent;
    @SerializedName("hasCoupon")
    public int hasCoupon;
    @SerializedName("type")
    public int type;
    @SerializedName("extType")
    public int extType;
    @SerializedName("viewCount")
    public int viewCount;
    @SerializedName("isShippingFree")
    public int isShippingFree;
    @SerializedName("freeQuantity")
    public int freeQuantity;
    @SerializedName("buyDayQuantity")
    public int buyDayQuantity;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("isCross")
    public int isCross;
    @SerializedName("categoryId")
    public String categoryId;
    @SerializedName("categoryName")
    public String categoryName;
    @SerializedName("productSource")
    public String productSource;
    @SerializedName("intro")
    public String intro;
    @SerializedName("syncStatus")
    public int syncStatus;
    @SerializedName("warehouseId")
    public String warehouseId;
    @SerializedName("serviceFee")
    public String serviceFee;
    @SerializedName("virtual")
    public int virtual;
    @SerializedName("xibeiFreeShipMoneyStatus")
    public int xibeiFreeShipMoneyStatus;
    @SerializedName("keyWord")
    public String keyWord;
    @SerializedName("promptId")
    public String promptId;
    @SerializedName("materialCode")
    public String materialCode;
    @SerializedName("cache_flag")
    public int cacheFlag;
    @SerializedName("thumbUrl")
    public String thumbUrl;
    @SerializedName("bannerImage")
    public String bannerImage;
    @SerializedName("retailPrice")
    public int retailPrice;
    @SerializedName("stock")
    public int stock;

    /**
     * skuId : c2881f89874b4a57b5cc3b2c0bea6797
     */

    @SerializedName("skuId")
    public String skuId;
}
