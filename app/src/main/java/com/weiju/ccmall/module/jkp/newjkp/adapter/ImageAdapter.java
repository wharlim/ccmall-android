package com.weiju.ccmall.module.jkp.newjkp.adapter;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.blankj.utilcode.utils.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.jkp.newjkp.entity.BannerEntity;
import com.weiju.ccmall.shared.util.FrescoUtil;

public class ImageAdapter extends BaseQuickAdapter<BannerEntity.Bean, BaseViewHolder> {

    public ImageAdapter() {
        super(R.layout.el_spacer_layout);
    }

    @Override
    protected void convert(BaseViewHolder helper, BannerEntity.Bean item) {
        SimpleDraweeView simpleDraweeView = helper.getView(R.id.eleBackgroundIv);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, SizeUtils.dp2px(72));
        simpleDraweeView.setLayoutParams(layoutParams);
        simpleDraweeView.setVisibility(View.VISIBLE);
        FrescoUtil.setImageSmall(simpleDraweeView, item.image);
    }
}