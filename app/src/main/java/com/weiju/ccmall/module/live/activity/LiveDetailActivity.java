package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.module.live.fragment.LiveDetailOverflowMenuFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveData;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.ColorFilterTransformation;

public class LiveDetailActivity extends BaseActivity {
    @BindView(R.id.ivVideoBg)
    ImageView ivVideoBg;
    @BindView(R.id.ivVideo)
    ImageView ivVideo;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.layoutTopTitle)
    FrameLayout layoutTopTitle;
    @BindView(R.id.tvLiveTitle)
    TextView tvLiveTitle;
    @BindView(R.id.tvDuration)
    TextView tvDuration;
    @BindView(R.id.tvTimeRange)
    TextView tvTimeRange;
    @BindView(R.id.tvWatchPeopleCount)
    TextView tvWatchPeopleCount;
    @BindView(R.id.tvActiveCount)
    TextView tvActiveCount;
    @BindView(R.id.tvSharedCount)
    TextView tvSharedCount;
    @BindView(R.id.tvClickedCount)
    TextView tvClickedCount;
    @BindView(R.id.tvSaleOrderCount)
    TextView tvSaleOrderCount;
    @BindView(R.id.tvSaleAmount)
    TextView tvSaleAmount;
    @BindView(R.id.tvCommOrderCount)
    TextView tvCommOrderCount;
    @BindView(R.id.tvCommAmount)
    TextView tvCommAmount;

    private LiveRoom liveRoom;
    private LiveData liveData;
    private User user;
    private ILiveService service = ServiceManager.getInstance().createService(ILiveService.class);

    public static void start(Context context, LiveRoom liveRoom) {
        Intent intent = new Intent(context, LiveDetailActivity.class);
        intent.putExtra("liveRoom", liveRoom);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        liveRoom = (LiveRoom) getIntent().getSerializableExtra("liveRoom");
        super.onCreate(savedInstanceState);
        user = SessionUtil.getInstance().getLoginUser();
        if (user == null) {
            finish();
            return;
        }
        setContentView(R.layout.activity_live_detail);
        ButterKnife.bind(this);
        initStatusBar();
        getLiveCount(liveRoom.liveId);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    private void getLiveCount(String liveId) {
        APIManager.startRequest(service.getCount(liveId), new BaseRequestListener<LiveData>() {
            @Override
            public void onSuccess(LiveData result) {
                initLiveData(result);
            }
        }, this);
    }

    private void initLiveData(LiveData data) {
        liveData = data;
        loadBg(Glide.with(LiveDetailActivity.this)
                .load(data.liveImage));
        if (liveRoom.status == 2) {
            ivVideo.setVisibility(View.GONE);
            LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) tvLiveTitle.getLayoutParams();
            lp.topMargin = SizeUtils.dp2px(40);
            tvLiveTitle.setLayoutParams(lp);
            tvLiveTitle.setTextSize(20);
            tvLiveTitle.setText("直播结束，回放生成中");
        } else {
            tvLiveTitle.setText(data.title);
        }
//        tvDuration.setText("时长：" + DateUtils.getVoiceTime(data.time+""));
        tvDuration.setText("时长：" + data.time + "分");
        String startTime = data.startTime;
        try {
            startTime = startTime.replace("-","/").substring(5, 16);
        } catch (Exception e) {

        }
        String endTime = data.endTime;
        try {
            endTime = endTime.replace("-","/").substring(5, 16);
        } catch (Exception e) {
        }
        if (!TextUtils.isEmpty(startTime) && !TextUtils.isEmpty(endTime)) {
            tvTimeRange.setText(startTime + " - " + endTime);
        }
        tvWatchPeopleCount.setText(data.joinliveMember + "");
        tvActiveCount.setText(data.interactionCount+"");
        tvSharedCount.setText(data.shareCount + "");
        tvClickedCount.setText(data.goodClickCount+"");
        //
        tvSaleOrderCount.setText(data.saleCount + "");
        tvSaleAmount.setText(MoneyUtil.centToYuanStrNoZero(data.saleMoney));
        tvCommOrderCount.setText(data.profitCount+"");
        tvCommAmount.setText(MoneyUtil.centToYuanStrNoZero(data.predictProfitMoney));
    }

    private void initStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            QMUIStatusBarHelper.translucent(this);
        }
    }

    private void loadBg(RequestBuilder<Drawable> rb) {
        rb.apply(new RequestOptions().placeholder(R.drawable.img_create_live_room_bg))
                .apply(RequestOptions.bitmapTransform(new MultiTransformation<>(
                new CenterCrop(),
                new ColorFilterTransformation(Color.parseColor("#BA000000"))
        )))
                .into(new SimpleTarget<Drawable>(ivVideoBg.getMeasuredWidth(), ivVideoBg.getMeasuredHeight()) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        ivVideoBg.setImageDrawable(resource);
                    }

                    @Override
                    public void onLoadStarted(@Nullable Drawable placeholder) {
                        ivVideoBg.setImageDrawable(placeholder);
                    }
                });
    }

    @OnClick({R.id.ivVideoBg, R.id.back, R.id.overflowMenu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivVideoBg:
                if (liveRoom.status != 2) LiveManager.toLiveRoom(this, liveRoom);
                break;
            case R.id.back:
                finish();
                break;
            case R.id.overflowMenu:
                showBottomSheetMenu();
                break;
        }
    }

    private void showBottomSheetMenu() {
        LiveDetailOverflowMenuFragment ovMenu = LiveDetailOverflowMenuFragment.newInstance(liveRoom, liveData);
        ovMenu.setCallback(() -> {
            APIManager.startRequest(service.clearLivePassword(liveRoom.liveId), new BaseRequestListener<Object>() {
                @Override
                public void onSuccess(Object result) {
                    ToastUtil.success("密码清除成功！");
                    liveRoom.livePassword = "";
                    liveData.hasLivePasswordDecode = false;
                    EventBus.getDefault().post(new EventMessage(Event.liveRoomChange));
                }
            }, this);
        });
        ovMenu.show(getSupportFragmentManager(), "LiveDetailOverflowMenuFragment");
    }

    @Subscribe
    public void onLiveChange(EventMessage ev) {
        if (ev.getEvent() == Event.liveRoomChange) {
            if (ev.getData() instanceof LiveRoom) {
                this.liveRoom = (LiveRoom) ev.getData();
            }
        }
    }
}
