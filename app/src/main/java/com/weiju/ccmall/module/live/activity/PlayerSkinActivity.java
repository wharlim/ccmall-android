package com.weiju.ccmall.module.live.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.aliyun.player.AliPlayer;
import com.aliyun.player.IPlayer;
import com.aliyun.player.bean.ErrorCode;
import com.aliyun.player.nativeclass.MediaInfo;
import com.aliyun.player.source.UrlSource;
import com.aliyun.player.source.VidSts;
import com.aliyun.private_service.PrivateService;
import com.aliyun.utils.VcPlayerLog;
import com.aliyun.vodplayerview.activity.AliyunPlayerSkinActivity;
import com.aliyun.vodplayerview.constants.PlayParameter;
import com.aliyun.vodplayerview.listener.OnChangeQualityListener;
import com.aliyun.vodplayerview.listener.OnStoppedListener;
import com.aliyun.vodplayerview.listener.RefreshStsCallback;
import com.aliyun.vodplayerview.utils.Common;
import com.aliyun.vodplayerview.utils.FixedToastUtils;
import com.aliyun.vodplayerview.utils.VidStsUtil;
import com.aliyun.vodplayerview.utils.database.DatabaseManager;
import com.aliyun.vodplayerview.utils.database.LoadDbDatasListener;
import com.aliyun.vodplayerview.utils.download.AliyunDownloadInfoListener;
import com.aliyun.vodplayerview.utils.download.AliyunDownloadManager;
import com.aliyun.vodplayerview.utils.download.AliyunDownloadMediaInfo;
import com.aliyun.vodplayerview.view.choice.AlivcShowMoreDialog;
import com.aliyun.vodplayerview.view.control.ControlView;
import com.aliyun.vodplayerview.view.download.AddDownloadView;
import com.aliyun.vodplayerview.view.download.AlivcDialog;
import com.aliyun.vodplayerview.view.download.AlivcDownloadMediaInfo;
import com.aliyun.vodplayerview.view.download.DownloadChoiceDialog;
import com.aliyun.vodplayerview.view.download.DownloadDataProvider;
import com.aliyun.vodplayerview.view.download.DownloadView;
import com.aliyun.vodplayerview.view.gesturedialog.BrightnessDialog;
import com.aliyun.vodplayerview.view.more.AliyunShowMoreValue;
import com.aliyun.vodplayerview.view.more.ShowMoreView;
import com.aliyun.vodplayerview.view.more.SpeedValue;
import com.aliyun.vodplayerview.view.tipsview.ErrorInfo;
import com.aliyun.vodplayerview.widget.AliyunScreenMode;
import com.aliyun.vodplayerview.widget.AliyunVodPlayerView;
import com.blankj.utilcode.utils.ScreenUtils;
import com.blankj.utilcode.utils.SizeUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.module.live.adapter.LiveChatMessageAdapter;
import com.weiju.ccmall.module.live.adapter.LiveHorizontalUserAdapter;
import com.weiju.ccmall.module.live.widgets.FloatingIv;
import com.weiju.ccmall.module.live.widgets.FloatingWindow;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.BanningMessage;
import com.weiju.ccmall.shared.bean.LiveCoupon;
import com.weiju.ccmall.shared.bean.LiveMessage;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.LivingRoomHotDataMsg;
import com.weiju.ccmall.shared.bean.RecordUrl;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.MaxHeightRecyclerView;
import com.weiju.ccmall.shared.component.dialog.LiveCouponListDialog;
import com.weiju.ccmall.shared.component.dialog.LiveCouponListNoDataDialog;
import com.weiju.ccmall.shared.component.dialog.LiveCouponOpenDialog;
import com.weiju.ccmall.shared.component.dialog.LiveProductDialog;
import com.weiju.ccmall.shared.component.dialog.LiveRedListDialog;
import com.weiju.ccmall.shared.component.dialog.LiveRedOpenDialog;
import com.weiju.ccmall.shared.component.dialog.LiveSendMessageDialog;
import com.weiju.ccmall.shared.component.dialog.LiveShareDialog;
import com.weiju.ccmall.shared.component.dialog.LiveUserDialog;
import com.weiju.ccmall.shared.component.dialog.LiveUserInfoDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PopupWindowManage;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.message.ChatroomCouponMessage;
import com.weiju.ccmall.shared.message.ChatroomNoticeMessage;
import com.weiju.ccmall.shared.message.ChatroomRedMessage;
import com.weiju.ccmall.shared.message.ChatroomStatusMessage;
import com.weiju.ccmall.shared.message.ChatroomTextMessage;
import com.weiju.ccmall.shared.message.ChatroomUserQuit;
import com.weiju.ccmall.shared.message.ChatroomWelcomeMessage;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ChatroomKit;
import com.weiju.ccmall.shared.util.DataInterface;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.SoftKeyBoardListener;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.rong.imlib.RongIMClient;
import io.rong.message.CommandNotificationMessage;
import io.rong.message.TextMessage;
import xiao.free.horizontalrefreshlayout.HorizontalRefreshLayout;
import xiao.free.horizontalrefreshlayout.RefreshCallBack;
import xiao.free.horizontalrefreshlayout.refreshhead.LoadingRefreshHeader;

/**
 * @author chenyanming
 * @time 2019/12/23 on 9:47
 * @desc 3
 */
public class PlayerSkinActivity extends AliyunPlayerSkinActivity {

    @BindView(R.id.floatingIv)
    FloatingIv floatingIv;
    @BindView(R.id.ivGoods)
    SimpleDraweeView ivGoods;
    @BindView(R.id.ivCloseGoods)
    ImageView ivCloseGoods;
    @BindView(R.id.tvGoodsName)
    TextView tvGoodsName;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.rlGoods)
    RelativeLayout rlGoods;
    private PlayerHandler playerHandler;
    private DownloadView dialogDownloadView;
    private AlivcShowMoreDialog showMoreDialog;

    private SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss.SS");
    private List<String> logStrs = new ArrayList<>();

    private AliyunScreenMode currentScreenMode = AliyunScreenMode.Small;

    private AliyunVodPlayerView mAliyunVodPlayerView = null;

    private DownloadDataProvider downloadDataProvider;
    private AliyunDownloadManager downloadManager;

    private ErrorInfo currentError = ErrorInfo.Normal;
    //判断是否在后台
    private boolean mIsInBackground = false;
    /**
     * 开启设置界面的请求码
     */
    private static final int CODE_REQUEST_SETTING = 1000;

    /**
     * get StsToken stats
     */
    private boolean inRequest;

    private static String[] PERMISSIONS_STORAGE = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE"
    };

    private static final int REQUEST_EXTERNAL_STORAGE = 1;

    /**
     * 当前tab
     */
    private Common commenUtils;
    private long oldTime;
    private long downloadOldTime;
    private static String preparedVid;

    private AliyunScreenMode mCurrentDownloadScreenMode;

    /**
     * 是否需要展示下载界面,如果是恢复数据,则不用展示下载界面
     */
    private boolean showAddDownloadView;

    /**
     * 是否鉴权过期
     */
    private boolean mIsTimeExpired = false;
    /**
     * 判断是否在下载中
     */
    private boolean mDownloadInPrepare = false;


    @BindView(R.id.top_bar_layout)
    LinearLayout mLayoutTopBar;
    @BindView(R.id.ivUser1)
    SimpleDraweeView mIvUserOne;
    @BindView(R.id.ivUser2)
    SimpleDraweeView mIvUserTwo;
    @BindView(R.id.ivUser3)
    SimpleDraweeView mIvUserThree;
    @BindView(R.id.ivUser4)
    SimpleDraweeView mIvUserFour;
    @BindView(R.id.tvLiveUser)
    TextView mTvLiveUser;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvPopularity)
    TextView mTvPopularity;
    @BindView(R.id.ivAvatar)
    SimpleDraweeView mIvAvatar;
    @BindView(R.id.ivLike)
    ImageView mIvLike;
    @BindView(R.id.layoutMessage)
    LinearLayout mLayoutMessage;
    @BindView(R.id.layoutFollow)
    LinearLayout mLayoutFollow;
    @BindView(R.id.tvFollow)
    TextView mTvFollow;
    @BindView(R.id.ivFollow)
    ImageView mIvFollow;
    @BindView(R.id.layoutSelectQingXi)
    RelativeLayout mLayoutSelectQingXi;
    @BindView(R.id.tvLiuChang)
    TextView mTvLiuChang;
    @BindView(R.id.tvGaoQing)
    TextView mTvGaoQing;
    @BindView(R.id.tvChaoQing)
    TextView mTvChaoQing;
    @BindView(R.id.tvQingXi)
    TextView mTvQingXi;
    @BindView(R.id.recyclerView)
    MaxHeightRecyclerView mMessageRecyclerView;

    @BindView(R.id.ivRedList)
    ImageView mIvRedList;
    @BindView(R.id.viewMoreLine)
    View mViewMoreLine;
    @BindView(R.id.ivCouponList)
    ImageView mIvCouponList;

    @BindView(R.id.rv_selections)
    RecyclerView mRecycler;
    @BindView(R.id.refresh)
    HorizontalRefreshLayout refreshLayout;
    @BindView(R.id.ivMore)
    ImageView mIvMore;
    @BindView(R.id.spacer)
    View spacer;
    @BindView(R.id.rlNoLogin)
    RelativeLayout rlNoLogin;

    //播放地址
    private String mPlayUrl;

    //房间Id
    private String mLiveId;

    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);
    private IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
    private WJDialog mOutLiveDialog;

    //观众弹窗
    private LiveUserDialog mLiveUserDialog;
    private String mMemberId;
    //分享弹窗
    private LiveShareDialog mLiveShareDialog;
    private LiveRoom mLiveRoom;
    private int mStatus;
    private int currentVideoPosition;
    private LiveUser mLiveUser;

    //直播中更多操作弹窗
    private PopupWindowManage mMoreWindowManage;
    private View mMorePopupView;

    //主播信息弹窗
    private LiveUserInfoDialog mLiveUserInfoDialog;
    //商品弹窗
    private LiveProductDialog mLiveProductDialog;
    private LiveChatMessageAdapter mLiveChatMessageAdapter;
    private Random random = new Random();

    private boolean mShowRedDialog = false;
    private boolean mShowCouponDialog = false;

    private LiveHorizontalUserAdapter mLiveHorizontalUserAdapter = new LiveHorizontalUserAdapter();


    private Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case ChatroomKit.MESSAGE_ARRIVED:
                case ChatroomKit.MESSAGE_SENT:

                    io.rong.imlib.model.Message message = (io.rong.imlib.model.Message) msg.obj;
                    if (!message.getTargetId().equals(mLiveId)) {
                        return false;
                    }
                    String objectName = message.getObjectName();
                    switch (objectName) {
                        case "RC:TxtMsg":
//                                文本消息
                            getTextMessage(message, false);
                            break;
                        case "RC:CmdNtf":
                            //刷新观众列表
                            Log.d("Seven", "RC:CmdNtf");
                            mUserCurrentPage = 1;
                            getLiveAudienceUserList();

                            CommandNotificationMessage cmdMsg = (CommandNotificationMessage) message.getContent();
                            if ("LivingRoomHotDataMsg".equals(cmdMsg.getName())) {
                                LivingRoomHotDataMsg livingRoomHotDataMsg = null;
                                try {
                                    livingRoomHotDataMsg = new Gson().fromJson(cmdMsg.getData(), LivingRoomHotDataMsg.class);
                                    LiveMessage liveMessage = new LiveMessage();
                                    liveMessage.memberId = message.getSenderUserId();
                                    liveMessage.name = livingRoomHotDataMsg.systemChatMsg.nickname;
                                    liveMessage.isWelcome = true;
                                    liveMessage.content = livingRoomHotDataMsg.systemChatMsg.content;
                                    mLiveChatMessageAdapter.addData(liveMessage);
                                    mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);
                                    // 人气
                                    mTvPopularity.setText(String.format("人气：%s", livingRoomHotDataMsg.popularIndex));
                                } catch (Exception e) {
                                    Log.e("Seven", "error! message: " + cmdMsg.getData() + ";e: " + e.getMessage());
                                }
                            }
                            break;
                        case "CCM:Chatroom:Text:Message":
                            if (mStatus != 1) {
                                return false;
                            }
                            ChatroomTextMessage contentText = (ChatroomTextMessage) message.getContent();
                            boolean isLoginUser = message.getSenderUserId().equals(mLiveRoom.memberId);
                            LiveMessage liveMessageText = new LiveMessage();
                            liveMessageText.memberId = message.getSenderUserId();
                            liveMessageText.name = contentText.getNickName();
                            liveMessageText.headImage = contentText.getHeadImage();
                            liveMessageText.showAvatar = isLoginUser;
                            liveMessageText.content = contentText.getContent();
                            mLiveChatMessageAdapter.addData(liveMessageText);
                            mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);

                            break;
                        case "CCM:Chatroom:Red:Envelope":
                            //红包
                            if (mStatus != 1) {
                                return false;
                            }
                            if (!mShowRedDialog) {
                                mShowRedDialog = true;
                                ChatroomRedMessage content = (ChatroomRedMessage) message.getContent();
                                String red = content.getRed();
                                LiveRedOpenDialog liveRedOpenDialog = new LiveRedOpenDialog(PlayerSkinActivity.this, mLiveId, red, mLiveUser);
                                liveRedOpenDialog.show();
                            } else {
                                ToastUtil.success("你收到一个新红包");
                            }
                            break;
                        case "CCM:Chatroom:Coupon":
                            //收到优惠券
                            if (mStatus != 1) {
                                return false;
                            }
                            if (!mShowCouponDialog) {
                                mShowCouponDialog = true;
                                ChatroomCouponMessage content = (ChatroomCouponMessage) message.getContent();
                                String couponId = content.getCouponId();
                                String sendId = content.getSendId();

                                LiveCouponOpenDialog liveRedOpenDialog = new LiveCouponOpenDialog(PlayerSkinActivity.this, mLiveId, sendId, couponId, mLiveUser.headImage);
                                liveRedOpenDialog.show();
                            } else {
                                ToastUtil.success("你收到一个新优惠券");
                            }

                            break;
                        case "CCM:Chatroom:Welcome":
                            //进入房间
                            if (mStatus != 1) {
                                return false;
                            }
                            ChatroomWelcomeMessage content = (ChatroomWelcomeMessage) message.getContent();
                            mLiveUserTotal = Math.max(content.getAudienceNum(), mLiveUserTotal);
                            if (mLiveUserTotal <= 5) {
                                // getLiveAudienceList(true, content);
                            } else {
                                mTvLiveUser.setText(String.format("观众\n%s", mLiveUserTotal));

                                LiveMessage liveMessage = new LiveMessage();
                                liveMessage.memberId = message.getSenderUserId();
                                liveMessage.name = content.getNickName();
                                liveMessage.headImage = content.getHeadImage();
                                liveMessage.isWelcome = true;
                                liveMessage.content = String.format("等%s位进入房间", mLiveUserTotal);
                                mLiveChatMessageAdapter.addData(liveMessage);
                                mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);

                            }

                            //刷新观众列表
                            mUserCurrentPage = 1;
                            getLiveAudienceUserList();

                            break;
                        case "CCM:Chatroom:Live:End":
                            String memberId = message.getSenderUserId();
                            if (!mIsLive && mLiveRoom.memberId.equals(memberId)) {
                                mIsLive = true;
                                LiveManager.toLiveEnd(PlayerSkinActivity.this, mLiveId, mLiveUser.headImage,
                                        mLiveUser.nickName, true, mLiveUser.followStatus);
                                finish();
                            }
                            break;

                        case "CCM:Chatroom:Live:Notice":
                            ChatroomNoticeMessage notice = (ChatroomNoticeMessage) message.getContent();
                            LiveMessage liveMessage = new LiveMessage();
                            liveMessage.memberId = message.getSenderUserId();
                            liveMessage.isNotice = true;
                            liveMessage.content = notice.getNotice();
                            mLiveChatMessageAdapter.addData(liveMessage);
                            mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);
                            break;
                        case "CCM:Chatroom:Status:Message":
                            ChatroomStatusMessage statusMessage = (ChatroomStatusMessage) message.getContent();


                            break;
                        case "CCM:Chatroom:Goods:Explain":
                            getLiveSku();
                            break;
                        default:
                    }

                    break;

                case ChatroomKit.MESSAGE_SEND_ERROR:
                    break;

                default:
            }
            return false;
        }
    });
    private int mOnlineNum;
    private int mFansNum;
    private int mLikeNum;
    private long banStartTime;
    private LiveSendMessageDialog mLiveSendMessageDialog;
    private int mLiveUserTotal;
    private boolean mIsNeetReconnect;
    private int mUserCurrentPage = 1;
    private boolean mIsRefreshLayout;
    //打开直播结束标识
    private boolean mIsLive = false;
    private RelativeLayout.LayoutParams mMessageLayoutParams;
    private String mSkuId;


    private void getTextMessage(io.rong.imlib.model.Message message, boolean showAvatar) {
        if (mStatus != 1) {
            return;
        }
        APIManager.startRequest(mUserService.getMemberInfoById(message.getSenderUserId()), new BaseRequestListener<User>() {
            @Override
            public void onSuccess(User result) {
                boolean isLoginUser = result.id.equals(mLiveRoom.memberId);
                LiveMessage liveMessage = new LiveMessage();
                liveMessage.memberId = message.getSenderUserId();
                liveMessage.name = result.nickname;
                liveMessage.headImage = result.avatar;
                liveMessage.showAvatar = showAvatar || isLoginUser;
                liveMessage.content = (showAvatar && !isLoginUser) || message.getContent() instanceof ChatroomWelcomeMessage ?
                        String.format("%s 进入房间", liveMessage.name) : ((TextMessage) (message.getContent())).getContent();
                mLiveChatMessageAdapter.addData(liveMessage);
                mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);
            }
        }, PlayerSkinActivity.this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.NoActionTheme);
        copyAssets();
        DatabaseManager.getInstance().createDataBase(this);

        super.onCreate(savedInstanceState);
        showAddDownloadView = false;

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_player_layout_skin);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);


        mLiveId = getIntent().getStringExtra("liveId");
        mMemberId = getIntent().getStringExtra("memberId");
        mStatus = getIntent().getIntExtra("status", 1);
        initView();
        initData();
        if (mStatus == 1) {
            getSysNote();
            getLiveSku();
        }
        initRong();

        startOnLineUpdateState();
    }

    private void initRong() {
        ChatroomKit.addEventHandler(mHandler);
        DataInterface.setBanStatus(false);
    }

    public void startBan(final long thisBanStartTime, long duration) {
        DataInterface.setBanStatus(true);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (banStartTime == thisBanStartTime) {
                    DataInterface.setBanStatus(false);
                }
            }
        }, duration * 1000 * 60);
    }

    private void getSysNote() {
        APIManager.startRequest(mILiveService.getSysNote(),
                new BaseRequestListener<com.weiju.ccmall.shared.bean.Message>(PlayerSkinActivity.this) {
                    @Override
                    public void onSuccess(com.weiju.ccmall.shared.bean.Message result) {
                        super.onSuccess(result);
                        LiveMessage liveMessage = new LiveMessage();
                        liveMessage.isNotice = true;
                        liveMessage.content = result.getContent();
                        mLiveChatMessageAdapter.addData(liveMessage);
                        mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);
                    }
                }, PlayerSkinActivity.this);
    }


    private void getLiveSku() {
        APIManager.startRequest(mILiveService.getLiveSku(mLiveId),
                new BaseRequestListener<SkuInfo>() {
                    @Override
                    public void onSuccess(SkuInfo result) {
                        super.onSuccess(result);
                        if (TextUtils.isEmpty(result.skuId)) {
                            rlGoods.setVisibility(View.GONE);
                            return;
                        }
                        mSkuId = result.skuId;
                        rlGoods.setVisibility(View.VISIBLE);
                        FrescoUtil.setImageSmall(ivGoods, result.thumb);
                        tvGoodsName.setText(result.name);
                        tvPrice.setText(MoneyUtil.centToYuan¥Str(result.retailPrice));
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                }, PlayerSkinActivity.this);
    }

    private void initData() {
        APIManager.startRequest(mILiveService.joinLive(mLiveId, LiveManager.getDeviceID()),
                new BaseRequestListener<LiveUser>(PlayerSkinActivity.this) {
                    @Override
                    public void onSuccess(LiveUser result) {
                        super.onSuccess(result);
                        mLiveUserTotal = Math.max(result.audienceNum, mLiveUserTotal);
                        LiveManager.joinChatRoom(PlayerSkinActivity.this, mLiveId, result.nickName, result.headImage, result.audienceNum, true);
                    }
                }, PlayerSkinActivity.this);

        APIManager.startRequest(mILiveService.getLive(mLiveId), new BaseRequestListener<LiveRoom>(PlayerSkinActivity.this) {
            @Override
            public void onSuccess(LiveRoom result) {
                mLiveRoom = result;
                if (result.status == 1 && null != result.pullUrl) {
                    mPlayUrl = result.pullUrl.rtmpUrlLud;
                    mIvLike.setVisibility(View.VISIBLE);
                    mIvMore.setVisibility(View.VISIBLE);
                    spacer.setVisibility(View.GONE);
                    mIvLike.setSelected(result.likeStatus == 1);
                } else if (result.recordUrl.size() > 0) {
                    mPlayUrl = result.recordUrl.get(0).recordUrl;
                }
                mTvPopularity.setText(String.format("人气：%s", mLiveRoom.popularity));
                setPlaySource();
            }
        }, PlayerSkinActivity.this);

        getLiveUserInfo(false);
//        getLiveAudienceList(false, null);
        getLiveAudienceUserList();
    }

    private void getLiveUserInfo(boolean isShowDialog) {
        APIManager.startRequest(mILiveService.getLiveUserInfo(mMemberId),
                new BaseRequestListener<LiveUser>() {
                    @Override
                    public void onSuccess(LiveUser result) {
                        mLiveUser = result;
                        SessionUtil.getInstance().setLiveUser(result);
                        User loginUser = SessionUtil.getInstance().getLoginUser();
                        FrescoUtil.setImage(mIvAvatar, result.headImage);
                        tvName.setText(result.nickName);
                        if (loginUser != null) {
                            mIvMore.setVisibility(loginUser.id.equals(result.memberId) ? View.GONE : View.VISIBLE);

//                        mTvPopularity.setText(String.format("人气：%s", result.popularity));
                            if (UiUtils.checkUserLogin(PlayerSkinActivity.this)) {
                                mTvFollow.setText(result.followStatus == 1 ? "已关注" : "关注");
                                mTvFollow.setSelected(result.followStatus == 0);
                                mIvFollow.setVisibility(result.followStatus == 1 || (loginUser.id.equals(result.memberId)) ?
                                        View.GONE : View.VISIBLE);
                                mTvFollow.setVisibility(loginUser.id.equals(result.memberId) ? View.GONE : View.VISIBLE);
                                if (null != mLiveUserInfoDialog && mLiveUserInfoDialog.isShowing()) {
                                    mLiveUserInfoDialog.setLiveUser(result);
                                }
                            }

                            if (isShowDialog) {
                                mLiveUserInfoDialog.show();
                                mLiveUserInfoDialog.setLiveUser(result);
                            }
                        } else {
                            mTvFollow.setSelected(result.followStatus == 0);
                        }

                    }
                }, PlayerSkinActivity.this);
    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            QMUIStatusBarHelper.translucent(PlayerSkinActivity.this);
            QMUIStatusBarHelper.setStatusBarDarkMode(PlayerSkinActivity.this);
        }

        int height = QMUIStatusBarHelper.getStatusbarHeight(PlayerSkinActivity.this);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mLayoutTopBar.getLayoutParams();
        layoutParams.topMargin = height + SizeUtils.dp2px(20);

        //游客模式遮罩
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) rlNoLogin.getLayoutParams();
        params.topMargin = height + SizeUtils.dp2px(20);
        User loginUser = SessionUtil.getInstance().getLoginUser();
        rlNoLogin.setVisibility(loginUser == null ? View.VISIBLE : View.GONE);

        initAliyunPlayerView();

        mIvLike.setVisibility(mStatus == 1 ? View.VISIBLE : View.GONE);
//        mIvMore.setVisibility(mStatus == 1 ? View.VISIBLE : View.GONE);
        mTvPopularity.setVisibility(mStatus == 1 ? View.VISIBLE : View.GONE);
//        spacer.setVisibility(mStatus == 1 ? View.GONE : View.VISIBLE);
        mLayoutMessage.setVisibility(mStatus == 1 ? View.VISIBLE : View.GONE);
        mLayoutFollow.setVisibility(mStatus == 3 ? View.VISIBLE : View.GONE);

//        mLayoutSelectQingXi.setVisibility(mStatus == 1 ? View.VISIBLE : View.GONE);
//        mTvQingXi.setVisibility(mStatus == 1 ? View.VISIBLE : View.GONE);

        mTvChaoQing.setSelected(true);
        if (mStatus == 1) {
            mIvRedList.setVisibility(View.VISIBLE);
            mIvCouponList.setVisibility(View.VISIBLE);
            mMessageRecyclerView.setVisibility(View.VISIBLE);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PlayerSkinActivity.this);
            mMessageRecyclerView.setLayoutManager(linearLayoutManager);
            mLiveChatMessageAdapter = new LiveChatMessageAdapter();
            mMessageRecyclerView.setAdapter(mLiveChatMessageAdapter);

            mMessageLayoutParams = (RelativeLayout.LayoutParams) mMessageRecyclerView.getLayoutParams();

            SoftKeyBoardListener.setListener(PlayerSkinActivity.this, new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {
                @Override
                public void keyBoardShow(int height) {
                    mMessageLayoutParams.bottomMargin = SizeUtils.dp2px(60) + height;
                    mMessageRecyclerView.setLayoutParams(mMessageLayoutParams);
                }

                @Override
                public void keyBoardHide(int height) {
                    mMessageLayoutParams.bottomMargin = SizeUtils.dp2px(100);
                    mMessageRecyclerView.setLayoutParams(mMessageLayoutParams);
                }
            });

        }


        mMoreWindowManage = PopupWindowManage.getInstance(PlayerSkinActivity.this);
        mMoreWindowManage.setBackgroundDrawable(R.drawable.bg_live_more_gray_10);

        mMorePopupView = View.inflate(PlayerSkinActivity.this, R.layout.view_popup_live_play_more, null);
        LinearLayout layoutTool = mMorePopupView.findViewById(R.id.layoutTool);
        LinearLayout layoutQingXi = mMorePopupView.findViewById(R.id.layoutQingXi);
        TextView qingxi = mMorePopupView.findViewById(R.id.tvQingXiMore);
        qingxi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                layoutTool.setVisibility(View.GONE);
                layoutQingXi.setVisibility(View.VISIBLE);
            }
        });

        TextView tvReport = mMorePopupView.findViewById(R.id.tvReport);
        tvReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: 2020/2/19   举报入口
                ReportActivity.start(PlayerSkinActivity.this, mLiveId);
                mMoreWindowManage.dismiss();
            }
        });

        TextView password = mMorePopupView.findViewById(R.id.tvPassword);
        password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        TextView liuchang = mMorePopupView.findViewById(R.id.tvLiuChangSelect);
        TextView gaoqing = mMorePopupView.findViewById(R.id.tvGaoQingSelect);
        gaoqing.setSelected(true);
        TextView chaoqing = mMorePopupView.findViewById(R.id.tvChaoQingSelect);

        liuchang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                liuchang.setSelected(true);
                gaoqing.setSelected(false);
                chaoqing.setSelected(false);
                mPlayUrl = mLiveRoom.pullUrl.rtmpUrlLld;
                setPlaySource();
            }
        });

        gaoqing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                liuchang.setSelected(false);
                gaoqing.setSelected(true);
                chaoqing.setSelected(false);
                mPlayUrl = mLiveRoom.pullUrl.rtmpUrlLsd;
                setPlaySource();
            }
        });

        chaoqing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                liuchang.setSelected(false);
                gaoqing.setSelected(false);
                chaoqing.setSelected(true);
                mPlayUrl = mLiveRoom.pullUrl.rtmpUrlLud;
                setPlaySource();
            }
        });


        refreshLayout.setRefreshCallback(new RefreshCallBack() {
            @Override
            public void onLeftRefreshing() {
                mUserCurrentPage = 1;
                getLiveAudienceUserList();
                mIsRefreshLayout = true;
            }

            @Override
            public void onRightRefreshing() {
                mUserCurrentPage++;
                getLiveAudienceUserList();
                mIsRefreshLayout = true;
            }
        });
        refreshLayout.setRefreshHeader(new LoadingRefreshHeader(PlayerSkinActivity.this), HorizontalRefreshLayout.LEFT);
        refreshLayout.setRefreshHeader(new LoadingRefreshHeader(PlayerSkinActivity.this), HorizontalRefreshLayout.RIGHT);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PlayerSkinActivity.this, LinearLayoutManager.HORIZONTAL, false);
        mRecycler.setLayoutManager(linearLayoutManager);
        mRecycler.setAdapter(mLiveHorizontalUserAdapter);
    }


    private void getLiveAudienceUserList() {
        APIManager.startRequest(mILiveService.getLiveAudienceList(mLiveId, mUserCurrentPage, 5),
                new BaseRequestListener<PaginationEntity<LiveUser, Object>>() {
                    @Override
                    public void onSuccess(PaginationEntity<LiveUser, Object> result) {
                        if (null != refreshLayout && mIsRefreshLayout) {
                            mIsRefreshLayout = false;
                            refreshLayout.onRefreshComplete();
                        }
                        if (mUserCurrentPage == 1) {
                            mLiveHorizontalUserAdapter.setNewData(result.list);
                        } else {
                            mLiveHorizontalUserAdapter.addData(result.list);
                            mRecycler.scrollToPosition(mLiveHorizontalUserAdapter.getData().size() - 1);

                        }
                        if (result.page >= result.totalPage) {
                            mLiveHorizontalUserAdapter.loadMoreEnd();
                        } else {
                            mLiveHorizontalUserAdapter.loadMoreComplete();
                        }

                    }
                }, PlayerSkinActivity.this);
    }

    /**
     * 设置屏幕亮度
     */
    private void setWindowBrightness(int brightness) {
        Window window = getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.screenBrightness = brightness / 255.0f;
        window.setAttributes(lp);
    }

    private void copyAssets() {
        commenUtils = Common.getInstance(getApplicationContext()).copyAssetsToSD("encrypt", "aliyun");
        commenUtils.setFileOperateCallback(

                new Common.FileOperateCallback() {
                    @Override
                    public void onSuccess() {
                        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/test_save/");
                        if (!file.exists()) {
                            file.mkdir();
                        }

                        // 获取AliyunDownloadManager对象
                        downloadManager = AliyunDownloadManager.getInstance(getApplicationContext());
                        downloadManager.setEncryptFilePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/aliyun/encryptedApp.dat");
                        PrivateService.initService(getApplicationContext(), Environment.getExternalStorageDirectory().getAbsolutePath() + "/aliyun/encryptedApp.dat");
                        downloadManager.setDownloadDir(file.getAbsolutePath());
                        //设置同时下载个数
                        downloadManager.setMaxNum(3);

                        downloadDataProvider = DownloadDataProvider.getSingleton(getApplicationContext());
                        // 更新sts回调
                        downloadManager.setRefreshStsCallback(new MyRefreshStsCallback());

                        // 视频下载的回调
                        downloadManager.setDownloadInfoListener(new MyDownloadInfoListener(PlayerSkinActivity.this));
                    }

                    @Override
                    public void onFailed(String error) {
                    }
                });
    }

    private void initAliyunPlayerView() {
        mAliyunVodPlayerView = (AliyunVodPlayerView) findViewById(R.id.video_view);
        //保持屏幕敞亮
        mAliyunVodPlayerView.setKeepScreenOn(true);
        String sdDir = Environment.getExternalStorageDirectory().getAbsolutePath() + "/test_save_cache";
        mAliyunVodPlayerView.setPlayingCache(false, sdDir, 60 * 60 /*时长, s */, 300 /*大小，MB*/);
        mAliyunVodPlayerView.setTheme(AliyunVodPlayerView.Theme.Red);
        mAliyunVodPlayerView.setAutoPlay(true);
        mAliyunVodPlayerView.setShowPause(mStatus == 3);
        mAliyunVodPlayerView.setCanDoubleClick(mStatus != 1);

        mAliyunVodPlayerView.setOnPreparedListener(new MyPrepareListener(this));
        mAliyunVodPlayerView.setNetConnectedListener(new MyNetConnectedListener(this));
        mAliyunVodPlayerView.setOnFirstFrameStartListener(new MyFrameInfoListener(this));
        mAliyunVodPlayerView.setOnChangeQualityListener(new MyChangeQualityListener(this));
        mAliyunVodPlayerView.setOnStoppedListener(new MyStoppedListener(this));
        mAliyunVodPlayerView.setmOnPlayerViewClickListener(new MyPlayViewClickListener(this));
        mAliyunVodPlayerView.setOrientationChangeListener(new MyOrientationChangeListener(this));
        mAliyunVodPlayerView.setOnTimeExpiredErrorListener(new MyOnTimeExpiredErrorListener(this));
        mAliyunVodPlayerView.setOnShowMoreClickListener(new MyShowMoreClickLisener(this));
        mAliyunVodPlayerView.setOnScreenBrightness(new MyOnScreenBrightnessListener(this));
        mAliyunVodPlayerView.setOnErrorListener(new MyOnErrorListener(this));
        mAliyunVodPlayerView.setScreenBrightness(BrightnessDialog.getActivityBrightness(PlayerSkinActivity.this));
        mAliyunVodPlayerView.enableNativeLog();

        mAliyunVodPlayerView.setOnCompletionListener(new MyCompletionListener(this));
        mAliyunVodPlayerView.setLiveEndListener(liveEndListener);
        mAliyunVodPlayerView.setLivePlayListener(mLivePlayListener);

        mAliyunVodPlayerView.setOnErrorListener(new IPlayer.OnErrorListener() {
            @Override
            public void onError(com.aliyun.player.bean.ErrorInfo errorInfo) {
                if (errorInfo.getCode().getValue() == ErrorCode.ERROR_SERVER_POP_UNKNOWN.getValue()) {
                    VidStsUtil.getVidSts(PlayParameter.PLAY_PARAM_VID, new RetryExpiredSts(PlayerSkinActivity.this));
                } else {
                    mAliyunVodPlayerView.reTry();
                }

            }
        });


        AliPlayer aliPlayer = mAliyunVodPlayerView.getAliPlayer();
        aliPlayer.setOnVideoSizeChangedListener(new IPlayer.OnVideoSizeChangedListener() {
            @Override
            public void onVideoSizeChanged(int width, int height) {
                if (width == 0 || height == 0) {
                    Log.e("bobo", "invalid video width(" + width + ") or height(" + height
                            + ")");
                    return;
                }
                int mSurfaceViewWidth = ScreenUtils.getScreenWidth();
                int mSurfaceViewHeight = ScreenUtils.getScreenHeight();

                int w = mSurfaceViewHeight * width / height;
                int margin = (mSurfaceViewWidth - w) / 2;
                RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.MATCH_PARENT,
                        RelativeLayout.LayoutParams.MATCH_PARENT);
                lp.setMargins(margin, 0, margin, 0);
                mAliyunVodPlayerView.getPlayerView().setLayoutParams(lp);
            }
        });


    }

    private static final int REQUEST_OVERLAY_CODE = 666;

    /**
     * 判断是否开启悬浮窗口权限，否则，跳转开启页
     */
    public boolean requestOverlayPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                floatingIv.postDelayed(() -> {
                    Intent intentOverlay = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
                    startActivityForResult(intentOverlay, REQUEST_OVERLAY_CODE);
                },500);
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    private FloatingWindow mFloatingWindow;
    private void showFloatingWindow() {
        if (mFloatingWindow == null){
            mFloatingWindow = new FloatingWindow();
        }
        View view = View.inflate(this, R.layout.floating_player_view, null);
        mFloatingWindow.showFloatingWindowView(this, view, mPlayUrl);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //    loadPlayList();
        //vid/url设置界面并且是取消
        if (requestCode == CODE_REQUEST_SETTING && resultCode == Activity.RESULT_CANCELED) {
            return;
        } else if (requestCode == CODE_REQUEST_SETTING && resultCode == Activity.RESULT_OK) {
            setPlaySource();
        }
        if (requestCode == REQUEST_OVERLAY_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    showFloatingWindow();
                } else {
                    ToastUtil.error("获取悬浮窗权限失败");
                }
            }
        }
    }

    /**
     * 播放本地资源
     */
    private void changePlayLocalSource(String url, String title) {
        UrlSource urlSource = new UrlSource();
        urlSource.setUri(url);
        urlSource.setTitle(title);
        mAliyunVodPlayerView.setLocalSource(urlSource);
    }

    @OnClick({R.id.ivCloseGoods, R.id.rlGoods})
    public void onGoodsViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivCloseGoods:
                rlGoods.setVisibility(View.GONE);
                break;
            case R.id.rlGoods:
                EventUtil.viewProductDetail(this, mSkuId, false, "", mLiveId);
                EventBus.getDefault().post(new EventMessage(Event.openNewActivity));
                break;
        }
    }


    private static class MyPrepareListener implements IPlayer.OnPreparedListener {

        private WeakReference<PlayerSkinActivity> activityWeakReference;

        public MyPrepareListener(PlayerSkinActivity skinActivity) {
            activityWeakReference = new WeakReference<>(skinActivity);
        }

        @Override
        public void onPrepared() {
            PlayerSkinActivity activity = activityWeakReference.get();
            if (activity != null) {
                activity.onPrepared();
            }
        }
    }

    private void onPrepared() {
        logStrs.add(format.format(new Date()) + getString(R.string.log_prepare_success));
        // FixedToastUtils.show(PlayerSkinActivity.this.getApplicationContext(), R.string.toast_prepare_success);
    }


    private static class MyFrameInfoListener implements IPlayer.OnRenderingStartListener {

        private WeakReference<PlayerSkinActivity> activityWeakReference;

        public MyFrameInfoListener(PlayerSkinActivity skinActivity) {
            activityWeakReference = new WeakReference<PlayerSkinActivity>(skinActivity);
        }

        @Override
        public void onRenderingStart() {
            PlayerSkinActivity activity = activityWeakReference.get();
            if (activity != null) {
                activity.onFirstFrameStart();
            }
        }
    }

    private void onFirstFrameStart() {
        if (mAliyunVodPlayerView != null) {
            Map<String, String> debugInfo = mAliyunVodPlayerView.getAllDebugInfo();
            if (debugInfo == null) {
                return;
            }
            long createPts = 0;
            if (debugInfo.get("create_player") != null) {
                String time = debugInfo.get("create_player");
                createPts = (long) Double.parseDouble(time);
                logStrs.add(format.format(new Date(createPts)) + getString(R.string.log_player_create_success));
            }
            if (debugInfo.get("open-url") != null) {
                String time = debugInfo.get("open-url");
                long openPts = (long) Double.parseDouble(time) + createPts;
                logStrs.add(format.format(new Date(openPts)) + getString(R.string.log_open_url_success));
            }
            if (debugInfo.get("find-stream") != null) {
                String time = debugInfo.get("find-stream");
                long findPts = (long) Double.parseDouble(time) + createPts;
                logStrs.add(format.format(new Date(findPts)) + getString(R.string.log_request_stream_success));
            }
            if (debugInfo.get("open-stream") != null) {
                String time = debugInfo.get("open-stream");
                long openPts = (long) Double.parseDouble(time) + createPts;
                logStrs.add(format.format(new Date(openPts)) + getString(R.string.log_start_open_stream));
            }
            logStrs.add(format.format(new Date()) + getString(R.string.log_first_frame_played));

        }
    }

    private class MyPlayViewClickListener implements AliyunVodPlayerView.OnPlayerViewClickListener {

        private WeakReference<PlayerSkinActivity> weakReference;

        public MyPlayViewClickListener(PlayerSkinActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void onClick(AliyunScreenMode screenMode, AliyunVodPlayerView.PlayViewType viewType) {
            long currentClickTime = System.currentTimeMillis();
            // 防止快速点击
            if (currentClickTime - oldTime <= 1000) {
                return;
            }
            oldTime = currentClickTime;
            // 如果当前的Type是Download, 就显示Download对话框
            if (viewType == AliyunVodPlayerView.PlayViewType.Download) {
                mCurrentDownloadScreenMode = screenMode;
                PlayerSkinActivity aliyunPlayerSkinActivity = weakReference.get();
                if (aliyunPlayerSkinActivity != null) {
                    aliyunPlayerSkinActivity.showAddDownloadView = true;
                }

                if (mAliyunVodPlayerView != null) {
                    MediaInfo currentMediaInfo = mAliyunVodPlayerView.getCurrentMediaInfo();
                    if (currentMediaInfo != null && currentMediaInfo.getVideoId().equals(PlayParameter.PLAY_PARAM_VID)) {
                        VidSts vidSts = new VidSts();
                        vidSts.setVid(PlayParameter.PLAY_PARAM_VID);
                        vidSts.setRegion(PlayParameter.PLAY_PARAM_REGION);
                        vidSts.setAccessKeyId(PlayParameter.PLAY_PARAM_AK_ID);
                        vidSts.setAccessKeySecret(PlayParameter.PLAY_PARAM_AK_SECRE);
                        vidSts.setSecurityToken(PlayParameter.PLAY_PARAM_SCU_TOKEN);
                        if (!mDownloadInPrepare) {
                            mDownloadInPrepare = true;
                            downloadManager.prepareDownload(vidSts);
                        }
                    }
                }
            }
        }
    }

    /**
     * 显示下载选择项 download 对话框
     *
     * @param screenMode
     */
    private void showAddDownloadView(AliyunScreenMode screenMode) {
        //这个时候视频的状态已经是delete了
        if (currentPreparedMediaInfo != null && currentPreparedMediaInfo.get(0).getVid().equals(preparedVid)) {
            downloadDialog = new DownloadChoiceDialog(this, screenMode);
            final AddDownloadView contentView = new AddDownloadView(this, screenMode);
            contentView.onPrepared(currentPreparedMediaInfo);
            contentView.setOnViewClickListener(viewClickListener);
            final View inflate = LayoutInflater.from(getApplicationContext()).inflate(
                    R.layout.alivc_dialog_download_video, null, false);
            dialogDownloadView = inflate.findViewById(R.id.download_view);
            downloadDialog.setContentView(contentView);
            downloadDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    if (dialogDownloadView != null) {
                        dialogDownloadView.setOnDownloadViewListener(null);
                        dialogDownloadView.setOnDownloadedItemClickListener(null);
                    }
                }
            });
            if (!downloadDialog.isShowing()) {
                downloadDialog.show();
            }
            downloadDialog.setCanceledOnTouchOutside(true);

            if (screenMode == AliyunScreenMode.Full) {
                contentView.setOnShowVideoListLisener(new AddDownloadView.OnShowNativeVideoBtnClickListener() {
                    @Override
                    public void onShowVideo() {
                        if (downloadDataProvider != null) {
                            downloadDataProvider.restoreMediaInfo(new LoadDbDatasListener() {
                                @Override
                                public void onLoadSuccess(List<AliyunDownloadMediaInfo> dataList) {
                                    if (dialogDownloadView != null) {
                                        dialogDownloadView.addAllDownloadMediaInfo(dataList);
                                    }
                                }
                            });
                        }
                        downloadDialog.setContentView(inflate);
                    }
                });

                dialogDownloadView.setOnDownloadViewListener(new DownloadView.OnDownloadViewListener() {
                    @Override
                    public void onStop(AliyunDownloadMediaInfo downloadMediaInfo) {
                        downloadManager.stopDownload(downloadMediaInfo);
                    }

                    @Override
                    public void onStart(AliyunDownloadMediaInfo downloadMediaInfo) {
                        downloadManager.startDownload(downloadMediaInfo);
                    }

                    @Override
                    public void onDeleteDownloadInfo(final ArrayList<AlivcDownloadMediaInfo> alivcDownloadMediaInfos) {
                        // 视频删除的dialog
                        final AlivcDialog alivcDialog = new AlivcDialog(PlayerSkinActivity.this);
                        alivcDialog.setDialogIcon(R.drawable.icon_delete_tips);
                        alivcDialog.setMessage(getResources().getString(R.string.alivc_delete_confirm));
                        alivcDialog.setOnConfirmclickListener(getResources().getString(R.string.alivc_dialog_sure),
                                new AlivcDialog.onConfirmClickListener() {
                                    @Override
                                    public void onConfirm() {
                                        alivcDialog.dismiss();
                                        if (alivcDownloadMediaInfos != null && alivcDownloadMediaInfos.size() > 0) {
                                            dialogDownloadView.deleteDownloadInfo();

                                        }
                                        if (downloadManager != null) {
                                            for (AlivcDownloadMediaInfo alivcDownloadMediaInfo : alivcDownloadMediaInfos) {
                                                downloadManager.deleteFile(alivcDownloadMediaInfo.getAliyunDownloadMediaInfo());
                                            }

                                        }
                                        downloadDataProvider.deleteAllDownloadInfo(alivcDownloadMediaInfos);
                                    }
                                });
                        alivcDialog.setOnCancelOnclickListener(getResources().getString(R.string.alivc_dialog_cancle),
                                new AlivcDialog.onCancelOnclickListener() {
                                    @Override
                                    public void onCancel() {
                                        alivcDialog.dismiss();
                                    }
                                });
                        alivcDialog.show();
                    }
                });

                dialogDownloadView.setOnDownloadedItemClickListener(new DownloadView.OnDownloadItemClickListener() {
                    @Override
                    public void onDownloadedItemClick(final int positin) {
                        ArrayList<AlivcDownloadMediaInfo> allDownloadMediaInfo = dialogDownloadView.getAllDownloadMediaInfo();
                        List<AliyunDownloadMediaInfo> dataList = new ArrayList<>();
                        for (AlivcDownloadMediaInfo alivcDownloadMediaInfo : allDownloadMediaInfo) {
                            dataList.add(alivcDownloadMediaInfo.getAliyunDownloadMediaInfo());
                        }
//                List<AliyunDownloadMediaInfo> dataList = downloadDataProvider.getAllDownloadMediaInfo();
                        // 存入顺序和显示顺序相反,  所以进行倒序
                        ArrayList<AliyunDownloadMediaInfo> tempList = new ArrayList<>();
                        int size = dataList.size();
                        for (AliyunDownloadMediaInfo aliyunDownloadMediaInfo : dataList) {
                            if (aliyunDownloadMediaInfo.getProgress() == 100) {
                                tempList.add(aliyunDownloadMediaInfo);
                            }
                        }

                        Collections.reverse(tempList);
                        if ((dataList.size() - 1) < 0 || (dataList.size() - 1) > tempList.size()) {
                            return;
                        }
                        tempList.add(dataList.get(dataList.size() - 1));
                        for (int i = 0; i < size; i++) {
                            AliyunDownloadMediaInfo aliyunDownloadMediaInfo = dataList.get(i);
                            if (!tempList.contains(aliyunDownloadMediaInfo)) {
                                tempList.add(aliyunDownloadMediaInfo);
                            }
                        }

                        if (positin < 0) {
                            FixedToastUtils.show(PlayerSkinActivity.this, "视频资源不存在");
                            return;
                        }

                        // 如果点击列表中的视频, 需要将类型改为vid
                        AliyunDownloadMediaInfo aliyunDownloadMediaInfo = tempList.get(positin);
                        PlayParameter.PLAY_PARAM_TYPE = "localSource";
                        if (aliyunDownloadMediaInfo != null) {
                            PlayParameter.PLAY_PARAM_URL = aliyunDownloadMediaInfo.getSavePath();
                            mAliyunVodPlayerView.updateScreenShow();
                            changePlayLocalSource(PlayParameter.PLAY_PARAM_URL, aliyunDownloadMediaInfo.getTitle());
                        }
                    }

                    @Override
                    public void onDownloadingItemClick(ArrayList<AlivcDownloadMediaInfo> infos, int position) {
                        AlivcDownloadMediaInfo alivcInfo = infos.get(position);
                        AliyunDownloadMediaInfo aliyunDownloadInfo = alivcInfo.getAliyunDownloadMediaInfo();
                        AliyunDownloadMediaInfo.Status status = aliyunDownloadInfo.getStatus();
                        if (status == AliyunDownloadMediaInfo.Status.Error || status == AliyunDownloadMediaInfo.Status.Wait) {
                            //downloadManager.removeDownloadMedia(aliyunDownloadInfo);
                            downloadManager.startDownload(aliyunDownloadInfo);

                        }
                    }

                });
            }
        }
    }

    private Dialog downloadDialog = null;

    private AliyunDownloadMediaInfo aliyunDownloadMediaInfo;
    /**
     * 开始下载的事件监听
     */
    private AddDownloadView.OnViewClickListener viewClickListener = new AddDownloadView.OnViewClickListener() {
        @Override
        public void onCancel() {
            if (downloadDialog != null) {
                downloadDialog.dismiss();
            }
        }

        @Override
        public void onDownload(AliyunDownloadMediaInfo info) {
            if (downloadDialog != null) {
                downloadDialog.dismiss();
            }
            aliyunDownloadMediaInfo = info;
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
                int permission = ContextCompat.checkSelfPermission(PlayerSkinActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);
                if (permission != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(PlayerSkinActivity.this, PERMISSIONS_STORAGE,
                            REQUEST_EXTERNAL_STORAGE);

                }
            }

        }
    };


    /**
     * 下载监听
     */
    private static class MyDownloadInfoListener implements AliyunDownloadInfoListener {

        private WeakReference<PlayerSkinActivity> weakReference;

        public MyDownloadInfoListener(PlayerSkinActivity aliyunPlayerSkinActivity) {
            weakReference = new WeakReference<>(aliyunPlayerSkinActivity);
        }

        @Override
        public void onPrepared(List<AliyunDownloadMediaInfo> infos) {
            preparedVid = infos.get(0).getVid();
            Collections.sort(infos, new Comparator<AliyunDownloadMediaInfo>() {
                @Override
                public int compare(AliyunDownloadMediaInfo mediaInfo1, AliyunDownloadMediaInfo mediaInfo2) {
                    if (mediaInfo1.getSize() > mediaInfo2.getSize()) {
                        return 1;
                    }
                    if (mediaInfo1.getSize() < mediaInfo2.getSize()) {
                        return -1;
                    }

                    if (mediaInfo1.getSize() == mediaInfo2.getSize()) {
                        return 0;
                    }
                    return 0;
                }
            });
            PlayerSkinActivity aliyunPlayerSkinActivity = weakReference.get();
            if (aliyunPlayerSkinActivity != null) {
                aliyunPlayerSkinActivity.mDownloadInPrepare = false;
                aliyunPlayerSkinActivity.onDownloadPrepared(infos, aliyunPlayerSkinActivity.showAddDownloadView);
            }
        }

        @Override
        public void onAdd(AliyunDownloadMediaInfo info) {
            PlayerSkinActivity aliyunPlayerSkinActivity = weakReference.get();
            if (aliyunPlayerSkinActivity != null) {
                if (aliyunPlayerSkinActivity.downloadDataProvider != null) {
                    aliyunPlayerSkinActivity.downloadDataProvider.addDownloadMediaInfo(info);
                }
            }
        }

        @Override
        public void onStart(AliyunDownloadMediaInfo info) {
            PlayerSkinActivity aliyunPlayerSkinActivity = weakReference.get();
            if (aliyunPlayerSkinActivity != null) {
                if (aliyunPlayerSkinActivity.dialogDownloadView != null) {
                    aliyunPlayerSkinActivity.dialogDownloadView.updateInfo(info);
                }


            }
        }

        @Override
        public void onProgress(AliyunDownloadMediaInfo info, int percent) {
            PlayerSkinActivity aliyunPlayerSkinActivity = weakReference.get();
            if (aliyunPlayerSkinActivity != null) {
                if (aliyunPlayerSkinActivity.dialogDownloadView != null) {
                    aliyunPlayerSkinActivity.dialogDownloadView.updateInfo(info);
                }

            }
        }

        @Override
        public void onStop(AliyunDownloadMediaInfo info) {
            PlayerSkinActivity aliyunPlayerSkinActivity = weakReference.get();
            if (aliyunPlayerSkinActivity != null) {
                if (aliyunPlayerSkinActivity.dialogDownloadView != null) {
                    aliyunPlayerSkinActivity.dialogDownloadView.updateInfo(info);
                }

            }
        }

        @Override
        public void onCompletion(AliyunDownloadMediaInfo info) {
            PlayerSkinActivity playerSkinActivity = weakReference.get();
            if (playerSkinActivity != null) {
                synchronized (playerSkinActivity) {

                    if (playerSkinActivity.dialogDownloadView != null) {
                        playerSkinActivity.dialogDownloadView.updateInfoByComplete(info);
                    }

                    if (playerSkinActivity.downloadDataProvider != null) {
                        playerSkinActivity.downloadDataProvider.addDownloadMediaInfo(info);
                    }
                }
            }
        }

        @Override
        public void onError(AliyunDownloadMediaInfo info, ErrorCode code, String msg, String requestId) {
            PlayerSkinActivity aliyunPlayerSkinActivity = weakReference.get();
            if (aliyunPlayerSkinActivity != null) {
                aliyunPlayerSkinActivity.mDownloadInPrepare = false;

                if (aliyunPlayerSkinActivity.dialogDownloadView != null) {
                    aliyunPlayerSkinActivity.dialogDownloadView.updateInfoByError(info);
                }


                Message message = Message.obtain();
                Bundle bundle = new Bundle();
                bundle.putString(DOWNLOAD_ERROR_KEY, msg);
                message.setData(bundle);
                message.what = DOWNLOAD_ERROR;
                aliyunPlayerSkinActivity.playerHandler = new PlayerHandler(aliyunPlayerSkinActivity);
                aliyunPlayerSkinActivity.playerHandler.sendMessage(message);
            }
        }

        @Override
        public void onWait(AliyunDownloadMediaInfo info) {
        }

        @Override
        public void onDelete(AliyunDownloadMediaInfo info) {
        }

        @Override
        public void onDeleteAll() {
        }

        @Override
        public void onFileProgress(AliyunDownloadMediaInfo info) {

        }
    }

    List<AliyunDownloadMediaInfo> aliyunDownloadMediaInfoList = new ArrayList<>();
    private List<AliyunDownloadMediaInfo> currentPreparedMediaInfo = null;

    private void onDownloadPrepared(List<AliyunDownloadMediaInfo> infos, boolean showAddDownloadView) {
        currentPreparedMediaInfo = new ArrayList<>();
        currentPreparedMediaInfo.addAll(infos);
        if (showAddDownloadView) {
            showAddDownloadView(mCurrentDownloadScreenMode);
        }

    }

    private static class MyChangeQualityListener implements OnChangeQualityListener {

        private WeakReference<PlayerSkinActivity> activityWeakReference;

        public MyChangeQualityListener(PlayerSkinActivity skinActivity) {
            activityWeakReference = new WeakReference<PlayerSkinActivity>(skinActivity);
        }

        @Override
        public void onChangeQualitySuccess(String finalQuality) {

            PlayerSkinActivity activity = activityWeakReference.get();
            if (activity != null) {
                activity.onChangeQualitySuccess(finalQuality);
            }
        }

        @Override
        public void onChangeQualityFail(int code, String msg) {
            PlayerSkinActivity activity = activityWeakReference.get();
            if (activity != null) {
                activity.onChangeQualityFail(code, msg);
            }
        }
    }

    private void onChangeQualitySuccess(String finalQuality) {
        logStrs.add(format.format(new Date()) + getString(R.string.log_change_quality_success));
        FixedToastUtils.show(PlayerSkinActivity.this.getApplicationContext(),
                getString(R.string.log_change_quality_success));
    }

    void onChangeQualityFail(int code, String msg) {
        logStrs.add(format.format(new Date()) + getString(R.string.log_change_quality_fail) + " : " + msg);
        FixedToastUtils.show(PlayerSkinActivity.this.getApplicationContext(),
                getString(R.string.log_change_quality_fail));
    }

    private static class MyStoppedListener implements OnStoppedListener {

        private WeakReference<AliyunPlayerSkinActivity> activityWeakReference;

        public MyStoppedListener(AliyunPlayerSkinActivity skinActivity) {
            activityWeakReference = new WeakReference<AliyunPlayerSkinActivity>(skinActivity);
        }

        @Override
        public void onStop() {
//            ToastUtil.error("&#160;播放停止");
        }
    }

    private static class MyRefreshStsCallback implements RefreshStsCallback {

        @Override
        public VidSts refreshSts(String vid, String quality, String format, String title, boolean encript) {
            VcPlayerLog.d("refreshSts ", "refreshSts , vid = " + vid);
            //NOTE: 注意：这个不能启动线程去请求。因为这个方法已经在线程中调用了。
            VidSts vidSts = VidStsUtil.getVidSts(vid);
            if (vidSts == null) {
                return null;
            } else {
                vidSts.setVid(vid);
                vidSts.setQuality(quality, true);
                vidSts.setTitle(title);
                return vidSts;
            }
        }
    }


    private void setPlaySource() {
        if (!TextUtils.isEmpty(mPlayUrl)) {
            UrlSource urlSource = new UrlSource();
            urlSource.setUri(mPlayUrl);
            mAliyunVodPlayerView.setLocalSource(urlSource);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsInBackground = false;
        updatePlayerViewMode();
        if (mAliyunVodPlayerView != null) {
            mAliyunVodPlayerView.setAutoPlay(true);
            mAliyunVodPlayerView.onResume();
        }
        if (mFloatingWindow != null) {
            mFloatingWindow.dismiss();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ToastUtil.hideLoading();

        mIsInBackground = true;
        if (mAliyunVodPlayerView != null) {
            mAliyunVodPlayerView.setAutoPlay(false);
            mAliyunVodPlayerView.onStop();
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updatePlayerViewMode();
    }


    private void updatePlayerViewMode() {
        if (mAliyunVodPlayerView != null) {
            int orientation = getResources().getConfiguration().orientation;
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                this.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                mAliyunVodPlayerView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
            }
        }
    }

    @Override
    protected void onDestroy() {
        if (mFloatingWindow != null) {
            mFloatingWindow.dismiss();
        }
        if (mAliyunVodPlayerView != null) {
            mAliyunVodPlayerView.onDestroy();
            mAliyunVodPlayerView = null;
        }

        if (playerHandler != null) {
            playerHandler.removeMessages(DOWNLOAD_ERROR);
            playerHandler = null;
        }

        if (commenUtils != null) {
            commenUtils.onDestroy();
            commenUtils = null;
        }
        ChatroomKit.quitChatRoom(new RongIMClient.OperationCallback() {
            @Override
            public void onSuccess() {
                ChatroomKit.removeEventHandler(mHandler);
                if (DataInterface.isLoginStatus()) {
                    ChatroomUserQuit userQuit = new ChatroomUserQuit();
                    userQuit.setId(ChatroomKit.getCurrentUser().getUserId());
                    ChatroomKit.sendMessage(userQuit, false);
                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {
                ChatroomKit.removeEventHandler(mHandler);
            }
        });


        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }

        super.onDestroy();

        if (downloadManager != null && downloadDataProvider != null) {
            ConcurrentLinkedQueue<AliyunDownloadMediaInfo> downloadMediaInfos = new ConcurrentLinkedQueue<>();
            downloadMediaInfos.addAll(downloadDataProvider.getAllDownloadMediaInfo());
            downloadManager.stopDownloads(downloadMediaInfos);
        }

        stopOnLineUpdateState();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mAliyunVodPlayerView != null) {
            boolean handler = mAliyunVodPlayerView.onKeyDown(keyCode, event);
            if (!handler) {
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        //解决某些手机上锁屏之后会出现标题栏的问题。
        updatePlayerViewMode();
    }

    private static final int DOWNLOAD_ERROR = 1;
    private static final String DOWNLOAD_ERROR_KEY = "error_key";

    private static class PlayerHandler extends Handler {
        //持有弱引用AliyunPlayerSkinActivity,GC回收时会被回收掉.
        private final WeakReference<PlayerSkinActivity> mActivty;

        public PlayerHandler(PlayerSkinActivity activity) {
            mActivty = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            PlayerSkinActivity activity = mActivty.get();
            super.handleMessage(msg);
            if (activity != null) {
                switch (msg.what) {
                    case DOWNLOAD_ERROR:
                        ToastUtil.error(msg.getData().getString(DOWNLOAD_ERROR_KEY));
                        Log.d("donwload", msg.getData().getString(DOWNLOAD_ERROR_KEY));
                        break;
                    default:
                        break;
                }
            }
        }
    }


    private static class MyOrientationChangeListener implements AliyunVodPlayerView.OnOrientationChangeListener {

        private final WeakReference<PlayerSkinActivity> weakReference;

        public MyOrientationChangeListener(PlayerSkinActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void orientationChange(boolean from, AliyunScreenMode currentMode) {
            PlayerSkinActivity activity = weakReference.get();

            if (activity != null) {
                activity.hideDownloadDialog(from, currentMode);
                activity.hideShowMoreDialog(from, currentMode);

            }
        }
    }

    private void hideShowMoreDialog(boolean from, AliyunScreenMode currentMode) {
        if (showMoreDialog != null) {
            if (currentMode == AliyunScreenMode.Small) {
                showMoreDialog.dismiss();
                currentScreenMode = currentMode;
            }
        }
    }

    private void hideDownloadDialog(boolean from, AliyunScreenMode currentMode) {

        if (downloadDialog != null) {
            if (currentScreenMode != currentMode) {
                downloadDialog.dismiss();
                currentScreenMode = currentMode;
            }
        }
    }

    /**
     * 判断是否有网络的监听
     */
    private class MyNetConnectedListener implements AliyunVodPlayerView.NetConnectedListener {
        WeakReference<PlayerSkinActivity> weakReference;

        public MyNetConnectedListener(PlayerSkinActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void onReNetConnected(boolean isReconnect) {
            PlayerSkinActivity activity = weakReference.get();
            if (activity != null) {
                activity.onReNetConnected(isReconnect);
            }
        }

        @Override
        public void onNetUnConnected() {
            PlayerSkinActivity activity = weakReference.get();
            if (activity != null) {
                activity.onNetUnConnected();
            }
        }
    }

    private void onNetUnConnected() {
        currentError = ErrorInfo.UnConnectInternet;
        if (aliyunDownloadMediaInfoList != null && aliyunDownloadMediaInfoList.size() > 0) {
            ConcurrentLinkedQueue<AliyunDownloadMediaInfo> allDownloadMediaInfo = new ConcurrentLinkedQueue<>();
            List<AliyunDownloadMediaInfo> mediaInfos = downloadDataProvider.getAllDownloadMediaInfo();
            allDownloadMediaInfo.addAll(mediaInfos);
            downloadManager.stopDownloads(allDownloadMediaInfo);
        }
    }

    private void onReNetConnected(boolean isReconnect) {
        currentError = ErrorInfo.Normal;
        if (isReconnect) {
            if (aliyunDownloadMediaInfoList != null && aliyunDownloadMediaInfoList.size() > 0) {
                int unCompleteDownload = 0;
                for (AliyunDownloadMediaInfo info : aliyunDownloadMediaInfoList) {
                    if (info.getStatus() == AliyunDownloadMediaInfo.Status.Stop) {
                        unCompleteDownload++;
                    }
                }

                if (unCompleteDownload > 0) {
                    FixedToastUtils.show(this, "网络恢复, 请手动开启下载任务...");
                }
            }

        }
    }

    /**
     * 因为鉴权过期,而去重新鉴权
     */
    private static class RetryExpiredSts implements VidStsUtil.OnStsResultListener {

        private WeakReference<PlayerSkinActivity> weakReference;

        public RetryExpiredSts(PlayerSkinActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void onSuccess(String vid, String akid, String akSecret, String token) {
            PlayerSkinActivity activity = weakReference.get();
            if (activity != null) {
                activity.onStsRetrySuccess(vid, akid, akSecret, token);
            }
        }

        @Override
        public void onFail() {

        }
    }

    private void onStsRetrySuccess(String mVid, String akid, String akSecret, String token) {
        PlayParameter.PLAY_PARAM_VID = mVid;
        PlayParameter.PLAY_PARAM_AK_ID = akid;
        PlayParameter.PLAY_PARAM_AK_SECRE = akSecret;
        PlayParameter.PLAY_PARAM_SCU_TOKEN = token;

        inRequest = false;
        mIsTimeExpired = false;

        VidSts vidSts = new VidSts();
        vidSts.setVid(PlayParameter.PLAY_PARAM_VID);
        vidSts.setRegion(PlayParameter.PLAY_PARAM_REGION);
        vidSts.setAccessKeyId(PlayParameter.PLAY_PARAM_AK_ID);
        vidSts.setAccessKeySecret(PlayParameter.PLAY_PARAM_AK_SECRE);
        vidSts.setSecurityToken(PlayParameter.PLAY_PARAM_SCU_TOKEN);

        mAliyunVodPlayerView.setVidSts(vidSts);
    }

    public static class MyOnTimeExpiredErrorListener implements AliyunVodPlayerView.OnTimeExpiredErrorListener {

        WeakReference<PlayerSkinActivity> weakReference;

        public MyOnTimeExpiredErrorListener(PlayerSkinActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void onTimeExpiredError() {
            PlayerSkinActivity activity = weakReference.get();
            if (activity != null) {
                activity.onTimExpiredError();
            }
        }
    }


    /**
     * 鉴权过期
     */
    private void onTimExpiredError() {
        VidStsUtil.getVidSts(PlayParameter.PLAY_PARAM_VID, new RetryExpiredSts(this));
    }

    private static class MyShowMoreClickLisener implements ControlView.OnShowMoreClickListener {
        WeakReference<PlayerSkinActivity> weakReference;

        MyShowMoreClickLisener(PlayerSkinActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void showMore() {
            PlayerSkinActivity activity = weakReference.get();
            if (activity != null) {
                long currentClickTime = System.currentTimeMillis();
                // 防止快速点击
                if (currentClickTime - activity.oldTime <= 1000) {
                    return;
                }
                activity.oldTime = currentClickTime;
                activity.showMore(activity);
            }

        }
    }

    private void showMore(final PlayerSkinActivity activity) {
        showMoreDialog = new AlivcShowMoreDialog(activity);
        AliyunShowMoreValue moreValue = new AliyunShowMoreValue();
        moreValue.setSpeed(mAliyunVodPlayerView.getCurrentSpeed());
        moreValue.setVolume((int) mAliyunVodPlayerView.getCurrentVolume());

        ShowMoreView showMoreView = new ShowMoreView(activity, moreValue);
        showMoreDialog.setContentView(showMoreView);
        showMoreDialog.show();
        showMoreView.setOnDownloadButtonClickListener(new ShowMoreView.OnDownloadButtonClickListener() {
            @Override
            public void onDownloadClick() {
                long currentClickTime = System.currentTimeMillis();
                // 防止快速点击
                if (currentClickTime - downloadOldTime <= 1000) {
                    return;
                }
                downloadOldTime = currentClickTime;
                // 点击下载
                showMoreDialog.dismiss();
                if ("url".equals(PlayParameter.PLAY_PARAM_TYPE) || "localSource".equals(PlayParameter.PLAY_PARAM_TYPE)) {
                    FixedToastUtils.show(activity, getResources().getString(R.string.alivc_video_not_support_download));
                    return;
                }
                mCurrentDownloadScreenMode = AliyunScreenMode.Full;
                showAddDownloadView = true;
                if (mAliyunVodPlayerView != null) {
                    MediaInfo currentMediaInfo = mAliyunVodPlayerView.getCurrentMediaInfo();
                    if (currentMediaInfo != null && currentMediaInfo.getVideoId().equals(PlayParameter.PLAY_PARAM_VID)) {
                        VidSts vidSts = new VidSts();
                        vidSts.setVid(PlayParameter.PLAY_PARAM_VID);
                        vidSts.setRegion(PlayParameter.PLAY_PARAM_REGION);
                        vidSts.setAccessKeyId(PlayParameter.PLAY_PARAM_AK_ID);
                        vidSts.setAccessKeySecret(PlayParameter.PLAY_PARAM_AK_SECRE);
                        vidSts.setSecurityToken(PlayParameter.PLAY_PARAM_SCU_TOKEN);
                        downloadManager.prepareDownload(vidSts);
                    }
                }
            }
        });


        showMoreView.setOnSpeedCheckedChangedListener(new ShowMoreView.OnSpeedCheckedChangedListener() {
            @Override
            public void onSpeedChanged(RadioGroup group, int checkedId) {
                // 点击速度切换
                if (checkedId == R.id.rb_speed_normal) {
                    mAliyunVodPlayerView.changeSpeed(SpeedValue.One);
                } else if (checkedId == R.id.rb_speed_onequartern) {
                    mAliyunVodPlayerView.changeSpeed(SpeedValue.OneQuartern);
                } else if (checkedId == R.id.rb_speed_onehalf) {
                    mAliyunVodPlayerView.changeSpeed(SpeedValue.OneHalf);
                } else if (checkedId == R.id.rb_speed_twice) {
                    mAliyunVodPlayerView.changeSpeed(SpeedValue.Twice);
                }

            }
        });

        /**
         * 初始化亮度
         */
        if (mAliyunVodPlayerView != null) {
            showMoreView.setBrightness(mAliyunVodPlayerView.getScreenBrightness());
        }
        // 亮度seek
        showMoreView.setOnLightSeekChangeListener(new ShowMoreView.OnLightSeekChangeListener() {
            @Override
            public void onStart(SeekBar seekBar) {

            }

            @Override
            public void onProgress(SeekBar seekBar, int progress, boolean fromUser) {
                setWindowBrightness(progress);
                if (mAliyunVodPlayerView != null) {
                    mAliyunVodPlayerView.setScreenBrightness(progress);
                }
            }

            @Override
            public void onStop(SeekBar seekBar) {

            }
        });

        /**
         * 初始化音量
         */
        if (mAliyunVodPlayerView != null) {
            showMoreView.setVoiceVolume(mAliyunVodPlayerView.getCurrentVolume());
        }
        showMoreView.setOnVoiceSeekChangeListener(new ShowMoreView.OnVoiceSeekChangeListener() {
            @Override
            public void onStart(SeekBar seekBar) {

            }

            @Override
            public void onProgress(SeekBar seekBar, int progress, boolean fromUser) {
                mAliyunVodPlayerView.setCurrentVolume(progress / 100.00f);
            }

            @Override
            public void onStop(SeekBar seekBar) {

            }
        });

    }

    private static class MyOnScreenBrightnessListener implements AliyunVodPlayerView.OnScreenBrightnessListener {

        private WeakReference<PlayerSkinActivity> weakReference;

        public MyOnScreenBrightnessListener(PlayerSkinActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void onScreenBrightness(int brightness) {
            PlayerSkinActivity aliyunPlayerSkinActivity = weakReference.get();
            if (aliyunPlayerSkinActivity != null) {
                aliyunPlayerSkinActivity.setWindowBrightness(brightness);
                if (aliyunPlayerSkinActivity.mAliyunVodPlayerView != null) {
                    aliyunPlayerSkinActivity.mAliyunVodPlayerView.setScreenBrightness(brightness);
                }
            }
        }
    }

    /**
     * 播放器出错监听
     */
    private static class MyOnErrorListener implements IPlayer.OnErrorListener {

        private WeakReference<PlayerSkinActivity> weakReference;

        public MyOnErrorListener(PlayerSkinActivity activity) {
            weakReference = new WeakReference<>(activity);
        }

        @Override
        public void onError(com.aliyun.player.bean.ErrorInfo errorInfo) {
            PlayerSkinActivity aliyunPlayerSkinActivity = weakReference.get();
            if (aliyunPlayerSkinActivity != null) {
                aliyunPlayerSkinActivity.onError(errorInfo);
            }
        }
    }


    private AliyunVodPlayerView.LivePlayListener mLivePlayListener = new AliyunVodPlayerView.LivePlayListener() {

        @Override
        public void livePlayError(String msg) {
            ToastUtil.error(msg);
        }

    };

    private AliyunVodPlayerView.LiveEndListener liveEndListener = new AliyunVodPlayerView.LiveEndListener() {
        @Override
        public void liveEnd() {
            if (mLiveRoom.status == 1) {
                APIManager.startRequest(mILiveService.getLive(mLiveId), new BaseRequestListener<LiveRoom>(PlayerSkinActivity.this) {
                    @Override
                    public void onSuccess(LiveRoom result) {
                        if (result.status != 1 && !mIsLive) {
                            mIsLive = true;
                            LiveManager.toLiveEnd(PlayerSkinActivity.this, mLiveId, mLiveUser.headImage,
                                    mLiveUser.nickName, true, mLiveUser.followStatus);
                            finish();
                        }
                    }
                }, PlayerSkinActivity.this);
            }
        }
    };

    private static class MyCompletionListener implements IPlayer.OnCompletionListener {

        private WeakReference<PlayerSkinActivity> activityWeakReference;

        public MyCompletionListener(PlayerSkinActivity skinActivity) {
            activityWeakReference = new WeakReference<PlayerSkinActivity>(skinActivity);
        }

        @Override
        public void onCompletion() {

            PlayerSkinActivity activity = activityWeakReference.get();
            if (activity != null) {
                activity.onCompletion();
            }
        }
    }

    private void onCompletion() {
        logStrs.add(format.format(new Date()) + getString(com.aliyun.vodplayer.R.string.log_play_completion));

        FixedToastUtils.show(PlayerSkinActivity.this.getApplicationContext(), com.aliyun.vodplayer.R.string.toast_play_compleion);

        // 当前视频播放结束, 播放下一个视频
        onNext();
    }

    /**
     * 播放下一个视频
     */
    private void onNext() {
        if (currentError == ErrorInfo.UnConnectInternet) {
            // 此处需要判断网络和播放类型
            // 网络资源, 播放完自动波下一个, 无网状态提示ErrorTipsView
            // 本地资源, 播放完需要重播, 显示Replay, 此处不需要处理
            mAliyunVodPlayerView.showErrorTipView(4014, "-1", "当前网络不可用");
            return;
        }

        currentVideoPosition++;
        if (currentVideoPosition > mLiveRoom.recordUrl.size() - 1) {
            //列表循环播放，如发现播放完成了从列表的第一个开始重新播放
            currentVideoPosition = 0;
        }

        if (mLiveRoom.recordUrl.size() > 0) {
            RecordUrl recordUrl = mLiveRoom.recordUrl.get(currentVideoPosition);
            if (null != recordUrl) {
                mPlayUrl = recordUrl.recordUrl;
                setPlaySource();
            }
        }
    }


    private void onError(com.aliyun.player.bean.ErrorInfo errorInfo) {
        //鉴权过期
        if (errorInfo.getCode().getValue() == ErrorCode.ERROR_SERVER_POP_UNKNOWN.getValue()) {
            mIsTimeExpired = true;
        }
    }

    @OnClick({R.id.ivCloseLive, R.id.flCloseLive})
    protected void closeLive() {
        showOuTDialog();
    }

    @OnClick({R.id.rlNoLogin})
    protected void goLogin() {
        UiUtils.checkUserLogin(this);
    }

    @OnClick({R.id.tvLiveUser, R.id.ivUser1, R.id.ivUser2, R.id.ivUser3})
    protected void showLiveUser() {
        LiveUserDialog mLiveUserDialog = new LiveUserDialog(PlayerSkinActivity.this, mLiveId);
        mLiveUserDialog.show();
    }

    @OnClick(R.id.layoutMessage)
    protected void sendMessage() {
        APIManager.startRequest(mILiveService.getBanningMessageStatus(mLiveId), new BaseRequestListener<BanningMessage>(PlayerSkinActivity.this) {
            @Override
            public void onSuccess(BanningMessage result) {
                if (!result.bannedMessageStatus) {
                    if (null == mLiveSendMessageDialog) {
                        User loginUser = SessionUtil.getInstance().getLoginUser();
                        mLiveSendMessageDialog = new LiveSendMessageDialog(PlayerSkinActivity.this);
                        mLiveSendMessageDialog.setSubmitListener(new LiveSendMessageDialog.OnSubmitListener() {
                            @Override
                            public void submit(String content) {
                                mLiveSendMessageDialog.dismiss();
                                ChatroomTextMessage chatroomTextMessage = new ChatroomTextMessage();
                                chatroomTextMessage.setContent(content);
                                chatroomTextMessage.setNickName(loginUser.nickname);
                                chatroomTextMessage.setHeadImage(loginUser.avatar);
                                ChatroomKit.sendMessage(chatroomTextMessage, true);
                            }
                        });
                    }
                    mLiveSendMessageDialog.show("发送", "");
                } else {
                    ToastUtil.error("很抱歉，您已被主播禁言");
                }
            }
        }, PlayerSkinActivity.this);

    }

    @OnClick(R.id.ivRedList)
    protected void redList() {
        mShowRedDialog = true;
        LiveRedListDialog redListDialog = new LiveRedListDialog(PlayerSkinActivity.this, mLiveId, LiveRedListDialog.PLAY, mLiveUser);
        redListDialog.show();
    }


    @OnClick(R.id.ivCouponList)
    protected void showCouponList() {
        mShowCouponDialog = true;
        APIManager.startRequest(mILiveService.getLiveCouponList(mLiveRoom.liveId, 1, 1),
                new BaseRequestListener<PaginationEntity<LiveCoupon, Object>>(PlayerSkinActivity.this) {
                    @Override
                    public void onSuccess(PaginationEntity<LiveCoupon, Object> result) {
                        if (result.list.size() > 0) {
                            LiveCouponListDialog liveCouponListDialog = new LiveCouponListDialog(PlayerSkinActivity.this, mLiveId, mLiveUser, LiveCouponListDialog.COUPON_PLAY);
                            liveCouponListDialog.show();
                        } else {
                            LiveCouponListNoDataDialog liveCouponListNoDataDialog = new LiveCouponListNoDataDialog(PlayerSkinActivity.this);
                            liveCouponListNoDataDialog.show();
                        }
                    }
                }, PlayerSkinActivity.this);

    }

    @OnClick({R.id.tvLiuChang, R.id.tvGaoQing, R.id.tvChaoQing})
    protected void qingxi(View view) {
        switch (view.getId()) {
            case R.id.tvLiuChang:
                mTvQingXi.setText("流畅");
                mTvLiuChang.setSelected(true);
                mTvGaoQing.setSelected(false);
                mTvChaoQing.setSelected(false);
                mPlayUrl = mLiveRoom.pullUrl.rtmpUrlLld;
                setPlaySource();
                break;
            case R.id.tvGaoQing:
                mTvQingXi.setText("高清");
                mTvLiuChang.setSelected(false);
                mTvGaoQing.setSelected(true);
                mTvChaoQing.setSelected(false);
                mPlayUrl = mLiveRoom.pullUrl.rtmpUrlLsd;
                setPlaySource();
                break;
            case R.id.tvChaoQing:
                mTvQingXi.setText("超清");
                mTvLiuChang.setSelected(false);
                mTvGaoQing.setSelected(false);
                mTvChaoQing.setSelected(true);
                mPlayUrl = mLiveRoom.pullUrl.rtmpUrlLud;
                setPlaySource();
                break;
            default:
        }
        mLayoutSelectQingXi.setVisibility(View.GONE);
    }

    @OnClick(R.id.tvQingXi)
    protected void showQingXi() {
        mLayoutSelectQingXi.setVisibility(mLayoutSelectQingXi.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
    }

    @OnClick(R.id.ivShare)
    protected void share() {
        if (UiUtils.checkUserLogin(PlayerSkinActivity.this) && null != mLiveRoom) {
            mLiveShareDialog = null;
            if (null == mLiveShareDialog) {
                User loginUser = SessionUtil.getInstance().getLoginUser();
                mLiveShareDialog = new LiveShareDialog(PlayerSkinActivity.this,
                        mLiveRoom.title, mLiveRoom.title, mLiveRoom.liveImage, mLiveId, loginUser.invitationCode, mLiveRoom.nickName);
            }
            mLiveShareDialog.show();
        }
    }

    @OnClick(R.id.ivMore)
    protected void more() {
        mMorePopupView.findViewById(R.id.layoutQingXi).setVisibility(View.GONE);
        mMorePopupView.findViewById(R.id.layoutTool).setVisibility(View.VISIBLE);
        mMoreWindowManage.showWindow(mViewMoreLine, mMorePopupView);
    }

    @OnClick({R.id.tvFollow, R.id.ivFollow})
    protected void follow() {
        if (mLiveUser.followStatus == 0) {
            followAnchor();
        } else {
            userInfo();
        }
    }

    @OnClick({R.id.layoutUserInfo, R.id.ivAvatar})
    protected void userInfo() {
        if (null == mLiveUserInfoDialog) {
            mLiveUserInfoDialog = new LiveUserInfoDialog(PlayerSkinActivity.this, followOnClickListener);
        }
        getLiveUserInfo(true);
    }

    @OnClick(R.id.ivLike)
    protected void like() {
        floatingIv.clickStart();
        if (null == mLiveRoom || mLiveRoom.likeStatus == 1) {
            return;
        }
        APIManager.startRequest(mILiveService.likeLive(mLiveRoom.liveId),
                new BaseRequestListener<Object>(PlayerSkinActivity.this) {
                    @Override
                    public void onSuccess(Object result) {
                        mIvLike.setSelected(true);
                        mLiveRoom.likeStatus = 1;
                        getLiveUserInfo(false);
                    }
                }, PlayerSkinActivity.this);
    }

    @OnClick(R.id.ivProduct)
    protected void proudct() {
        if (null == mLiveProductDialog) {
            mLiveProductDialog = new LiveProductDialog(PlayerSkinActivity.this, mLiveId, true);
        }
        mLiveProductDialog.show();
        mLiveProductDialog.getLiveSkus(true);
    }


    private LiveUserInfoDialog.FollowOnClickListener followOnClickListener = new LiveUserInfoDialog.FollowOnClickListener() {
        @Override
        public void follow() {
            if (mLiveUser.followStatus == 0) {
                followAnchor();
            } else {
                cancelFollowAnchor();
            }
        }
    };

    /**
     * 关注主播
     */
    private void followAnchor() {
        APIManager.startRequest(mILiveService.followAnchor(mMemberId, mLiveId), new BaseRequestListener<Object>(PlayerSkinActivity.this) {
            @Override
            public void onSuccess(Object result) {
                getLiveUserInfo(false);
            }
        }, PlayerSkinActivity.this);
    }


    /**
     * 取消关注主播
     */
    private void cancelFollowAnchor() {
        APIManager.startRequest(mILiveService.cancelFollowAnchor(mMemberId), new BaseRequestListener<Object>(PlayerSkinActivity.this) {
            @Override
            public void onSuccess(Object result) {
                getLiveUserInfo(false);
            }
        }, PlayerSkinActivity.this);
    }


    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (event != null && event.getKeyCode() == 67) {
            if (mAliyunVodPlayerView != null) {
                //删除按键监听,部分手机在EditText没有内容时,点击删除按钮会隐藏软键盘
                return false;
            }
        } else if (event != null && event.getKeyCode() == 4) {
            //回退键的KeyCode是4.
            if (null != mOutLiveDialog && mOutLiveDialog.isShowing()) {
                return false;
            }
            showOuTDialog();
            return false;
        }
        return super.dispatchKeyEvent(event);
    }

    private void showOuTDialog() {
        if (null == mOutLiveDialog) {
            mOutLiveDialog = new WJDialog(PlayerSkinActivity.this);
        }
        if (mOutLiveDialog.isShowing()) {
            return;
        }
        mOutLiveDialog.show();
        mOutLiveDialog.setContentText("您确定离开直播室吗？");
        mOutLiveDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean login = SessionUtil.getInstance().isLogin();
                if (login) {
                    outLive();
                } else {
                    finish();
                }
            }
        });
    }

    /**
     * 离开直播间
     */
    private void outLive() {
        APIManager.startRequest(mILiveService.outLive(mLiveId), new BaseRequestListener<Object>(PlayerSkinActivity.this) {
            @Override
            public void onSuccess(Object result) {
                mOutLiveDialog.dismiss();
                finish();
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mOutLiveDialog.dismiss();
                finish();
            }

            @Override
            public void onComplete() {
                super.onComplete();
                if (mOutLiveDialog.isShowing()){
                    mOutLiveDialog.dismiss();
                    finish();
                }
            }
        }, PlayerSkinActivity.this);

    }


    /**
     * 获取观众
     */
    private void getLiveAudienceList(boolean isAddMessage, ChatroomWelcomeMessage content) {
        mIvUserOne.setVisibility(View.GONE);
        mIvUserTwo.setVisibility(View.GONE);
        mIvUserThree.setVisibility(View.GONE);
        mIvUserFour.setVisibility(View.GONE);
        mTvLiveUser.setVisibility(View.GONE);
        APIManager.startRequest(mILiveService.getLiveAudienceList(mLiveId, 1, 5), new BaseRequestListener<PaginationEntity<LiveUser, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<LiveUser, Object> result) {
                if (result.list.size() == 0) {
                    return;
                }

                if (isAddMessage) {
                    LiveMessage liveMessage = new LiveMessage();
                    liveMessage.name = content.getNickName();
                    liveMessage.headImage = content.getHeadImage();
                    liveMessage.isWelcome = true;
                    liveMessage.content = String.format("等%s位进入房间", mLiveUserTotal);
                    mLiveChatMessageAdapter.addData(liveMessage);
                    mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);
                }

                switch (result.list.size()) {
                    case 1:
                        mIvUserOne.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserOne, result.list.get(0).headImage);
                        break;
                    case 2:
                        mIvUserOne.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserOne, result.list.get(0).headImage);
                        mIvUserTwo.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserTwo, result.list.get(1).headImage);
                        break;
                    case 4:
                        mIvUserOne.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserOne, result.list.get(0).headImage);
                        mIvUserTwo.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserTwo, result.list.get(1).headImage);
                        mIvUserThree.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserThree, result.list.get(2).headImage);
                        mIvUserFour.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserFour, result.list.get(3).headImage);
                        break;
                    default:
                        mIvUserOne.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserOne, result.list.get(0).headImage);
                        mIvUserTwo.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserTwo, result.list.get(1).headImage);
                        mIvUserThree.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserThree, result.list.get(2).headImage);

                }
                mLiveUserTotal = Math.max(result.total, mLiveUserTotal);

                mTvLiveUser.setText(String.format("观众\n%s", result.total));
                mTvLiveUser.setVisibility(result.total > 4 ? View.VISIBLE : View.GONE);
            }
        }, PlayerSkinActivity.this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case liveUserRefresh:
//                getLiveAudienceList();
                break;
            case dialogDismiss:
                mShowRedDialog = false;
                break;
            case couponDialogDismiss:
                mShowCouponDialog = false;
                break;
            case couponDialogShow:
                mShowCouponDialog = true;
                break;
            case logout:
                finish();
                break;
            case loginSuccess:
                rlNoLogin.setVisibility(View.GONE);
                initData();
                break;
            case goToLoginRongIM:
                User loginUser = SessionUtil.getInstance().getLoginUser();
                LiveManager.joinChatRoom(PlayerSkinActivity.this, mLiveId, loginUser.nickname, loginUser.avatar, mLiveUserTotal, true);
                break;
            case networkConnected:
                mIsNeetReconnect = false;
                mAliyunVodPlayerView.reTry();
                break;
            case networkDisconnected:
                mIsNeetReconnect = true;
                break;
            case openNewActivity:
                if (mStatus == 1){
                    if (!requestOverlayPermission()) {
                        showFloatingWindow();
                    } else {
                        ToastUtil.error("请开启悬浮窗权限");
                    }
                }
                break;
            case loginError:
                if (null != mOutLiveDialog) {
                    mOutLiveDialog.dismiss();
                }
                finish();
            default:
        }
    }

    private CountDownTimer mCountDownTimer;

    private void startOnLineUpdateState() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        mCountDownTimer = new CountDownTimer(Long.MAX_VALUE, 30000) {
            @Override
            public void onTick(long millisUntilFinished) {
                APIManager.startRequest(mILiveService.sendOutLine(mLiveId), new BaseRequestListener<Object>() {
                    @Override
                    public void onSuccess(Object result) {
                        // no -op
                        Log.d("Seven", "setOutLine");
                    }
                }, PlayerSkinActivity.this);
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    private void stopOnLineUpdateState() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }
}
