package com.weiju.ccmall.module.balance;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.utils.TimeUtils;
import com.github.mikephil.charting.charts.LineChart;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.ExchGoldTicket;
import com.weiju.ccmall.shared.bean.ExchRatioTrend;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBalanceService;
import com.weiju.ccmall.shared.util.ChartUtils;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/4/29 on 10:07
 * @desc 兑换金券
 */
public class ExchGoldenTicketActivity extends BaseActivity {

    @BindView(R.id.tvDayRatio)
    TextView mTvDayRatio;
    @BindView(R.id.tvCoin)
    TextView mTvCoin;
    @BindView(R.id.etExchCoinNum)
    EditText mEtExchCoinNum;
    @BindView(R.id.tvGoldTicket)
    TextView mTvGoldTicket;
    @BindView(R.id.tvExch)
    TextView mTvExch;
    @BindView(R.id.chart)
    LineChart mChart;

    private IBalanceService mBalanceService = ServiceManager.getInstance().createService(IBalanceService.class);
    private ExchGoldTicket mExchGoldTicket;
    private long apiCoinRatio = 100000000;
    private int lineColor[] = new int[]{R.color.red};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exch_golden_ticket);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);
        initView();
        initData();

    }

    private void initData() {
        exchRatio();
        exchRatioTrend();
    }

    private void initView() {
        setLeftBlack();
        setTitle("兑换金券");
        mEtExchCoinNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mExchGoldTicket == null) {
                    return;
                }
                if (s.length() > 0 && !s.toString().equals(".")) {
                    double coinD = Double.parseDouble(s.toString());
                     long coinL = (long)coinD*apiCoinRatio;
                     if (coinL>mExchGoldTicket.coin){
                         ToastUtil.error("您的可兑换CCM不足");
                         String content = s.toString().substring(0, s.length() - 1);
                         mEtExchCoinNum.setText(content);
                         mEtExchCoinNum.setSelection(content.length());
                         return;
                     }
                    long goldTicketL = (long) (coinD * mExchGoldTicket.dayRatio);
                    setGoldTicket(MoneyUtil.centToYuanStrNoZero(goldTicketL));
                } else {
                    setGoldTicket("0");

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }


    private void setExchRatioTrendData(List<ExchRatioTrend> dataExchRatioTrend) {
        List<String> xAxisValue = new ArrayList<>();
        ArrayList<List<Long>> yXAxisValues = new ArrayList<>();
        List<Long> lineData = new ArrayList<>();
        for (ExchRatioTrend exchRatioTrend : dataExchRatioTrend) {
            lineData.add(exchRatioTrend.dayRatio);
            String date = TimeUtils.date2String(TimeUtils.string2Date(exchRatioTrend.day, "yyyyMMdd"), "MM月dd");
            xAxisValue.add(date);
        }
        yXAxisValues.add(lineData);
        ChartUtils.setLinesChart(ExchGoldenTicketActivity.this, mChart, xAxisValue, null, yXAxisValues, lineColor);
    }

    /**
     * 兑换为金券的比例
     */
    private void exchRatio() {
        APIManager.startRequest(mBalanceService.exchRatio(), new BaseRequestListener<ExchGoldTicket>(this) {
            @SuppressLint("DefaultLocale")
            @Override
            public void onSuccess(ExchGoldTicket exchGoldTicket) {
                mExchGoldTicket = exchGoldTicket;
                mTvDayRatio.setText(String.format("1CCM可以兑换%s金券", MoneyUtil.centToYuanStrNoZero(exchGoldTicket.dayRatio)));
                mTvCoin.setText(String.format("可兑换的CCM%s", MoneyUtil.coinToYuanStrNoZero(exchGoldTicket.coin)));
//                mEtExchCoinNum.setText(MoneyUtil.coinToYuanStrNoZero(exchGoldTicket.coin));
                mEtExchCoinNum.setText("");
//                setGoldTicket(MoneyUtil.centToYuanStrNoZero(exchGoldTicket.goldTicket));
                setGoldTicket("0");
            }
        },this);
    }

    /**
     * 兑换比例趋势
     */
    private void exchRatioTrend() {
        APIManager.startRequest(mBalanceService.exchRatioTrend(), new BaseRequestListener<List<ExchRatioTrend>>(this) {
            @SuppressLint("DefaultLocale")
            @Override
            public void onSuccess(List<ExchRatioTrend> exchRatioTrends) {
                setExchRatioTrendData(exchRatioTrends);
            }
        },this);
    }


    private void setGoldTicket(String goldTicket) {
        mTvGoldTicket.setText(Html.fromHtml(String.format("可兑换<font color =\"#f51861\">%s</font>金券", goldTicket)));
    }

    /**
     * 兑换为金券
     */
    @OnClick(R.id.tvExch)
    protected void exch() {
        String coin = mEtExchCoinNum.getText().toString();
        if (TextUtils.isEmpty(coin)) {
            ToastUtil.error("清输入需要兑换的CCM");
            return;
        }




        ToastUtil.showLoading(ExchGoldenTicketActivity.this);
        double coinD = Double.parseDouble(coin);
        long coinL = (long) (coinD * apiCoinRatio);

        Intent intent = new Intent(ExchGoldenTicketActivity.this,ExchGoldenTicketPassWordActivity.class);
        intent.putExtra("coin",coinL);
        startActivity(intent);
    }



    @OnClick(R.id.tvAllExch)
    protected void allExch() {
        if (mExchGoldTicket != null) {
            String coin = MoneyUtil.coinToYuanStrNoZero(mExchGoldTicket.coin);
            mEtExchCoinNum.setText(coin);
            mEtExchCoinNum.setSelection(coin.length());
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void transferHandler(EventMessage message) {
        switch (message.getEvent()){
            case exchSuccess:
//                initData();
                finish();
                break;
            default:
        }
    }

}
