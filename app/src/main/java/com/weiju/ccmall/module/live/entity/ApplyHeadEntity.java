package com.weiju.ccmall.module.live.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/4/17.
 */
public class ApplyHeadEntity {

    /**
     * applyBean : {"mchInId":"c9b67b1b671b4e5880b16f4d836f70e8","phone":"15070981234","mail":"1234@qq.com","supplierPrincipal":"af8d785b384ad9281286275095141f15","categoryId":"62d29248b1da4e409bec5d8aba864eaf","storeName":"某某","storeBusinessProvince":"上海市","storeBusinessCity":"上海市","storeBusinessArea":"浦东区","storeBusinessAddr":"哈哈哈哈","createDate":"2020-04-16 19:42:31.0","updateDate":"2020-04-16 19:42:31.0","deleteFlag":"0"}
     * categroyName : 美妆护肤
     */

    @SerializedName("applyBean")
    public ApplyInfoEntity applyBean;
    @SerializedName("categroyName")
    public String categroyName;

}
