package com.weiju.ccmall.module.live.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.bigkoo.pickerview.TimePickerView;
import com.blankj.utilcode.utils.KeyboardUtils;
import com.blankj.utilcode.utils.TimeUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.ICommunityService;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.AddLive;
import com.weiju.ccmall.shared.bean.Image;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.UploadResponse;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.body.AddLiveBody;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.LiveProductDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.manager.UploadManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.service.contract.IUploadService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.PermissionsUtils;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.PicassoEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.functions.Action1;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;


/**
 * @author chenyanming
 * @time 2019/11/21 on 16:36
 * @desc ${TODD}
 */
public class CreatLiveRoomActivity extends BaseActivity {

    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.ivAvatar)
    SimpleDraweeView mIvAvatar;
    @BindView(R.id.tvToSelectFile)
    TextView mTvToSelectFile;
    @BindView(R.id.tvSelectFile)
    TextView mTvSelectFile;
    @BindView(R.id.etTitle)
    EditText mEtTitle;
    @BindView(R.id.etPassword)
    EditText mEtPassword;
    @BindView(R.id.radioPasswd)
    AppCompatRadioButton radioPasswd;
    @BindView(R.id.radioTime)
    AppCompatRadioButton radioTime;

    @BindView(R.id.tvCommit)
    TextView tvCommit;
    @BindView(R.id.tvRelate)
    TextView tvRelate;


    private static int REQUEST_AVATAR_CHOOSE = 1000;
    private static int REQUEST_FILE_CHOOSE = 1001;
    private Uri mAvatarUri;
    private Uri mFileUri;

    private ICommunityService mPageService = ServiceManager.getInstance().createService(ICommunityService.class);
    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);
    private IUploadService mUploadService = ServiceManager.getInstance().createService(IUploadService.class);

    private boolean mIsFirstUp;
    private String mAvatarUrl = "";
    private String mFileUrl = "";
    private Date mForecastTime;
    private AddLive mAddLive;
    private String mLiveId;
    private boolean mShopkeeper;
    private List<String> mSkuIds = new ArrayList<>();
    //商品弹窗
    private LiveProductDialog mLiveProductDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        EventBus.getDefault().register(this);

        //让布局向上移来显示软键盘
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setContentView(R.layout.activity_creat_live_room);
        ButterKnife.bind(this);

        initIntent();
        initView();
        initData();

    }


    private void initData() {
        if (!TextUtils.isEmpty(mLiveId)) {
            getLiveDetail();
        }
    }

    private void initIntent() {
        mLiveId = getIntent().getStringExtra("liveId");
        mShopkeeper = getIntent().getBooleanExtra("shopkeeper", false);
    }

    private void getLiveDetail() {
        APIManager.startRequest(mILiveService.getLive(mLiveId), new BaseRequestListener<LiveRoom>(CreatLiveRoomActivity.this) {
            @Override
            public void onSuccess(LiveRoom result) {
                if (!TextUtils.isEmpty(result.liveImage)) {
                    FrescoUtil.setImage(mIvAvatar, result.liveImage);
                }

                mEtTitle.setText(result.title);
                if (!TextUtils.isEmpty(result.startTime)) {
                    mForecastTime = TimeUtils.string2Date(result.startTime);
                    tvTime.setText(TimeUtils.date2String(mForecastTime, "yyyy-MM-dd HH:mm"));
                }
                mTvSelectFile.setVisibility(TextUtils.isEmpty(result.liveDoc) ? View.GONE : View.VISIBLE);
                mTvToSelectFile.setVisibility(TextUtils.isEmpty(result.liveDoc) ? View.VISIBLE : View.GONE);
                mAvatarUrl = result.liveImage;
            }
        }, CreatLiveRoomActivity.this);
    }

    private void initView() {
        mEtPassword.setEnabled(false);
        radioPasswd.setOnCheckedChangeListener((buttonView, isChecked) -> {
            mEtPassword.setEnabled(isChecked);
            mEtPassword.setHint(isChecked ? "请设置密码" : "不设置密码");
            if (isChecked) {
                mEtPassword.requestFocus();
            }
        });

        tvTime.setEnabled(false);
        radioTime.setOnCheckedChangeListener((buttonView, isChecked) -> {
            tvTime.setEnabled(isChecked);
            tvTime.setText("");
            tvTime.setHint(isChecked ? "请选择预告开播时间" : "不创建预告");
            if (isChecked) {
                tvCommit.setText("创建预告");
                KeyboardUtils.hideSoftInput(this);
            } else {
                tvCommit.setText("发起直播");
            }
        });

        setLeftBlack();
        setTitle("创建直播");
    }

    @OnClick(R.id.tvTime)
    protected void selectTime() {
        KeyboardUtils.hideSoftInput(CreatLiveRoomActivity.this);
        int colorGray = getResources().getColor(R.color.text_gray);
        int colorBlack = getResources().getColor(R.color.text_black);
        int colorRed = getResources().getColor(R.color.red_live);
        Calendar startDate = Calendar.getInstance();
        startDate.setTimeInMillis(System.currentTimeMillis());

        Calendar endDate = Calendar.getInstance();
        endDate.set(2200, 12, 30);
        TimePickerView pvTime = new TimePickerView.Builder(
                CreatLiveRoomActivity.this,
                mListener)
                .setType(TimePickerView.Type.YEAR_MONTH_DAY_HOUR_MIN)
                .setSubmitColor(colorRed)
                .setCancelColor(colorGray)
                .setTextColorOut(colorGray)
                .setTextColorCenter(colorBlack)
                .setRangDate(startDate, endDate)
                .setTitleColor(colorGray)
                .setDate(startDate)
                .build();
        pvTime.setDate(Calendar.getInstance());
        pvTime.show();
    }

    private TimePickerView.OnTimeSelectListener mListener = new TimePickerView.OnTimeSelectListener() {
        @Override
        public void onTimeSelect(Date date, View v) {
            mForecastTime = date;
            tvTime.setText(TimeUtils.date2String(date, "yyyy-MM-dd HH:mm"));
        }
    };


    @OnClick(R.id.ivAvatar)
    protected void selectAvatar() {
        RxPermissions rxPermissions = new RxPermissions(CreatLiveRoomActivity.this);
        rxPermissions.request(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_PHONE_STATE
        ).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (aBoolean) {
                    Matisse.from(CreatLiveRoomActivity.this)
                            .choose(MimeType.ofImage())
                            .captureStrategy(
                                    new CaptureStrategy(true, "com.globalmingpin.qqmp"))
                            .theme(R.style.Matisse_Dracula)
                            .countable(false)
                            .maxSelectable(1)
                            .imageEngine(new PicassoEngine())
                            .forResult(REQUEST_AVATAR_CHOOSE);
                } else {
                    PermissionsUtils.goPermissionsSetting(CreatLiveRoomActivity.this);
                    ToastUtil.error("发起直播需要开启相机权限");
                }
            }
        });

    }

    @OnClick(R.id.layoutSelectFile)
    protected void selectFile() {
        RxPermissions rxPermissions = new RxPermissions(CreatLiveRoomActivity.this);
        rxPermissions.request(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_PHONE_STATE
        ).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (aBoolean) {
                    Matisse.from(CreatLiveRoomActivity.this)
                            .choose(MimeType.ofImage())
                            .captureStrategy(
                                    new CaptureStrategy(true, "com.globalmingpin.qqmp"))
                            .theme(R.style.Matisse_Dracula)
                            .countable(false)
                            .maxSelectable(1)
                            .imageEngine(new PicassoEngine())
                            .forResult(REQUEST_FILE_CHOOSE);
                } else {
                    PermissionsUtils.goPermissionsSetting(CreatLiveRoomActivity.this);
                    ToastUtil.error("发起直播需要开启相机权限");
                }
            }
        });
    }

    @OnClick(R.id.tvRelate)
    protected void relate() {
        if (null == mLiveProductDialog) {
            mLiveProductDialog = new LiveProductDialog(this, mLiveId, false, mShopkeeper, true);
        }
        mLiveProductDialog.show();
    }

    @OnClick(R.id.tvCommit)
    protected void commit() {
        RxPermissions rxPermissions = new RxPermissions(CreatLiveRoomActivity.this);
        rxPermissions.request(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_PHONE_STATE
        ).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (aBoolean) {
                    checkData();
                } else {
                    PermissionsUtils.goPermissionsSetting(CreatLiveRoomActivity.this);
                    ToastUtil.error("发起直播需要开启相机权限");
                }
            }
        });


    }

    private void checkData() {
        String title = mEtTitle.getText().toString();
        String livePassword = mEtPassword.getText().toString();

        if (null == mAvatarUri) {
            ToastUtil.error("请选择封面");
            return;
        }

        if (TextUtils.isEmpty(title)) {
            ToastUtil.error("请输入直播间标题");
            return;
        }

        if (radioTime.isChecked() && null == mForecastTime) {
            ToastUtil.error("请选择直播时间");
            return;
        }

        if (radioPasswd.isChecked() && TextUtils.isEmpty(livePassword)) {
            ToastUtil.error("请输入密码");
            return;
        }


        if (null != mAvatarUri && null != mFileUri) {
            uploadImage();
        } else if (null != mAvatarUri) {
            uploadAvatarImage();
        } else if (null != mFileUri) {
            uploadFileImage();
        } else {
            creatRoom();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_AVATAR_CHOOSE && null != data) {
            ArrayList<Uri> uris = (ArrayList<Uri>) Matisse.obtainResult(data);
            mAvatarUri = uris.get(0);
            mIvAvatar.setImageURI(mAvatarUri);
        } else if (requestCode == REQUEST_FILE_CHOOSE && null != data) {
            ArrayList<Uri> uris = (ArrayList<Uri>) Matisse.obtainResult(data);
            mFileUri = uris.get(0);
            mTvToSelectFile.setVisibility(View.GONE);
            mTvSelectFile.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 上传两张图片
     */
    @SuppressLint("CheckResult")
    private void uploadImage() {
        ToastUtil.showLoading(CreatLiveRoomActivity.this);
        mIsFirstUp = true;
        MediaType mediaType = MediaType.parse("multipart/form-data");
        RequestBody version = RequestBody.create(MediaType.parse("multipart/form-data"), "1.0");

        File avatarFile = UploadManager.uri2File(mAvatarUri, CreatLiveRoomActivity.this);
        RequestBody requestAvatarFile = RequestBody.create(mediaType, avatarFile);
        MultipartBody.Part avatarBody = MultipartBody.Part.createFormData("file", avatarFile.getName(), requestAvatarFile);
        Observable<RequestResult<Image>> avatarObservable = mPageService.uploadImageNew(version, avatarBody);

        File fileUri = UploadManager.uri2File(mFileUri, CreatLiveRoomActivity.this);
        RequestBody requestFile = RequestBody.create(mediaType, fileUri);
        MultipartBody.Part fileBody = MultipartBody.Part.createFormData("file", fileUri.getName(), requestFile);
        Observable<RequestResult<Image>> fileObservable = mPageService.uploadImageNew(version, fileBody);

        if (avatarObservable != null && fileObservable != null) {
            Observable.concat(avatarObservable, fileObservable).subscribeOn(Schedulers.io()).
                    observeOn(AndroidSchedulers.mainThread()).subscribe(new Consumer<RequestResult<Image>>() {
                @Override
                public void accept(RequestResult<Image> imageRequestResult) throws Exception {
                    if (mIsFirstUp) {
                        mAvatarUrl = imageRequestResult.data.imgUrl;
                        mIsFirstUp = false;
                    } else {
                        mFileUrl = imageRequestResult.data.imgUrl;
                        creatRoom();
                    }
                }
            });
        }

    }

    /**
     * 上传封面图
     */
    @SuppressLint("CheckResult")
    private void uploadAvatarImage() {

        File avatarFile = UploadManager.uri2File(mAvatarUri, CreatLiveRoomActivity.this);
        Luban.with(CreatLiveRoomActivity.this)
                .load(avatarFile)                     //传人要压缩的图片
                .setCompressListener(new OnCompressListener() { //设置回调
                    @Override
                    public void onStart() {
                        // 压缩开始前调用，可以在方法内启动 loading UI
                        ToastUtil.showLoading(CreatLiveRoomActivity.this);
                    }

                    @Override
                    public void onSuccess(File file) {
                        RequestBody fileRequestBody = RequestBody.create(MediaType.parse("image"), file);
                        MultipartBody.Part fileBody = MultipartBody.Part.createFormData("file", file.getName(), fileRequestBody);
                        RequestBody versionRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), Constants.API_VERSION);

                        APIManager.startRequest(mUploadService.uploadImage(fileBody, versionRequestBody), new BaseRequestListener<UploadResponse>(CreatLiveRoomActivity.this) {
                            @Override
                            public void onSuccess(UploadResponse result) {
                                mAvatarUrl = result.url;
                                creatRoom();
                            }
                        }, CreatLiveRoomActivity.this);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtil.hideLoading();
                        ToastUtil.error("上传图片出错");
                        // 当压缩过程出现问题时调用
                    }
                }).launch();

    }

    /**
     * 上传文件图片
     */
    private void uploadFileImage() {
        MediaType mediaType = MediaType.parse("multipart/form-data");
        RequestBody version = RequestBody.create(MediaType.parse("multipart/form-data"), "1.0");

        File fileUri = UploadManager.uri2File(mFileUri, CreatLiveRoomActivity.this);
        RequestBody requestFile = RequestBody.create(mediaType, fileUri);
        MultipartBody.Part fileBody = MultipartBody.Part.createFormData("file", fileUri.getName(), requestFile);

        APIManager.startRequest(mPageService.uploadImageNew(version, fileBody), new BaseRequestListener<Image>(CreatLiveRoomActivity.this) {
            @Override
            public void onSuccess(Image result) {
                mFileUrl = result.imgUrl;
                creatRoom();
            }
        }, CreatLiveRoomActivity.this);
    }

    /**
     * 创建房间
     */
    private void creatRoom() {
        if (UiUtils.checkUserLogin(CreatLiveRoomActivity.this)) {
            String startTime = TimeUtils.date2String(new Date());
            String forecastTime = null;
            if (mForecastTime != null && radioTime.isChecked()) {
                forecastTime = TimeUtils.date2String(mForecastTime);
            }
            String title = mEtTitle.getText().toString();
            String livePassword = mEtPassword.getText().toString().trim();
            String url = TextUtils.isEmpty(mLiveId) ? "live/addLive" : "vhall/updateLive";
            if (mLiveProductDialog != null) {
                mSkuIds.clear();
                List<SkuInfo> infos = mLiveProductDialog.getData();
                for (SkuInfo info : infos) {
                    mSkuIds.add(info.skuId);
                }
            }

            AddLiveBody addLiveBody = new AddLiveBody(mLiveId, startTime, title, mFileUrl, mAvatarUrl, radioPasswd.isChecked() ? StringUtil.md5(livePassword) : "", mSkuIds);
            addLiveBody.setForecastTime(forecastTime);
            addLiveBody.setLivePasswordDecode(livePassword);
            APIManager.startRequest(mILiveService.addLive(url, addLiveBody), new BaseRequestListener<AddLive>(CreatLiveRoomActivity.this) {
                @Override
                public void onSuccess(AddLive result) {
                    mAddLive = result;
                    EventBus.getDefault().post(new EventMessage(Event.liveRoomChange));
                    if (radioTime.isChecked()) {
                        // 打开预告详情
                        LiveNoticeActivity.start(CreatLiveRoomActivity.this, true, result.liveId, mAvatarUri);
                        finish();
                    } else {
                        startLive(result.liveId, result.memberId);
                    }
                }
            }, CreatLiveRoomActivity.this);
        }
    }

    /**
     * 发起直播
     */
    private void startLive(String liveId, String memberId) {
        LivePushActivity.startActivity(CreatLiveRoomActivity.this, LiveManager.getAlivcLivePushConfig(CreatLiveRoomActivity.this),
                liveId, true, false, false, LiveManager.mOrientationEnum,
                Camera.CameraInfo.CAMERA_FACING_FRONT, false, "", "",
                false, LiveManager.getAlivcLivePushConfig(CreatLiveRoomActivity.this).isExternMainStream(), memberId);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case logout:
            case startLiveSuccess:
                finish();
                break;
            case loginError:
                EventBus.getDefault().post(new EventMessage(Event.logout));
                ToastUtil.error("登录失效，请重新登录");
                finish();
                break;
            case addLiveProduct:
                List<SkuInfo> chooseSkus = (List<SkuInfo>) message.getData();
                mLiveProductDialog.setData(chooseSkus);
                if (chooseSkus.size() > 0) {
                    tvRelate.setText(MessageFormat.format("已关联{0}件商品", chooseSkus.size()));
                } else {
                    tvRelate.setText("");
                }
                break;
            default:
        }
    }

    public static void start(Context context, boolean shopkeeper) {
        Intent intent = new Intent(context, CreatLiveRoomActivity.class);
        intent.putExtra("shopkeeper", shopkeeper);
        context.startActivity(intent);
    }

}
