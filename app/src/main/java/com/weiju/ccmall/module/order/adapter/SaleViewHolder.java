package com.weiju.ccmall.module.order.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderSaleDetailActivity;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SaleViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.avatarIv)
    SimpleDraweeView avatarIv;
    @BindView(R.id.contactBtn)
    TextView contactBtn;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.orderNo)
    TextView orderNo;
    @BindView(R.id.price)
    TextView price;
    @BindView(R.id.orderType)
    TextView orderType;
    @BindView(R.id.orderState)
    TextView orderState;
    @BindView(R.id.income)
    TextView income;
    @BindView(R.id.orderState2)
    TextView orderState2;

    private Order order;

    public static SaleViewHolder newItem(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sale_order, parent, false);
        return new SaleViewHolder(itemView);
    }

    private SaleViewHolder(@NonNull View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    @OnClick(R.id.contactBtn)
    public void onViewClicked() {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null && order != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    Context context = itemView.getContext();
                    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, context);
                    ChatInfo chatInfo = new ChatInfo();
                    chatInfo.setType(TIMConversationType.C2C);
                    chatInfo.setId(order.orderMain.memberId);
                    chatInfo.setChatName(order.orderMain.nickName);
                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.putExtra("chatInfo", chatInfo);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }

    public void bindView(Order order, View.OnClickListener clickListener) {
        itemView.setOnClickListener(v -> {
            clickListener.onClick(v);
            OrderSaleDetailActivity.start(itemView.getContext(), order.orderMain.orderCode);
        });
        name.setText(order.orderMain.nickName);
        FrescoUtil.setImageSmall(avatarIv, order.orderAssociatedHeadImage);
        if (order.orderMain.createDate.length() >= 16) {
            time.setText("于"+order.orderMain.createDate.substring(5, 16) + "下单");
        } else {
            time.setText("于"+order.orderMain.createDate + "下单");
        }
        orderNo.setText(order.orderMain.orderCode);
        price.setText(order.orderMain.totalProductMoney/100 + "元");
        orderType.setText("嗨购订单");
        orderState.setText(order.orderMain.orderStatusStr);
        orderState2.setText("("+profitStatusStr(order.memberProfitBean.status)+")");
        income.setText(String.format(Locale.getDefault(), "¥%.2f", order.memberProfitBean.profitMoney/(100.0)));
        this.order = order;
    }

    public static String profitStatusStr(int status) {
        String ret = "";
        switch (status) {
            case -1:
                ret = "未付款";
                break;
            case 0:
                ret = "未解冻";
                break;
            case 1:
                ret = "已解冻";
                break;
        }
        return ret;
    }
}
