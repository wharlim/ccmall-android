package com.weiju.ccmall.module.live.consts;

public class OpeningLiveType {
    public static final int LEADER = 0;
    public static final int VIP = 1;
    public static final int ANCHOR = 2;
}
