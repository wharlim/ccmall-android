package com.weiju.ccmall.module.pickUp.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.pay.PayActivity;
import com.weiju.ccmall.module.pickUp.adapter.PickUpDetailAdapter;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.OrderVouchers;
import com.weiju.ccmall.shared.bean.PickUp;
import com.weiju.ccmall.shared.bean.PickUpEx;
import com.weiju.ccmall.shared.bean.SkuAmount;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IPickUpService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/8/1 on 17:36
 * @desc ${TODD}
 */
public class PickUpDetailListActivity extends BaseListActivity {

    @BindView(R.id.tvTip)
    TextView mTvTip;
    @BindView(R.id.tvSelect)
    TextView mTvSelect;

    public static final int UN_USE = 0;
    public static final int PAY_USE = 1;
    private int mType;
    private PickUpDetailAdapter mAdapter = new PickUpDetailAdapter();
    private IPickUpService mService = ServiceManager.getInstance().createService(IPickUpService.class);
    private String mVouchersId;
    private ArrayList<SkuAmount> mCalcOrderCouponListBody;
    private IOrderService mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
    private int mSelectVoucher;

    @Override
    public String getTitleStr() {
        return mType == UN_USE ? "提货券列表" : "查看可用提货券列表";
    }

    @Override
    public void initView() {
        super.initView();
        EventBus.getDefault().register(this);
        mTvTip.setVisibility(mType == 0 ? View.GONE : View.VISIBLE);
        mTvSelect.setVisibility(mType == 0 ? View.GONE : View.VISIBLE);
        mTvSelect.setSelected(mSelectVoucher == PayActivity.NO_SELECT_VOUCHER);
        mTvSelect.setText(mSelectVoucher == PayActivity.NO_SELECT_VOUCHER ? "使用提货券" : "不使用提货券");
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.window_background));
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_pick_up_detail_list;
    }

    @Override
    public void getIntentData() {
        mType = getIntent().getIntExtra("type", UN_USE);
        mVouchersId = getIntent().getStringExtra("vouchersId");
        mSelectVoucher = getIntent().getIntExtra("select", PayActivity.NO_SELECT_VOUCHER);
        mCalcOrderCouponListBody = (ArrayList<SkuAmount>) getIntent().getSerializableExtra("body");
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        PickUp item = mAdapter.getItem(position);
        if (null != item) {
            Intent intent = new Intent(PickUpDetailListActivity.this, PickUpProductListActivity.class);
            intent.putExtra("vouchersId", item.vouchersId);
            startActivity(intent);
        }

    }

    @Override
    public boolean isLoadMore() {
        return mType == UN_USE;
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        if (mType == UN_USE) {
            APIManager.startRequest(mService.getUnusedList(mCurrentPage, 15, mVouchersId),
                    new BaseRequestListener<PaginationEntity<PickUp, PickUpEx>>(mRefreshLayout) {
                        @Override
                        public void onSuccess(PaginationEntity<PickUp, PickUpEx> result) {
                            if (mCurrentPage == 1) {
                                mAdapter.setNewData(result.list);
                            } else {
                                mAdapter.addData(result.list);
                            }
                            if (result.page >= result.totalPage) {
                                mAdapter.loadMoreEnd(isRefresh && result.list.size() < 5);
                            } else {
                                mAdapter.loadMoreComplete();
                            }
                        }
                    }, this);
        } else {
            APIManager.startRequest(mOrderService.calcOrderVouchersList(mCalcOrderCouponListBody),
                    new BaseRequestListener<OrderVouchers>(mRefreshLayout) {
                        @Override
                        public void onSuccess(OrderVouchers result) {
                            mAdapter.setNewData(result.list);
                        }
                    }, this);
        }

    }

    @OnClick(R.id.tvSelect)
    protected void select() {
        Intent intent = new Intent(this, PayActivity.class);
        intent.putExtra("select", mSelectVoucher == PayActivity.SELECT_VOUCHER ?
                PayActivity.NO_SELECT_VOUCHER : PayActivity.SELECT_VOUCHER);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void transferHandler(EventMessage message) {
        switch (message.getEvent()) {
            case createOrderSuccess:
                mCurrentPage = 1;
                getData(true);
                break;
            default:
        }
    }
}
