package com.weiju.ccmall.module.blockchain.transferout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.beans.BlockChainInfo;
import com.weiju.ccmall.module.blockchain.beans.Excess;
import com.weiju.ccmall.module.blockchain.events.TransferOutSuccess;
import com.weiju.ccmall.module.blockchain.utils.BlockChainUtil;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBlockChain;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 转出
 */
public class TransferOutActivity extends BaseActivity {

    @BindView(R.id.et_receiver_address)
    EditText etReceiverAddress;
    @BindView(R.id.et_transfer_coin)
    EditText etTransferCoin;
    @BindView(R.id.tv_available_balance)
    TextView tvAvailableBalance;
    @BindView(R.id.tv_fee)
    TextView tvFee;
    @BindView(R.id.tv_hint)
    TextView tvHint;
    @BindView(R.id.tv_commit)
    TextView tvCommit;
    @BindView(R.id.tv_hint_balance)
    TextView tvHintBalance;

    IBlockChain blockChain;
    @BindView(R.id.iv_clear)
    ImageView ivClear;
    @BindView(R.id.tvLimit)
    TextView tvLimit;

    private double serviceFee = 0.08;
    private double balance = 0;

    private String toAddress;
    private double transferCoin;
    private String mLimitInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_out);
        blockChain = ServiceManager.getInstance().createService(IBlockChain.class);
        ButterKnife.bind(this);
        setTitle("转出");
        setLeftBlack();

        ToastUtil.showLoading(this);
        APIManager.startRequest(blockChain.getAccountInfo(), new BaseRequestListener<BlockChainInfo>() {
            @Override
            public void onSuccess(BlockChainInfo result) {
                super.onSuccess(result);
                tvAvailableBalance.setText("可转出余额" + BlockChainUtil.formatCCMCoin(result.bi) + "CCM");
                balance = result.bi;
            }

            @Override
            public void onComplete() {
                super.onComplete();
                ToastUtil.hideLoading();
            }
        }, this);
        tvFee.setText(BlockChainUtil.formatCCMCoin(serviceFee) + "CCM");
        etReceiverAddress.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                String addr = etReceiverAddress.getText().toString().trim();
                if (!TextUtils.isEmpty(addr) && !BlockChainUtil.isValidAddress(addr)) {
                    tvHint.setText("钱包地址填写错误");
                } else {
                    tvHint.setText("");
                }
            }
        });
        etTransferCoin.setFilters(new InputFilter[]{new CoinInputFilter()});
        etReceiverAddress.setFilters(new InputFilter[]{new AddressInputFilter(), new InputFilter.LengthFilter(34)});
        etReceiverAddress.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                updateCommitBtnState();
                String addr = etReceiverAddress.getText().toString();
                ivClear.setVisibility(TextUtils.isEmpty(addr) ? View.INVISIBLE : View.VISIBLE);
            }
        });
        etTransferCoin.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                updateCommitBtnState();
            }
        });
        showHintDialog();
        getTransInfo();
    }

    private void getTransInfo(){
        APIManager.startRequest(blockChain.getTransInfo(), new BaseRequestListener<Excess>(this) {
            @Override
            public void onSuccess(Excess result) {
                super.onSuccess(result);
                mLimitInfo = result.limitInfo;
                tvLimit.setText(result.limitCcm);
                tvLimit.setVisibility(View.VISIBLE);
            }
        },this);
    }

    private void showHintDialog() {
        if (BlockChainUtil.getWhetherTransferOutHintIsRead()) {
            return;
        }
        BlockChainUtil.setTransferOutHintToRead();
        WJDialog blockChainActiveDialog = new WJDialog(this);
        blockChainActiveDialog.show();
        blockChainActiveDialog.setTitle("温馨提示");
        blockChainActiveDialog.setContentText("转账前，请注意对方地址已升级为最新的通用钱包地址，以免发生转账失败。通用格式地址采用更安全的地址格式.");
        blockChainActiveDialog.hideCancelBtn();
        blockChainActiveDialog.setConfirmText("知道了");
        blockChainActiveDialog.setConfirmTextColor(R.color.color_bule);
        blockChainActiveDialog.setOnConfirmListener(v -> {
            blockChainActiveDialog.dismiss();
        });
    }

    @OnClick({R.id.tv_transfer_all, R.id.tv_commit, R.id.tvLimit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_transfer_all:
                if (balance <= serviceFee) {
                    ToastUtil.error("余额不足!");
                    return;
                }
                etTransferCoin.setText(BlockChainUtil.formatCCMCoin(balance - serviceFee));
                break;
            case R.id.tv_commit:
                if (tvCommit.isEnabled()) {
                    checkCcmExcess();
                }
                break;
            case R.id.tvLimit:
                showLimitDialog();
                break;
        }
    }

    private void checkCcmExcess() {
        APIManager.startRequest(blockChain.checkCcmExcess(String.valueOf(transferCoin)), new BaseRequestListener<Excess>(this) {
            @Override
            public void onSuccess(Excess result) {
                super.onSuccess(result);
                if (result.isAllow == 1) {
                    ConfirmActivity.start(TransferOutActivity.this, toAddress, transferCoin);
                } else {
                    ToastUtil.error(result.msg);
                }
            }
        }, this);
    }

    private void showLimitDialog() {
        WJDialog wjDialog = new WJDialog(this);
        wjDialog.show();
        wjDialog.setTitle("温馨提示");
        wjDialog.setContentText(mLimitInfo.replace("\\n","\n"));
        wjDialog.hideCancelBtn();
        wjDialog.setConfirmText("我知道了");
        wjDialog.setConfirmTextColor(R.color.color_bule);
        wjDialog.setOnConfirmListener(v -> wjDialog.dismiss());
    }

    private void updateCommitBtnState() {
        String address = etReceiverAddress.getText().toString().trim();
        String coin = etTransferCoin.getText().toString().trim();
        this.toAddress = address;
        boolean isValidAddr = BlockChainUtil.isValidAddress(address);
        if (isValidAddr) {
            tvHint.setText("");
        }
        if (!TextUtils.isEmpty(coin)) {
            double dc = 0;
            try {
                dc = Double.valueOf(coin);
            } catch (Exception e) {
                dc = 0;
            }
            long cost = (long) ((dc + serviceFee) * 1000000);
            long balance = (long) (this.balance * 1000000);

            if (cost > balance) {
                tvHintBalance.setText("余额不足");
            } else {
                tvHintBalance.setText("");
            }

            if (dc != 0 && cost <= balance && isValidAddr) {
                tvCommit.setEnabled(true);
                transferCoin = dc;
                return;
            }
        }
        tvCommit.setEnabled(false);
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, TransferOutActivity.class);
        context.startActivity(intent);
    }

    @OnClick(R.id.iv_clear)
    public void onViewClicked() {
        etReceiverAddress.setText("");
    }

    public static class CoinInputFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            Log.d("Seven", source + "," + start + "," + end + "," + dest + "," + dstart + "," + dend);
            String destStr = dest.toString();
            int len = dest.length();
            int pointIndex = destStr.indexOf(".");
            if (pointIndex < 0 || pointIndex >= dstart) {
                return null;
            } else {
                if (len - 1 == pointIndex + 6) {
                    return "";
                }
            }
            return null;
        }
    }

    public static class AddressInputFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            return source.toString().replaceAll("[^1-9a-zA-HJ-NP-Z]", "");
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onTransferSuccess(TransferOutSuccess obj) {
        finish();
    }
}
