package com.weiju.ccmall.module.blockchain.transferccm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.beans.TransferableCCM;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBlockChain;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TransferCCMActivity extends BaseActivity {
    @BindView(R.id.tv_ccm_transferable)
    TextView tvCcmTransferable;
    @BindView(R.id.tv_commit)
    TextView tvCommit;

    IBlockChain blockChain;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_ccm);
        ButterKnife.bind(this);
        setTitle("CCM迁移");
        setLeftBlack();
        blockChain = ServiceManager.getInstance().createService(IBlockChain.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadTransferableCCM();
    }

    private void loadTransferableCCM() {
        ToastUtil.showLoading(this);
        APIManager.startRequest(blockChain.getTransferableCcmCount(), new BaseRequestListener<TransferableCCM>() {
            @Override
            public void onSuccess(TransferableCCM result) {
                super.onSuccess(result);
                if (!isDestroyed()) {
                    tvCcmTransferable.setText(result.coin);
                    try {
                        double coin = Double.valueOf(result.coin);
                        tvCommit.setEnabled(coin > 0);
                    } catch (Exception e) {
                        tvCommit.setEnabled(false);
                    }
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
                ToastUtil.hideLoading();
            }
        }, this);
    }

    @OnClick(R.id.tv_commit)
    public void onViewClicked() {
        if (tvCommit.isEnabled()) {
            transferCCM();
        }
    }

    private void transferCCM() {
        ToastUtil.showLoading(this);
        APIManager.startRequest(blockChain.transferableCcm(), new BaseRequestListener<Object>() {
            @Override
            public void onComplete() {
                super.onComplete();
                ToastUtil.hideLoading();
            }

            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                if (!isDestroyed()) {
//                    ToastUtil.success("迁移成功！");
                    TransferCCMSuccessDialogFragment.newInstance().show(getSupportFragmentManager(),
                            "TransferCCMSuccessDialogFragment");
                    loadTransferableCCM();
                }
            }
        }, this);
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, TransferCCMActivity.class);
        context.startActivity(intent);
    }
}
