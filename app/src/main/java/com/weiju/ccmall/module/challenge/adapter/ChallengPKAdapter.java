package com.weiju.ccmall.module.challenge.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Challenge;
import com.weiju.ccmall.shared.util.FrescoUtil;

/**
 * @author chenyanming
 * @time 2019/6/27 on 14:07
 * @desc ${TODD}
 */
public class ChallengPKAdapter extends BaseQuickAdapter<Challenge, BaseViewHolder> {
    public ChallengPKAdapter() {
        super(R.layout.item_challeng_pk);
    }

    @Override
    protected void convert(BaseViewHolder helper, Challenge item) {
        FrescoUtil.setImageSmall(helper.getView(R.id.ivAvatar), item.headImage);
        helper.setText(R.id.tvName, item.nickName);
        helper.setText(R.id.tvScore, String.format("%s积分", item.pkScore));
    }
}
