package com.weiju.ccmall.module.live.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.imsdk.TIMConversation;
import com.tencent.imsdk.TIMManager;
import com.tencent.imsdk.TIMMessage;
import com.tencent.imsdk.TIMMessageListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.publish.PublishDailog;
import com.weiju.ccmall.module.live.activity.CreatLiveRoomActivity;
import com.weiju.ccmall.module.live.activity.LiveHistoryListActivity;
import com.weiju.ccmall.module.live.activity.MyHistoryLivesActivity;
import com.weiju.ccmall.module.live.activity.MyLivesActivity;
import com.weiju.ccmall.newRetail.activity.ConversationListActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/4/20.
 */
public class LiveHomeFragment extends BaseFragment {
    @BindView(R.id.barPading)
    View mBarPading;

    @BindView(R.id.ic_publish_new)
    ImageView mIcPublishNew;
    @BindView(R.id.avatarIv)
    ImageView avatarIv;
    @BindView(R.id.avatarIvContainer)
    View avatarIvContainer;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.tvLiveTabRecommend)
    TextView tvLiveTabRecommend;
    @BindView(R.id.tvLiveTabFocus)
    TextView tvLiveTabFocus;
    @BindView(R.id.tv_message_number)
    TextView tvMessageNumber;

    private LiveUser liveUser;

    private Set<LiveHomeListFragment> listFragmentSet = new HashSet<>();
    private PublishDailog mDailog;

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_live_home, container, false);
        ButterKnife.bind(this, view);
        TIMManager.getInstance().addMessageListener(messageListener);
        initView();
        initData();
        initViewPage();
        return view;
    }

    private void initView() {
        //导航栏高度
        int height = QMUIStatusBarHelper.getStatusbarHeight(getContext());
        mBarPading.getLayoutParams().height = height;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            initData();
        }
    }

    private void initData() {
        User user = SessionUtil.getInstance().getLoginUser();
        if (null == user) {
            avatarIv.setVisibility(View.INVISIBLE);
            return;
        }
        if (!TextUtils.isEmpty(user.avatar)) {
            Glide.with(this).load(Uri.parse(user.avatar))
                    .apply(new RequestOptions().placeholder(R.mipmap.default_avatar))
                    .into(avatarIv);
        }
        avatarIv.setVisibility(View.VISIBLE);
        APIManager.startRequest(mService.getLiveUserInfo(user.id), new BaseRequestListener<LiveUser>() {
            @Override
            public void onSuccess(LiveUser result) {
                liveUser = result;
                SessionUtil.getInstance().setLiveUser(result);
                mIcPublishNew.setVisibility(result.liveLimit == 1 ? View.VISIBLE : View.GONE);
                avatarIvContainer.setVisibility(result.liveLimit == 1? View.VISIBLE: View.GONE);
            }
        }, getContext());
    }

    private void initViewPage() {
        viewPager.setAdapter(new FragmentStatePagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                return LiveHomeListFragment.newInstance(i);
            }

            @Override
            public int getCount() {
                return 2;
            }
        });
        viewPager.setCurrentItem(0);
        unselectAllTab();
        tvLiveTabRecommend.setSelected(true);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                unselectAllTab();
                switch (i) {
                    case 0:
                        tvLiveTabRecommend.setSelected(true);
                        break;
                    case 1:
                        tvLiveTabFocus.setSelected(true);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        Log.d("Seven", "attach fragment " + fragment);
        if (fragment instanceof LiveHomeListFragment) {
            listFragmentSet.add((LiveHomeListFragment) fragment);
        }
    }

    @OnClick(R.id.ic_publish_new)
    protected void publish() {
        if (UiUtils.checkUserLogin(getContext())) {
            if (mDailog == null) {
                mDailog = new PublishDailog(getContext());
                mDailog.setOnLiveCallBack(onClickCallBack);
            }
            mDailog.show();
            mDailog.setLiveView();
            mDailog.setNoticeCount(liveUser.liveNoticeCount);
        }
    }

    @OnClick(R.id.tv_message)
    protected void message() {
        if (UiUtils.checkUserLogin(getContext())) {
            startActivity(new Intent(getContext(), ConversationListActivity.class));
        }
    }

    @OnClick(R.id.avatarIvContainer)
    protected void myLive() {
        Log.d("Seven", "myLive " + liveUser);
        if (liveUser == null) {
            return;
        }
        MyLivesActivity.start(getContext(), liveUser, false);
    }

    private void unselectAllTab() {
        tvLiveTabFocus.setSelected(false);
        tvLiveTabRecommend.setSelected(false);
    }

    @OnClick({R.id.tvLiveTabFocus, R.id.tvLiveTabRecommend})
    protected void onTab(View view) {
        unselectAllTab();
        view.setSelected(true);
        int id = view.getId();
        if (id == R.id.tvLiveTabFocus) {
            viewPager.setCurrentItem(1, true);
        } else if (id == R.id.tvLiveTabRecommend){
            viewPager.setCurrentItem(0, true);
        }
    }


    private PublishDailog.OnLiveCallBack onClickCallBack = new PublishDailog.OnLiveCallBack() {
        @Override
        public void onTakeLive() {
            CreatLiveRoomActivity.start(getContext(), liveUser.shopkeeper);
        }

        @Override
        public void onLiveHistory() {
            startActivity(new Intent(getContext(), LiveHistoryListActivity.class));
        }

        @Override
        public void onLiveNotice() {
            MyHistoryLivesActivity.start(getContext(), true);
        }
    };


    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this))//加上判断
            EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            updateTotalUnreadMessage();
            initData();
        }
    }

    private void updateTotalUnreadMessage() {
        long totalUnreadMessage = 0;
        List<TIMConversation> conversations = TIMManager.getInstance().getConversationList();
        if (conversations != null) {
            for (TIMConversation conversation : conversations) {
                totalUnreadMessage += conversation.getUnreadMessageNum();
            }
        }

        if (totalUnreadMessage > 0) {
            tvMessageNumber.setVisibility(View.VISIBLE);
            if (totalUnreadMessage > 99) {
                totalUnreadMessage = 99;
            }
            tvMessageNumber.setText(String.valueOf(totalUnreadMessage));
        } else {
            tvMessageNumber.setVisibility(View.INVISIBLE);
        }
        EventBus.getDefault().post(new EventMessage(Event.liveHomeMessageUpdate, totalUnreadMessage));
    }

    TIMMessageListener messageListener = new TIMMessageListener() {//消息监听器
        @Override
        public boolean onNewMessages(List<TIMMessage> msgs) {//收到新消息
            updateTotalUnreadMessage();
            //消息的内容解析请参考消息收发文档中的消息解析说明
            return false; //返回true将终止回调链，不再调用下一个新消息监听器
        }
    };

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case liveRoomChange:
                for (LiveHomeListFragment f :
                        listFragmentSet) {
                    f.getData(true);
                }
                break;
            case logout:
//                finish();
                break;
            case ApplyPaySuccess:
                initData();
                break;
            default:
        }
    }
}
