package com.weiju.ccmall.module.challenge.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.challenge.adapter.ChallengAdapter;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Challenge;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IChallengeService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author chenyanming
 * @time 2019/1/28 on 15:48
 * @desc 挑战
 */
public class ChallengeListActivity extends BaseActivity implements PageManager.RequestListener {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;

    private ChallengAdapter mAdapter;
    private List<Challenge> mDatas = new ArrayList<>();
    private PageManager mPageManager;
    private IChallengeService mService = ServiceManager.getInstance().createService(IChallengeService.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_list2);
        ButterKnife.bind(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.setStatusBarColor(getResources().getColor(R.color.redLottery));
            QMUIStatusBarHelper.setStatusBarDarkMode(this);
        }

        mAdapter = new ChallengAdapter(mDatas);
        mRecyclerView.setAdapter(mAdapter);
        try {

            mPageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(mRecyclerView)
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .setRequestListener(this)
                    .build(this);
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }

        EventBus.getDefault().register(this);
        initView();
        initData();
    }

    private void initData() {
        mPageManager.onRefresh();
    }

    private void initView() {
        showHeader();
        setTitle("挑战自我");
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_white);
        getHeaderLayout().setRightText("挑战记录");
        getHeaderLayout().setOnRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ChallengeListActivity.this, ChallengeHisActiivty.class);
                startActivity(intent);
            }
        });
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getHeaderLayout().makeHeaderColor(R.color.redLottery);

        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.background));

        mRecyclerView.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                Challenge item = mAdapter.getItem(position);
                if (null != item) {
                    Intent intent = new Intent(ChallengeListActivity.this,
                            item.type < 2 ? ChallengeDetailActivity.class : ChallengePKDetailActivity.class);
                    intent.putExtra("activityId", item.activityId);
                    intent.putExtra("minScore", item.minScore);
                    intent.putExtra("maxScore", item.maxScore);
                    startActivity(intent);

                }

            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void nextPage(final int page) {
        APIManager.startRequest(mService.getActivityList(page, 15),
                new BaseRequestListener<PaginationEntity<Challenge, Object>>(this) {
                    @Override
                    public void onSuccess(PaginationEntity<Challenge, Object> result) {
                        mPageManager.setLoading(false);
                        mPageManager.setTotalPage(result.totalPage);
                        if (page == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        mRefreshLayout.setRefreshing(false);
                    }
                },this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(EventMessage message) {
        switch (message.getEvent()) {
            case joinChallenge:
            case countDownComplete:
            case challengeTrans:
                mPageManager.onRefresh();
                break;
            default:
        }
    }


}
