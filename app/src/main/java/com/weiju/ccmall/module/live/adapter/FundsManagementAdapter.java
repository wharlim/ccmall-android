package com.weiju.ccmall.module.live.adapter;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.activity.FundsManagementActivity;
import com.weiju.ccmall.module.live.entity.SettleAccountEntity;
import com.weiju.ccmall.shared.util.MoneyUtil;

import java.util.List;

public class FundsManagementAdapter extends BaseQuickAdapter<SettleAccountEntity, BaseViewHolder> {

    private int mType;

    public FundsManagementAdapter(@Nullable List<SettleAccountEntity> data, int type) {
        super(R.layout.item_funds_management, data);
        mType = type;
    }

    @Override
    protected void convert(BaseViewHolder helper, SettleAccountEntity item) {
        String sellOrder = "销售订单" + item.orderCode;
        String amount = "+" + MoneyUtil.centToYuan(item.profitMoney);
        int colorAmount = helper.itemView.getContext().getResources().getColor(R.color.red);
        if (mType == FundsManagementActivity.TYPE_LOAN) {
            //记录类型（1：销售订单；2：提现记录）
            if (item.type == 2) {
                sellOrder = "提现";
                amount = MoneyUtil.centToYuanStrNoZero(item.profitMoney);
                colorAmount = helper.itemView.getContext().getResources().getColor(R.color.color_44945F);
            } else {
            }
            helper.setVisible(R.id.tvSurplusAmount, true).setText(R.id.tvSurplusAmount, item.dealStatusStr);
        } else {
            helper.setVisible(R.id.tvSurplusAmount, false);
        }
        if (TextUtils.isEmpty(item.month)){
            helper.setVisible(R.id.tvTime, false);
        } else {
            helper.setVisible(R.id.tvTime, true).setText(R.id.tvTime, item.month);
        }
        helper.setText(R.id.tvSellOrder, sellOrder)
                .setText(R.id.tvSellTime, item.payDate)
                .setText(R.id.tvAmount, amount)
                .setTextColor(R.id.tvAmount, colorAmount);
//                .setText(R.id.tvPrice, MoneyUtil.centToYuan¥Str(item.retailPrice));
    }
}
