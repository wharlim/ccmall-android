package com.weiju.ccmall.module.user;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.util.BaseUrl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangeNetWorkTypeActivity extends BaseActivity {

    @BindView(R.id.sp_header)
    Spinner sp_header;

    String customIp = "https://";

    @BindView(R.id.et_ip)
    AutoCompleteTextView et_ip;

    @BindView(R.id.et_port)
    EditText et_port;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_network_type_layout);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.bt_test, R.id.bt_release, R.id.bt_waibao, R.id.bt_custom})
    public void onClicks(View view) {
        switch (view.getId()) {
            case R.id.bt_test:
                BaseUrl.getInstance().setNetWorkType(BaseUrl.CCMALL_TEST);
                Toast.makeText(this, "pre环境（外网）", Toast.LENGTH_SHORT).show();
                break;
            case R.id.bt_release:
                BaseUrl.getInstance().setNetWorkType(BaseUrl.CCMALL_RELEASE_1);
                Toast.makeText(this, "正式环境", Toast.LENGTH_SHORT).show();
                break;
            case R.id.bt_waibao:
                BaseUrl.getInstance().setNetWorkType(BaseUrl.CCMALL_WAIBAO_TEST);
                Toast.makeText(this, "testapi.weijuit.com", Toast.LENGTH_SHORT).show();
                break;
            case R.id.bt_custom:
                if (!TextUtils.isEmpty(et_ip.getText().toString())) {
                    BaseUrl.getInstance().setNetWorkType(BaseUrl.CCMALL_CUSTOMIZE);
                    String port = et_port.getText().toString();
                    customIp = (String) sp_header.getSelectedItem();
                    BaseUrl.getInstance().setBaseUrl(customIp + et_ip.getText().toString() + ":" + port + "/ccmapi/");
                    Toast.makeText(this, "自定义", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MyApplication.getContext(), "IP不能为空", Toast.LENGTH_SHORT).show();
                }

                break;
            default:
                break;
        }
        ChangeNetWorkTypeActivity.this.finish();
    }

    @OnClick({R.id.bt_192_168_17_29, R.id.bt_grayapi, R.id.bt_preapi})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.bt_192_168_17_29:
                BaseUrl.getInstance().setNetWorkType(BaseUrl.CCMALL_beta);
                Toast.makeText(this, "beta环境", Toast.LENGTH_SHORT).show();
                break;
            case R.id.bt_grayapi:
                BaseUrl.getInstance().setNetWorkType(BaseUrl.CCMALL_grayapi);
                Toast.makeText(this, "gray环境", Toast.LENGTH_SHORT).show();
                break;
            case R.id.bt_preapi:
                BaseUrl.getInstance().setNetWorkType(BaseUrl.CCMALL_preapi);
                Toast.makeText(this, "pre环境", Toast.LENGTH_SHORT).show();
                break;
        }
        ChangeNetWorkTypeActivity.this.finish();
    }
}
