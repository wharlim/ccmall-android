package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

public class ReceiptSupportBankEntity {

    /**
     * channelId : GftQPay
     * channelName : 积分通道L
     * bankName : 工商银行
     * cardType : 2
     * single : 0
     * currentDay : 0
     * status : 1
     * remark : null
     * type : null
     * bankCode : null
     */

    @SerializedName("channelId")
    public String channelId;
    @SerializedName("channelName")
    public String channelName;
    @SerializedName("bankName")
    public String bankName;
    @SerializedName("cardType")
    public int cardType;
    @SerializedName("single")
    public int single;
    @SerializedName("currentDay")
    public int currentDay;
    @SerializedName("status")
    public int status;
    @SerializedName("remark")
    public Object remark;
    @SerializedName("type")
    public Object type;
    @SerializedName("bankCode")
    public String bankCode;
}
