package com.weiju.ccmall.module.product;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.ScreenUtils;
import com.blankj.utilcode.utils.SizeUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.TimeUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.common.base.Joiner;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.video.base.GSYVideoPlayer;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.cart.CartActivity;
import com.weiju.ccmall.module.groupBuy.JoinGroupActivity;
import com.weiju.ccmall.module.groupBuy.JoinGroupView;
import com.weiju.ccmall.module.groupBuy.MsgGroupDialog;
import com.weiju.ccmall.module.instant.MsgInstant;
import com.weiju.ccmall.module.page.CustomPageActivity;
import com.weiju.ccmall.module.product.adapter.ProductActivityAdapter;
import com.weiju.ccmall.module.product.adapter.ProductVideoViewPagerAdapter;
import com.weiju.ccmall.module.product.views.ActivityListLayout;
import com.weiju.ccmall.module.shop.ShopActivity;
import com.weiju.ccmall.module.store.StoreCard;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Coupon;
import com.weiju.ccmall.shared.bean.JoinProduct;
import com.weiju.ccmall.shared.bean.Presents;
import com.weiju.ccmall.shared.bean.Product;
import com.weiju.ccmall.shared.bean.ProductActivityModel;
import com.weiju.ccmall.shared.bean.ProductComment;
import com.weiju.ccmall.shared.bean.PropertyValue;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.SkuPvIds;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.ViewHistory;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.CountDown;
import com.weiju.ccmall.shared.component.DragScrollDetailsLayout;
import com.weiju.ccmall.shared.component.FlashSaleLabel;
import com.weiju.ccmall.shared.component.FlowLayout;
import com.weiju.ccmall.shared.component.TagTextView;
import com.weiju.ccmall.shared.component.dialog.CouponBottomDialog;
import com.weiju.ccmall.shared.component.dialog.EditStockDialog;
import com.weiju.ccmall.shared.component.dialog.ProductActivityDialog;
import com.weiju.ccmall.shared.component.dialog.ProductVerifyDialog;
import com.weiju.ccmall.shared.component.dialog.SkuSelectorDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.component.zuji.ZujiDialog;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.decoration.SpacesItemDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.CartManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.ProductService;
import com.weiju.ccmall.shared.service.contract.ICollectService;
import com.weiju.ccmall.shared.service.contract.ICouponService;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.CSUtils;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ShareUtils;
import com.weiju.ccmall.shared.util.TextViewUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;
import com.weiju.ccmall.shared.util.WebViewUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

//import com.weiju.ccmall.shared.component.DragScrollDetailsLayout;
//import cn.bingoogolapple.photopicker.widget.BGANinePhotoLayout;

public class NewProductDetailActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView mWebview;
    @BindView(R.id.ivBack)
    ImageView mIvBack;
    @BindView(R.id.tvTagTitle)
    TagTextView mTvTagTitle;
    @BindView(R.id.tvDesc)
    TextView mTvDesc;
    @BindView(R.id.tvRetailPrice)
    TextView mTvRetailPrice;
    @BindView(R.id.tvMarketPrice)
    TextView mTvMarketPrice;
    @BindView(R.id.layoutPrice)
    LinearLayout mLayoutPrice;
    @BindView(R.id.tvSales)
    TextView mTvSales;
    @BindView(R.id.tvExpress)
    TextView mTvExpress;
    @BindView(R.id.tvRegion)
    TextView mTvRegion;
    @BindView(R.id.productAuthLayout)
    LinearLayout mProductAuthLayout;
    @BindView(R.id.tvSkuInfo)
    TextView mTvSkuInfo;
    @BindView(R.id.storeCard)
    StoreCard mStoreCard;
    @BindView(R.id.serviceBtn)
    TextView mServiceBtn;
    @BindView(R.id.favBtn)
    TextView mFavBtn;
    @BindView(R.id.cartBtn)
    TextView mCartBtn;
    @BindView(R.id.addToCartBtn)
    TextView mAddToCartBtn;
    @BindView(R.id.buyNowBtn)
    TextView mBuyNowBtn;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.flashSaleLabel)
    FlashSaleLabel mFlashSaleLabel;
    @BindView(R.id.bottomLine)
    View mBottomLine;
    @BindView(R.id.cartBtnBadgeTv)
    TextView mTvcartBtnBadge;
    @BindView(R.id.layoutDragScroll)
    DragScrollDetailsLayout mLayoutDragScroll;
    @BindView(R.id.layoutTitle)
    RelativeLayout mLayoutTitle;
    @BindView(R.id.shareBtn)
    ImageView mShareBtn;
    @BindView(R.id.ivCountry)
    SimpleDraweeView mIvCountry;
    @BindView(R.id.tvCountry)
    TextView mTvCountry;
    @BindView(R.id.headerTitleTv)
    TextView mHeaderTitleTv;
    @BindView(R.id.ivBack2)
    ImageView mIvBack2;
    @BindView(R.id.ivShare)
    ImageView mIvShare;
    @BindView(R.id.cartBtnLayout)
    FrameLayout mCartBtnLayout;
    @BindView(R.id.couponLayout)
    LinearLayout mLayoutCoupon;

    @BindView(R.id.tvProductAuth1)
    TextView mTvProductAuth1;
    @BindView(R.id.tvProductAuth2)
    TextView mTvProductAuth2;
    @BindView(R.id.tvProductAuth3)
    TextView mTvProductAuth3;
    @BindView(R.id.layoutVipInfo)
    RelativeLayout mLayoutVipInfo;
    @BindView(R.id.tvBottomGreyText)
    TextView mTvBottomGreyText;
    @BindView(R.id.qrCodeBtn)
    ImageView mQrCodeBtn;
    @BindView(R.id.ivQrCode)
    ImageView mIvQrCode;
    @BindView(R.id.tvGroupReturnPrice)
    TextView mTvGroupReturnPrice;
    @BindView(R.id.layoutGroupReturn)
    LinearLayout mLayoutGroupReturn;
    @BindView(R.id.tvNoGroupPrice)
    TextView mTvNoGroupPrice;
    @BindView(R.id.tvGroupNo)
    TextView mTvGroupNo;
    @BindView(R.id.layoutNoGroup)
    LinearLayout mLayoutNoGroup;
    @BindView(R.id.tvStartGroupPrice)
    TextView mTvStartGroupPrice;
    @BindView(R.id.layoutStartGroup)
    LinearLayout mLayoutStartGroup;
    @BindView(R.id.layoutGroup)
    LinearLayout mLayoutGroup;
    @BindView(R.id.tvVipPrice)
    TextView mTvVipPrice;
    @BindView(R.id.tvVipJoin)
    TextView mTvVipJoin;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.ivBannerVideo)
    ImageView mIvBannerVideo;
    @BindView(R.id.ivBannerImage)
    ImageView mIvBannerImage;
    @BindView(R.id.tvBannerImageText)
    TextView mTvBannerImageText;
    @BindView(R.id.tvCommentCount)
    TextView mTvCommentCount;
    @BindView(R.id.ivCommentAvatar)
    SimpleDraweeView mIvCommentAvatar;
    @BindView(R.id.tvCommentName)
    TextView mTvCommentName;
    @BindView(R.id.tvCommentDate)
    TextView mTvCommentDate;
    @BindView(R.id.tvCommentContent)
    TextView mTvCommentContent;
    /*@BindView(R.id.layoutCommentNineImages)
    BGANinePhotoLayout mLayoutCommentNineImages;*/
    @BindView(R.id.tvCommentAll)
    TextView mTvCommentAll;
    @BindView(R.id.layoutComent)
    LinearLayout mLayoutComent;
    @BindView(R.id.rvActivity)
    RecyclerView mRvActivity;
    @BindView(R.id.layoutActivity)
    LinearLayout mLayoutActivity;
    @BindView(R.id.layoutCouponTag)
    FlowLayout mLayoutCouponTag;
    @BindView(R.id.tv_cc)
    TextView mTvCc;
    @BindView(R.id.topProList)
    RelativeLayout mTopProList;
    @BindView(R.id.tvSaleTime)
    TextView mTvSaleTime;
    @BindView(R.id.lyVideoPic)
    LinearLayout mLyVideoPic;
    @BindView(R.id.tvGolden)
    TextView mTvGolden;
    @BindView(R.id.layoutSuperGroup)
    LinearLayout mLayoutSuperGroup;
    @BindView(R.id.tvSuperGroupPrice)
    TextView mTvSuperGroupPrice;
    @BindView(R.id.tvSuperGroupText)
    TextView mTvSuperGroupText;
    @BindView(R.id.shopIcon)
    SimpleDraweeView shopIcon;
    @BindView(R.id.shopName)
    TextView shopName;
    @BindView(R.id.shopContainer)
    LinearLayout shopContainer;
    @BindView(R.id.activities_ccm)
    ActivityListLayout activityListLayout;
    @BindView(R.id.layoutProductBottom)
    LinearLayout mLayoutProductBottom;
    @BindView(R.id.layoutGift)
    LinearLayout mLayoutGift;
    @BindView(R.id.tvAddGift)
    TextView mTvAddGift;

    private String mSkuId;
    private ICollectService mCollectService;
    private IProductService mProductService;
    private SkuInfo mSkuInfo;
    private Product mProduct;
    private ZujiDialog mDialog;
    private List<SkuInfo> mViewHistoryDatas;
    private boolean mInstantBuy = false;
    private ICouponService mCouponService;
    private CouponBottomDialog mCouponBottomDialog;
    private SkuSelectorDialog mSkuSelectorDialog;
    private List<Coupon> mCouponList;

    private EditStockDialog mEditStockDialog;

    String orderType = "";
    private String liveId;

    public static final int TYPE_MY_GIFT = 1;
    public static final int TYPE_ADD_GIFT = 2;
    private int mType;
    private String mMemberSkuId;
    private List<SkuInfo> mMyGiftSkuList;
    private ArrayList<SkuInfo> mSkuInfoList;
    //我的礼包上下架状态
    private int mStatus;

//    private boolean isNewRetail = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_product_detail);
        ButterKnife.bind(this);

        EventBus.getDefault().register(this);

        getIntentData();
        initView();
        initData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ToastUtil.hideLoading();
        GSYVideoManager.onResume();
        if (mSkuSelectorDialog != null && mSkuSelectorDialog.isShowing()) {
            mSkuSelectorDialog.dismiss();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        GSYVideoManager.onPause();
    }


    @Override
    protected void onDestroy() {
        if (mSkuSelectorDialog != null){
            mSkuSelectorDialog.dismiss();
        }
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        GSYVideoPlayer.releaseAllVideos();
    }

    private void initData() {
        mProductService = ServiceManager.getInstance().createService(IProductService.class);
        mCollectService = ServiceManager.getInstance().createService(ICollectService.class);
        mCouponService = ServiceManager.getInstance().createService(ICouponService.class);
        orderType = getSharedPreferences(Const.ORDER_TYPE, 0).getString(Const.ORDER_TYPE, "");
        String passOrderType = getIntent().getStringExtra(Const.ORDER_TYPE);
        if (!TextUtils.isEmpty(passOrderType)) {
            orderType = passOrderType;
        }
        getProductData(mType == TYPE_MY_GIFT ? mMemberSkuId : mSkuId);
        addViewRecord();
        User user = SessionUtil.getInstance().getLoginUser();
        if (user != null) {
            getViewHistoryData(false);
        }
        CartManager.getAmount();


    }


    /**
     * 添加足迹的浏览记录
     */
    private void addViewRecord() {
        User user = SessionUtil.getInstance().getLoginUser();
        if (user != null) {
            ProductService.addViewRecord(user.id, mSkuId);
        }
    }

    private void getProductData(String id) {
        Observable<RequestResult<SkuInfo>> observable = mProductService.getSkuById(id);
        if (mType == TYPE_MY_GIFT) {
            observable = mProductService.getSkuByMemberSkuId(id);
        }
        APIManager.startRequest(observable, new BaseRequestListener<SkuInfo>(this) {
            @Override
            public void onSuccess(SkuInfo skuInfo) {
                mSkuInfo = skuInfo;
                if (mSkuInfo.productType == 18) {
                    getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, Const.ORDER_TYPE_FREE_ORDER).commit();
                } else if (mSkuInfo.productType == 0) {
                    getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
                }
                getProductInfoById(mSkuInfo.productId);
//                getProductInfoById("3638083b157c494d92d81ed848959c70");
                getProductCoupon(mSkuInfo.productId);
                getProductComment(mSkuInfo.productId);
                setSkuViews();
            }
        }, this);
    }

    /**
     * 获取商品评论
     *
     * @param productId
     */
    private void getProductComment(String productId) {
        APIManager.startRequest(
                mProductService.getProductComment(productId, 1, 1),
                new BaseRequestListener<PaginationEntity<ProductComment, Object>>(this) {
                    @Override
                    public void onSuccess(PaginationEntity<ProductComment, Object> result) {
                        if (result.total <= 0) {
                            return;
                        }
                        mLayoutComent.setVisibility(View.VISIBLE);
                        mTvCommentCount.setText("商品评价（" + result.total + "）");
                        if (result.list != null && result.list.size() > 0) {
                            ProductComment productComment = result.list.get(0);
                            mTvCommentName.setText(productComment.nickName);
                            mTvCommentContent.setText(productComment.content);
                            mTvCommentDate.setText(productComment.commentDate + "  " + productComment.properties);
//                            mLayoutCommentNineImages.setData((ArrayList<String>) productComment.images);
                            FrescoUtil.setImageSmall(mIvCommentAvatar, result.list.get(0).headImage);
                        }

                    }
                }, this

        );
    }

    /**
     * 获取商品优惠券
     *
     * @param productId 商品 id
     */
    private void getProductCoupon(String productId) {
        APIManager.startRequest(mCouponService.getProductCouponList(productId), new BaseRequestListener<List<Coupon>>(this) {

            @Override
            public void onSuccess(List<Coupon> result) {
                if (result.size() > 0) {
                    mCouponList = result;
                    mLayoutCoupon.setVisibility(View.VISIBLE);
                    mLayoutCouponTag.removeAllViews();
                    int totalEms = (ScreenUtils.getScreenWidth() - SizeUtils.dp2px(80)) / SizeUtils.dp2px(14);
                    totalEms -= 2; // 预留2个字符的位置给n+
                    int shown = 0;
                    for (Coupon coupon : mCouponList) {
                        totalEms -= coupon.title.length();
                        String title = coupon.title;
                        boolean shouldBreak = false;
                        if (totalEms <= 0) {
                            shouldBreak = true;
                            title = (mCouponList.size() - shown) + "+";

                            if (totalEms > -2 && shown == mCouponList.size() - 1) {
                                title = coupon.title;
                            }
                        }

                        TextView view = new TextView(NewProductDetailActivity.this);
                        view.setText(title);
                        view.setTextSize(13);
                        view.setSingleLine(true);
                        view.setTextColor(Color.WHITE);
                        view.setBackgroundResource(R.drawable.ic_product_coupon);
                        FlowLayout.LayoutParams layoutParams = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
                        layoutParams.setMargins(0, 0, SizeUtils.dp2px(5), 0);
                        view.setLayoutParams(layoutParams);

                        mLayoutCouponTag.addView(view);
                        if (shouldBreak) {
                            break;
                        }
                        shown++;
                    }
                } else {
                    mLayoutCoupon.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError(Throwable e) {
                mLayoutCoupon.setVisibility(View.GONE);
            }
        }, this);
    }

    private void getProductInfoById(String productId) {
        APIManager.startRequest(mProductService.getDetailById(productId), new BaseRequestListener<Product>(this) {
            @Override
            public void onSuccess(Product product) {
                mProduct = product;
                List<String> skuIdList = getSkuIdList();
                if (mType == TYPE_MY_GIFT) {
                    getProductsSkusByMemberId(skuIdList);
                } else if (mType == TYPE_ADD_GIFT) {
                    getProductsSkus(skuIdList);
                }
                setVideoViewPager();
                setProductView();
                instantBuy();
                if (mProduct.extType == 2) {
                    setGroupView();
                }

            }
        }, this);
    }

    /**
     * 获取SkuID
     *
     * @return
     */
    private List<String> getSkuIdList() {
        List<String> skuIds = new ArrayList<>();
        for (SkuPvIds skus : mProduct.skus) {
            skuIds.add(skus.skuId);
        }
        return skuIds;
    }

    /**
     * 批量获取渠道产品规格
     */
    private void getProductsSkus(List<String> skuIds) {
        APIManager.startRequest(mProductService.getListBySkuIds(Joiner.on(",").join(skuIds)),
                new BaseRequestListener<ArrayList<SkuInfo>>(this) {
                    @Override
                    public void onSuccess(ArrayList<SkuInfo> result) {
                        super.onSuccess(result);
                        mSkuInfoList = result;
                        Log.e("v", "d");
                    }
                }, this);
    }


    /**
     * 批量获取渠道产品规格
     */
    private void getProductsSkusByMemberId(List<String> skuIds) {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (null == loginUser) {
            return;
        }
        APIManager.startRequest(mProductService.getProductsSkus(loginUser.id, Joiner.on(",").join(skuIds)),
                new BaseRequestListener<List<SkuInfo>>(this) {
                    @Override
                    public void onSuccess(List<SkuInfo> result) {
                        super.onSuccess(result);
                        mMyGiftSkuList = result;
                    }
                }, this);
    }


    /**
     * 设置团购相关的东西
     */
    private void setGroupView() {
        mTvSkuInfo.setVisibility(View.GONE);
        mLayoutVipInfo.setVisibility(View.GONE);

        for (int i = 0; mProduct.groupExt.activityInfoList != null && i < mProduct.groupExt.activityInfoList.size() && i < 3; i++) {
            Product.GroupExtEntity.ActivityInfoListEntity entity = mProduct.groupExt.activityInfoList.get(i);
            JoinGroupView joinGroupView = new JoinGroupView(this);
            joinGroupView.setData(entity);
            joinGroupView.setProduct(mProduct);
            joinGroupView.setSkuinfo(mSkuInfo);
            mLayoutGroup.addView(joinGroupView);
        }
        int groupLeaderReturn = mProduct.getGroupEntity(mSkuInfo.skuId).groupLeaderReturn;
        if (groupLeaderReturn > 0) {
//            mLayoutGroupReturn.setVisibility(View.VISIBLE);
//            mTvGroupReturnPrice.setText(ConvertUtil.centToCurrency(this, groupLeaderReturn));
        }
    }

    /**
     * 秒杀立即买
     */
    private void instantBuy() {
        if (mFlashSaleLabel.isSale() && mInstantBuy) {
            CartManager.buyNow(this, this, mSkuInfo, 1, null, orderType);
        }
    }

    private void setProductView() {
        if (mProduct.auths == null || mProduct.auths.size() < 1) {
            mProductAuthLayout.setVisibility(View.GONE);
        } else {
            TextView tvAuth[] = {mTvProductAuth1, mTvProductAuth2, mTvProductAuth3};
            for (int i = 0; i < mProduct.auths.size(); i++) {
                if (i > 2) {
                    break;
                }
                tvAuth[i].setVisibility(View.VISIBLE);
                tvAuth[i].setText(mProduct.auths.get(i).title);
            }
        }

        mTvSales.setText(String.format("销量：%d件", mProduct.saleCount));
        mTvTagTitle.setTags(mProduct.tags);
        if (mProduct.store != null && !StringUtils.isEmpty(mProduct.store.storeId)) {
            mStoreCard.setVisibility(View.VISIBLE);
            mStoreCard.setStore(mProduct.store);
        }
        if (!StringUtils.isEmpty(mProduct.store.expressName)) {
            mTvExpress.setText(String.format("快递：%s", mProduct.store.expressName));
        }
        mTvRegion.setText(mProduct.store.shipAddress);
        WebViewUtil.loadDataToWebView(mWebview, mProduct.content);
        setInstantView();
        setBottomViews();
        if (mProduct.country != null) {
            FrescoUtil.setImage(mIvCountry, mProduct.country.flag);
            mTvCountry.setText(mProduct.country.countryName);
        }
    }

    private void setInstantView() {
        if (!mProduct.isInstant()) {
            mFlashSaleLabel.setVisibility(View.GONE);
        } else {
            mFlashSaleLabel.setVisibility(View.VISIBLE);
            mLayoutPrice.setVisibility(View.GONE);
            final Date startDate = TimeUtils.string2Date(mProduct.sellBegin);
            final Date endDate = TimeUtils.string2Date(mProduct.sellEnd);
            mFlashSaleLabel.setOnFinishListener(new CountDown.OnFinishListener() {
                @Override
                public void onFinish() {
                    setBottomViews();
                }
            });
            mFlashSaleLabel.setData(
                    MoneyUtil.centToYuan¥StrNoZero(mSkuInfo.retailPrice),
                    startDate,
                    endDate);
        }
    }

    /**
     * 设置下面的那些 view
     */
    private void setBottomViews() {
        if (mSkuInfo == null || mProduct == null) {
            return;
        }
        mAddToCartBtn.setVisibility(View.GONE);
        mBuyNowBtn.setVisibility(View.GONE);
        mTvBottomGreyText.setVisibility(View.GONE);
        mLayoutNoGroup.setVisibility(View.GONE);
        mLayoutStartGroup.setVisibility(View.GONE);

        String greyText = getBottomGreyText();
        if (StringUtils.isEmpty(greyText)) {
            if (mProduct.extType == 2) {
                mLayoutNoGroup.setVisibility(View.VISIBLE);
                mLayoutStartGroup.setVisibility(View.VISIBLE);
                mTvNoGroupPrice.setText(MoneyUtil.centToYuan¥StrNoZero(mSkuInfo.retailPrice));
                mTvRetailPrice.setText(MoneyUtil.centToYuan¥StrNoZero(mProduct.getGroupEntity(mSkuInfo.skuId).groupPrice));
                mTvStartGroupPrice.setText(MoneyUtil.centToYuan¥StrNoZero(mProduct.getGroupEntity(mSkuInfo.skuId).groupPrice));
            } else if (mProduct.type == 2 || (mSkuInfo.productType == 9 && mSkuInfo.newerConfig != null && !mSkuInfo.newerConfig.locked)) {
                mBuyNowBtn.setVisibility(View.VISIBLE);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mBuyNowBtn.getLayoutParams();
                layoutParams.weight = 4;
                layoutParams.width = 0;
                mBuyNowBtn.setLayoutParams(layoutParams);
            } else if (mProduct.extType == 5) {
                mLayoutSuperGroup.setVisibility(View.VISIBLE);
                mTvSuperGroupPrice.setText(MoneyUtil.centToYuan¥StrNoZero(mSkuInfo.retailPrice));
                mTvSuperGroupText.setText(null != mProduct.groupExt.initiatorBean &&
                        !TextUtils.isEmpty(mProduct.groupExt.initiatorBean.groupCode) ? "继续邀请" : "立即开团");
            } else if (mSkuInfo.productType == 18 || mSkuInfo.productType == 20 || mSkuInfo.productType == 25 || mSkuInfo.isCatPi()) { // hi购产品(18)和虚拟物品(20)和CCM抵扣商品(25)和密码pi商品都不能加入购物车
                mBuyNowBtn.setVisibility(View.VISIBLE);
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mBuyNowBtn.getLayoutParams();
                layoutParams.weight = 4;
                layoutParams.width = 0;
                mBuyNowBtn.setLayoutParams(layoutParams);
                mBuyNowBtn.setText("立即购买");
                mCartBtn.setVisibility(View.GONE);
            } else {
                mAddToCartBtn.setVisibility(View.VISIBLE);
                mBuyNowBtn.setVisibility(View.VISIBLE);
            }
        } else {
            mTvBottomGreyText.setText(greyText);
            mTvBottomGreyText.setVisibility(View.VISIBLE);
        }
        if (mSkuInfo.productType == 26) {//26服务商，隐藏加入购物车
            mAddToCartBtn.setVisibility(View.GONE);
            mCartBtn.setVisibility(View.GONE);
        }
    }

    /**
     * @return 不可购买时的提示文字
     */
    public String getBottomGreyText() {
        if (mSkuInfo == null || mProduct == null) {
            return null;
        }
        String greyText = null;
        if (mSkuInfo.status == 0) { //下架产品
            greyText = "已下架";
        } else if (mSkuInfo.stock <= 0) {
            greyText = "已告罄";
        } else if (mProduct.isInstant()) {
            if (mSkuInfo.sales >= mSkuInfo.quantity) {
                greyText = "已告罄";
            } else if (!mFlashSaleLabel.isSale()) {
                greyText = mFlashSaleLabel.getStatusStr();
            }
        } else if (mSkuInfo.productType == 9 && mSkuInfo.newerConfig != null && (mSkuInfo.newerConfig.locked || !mSkuInfo.newerConfig.canBuy)) {//新人专区
            greyText = !mSkuInfo.newerConfig.canBuy ? "不可购买" : "立即购买";
        } else if (mProduct.extType == 5 && null != mProduct.groupExt && null != mProduct.groupExt.activityBean) {
            if (mProduct.groupExt.activityBean.status == 0) {
                greyText = "已下架";
            } else if (!TextUtils.isEmpty(mProduct.groupExt.activityBean.endDate) ||
                    !TextUtils.isEmpty(mProduct.groupExt.activityBean.startDate)) {
                Date date = new Date();
                Date startDate = TimeUtils.string2Date(mProduct.groupExt.activityBean.startDate);
                Date endDate = TimeUtils.string2Date(mProduct.groupExt.activityBean.endDate);
                if (date.getTime() > endDate.getTime()) {
                    greyText = "已结束";
                } else if (date.getTime() < startDate.getTime()) {
                    greyText = "即将开抢";
                }

            }
        }
        return greyText;
    }

    private void setSkuViews() {
        if (mSkuInfo.isLiveStoreProduct()) {
            mServiceBtn.setText("卖家");
            Drawable drawable = getResources().getDrawable(R.mipmap.icon_message);
            drawable.setBounds(0, 0, SizeUtils.dp2px(17), SizeUtils.dp2px(17));
//            mServiceBtn.setCompoundDrawablePadding(SizeUtils.dp2px(5));
            mServiceBtn.setCompoundDrawables(null, drawable, null, null);
        }

        // CCM抵扣活动
        if (mSkuInfo.activityBrief != null && !mSkuInfo.activityBrief.isEmpty()) {
            activityListLayout.setVisibility(View.VISIBLE);
            for (String activity : mSkuInfo.activityBrief) {
                activityListLayout.addItem(activity);
            }
//            activityListLayout.addItem("abd<span style=\"color:red\">cdd</span>");
        } else {
            activityListLayout.setVisibility(View.GONE);
        }
        mTvTagTitle.setText(mSkuInfo.name);
        if (StringUtils.isEmpty(mSkuInfo.desc)) {
            mTvDesc.setVisibility(View.GONE);
        } else {
            mTvDesc.setText(mSkuInfo.desc);
        }
        mTvSkuInfo.setText(mSkuInfo.properties);
        mTvRetailPrice.setText(MoneyUtil.centToYuan¥StrNoZero(mSkuInfo.retailPrice));
        mTvMarketPrice.setText(MoneyUtil.centToYuan¥StrNoZero(mSkuInfo.marketPrice));
        if (mSkuInfo.extType != 2 && mSkuInfo.getTypePrice(AppTypes.FAMILY.MEMBER_ZUNXIANG) != mSkuInfo.retailPrice && Config.IS_DISCOUNT) {
            mTvVipPrice.setText(String.format("尊享价：%s", MoneyUtil.centToYuan¥StrNoZero(mSkuInfo.getTypePrice(AppTypes.FAMILY.MEMBER_ZUNXIANG))));
            mTvVipPrice.setVisibility(View.VISIBLE);
        } else {
            mTvVipPrice.setVisibility(View.GONE);
        }
        TextViewUtil.addThroughLine(mTvMarketPrice);
        mFavBtn.setSelected(mSkuInfo.isFav == 1);

        if (mSkuInfo.isShowVipView() && Config.IS_DISCOUNT) {
            mLayoutVipInfo.setVisibility(View.VISIBLE);
            mTvVipJoin.setText(Html.fromHtml(String.format("店主购买本商品至少可返<font color =\"#f51861\">%s</font>元", ConvertUtil.centToCurrencyNoZero(this, mSkuInfo.getVipRefundPrice()))));
        } else {
            mLayoutVipInfo.setVisibility(View.GONE);
        }

        if (mSkuInfo.productType == 11) {
            mTvGolden.setVisibility(View.VISIBLE);
            mTvGolden.setText(String.format("+%s金券", MoneyUtil.centToYuanStrNoZero(mSkuInfo.goldenTicket)));
        }

        if (TextUtils.isEmpty(mSkuInfo.skuEarnings)) {
            mTvCc.setVisibility(View.GONE);
        } else {
            mTvCc.setVisibility(View.VISIBLE);
            mTvCc.setText(Html.fromHtml(mSkuInfo.skuEarnings));
        }

        setNewerConfig();
        setActivityView();
        // 店铺
        if (TextUtils.isEmpty(mSkuInfo.storeId)) {
            shopContainer.setVisibility(View.GONE);
        } else {
            shopContainer.setVisibility(View.VISIBLE);
            shopName.setText(mSkuInfo.storeName);
//            FrescoUtil.setImage(shopIcon, mSkuInfo.thumb);
        }
    }

    /**
     * 新人专区
     */
    private void setNewerConfig() {
        if (mSkuInfo.productType == 9 && mSkuInfo.newerConfig != null && (!mSkuInfo.newerConfig.canBuy || mSkuInfo.newerConfig.locked)) {
            mTvSaleTime.setVisibility(View.VISIBLE);
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mLyVideoPic.getLayoutParams();
            layoutParams.bottomMargin = 45;
            mLyVideoPic.setLayoutParams(layoutParams);

            RelativeLayout.LayoutParams layoutParams1 = (RelativeLayout.LayoutParams) mTvBannerImageText.getLayoutParams();
            layoutParams1.bottomMargin = 45;
            mTvBannerImageText.setLayoutParams(layoutParams1);

            TextViewUtil.setNewerBuyDate(NewProductDetailActivity.this, mTvSaleTime,
                    mSkuInfo.newerConfig, "该商品为新人专区商品,%s可购买",
                    "该商品为新人专区商品,还剩%d:%d:%d可购买", true);
        }
    }

    /**
     * 设置促销活动的 view
     */
    @SuppressLint("ClickableViewAccessibility")
    private void setActivityView() {
        ArrayList<ProductActivityModel> models = getActivityMoodels(false);
        if (models.size() > 0) {
            mLayoutActivity.setVisibility(View.VISIBLE);
            mRvActivity.setLayoutManager(new LinearLayoutManager(this));
            ProductActivityAdapter productActivityAdapter = new ProductActivityAdapter(models, false);
            productActivityAdapter.setSingLine(true);
            mRvActivity.setAdapter(productActivityAdapter);
            mRvActivity.addItemDecoration(new SpacesItemDecoration(SizeUtils.dp2px(5)));
            mRvActivity.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        layoutActivityClick();
                    }
                    return true;
                }
            });
        } else {
            mLayoutActivity.setVisibility(View.GONE);
        }
    }

    /**
     * 获取所有促销的 item  包括赠品相关的东西
     *
     * @param containsImg
     * @return
     */
    private ArrayList<ProductActivityModel> getActivityMoodels(boolean containsImg) {
        ArrayList<ProductActivityModel> models = new ArrayList<>();
        if (mSkuInfo.activityTag != null && mSkuInfo.activityTag.size() > 0) {
            for (SkuInfo.ActivityTagEntity activityTagEntity : mSkuInfo.activityTag) {
                models.add(new ProductActivityModel(ProductActivityModel.TEXT, activityTagEntity));
            }

        }
        if (mSkuInfo.presents != null && mSkuInfo.presents.size() > 0) {
            StringBuffer presentsTags = new StringBuffer();
            for (Presents present : mSkuInfo.presents) {
                presentsTags.append(present.skuName).append("*").append(present.quantity).append("; ");
            }
            SkuInfo.ActivityTagEntity activityTagEntity = new SkuInfo.ActivityTagEntity();
            // todo replace icon url
            activityTagEntity.iconUrl = "http://static.wyhou.com/wyhou/1546811647084.jpg";
            activityTagEntity.title = presentsTags.toString();
            models.add(new ProductActivityModel(ProductActivityModel.TEXT, activityTagEntity));
            if (containsImg) {
                models.add(new ProductActivityModel(ProductActivityModel.IMG, mSkuInfo.presents));
            }
        }
        return models;
    }


    private void setVideoViewPager() {
        ProductVideoViewPagerAdapter adapter = new ProductVideoViewPagerAdapter(this, mProduct);
        mViewPager.setAdapter(adapter);
        final boolean isShowVideo = !StringUtils.isEmpty(mProduct.mediaUrl);
        if (!isShowVideo) {
            mIvBannerVideo.setVisibility(View.GONE);
            mIvBannerImage.setVisibility(View.GONE);
            mTvBannerImageText.setText(String.format("%d/%d", 1, mSkuInfo.images.size()));
            mTvBannerImageText.setVisibility(View.VISIBLE);
        } else {
            mTvBannerImageText.setVisibility(View.GONE);
        }
        mIvBannerVideo.setSelected(true);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (isShowVideo) {
                    if (position != 0) {
                        GSYVideoManager.onPause();
                    }
                }

                mIvBannerVideo.setSelected(position == 0);
                mIvBannerImage.setSelected(position != 0);
                if (position != 0 || !isShowVideo) {
                    int add = isShowVideo ? 0 : 1;
                    mTvBannerImageText.setVisibility(View.VISIBLE);
                    mTvBannerImageText.setText(String.format("%d/%d", position + add, mSkuInfo.images.size()));
                } else {
                    mTvBannerImageText.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mIvBannerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(1);
            }
        });
        mIvBannerVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(0);
            }
        });
    }

    private void initView() {
        initPrtView();
        initProductVideoView();
        initScrollView();
        WebViewUtil.configWebView(mWebview);
        mTvSkuInfo.setVisibility(mType == TYPE_ADD_GIFT || mType == TYPE_MY_GIFT ? View.GONE : View.VISIBLE);
        mLayoutProductBottom.setVisibility(mType == TYPE_ADD_GIFT || mType == TYPE_MY_GIFT ? View.GONE : View.VISIBLE);
        mQrCodeBtn.setVisibility(mType == TYPE_ADD_GIFT || mType == TYPE_MY_GIFT ? View.GONE : View.VISIBLE);
        mShareBtn.setVisibility(mType == TYPE_ADD_GIFT ? View.GONE : View.VISIBLE);
        mTvAddGift.setVisibility(mType == TYPE_ADD_GIFT || mStatus == 0 ? View.VISIBLE : View.GONE);
        mLayoutGift.setVisibility(mType == TYPE_MY_GIFT && mStatus == 1 ? View.VISIBLE : View.GONE);
    }

    private void initProductVideoView() {
        WindowManager wm = (WindowManager) this
                .getSystemService(Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mTopProList.getLayoutParams();
        layoutParams.height = width;
        mTopProList.setLayoutParams(layoutParams);
    }

    /**
     * 设置那些滑动变换的 view
     */
    private void initScrollView() {
        mLayoutDragScroll.setOnSlideDetailsListener(new DragScrollDetailsLayout.OnSlideFinishListener() {
            @Override
            public void onStatueChanged(DragScrollDetailsLayout.CurrentTargetIndex status) {
                switch (status) {
                    case UPSTAIRS:
                        LogUtils.e("滑到上面");
                        mShareBtn.setVisibility(mType == TYPE_MY_GIFT ? View.VISIBLE : View.GONE);
                        mIvBack.setVisibility(View.VISIBLE);
                        mQrCodeBtn.setVisibility(mType == TYPE_ADD_GIFT || mType == TYPE_MY_GIFT ? View.GONE : View.VISIBLE);
                        mLayoutTitle.setVisibility(View.GONE);
                        break;
                    case DOWNSTAIRS:
                        LogUtils.e("滑到下面");
                        mShareBtn.setVisibility(View.GONE);
                        mIvBack.setVisibility(View.GONE);
                        mQrCodeBtn.setVisibility(View.GONE);
                        mLayoutTitle.setVisibility(View.VISIBLE);
                        break;
                    default:

                }
            }
        });
    }

    /**
     * 初始化下拉弹出足迹框的控件
     */
    private void initPrtView() {
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshlayout) {
                if (mViewHistoryDatas == null || mViewHistoryDatas.size() < 1) {
                    getViewHistoryData(true);
                } else {
                    mRefreshLayout.finishRefresh(100);
                    showViewHistoryDialog(mViewHistoryDatas);
                }

            }
        });
    }

    /**
     * 获取浏览记录
     */
    private void getViewHistoryData(final boolean isShow) {
        APIManager.startRequest(mProductService.getViewHistory(), new BaseRequestListener<ViewHistory>() {
            @Override
            public void onSuccess(ViewHistory result) {
                super.onSuccess(result);
                mViewHistoryDatas = result.datas;
                if (isShow) {
                    showViewHistoryDialog(mViewHistoryDatas);
                }
            }
        }, this);
    }

    private void showViewHistoryDialog(List<SkuInfo> skuInfos) {
        if (isDestroyed()) {
            return;
        }
        if (skuInfos == null || skuInfos.size() < 1) {
            return;
        }
        if (mDialog == null) {
            mDialog = new ZujiDialog(this);
        }
        mDialog.setTitle("我的足迹 (1/" + skuInfos.size() + ")");
        mDialog.setViewPager(skuInfos, getSupportFragmentManager());
        mDialog.show();
    }

    private void getIntentData() {
        mSkuId = getIntent().getStringExtra(Key.SKU_ID);
        mType = getIntent().getIntExtra("type", 0);
        mMemberSkuId = getIntent().getStringExtra("memberSkuId");
        mStatus = getIntent().getIntExtra("status", -1);
        liveId = getIntent().getStringExtra("liveId");
        if (StringUtils.isEmpty(mSkuId)) {
            ToastUtil.error("参数错误");
            finish();
        }
    }

    @OnClick({R.id.serviceBtn})
    public void onMServiceBtnClicked() {
        // 接入客服
        if (mSkuInfo == null) {
            return;
        }
        if (mSkuInfo.isLiveStoreProduct()) {
            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (loginUser != null) {
                TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                    @Override
                    public void onError(int i, String s) {
                    }

                    @Override
                    public void onSuccess() {
                        IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                        APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                        }, NewProductDetailActivity.this);
                        ChatInfo chatInfo = new ChatInfo();
                        chatInfo.setType(TIMConversationType.C2C);
                        chatInfo.setId(mSkuInfo.storeOwnerMemberId);
                        chatInfo.setChatName(mSkuInfo.storeName);
//                    chatInfo.setId(mOrder.orderMain.memberId);
//                    chatInfo.setChatName(mOrder.orderMain.nickName);
                        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                        intent.putExtra("chatInfo", chatInfo);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
            }
        } else {
            CSUtils.start(this, "从产品详情点进来的，正在查看 " + mSkuInfo.name);
        }

    }

    @OnClick({R.id.viewStoreProductBtn})
    public void onStoreClicked() {
        // 进入店铺主页
        Intent intent = new Intent(this, CustomPageActivity.class);
        intent.putExtra("pageId", mSkuInfo.storeId);
        startActivity(intent);
    }

    @OnClick(R.id.favBtn)
    public void onMFavBtnClicked() {
        if (mSkuInfo == null) {
            return;
        }
        String url;
        String urlAddCollect = "collect/addCollect";
        String urlDelCollect = "collect/delCollect";
        url = mFavBtn.isSelected() ? urlDelCollect : urlAddCollect;
        APIManager.startRequest(mCollectService.changeCollect(url, mSkuInfo.skuId), new BaseRequestListener<PaginationEntity<SkuInfo, Object>>(this) {
            @Override
            public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                mFavBtn.setSelected(!mFavBtn.isSelected());
            }
        }, this);
    }

    @OnClick(R.id.cartBtnLayout)
    public void onMCartBtnClicked() {
        if (UiUtils.checkUserLogin(this)) {
            Intent intent = new Intent(this, CartActivity.class);
            intent.putExtra("tab", "cart");
            intent.putExtra("from", "product");
            startActivity(intent);
        }
    }

    @OnClick({R.id.addToCartBtn, R.id.layoutNoGroup})
    public void onMAddToCartBtnClicked() {
        if (mProduct == null || mSkuInfo == null) {
            return;
        }
        mSkuSelectorDialog = new SkuSelectorDialog(this, mProduct, mSkuInfo, AppTypes.SKU_SELECTOR_DIALOG.ACTION_CART, this, orderType);
        mSkuSelectorDialog.setSelectListener(new SkuDialogSelectLietener());
        mSkuSelectorDialog.setLiveId(liveId);
        mSkuSelectorDialog.show();
    }

    @OnClick(R.id.buyNowBtn)
    public void onMBuyNowBtnClicked() {
        if (mProduct == null || mSkuInfo == null) {
            return;
        }
        mSkuSelectorDialog = new SkuSelectorDialog(this, mProduct, mSkuInfo, AppTypes.SKU_SELECTOR_DIALOG.ACTION_BUY, this, orderType);
        mSkuSelectorDialog.setSelectListener(new SkuDialogSelectLietener());
        mSkuSelectorDialog.setLiveId(liveId);
        mSkuSelectorDialog.show();
    }

    @OnClick(R.id.tvSkuInfo)
    protected void showSkuSelectorDialog(View view) {
        if (mProduct != null && mSkuInfo != null) {
            mSkuSelectorDialog = new SkuSelectorDialog(this, mProduct, mSkuInfo, getAction(), this, orderType);
            mSkuSelectorDialog.setSelectListener(new SkuDialogSelectLietener());
            mSkuSelectorDialog.setLiveId(liveId);
            mSkuSelectorDialog.show();
//            mSkuSelectorDialog.showBottomText(mProduct.extType == 5 &&
//                    null != mProduct.groupExt.initiatorBean && !TextUtils.isEmpty(mProduct.groupExt.initiatorBean.groupCode));
        }
    }

    private int getAction() {
        if (mSkuInfo.productType == 2) {
            return AppTypes.SKU_SELECTOR_DIALOG.ACTION_BUY;
        } else if (mProduct.extType == 5) {
            return AppTypes.SKU_SELECTOR_DIALOG.ACTION_CREATE_SUPER_GROUP;
        } else {
            return AppTypes.SKU_SELECTOR_DIALOG.ACTION_CART;
        }
    }

    @OnClick(R.id.goToShop)
    public void onViewClicked() {
        if (mSkuInfo == null) {
            return;
        }

        ShopActivity.start(this, mSkuInfo.storeId, mSkuInfo.isLiveStoreProduct());
    }


    private class SkuDialogSelectLietener implements SkuSelectorDialog.OnSelectListener {
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        @Override
        public void onSelectSku(SkuInfo skuInfo) {
            mSkuInfo = skuInfo;
            /*if (mSkuInfo.productType == 12) {
                if (mSkuSelectorDialog.isShowing()) {
                    return;
                }
                AlertDialog.Builder alert = new AlertDialog.Builder(NewProductDetailActivity.this, R.style.MyDialog);
                alert.setView(R.layout.input_phone_dialog);
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }*/
            setSkuViews();
            setBottomViews();
        }
    }

    @OnClick(R.id.productAuthLayout)
    protected void showProductAuth() {
        if (mProduct != null && mProduct.auths != null) {
            new ProductVerifyDialog(this, mProduct.auths).show();
        }
    }

    @OnClick({R.id.ivBack, R.id.ivBack2})
    public void onBackClicked() {
        finish();
    }

    @OnClick({R.id.couponLayout})
    public void onReceviceCoupon() {
        if (mCouponList != null) {
            mCouponBottomDialog = new CouponBottomDialog(this);
            mCouponBottomDialog.show();
            mCouponBottomDialog.setData(mCouponList);
        }
    }

    @OnClick({R.id.shareBtn, R.id.ivShare})
    public void onViewClicked(View view) {
        if (mSkuInfo == null) {
            ToastUtil.error("等待数据");
            return;
        } else if (UiUtils.checkUserLogin(this)) {
            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (mType == TYPE_MY_GIFT) {
                ShareUtils.showShareDialog(this, mSkuInfo.name, mSkuInfo.desc, mSkuInfo.thumb, BuildConfig.WECHAT_URL + "p/" +
                        mMemberSkuId + "/" + loginUser.invitationCode + "?gift=shareGift");
            } else {
                ShareUtils.showShareDialog(this, mSkuInfo.name, mSkuInfo.desc, mSkuInfo.thumb, BuildConfig.WECHAT_URL + "p/" +
                        mSkuInfo.skuId + "/" + loginUser.invitationCode);
            }
        }
    }

    @OnClick({R.id.ivQrCode, R.id.qrCodeBtn})
    public void onQrcodeClick(View view) {
        if (mSkuInfo == null || mProduct == null) {
            ToastUtil.error("等待数据");
            return;
        } else if (UiUtils.checkUserLogin(this)) {
            long price;
            if (mProduct.extType == 2) {
                price = mProduct.getGroupEntity(mSkuInfo.skuId).groupPrice;
            } else {
                price = mSkuInfo.retailPrice;
            }
//            intent.putExtra("imgUrl", mSkuInfo.thumb);
//            intent.putExtra("linkUrl", BuildConfig.WECHAT_URL + "p/" + mSkuInfo.skuId + "/" + loginUser.invitationCode);
//            intent.putExtra("skuName", mSkuInfo.name);
//            intent.putExtra("price", price);

            Intent intent = new Intent(this, ProductQrcodeShowActivity.class);
            intent.putExtra("sku", mSkuInfo);
            intent.putExtra("spu", mProduct);
            startActivity(intent);
        }
    }

    @OnClick(R.id.tvCommentAll)
    public void onCommentAll() {
        if (mProduct == null) {
            ToastUtil.error("等待数据");
            return;
        }
        Intent intent = new Intent(this, ProductCommentListActivity.class);
        intent.putExtra("id", mProduct.productId);
        intent.putExtra("img", mProduct.thumb);
        intent.putExtra("name", mProduct.name);
        startActivity(intent);
    }

    @OnClick({R.id.layoutActivity})
    public void layoutActivityClick() {
        ProductActivityDialog productActivityDialog = new ProductActivityDialog(this);
        productActivityDialog.show();
        productActivityDialog.setData(getActivityMoodels(true));
    }

//    @OnClick(R.id.layoutVipInfo)
//    public void onViewClicked() {
////        startActivity(new Intent(this, ShopkeeperActivity.class));
//        Intent intent = new Intent(this, CustomPageActivity.class);
//        intent.putExtra("pageId", Key.PAGE_TO_BE_SHOPKEEPER);
//        startActivity(intent);
//    }

    @OnClick(R.id.layoutStartGroup)
    public void onMLayoutStartGroupClicked() {
        // 团购下单
        mSkuSelectorDialog = new SkuSelectorDialog(this, mProduct, mSkuInfo, AppTypes.SKU_SELECTOR_DIALOG.ACTION_CREATE_GROUP);
        mSkuSelectorDialog.setSelectListener(new SkuDialogSelectLietener());
        mSkuSelectorDialog.show();
    }

    @OnClick(R.id.layoutSuperGroup)
    public void onSuperGroupClicked() {
        if (UiUtils.checkUserLogin(NewProductDetailActivity.this)) {
            if (null == mProduct.groupExt.initiatorBean || TextUtils.isEmpty(mProduct.groupExt.initiatorBean.groupCode)) {
                // 超级团购下单
                mSkuSelectorDialog = new SkuSelectorDialog(this, mProduct, mSkuInfo, AppTypes.SKU_SELECTOR_DIALOG.ACTION_CREATE_SUPER_GROUP);
                mSkuSelectorDialog.setSelectListener(new SkuDialogSelectLietener());
                mSkuSelectorDialog.show();
            } else {
                //拼团详情
                Intent intent = new Intent(NewProductDetailActivity.this, JoinGroupActivity.class);
                intent.putExtra(Config.INTENT_KEY_TYPE_NAME, JoinGroupActivity.TYPE_HOST);
                intent.putExtra(Config.INTENT_KEY_ID, mProduct.groupExt.initiatorBean.groupCode);
                intent.putExtra("isSuperGroup", true);
                startActivity(intent);
            }
        }
    }

    @OnClick({R.id.tvAddGift, R.id.tvEditStock})
    protected void addGift(View view) {
        if (view.getId() == R.id.tvAddGift && mType == TYPE_ADD_GIFT) {
            if (null == mEditStockDialog) {
                mEditStockDialog = new EditStockDialog(NewProductDetailActivity.this, mOnConfirmListener);
                mEditStockDialog.setPropertyValue(getPropertyValue());
            }
        } else {
            if (null == mEditStockDialog) {
                mEditStockDialog = new EditStockDialog(NewProductDetailActivity.this, mOnConfirmListener);
                mEditStockDialog.setPropertyValue(getMyPropertyValue());
            }
        }
        mEditStockDialog.show();
    }

//    @OnClick(R.id.tvEditStock)
//    protected void editGift() {
//
//        mEditStockDialog.show();
//    }


    @OnClick(R.id.tvRemoveGift)
    protected void removeGift() {
        WJDialog wjDialog = new WJDialog(NewProductDetailActivity.this);
        wjDialog.show();
        wjDialog.setContentText("是否下架该产品？");
        wjDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<JoinProduct> deleteProductList = new ArrayList<>();
                for (SkuInfo skuInfo : mMyGiftSkuList) {
                    JoinProduct joinProduct = new JoinProduct();
                    joinProduct.skuId = skuInfo.skuId;
                    joinProduct.status = "0";
                    joinProduct.stock = String.valueOf(skuInfo.stock);
                    deleteProductList.add(joinProduct);
                }
                deleteProduct(deleteProductList);
                wjDialog.dismiss();
            }
        });
    }


    /**
     * 获取sku规格信息
     *
     * @return
     */
    private List<PropertyValue> getPropertyValue() {
        List<PropertyValue> list = new ArrayList<>();
        if (null == mSkuInfoList || mSkuInfoList.size() == 0) {
            return list;
        }
        for (SkuInfo skuInfo : mSkuInfoList) {
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.value = skuInfo.properties;
            propertyValue.skuId = skuInfo.skuId;
            list.add(propertyValue);
        }
        return list;
    }

    /**
     * 获取sku规格信息
     *
     * @return
     */
    private List<PropertyValue> getMyPropertyValue() {
        List<PropertyValue> list = new ArrayList<>();
        if (null == mMyGiftSkuList || mMyGiftSkuList.size() == 0) {
            return list;
        }
        for (SkuInfo skus : mMyGiftSkuList) {
            PropertyValue propertyValue = new PropertyValue();
            propertyValue.value = skus.properties;
            propertyValue.stock = String.valueOf(skus.stock);
            propertyValue.skuId = skus.skuId;
            list.add(propertyValue);
        }
        return list;
    }

    /**
     * 编辑产品库存弹窗回调
     */
    private EditStockDialog.OnConfirmListener mOnConfirmListener = new EditStockDialog.OnConfirmListener() {
        @Override
        public void onConfirm(List<PropertyValue> list) {
            ArrayList<JoinProduct> joinProductList = new ArrayList<>();
            for (PropertyValue value : list) {
                JoinProduct joinProduct = new JoinProduct();
                joinProduct.skuId = value.skuId;
                joinProduct.status = "1";
                joinProduct.stock = value.stock;
                joinProductList.add(joinProduct);
            }
            if (mType == TYPE_ADD_GIFT) {
                joinProduct(joinProductList);
            } else {
                editProduct(joinProductList);
            }

        }
    };

    /**
     * 上架礼包产品
     *
     * @param list
     */
    private void joinProduct(ArrayList<JoinProduct> list) {
        APIManager.startRequest(mProductService.joinProduct(list), new BaseRequestListener<Object>(NewProductDetailActivity.this) {
            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("添加成功");
                EventBus.getDefault().post(new EventMessage(Event.joinProduct));
                finish();
            }
        }, this);
    }

    /**
     * 修改礼包产品
     *
     * @param list
     */
    private void editProduct(ArrayList<JoinProduct> list) {
        APIManager.startRequest(mProductService.editProduct(list), new BaseRequestListener<Object>(NewProductDetailActivity.this) {
            @Override
            public void onSuccess(Object result) {
                ToastUtil.success(mStatus == 0 ? "上架成功" : "修改成功");
                getProductsSkusByMemberId(getSkuIdList());
                EventBus.getDefault().post(new EventMessage(Event.joinProduct));
                if (mStatus == 0) {
                    finish();
                }
            }
        }, this);
    }

    /**
     * 下架礼包产品
     *
     * @param list
     */
    private void deleteProduct(ArrayList<JoinProduct> list) {
        APIManager.startRequest(mProductService.editProduct(list), new BaseRequestListener<Object>(NewProductDetailActivity.this) {
            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("下架成功");
                EventBus.getDefault().post(new EventMessage(Event.delectProduct));
                finish();
            }
        }, this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getMsg(MsgInstant msgInstant) {
        switch (msgInstant.getAction()) {
            case MsgInstant.ACTION_BUY:
                mInstantBuy = true;
                EventBus.getDefault().removeStickyEvent(msgInstant);
                break;
            default:
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(EventMessage message) {
        switch (message.getEvent()) {
            case cartAmountUpdate:
                if (mSkuSelectorDialog != null) {
                    mSkuSelectorDialog.dismiss();
                }
                if (mSkuInfo.productType != 18) {
                    int total = (int) message.getData();
                    mTvcartBtnBadge.setText(total > 99 ? "99+" : String.valueOf(total));
                    mTvcartBtnBadge.setVisibility(total > 0 ? View.VISIBLE : View.GONE);
                }

                break;
            case sendSelectDialog:
                MsgGroupDialog msgGroupDialog = new MsgGroupDialog(mSkuInfo, mProduct);
                EventBus.getDefault().postSticky(msgGroupDialog);
                break;
            case createOrderSuccess:
            case goToLogin:
                finish();
            default:
        }
    }
}
