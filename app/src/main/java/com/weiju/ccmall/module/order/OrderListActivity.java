package com.weiju.ccmall.module.order;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.weiju.ccmall.R;
import com.weiju.ccmall.newRetail.activity.StoreOrderSearchActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.util.ConvertUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Chan on 2017/6/16.
 *
 * @author Chan
 * @package com.weiju.ccmall.module.order
 * @since 2017/6/16 下午6:37
 */

public class OrderListActivity extends BaseActivity {

    @BindView(R.id.magicIndicator)
    protected MagicIndicator mMagicIndicator;
    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;

    protected List<BaseFragment> mFragments = new ArrayList<>();
    protected List<Page> mPages = new ArrayList<>();
    private String mType;
    private int mMode;
    public final static int MODE_BUYER = 0;
    public final static int MODE_SELLER = 1;

    public final static int MODE_MY_STORE = 2;

    public final static int MODE_MY_SALE = 3;

    public final static int MODE_MY_ORDER = 4;

    /**
     * 礼包订单
     */
    public final static int MODE_MY_GIFT = 5;

    //直播店铺订单
    public final static int MODE_LIVE_STORE = 6;
    private CommonNavigator mCommonNavigator;
    private int mCount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_tab_page);
        ButterKnife.bind(this);
        getIntentData();
        initData();
        initView();
        selectTabItem(mType);
    }


    private void getIntentData() {
        Intent intent = getIntent();
        mMode = intent.getIntExtra("mode", MODE_BUYER);
        mType = intent.getExtras().getString("type");
        mCount = intent.getExtras().getInt("waitShipCount");
    }

    private void initView() {
        switch (mMode) {
            case MODE_BUYER:
                setTitle("我的订单");
                break;
            case MODE_MY_GIFT:
                setTitle("订单管理");
                break;
            case MODE_SELLER:
                setTitle("我的销售订单");
                break;
            case MODE_LIVE_STORE:
                setTitle("店铺订单");
                break;
            default:
        }
        setLeftBlack();
        initViewPager();
        initIndicator();
    }

    private void initViewPager() {
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mPages.get(position).name;
            }
        });
        mViewPager.setOffscreenPageLimit(mFragments.size());
    }

    public void upDateCount(int total) {
        if (mMode == MODE_LIVE_STORE) {
            mPages.get(1).name = "待发货(" + total + ")";
        }
        mCommonNavigator.notifyDataSetChanged();
    }

    private void initIndicator() {
        mCommonNavigator = new CommonNavigator(this);
        mCommonNavigator.setAdjustMode(true);
        mCommonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mPages.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(mPages.get(index).name);
                titleView.setNormalColor(getResources().getColor(R.color.text_black));
                titleView.setSelectedColor(getResources().getColor(R.color.red));
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(16);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getResources().getColor(R.color.red));
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                return indicator;
            }
        });
        mMagicIndicator.setNavigator(mCommonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    private void initData() {
        switch (mMode) {
            case MODE_BUYER:
                setBuyerData();
                break;
            case MODE_MY_GIFT:
                setGiftData();
                break;
            case MODE_SELLER:
                setSellerData();
                break;
            case MODE_LIVE_STORE:
                setLiveStoreData();
                break;
            default:
        }
    }

    private void setSellerData() {
        mPages.add(new Page(AppTypes.ORDER.SELLER_WAIT_SHIP, "待发货", mMode));
        mPages.add(new Page(AppTypes.ORDER.SELLER_HAS_SHIP, "已发货", mMode));
        mPages.add(new Page(AppTypes.ORDER.SELLER_HAS_COMPLETE, "已收货", mMode));
        mPages.add(new Page(AppTypes.ORDER.SELLER_HAS_CLOSE, "已关闭", mMode));
        for (Page page : mPages) {
            mFragments.add(OrderListFragment.newInstance(page));
        }
    }

    private void setGiftData() {
        mPages.add(new Page("all", "全部", mMode));
        mPages.add(new Page("paid", "待发货", mMode));
        mPages.add(new Page("dispatched", "已发货", mMode));
        mPages.add(new Page("has-received", "已完成", mMode));
        for (Page page : mPages) {
            OrderListFragment orderListFragment = new OrderListFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("page", page);
            orderListFragment.setArguments(bundle);
            mFragments.add(orderListFragment);
        }
    }

    private void setLiveStoreData() {
        mHeaderLayout.setRightDrawable(R.mipmap.icon_search);
        mHeaderLayout.setOnRightClickListener(view -> {//去搜索订单页
            Intent intent = new Intent(OrderListActivity.this, StoreOrderSearchActivity.class);
            intent.putExtra("mode", mMode);
            startActivity(intent);
        });
        mPages.add(new Page("all", "全部", mMode));
        mPages.add(new Page("paid","待发货(" + mCount + ")", mMode));
        mPages.add(new Page("dispatched", "已发货", mMode));
        mPages.add(new Page("has-received", "已完成", mMode));
        for (Page page : mPages) {
            OrderListFragment orderListFragment = new OrderListFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("page", page);
            orderListFragment.setArguments(bundle);
            mFragments.add(orderListFragment);
        }
    }

    private void setBuyerData() {
        mPages.add(new Page("all", "全部", mMode));
        mPages.add(new Page("wait-pay", "待付款", mMode));
        mPages.add(new Page("paid", "待发货", mMode));
        mPages.add(new Page("dispatched", "已发货", mMode));
        for (Page page : mPages) {
            OrderListFragment orderListFragment = new OrderListFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("page", page);
            orderListFragment.setArguments(bundle);
            mFragments.add(orderListFragment);
        }
        /*setTitle("我的订单");
        mPages.add(new Page(AppTypes.ORDER.MY_ORDER_ALL, "全部", 4));
        mPages.add(new Page(AppTypes.ORDER.MY_ORDER_PENDING_PAYMENT, "待付款", 4));
//            mPages.add(new Page(AppTypes.ORDER.MY_ORDER_TO_BE_CONFIRMED, "待确认", 4));
        mPages.add(new Page(AppTypes.ORDER.MY_ORDER_TO_BE_DELIVERED, "待发货", 4));
        mPages.add(new Page(AppTypes.ORDER.MY_ORDER_PEDNDING_RECEIPT, "待收货", 4));

        for (Page page : mPages) {
            mFragments.add(OrderListFragment.newInstance(page));
        }*/


//        mPages.add(new Page("wait-comment", "待评价"));
//        mFragments.add(new WaitCommentFragment());
    }

    public void selectTabItem(String type) {
        int index = 0;
        for (Page page : mPages) {
            if (page.id.equalsIgnoreCase(type)) {
                index = mPages.indexOf(page);
                break;
            }
        }
        mViewPager.setCurrentItem(index);
    }
}
