package com.weiju.ccmall.module.groupBuy.adapter;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SuperMatch;

/**
 * @author chenyanming
 * @time 2019/8/15 on 10:46
 * @desc ${TODD}
 */
public class SuperMatchAdapter extends BaseQuickAdapter<SuperMatch, BaseViewHolder> {
    public SuperMatchAdapter() {
        super(R.layout.item_super_match);
    }

    @Override
    protected void convert(BaseViewHolder helper, SuperMatch item) {
        helper.setText(R.id.tvDate, String.format("拼团成功时间：%s", item.matchBean.successDate));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        linearLayoutManager.setAutoMeasureEnabled(true);
        RecyclerView recyclerView = helper.getView(R.id.itemRecyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);
        SuperMatchItemAdapter matchItemAdapter = new SuperMatchItemAdapter();
        matchItemAdapter.setNewData(item.memberList);
        recyclerView.setAdapter(matchItemAdapter);
    }
}
