package com.weiju.ccmall.module.xysh.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.AddEditDebitCardActivity;
import com.weiju.ccmall.module.xysh.activity.AddEditXinYongActivity;
import com.weiju.ccmall.module.xysh.activity.repayment.RepaymentDetailActivity;
import com.weiju.ccmall.module.xysh.adapter.BankAdminAdapter;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Bank;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.contracts.RequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * @author chenyanming
 * @time 2019/12/11 on 18:48
 * @desc
 */
public class BankAdminFragment extends BaseListFragment {

    @BindView(R.id.tvAdd)
    TextView mTvAdd;

    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    private BankAdminAdapter mAdapter;
    private int mType;
    private boolean selectMode;

    /**
     * @param type 信用卡：0    储蓄卡：1
     * @return
     */
    public static BankAdminFragment newInstance(int type, boolean select) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        args.putBoolean("selectMode", select);
        BankAdminFragment fragment = new BankAdminFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static BankAdminFragment newInstance(int type) {
        return newInstance(type, false);
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mType = getArguments().getInt("type");
        selectMode = getArguments().getBoolean("selectMode", false);
        mAdapter = new BankAdminAdapter(selectMode);
        mAdapter.setType(mType);
    }

    @Override
    public void initView() {
        super.initView();
        if (mType == 1) {
            mTvAdd.setText("添加新储蓄卡");
        }
        EventBus.getDefault().register(this);

    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (selectMode) {
            QueryUserBankCardResult.BankInfListBean selected = mAdapter.getData().get(position);
            Intent data = new Intent();
            data.putExtra("card", selected);
            getActivity().setResult(Activity.RESULT_OK, data);
            getActivity().finish();
            return;
        }
        if (view != null && view.getId() == R.id.tvCheckPlan) {
            // 查看计划
            RepaymentDetailActivity.start(getContext(), mAdapter.getData().get(position));
            return;
        }
        if (mType == 0) {
            Intent intent = new Intent(getActivity(), AddEditXinYongActivity.class);
            intent.putExtra("isEdit", true);
            intent.putExtra("bankInfListBean", mAdapter.getData().get(position));
            startActivity(intent);
        }else {
            Intent intent = new Intent(getActivity(), AddEditDebitCardActivity.class);
            intent.putExtra("isEdit", true);
            intent.putExtra("bankInfListBean", mAdapter.getData().get(position));
            startActivity(intent);
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(service.cardInfoGet(mType), new Observer<QueryUserBankCardResult>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(QueryUserBankCardResult queryUserBankCardResult) {
                mAdapter.setNewData(queryUserBankCardResult.bankInfList);
                mAdapter.loadMoreEnd(true);
            }

            @Override
            public void onError(Throwable e) {
                mRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onComplete() {
                mRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.fragment_bank_admin;
    }

    @OnClick(R.id.layoutAdd)
    protected void add() {
        if (mType == 0) {
            startActivity(new Intent(getActivity(), AddEditXinYongActivity.class));
        }else {
            startActivity(new Intent(getActivity(), AddEditDebitCardActivity.class));
        }

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
        switch (message.getEvent()) {
            case bankChange:
                getData(true);
                break;
            default:
        }
    }

}
