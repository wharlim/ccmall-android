package com.weiju.ccmall.module.xysh.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.CardDoctorHistoryItem;

public class CardDoctorHistoryAdapter extends BaseQuickAdapter<CardDoctorHistoryItem, BaseViewHolder> {
    public CardDoctorHistoryAdapter() {
        super(R.layout.item_card_doctor_hostory);
    }

    @Override
    protected void convert(BaseViewHolder helper, CardDoctorHistoryItem item) {
        helper.setText(R.id.tvBankName, item.property2);
        helper.setText(R.id.tvCardNo, item.property1);
        helper.setText(R.id.tvCreateTime, item.createDate);
    }
}
