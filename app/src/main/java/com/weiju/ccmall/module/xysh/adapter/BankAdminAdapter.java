package com.weiju.ccmall.module.xysh.adapter;

import android.graphics.Color;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.helper.BankUtils;

/**
 * @author chenyanming
 * @time 2019/12/11 on 18:49
 * @desc
 */
public class BankAdminAdapter extends BaseQuickAdapter<QueryUserBankCardResult.BankInfListBean, BaseViewHolder> {
    public BankAdminAdapter() {
        this(false);
    }

    public BankAdminAdapter(boolean select) {
        super(R.layout.item_bank_admin);
        selectMode = select;
    }

    private int mType;
    private boolean selectMode; // 选择模式

    public void setType(int type) {
        mType = type;
    }

    @Override
    protected void convert(BaseViewHolder helper, QueryUserBankCardResult.BankInfListBean item) {
        helper.setImageResource(R.id.ivAvatar, BankUtils.getBankIconByName(item.bankName));
        helper.setText(R.id.tvBankName, item.bankName);
        helper.setText(R.id.tvCardNumber, BankUtils.formatBankCardNo(item.cardNo));
        if (mType == 1) {
            helper.setVisible(R.id.tvCheckPlan, false);
            helper.setVisible(R.id.tvRePayDate, false);
            helper.setVisible(R.id.tvBill, false);
            helper.setVisible(R.id.tvRepay, false);
            helper.setVisible(R.id.iv_state, false);
        } else {
            helper.setText(R.id.tvBill, String.format("账单日  每月%s日", item.billDate));
            helper.setText(R.id.tvRepay, String.format("还款日  每月%s日", item.repayDate));
            helper.setText(R.id.tvRePayDate, String.format("还款计划 %s天后完成", item.planSts));
            if (item.isRunning()) {
                helper.setImageResource(R.id.iv_state, R.drawable.ic_plan_running);
            } else if (item.isSuccess()) {
                helper.setImageResource(R.id.iv_state, R.drawable.ic_plan_success);
            } else if (item.isFail()){
                helper.setImageResource(R.id.iv_state, R.drawable.ic_plan_fail);
            } else {
                helper.setImageResource(R.id.iv_state, R.drawable.ic_plan_none);
            }
            helper.addOnClickListener(R.id.tvCheckPlan);
        }

        if (selectMode) {
            helper.setVisible(R.id.tvCheckPlan, false);
            helper.setVisible(R.id.iv_state, false);
            helper.setVisible(R.id.ivEditMark, false);
        }
    }
}
