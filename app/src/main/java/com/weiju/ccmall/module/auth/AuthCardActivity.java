package com.weiju.ccmall.module.auth;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.ToastUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.auth.model.AuthModel;
import com.weiju.ccmall.module.auth.model.CardItemModel;
import com.weiju.ccmall.module.auth.model.body.AddCardBody;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.AutoAuthStatus;
import com.weiju.ccmall.shared.bean.CardUrl;
import com.weiju.ccmall.shared.bean.UploadResponse;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.component.dialog.CityPickerDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.manager.UploadManager;
import com.weiju.ccmall.shared.service.contract.ICaptchaService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ActivityControl;
import com.weiju.ccmall.shared.util.CountDownRxUtils;
import com.zhihu.matisse.Matisse;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class AuthCardActivity extends BaseActivity {
    @BindView(R.id.etYhZhihang)
    EditText mEtYhZhihang;
    @BindView(R.id.etCardNumber)
    EditText mEtCardNumber;
    @BindView(R.id.IvIdCard1)
    SimpleDraweeView mIvIdCard1;
    @BindView(R.id.ivDeleteCard1)
    ImageView mIvDeleteCard1;
    @BindView(R.id.tvSubmit)
    TextView mTvSubmit;
    @BindView(R.id.spSelectYH)
    Spinner mSpSelectYH;
    @BindView(R.id.tvYhLocation)
    TextView mTvYhLocation;
    @BindView(R.id.etPhoneNumber)
    EditText mEtPhoneNumber;
    @BindView(R.id.activity_auth_identity)
    ScrollView mActivityAuthIdentity;
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.tvNumber)
    TextView mTvNumber;
    @BindView(R.id.etCheckNumber)
    EditText mEtCheckNumber;
    @BindView(R.id.tvGetCheckNumber)
    TextView mTvGetCheckNumber;

    private String mImgUrl = "";
    private CityPickerDialog mCityPickerDialog;
    private String mProvince;
    private String mCity;
    private String mCounty;
    private CardItemModel mCardItemModel;
    private IUserService mService = ServiceManager.getInstance().createService(IUserService.class);
//    private CardDetailModel mModel;
    //    private String mUrl = "account/add";
    private String mUrl = "accountAuth/auth";
    private boolean mIsEdit;
    private ICaptchaService mCaptchaService;
    private final int TIME_COUNT = 60;
    private com.weiju.ccmall.module.auth.model.body.SubmitAuthBody authBody;
//    IUserService service = ServiceManager.getInstance().createService(IUserService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_card);
        ButterKnife.bind(this);
        initData();
        setTitle("绑定银行卡");
        setLeftBlack();
    }


    private void initData() {
        mIsEdit = getIntent().getBooleanExtra("isEdit", false);
        authBody = (com.weiju.ccmall.module.auth.model.body.SubmitAuthBody) getIntent().getSerializableExtra("AuthBody");
        if (authBody != null) {
            mTvName.setText(authBody.userName);
            mTvNumber.setText(authBody.identityCard);
        }

        mCityPickerDialog = new CityPickerDialog(this);
        mCaptchaService = ServiceManager.getInstance().createService(ICaptchaService.class);
        if (mIsEdit) {
//            mUrl = "account/edit";
            /*Observable<RequestResult<CardDetailModel>> gongCatCard = MyApplication.isGongCat ?
                    mService.getGongCatCard(SessionUtil.getInstance().getOAuthToken()) :
                    mService.getCard();
            APIManager.startRequest(gongCatCard, new BaseRequestListener<CardDetailModel>(this) {
                @Override
                public void onSuccess(CardDetailModel model) {
                    mModel = model;
//                    mTvName.setText(model.bankUser);
                    mEtCardNumber.setText(model.bankAccount);
                    mImgUrl = model.bankcardFrontImg;
                    mIvIdCard1.setImageURI(model.bankcardFrontImg);
//                    mCardItemModel = new CardItemModel(model.bankId, model.bankName);
                    mProvince = model.bankcardProvince;
                    mCity = model.bankcardCity;
                    mCounty = model.bankcardArea;
                    mTvYhLocation.setText(mProvince + mCity + mCounty);
                    mEtYhZhihang.setText(model.bankcardAddress);
                }
            },this);*/
            APIManager.startRequest(mService.getAuth(), new BaseRequestListener<AuthModel>(this) {
                @Override
                public void onSuccess(AuthModel result) {
                    AuthModel.MemberAccountBean model = result.memberAccountBean;
//                    mModel = model;
//                    mTvName.setText(model.bankUser);
//                    mEtCardNumber.setText(model.bankAccount);
                    mImgUrl = model.bankcardFrontImg;
                    mIvIdCard1.setImageURI(model.bankcardFrontImg);
//                    mCardItemModel = new CardItemModel(model.bankId, model.bankName);
                    mProvince = model.bankcardProvince;
                    mCity = model.bankcardCity;
                    mCounty = model.bankcardArea;
                    mTvYhLocation.setText(mProvince + mCity + mCounty);
                    mEtYhZhihang.setText(model.bankcardAddress);
                }
            },this);
        }

        APIManager.startRequest(mService.getBankList(), new BaseRequestListener<List<CardItemModel>>(this) {
            @Override
            public void onSuccess(List<CardItemModel> cardItemModels) {
                initSprinner(cardItemModels);
            }
        },this);

        /*APIManager.startRequest(
                mService.getAuth(),
                new BaseRequestListener<com.weiju.ccmall.module.auth.model.AuthModel>(this) {
                    @Override
                    public void onSuccess(AuthModel result) {
                        mTvName.setText(result.getUserName());
                        mTvNumber.setText(result.getIdentityCard());
                    }
                }
        );*/
    }

    private void initSprinner(final List<CardItemModel> cardItemModels) {
        ArrayList<String> names = new ArrayList<>();
        CardItemModel cardItemModel1 = new CardItemModel("0", "请选择开户银行");
        cardItemModels.add(0, cardItemModel1);
        for (CardItemModel cardItemModel : cardItemModels) {
            names.add(cardItemModel.bankName);
        }
        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, names);
        mSpSelectYH.setAdapter(adapter);
        mSpSelectYH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCardItemModel = cardItemModels.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @OnClick({R.id.ivDeleteCard1, R.id.tvSubmit, R.id.tvYhLocation, R.id.IvIdCard1})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivDeleteCard1:
                mIvIdCard1.setImageURI("");
                mImgUrl = "";
                mIvDeleteCard1.setVisibility(View.GONE);
                break;
            case R.id.tvSubmit:
                if (checkData()) {
                    addCard();
                }
                break;
            case R.id.tvYhLocation:
                showLocationWindow();
                break;
            case R.id.IvIdCard1:
                UploadManager.selectImage(this);
                break;
        }
    }

    private void uploadImage(final Uri uri) {
        UploadManager.uploadImage(this, uri, new BaseRequestListener<UploadResponse>(this) {
            @Override
            public void onSuccess(UploadResponse result) {
                mImgUrl = result.url;
                mIvIdCard1.setImageURI(result.url);
                mIvDeleteCard1.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showLocationWindow() {
        mCityPickerDialog.showPickerView(new CityPickerDialog.CitySelectListener() {
            @Override
            public void select(String province, String city, String county) {
                mProvince = province;
                mCity = city;
                mCounty = county;

                mTvYhLocation.setText(mProvince + mCity + mCounty);
            }
        });
    }

    @SuppressLint("CheckResult")
    private void addCard() {

        //bankcardFrontImg,bankId,bankcardAddress,bankAccount,bankcardProvince,bankcardCity,bankcardArea,checkNumber,bankReservedPhone,idcardFrontImg,idcardBackImg,idcardHeadImg,identityCard,userName
        AddCardBody addCardBody = new AddCardBody(mImgUrl, mCardItemModel.bankId, mEtYhZhihang.getText().toString().trim(),
                mEtCardNumber.getText().toString().trim(), mProvince, mCity, mCounty, mEtCheckNumber.getText().toString().trim(), mEtPhoneNumber.getText().toString(),
                authBody.idcardFrontImg, authBody.idcardBackImg, authBody.idcardHeadImg, authBody.identityCard, authBody.userName);

        APIManager.startRequest(mService.addCard(mUrl, addCardBody.toMap()), new BaseRequestListener<CardUrl>(this) {

            @Override
            public void onSuccess(CardUrl result) {
                super.onSuccess(result);
                new Thread() {
                    @Override
                    public void run() {
                        super.run();
                        APIManager.startRequest(mService.getStatus(), new Observer<RequestResult<AutoAuthStatus>>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(RequestResult<AutoAuthStatus> autoAuthStatusRequestResult) {
                                AutoAuthStatus autoAuthStatus = autoAuthStatusRequestResult.data;
                                if (autoAuthStatusRequestResult.isSuccess()) {
                                    if (2 == autoAuthStatus.autoAuthStatus) {
                                        ActivityControl.getInstance().add(AuthCardActivity.this);
//                                        startActivity(new Intent(AuthCardActivity.this, AuthIdentitySuccssdActivity.class));
                                        AuthIdentitySuccssdActivity.start(AuthCardActivity.this, authBody.identityCard, mEtCardNumber.getText().toString().trim());
                                        EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_USER_CHANGE));
                                    } else if (1 == autoAuthStatus.autoAuthStatus) {
                                        startActivity(new Intent(AuthCardActivity.this, SubmitStatusActivity.class));
                                        MsgStatus msgStatus = new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_FAIL);
//                                        msgStatus.setTips(autoAuthStatusRequestResult.data.);
                                        ActivityControl.getInstance().add(AuthCardActivity.this);
                                        EventBus.getDefault().postSticky(msgStatus);
                                    }
                                }
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onComplete() {

                            }
                        });
                    }
                }.start();

                EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_CARD_CHANGE));
            }
        },this);
    }


    /*@Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatusMsg(MsgStatus message) {
        String message1 = message.getMessage();
        if("认证成功".equals(message1)){
            startActivity(new Intent(this, AuthIdentitySuccssdActivity.class));
        }else if("验证不通过".equals(message1)){
            startActivity(new Intent(this SubmitStatusActivity.class));
            MsgStatus msgStatus = new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_FAIL);
            msgStatus.setTips(model.memberAuthBean.checkRemark);
            EventBus.getDefault().postSticky(msgStatus);

        }
    }*/

    private void goSubmitSucceedActivity() {
        ToastUtils.showShortToast("提交银行卡信息成功");

//        startActivity(new Intent(this, SubmitStatusActivity.class));

        EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_CARD_CHANGE));
        EventBus.getDefault().postSticky(new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_CARD_SUBMIT_SUCCESS));
        finish();
    }

    /**
     * @return 是否输入了所有信息
     */
    private boolean checkData() {
        if (mEtCardNumber.getText().length() < 1) {
            ToastUtils.showShortToast("请输入银行卡号");
            return false;
        } else if (StringUtils.isEmpty(mImgUrl)) {
            ToastUtils.showShortToast("请上传银行卡照片");
            return false;
        } else if ("请选择开户银行".equals((String) mSpSelectYH.getSelectedItem())) {
            ToastUtils.showShortToast("请选择开户银行");
            return false;
        } else if (StringUtils.isEmpty(mCity)) {
            ToastUtils.showShortToast("请选择开户地址");
            return false;
        } else if (StringUtils.isEmpty(mEtYhZhihang.getText().toString())) {
            ToastUtils.showShortToast("请输入支行地址");
            return false;
        } else if (StringUtils.isEmpty(mEtPhoneNumber.getText().toString())) {
            ToastUtils.showShortToast("请输入银行卡预留手机号");
            return false;
        } else if (StringUtils.isEmpty(mEtCheckNumber.getText().toString())) {
            ToastUtils.showShortToast("请输入验证码");
            return false;
        } else {
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Config.REQUEST_CODE_CHOOSE_IMAGE_SELECT && resultCode == RESULT_OK) {
            List<Uri> uris = Matisse.obtainResult(data);
            LogUtils.e("拿到图片" + uris.get(0).getPath());
            uploadImage(uris.get(0));
        }
    }

    @OnClick(R.id.tvGetCheckNumber)
    public void onViewClicked() {
        APIManager.startRequest(
                mCaptchaService.getMemberAuthMsgByReservedPhone(mEtPhoneNumber.getText().toString()),
                new BaseRequestListener<Object>(this) {
                    @Override
                    public void onSuccess(Object result) {
                        CountDownRxUtils.textViewCountDown(mTvGetCheckNumber, TIME_COUNT, "获取验证码");
                    }
                },this
        );
    }
}

