package com.weiju.ccmall.module.xysh.activity.quick_pay;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.ReceiptOrder;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class QuickPayOrderDetailActivity extends BaseActivity {
    @BindView(R.id.tvTansferAmt)
    TextView tvTansferAmt;
    @BindView(R.id.tvReceiptAmt)
    TextView tvReceiptAmt;
    @BindView(R.id.tvServiceCharge)
    TextView tvServiceCharge;
    @BindView(R.id.tvReceiptCard)
    TextView tvReceiptCard;
    @BindView(R.id.tvPayCard)
    TextView tvPayCard;
    @BindView(R.id.tvChannel)
    TextView tvChannel;
    @BindView(R.id.tvOrderNo)
    TextView tvOrderNo;
    @BindView(R.id.tvTransferTime)
    TextView tvTransferTime;
    @BindView(R.id.tvState)
    TextView tvState;

    private QueryUserBankCardResult.BankInfListBean payCard;
    private QueryUserBankCardResult.BankInfListBean receiptCard;
    private ReceiptOrder order;
    private ChannelItem channel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_pay_order_detail);
        ButterKnife.bind(this);
        setLeftBlack();
        setTitle("订单详情");
        payCard = (QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("pay");
        receiptCard = (QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("receipt");
        order = (ReceiptOrder) getIntent().getSerializableExtra("order");
        channel = (ChannelItem) getIntent().getSerializableExtra("channel");
        initView();
    }

    private void initView() {
        tvTansferAmt.setText(String.format("¥%.2f", order.txnAmt));
        tvReceiptAmt.setText(String.format("¥%.2f", order.repayAmt));
        tvServiceCharge.setText(String.format("¥%.2f", order.feeAmt));
        tvReceiptCard.setText(String.format("%s(%s)", receiptCard.bankName, BankUtils.cutBankCardNo(receiptCard.cardNo)));
        tvPayCard.setText(String.format("%s(%s)", payCard.bankName, BankUtils.cutBankCardNo(payCard.cardNo)));
        tvChannel.setText(channel.channelName);
        tvOrderNo.setText(order.orderId);
        tvTransferTime.setText(order.executeDate);
        tvState.setText(order.getOrderStatus());
    }

    public static void start(Context context,
                             QueryUserBankCardResult.BankInfListBean pay,
                             QueryUserBankCardResult.BankInfListBean receipt,
                             ReceiptOrder order,
                             ChannelItem channel) {
        Intent intent = new Intent(context, QuickPayOrderDetailActivity.class);
        intent.putExtra("pay", pay);
        intent.putExtra("receipt", receipt);
        intent.putExtra("order", order);
        intent.putExtra("channel", channel);
        context.startActivity(intent);
    }
}
