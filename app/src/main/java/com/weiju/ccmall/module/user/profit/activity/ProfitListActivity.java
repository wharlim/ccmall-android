package com.weiju.ccmall.module.user.profit.activity;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderListFragment;
import com.weiju.ccmall.module.user.profit.fragment.ProfitListFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Balance;
import com.weiju.ccmall.shared.bean.CommonExtra;
import com.weiju.ccmall.shared.bean.DayProfit;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBalanceService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/6/21 on 16:31
 * @desc 收入列表
 */
public class ProfitListActivity extends BaseActivity {
    @BindView(R.id.layoutDayProfit)
    protected RelativeLayout mLayoutDayProfit;
    @BindView(R.id.tvDayProfit)
    protected TextView mTvDayProfit;
    @BindView(R.id.tvGetDayProfit)
    protected TextView mTvGetDayProfit;
    @BindView(R.id.tvGetDayProfiting)
    protected TextView mTvGetDayProfiting;
    @BindView(R.id.tvGetDayProfitSuccess)
    protected TextView mTvGetDayProfitSuccess;
    @BindView(R.id.tvDayProfiting)
    protected TextView mTvDayProfiting;
    @BindView(R.id.totalProfitTv)
    protected TextView mTotalProfitTv;
    @BindView(R.id.leftProfitTv)
    protected TextView mLeftProfitTv;
    @BindView(R.id.magicIndicator)
    protected MagicIndicator mMagicIndicator;
    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;


    private IBalanceService mBalanceService;
    private DayProfit mDayProfit;
    protected List<BaseFragment> mFragments = new ArrayList<>();
    protected List<Page> mPages = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profit_list);
        ButterKnife.bind(this);


        initView();
        initData();

    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.setStatusBarColor(getResources().getColor(R.color.red));
            QMUIStatusBarHelper.setStatusBarDarkMode(this);
        }

        setLeftBlack();
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_white);
        setTitle("收入");
        getHeaderLayout().makeHeaderRed();

        initPage();
        initViewPager();
        initIndicator();
    }

    private void initPage() {
        mPages.add(new Page("shangcheng", "商城"));
        mPages.add(new Page("xysh", "信用生活"));
        mPages.add(new Page("jkp", "集靠谱"));
        for (Page page : mPages) {
            ProfitListFragment profitListFragment = new ProfitListFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("page", page);
            profitListFragment.setArguments(bundle);
            mFragments.add(profitListFragment);
        }
    }

    private void initViewPager() {
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mPages.get(position).name;
            }
        });
        mViewPager.setOffscreenPageLimit(mFragments.size());
    }


    private void initIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mPages.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(mPages.get(index).name);
                titleView.setNormalColor(getResources().getColor(R.color.text_black));
                titleView.setSelectedColor(getResources().getColor(R.color.red));
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(16);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getResources().getColor(R.color.red));
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                indicator.setMode(LinePagerIndicator.MODE_WRAP_CONTENT);
                return indicator;
            }
        });
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    private void initData() {
        mBalanceService = ServiceManager.getInstance().createService(IBalanceService.class);
        reloadDayProfitStatus();
        reloadCommonExtra();
    }

    /**
     * 获取头部收入合计信息
     */
    private void reloadCommonExtra() {
        APIManager.startRequest(mBalanceService.getProfitListByPlatForm(1, 1, 1), new BaseRequestListener<PaginationEntity<Balance, CommonExtra>>(this) {
            @Override
            public void onSuccess(PaginationEntity<Balance, CommonExtra> result) {
                setCommonExtra(result.ex);
            }
        },this);
    }

    /**
     * 获取每日分红信息
     */
    private void reloadDayProfitStatus() {
        APIManager.startRequest(mBalanceService.getDayProfitStatus(), new BaseRequestListener<DayProfit>() {
            @Override
            public void onSuccess(DayProfit dayProfit) {
                mDayProfit = dayProfit;
                setDayProfit(dayProfit);
            }
        },this);

    }

    private void setCommonExtra(CommonExtra commonExtra) {
        mTotalProfitTv.setText(String.valueOf(ConvertUtil.cent2yuanNoZero(commonExtra.profitSumMoney)));
        mLeftProfitTv.setVisibility(View.VISIBLE);
        mLeftProfitTv.setText(String.format("未到账收入：%s", ConvertUtil.cent2yuan(commonExtra.freezeSumMoney)));
    }

    public void setDayProfit(DayProfit dayProfit) {
        if (dayProfit != null) {
            mLayoutDayProfit.setVisibility(View.VISIBLE);
            mTvGetDayProfitSuccess.setVisibility(dayProfit.status == 1 ? View.VISIBLE : View.GONE);
            mTvGetDayProfit.setVisibility(dayProfit.status != 1 && dayProfit.status != 5 ? View.VISIBLE : View.GONE);
            mTvGetDayProfiting.setVisibility(dayProfit.status == 5 ? View.VISIBLE : View.GONE);
            mTvDayProfit.setText(MoneyUtil.centToYuanStrNoZero(dayProfit.money));
            mTvDayProfiting.setVisibility(dayProfit.status == 5 ? View.VISIBLE : View.INVISIBLE);
            mTvDayProfit.setVisibility(dayProfit.status != 5 ? View.VISIBLE : View.GONE);
            if (dayProfit.money == 0) {
                mTvGetDayProfitSuccess.setText("CCM达到200方可分红");
                mTvGetDayProfitSuccess.setVisibility(View.VISIBLE);
                mTvGetDayProfit.setVisibility(View.GONE);
                mTvGetDayProfiting.setVisibility(View.GONE);
                mTvGetDayProfitSuccess.setTextColor(Color.parseColor("#666666"));
            }

        }
    }


    /**
     * 领取每日分红
     */
    @OnClick(R.id.tvGetDayProfit)
    protected void getDayProfit() {
        ToastUtil.showLoading(ProfitListActivity.this);
        APIManager.startRequest(mBalanceService.getDayProfit(), new BaseRequestListener<DayProfit>() {
            @Override
            public void onSuccess(DayProfit dayProfit) {
                ToastUtil.hideLoading();
                if (mDayProfit != null) {
                    mDayProfit.status = dayProfit.status;
                    setDayProfit(mDayProfit);
                }
                ToastUtil.success(dayProfit.msg);
            }

            @Override
            public void onError(Throwable e) {
                super.onE(e);
                ToastUtil.hideLoading();
            }
        },this);
    }

}
