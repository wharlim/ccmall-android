package com.weiju.ccmall.module.user;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.common.base.Strings;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.basic.BaseSubscriber;
import com.weiju.ccmall.shared.bean.CheckNumber;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.UserToken;
import com.weiju.ccmall.shared.bean.WeChatLoginModel;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.CaptchaBtn;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.ICaptchaService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.CountDownRxUtils;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;

import java.net.URL;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * 微信登录。未注册过的用户将跳到此页面，注册信息
 */
public class WxRegisterActivity extends BaseActivity {

    @BindView(R.id.ivAvatar)
    SimpleDraweeView mIvAvatar;
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.newPhoneEt)
    EditText mNewPhoneEt;
    @BindView(R.id.captchaEt)
    EditText mCaptchaEt;
    @BindView(R.id.captchaBtn)
    CaptchaBtn mCaptchaBtn;
    @BindView(R.id.etInvitationCode)
    EditText mEtInvitationCode;
    private WeChatLoginModel mModel;

    private IUserService mUserService;
    private ICaptchaService mCaptchaService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wx_register);
        ButterKnife.bind(this);

        getIntentData();
        initData();
    }

    private void initData() {
        setTitle("补全信息");
        setLeftBlack();

        FrescoUtil.setImageSmall(mIvAvatar, mModel.headImage);
        mTvName.setText(mModel.nickName);

        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        mCaptchaService = ServiceManager.getInstance().createService(ICaptchaService.class);
    }

    private void getIntentData() {
        mModel = (WeChatLoginModel) getIntent().getSerializableExtra("data");
    }

    @OnClick(R.id.tvSubmit)
    public void onViewClicked() {
        String code = mEtInvitationCode.getText().toString();
//        if (com.blankj.utilcode.utils.StringUtils.isEmpty(code)) {
//            ToastUtil.error("请输入您的邀请码");
//            mEtInvitationCode.requestFocus();
//            return;
//        }
        if (!UiUtils.checkETPhone(mNewPhoneEt)) {
            return;
        }
        String captcha = mCaptchaEt.getText().toString();
        if (Strings.isNullOrEmpty(captcha)) {
            ToastUtil.error("请输入验证码");
            mCaptchaEt.requestFocus();
            return;
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", mNewPhoneEt.getText().toString());
        params.put("checkNumber", captcha);
        params.put("nickName", mModel.nickName);
        params.put("headImage", mModel.headImage);
        params.put("wechatOpenId", mModel.openid);
        params.put("wechatUnionId", mModel.unionid);
        params.put("inviterPhone", code);
//        params.put("password", "");
        Observable<RequestResult<User>> registerAndLoginObservable = mUserService.xcxAdd(params)
                .flatMap(new Function<RequestResult<CheckNumber>, Observable<RequestResult<UserToken>>>() {
                    @Override
                    public Observable<RequestResult<UserToken>> apply(final RequestResult<CheckNumber> result) throws Exception {
                        if (result.code != 0) {
//                            RequestResult<UserToken> userRequestResult = new RequestResult<>();
//                            userRequestResult.code = result.code;
//                            userRequestResult.message = result.message;
//                            userRequestResult.data = new UserToken();
//                            return Observable.just(userRequestResult);
                            throw new RuntimeException(result.message);
                        }
                        return mUserService.login(mNewPhoneEt.getText().toString(), result.data.checkNumber,1,1);
                    }
                })
                .flatMap(new LoginActivity.FunctionGetUserInfo());
        BaseSubscriber baseSubscriber = new BaseSubscriber<User>() {
            @Override
            public void onNext(User user) {
                super.onNext(user);
                UserService.login(user);
                EventBus.getDefault().post(new EventMessage(Event.loginSuccess));
                finish();
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.error(e.getMessage());
            }
        };
        baseSubscriber.setProgressDialog(getProgressDialog());
        execute(registerAndLoginObservable, baseSubscriber);
    }

    @OnClick(R.id.captchaBtn)
    public void onMTvGetMsgClicked() {
        if (UiUtils.checkETPhone(mNewPhoneEt)) {
            String phone = mNewPhoneEt.getText().toString();
            String token = StringUtil.md5(BuildConfig.TOKEN_SALT + phone);
            APIManager.startRequest(mCaptchaService.getCaptchaForRegister(token, phone), new BaseRequestListener<Object>(this) {

                @Override
                public void onSuccess(Object result) {
                    CountDownRxUtils.textViewCountDown(mCaptchaBtn, 60, "获取验证码");
                }
            },this);
        }
    }
}
