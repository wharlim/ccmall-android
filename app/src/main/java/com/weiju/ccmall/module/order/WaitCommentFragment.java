package com.weiju.ccmall.module.order;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.adapter.OrderWaitCommentListAdapter;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.OrderComment;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.decoration.SpacesItemDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.order
 * @since 2017-07-06
 */
public class WaitCommentFragment extends BaseFragment implements PageManager.RequestListener {

    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.noDataLayout)
    NoData mNoDataLayout;
    private IOrderService mOrderService;
    private OrderWaitCommentListAdapter mOrderWaitCommentListAdapter;
    private PageManager mPageManager;
    private boolean isLoaded;

    private String classifacationType = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.base_list_layout, container, false);
        ButterKnife.bind(this, view);
        mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
        mOrderWaitCommentListAdapter = new OrderWaitCommentListAdapter(getContext());
        mRecyclerView.setAdapter(mOrderWaitCommentListAdapter);
        try {
            mPageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(mRecyclerView)
                    .setItemDecoration(new SpacesItemDecoration(ConvertUtil.dip2px(15), true))
                    .setRequestListener(this)
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .build(getContext());
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }
        EventBus.getDefault().register(this);
        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isAdded() && !isLoaded) {
            mPageManager.onRefresh();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getUserVisibleHint() && !isLoaded) {
            mPageManager.onRefresh();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
        if (message.getEvent().equals(Event.commentFinish)) {
            if (mOrderWaitCommentListAdapter == null || mOrderWaitCommentListAdapter.getItems().isEmpty()) {
                return;
            }
            nextPage(1);
        }
    }

    @Override
    public void nextPage(final int page) {
        if (Const.NEWRETAIL_MODE.equals(classifacationType)) {
            APIManager.startRequest(mOrderService.getOrderWaitCommentList(page, "onnOrder"), new BaseRequestListener<PaginationEntity<OrderComment, Object>>(mRefreshLayout) {
                @Override
                public void onSuccess(PaginationEntity<OrderComment, Object> result) {
                    if (page == 1) {
                        mOrderWaitCommentListAdapter.removeAllItems();
                    }
                    mOrderWaitCommentListAdapter.addItems(result.list);
                    mPageManager.setTotalPage(result.totalPage);
                    mPageManager.setLoading(false);

                    mNoDataLayout.setVisibility(result.total > 0 ? View.GONE : View.VISIBLE);
                }

                @Override
                public void onError(Throwable e) {
                    super.onError(e);
                    mRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    mRefreshLayout.setRefreshing(false);
                    isLoaded = true;
                }
            }, getContext());
        } else {
            APIManager.startRequest(mOrderService.getOrderWaitCommentList(page), new BaseRequestListener<PaginationEntity<OrderComment, Object>>(mRefreshLayout) {
                @Override
                public void onSuccess(PaginationEntity<OrderComment, Object> result) {
                    if (page == 1) {
                        mOrderWaitCommentListAdapter.removeAllItems();
                    }
                    mOrderWaitCommentListAdapter.addItems(result.list);
                    mPageManager.setTotalPage(result.totalPage);
                    mPageManager.setLoading(false);

                    mNoDataLayout.setVisibility(result.total > 0 ? View.GONE : View.VISIBLE);
                }

                @Override
                public void onError(Throwable e) {
                    super.onError(e);
                    mRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    mRefreshLayout.setRefreshing(false);
                    isLoaded = true;
                }
            }, getContext());
        }
    }
}
