package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

public class TodayPayStatus {
    // todayStatus  0:成功，1:失败，2:执行中/未执行(2不用显示)
    @SerializedName("todayStatus")
    public int todayStatus;
    @SerializedName("todayAmount")
    public float todayAmount;
}
