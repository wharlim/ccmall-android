package com.weiju.ccmall.module.blockchain.transferccm;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.shop.views.Utils;

public class TransferCCMSuccessDialogFragment extends DialogFragment {

    public TransferCCMSuccessDialogFragment() {}

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismiss();
            }
        },2000);
        return inflater.inflate(R.layout.dialog_transferccm_success, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Window w = getDialog().getWindow();
        if (w != null) {
            w.setLayout(Utils.dpToPx(309), Utils.dpToPx(250));
        }
    }

    public static TransferCCMSuccessDialogFragment newInstance() {
        return new TransferCCMSuccessDialogFragment();
    }
}
