package com.weiju.ccmall.module;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.NearStore.NearStoreListFragment;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.cart.CartFragment;
import com.weiju.ccmall.module.category.CategoryFragment;
import com.weiju.ccmall.module.home.HomeFragment;
import com.weiju.ccmall.module.user.UserCenterFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.MainAdModel;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.MainAdView;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IAdService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zjm
 */
public class NewMainActivity extends BaseActivity {

    @BindView(R.id.viewPager)
    ViewPager mViewPager;

    private ArrayList<BaseFragment> mFragments = new ArrayList<>();
    private FragmentPagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_maxin);
        ButterKnife.bind(this);
        initView();
        initAdDialog();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addCategory(Intent.CATEGORY_HOME);
        startActivity(intent);
    }

    private void initAdDialog() {
        final WJDialog wjDialog = new WJDialog(this);
        final MainAdView mainAdView = new MainAdView(this);
        IAdService service = ServiceManager.getInstance().createService(IAdService.class);
        APIManager.startRequest(service.getMainAd(), new BaseRequestListener<MainAdModel>(this) {

            @Override
            public void onSuccess(MainAdModel result) {
                if (StringUtils.isEmpty(result.backUrl)) {
                    return;
                }
                wjDialog.show();
                mainAdView.setCloseClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        wjDialog.dismiss();
                    }
                });
                mainAdView.setData(result);
                wjDialog.setContentView(mainAdView);
            }
        },this);
    }

    private void initView() {
        mFragments.add(new HomeFragment());
        mFragments.add(new CategoryFragment());
        mFragments.add(NearStoreListFragment.newInstance(true));
        mFragments.add(CartFragment.newInstance(AppTypes.CART.FROM_HOME));
        mFragments.add(new UserCenterFragment());

        mPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }
        };
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOffscreenPageLimit(mFragments.size());
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case viewHome:
                mViewPager.setCurrentItem(0);
                LogUtils.e("选中了首页");
                break;
            case viewCategory:
                mViewPager.setCurrentItem(1);
                EventBus.getDefault().postSticky(new EventMessage(Event.changeCategory, message.getData()));
                break;
            case viewCart:
                if (SessionUtil.getInstance().isLogin()) {
                    mViewPager.setCurrentItem(2);
                } else {
                    ToastUtil.error(Config.NET_MESSAGE.NO_LOGIN);
                    EventBus.getDefault().post(new EventMessage(Event.goToLogin));
                }
                break;
            case cartAmountUpdate:
                int total = (int) message.getData();
                break;
            case logout:
                break;
            default:
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatusMsg(MsgStatus message) {
        switch (message.getAction()) {
            case MsgStatus.ACTION_EDIT_PHONE:
                break;
            default:
        }
    }

//    @Subscribe(threadMode = ThreadMode.MAIN)
//    public void getMessage(MsgMain msgMain) {
//        switch (msgMain.getAction()) {
//            case MsgMain.SELECT_HOME:
//                startActivity(new Intent(this, NewMainActivity.class));
//                mViewPager.setCurrentItem(0);
//                break;
//        }
//    }

}
