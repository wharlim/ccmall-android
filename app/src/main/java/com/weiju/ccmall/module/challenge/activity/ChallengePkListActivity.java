package com.weiju.ccmall.module.challenge.activity;

import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.challenge.adapter.ChallengPKAdapter;
import com.weiju.ccmall.module.challenge.manager.ChallengeManager;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Challenge;
import com.weiju.ccmall.shared.bean.ScoreModel;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IChallengeService;
import com.weiju.ccmall.shared.service.contract.IPointService;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/6/27 on 11:43
 * @desc ${TODD}
 */
public class ChallengePkListActivity extends BaseListActivity {
    @BindView(R.id.tvScore)
    protected TextView mTvScore;

    private ChallengPKAdapter mAdapter = new ChallengPKAdapter();
    private IChallengeService mService = ServiceManager.getInstance().createService(IChallengeService.class);
    private String mActivityId;
    private int mPkScore;
    IPointService mPointService = ServiceManager.getInstance().createService(IPointService.class);
    private ScoreModel mScoreModel;

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mActivityId = getIntent().getStringExtra("activityId");
        mPkScore = getIntent().getIntExtra("pkScore", 10);
    }

    @Override
    public String getTitleStr() {
        return "巅峰对决";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        Challenge item = mAdapter.getItem(position);
        if (null != item) {
            if (mScoreModel.totalScore < item.pkScore) {
                ChallengeManager.showNoScoreDialog(ChallengePkListActivity.this);
            } else {
                WJDialog wjDialog = new WJDialog(ChallengePkListActivity.this);
                wjDialog.show();
                wjDialog.setContentText("您准备好了吗");
                wjDialog.setCancelText("再想想");
                wjDialog.setConfirmText("我能行");
                wjDialog.setConfirmTextColor(R.color.red);
                wjDialog.setOnCancelListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        wjDialog.dismiss();
                    }
                });
                wjDialog.setOnConfirmListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        wjDialog.dismiss();
                        ChallengeManager.joinPkChallenge(ChallengePkListActivity.this, item.challengeId, item.memberId);
                    }
                });
            }
        }

    }

    @Override
    public boolean isLoadMore() {
        return false;
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mPointService.getScore(), new BaseRequestListener<ScoreModel>() {
            @Override
            public void onSuccess(ScoreModel result) {
                super.onSuccess(result);
                mScoreModel = result;
                mTvScore.setText(String.valueOf(result.totalScore));
            }
        },this);

        APIManager.startRequest(mService.getPkChallengeList(mActivityId, mPkScore, mPkScore),
                new BaseRequestListener<List<Challenge>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(List<Challenge> result) {
                        mAdapter.setNewData(result);
                    }
                },this);
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_challenge_pk_list;
    }

    @OnClick(R.id.tvAddPkChallenge)
    public void addPkChallenge() {
        ChallengeManager.addPkChallenge(ChallengePkListActivity.this, mActivityId, mPkScore);
    }
}
