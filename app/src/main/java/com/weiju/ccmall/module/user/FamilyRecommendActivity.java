package com.weiju.ccmall.module.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.adapter.FamilyRecommendAdapter;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Family;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FamilyRecommendActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.rvList)
    RecyclerView mRvList;
    @BindView(R.id.layoutRefresh)
    SwipeRefreshLayout mLayoutRefresh;
    private IUserService mService;

    List<Family.DatasEntity> mDatas = new ArrayList<>();
    FamilyRecommendAdapter mAdapter = new FamilyRecommendAdapter(mDatas);

    int curPage = 1;
    static final int PAGE_SIZE = 10;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);
        ButterKnife.bind(this);
        mService = ServiceManager.getInstance().createService(IUserService.class);
        initView();
    }

    private void initView() {
        setTitle("待开发新客户");
        setLeftBlack();
//        getHeaderLayout().setRightDrawable(R.drawable.ic_screen);


        mRvList.setAdapter(mAdapter);
        mRvList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter.setEmptyView(new NoData(this));
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                loadData(curPage+1);
            }
        });
        View label = LayoutInflater.from(this).inflate(R.layout.item_family_top_label, null);
        mAdapter.addHeaderView(label);
        mLayoutRefresh.setOnRefreshListener(this);
        setLayoutRefresh(mLayoutRefresh);
        mLayoutRefresh.setRefreshing(true);
        onRefresh();
    }

    @Override
    public void onRefresh() {
        loadData(1);
    }

    private void loadData(int page) {
        BaseRequestListener<Family> l = new BaseRequestListener<Family>() {
            @Override
            public void onSuccess(Family result) {
                super.onSuccess(result);
                if (result.datas == null) {
                    result.datas = new ArrayList<>();
                }
                if (page == 1) {
                    mDatas.clear();
                }
                // 加载数据
                mDatas.addAll(result.datas);
                mAdapter.notifyDataSetChanged();
                curPage = page;
                mLayoutRefresh.setRefreshing(false);
                if (result.datas.size() < PAGE_SIZE) {
                    mAdapter.loadMoreEnd();
                } else {
                    mAdapter.loadMoreComplete();
                }
            }
        };
        APIManager.startRequest(mService.getAwardClientList(page, PAGE_SIZE), l, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ChangeCustomerNoteNameActivity.REQUEST_CODE && data != null) {
                String memberId = data.getStringExtra("memberId");
                String noteName = data.getStringExtra("noteName");
                for(int i = 0; i < mDatas.size(); ++i) {
                    if (Objects.equals(mDatas.get(i).memberId, memberId)) {
                        mDatas.get(i).noteName = noteName;
//                        mAdapter.notifyItemChanged(i);
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, FamilyRecommendActivity.class);
        context.startActivity(intent);
    }
}
