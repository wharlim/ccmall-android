package com.weiju.ccmall.module.shop.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import static com.weiju.ccmall.module.shop.views.Utils.dpToPx;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.shop.bean.ShopItem;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

public class ShopTitleView extends LinearLayout {
    private LinearLayout mShopData;

    private ShopItem shop;
    private float mCollectX;

    private IProductService iProductService;
    private SimpleDraweeView mShopIcon;
    private TextView mShopTitle;
    private TextView mGoodCountTv;
    private TextView mFollowCountTv;
    private ImageView mCollect;

    public ShopTitleView(Context context) {
        super(context);
    }

    public ShopTitleView(Context context,  AttributeSet attrs) {
        super(context, attrs);
    }

    public ShopTitleView(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mShopTitle = findViewById(R.id.shop_title);
        mShopData = findViewById(R.id.shop_data);
        mShopIcon = findViewById(R.id.icon_shop);
        mCollect = findViewById(R.id.collect);

        iProductService = ServiceManager.getInstance().createService(IProductService.class);
        mShopIcon = findViewById(R.id.icon_shop);
        mGoodCountTv = findViewById(R.id.good_count);
        mFollowCountTv = findViewById(R.id.follow_count);
    }

    public void setShopItem(ShopItem result) {
        shop = result;
        FrescoUtil.setImageSmall(mShopIcon, result.thumbUrl);
        mShopTitle.setText(result.storeName);
        mGoodCountTv.setText(String.valueOf(result.productNum));
        mFollowCountTv.setText(String.valueOf(result.concernNum));
        mCollect.setOnClickListener(v -> {
            if (shop.collectStatus > 0) {
                delCollectStore();
            } else {
                collectStore();
            }
        });
        updateCollectBtnUi();
    }

    private void updateCollectBtnUi() {
        if (shop.collectStatus > 0) {
            mCollect.setImageResource(R.mipmap.collected);
        } else {
            mCollect.setImageResource(R.mipmap.collect);
        }
    }

    private void delCollectStore() {
        APIManager.startRequest(iProductService.delCollectStore(shop.storeId), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                ToastUtil.success("取消收藏成功!");
                shop.collectStatus = 0;
                shop.concernNum--;
                mFollowCountTv.setText(String.valueOf(shop.concernNum));
                updateCollectBtnUi();
            }
        }, getContext());
    }

    private void collectStore() {
        APIManager.startRequest(iProductService.collectStore(shop.storeId), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                ToastUtil.success("收藏店铺成功!");
                shop.collectStatus = 1;
                shop.concernNum++;
                mFollowCountTv.setText(String.valueOf(shop.concernNum));
                updateCollectBtnUi();
            }
        }, getContext());
    }

    public void setPercent(int percent) {
        if (mCollectX == 0f) {
            mCollectX = mCollect.getX();
        }
        float p = percent / 100.0f;
        Log.d("Seven", "percent: " + percent);
        // title位置变化
        mShopData.setAlpha(1-p);
        int max = dpToPx(getContext(), 18 - 7);
        mShopTitle.setY(max * p + dpToPx(getContext(), 7));

        float scale = 0.3f * (1-p) + 0.7f;
        mShopIcon.setScaleX(scale);
        mShopIcon.setScaleY(scale);

        float delta = p * dpToPx(getContext(), 30);
        mCollect.setX(mCollectX - delta);
    }
}
