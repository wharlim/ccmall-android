package com.weiju.ccmall.module.blockchain.beans;

import com.google.gson.annotations.SerializedName;

public class TemplateItem {


    /**
     * id : 4
     * templateId : a8241f1d354f4b08aab91a82ebe797f3
     * templateName : 全网图表
     * remark : 啊啊
     * fileUrl : https://static.create-chain.net/ccmall/c0/34/66/55661153660d43f39500075241e86484.zip
     * status : 1
     * createDate : 2019-11-14 18:22:22
     * updateDate : 2019-11-14 18:39:01
     * deleteFlag : 0
     */

    @SerializedName("id")
    public int id;
    @SerializedName("templateId")
    public String templateId;
    @SerializedName("templateName")
    public String templateName;
    @SerializedName("remark")
    public String remark;
    @SerializedName("fileUrl")
    public String fileUrl;
    @SerializedName("status")
    public int status;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
}
