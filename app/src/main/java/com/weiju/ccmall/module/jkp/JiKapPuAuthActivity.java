package com.weiju.ccmall.module.jkp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;

public class JiKapPuAuthActivity extends BaseActivity {

    public static void start(Context context ) {
        Intent intent = new Intent(context, JiKapPuAuthActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);
        getHeaderLayout().hide();


        getSupportFragmentManager().beginTransaction()
                .add(R.id.baseContentLayout, new JiKaoPuWebFragment())
                .commit();
    }
}
