package com.weiju.ccmall.module.live.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/5/14.
 */
public class SettleAccountEntity {
    /**
     * "payDate":"2020-04-21 20:25:47"
     */

    @SerializedName("orderCode")
    public String orderCode;    //订单号
    @SerializedName("profitMoney")
    public long profitMoney;    //待结算金额
    @SerializedName("payDate")
    public String payDate;      //支付日期

    /**
     * dealId : 0211589653347462
     * date : 2020-05-14 11:12:50
     * profitMoney : -10000
     * totalMoney : 2990000
     * type : 2
     * dealStatus : 1
     * dealStatusStr : 等待审核
     */

    @SerializedName("totalMoney")
    public long totalMoney;     //用户余额
    @SerializedName("dealId")
    public String dealId;
    @SerializedName("date")
    public String date;
    @SerializedName("type")
    public int type;        //记录类型（1：销售订单；2：提现记录）
    @SerializedName("dealStatus")
    public int dealStatus;
    @SerializedName("dealStatusStr")
    public String dealStatusStr;

    public String month;
}
