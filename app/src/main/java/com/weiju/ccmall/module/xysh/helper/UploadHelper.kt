package com.weiju.ccmall.module.xysh.helper

import com.weiju.ccmall.shared.util.Timber
import okhttp3.MediaType
import okhttp3.MultipartBody.Part
import okhttp3.RequestBody
import java.io.File
import java.util.*
import kotlin.collections.HashMap

/**
 * Created by 林先生_lcy.
 * User: lcy
 * Date: 2019/4/3
 * Time: 上午9:30
 */
object UploadHelper{

    val parts = HashMap<String, RequestBody>(1)
//
//    fun getPart(key: String, value: String, isPicture: Boolean){
//        if(!isPicture) {
//            val body = RequestBody.create(MediaType.parse("text/plain;charset=UTF-8"), value)
//            parts.add(Part.create(key, body))
//        } else {
//            val body = RequestBody.create(MediaType.parse("image/*"), File(value))
//            parts.add()
//        }
//
//    }

    fun getPart(key: String, path: String):Part{
        val body = RequestBody.create(MediaType.parse("image/*"), File(path))
        return Part.createFormData(key, getName(path), body)
    }

    fun putBody(key: String, value: String?, isPicture: Boolean){
        if(!isPicture)
            parts[key] = RequestBody.create(MediaType.parse("text/plain;charset=UTF-8"), value)
        else
           parts[key] = RequestBody.create(MediaType.parse("image/*"), File(value))
    }

    private fun getName(path: String): String{
        val type = path.substring(path.lastIndexOf("."))
        return "${UUID.randomUUID()}.$type"
    }

}
