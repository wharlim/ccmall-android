package com.weiju.ccmall.module.community;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.StringUtil;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * @author Stone
 */

public class CourseFragment extends BaseFragment {

    private int mType;
    private int pageOffset = 1;
    private int pageSize = 15;
    @BindView(R.id.rvList)
    RecyclerView rvList;
    @BindView(R.id.titleView)
    TitleView titleView;
    private BaseQuickAdapter mAdapter;
    private boolean mIsNeedHeader;
    private String mCategoryId;
    private String url = "course/getCourseListByCId";
    private boolean isSearch;
    private String key = "";
    private ICommunityService mPageService;
    private String plateId;

    @Override
    protected int getFragmentResId() {
        return R.layout.activity_base_list;
    }

    public static CourseFragment newInstance(int type, boolean isNeedHeader, String categoryId, boolean isSearch) {
        CourseFragment courseFragment = new CourseFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putBoolean("isNeedHeader", isNeedHeader);
        bundle.putString("categoryId", categoryId);
        bundle.putBoolean("isSearch", isSearch);
        courseFragment.setArguments(bundle);
        return courseFragment;
    }

    public static CourseFragment newInstance(int type, boolean isNeedHeader, String categoryId, boolean isSearch, String plateId) {
        CourseFragment courseFragment = new CourseFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("type", type);
        bundle.putString("plateId", plateId);
        bundle.putBoolean("isNeedHeader", isNeedHeader);
        bundle.putString("categoryId", categoryId);
        bundle.putBoolean("isSearch", isSearch);
        courseFragment.setArguments(bundle);
        return courseFragment;
    }

    @Override
    protected void initViewConfig() {
        super.initViewConfig();
        titleView.setVisibility(View.GONE);
        mType = getArguments().getInt("type", 0);
        plateId = getArguments().getString("plateId");
        mIsNeedHeader = getArguments().getBoolean("isNeedHeader", true);
        mCategoryId = getArguments().getString("categoryId");
        isSearch = getArguments().getBoolean("isSearch", false);
        if (isSearch) {
            url = "course/searchCourseList";
        }
        mAdapter = createAdapter();
        mAdapter.setEmptyView(new NoData(getContext()));
        rvList.setLayoutManager(new LinearLayoutManager(mActivity));
        rvList.setAdapter(mAdapter);
    }

    private BaseQuickAdapter createAdapter() {
        if (mType == 1) {
            mAdapter = new CourseSectionAdapter(mType, R.layout.view_item_course);
        } else if (mType == 2) {
//            mAdapter = new CourseSectionAdapter(mType, R.layout.item_course_layout, mIsNeedHeader);
            mAdapter = new CourseSectionAdapter(mType, R.layout.view_item_course);
        } else if (mType ==  3) {
            mAdapter = new CourseSectionAdapter(mType, R.layout.item_course_video_layout);
        } else {
            mAdapter = new CourseSectionAdapter(mType, R.layout.view_item_course);
        }
        return mAdapter;
    }

    @Override
    protected void initListener() {
        super.initListener();
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                pageOffset++;
                requestData();
            }
        }, rvList);
    }

    @Override
    protected void initDataNew() {
        super.initDataNew();
        mPageService = ServiceManager.getInstance().createService(ICommunityService.class);
        addHeadView();
        if (!isSearch) {
            requestData();
        }
    }

    private void addHeadView() {
        if (pageOffset == 1 && mIsNeedHeader && !isSearch) {
            LinearLayout headerLayout = mAdapter.getHeaderLayout();
            if (headerLayout != null) {
                headerLayout.removeAllViews();
            }
            View headView = LayoutInflater.from(mActivity).inflate(R.layout.layout_head_search_course, null);
            headView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSearchClick();
                }
            });
            mAdapter.addHeaderView(headView);
        }
    }

    private void onSearchClick() {
        Intent intent = new Intent(mActivity, CourseAllActivity.class);
        intent.putExtra(Constants.KEY_IS_EDIT, true);
        intent.putExtra(Constants.KEY_EXTROS, mType);
        startActivity(intent);
    }

    private void requestData() {
        if (mIsNeedHeader) {
            APIManager.startRequest(mPageService.getCourseList(mType, pageOffset, pageSize, plateId), new BaseRequestListener<PaginationEntity<CourseModule, Object>>() {
                @Override
                public void onSuccess(PaginationEntity<CourseModule, Object> result) {
                    super.onSuccess(result);
                    dealListData(result);
                }

                @Override
                public void onStart() {
                }

                @Override
                public void onError(Throwable e) {
                }

                @Override
                public void onComplete() {
                }
            },getContext());
        } else {
            APIManager.startRequest(mPageService.getCourseListByCategoryId(url, key, mCategoryId, mType, pageOffset, pageSize), new BaseRequestListener<PaginationEntity<Course, Object>>() {
                @Override
                public void onSuccess(PaginationEntity<Course, Object> result) {
                    super.onSuccess(result);
                    dealAllSortList(result);
                }
            },getContext());
        }
    }

    private void dealAllSortList(PaginationEntity<Course, Object> result) {
        ArrayList<CourseSection> sections = new ArrayList<>();
        if (mType == 3) {
            sections.add(new CourseSection(result.list));
        } else {
            for (Course course : result.list) {
                sections.add(new CourseSection(course));
            }
        }
        adapterSetData(result.totalPage, sections);

    }

    private void dealListData(PaginationEntity<CourseModule, Object> result) {
        ArrayList<CourseSection> sections = new ArrayList<>();
        if (StringUtil.isNullOrEmpty(result.list)) {
            return;
        }
        for (CourseModule courseModule : result.list) {
            if (mIsNeedHeader && !StringUtil.isNullOrEmpty(courseModule.getCourseList())) {
                boolean isShowMore = courseModule.getCourseCount() > 4;
                CourseSection courseSection = new CourseSection(true, courseModule.getCategory().getTitle(), courseModule.getCategory(), isShowMore);
                courseSection.hasDetailAuthorization = courseModule.isHasDetailAuthorization();
                sections.add(courseSection);
            }
            if (mType != 3) {
                for (Course courseListBean : courseModule.getCourseList()) {
                    CourseSection courseSection1 = new CourseSection(courseListBean);
                    courseSection1.hasDetailAuthorization = courseModule.isHasDetailAuthorization();
                    sections.add(courseSection1);
                }
            } else if (!StringUtil.isNullOrEmpty(courseModule.getCourseList())) {
                CourseSection courseSection1 = new CourseSection(courseModule.getCourseList());
                courseSection1.hasDetailAuthorization = courseModule.isHasDetailAuthorization();
                sections.add(courseSection1);
            }
        }
        adapterSetData(result.totalPage, sections);

    }

    private void adapterSetData(int totalPage, ArrayList<CourseSection> sections) {
        if (pageOffset == 1) {
            mAdapter.setNewData(sections);
        } else {
            mAdapter.addData(sections);
        }
        if (pageOffset < totalPage) {
            mAdapter.loadMoreComplete();
        } else {
            mAdapter.loadMoreEnd();
        }
    }

    public void setValueAndRequest(String value) {
        key = value;
        requestData();
    }
}

