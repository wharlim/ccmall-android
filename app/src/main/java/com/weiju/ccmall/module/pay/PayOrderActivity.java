package com.weiju.ccmall.module.pay;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.ToastUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.OrderProduct;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.PayTypeLayout;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.AliPayUtils;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.PayUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.WePayUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.pay
 * @since 2017-06-09
 */
public class PayOrderActivity extends BaseActivity {

    public static final int GOODSTYPE_ANCHOR = 1; // 开通主播所需支付的费用
    public static final int GOODSTYPE_LIVE_STORE = 2; // 开通直播店铺
    public static final int GOODSTYPE_CREDIT_LIFE_UPGRADE = 3; // 升级信用生活会员等级

    @BindView(R.id.priceTv)
    TextView mPriceTv;
    @BindView(R.id.layoutPayType)
    PayTypeLayout mLayoutPayType;
    @BindView(R.id.confirmBtn)
    TextView mConfirmBtn;
    private IOrderService mOrderService;
    private String mOrderCode;
    private Order mOrder;
    private int mPayType = -1;
    private int mSelectType;
    private int goodsType = -1; // -1是无效的 1 表示开通主播的费用

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_order);
        ButterKnife.bind(this);

        initView();
        getIntentData();
        initData();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    private void initView() {
        showHeader();
        setTitle("选择支付方式");
        setLeftBlack();
    }


    private void initData() {
        WePayUtils.initWePay(this);
        mLayoutPayType.setSelectPayType(mSelectType);
        mLayoutPayType.setGoodsType(goodsType);

        mOrderService = ServiceManager.getInstance().createService(IOrderService.class);

        APIManager.startRequest(mOrderService.getOrderByCode(mOrderCode,""), new BaseRequestListener<Order>(this) {
            @Override
            public void onSuccess(Order order) {
                mOrder = order;
                OrderProduct product = order.products.get(0);
                if (product != null){
                    mLayoutPayType.setProductType(product.productType);
                }
                if (order.orderMain.orderType == 5 || order.orderMain.orderType == 6 || order.orderMain.orderType == 7) {
                    mLayoutPayType.initData((long) ((order.orderMain.totalMoney - order.orderMain.freight) * 0.05), 0, false, 1);
//                    mLayoutPayType.initData((long) ((order.orderMain.totalMoney - order.orderMain.freight) * 0.05), 0);
                    mPriceTv.setText(ConvertUtil.centToCurrency(PayOrderActivity.this, order.orderMain.totalMoney));
                    if (mSelectType != -1) {
                        payOrder();
                    }
                    getSharedPreferences(Const.PAY_199, 0).edit().putString(Const.PAY_199, Const.PAY_199).commit();
                } else {
                    mLayoutPayType.initData((long) ((order.orderMain.totalMoney - order.orderMain.freight) * 0.05), 0, false, 1);
//                    mLayoutPayType.initData((long) ((order.orderMain.totalMoney - order.orderMain.freight) * 0.05), 0);
                    mPriceTv.setText(ConvertUtil.centToCurrency(PayOrderActivity.this, order.orderMain.totalMoney));
                    if (mSelectType != -1) {
                        payOrder();
                    }
                }
            }
        }, this);
    }


    private void getIntentData() {
        Intent intent = getIntent();
        mOrderCode = intent.getStringExtra("orderCode");
        mSelectType = intent.getIntExtra("selectType", -1);
        goodsType = intent.getIntExtra("goodsType", -1);
        if (StringUtils.isEmpty(mOrderCode)) {
            ToastUtil.error("参数错误");
            finish();
            return;
        }
    }

    @OnClick(R.id.confirmBtn)
    protected void payOrder() {
        if (mOrder == null) { // 获取订单信息失败
            return ;
        }
        if (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6 || mOrder.orderMain.orderType == 7) {
            if (mOrder.orderMain.totalMoney <= 0) {
                ToastUtil.error("金额不能为0");
                return;
            }

        }
        mPayType = mLayoutPayType.getSelectType();
        switch (mPayType) {
            case AppTypes.PAY_TYPE.WECHAT:
//                WeixinPayUtils.pay(
//                        this,
//                        mOrder.orderMain.payMoney,
//                        mOrder.orderMain.orderCode
//                );
//                WePayUtils.payByWeb(this, mOrder, mLayoutPayType.getSelectType());
//                break;
            case AppTypes.PAY_TYPE.WECHAT_WEB:
            case AppTypes.PAY_TYPE.ALI_WEB:
            case AppTypes.PAY_TYPE.UN_WEB:
                WePayUtils.payByWeb(PayOrderActivity.this, mOrder, mPayType);
                break;
            case AppTypes.PAY_TYPE.BALANCE:
                // 余额支付
                if (mLayoutPayType.getBalance() < mOrder.orderMain.totalMoney) {
                    ToastUtil.error("账户余额不足");
                } else {
                    if (goodsType == GOODSTYPE_LIVE_STORE || goodsType == GOODSTYPE_CREDIT_LIFE_UPGRADE){
                        PayUtil.payBalance(PayOrderActivity.this, mOrder, mPayType, false);
                    }else {
                        PayUtil.payBalance(PayOrderActivity.this, mOrder, mPayType, goodsType != GOODSTYPE_ANCHOR);
                    }
                }
                break;
            case AppTypes.PAY_TYPE.BALANCE_GOLD:
                if (mLayoutPayType.getBalanceGold() < mOrder.orderMain.totalMoney) {
                    ToastUtil.error("账户购物券不足");
                } else {
                    PayUtil.payBalance(PayOrderActivity.this, mOrder, mPayType);
                }
                break;
            case AppTypes.PAY_TYPE.ALI:
                AliPayUtils.pay(this, mOrder);
                break;
            case AppTypes.PAY_TYPE.CHANJIE:
            case AppTypes.PAY_TYPE.SHANDE:
                WePayUtils.payByWeb(this, mOrder, mPayType);
                break;
            default:
                ToastUtil.error("请选择支付方式");
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(PayMsg msgStatus) {
        switch (msgStatus.getAction()) {
            case PayMsg.ACTION_ALIPAY_SUCCEED:
            case PayMsg.ACTION_WEBPAY_SUCCEED:
            case PayMsg.ACTION_WXPAY_SUCCEED:
                EventBus.getDefault().post(new EventMessage(Event.paySuccess));
                if (goodsType != GOODSTYPE_LIVE_STORE && goodsType != GOODSTYPE_CREDIT_LIFE_UPGRADE) {
                    EventUtil.viewOrderDetail(this, mOrder.orderMain.orderCode, false);
                }
                ToastUtil.success("支付成功");
                finish();
                break;
            case PayMsg.ACTION_BALANCE_SUCCEED:
                finish();
                break;
            case PayMsg.ACTION_WXPAY_FAIL:
                ToastUtils.showShortToast(msgStatus.message);
                break;
            case PayMsg.ACTION_ALIPAY_FAIL:
                ToastUtils.showShortToast(msgStatus.message);
                break;
            case PayMsg.ACTION_WEBPAY_FAIL:
                finish();
                EventUtil.viewHome(PayOrderActivity.this);
                break;
            default:
        }
    }
}
