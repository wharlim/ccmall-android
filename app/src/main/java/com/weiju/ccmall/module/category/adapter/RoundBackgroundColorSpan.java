package com.weiju.ccmall.module.category.adapter;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.style.ReplacementSpan;

import com.weiju.ccmall.module.shop.views.Utils;

public class RoundBackgroundColorSpan extends ReplacementSpan {
    private int bgColor;
    private int textColor;
    public RoundBackgroundColorSpan(int bgColor, int textColor) {
        super();
        this.bgColor = bgColor;
        this.textColor = textColor;
    }
    @Override
    public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
        //设置宽度为文字宽度加4dp
        return ((int)paint.measureText(text, start, end)+ Utils.dpToPx(8));
    }

    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
        int originalColor = paint.getColor();
        paint.setColor(this.bgColor);
        //画圆角矩形背景
        canvas.drawRoundRect(new RectF(x + Utils.dpToPx(1),
                        top+ Utils.dpToPx(2),
                        x + ((int) paint.measureText(text, start, end)+ Utils.dpToPx(7)),
                        bottom-Utils.dpToPx(2)),

                Utils.dpToPx(4),
                Utils.dpToPx(4),
                paint);
        paint.setColor(this.textColor);
        //画文字,两边各增加4dp
        canvas.drawText(text, start, end, x+Utils.dpToPx(4), y, paint);
        //将paint复原
        paint.setColor(originalColor);
    }
}
