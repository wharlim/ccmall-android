package com.weiju.ccmall.module.blockchain.beans;

import com.google.gson.annotations.SerializedName;

public class BlockChainSwitch {

    /**
     * iosReview : {"appReviewId":1,"isOpen":1,"reviewStatus":1,"reviewVersion":"1.12.5","moduleType":1}
     * blockchain : {"appReviewId":5,"isOpen":1,"reviewStatus":0,"reviewVersion":"","moduleType":3}
     */

    @SerializedName("iosReview")
    public IosReviewBean iosReview;
    @SerializedName("blockchain")
    public BlockchainBean blockchain;

    public static class IosReviewBean {
        /**
         * appReviewId : 1
         * isOpen : 1
         * reviewStatus : 1
         * reviewVersion : 1.12.5
         * moduleType : 1
         */

        @SerializedName("appReviewId")
        public int appReviewId;
        @SerializedName("isOpen")
        public int isOpen;
        @SerializedName("reviewStatus")
        public int reviewStatus;
        @SerializedName("reviewVersion")
        public String reviewVersion;
        @SerializedName("moduleType")
        public int moduleType;
    }

    public static class BlockchainBean {
        /**
         * appReviewId : 5
         * isOpen : 1
         * reviewStatus : 0
         * reviewVersion :
         * moduleType : 3
         */

        @SerializedName("appReviewId")
        public int appReviewId;
        @SerializedName("isOpen")
        public int isOpen;
        @SerializedName("reviewStatus")
        public int reviewStatus;
        @SerializedName("reviewVersion")
        public String reviewVersion;
        @SerializedName("moduleType")
        public int moduleType;
    }
}
