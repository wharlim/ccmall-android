package com.weiju.ccmall.module.xysh.activity.repayment;

import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.RepaymentListActivity;
import com.weiju.ccmall.module.xysh.adapter.RepaymentPlanAdapter;
import com.weiju.ccmall.module.xysh.bean.PlanDetail;
import com.weiju.ccmall.module.xysh.bean.PlanForCard;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class RepaymentPlanListFragment extends BaseListFragment {

    private RepaymentPlanAdapter mAdapter = new RepaymentPlanAdapter();
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    QueryUserBankCardResult.BankInfListBean creditCard;
    private int page;
    private int limit = 15;

    private String getTranSts() {
        int type = getArguments().getInt("type");
        if (type == 0) {
            return "";
        } else {
            return String.valueOf(type);
        }
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        creditCard = (QueryUserBankCardResult.BankInfListBean) getArguments().getSerializable("creditCard");
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        PlanForCard planForCard = mAdapter.getData().get(position);
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser == null) {
            ToastUtil.error("未登录！");
            return;
        }
        APIManager.startRequest(service.payBackPlanList(loginUser.phone, planForCard.planNo), new Observer<PlanDetail>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(PlanDetail planDetail) {
                RepaymentPlanActivity.startForDetail(getContext(), planDetail, creditCard, planForCard.planNo, planForCard.isRunning());
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }
    @Override
    public int getContentLayoutRes() {
        return R.layout.fragment_repayment_plan_list;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessage(EventMessage message) {
        if (message.getEvent() == Event.deleteRepayPlan) {
            getData(true);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void getData(boolean isRefresh) {
//        List<String> list = new ArrayList<>();
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        list.add("dd");
//        mAdapter.setNewData(list);
//        mRefreshLayout.setRefreshing(false);
        if (isRefresh) {
            page = 1;
        }
        APIManager.startRequest(service.newPayBackPlanStatusQuery(creditCard.cardNo, getTranSts(), page, limit),
                new Observer<XYSHCommonResult<PlanForCard.PlanForCardContainer>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(XYSHCommonResult<PlanForCard.PlanForCardContainer> ret) {
                        if (ret.success || ret.code == 1) {
                            if (page == 1) {
                                mAdapter.setNewData(ret.data.data);
                            } else {
                                mAdapter.addData(ret.data.data);
                            }
                            if (ret.data.data.size() < limit) {
                                mAdapter.loadMoreEnd(true);
                            } else {
                                mAdapter.loadMoreComplete();
                            }
                            page++;
                            mRefreshLayout.setRefreshing(false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public static RepaymentPlanListFragment newInstance(int type, QueryUserBankCardResult.BankInfListBean creditCard) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        args.putSerializable("creditCard", creditCard);
        RepaymentPlanListFragment fragment = new RepaymentPlanListFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
