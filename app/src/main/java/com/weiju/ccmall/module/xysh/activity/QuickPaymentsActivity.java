package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.quick_pay.SelectCardActivity;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.fragment.PayChannelBottomSheetDialogFragment;
import com.weiju.ccmall.module.xysh.fragment.PayChannelFragment;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class QuickPaymentsActivity extends BaseActivity implements PayChannelFragment.OnChannelClickListener {

    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    @BindView(R.id.etTransferAmt)
    EditText etTransferAmt;
    @BindView(R.id.tvPayCard)
    TextView tvPayCard;
    @BindView(R.id.llPayCardContainer)
    LinearLayout llPayCardContainer;
    @BindView(R.id.tvReceiptCard)
    TextView tvReceiptCard;
    @BindView(R.id.llReceiptCardContainer)
    LinearLayout llReceiptCardContainer;
    @BindView(R.id.tv_next)
    TextView tvNext;

    float transferAmt = 0f;

    private static final int REQ_SELECT_CREDIT = 1;
    private static final int REQ_SELECT_DEBIT = 2;

    private PayChannelBottomSheetDialogFragment dialogFragment;

    QueryUserBankCardResult.BankInfListBean payCard;
    QueryUserBankCardResult.BankInfListBean receiptCard;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_payments);
        ButterKnife.bind(this);
        setLeftBlack();
        setTitle("发起收款");
        //initPayChannels();
        loadDefaultPayCard();
        loadDefaultReceiptCard();
        etTransferAmt.post(new Runnable() {
            @Override
            public void run() {
                etTransferAmt.requestFocus();
            }
        });
    }

    //private void initPayChannels() {
    //    getSupportFragmentManager().beginTransaction()
    //            .add(R.id.fl_pay_channels, PayChannelFragment.newInstance())
    //            .commit();
    //}
    private void loadDefaultReceiptCard() {
        APIManager.startRequest(service.cardInfoGet(1), new Observer<QueryUserBankCardResult>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(QueryUserBankCardResult ret) {
                if (ret.code == 1 && ret.bankInfList.size() > 0) {
                    setReceiptCard(ret.bankInfList.get(0));
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }
    private void loadDefaultPayCard() {
        APIManager.startRequest(service.cardInfoGet(0), new Observer<QueryUserBankCardResult>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(QueryUserBankCardResult ret) {
                if (ret.code == 1 && ret.bankInfList.size() > 0) {
                    setPayCard(ret.bankInfList.get(0));
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof PayChannelBottomSheetDialogFragment) {
            dialogFragment = (PayChannelBottomSheetDialogFragment)fragment;
            dialogFragment.setOnChannelClickListener(this);
        }
    }

    //    private void addPayChannel() {
    //        View channelView = LayoutInflater.from(this).inflate(R.layout.item_pay_channel, llPayChannels, false);
    //        llPayChannels.addView(channelView);
    //        channelView.setOnClickListener(v -> {
    //            SelectCardActivity.start(this);
    //        });
    //    }

    public static void start(Context context) {
        Intent intent = new Intent(context, QuickPaymentsActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onChannelClick(int position, ChannelItem channelItem) {
        //if (!channelItem.getWasInTime()) {
        //    ToastUtil.error("该通道不在交易时间内");
        //    return;
        //}
        bindCheck(channelItem);
    }

    public void bindCheck(ChannelItem channelItem) {
        ToastUtil.showLoading(this, true);
        APIManager.startRequest(service.bindCheck(channelItem.channelShortName, receiptCard.cardNo, payCard.cardNo),
                new Observer<XYSHCommonResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(XYSHCommonResult<Object> ret) {
                        ToastUtil.hideLoading();
                        if (ret.code == 1) {
                            SelectCardActivity.start(QuickPaymentsActivity.this, channelItem, transferAmt, payCard, receiptCard);
                            dialogFragment.dismiss();
                        } else {
                            ToastUtil.error("" + ret.msg);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtil.error(e.getMessage());
                        ToastUtil.hideLoading();
                    }

                    @Override
                    public void onComplete() {
                        ToastUtil.hideLoading();
                    }
                });
    }

    @OnClick({R.id.llPayCardContainer, R.id.llReceiptCardContainer, R.id.tv_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llPayCardContainer:
                BankAdminActivity.startForSelectCredit(this, REQ_SELECT_CREDIT);
                break;
            case R.id.llReceiptCardContainer:
                BankAdminActivity.startForSelectDebit(this, REQ_SELECT_DEBIT);
                break;
            case R.id.tv_next:
                try {
                    String transferAmtTxt = etTransferAmt.getText().toString().trim();
                    if (TextUtils.isEmpty(transferAmtTxt)) {
                        ToastUtil.error("交易金额不能为空!");
                        return;
                    }
                    transferAmt = Float.valueOf(etTransferAmt.getText().toString().trim());
                } catch (Exception e) {
                    ToastUtil.error("请输入有效的交易金额!");
                    return;
                }
                if (transferAmt < 200f) {
                    ToastUtil.error("输入交易金额不能低于200元");
                    return;
                }
                if (payCard == null || receiptCard == null) {
                    ToastUtil.error("请选择扣款银行卡和到账储蓄卡!");
                    return;
                }
                PayChannelBottomSheetDialogFragment.newInstance(payCard, receiptCard).show(getSupportFragmentManager(), "PayChannelBottomSheetDialogFragment");
                break;
        }
    }
    private void setPayCard(QueryUserBankCardResult.BankInfListBean card) {
        payCard = card;
        tvPayCard.setText(String.format("%s(%s)", card.bankName, BankUtils.cutBankCardNo(card.cardNo)));
        tvPayCard.setTextColor(getResources().getColor(R.color.text_black));
    }

    private void setReceiptCard(QueryUserBankCardResult.BankInfListBean card) {
        receiptCard = card;
        tvReceiptCard.setText(String.format("%s(%s)", card.bankName, BankUtils.cutBankCardNo(card.cardNo)));
        tvReceiptCard.setTextColor(getResources().getColor(R.color.text_black));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            if (requestCode == REQ_SELECT_CREDIT) {
                setPayCard(BankAdminActivity.getSelectResult(data));
            } else if (requestCode == REQ_SELECT_DEBIT) {
                setReceiptCard(BankAdminActivity.getSelectResult(data));
            }
        }
    }
}
