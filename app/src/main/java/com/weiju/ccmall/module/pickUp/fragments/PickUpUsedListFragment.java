package com.weiju.ccmall.module.pickUp.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.pickUp.adapter.PickUpUsedAdapter;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.PickUp;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IPickUpService;

public class PickUpUsedListFragment extends BaseListFragment {
    private PickUpUsedAdapter mAdapter;
    private IPickUpService mService = ServiceManager.getInstance().createService(IPickUpService.class);
    private int mCurrentPage = 1;

    public static final int TYPE_USED = 1;
    public static final int TYPE_GIVING = 5;
    public static final int TYPE_RECEIVED = 4;

    private int type;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        type = getArguments().getInt("type", TYPE_USED);
        mAdapter = new PickUpUsedAdapter(type);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.bg_gray));
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
            mAdapter.setEnableLoadMore(true);
        }
        int pageSize = 15;
        APIManager.startRequest(mService.queryListByType(mCurrentPage, pageSize, type),
                new BaseRequestListener<PaginationEntity<PickUp, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<PickUp, Object> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.list.size() < pageSize) {
                            mAdapter.loadMoreEnd(true);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                        mCurrentPage++;
                    }
                },getContext());
    }

    public static PickUpUsedListFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        PickUpUsedListFragment fragment = new PickUpUsedListFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
