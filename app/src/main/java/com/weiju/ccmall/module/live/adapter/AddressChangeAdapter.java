package com.weiju.ccmall.module.live.adapter;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Address;

public class AddressChangeAdapter extends BaseQuickAdapter<Address, BaseViewHolder> {


    public AddressChangeAdapter() {
        super(R.layout.item_address_change);
    }

    @Override
    protected void convert(BaseViewHolder helper, Address item) {
        helper.getView(R.id.ivSelect).setVisibility(item.isSelect ? View.VISIBLE : View.INVISIBLE);
        helper.setText(R.id.tvPhone, item.phone)
                .setText(R.id.tvContacts, item.contacts)
                .setText(R.id.tvAddressDetail, item.getFullAddress())
                .setVisible(R.id.ivDefault, item.isDefault)
        ;
    }
}
