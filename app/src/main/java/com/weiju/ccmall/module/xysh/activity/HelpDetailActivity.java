package com.weiju.ccmall.module.xysh.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.ButterKnife;

/**
 * @author chenyanming
 * @time 2019/12/16 on 15:29
 * @desc
 */
public class HelpDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_detail);
        ButterKnife.bind(this);
        setTitle("帮助文档");
        setLeftBlack();
    }
}
