package com.weiju.ccmall.module.live.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.activity.GoodsEditActivity;
import com.weiju.ccmall.module.live.activity.GoodsManagementActivity;
import com.weiju.ccmall.module.live.adapter.GoodsManagementAdapter;
import com.weiju.ccmall.module.live.entity.ProductEntity;
import com.weiju.ccmall.module.live.entity.UploadProductEntity;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.OnClick;

public class GoodsManagementFragment extends BaseListFragment {

    private List<ProductEntity> mData = new ArrayList<>();
    private GoodsManagementAdapter mAdapter;

    private int mType;
    private int mPage;
    ILiveStoreService mService;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.fragment_goods_management;
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mType = getArguments().getInt("type");
        mAdapter = new GoodsManagementAdapter(mData, mType);
    }

    @Override
    public void initData() {
        mService = ServiceManager.getInstance().createService(ILiveStoreService.class);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.bg_gray));
        super.initData();
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        ProductEntity entity = mData.get(position);
        switch (view.getId()) {
            case R.id.tvDelete:
                WJDialog mDialogDelete = new WJDialog(getContext());
                mDialogDelete.show();
                mDialogDelete.setTitle("删除产品");
                mDialogDelete.setContentText("删除后无法恢复，确定删除吗？");
                mDialogDelete.setOnConfirmListener(v -> {
                    mDialogDelete.dismiss();
                    delProduct(entity.productId);
                });
                break;
            case R.id.tvEdit:
                getProductInfo(entity.productId);
                break;
            case R.id.tvStandUpOrDown:
                WJDialog mDialogUpdate = new WJDialog(getContext());
                mDialogUpdate.show();
                mDialogUpdate.setTitle(mType == 1 ? "下架产品" : "上架产品");
                mDialogUpdate.setContentText(mType == 1 ? "确定下架该产品吗？" : "确定上架该产品吗？");
                mDialogUpdate.setOnConfirmListener(v -> {
                    mDialogUpdate.dismiss();
                    upProductStatus(mType == 1 ? 0 : 1, entity.productId);
                });
                break;
            case R.id.tvPreview:
                preViewPro(entity.skuId);
                break;
            default:
                break;
        }
    }

    private void preViewPro(String skuId) {
        EventUtil.viewProductDetail(getContext(), skuId, false);
    }

    private void getProductInfo(String productId) {
        APIManager.startRequest(mService.getProductInfo(productId), new BaseRequestListener<UploadProductEntity>(getActivity()) {
            @Override
            public void onSuccess(UploadProductEntity result) {
                super.onSuccess(result);
                GoodsEditActivity.start(getContext(), result);
            }
        }, getContext());
    }

    private void upProductStatus(int type, String productId) {
        APIManager.startRequest(mService.upProductStatus(type, productId), new BaseRequestListener<Object>(getActivity()) {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                ToastUtil.success(mType == 1 ? "产品下架成功" : "产品上架成功");
                EventBus.getDefault().post(new EventMessage(Event.liveStoreProductUpdate));
            }
        }, getContext());
    }

    private void delProduct(String productId) {
        APIManager.startRequest(mService.delProduct(productId), new BaseRequestListener<Object>(getActivity()) {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                ToastUtil.success("删除产品成功");
                EventBus.getDefault().post(new EventMessage(Event.liveStoreProductUpdate));
            }
        }, getContext());
    }


    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(final boolean isRefresh) {
        if (isRefresh) {
            mPage = 1;
        } else {
            mPage++;
        }
        APIManager.startRequest(mService.getProList(mType, mPage, 15),
                new BaseRequestListener<PaginationEntity<ProductEntity, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<ProductEntity, Object> result) {
                        super.onSuccess(result);
                        if (mPage == 1) {
                            mData.clear();
                            if (mType == 1) {
                                ((GoodsManagementActivity) getActivity()).upDateCount(result.total);
                            }
                        }
                        mData.addAll(result.list);
                        mAdapter.notifyDataSetChanged();
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && mData.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, getContext()
        );

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
        if (message.getEvent().equals(Event.liveStoreProductUpdate)) {
            getData(true);
        }
    }

    @OnClick(R.id.tvAddGoods)
    public void onViewClicked() {
        GoodsEditActivity.start(getContext());
    }

    /**
     * @param type 出售中：1    已下架：0
     * @return
     */
    public static GoodsManagementFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        GoodsManagementFragment fragment = new GoodsManagementFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
