package com.weiju.ccmall.module.xysh.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.SelectSupportBankItem;

public class SelectSupportBankAdapter extends BaseQuickAdapter<SelectSupportBankItem, BaseViewHolder> {
    public SelectSupportBankAdapter() {
        super(R.layout.item_select_support_bank);
    }

    @Override
    protected void convert(BaseViewHolder helper, SelectSupportBankItem item) {
        helper.setText(R.id.tvBankName, item.bankName);
    }
}
