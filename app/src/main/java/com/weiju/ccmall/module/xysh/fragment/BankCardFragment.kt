package com.weiju.ccmall.module.xysh.fragment

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.support.v7.widget.Toolbar
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.qmuiteam.qmui.util.QMUIStatusBarHelper
import com.weiju.ccmall.R
import com.weiju.ccmall.module.xysh.XyshService
import com.weiju.ccmall.module.xysh.activity.SelectBankActivity
import com.weiju.ccmall.module.xysh.helper.ListDialog
import com.weiju.ccmall.shared.basic.AgentFragment
import com.weiju.ccmall.shared.bean.Bank
import com.weiju.ccmall.shared.constant.Action
import com.weiju.ccmall.shared.util.SessionUtil
import com.weiju.ccmall.shared.util.ToastUtil
import kotlinx.android.synthetic.main.fragment_bank_card.*

/**
 * Created by 林先生_lcy.
 * User: lcy
 * Date: 2019/4/2
 * Time: 下午6:53
 */
class BankCardFragment : AgentFragment() {

    private var pos: Int = 0
    private var pro: String? = SessionUtil.getInstance().getString("prov_sm")
    private var ci: String? = SessionUtil.getInstance().getString("city_sm")
    private var dialog: ListDialog? = null
    private val mAdapter = Adapter()
    //选择的银行卡
    private var mBank: Bank? = null

    override fun layoutId(): Int {
        return R.layout.fragment_bank_card
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        QMUIStatusBarHelper.setStatusBarDarkMode(activity)
        QMUIStatusBarHelper.translucent(activity, resources.getColor(R.color.red))
        val toolbar = activity?.findViewById<Toolbar>(R.id.toolbar)
        toolbar?.setBackgroundColor(resources.getColor(R.color.red))
        toolbar?.setNavigationIcon(R.drawable.ioco)
        toolbar?.findViewById<TextView>(R.id.toolbarTitle)?.setTextColor(-1)
        super.onViewCreated(view, savedInstanceState)
        activity?.title = "添加银行卡"
        dialog = ListDialog(context!!)
        dialog?.setAdapter(mAdapter)
        init()
        province.setOnClickListener {
            request(::getArea, service2(XyshService::class.java).getArea(
                    SessionUtil.getInstance().oAuthToken, null, null
            ))
        }

        tvBankName.setOnClickListener {
            val intent = Intent(activity, SelectBankActivity::class.java)
            startActivityForResult(intent, Action.SELECT_BANK)
        }

        city.setOnClickListener {
            if (pro == null)
                ToastUtil.error("请先选择省份")
            else
                request(::getArea, service2(XyshService::class.java).getArea(
                        SessionUtil.getInstance().oAuthToken, pro, null
                ), 1)
        }
        area.setOnClickListener {
            when {
                pro == null -> ToastUtil.error("请先选择省份")
                ci == null -> ToastUtil.error("请先选择城市")
                else -> request(::getArea, service2(XyshService::class.java).getArea(
                        SessionUtil.getInstance().oAuthToken, pro, ci
                ), 2)
            }
        }
        next.setOnClickListener {

            if (null == mBank) {
                ToastUtil.error("请选择开户行")
                return@setOnClickListener
            }
            when {
                TextUtils.isEmpty(idNum.text) -> ToastUtil.error("请填写银行卡号")
                TextUtils.isEmpty(phone.text) -> ToastUtil.error("请填写预留手机号")
                TextUtils.isEmpty(province.text) -> ToastUtil.error("请填写开户省")
                TextUtils.isEmpty(city.text) -> ToastUtil.error("请填写开户市")
                TextUtils.isEmpty(area.text) -> ToastUtil.error("请填写开户区/县")
                else -> {
                    save()
//                    request(::success, service2(XyshService::class.java).userBankRelauth(
//                            mBank?.bankNo,
//                            mBank?.bankName,
//                            SessionUtil.getInstance().loginUser.phone,
//                            idNum.text.toString(),
//                            phone.text.toString(),
//                            SessionUtil.getInstance().oAuthToken,
//                            pro, ci, area.text.toString()
//                    ))
                }
            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode) {
            Action.SELECT_BANK -> {
                mBank = data?.extras?.get("bank") as Bank?
                if (null != mBank) {
                    tvBankName.text = mBank?.bankName
                }
            }
        }

    }

    private fun init() {
        val util = SessionUtil.getInstance()
        val idn = util.getString("idNum")
        if (idn != null)
            idNum.setText(idn)
        val bn = util.getString("bankName")
        if (bn != null)
            bankName.setText(bn)
        val p = util.getString("resTel_Phone")
        if (p != null)
            phone.setText(p)
        if (pro != null)
            province.text = pro
        if (ci != null)
            city.text = ci
        val zone = util.getString("zone")
        if (zone != null)
            area.text = zone
    }

    private fun save() {
        val util = SessionUtil.getInstance()
        util.putString("idNum", idNum.text.toString())
        util.putString("bankName", bankName.text.toString())
        util.putString("resTel_Phone", phone.text.toString())
        util.putString("prov_sm", pro)
        util.putString("city_sm", ci)
        util.putString("zone", area.text.toString())
    }

    private fun getArea(postCode: Int, data: MutableList<String?>?) {
        pos = postCode
        mAdapter.setNewData(data)
        dialog?.resetHeight(data?.size)
        dialog?.show()
    }

    private fun success(postCode: Int, data: Any?) {
        ToastUtil.success("实名认证成功！")
        activity?.setResult(1015)
        activity?.finish()
    }

    private inner class Adapter : RecyclerView.Adapter<ViewHolder>() {

        private val data = ArrayList<Any?>(0)

        override fun onCreateViewHolder(vg: ViewGroup, p1: Int): ViewHolder {
            val v = LayoutInflater.from(vg.context).inflate(R.layout.item_p_city, vg, false)
            return object : ViewHolder(v) {}
        }

        override fun getItemCount(): Int {
            return data.size
        }

        override fun onBindViewHolder(h: ViewHolder, p: Int) {
            val view = h.itemView.findViewById<TextView>(R.id.name)
            view.text = data[p]?.toString()
            view.setOnClickListener {
                when (pos) {
                    0 -> {
                        pro = data[p]?.toString()
                        province.text = pro
                    }
                    1 -> {
                        ci = data[p]?.toString()
                        city.text = ci
                    }
                    2 -> area.text = data[p]?.toString()
                }
                dialog?.hide()
            }
        }

        fun setData(data: MutableList<String?>?) {
            if (data == null)
                return
            this.data.addAll(data)
            notifyDataSetChanged()
        }

        fun setNewData(data: MutableList<String?>?) {
            this.data.clear()
            if (data == null)
                return
            this.data.addAll(data)
            notifyDataSetChanged()
        }

    }

}
