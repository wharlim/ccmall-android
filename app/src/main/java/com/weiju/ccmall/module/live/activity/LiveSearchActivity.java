package com.weiju.ccmall.module.live.activity;

import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.GridLayoutManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.sobot.chat.widget.kpswitch.util.KeyboardUtil;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.module.live.adapter.LiveHomeAdapter;
import com.weiju.ccmall.module.live.widgets.SearchPopupMenu;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.NullLoadMoreView;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/5 on 16:36
 * @desc
 */
public class LiveSearchActivity extends BaseListActivity implements SearchPopupMenu.OnMenuClickListener {
    @BindView(R.id.layoutTopTitle)
    RelativeLayout mLayoutTitle;
    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.keywordEt)
    protected EditText mKeywordEt;

    @BindView(R.id.flSearchType)
    protected FrameLayout flSearchType;
    @BindView(R.id.tvSearchType)
    protected TextView tvSearchType;

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);

    private LiveHomeAdapter mAdapter = new LiveHomeAdapter();
    private User mLoginUser;
    private String mKeyword;

    private int liveListSearchType;

    @Override
    public String getTitleStr() {
        return "";
    }

    @Override
    public void initView() {
        super.initView();
        EventBus.getDefault().register(this);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            QMUIStatusBarHelper.translucent(LiveSearchActivity.this);
//            QMUIStatusBarHelper.setStatusBarLightMode(LiveSearchActivity.this);
//            QMUIStatusBarHelper.setStatusBarDarkMode(LiveSearchActivity.this);
//        }
        getHeaderLayout().setVisibility(View.GONE);
//        int height = QMUIStatusBarHelper.getStatusbarHeight(LiveSearchActivity.this);
//        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mLayoutTitle.getLayoutParams();
//        layoutParams.setMargins(0, height, 0, 0);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(LiveSearchActivity.this, 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setBackgroundColor(Color.parseColor("#EFEFEF"));
        mAdapter.setLoadMoreView(new NullLoadMoreView());
        mTitle.setText("搜索");

        mLoginUser = SessionUtil.getInstance().getLoginUser();

        mKeywordEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH || (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    search(textView.getText().toString());
                    return true;
                }
                return false;
            }
        });

    }

    private void search(String keyword) {
        if (TextUtils.isEmpty(keyword)) {
            ToastUtil.error("请输入内容");
            return;
        }
        KeyboardUtil.hideKeyboard(mKeywordEt);
        mKeyword = keyword;
        mCurrentPage = 1;
        getData(true);
    }

    @Override
    public View getEmptyView() {
        View inflate = View.inflate(this, R.layout.live_no_data, null);
        return inflate;
    }

    @Override
    public void initData() {
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        LiveRoom item = mAdapter.getItem(position);
        if (null == item) {
            return;
        }
        LiveManager.checkRoomPassword(LiveSearchActivity.this, item);

    }

    @OnClick(R.id.flSearchType)
    public void showSearchTypePopup() {
        tvSearchType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_search_arrow_open, 0);
        SearchPopupMenu searchPopupMenu = new SearchPopupMenu(this, this);
        searchPopupMenu.showAsDropDown(flSearchType);
        searchPopupMenu.setOnDismissListener(() -> {
            tvSearchType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_search_arrow_close, 0);
        });
    }

    @Override
    public void onMenuClick(int menuIndex, String title) {
        tvSearchType.setText(title);
        switch (menuIndex) {
            case 0:
                liveListSearchType = 1;
                break;
            case 1:
                liveListSearchType = 2;
                break;
            case 2:
                liveListSearchType = 0;
                break;
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        if (TextUtils.isEmpty(mKeyword)) {
            return;
        }
        APIManager.startRequest(mService.searchLiveList(liveListSearchType, mKeyword, mCurrentPage, Constants.PAGE_SIZE),
                new BaseRequestListener<PaginationEntity<LiveRoom, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<LiveRoom, Object> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                },LiveSearchActivity.this);

    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_live_search;
    }

    @OnClick(R.id.back)
    protected void back() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case liveRoomChange:
                mCurrentPage = 1;
                getData(true);
                break;
            default:
        }
    }

}
