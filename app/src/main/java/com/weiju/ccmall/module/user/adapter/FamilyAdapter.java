package com.weiju.ccmall.module.user.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.blankj.utilcode.utils.StringUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderSaleDetailActivity;
import com.weiju.ccmall.module.user.ChangeCustomerNoteNameActivity;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Family;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import java.util.List;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/8/2.
 */
public class FamilyAdapter extends BaseQuickAdapter<Family.DatasEntity, BaseViewHolder> {

    public FamilyAdapter(@Nullable List<Family.DatasEntity> data) {
        super(R.layout.item_family, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Family.DatasEntity item) {
//        SimpleDraweeView simpleDraweeView = helper.getView(R.id.ivAvatar);
//        FrescoUtil.setImageSmall(simpleDraweeView, item.headImage);
//        helper.setText(R.id.tvName, String.format("%s%s",TextUtils.isEmpty(item.nickName)?"":item.nickName ,
//                TextUtils.isEmpty(item.phone)?"":"(" +  item.phone+ ")"));
//        if (StringUtils.isEmpty(item.memberStr)) {
//            helper.setVisible(R.id.tvTag, false);
//        } else {
//            helper.setText(R.id.tvTag, item.memberStr);
//        }
//        helper.setText(R.id.tvMoney, ConvertUtil.centToCurrency(helper.itemView.getContext(), item.currMonthSaleMoney));
        SimpleDraweeView simpleDraweeView = helper.getView(R.id.avatarIv);
        FrescoUtil.setImageSmall(simpleDraweeView, item.headImage);
        helper.setText(R.id.customerLevel, item.memberTypeStr);
        helper.setText(R.id.noteName, TextUtils.isEmpty(item.noteName) ? item.nickName : item.noteName);
        helper.setText(R.id.no, item.phone);
        if (!TextUtils.isEmpty(item.noteName)) {
            helper.setText(R.id.nickName, "昵称:"+item.nickName);
        } else {
            helper.setText(R.id.nickName, "");
        }
        helper.setText(R.id.consume, "¥" + item.currMonthSaleMoney/100);
        helper.getView(R.id.setNoteBtn).setOnClickListener(v -> {
            ChangeCustomerNoteNameActivity.start((Activity) helper.itemView.getContext(),
                    item.memberId, item.noteName);
        });
        helper.getView(R.id.chatBtn).setOnClickListener(v -> {
            contactCustomer(v.getContext(), item);
        });
    }

    private void contactCustomer(Context context, Family.DatasEntity item) {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null && item != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, context);
                    ChatInfo chatInfo = new ChatInfo();
                    chatInfo.setType(TIMConversationType.C2C);
                    chatInfo.setId(item.memberId);
                    chatInfo.setChatName(TextUtils.isEmpty(item.noteName) ? item.nickName : item.noteName);
                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.putExtra("chatInfo", chatInfo);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }
}
