package com.weiju.ccmall.module.jkp;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alibaba.baichuan.android.trade.adapter.login.AlibcLogin;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.adapter.ProductVideoViewPagerAdapter;
import com.weiju.ccmall.module.user.LoginActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.basic.BaseSubscriber;
import com.weiju.ccmall.shared.bean.JkpOriginalProduct;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.component.TagTextView;
import com.weiju.ccmall.shared.component.dialog.ShareDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.WebViewUtil;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class JiKaoPuProductDetailActivity extends BaseActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tvBannerImageText)
    TextView tvBannerImageText;
    @BindView(R.id.tvTagTitle)
    TagTextView tvTagTitle;
    @BindView(R.id.tvDesc)
    TextView tvDesc;
    @BindView(R.id.tvRetailPrice)
    TextView tvRetailPrice;
    @BindView(R.id.tvMarketPrice)
    TextView tvMarketPrice;
    @BindView(R.id.layoutPrice)
    LinearLayout layoutPrice;
    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.tvCouponValue)
    TextView tvCouponValue;
    @BindView(R.id.tvPeriodOfValidity)
    TextView tvPeriodOfValidity;
    @BindView(R.id.tvShare)
    TextView tvShare;
    @BindView(R.id.tvGetCoupon)
    TextView tvGetCoupon;
    @BindView(R.id.tvCouponUnit)
    TextView tvCouponUnit;

    private String skuId;
//    private String originId;
    private SkuInfo skuInfo;
    private JkpOriginalProduct jpkOriginalProduct;
    private IJkpProductService service;
    private boolean isTaobaoAuth;

    private ShareDialog shareDialog;

    {
        service = ServiceManager.getInstance().createService(IJkpProductService.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jikaopu_product_detail);
        ButterKnife.bind(this);
        setTitle("商品详情");
        setLeftBlack();
        WebViewUtil.configWebView(webview);
        webview.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        webview.setWebViewClient(WebViewUtil.getOverrideAllUrlLoadingClient());
        getIntentData();
        getProductDetail();

//        AlibcLogin.getInstance().logout(this, new LogoutCallback() {
//            @Override
//            public void onSuccess() {
//
//            }
//
//            @Override
//            public void onFailure(int i, String s) {
//
//            }
//        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        ToastUtil.hideLoading();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getTaoBaoAuth();
    }

    private void getIntentData() {
        skuInfo = (SkuInfo) getIntent().getSerializableExtra("skuInfo");
        skuId = getIntent().getStringExtra("skuId");
    }

    private void getProductDetail() {
        if (skuInfo == null) {
            APIManager.startRequest(service.goodsDetail(skuId), new BaseRequestListener<SkuInfo>() {
                @Override
                public void onSuccess(SkuInfo result) {
                    super.onSuccess(result);
                    skuInfo = result;
//                    setSkuViews();
                    getJpkOriginalProduct();
                    getTaoBaoDetail();
                }
            }, this);
        } else {
//            setSkuViews();
            getJpkOriginalProduct();
            getTaoBaoDetail();
        }
    }

    private void setSkuViews() {

        tvTagTitle.setText(skuInfo.name);
        if (TextUtils.isEmpty(skuInfo.desc)) {
            tvDesc.setVisibility(View.GONE);
        } else {
            tvDesc.setVisibility(View.VISIBLE);
            tvDesc.setText(skuInfo.desc);
        }
        tvRetailPrice.setText(MoneyUtil.centToYuan¥StrNoZero(skuInfo.retailPrice));
        tvMarketPrice.setText(MoneyUtil.centToYuan¥StrNoZero(skuInfo.marketPrice));
        TextViewUtil.addThroughLine(tvMarketPrice);
        // 优惠券
        tvCouponValue.setText(MoneyUtil.centToYuanStrNoZero(skuInfo.costPrice));
    }

    private void getJpkOriginalProduct() {
        ToastUtil.showLoading(this);
        APIManager.startRequest(service.getUrl(skuInfo.originId), new BaseRequestListener<JkpOriginalProduct>() {
            @Override
            public void onSuccess(JkpOriginalProduct result) {
                super.onSuccess(result);
                ToastUtil.hideLoading();
                jpkOriginalProduct = result;
                if (result.isValidity()) {
                    tvPeriodOfValidity.setText(String.format("有效期：%s", result.couponEndTime));
                } else {
                    tvPeriodOfValidity.setText("有效期：已过期");
                }

                tvTagTitle.setText(result.title);
                tvDesc.setVisibility(View.GONE);
                // 优惠券
                int costPrice = result.getCouponAmountFromCouponInfo();
                tvCouponValue.setText(String.valueOf(costPrice));
                skuInfo.costPrice = MoneyUtil.yuanStrToCentLong(String.valueOf(costPrice));
                //满减价格
                int fullAmount = result.getFullAmountFromCouponInfo();
                if (fullAmount * 100 > result.zkFinalPrice) {
                    skuInfo.retailPrice = result.zkFinalPrice;
                    tvRetailPrice.setText(MoneyUtil.centToYuanStrNo00End(result.zkFinalPrice));
                    tvCouponValue.setTextSize(16);
                    tvCouponValue.setText(result.couponInfo);
                    tvCouponUnit.setVisibility(View.GONE);
                } else {
                    skuInfo.retailPrice = result.zkFinalPrice - costPrice * 100;
                    tvRetailPrice.setText(MoneyUtil.centToYuanStrNo00End(result.zkFinalPrice - costPrice * 100));
                }
                skuInfo.marketPrice = result.zkFinalPrice;
                tvMarketPrice.setText(MoneyUtil.centToYuan¥StrNoZero(result.zkFinalPrice));
                TextViewUtil.addThroughLine(tvMarketPrice);
//                tvRetailPrice.setText(MoneyUtil.centToYuanStrNo00End(result.zkFinalPrice - costPrice * 100));

                ProductVideoViewPagerAdapter adapter =
                        new ProductVideoViewPagerAdapter(JiKaoPuProductDetailActivity.this, Arrays.asList(result.pictUrl));
                viewPager.setAdapter(adapter);
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.hideLoading();
//                ToastUtil.error("商品已过期或不存在!");
//                finish();
            }
        }, this);
    }

    private void getTaoBaoDetail() {
        OutLinkUtils.getTaoBaoProductDetail(skuInfo.originId, new OutLinkUtils.TaoBaoProductDetailCallBack() {
            @Override
            public void onFailure(String msg) {
                ToastUtil.error(msg + "");
            }

            @Override
            public void onSuccess(OutLinkUtils.TaoBaoProductDetail detail) {
                if (detail == null) {
                    return;
                }
                //Log.d("Seven", "pcDescContent: " + detail.data.pcDescContent);
                //Log.d("Seven", "pcDescContent: " + removeGif(detail.data.pcDescContent));
                WebViewUtil.loadDataToWebViewForJkpDetail(webview, removeGif(detail.data.pcDescContent), "https://assets.alicdn.com");
            }
        });
    }

    private String removeGif(String html) {
        if (TextUtils.isEmpty(html)) {
            return html;
        }
        String reg = "<img[^<>]*?\\.gif[^<>]*?>";
        html = html.replaceAll(reg, "");
        //Log.d("Seven", "1-> " + html);
        reg = "<img[^<>]*?\\.gif[^<>]*?>[^<>]*?</img>";
        html = html.replaceAll(reg, "");
        //Log.d("Seven", "2-> " + html);
        return html;
    }

//    private ArrayList<String> parseImgs(String html) {
//        ArrayList<String> ret = new ArrayList<>();
//        if (TextUtils.isEmpty(html)) {
//            return ret;
//        }
//        String reg = "src=\"(\\S+).jpg\"";
//        Pattern pattern = Pattern.compile(reg);
//        Matcher matcher = pattern.matcher(html);
//        while (matcher.find()) {
//            ret.add("https:" + matcher.group(1) + ".jpg");
//        }
//        return ret;
//    }

    public static void start(Context context, SkuInfo skuInfo) {
        Intent intent = new Intent(context, JiKaoPuProductDetailActivity.class);
//        intent.putExtra("skuId", skuId);
//        intent.putExtra("originId", originId);
        intent.putExtra("skuInfo", skuInfo);
        context.startActivity(intent);
    }
    public static void start(Context context, String skuId) {
        Intent intent = new Intent(context, JiKaoPuProductDetailActivity.class);
        intent.putExtra("skuId", skuId);
        context.startActivity(intent);
    }

    private void copy(String content) {
        ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData mClipData = ClipData.newPlainText("Label", content);
        cm.setPrimaryClip(mClipData);
    }


    @OnClick({R.id.tvShare, R.id.tvGetCoupon, R.id.llCouponContainer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvShare:
//                share();
                if (jpkOriginalProduct == null || skuInfo == null) {
                    ToastUtil.error("商品已过期或不存在!");
                    return;
                }
                if (!isTaobaoAuth) {
                    showAuthTaobaoDialog();
                    return;
                }
                CreateShareActivity.start(this, jpkOriginalProduct, skuInfo);
                break;
            case R.id.llCouponContainer:
            case R.id.tvGetCoupon:
                getCoupon();
                break;
        }
    }

    private String getShareText() {
        return skuInfo.name + "\n" +
                "[在售价]" + MoneyUtil.centToYuan¥StrNoZero(skuInfo.marketPrice) + "\n" +
                "[券后价]" + MoneyUtil.centToYuan¥StrNoZero(skuInfo.retailPrice) + "\n" +
                "复制本条信息打开手机淘宝领卷" + jpkOriginalProduct.tbkPwd;
//        return "test";
    }

    private String getShareImgUrl() {
        return skuInfo.thumb;
    }

    private void share() {
//        Log.d("Seven", "share " + getShareText());
        shareDialog = new ShareDialog(this, getShareText(), new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                ToastUtil.showLoading(JiKaoPuProductDetailActivity.this);
                copy(getShareText());
                ToastUtil.success("已复制！");
            }

            @Override
            public void onResult(SHARE_MEDIA share_media) {
                ToastUtil.success("分享成功");
                ToastUtil.hideLoading();
            }

            @Override
            public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                ToastUtil.error("分享出错:" + throwable.getMessage());
                ToastUtil.hideLoading();
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media) {
                ToastUtil.hideLoading();
            }
        });
        shareDialog.show();
    }

    private void getCoupon() {
        if (!isTaobaoAuth) {
            showAuthTaobaoDialog();
            return;
        }
        if(jpkOriginalProduct == null) {
            ToastUtil.error("商品已过期或不存在!");
            return;
        }
        UserService.checkIsLogin(this, new UserService.IsLoginListener() {
            @Override
            public void isLogin() {
                OutLinkUtils.openTaobao(JiKaoPuProductDetailActivity.this, jpkOriginalProduct.couponShortUrl);
            }
        });
    }

    private void getTaoBaoAuth() {
        APIManager.startRequest(service.getRelationInfo(), new BaseSubscriber<RequestResult<Object>>() {
            @Override
            public void onNext(RequestResult<Object> ret) {
                isTaobaoAuth = ret.code == 0;
                Log.d("Seven", "淘宝授权: " + isTaobaoAuth);
                if (ret.code == Constants.NOT_LOGIN_CODE){
                    startActivity(new Intent(JiKaoPuProductDetailActivity.this, LoginActivity.class));
                    finish();
                }
            }
        });
    }

    private void showAuthTaobaoDialog() {
        OutLinkUtils.aliLogin(this);
    }

    private void uploadTaobaokey() {
        if (AlibcLogin.getInstance().isLogin()) {
//            Log.d("Seven", AlibcLogin.getInstance().getSession().openId + "");
//            Log.d("Seven", AlibcLogin.getInstance().getSession().openSid + "");
            APIManager.startRequest(service.saveRelationId(AlibcLogin.getInstance().getSession().openSid), new BaseRequestListener<Object>() {
                @Override
                public void onSuccess(Object result) {

                }
            }, this);
        }
    }
}
