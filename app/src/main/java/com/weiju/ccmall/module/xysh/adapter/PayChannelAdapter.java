package com.weiju.ccmall.module.xysh.adapter;

import android.graphics.Color;
import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.fragment.PayChannelFragment;

public class PayChannelAdapter extends BaseQuickAdapter<ChannelItem, BaseViewHolder> {
    private int type;
    private ChannelItem selectChannel;
    private ChannelItem firstUnSupportChannel;
    public PayChannelAdapter(int type) {
        super(R.layout.item_pay_channel);
        this.type = type;
    }

    public void setSelectChannel(ChannelItem selectChannel) {
        this.selectChannel = selectChannel;
        //Log.d("Seven", "setSelectChannel " + selectChannel);
        notifyDataSetChanged();
    }

    public ChannelItem getSelectChannel() {
        return selectChannel;
    }

    public void setFirstUnSupportChannel(ChannelItem firstUnSupportChannel) {
        this.firstUnSupportChannel = firstUnSupportChannel;
    }

    @Override
    protected void convert(BaseViewHolder helper, ChannelItem item) {
        helper.setVisible(R.id.ll_transferred_time, type == PayChannelFragment.TYPE_RECEIPT);
        helper.setVisible(R.id.ll_tips, type == PayChannelFragment.TYPE_RECEIPT && !item.getWasInTime());
        if (!TextUtils.isEmpty(item.accountDate)) {
            helper.setText(R.id.tv_transferred_time, item.accountDate);
        }
        helper.setText(R.id.tv_channel_name, item.channelName);
        String charge = String.format("%.2f%%+¥%.1f/笔", item.channelRate, item.serviceCharge);
        helper.setText(R.id.tv_charge_rate, charge);
        helper.setText(R.id.tv_transfer_limit_one_day, item.dailyLimit + "");
        String limitOneTime = String.format("%s-%s", item.singleMinimum+"", item.singleLimit + "");
        helper.setText(R.id.tv_transfer_limit_one_time, limitOneTime);
        helper.setText(R.id.tv_transferring_time, item.startTime + "-" + item.endTime);
        helper.addOnClickListener(R.id.ll_supported_banks);
        helper.setVisible(R.id.tvGoAuth, item.openStatus == 0);
        if (item.openStatus == 0) {
            helper.setText(R.id.tv_auth_state, "未认证").setVisible(R.id.tv_auth_state, false);
        } else if (item.openStatus == 1) {
            helper.setText(R.id.tv_auth_state, "已认证").setVisible(R.id.tv_auth_state, true);
        } else if (item.openStatus == -1){
            helper.setVisible(R.id.tv_auth_state, false);
        } else {
            helper.setText(R.id.tv_auth_state, "不支持").setVisible(R.id.tv_auth_state, true);
        }
        if (selectChannel == item) {
            helper.setBackgroundRes(R.id.llChannelContent, R.drawable.bg_pay_channel_selected);
        } else {
            helper.setBackgroundColor(R.id.llChannelContent, Color.WHITE);
        }

        helper.setVisible(R.id.tvUnSupportHeader, firstUnSupportChannel == item);
        // 支持和不支持label的颜色设置
        if (item.isSupport()) {
            int supportColor = helper.itemView.getContext().getResources().getColor(R.color.color_656565);
            int supportColor2 = helper.itemView.getContext().getResources().getColor(R.color.text_black);
            int supportColor3 = helper.itemView.getContext().getResources().getColor(R.color.text_gray);
            helper.setTextColor(R.id.tvLabel1, supportColor);
            helper.setTextColor(R.id.tvLabel2, supportColor);
            helper.setTextColor(R.id.tvLabel3, supportColor);
            helper.setTextColor(R.id.tvLabel4, supportColor);
            helper.setTextColor(R.id.tvLabel5, supportColor);
            helper.setTextColor(R.id.tvLabel6, supportColor);
            helper.setTextColor(R.id.tv_channel_name, supportColor2);
            helper.setTextColor(R.id.tv_auth_state, supportColor3);
            helper.setTextColor(R.id.tv_charge_rate, supportColor3);
            helper.setTextColor(R.id.tv_transfer_limit_one_day, supportColor3);
            helper.setTextColor(R.id.tv_transfer_limit_one_time, supportColor3);
            helper.setTextColor(R.id.tv_transfer_limit_one_time, supportColor3);
            helper.setTextColor(R.id.tv_transferred_time, supportColor3);
            helper.setTextColor(R.id.tv_transferring_time, supportColor3);
        } else {
            int unsupportColor = helper.itemView.getContext().getResources().getColor(R.color.color_cbcbcb);
            helper.setTextColor(R.id.tvLabel1, unsupportColor);
            helper.setTextColor(R.id.tvLabel2, unsupportColor);
            helper.setTextColor(R.id.tvLabel3, unsupportColor);
            helper.setTextColor(R.id.tvLabel4, unsupportColor);
            helper.setTextColor(R.id.tvLabel5, unsupportColor);
            helper.setTextColor(R.id.tvLabel6, unsupportColor);
            helper.setTextColor(R.id.tv_channel_name, unsupportColor);
            helper.setTextColor(R.id.tv_channel_name, unsupportColor);
            helper.setTextColor(R.id.tv_auth_state, unsupportColor);
            helper.setTextColor(R.id.tv_charge_rate, unsupportColor);
            helper.setTextColor(R.id.tv_transfer_limit_one_day, unsupportColor);
            helper.setTextColor(R.id.tv_transfer_limit_one_time, unsupportColor);
            helper.setTextColor(R.id.tv_transferred_time, unsupportColor);
            helper.setTextColor(R.id.tv_transferring_time, unsupportColor);
        }
    }
}
