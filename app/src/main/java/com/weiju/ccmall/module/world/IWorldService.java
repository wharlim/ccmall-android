package com.weiju.ccmall.module.world;

import com.weiju.ccmall.module.world.entity.CatgoryEntity;
import com.weiju.ccmall.module.world.entity.CatgoryItemEntity;
import com.weiju.ccmall.module.world.entity.CreateWxPayEntity;
import com.weiju.ccmall.module.world.entity.FreightEntity;
import com.weiju.ccmall.module.world.entity.OrderEntity;
import com.weiju.ccmall.module.world.entity.OrderRequestBodyEntity;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author Ben
 * @date 2020/4/24.
 */
public interface IWorldService {

    @GET("world/getCatgory")
    Observable<RequestResult<CatgoryEntity>> getCatgory();

    @GET("world/searchGoods")
    Observable<RequestResult<PaginationEntity<CatgoryItemEntity, Object>>> getGoods(@Query("categoryId") String categoryId,
                                                                                    @Query("pageOffset") int page, @Query("pageSize") int pageSize);

    @GET("world/searchGoods")
    Observable<RequestResult<PaginationEntity<CatgoryItemEntity, Object>>> searchGoods(@Query("searchKey") String searchKey,
                                                                                       @Query("pageOffset") int page, @Query("pageSize") int pageSize);

    @GET("world/getGoodsDetail")
    Observable<RequestResult<CatgoryItemEntity>> getGoodsDetail(@Query("itemId") String itemId);

    /**
     * @param status 所有不传 0已关闭，1待付款 ，2待审核，3，待发货，，4，已发货，5，已完成
     */
    @GET("world/getOrderList")
    Observable<RequestResult<PaginationEntity<OrderEntity, Object>>> getOrderList(@Query("status") String status,
                                                                                        @Query("pageOffset") int page, @Query("pageSize") int pageSize);

    @GET("world/getOrderList")
    Observable<RequestResult<PaginationEntity<OrderEntity, Object>>> getAllOrderList(@Query("pageOffset") int page, @Query("pageSize") int pageSize);

    @GET("world/getPostFee")
    Observable<RequestResult<FreightEntity>> getPostFee(@Query("addressId") String addressId, @Query("itemId") String itemId, @Query("quantity") int quantity);

    @POST("world/addOrder")
    Observable<RequestResult<OrderEntity>> addOrder(@Body OrderRequestBodyEntity requestBody);

    @FormUrlEncoded
    @POST("pay/createWebhookPay")
    Observable<RequestResult<CreateWxPayEntity>> createWebhookPay(@Field("merchantOrderNo") String merchantOrderNo,
                                                                    @Field("token") String token);

    @GET("world/getOrderDetal")
    Observable<RequestResult<OrderEntity>> getOrderDetail(@Query("merchantOrderNo") String merchantOrderNo);

    @FormUrlEncoded
    @POST("world/received")
    Observable<RequestResult<Object>> received(@Field("merchantOrderNo") String merchantOrderNo);

}
