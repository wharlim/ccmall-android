package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.RegexUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.entity.ApplyHeadEntity;
import com.weiju.ccmall.module.live.entity.ApplyInfoEntity;
import com.weiju.ccmall.module.live.entity.ShopApplyEntity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Address;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.RegionSelectDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.contracts.IRegion;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProviderApplyFirstActivity extends BaseActivity {

    @BindView(R.id.etShopName)
    EditText mEtShopName;
    @BindView(R.id.regionTv)
    TextView mRegionTv;
    @BindView(R.id.regionLayout)
    LinearLayout mRegionLayout;
    @BindView(R.id.etDetailedAddress)
    EditText mEtDetailedAddress;
    @BindView(R.id.etPhone)
    EditText mEtPhone;
    @BindView(R.id.etEmail)
    EditText mEtEmail;
    @BindView(R.id.tvNext)
    TextView mTvNext;
    private Address mAddress = new Address();

    ILiveStoreService iLiveStoreService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_apply_first);
        ButterKnife.bind(this);

        setTitle("入驻申请");
        setLeftBlack();
        iLiveStoreService = ServiceManager.getInstance().createService(ILiveStoreService.class);
        initView();
    }

    private void initView() {
        String mchInId = getIntent().getStringExtra("mchInId");
        if (!TextUtils.isEmpty(mchInId)) {
            APIManager.startRequest(iLiveStoreService.getApplyInfo(mchInId),
                    new BaseRequestListener<ApplyHeadEntity>(this) {
                        @Override
                        public void onSuccess(ApplyHeadEntity resultHead) {
                            super.onSuccess(resultHead);
                            ApplyInfoEntity result = resultHead.applyBean;
                            mEtShopName.setText(result.storeName);
                            mAddress.provinceName = result.storeBusinessProvince;
                            mAddress.cityName = result.storeBusinessCity;
                            mAddress.districtName = result.storeBusinessArea;
                            mRegionTv.setText(String.format("%s%s%s", result.storeBusinessProvince, result.storeBusinessCity, result.storeBusinessArea));
                            mEtDetailedAddress.setText(result.storeBusinessAddr);
                            mEtPhone.setText(result.phone);
                            mEtEmail.setText(result.mail);
                        }
                    }, this);
        } else {
            APIManager.startRequest(iLiveStoreService.checkStoreExist(),
                    new BaseRequestListener<ApplyInfoEntity>(this) {
                        @Override
                        public void onSuccess(ApplyInfoEntity result) {
                            super.onSuccess(result);
                            if (result.existFlag == 0) {
                                mEtShopName.setText(result.storeName);
                                mEtShopName.setSelection(result.storeName.length());
                            }
                        }
                    }, this);
        }
    }

    @OnClick(R.id.regionLayout)
    protected void showRegionSelector() {
        new RegionSelectDialog(this, regions -> {
            IRegion province = regions.get("province");
            if (province != null) {
                mAddress.provinceId = province.getId();
                mAddress.provinceName = province.getName();
            }
            IRegion city = regions.get("city");
            if (city != null) {
                mAddress.cityId = city.getId();
                mAddress.cityName = city.getName();
            }
            IRegion distinct = regions.get("distinct");
            if (distinct != null) {
                mAddress.districtId = distinct.getId();
                mAddress.districtName = distinct.getName();
            }
            mRegionTv.setText(mAddress.getFullRegion());
        }).show();
    }

    @OnClick(R.id.tvNext)
    protected void onNext() {

        String shopName = mEtShopName.getText().toString();
        if (shopName.isEmpty()) {
            ToastUtil.error("店铺名称不能为空");
            return;
        }

        String provinceName = mAddress.provinceName;
        String cityName = mAddress.cityName;
        String districtName = mAddress.districtName;

        if (StringUtils.isEmpty(provinceName) || StringUtils.isEmpty(cityName) || StringUtils.isEmpty(districtName)) {
            ToastUtil.error("请选择地区");
            return;
        }

        String detailAddr = mEtDetailedAddress.getText().toString();
        if (detailAddr.isEmpty()) {
            ToastUtil.error("请输入详细地址");
            return;
        }

        String phone = mEtPhone.getText().toString();
        if (phone.isEmpty()) {
            ToastUtil.error("联系电话不能为空");
            return;
        }
        if (!RegexUtils.isMobileSimple(phone)) {
            ToastUtil.error("请检查联系电话格式");
            return;
        }
        String email = mEtEmail.getText().toString();
        if (email.isEmpty()) {
            ToastUtil.error("电子邮箱不能为空");
            return;
        }

        if (!RegexUtils.isEmail(email)) {
            ToastUtil.error("请检查电子邮箱格式");
            return;
        }

        APIManager.startRequest(iLiveStoreService.checkApplyField(shopName, provinceName, cityName, districtName, detailAddr, phone, email),
                new BaseRequestListener<Object>(this) {
                    @Override
                    public void onSuccess(Object result) {
                        super.onSuccess(result);
                        ShopApplyEntity shopApplyEntity = new ShopApplyEntity();
                        shopApplyEntity.setShopName(shopName);
                        shopApplyEntity.setProvinceName(provinceName);
                        shopApplyEntity.setCityName(cityName);
                        shopApplyEntity.setDistrictName(districtName);
                        shopApplyEntity.setAddr(detailAddr);
                        shopApplyEntity.setPhone(phone);
                        shopApplyEntity.setMail(email);
                        ProviderApplySecondActivity.start(ProviderApplyFirstActivity.this, shopApplyEntity);
                    }
                }, this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void signApply(EventMessage message) {
        if (message.getEvent().equals(Event.signApplySuccess)) {
            finish();
        }
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, ProviderApplyFirstActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, String mchInId) {
        Intent intent = new Intent(context, ProviderApplyFirstActivity.class);
        intent.putExtra("mchInId", mchInId);
        context.startActivity(intent);
    }

}
