package com.weiju.ccmall.module.shop.views;

import android.content.Context;

import com.weiju.ccmall.MyApplication;

public class Utils {

    public static int dpToPx(Context context, float dpValue) {//dp转换为px
        float scale = context.getResources().getDisplayMetrics().density;//获得当前屏幕密度
        return (int) (dpValue * scale + 0.5f);
    }

    public static int dpToPx(float dpValue) {//dp转换为px
        float scale = MyApplication.getContext().getResources().getDisplayMetrics().density;//获得当前屏幕密度
        return (int) (dpValue * scale + 0.5f);
    }
}
