package com.weiju.ccmall.module.jkp;

import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.alibaba.baichuan.android.trade.AlibcTrade;
import com.alibaba.baichuan.android.trade.callback.AlibcTradeCallback;
import com.alibaba.baichuan.android.trade.model.AlibcShowParams;
import com.alibaba.baichuan.android.trade.model.AlibcTaokeParams;
import com.alibaba.baichuan.android.trade.model.OpenType;
import com.alibaba.baichuan.android.trade.model.TradeResult;
import com.alibaba.baichuan.android.trade.page.AlibcPage;
import com.alibaba.baichuan.android.trade.utils.json.JSONUtils;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.AgentWebConfig;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.ShareDialog;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.just.agentweb.DefaultWebClient.SCHEME_SMS;

/**
 * @author chenyanming
 * @time 2019/4/29 on 15:04
 * @desc 集靠谱
 */
public class JiKaoPuWebFragment extends BaseFragment {

    @BindView(R.id.layoutWebview)
    FrameLayout mLayoutWebview;
    private AgentWeb mWeb;

    @BindView(R.id.barPading)
    View mBarPading;

    private ClipboardManager cm;
    private ClipData mClipData;

    private IUserService mService = ServiceManager.getInstance().createService(IUserService.class);

    private boolean isOpen = true;

    private final String url = "%sapp-enter/%s";
    private final String authAuccessCallback = "https://testjkp.create-chain.net/oauth";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_jkp_webview, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isOpen) {
            isOpen = false;
//            openWeb(String.format("%smain/home?token=%s", BuildConfig.JKP_PREFIX, SessionUtil.getInstance().getOAuthToken()));
            String openUrl = String.format(url, BuildConfig.JKP_PREFIX, SessionUtil.getInstance().getOAuthToken());
            Log.d("Seven", openUrl);
            openWeb(openUrl);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void openWeb(String url) {
        mWeb = AgentWeb.with(this)
                .setAgentWebParent(mLayoutWebview, new FrameLayout.LayoutParams(-1, -1))
                .useDefaultIndicator(R.color.red)// 使用默认进度条
                .setWebViewClient(mWebViewClient)
                .createAgentWeb()//
                .ready()
                .go(url);

        mWeb.clearWebCache();
        mWeb.getJsInterfaceHolder().addJavaObject("android", new AndroidInterface());
        initLoginToken(mWeb.getWebCreator().get());
    }

    private void initLoginToken(WebView webView) {
        HashMap<String, String> params = new HashMap<>(1);
        params.put("token", SessionUtil.getInstance().getOAuthToken());
        String jsCode = String.format("javascript:window.jkpLoginToken = %s", JSONUtils.toJson(params));
        webView.loadUrl(jsCode);

        webView.evaluateJavascript(String.format("window.updateJkpToken('%s')", SessionUtil.getInstance().getOAuthToken()), new ValueCallback<String>() {
            @Override
            public void onReceiveValue(String value) {

            }
        });
    }


    class AndroidInterface {

        @JavascriptInterface
        public void onClickTakeToken() {
            UserService.checkIsLogin(getActivity(), new UserService.IsLoginListener() {
                @Override
                public void isLogin() {
                    initLoginToken(mWeb.getWebCreator().get());
                }
            });
        }

        @JavascriptInterface
        public void onClickTakeCoupon(final String url) {
            UserService.checkIsLogin(getActivity(), new UserService.IsLoginListener() {
                @Override
                public void isLogin() {
                    AlibcShowParams alibcShowParams = new AlibcShowParams(OpenType.Native, false);
                    //淘宝协议
                    alibcShowParams.setClientType("taobao_scheme");
                    AlibcTaokeParams alibcTaokeParams = new AlibcTaokeParams("57328044", "23373400", "appisvcode");
                    alibcTaokeParams.pid = "mm_26632322_6858406_23810104";
                    alibcTaokeParams.subPid = "mm_26632322_6858406_23810104";
                    Map<String, String> exParams = new HashMap<>();

                    exParams.put("isv_code", "appisvcode");
                    exParams.put("alibaba", "阿里巴巴");

                    /**
                     * 打开电商组件, 使用默认的webview打开
                     *
                     * @param activity             必填
                     * @param tradePage            页面类型,必填，不可为null，详情见下面tradePage类型介绍
                     * @param showParams           show参数
                     * @param taokeParams          淘客参数
                     * @param trackParam           yhhpass参数
                     * @param tradeProcessCallback 交易流程的回调，必填，不允许为null；
                     * @return 0标识跳转到手淘打开了, 1标识用h5打开,-1标识出错
                     */
                    AlibcTrade.show(getActivity(), new AlibcPage(url), alibcShowParams, alibcTaokeParams, exParams, new AlibcTradeCallback() {

                        @Override
                        public void onTradeSuccess(TradeResult tradeResult) {
                            //打开电商组件，用户操作中成功信息回调。tradeResult：成功信息（结果类型：加购，支付；支付结果）
                        }

                        @Override
                        public void onFailure(int code, String msg) {
                            //打开电商组件，用户操作中错误信息回调。code：错误码；msg：错误信息
                            ToastUtil.success(msg);
                        }
                    });

                }
            });
        }

        @JavascriptInterface
        public void onClickCopy(String content) {
            copy(content);
            ToastUtil.success("复制成功");
        }

        @JavascriptInterface
        public void onClickShare(String title, String tkPwd, String img) {
            copy(title + tkPwd);

            new ShareDialog(getActivity(), title + tkPwd, img, new UMShareListener() {
                @Override
                public void onStart(SHARE_MEDIA share_media) {
                    ToastUtil.showLoading(getActivity());
                }

                @Override
                public void onResult(SHARE_MEDIA share_media) {
                    ToastUtil.success("分享成功");
                    ToastUtil.hideLoading();
                }

                @Override
                public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                    ToastUtil.error("分享出错:" + throwable.getMessage());
                    ToastUtil.hideLoading();
                }

                @Override
                public void onCancel(SHARE_MEDIA share_media) {
                    ToastUtil.hideLoading();
                }
            }).show();
        }
    }

    private void copy(String content) {
        cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
// 创建普通字符型ClipData
        mClipData = ClipData.newPlainText("Label", content);
// 将ClipData内容放到系统剪贴板里。
        cm.setPrimaryClip(mClipData);
    }

    protected WebViewClient mWebViewClient = new WebViewClient() {

        private HashMap<String, Long> timer = new HashMap<>();

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            Log.d("Seven", "onReceivedError ret " + request.getUrl() + ", " + error);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            initLoginToken(view);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                return shouldOverrideUrlLoading(view, request.getUrl() + "");
            } else {
                return false;
            }
        }


        //
        @Override
        public boolean shouldOverrideUrlLoading(final WebView view, String url) {
            //intent:// scheme的处理 如果返回false ， 则交给 DefaultWebClient 处理 ， 默认会打开该Activity  ， 如果Activity不存在则跳到应用市场上去.  true 表示拦截
            //例如优酷视频播放 ，intent://play?...package=com.youku.phone;end;
            //优酷想唤起自己应用播放该视频 ， 下面拦截地址返回 true  则会在应用内 H5 播放 ，禁止优酷唤起播放该视频， 如果返回 false ， DefaultWebClient  会根据intent 协议处理 该地址 ， 首先匹配该应用存不存在 ，如果存在 ， 唤起该应用播放 ， 如果不存在 ， 则跳到应用市场下载该应用 .
            boolean ret = handleLinked(url);
            return ret;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        //2972 1483|3005 1536|2868 1785| 2889 1523| 2912 1537|2941 1628|2925 1561|2864 1669|2953 1508|2932 1693|
        //2926.1 1592.3

        //2731 1749|1234 1808|2203 1230|1648 1752|
        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            initLoginToken(view);
        }

        /*错误页回调该方法 ， 如果重写了该方法， 上面传入了布局将不会显示 ， 交由开发者实现，注意参数对齐。*/
       /* public void onMainFrameError(AgentWebUIController agentWebUIController, WebView view, int errorCode, String description, String failingUrl) {

            Log.i(TAG, "AgentWebFragment onMainFrameError");
            agentWebUIController.onMainFrameError(view,errorCode,description,failingUrl);

        }*/
        @Override
        public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
            super.onReceivedHttpError(view, request, errorResponse);

        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);

        }
    };

    private boolean handleLinked(String url) {
        if (authAuccessCallback.equals(url)) {
            getActivity().finish();
            return true;
        }
        if (url.startsWith(WebView.SCHEME_TEL)
                || url.startsWith(SCHEME_SMS)
                || url.startsWith(WebView.SCHEME_MAILTO)
                || url.startsWith(WebView.SCHEME_GEO)) {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
            } catch (ActivityNotFoundException ignored) {
                if (AgentWebConfig.DEBUG) {
                    ignored.printStackTrace();
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public void onResume() {
        super.onResume();
        ToastUtil.hideLoading();

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case setOAuthTokenSuccess:
                if (mWeb != null) {
                    initLoginToken(mWeb.getWebCreator().get());
                }
                break;

            case onKeyDown:
                if (mWeb != null) {
                    mWeb.getWebCreator().get().goBack();
                }
                break;
            default:
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onLogoutEvent(EventMessage message) {
        switch (message.getEvent()) {
            case logout:
                isOpen = true;
                break;

            default:
        }
    }

}
