package com.weiju.ccmall.module.jkp.newjkp;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.category.adapter.RoundBackgroundColorSpan;
import com.weiju.ccmall.shared.bean.Tag;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import java.util.Arrays;
import java.util.List;

/**
 * @author Ben
 * @date 2020/4/10.
 */
public class ClassifyAdapter extends BaseQuickAdapter<GoodsListByCategory.DataBean, BaseViewHolder> {
    public ClassifyAdapter(@Nullable List<GoodsListByCategory.DataBean> data) {
        super(R.layout.item_classify, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, GoodsListByCategory.DataBean product) {
        FrescoUtil.setImageSmall(helper.getView(R.id.itemThumbIv), product.itempic);
        SpannableStringBuilder ssb = new SpannableStringBuilder();
//        if (product.name != null) {
//            ssb.append(product.name);
//        }
//        setTag(ssb, product.tags);
//        helper.setText(R.id.itemTitleTv, ssb)
        helper.setText(R.id.itemTitleTv, product.itemtitle)
//                .setText(R.id.itemSalesTv, String.format("销量：%s件", product.itemsale))
                .setText(R.id.itemPriceTv, "¥"+ MoneyUtil.cleanZero(product.itemendprice))
//                .setText(R.id.itemMarkPriceTv, MoneyUtil.cleanZero(product.itemprice))
                .setText(R.id.tvCCM, String.format("¥%s元优惠券", MoneyUtil.cleanZero(product.couponmoney)));

        TextViewUtil.addThroughLine(helper.getView(R.id.itemMarkPriceTv));
//        helper.setVisible(R.id.ivBanjia, product.isBanjia() ? View.VISIBLE : View.GONE)
//                .setVisible(R.id.ivBanjia, product.type == 9 && product.newerConfig != null &&
//                        (!product.newerConfig.canBuy || product.newerConfig.locked) ? View.VISIBLE : View.GONE);
//        TextViewUtil.setNewerBuyDate(mContext, helper.getView(R.id.tvSaleTime), product.newerConfig, "%s可购买",
//                "还剩%d:%d:%d可购买", false);
    }

    public void setTag(SpannableStringBuilder title, List<Tag> tags) {
        for (Tag tag: tags) {
            title.insert(0, tag.name);
            int start = 0;
            int end = tag.name.length();
            //稍微设置标签文字小一点
            title.setSpan(new RelativeSizeSpan(0.9f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            //设置圆角背景
            title.setSpan(new RoundBackgroundColorSpan(mContext.getResources().getColor(R.color.red), Color.WHITE), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
    }

    public void setTag(LinearLayout layout, List<Tag> tags) {
        int padding = ConvertUtil.dip2px(3);

        int dividers = SizeUtils.dp2px(5);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, dividers, 0);

        layout.removeAllViews();
        if (tags == null) {
            Tag tag = new Tag();
            tag.name = "  ";
            tags = Arrays.asList(tag);
        }

//            if (tags != null) {
        int length = 0;
        for (Tag tag : tags) {
            TextView textView = new TextView(mContext);
            textView.setLayoutParams(layoutParams);
            textView.setTextSize(12);
            textView.setTextColor(mContext.getResources().getColor(R.color.white));
            textView.setBackgroundResource(R.drawable.btn_bg_red);
            textView.setText(tag.name);
            textView.setMaxLines(1);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            textView.setPadding(padding, 0, padding, 0);

            length += tag.name.length();
            if (length > 10) {
                break;
            }

            layout.addView(textView);
//                }
        }
    }
}
