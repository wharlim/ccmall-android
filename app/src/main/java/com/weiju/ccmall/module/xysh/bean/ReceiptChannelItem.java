package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

public class ReceiptChannelItem {

    /**
     * payChannelId : 20
     * channelName : 收款通道Q
     * recommend : 0
     * clearingForm : S0秒到
     * remind : 支持绝大多数银行,含招商银行,兴业银行,邮政银行,交通银行,浦发银行,光大银行等
     * createDate : 1576915635000
     * channelStatus : 1
     * upgradeDecrease : 50000
     * minimum : 200
     * maxmum : 50000
     * startTime : 09:00
     * endTime : 21:00
     * rate : 0.0068
     * charge : 2.0
     * remark : 收款通道Q
     * hasRegisterUrl : http://120.77.158.195:8090/yptpay/exist
     * registerUrl :
     * payUrl : http://120.78.145.66/yizhihuan/#/YPTCash?channelId=14&rate=0.0068&serviceCharge=2
     * testStatus : 1
     * agencyRate : 0.0059
     * agencyCharge : 1.0
     * channelCode : GftQPay
     * channelCcmRate : 1.3
     */

    @SerializedName("payChannelId")
    public String payChannelId;
    @SerializedName("channelName")
    public String channelName;
    @SerializedName("recommend")
    public int recommend;
    @SerializedName("clearingForm")
    public String clearingForm;
    @SerializedName("remind")
    public String remind;
    @SerializedName("createDate")
    public long createDate;
    @SerializedName("channelStatus")
    public int channelStatus;
    @SerializedName("upgradeDecrease")
    public int upgradeDecrease;
    @SerializedName("minimum")
    public int minimum;
    @SerializedName("maxmum")
    public int maxmum;
    @SerializedName("startTime")
    public String startTime;
    @SerializedName("endTime")
    public String endTime;
    @SerializedName("rate")
    public double rate;
    @SerializedName("charge")
    public float charge;
    @SerializedName("remark")
    public String remark;
    @SerializedName("hasRegisterUrl")
    public String hasRegisterUrl;
    @SerializedName("registerUrl")
    public String registerUrl;
    @SerializedName("payUrl")
    public String payUrl;
    @SerializedName("testStatus")
    public int testStatus;
    @SerializedName("agencyRate")
    public double agencyRate;
    @SerializedName("agencyCharge")
    public double agencyCharge;
    @SerializedName("channelCode")
    public String channelCode;
    @SerializedName("channelCcmRate")
    public double channelCcmRate;
    @SerializedName("accountDate")
    public String accountDate;
    /**
     * channelShortName : GftQPay
     * h5Verification : 0
     */

    @SerializedName("channelShortName")
    public String channelShortName;
    @SerializedName("h5Verification")
    public int h5Verification;  //1是新通道
    @SerializedName("openStatus")
    public int openStatus;
    @SerializedName("authCodeStatus")
    public int authCodeStatus;

    public ChannelItem toChannelItem() {
        ChannelItem o = new ChannelItem();
        o.channelShortName = channelCode;
        o.channelRate = rate*100;
        o.channelName = channelName;
        o.singleMinimum = minimum;
        o.singleLimit = maxmum;
        o.dailyLimit = upgradeDecrease;
        o.serviceCharge = charge;
        o.startTime = startTime;
        o.endTime = endTime;
        o.openStatus = openStatus;
        o.authCodeStatus = authCodeStatus; // 收款通道所有都需要短信认证
        o.accountDate = accountDate;
        o.channelShortName = channelShortName;
        o.h5Verification = h5Verification;
        return o;
    }
}
