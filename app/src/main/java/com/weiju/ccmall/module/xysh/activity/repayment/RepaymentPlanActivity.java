package com.weiju.ccmall.module.xysh.activity.repayment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.IntelligenceRepaymentActivity;
import com.weiju.ccmall.module.xysh.bean.PlanCreateItem;
import com.weiju.ccmall.module.xysh.bean.PlanDetail;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.module.xysh.helper.PayBackPlanCreateHelper;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * 还款计划详情
 */
public class RepaymentPlanActivity extends BaseActivity {

    @BindView(R.id.ivBankCardIcon)
    SimpleDraweeView ivBankCardIcon;
    @BindView(R.id.tvCardNameAndNo)
    TextView tvCardNameAndNo;
    @BindView(R.id.tvPlanTime)
    TextView tvPlanTime;
    @BindView(R.id.tvPlanType)
    TextView tvPlanType;
    @BindView(R.id.tvTotalRepayMoney)
    TextView tvTotalRepayMoney;
    @BindView(R.id.tvTotalCharge)
    TextView tvTotalCharge;
    @BindView(R.id.tvStartMoney)
    TextView tvStartMoney;
    @BindView(R.id.magicIndicator)
    MagicIndicator magicIndicator;
    @BindView(R.id.viewpager)
    ViewPager viewpager;
    @BindView(R.id.tvNext)
    TextView tvNext;
    @BindView(R.id.tvRefresh)
    TextView tvRefresh;
    @BindView(R.id.tvMoneyTip)
    TextView mTvMoneyTip;
    @BindView(R.id.tvDebitCard)
    TextView tvDebitCard;
    @BindView(R.id.llDebitContainer)
    LinearLayout llDebitContainer;
    @BindView(R.id.ivDebitIcon)
    ImageView ivDebitIcon;

    private boolean isPreview;
    private boolean isRunning; // 执行中
    private PlanCreateItem previewPlan;
    String planNo;
    PlanDetail planDetail;
    private QueryUserBankCardResult.BankInfListBean creditCard;
    private QueryUserBankCardResult.BankInfListBean debitCard;
    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isPreview = getIntent().getBooleanExtra("isPreview", false);
        isRunning = getIntent().getBooleanExtra("isRunning", false);
        planDetail = (PlanDetail) getIntent().getSerializableExtra("planDetail");
        creditCard = (QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("creditCard");
        debitCard = (QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("debitCard");
        setContentView(R.layout.activity_repayment_plan);
        ButterKnife.bind(this);
        if (isPreview) {
            setTitle("预览计划任务");
            previewPlan = (PlanCreateItem) getIntent().getSerializableExtra("previewPlan");
            mTvMoneyTip.setText("消费金额");
        } else {
            setTitle("计划任务");
            planNo = getIntent().getStringExtra("planNo");
            if (isRunning) {
                getHeaderLayout().setRightText("终止计划");
                getHeaderLayout().setOnRightClickListener(v -> {
                    terminatePlan();
                });
            }
        }
        setLeftBlack();
        initBankCard();
        initViewState(isPreview);
        initViewPage();
        initIndicator();
    }

    private void terminatePlan() {
        WJDialog wjDialog = new WJDialog(this);
        wjDialog.show();
        wjDialog.setTitle("提示");
        wjDialog.setContentText("确定终止该还款计划吗？");
        wjDialog.setOnConfirmListener(v -> {
            wjDialog.dismiss();
            ToastUtil.showLoading(this, true);
            APIManager.startRequest(service.terminatePayBackPlan(planNo), new Observer<XYSHCommonResult<Object>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(XYSHCommonResult<Object> ret) {
                    ToastUtil.hideLoading();
                    if (ret.code == 0) {
                        ToastUtil.success("计划终止成功！");
                        EventBus.getDefault().post(new EventMessage(Event.deleteRepayPlan));
                        finish();
                    } else {
                        String msg = ret.message;
                        if (TextUtils.isEmpty(msg)) {
                            msg = "终止计划失败！";
                        }
                        ToastUtil.error(msg);
                    }
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });
        });
    }

    private void initViewPage() {
        viewpager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                return RepaymentOneDayFragment.newInstance(isPreview).setData(planDetail.payBackDetail.get(i).datas,
                        planDetail.payBackDetail.get(i).txnDt, planNo, planDetail.payType);
            }

            @Override
            public int getCount() {
//                return plan.listTranRecords.size();
                return planDetail.payBackDetail.size();
            }
        });
    }

    private void initIndicator() {
        if (isDestroyed()) {
            return;
        }
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setLeftPadding(ConvertUtil.dip2px(20));
        commonNavigator.setRightPadding(ConvertUtil.dip2px(20));
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return planDetail.payBackDetail.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);

                titleView.setText(planDetail.payBackDetail.get(index).txnDt);
                titleView.setNormalColor(getResources().getColor(R.color.default_text_color));
                titleView.setSelectedColor(getResources().getColor(R.color.red));
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewpager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(14);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getResources().getColor(R.color.red));
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                return indicator;
            }
        });
        magicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(magicIndicator, viewpager);
    }

    private void initBankCard() {
        ivBankCardIcon.setImageResource(BankUtils.getBankIconByName(creditCard.bankName));
        tvCardNameAndNo.setText(String.format("%s(%s)", creditCard.bankName, BankUtils.cutBankCardNo(creditCard.cardNo)));
        tvPlanTime.setText(String.format("计划日期：%s~%s（%d天）", planDetail.startDt, planDetail.endDt, planDetail.payBackDetail.size()));
        if (planDetail.payType == IntelligenceRepaymentActivity.REPAY_TYPE_FULL_AUTOMATION) {
            tvPlanType.setText("自动还款");
        } else if (planDetail.payType == IntelligenceRepaymentActivity.REPAY_TYPE_SEMI_AUTOMATION) {
            tvPlanType.setText("手动还款");
        }
        tvTotalRepayMoney.setText(String.format("%.2f", planDetail.consumeAmount));
        tvTotalCharge.setText(String.format("%.2f", planDetail.feeAmt));
        tvStartMoney.setText(String.format("%.2f", planDetail.frozenAmt));
        // 初始化储蓄卡
        if (debitCard != null) {
            llDebitContainer.setVisibility(View.VISIBLE);
            ivDebitIcon.setImageResource(BankUtils.getBankIconByName(debitCard.bankName));
            tvDebitCard.setText(String.format("%s(%s)", debitCard.bankName, BankUtils.cutBankCardNo(debitCard.cardNo)));
        } else {
            llDebitContainer.setVisibility(View.GONE);
        }
    }

    private void initViewState(boolean isPreview) {
        if (isPreview) {
            tvNext.setVisibility(View.VISIBLE);
            tvRefresh.setVisibility(View.VISIBLE);
        } else {
            tvNext.setVisibility(View.GONE);
            tvRefresh.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.tvNext)
    public void onNext() {
        RepaymentPlanConfirmFragment.newInstance(creditCard.bankName, creditCard.cardNo, planDetail.frozenAmt, previewPlan.planNo)
                .show(getSupportFragmentManager());
    }

    @OnClick(R.id.tvRefresh)
    public void onRefresh() {
        ToastUtil.showLoading(this, true);
        PayBackPlanCreateHelper.reCreate(new PayBackPlanCreateHelper.CreateCallBack() {
            @Override
            public void created(PlanCreateItem ret) {
                ToastUtil.hideLoading();
                if (ret == null) {
                    return;
                }
                planDetail = PayBackPlanCreateHelper.PlanCreateItem2PlanDetail(ret, planDetail.frozenAmt, planDetail.payType);
                initBankCard();
                initViewState(isPreview);
                initViewPage();
                initIndicator();
            }
        });
    }

    /**
     * 打开还款计划详情
     *
     * @param context
     */
    public static void startForDetail(Context context, PlanDetail planDetail, QueryUserBankCardResult.BankInfListBean creditCard, String planNo, boolean isRunning) {
        addSummaryItemForEveryDay(planDetail);
        Intent activity = new Intent(context, RepaymentPlanActivity.class);
        activity.putExtra("isPreview", false);
        activity.putExtra("planDetail", planDetail);
        activity.putExtra("planNo", planNo);
        activity.putExtra("creditCard", creditCard);
        activity.putExtra("isRunning", isRunning);
        context.startActivity(activity);
    }

    public static void startForPreview(Context context, PlanCreateItem previewPlan, PlanDetail planDetail, QueryUserBankCardResult.BankInfListBean creditCard,
                                       QueryUserBankCardResult.BankInfListBean debitCard) {
        addSummaryItemForEveryDay(planDetail);
        Intent activity = new Intent(context, RepaymentPlanActivity.class);
        activity.putExtra("isPreview", true);
        activity.putExtra("previewPlan", previewPlan);
        activity.putExtra("creditCard", creditCard);
        if (debitCard != null) {
            activity.putExtra("debitCard", debitCard);
        }
        activity.putExtra("planDetail", planDetail);
        context.startActivity(activity);
    }

    private static void addSummaryItemForEveryDay(PlanDetail planDetail) {
        if (planDetail.payType == IntelligenceRepaymentActivity.REPAY_TYPE_FULL_AUTOMATION) {
            return;
        }
        for (PlanDetail.PayBackDetailBean item :
                planDetail.payBackDetail) {
            PlanDetail.PayBackDetailBean.PlanItemSummary summary =
                    new PlanDetail.PayBackDetailBean.PlanItemSummary();
            summary.repayTimeout = String.format("%s 晚上24点前", item.txnDt);
            float payMoney = 0f;
            for (PlanDetail.PayBackDetailBean.PlanItem pItem :
                    item.datas) {
                payMoney += pItem.txnAmt;
            }
            summary.payMoney = payMoney;
            item.datas.add(summary);
        }
    }
}
