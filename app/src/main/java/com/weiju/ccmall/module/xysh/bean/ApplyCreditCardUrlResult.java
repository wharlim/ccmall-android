package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

public class ApplyCreditCardUrlResult {

    /**
     * url : https://xyk.cebbank.com/cebmms/apply/ps/ud-card-list.htm?class=wxjk&pro_code=FHTG143831SZ0015ZECS&jkSta=ojk
     * tradeNo : 1216013106140938241
     */

    @SerializedName("url")
    public String url;
    @SerializedName("tradeNo")
    public String tradeNo;
}
