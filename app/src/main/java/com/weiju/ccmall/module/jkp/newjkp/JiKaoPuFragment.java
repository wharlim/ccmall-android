package com.weiju.ccmall.module.jkp.newjkp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.qcloud.tim.uikit.component.CircleImageView;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.jkp.IJkpProductService;
import com.weiju.ccmall.module.jkp.IntelligenceSearchDialog;
import com.weiju.ccmall.module.jkp.OutLinkUtils;
import com.weiju.ccmall.module.jkp.TransLevelDialog;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.search.SearchActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseObserver;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.JkpLevel;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PopupWindowManage;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.page.PageType;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/4/10.
 */
public class JiKaoPuFragment extends BaseFragment {

    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;
    @BindView(R.id.tabLayout)
    protected TabLayout mTabLayout;

    @BindView(R.id.barPadding)
    View mBarPadding;
    @BindView(R.id.searchLayout)
    TextView mSearchLayout;
    @BindView(R.id.linkToBeShopkeeper)
    LinearLayout mLinkToBeShopkeeper;
    @BindView(R.id.userAvatar)
    CircleImageView mUserAvatar;
    @BindView(R.id.ivLevel)
    ImageView mIvLevel;
    @BindView(R.id.vShadow)
    View mVShadow;

    @BindView(R.id.ivNoData)
    ImageView mIvNoData;
    @BindView(R.id.tvNoData)
    TextView mTvNoData;
    @BindView(R.id.tvGoMain)
    TextView mTvNoDataBtn;
    @BindView(R.id.layoutNodata)
    LinearLayout mLayoutNodata;

    private IJkpProductService jkpProductService = ServiceManager.getInstance().createService(IJkpProductService.class);

    private JkpLevel jkpLevel;
    private PopupWindowManage mPopupWindowManage;
    protected List<Page> pages = new ArrayList<>();
    protected List<Fragment> fragments = new ArrayList<>();
    private FragmentStatePagerAdapter fragmentStatePagerAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_jikaopu, container, false);
        ButterKnife.bind(this, rootView);
        EventBus.getDefault().register(this);
        initView();
        initNoData();
        initData();
        return rootView;
    }

    private void initView() {
        //导航栏高度
        int height = QMUIStatusBarHelper.getStatusbarHeight(getContext());
        mBarPadding.getLayoutParams().height = height;

        mSearchLayout.setGravity(Gravity.CENTER);
        mUserAvatar.setVisibility(View.VISIBLE);
        loadUserIcon();
        initViewPager();
        mPopupWindowManage = PopupWindowManage.getInstance(getContext());
    }

    private void loadUserIcon() {
        if (UiUtils.checkUserLogin(getContext())) {
            User user = SessionUtil.getInstance().getLoginUser();
            Glide.with(this)
                    .load(Uri.parse(user.avatar))
                    .apply(RequestOptions.placeholderOf(R.mipmap.default_avatar))
                    .into(mUserAvatar);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && SessionUtil.getInstance().isLogin()) {
            tryToShowIntelligenceSearchDialog();
            loadUserIcon();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && UiUtils.checkUserLogin(getContext())) {
            check(SessionUtil.getInstance().getLoginUser().id);
            tryToShowIntelligenceSearchDialog();
            loadUserIcon();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void tryToShowIntelligenceSearchDialog() {
        String clipboard = OutLinkUtils.getClipboard(true);
        if (shouldCallSuperSearch(clipboard)) {
            IntelligenceSearchDialog.newInstance(clipboard).show(getChildFragmentManager(),
                    "IntelligenceSearchDialog");
        }
    }

    private boolean shouldCallSuperSearch(String clipboard) {
        String regEx="[\n`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。， 、？]";
        String extraSpecialTxt = clipboard.replaceAll(regEx, "");
//        String tkl = "[^a-zA-Z\\d].+[^a-zA-Z\\d]";
//        boolean isTkl = Pattern.matches(tkl, clipboard);
        return !TextUtils.isEmpty(clipboard) && !(clipboard.startsWith("http://")||clipboard.startsWith("https://"))
                && extraSpecialTxt.length() >= 10;
    }

    private void check(String memId) {
        APIManager.startRequest(jkpProductService.checktkLevelStatus(memId), new BaseRequestListener<JkpLevel>() {
            @Override
            public void onSuccess(JkpLevel result) {
                jkpLevel = result;
                updateLevelIcon();
            }
        }, getContext());
    }

    private void updateLevelIcon() {
        if (jkpLevel.memberTkBean != null) {
            // 绑定了信息
            // 集靠谱等级：0，超级会员；6，经销商；8，团队长
            if (jkpLevel.memberTkBean.jkpLevel >= 0 && jkpLevel.memberTkBean.jkpLevel < 6) {
                mIvLevel.setImageResource(R.drawable.ic_svip);
            } else if (jkpLevel.memberTkBean.jkpLevel >= 6 && jkpLevel.memberTkBean.jkpLevel < 8) {
                mIvLevel.setImageResource(R.drawable.ic_lv1);
            } else if (jkpLevel.memberTkBean.jkpLevel >= 8) {
                mIvLevel.setImageResource(R.drawable.ic_lv2);
            }
        }
    }

    private void initData() {
        ToastUtil.showLoading(getActivity());
        APIManager.startRequest(jkpProductService.getCategoryList(), new BaseObserver<List<CategoryList>>(getContext()) {
            @Override
            public void onHandleSuccess(List<CategoryList> categoryLists) {
                pages.clear();
                fragments.clear();
                mLayoutNodata.setVisibility(View.GONE);

                List<Page> list = new ArrayList<>();
                String id = "0";
                String mainName = "首页";
                list.add(new Page(id, mainName));
                fragments.add(ClassifyFragment.newInstance(id, mainName));

                for (CategoryList classify : categoryLists) {
                    list.add(new Page(classify.cid, classify.mainName));
                    fragments.add(ClassifyFragment.newInstance(classify.cid, classify.mainName));
                }
                pages.addAll(list);
                fragmentStatePagerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNext(RequestResult<List<CategoryList>> value) {
                super.onNext(value);
                if (value.code != 0) {
                    mLayoutNodata.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mLayoutNodata.setVisibility(View.VISIBLE);
            }

        });

    }

    private void initViewPager() {
        fragmentStatePagerAdapter = new FragmentStatePagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return pages.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return pages.get(position).name;
            }
        };
        mTabLayout.setTabIndicatorFullWidth(false);
        mViewPager.setAdapter(fragmentStatePagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
//        mViewPager.setOffscreenPageLimit(pages.size());
        mViewPager.setCurrentItem(0);
    }


    private void initNoData() {
        mIvNoData.setImageResource(R.mipmap.no_data_normal);
        mTvNoData.setText("这个页面去火星了");
        mTvNoDataBtn.setText("刷新看看");
        mTvNoDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initData();
            }
        });
    }


    @OnClick(R.id.userAvatar)
    public void userCenter() {
        if (jkpLevel == null || jkpLevel.memberTkBean == null) {
//            ToastUtil.error("您还没授权绑定淘宝!");
//            OutLinkUtils.openTaobao(getActivity(), "");
        } else {
//            showMenu();
            Intent intent = new Intent(getContext(), WebViewJavaActivity.class);
            intent.putExtra("url", BuildConfig.WECHAT_URL + "jkp/benifit/" + jkpLevel.memberTkBean.jkpLevel);
            getContext().startActivity(intent);
        }
    }

    @OnClick(R.id.searchLayout)
    protected void clickSearchLayout() {
        Intent intent = new Intent(getContext(), SearchActivity.class);
        intent.putExtra("pageType", PageType.JKP.name());
        startActivity(intent);
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        if (childFragment instanceof TransLevelDialog) {
            ((TransLevelDialog) childFragment).setOnTransferListener(() -> {
                String mId = SessionUtil.getInstance().getMemberId();
                ToastUtil.showLoading(getContext());
                APIManager.startRequest(jkpProductService.tkLevelTransform(mId), new BaseRequestListener<JkpLevel>() {
                    @Override
                    public void onSuccess(JkpLevel result) {
                        ToastUtil.success("激活成功!");
                        ToastUtil.hideLoading();
                        jkpLevel = result;
                        updateLevelIcon();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtil.hideLoading();
                    }
                }, getContext());
            });
        }
    }

    private void showMenu() {
        View view = View.inflate(getContext(), R.layout.view_trans_level, null);
        TextView tvVipLv = view.findViewById(R.id.tvVipLevel);
        tvVipLv.setOnClickListener(v -> {
            mPopupWindowManage.dismiss();
            Intent intent = new Intent(getContext(), WebViewJavaActivity.class);
            intent.putExtra("url", BuildConfig.WECHAT_URL + "jkp/benifit/" + jkpLevel.memberTkBean.jkpLevel);
//            intent.putExtra("hideToolbar", true);
            getContext().startActivity(intent);
        });
//        tvVipLv.setText("会员等级");
        View tvTransLevel = view.findViewById(R.id.tvTransLevel);
        if (jkpLevel.isUp == 1) {
            tvTransLevel.setVisibility(View.VISIBLE);
            tvTransLevel.setOnClickListener(v -> {
                mPopupWindowManage.dismiss();
                TransLevelDialog.newInstance().show(getChildFragmentManager(), "TransLevelDialog");
            });
        } else {
            tvTransLevel.setVisibility(View.GONE);
        }
        mPopupWindowManage.setYinYing(mVShadow);
        mPopupWindowManage.showWindowNormal(mUserAvatar, view, -ConvertUtil.dip2px(15));
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case logout:
            case loginSuccess:
                initData();
                break;
            default:
        }
    }

    public static JiKaoPuFragment newInstance() {
        return new JiKaoPuFragment();
    }

}
