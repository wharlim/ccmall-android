package com.weiju.ccmall.module.user;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderListActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Achievement;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.MoneyUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/11/5 on 14:54
 * @desc ${TODD}
 */
public class MyGiftActivity extends BaseActivity {

    @BindView(R.id.tvMoney)
    protected TextView mTvMoney;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_gift);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    private void initData() {
        IUserService userService = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(userService.getAchievement(), new BaseRequestListener<Achievement>() {
            @Override
            public void onSuccess(Achievement result) {
                mTvMoney.setText(MoneyUtil.centToYuanStrNoZero(result.money));
            }
        }, MyGiftActivity.this);
    }

    private void initView() {
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_white);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setTitle("我的礼包");
        mHeaderLayout.makeHeaderRed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.setStatusBarColor(getResources().getColor(R.color.red));
            QMUIStatusBarHelper.setStatusBarDarkMode(this);
        }
    }

    @OnClick(R.id.tvProduct)
    protected void product() {
        startActivity(new Intent(MyGiftActivity.this, MyGiftProductListActivity.class));
    }

    @OnClick(R.id.tvOrder)
    protected void order() {
        Intent intent = new Intent(MyGiftActivity.this, OrderListActivity.class);
        intent.putExtra("mode", OrderListActivity.MODE_MY_GIFT);
        startActivity(intent);
    }


}
