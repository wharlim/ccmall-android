package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.weiju.ccmall.module.live.fragment.GoodsManagementFragment;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseTopTabActivity;
import com.weiju.ccmall.shared.bean.event.EventMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

public class GoodsManagementActivity extends BaseTopTabActivity {

    private String[] mTitle = {"出售中", "已下架"};

    @Override
    public void initView() {
        setTitle("直播小店产品");
        setLeftBlack();
    }

    @Override
    public String[] getTitles() {
        return mTitle;
    }

    @Override
    public List<BaseFragment> getFragments() {
        ArrayList<BaseFragment> baseFragments = new ArrayList<>();
        baseFragments.add(GoodsManagementFragment.newInstance(1));
        baseFragments.add(GoodsManagementFragment.newInstance(0));
        return baseFragments;
    }

    @Override
    public void initData() {
        super.initData();
        if (mNavigatorAdapter == null) {
            GoodsManagementActivity.super.initView();
        } else {
            mNavigatorAdapter.notifyDataSetChanged();
        }

    }

    public void upDateCount(int total) {
        mTitle[0] = "出售中(" + total + ")";
        mNavigatorAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
//        if (message.getEvent().equals(Event.commentFinish)) {
//            initData();
//        }
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, GoodsManagementActivity.class);
        context.startActivity(intent);
    }
}
