package com.weiju.ccmall.module.message;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IInstantService;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.ValidateUtil;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ExchangeTicketActivity extends BaseActivity {

    @BindView(R.id.tvName)
    EditText tvName;
    @BindView(R.id.tvPhone)
    EditText tvPhone;
    @BindView(R.id.tvAddress)
    EditText tvAddress;
    private IInstantService instantService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange_ticket);
        ButterKnife.bind(this);
        initTitle();
        instantService = ServiceManager.getInstance().createService(IInstantService.class);
    }

    private void initTitle() {
        mHeaderLayout.setTitle("兑换门票");
        mHeaderLayout.setLeftDrawable(R.mipmap.icon_back_black);
        mHeaderLayout.setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @OnClick(R.id.tvConfirm)
    public void onViewClicked() {
        String name = tvName.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            ToastUtil.error("姓名不能为空!");
            tvName.requestFocus();
            return ;
        }
        String phone = tvPhone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            ToastUtil.error("电话不能为空!");
            tvPhone.requestFocus();
            return ;
        }
        if (!ValidateUtil.isPhone(phone)) {
            ToastUtil.error("手机号格式不正确");
            tvPhone.requestFocus();
            return;
        }
        String address = tvAddress.getText().toString().trim();
        if (TextUtils.isEmpty(address)) {
            ToastUtil.error("地址不能为空!");
            tvAddress.requestFocus();
            return ;
        }
        APIManager.startRequest(instantService.seckillExchangeTicket(name, phone, address), new BaseRequestListener<HashMap>() {
            @Override
            public void onSuccess(HashMap result) {
                super.onSuccess(result);
                ToastUtil.success("提交兑换信息成功！");
                finish();
            }
        }, this);
    }

    public static void start(Context context) {
        Intent activity = new Intent(context, ExchangeTicketActivity.class);
        context.startActivity(activity);
    }
}
