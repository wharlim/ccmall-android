package com.weiju.ccmall.module.world.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Ben
 * @date 2020/4/24.
 */
public class CatgoryItemEntity implements Serializable {

    /**
     * barCode : 9421903867698
     * brandName : Hello Shrek
     * categoryName : 羊奶粉
     * deliverCity : 广州
     * gpSalePrice : 1
     * itemId : CC16A375A0F840631
     * salePrice : 16000
     * skuName : Hello Shrek羊奶粉 400g/罐
     * stockQuantity : 13
     * supplyType : 一般贸易
     * tax : 0
     * gmtPutAway :
     * spuId : 0
     * brandId : 50
     * originCountryId :
     * categoryId : 1
     * nationName : 新西兰
     * mainImages :
     * primaryCategoryId :
     * primaryCategoryName :
     * secondaryCategoryId :
     * secondaryCategoryName :
     * virtualWarehouse :
     * attr :
     * retailPriceLowerLimit : 0
     * retailPriceUpperLimit : 0
     * detailImages :
     * guarantyPeriod :
     * expirationDate :
     * mainImagesUrl : https://img0.oceanz.cn/CPRODUCT_IMG/bbdbd01abaa47bc4416d283765dbec59.jpg
     * title :
     * price : 16000
     * countRate : 0
     * priceStr : 160
     */

    @SerializedName("barCode")
    public String barCode;
    @SerializedName("brandName")
    public String brandName;
    @SerializedName("categoryName")
    public String categoryName;
    @SerializedName("deliverCity")
    public String deliverCity;
    @SerializedName("gpSalePrice")
    public int gpSalePrice;
    @SerializedName("itemId")
    public String itemId;
    @SerializedName("salePrice")
    public int salePrice;
    @SerializedName("skuName")
    public String skuName;
    @SerializedName("stockQuantity")
    public int stockQuantity;
    @SerializedName("supplyType")
    public String supplyType;
    @SerializedName("tax")
    public double tax;
    @SerializedName("gmtPutAway")
    public String gmtPutAway;
    @SerializedName("spuId")
    public String spuId;
    @SerializedName("brandId")
    public String brandId;
    @SerializedName("originCountryId")
    public String originCountryId;
    @SerializedName("categoryId")
    public String categoryId;
    @SerializedName("nationName")
    public String nationName;
    @SerializedName("mainImages")
    public List<String> mainImages;
    @SerializedName("primaryCategoryId")
    public String primaryCategoryId;
    @SerializedName("primaryCategoryName")
    public String primaryCategoryName;
    @SerializedName("secondaryCategoryId")
    public String secondaryCategoryId;
    @SerializedName("secondaryCategoryName")
    public String secondaryCategoryName;
    @SerializedName("virtualWarehouse")
    public String virtualWarehouse;
    @SerializedName("attr")
    public String attr;
    @SerializedName("retailPriceLowerLimit")
    public int retailPriceLowerLimit;
    @SerializedName("retailPriceUpperLimit")
    public int retailPriceUpperLimit;
    @SerializedName("detailImages")
    public List<String> detailImages;
    @SerializedName("guarantyPeriod")
    public String guarantyPeriod;
    @SerializedName("expirationDate")
    public String expirationDate;
    @SerializedName("mainImagesUrl")
    public String mainImagesUrl;
    @SerializedName("title")
    public String title;
    @SerializedName("price")
    public long price;
    @SerializedName("countRate")
    public int countRate;
    @SerializedName("countRateExc")
    public double countRateExc;
    @SerializedName("priceStr")
    public int priceStr;

    public int quantity;

}
