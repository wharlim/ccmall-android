package com.weiju.ccmall.module.live.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Ben
 * @date 2020/5/14.
 */
public class AlipayAccountEntity implements Serializable {

    /**
     * accountId : ea1f2746cdb049e59469de08a3362e67
     * phone :
     * accountType : 2
     * bankId :
     * bankAccount :
     * bankUser :
     * bankReservedPhone :
     * alipayAccount : 15070985787
     * alipayUser : 易东英
     * wechatAccount :
     * wechatUser :
     * headImage :
     * bankName :
     * bankLogo :
     * accountStatus : 1
     * idcardFrontImg :
     * idcardBackImg :
     * idcardHeadImg :
     * bankcardFrontImg :
     * bankcardProvince :
     * bankcardCity :
     * bankcardArea :
     * bankcardAddress :
     * bankcardCode :
     * checkDate :
     * checkResult :
     * remark :
     * formatAlipayAccount : 150****5787
     */

    @SerializedName("accountId")
    public String accountId;
    @SerializedName("phone")
    public String phone;
    @SerializedName("accountType")
    public int accountType;
    @SerializedName("bankId")
    public String bankId;
    @SerializedName("bankAccount")
    public String bankAccount;
    @SerializedName("bankUser")
    public String bankUser;
    @SerializedName("bankReservedPhone")
    public String bankReservedPhone;
    @SerializedName("alipayAccount")
    public String alipayAccount;
    @SerializedName("alipayUser")
    public String alipayUser;
    @SerializedName("wechatAccount")
    public String wechatAccount;
    @SerializedName("wechatUser")
    public String wechatUser;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("bankName")
    public String bankName;
    @SerializedName("bankLogo")
    public String bankLogo;
    @SerializedName("accountStatus")
    public int accountStatus;   //绑定状态（0待审核，1绑定成功，2绑定失败，3未绑定）
    @SerializedName("idcardFrontImg")
    public String idcardFrontImg;
    @SerializedName("idcardBackImg")
    public String idcardBackImg;
    @SerializedName("idcardHeadImg")
    public String idcardHeadImg;
    @SerializedName("bankcardFrontImg")
    public String bankcardFrontImg;
    @SerializedName("bankcardProvince")
    public String bankcardProvince;
    @SerializedName("bankcardCity")
    public String bankcardCity;
    @SerializedName("bankcardArea")
    public String bankcardArea;
    @SerializedName("bankcardAddress")
    public String bankcardAddress;
    @SerializedName("bankcardCode")
    public String bankcardCode;
    @SerializedName("checkDate")
    public String checkDate;
    @SerializedName("checkResult")
    public String checkResult;
    @SerializedName("remark")
    public String remark;
    @SerializedName("formatAlipayAccount")
    public String formatAlipayAccount;

    public String totalMoney;
}
