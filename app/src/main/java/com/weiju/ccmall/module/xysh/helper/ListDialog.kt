package com.weiju.ccmall.module.xysh.helper

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.ColorDrawable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.Window

import com.weiju.ccmall.R

/**
 * Created by 林先生_lcy.
 * User: lcy
 * Date: 2019/4/3
 * Time: 下午4:37
 */
class ListDialog(context: Context) : Dialog(context) {

    init {
        setContentView(R.layout.recycler_view)
        val contentView = findViewById<ViewGroup>(android.R.id.content)
//        if (contentView != null)
//            if (contentView.childCount > 0 && contentView.getChildAt(0) != null) {
//                val childAt = contentView.getChildAt(0)
//                val s = context.resources.displayMetrics.widthPixels / 1080f
//                childAt.minimumHeight = (s * 473).toInt();
//                contentView.minimumHeight = (s * 900).toInt();
//            }
        val window = this.window
        if (window != null) {
            window.setLayout(-1, -2)
            window.setBackgroundDrawable(ColorDrawable(0))
            window.setGravity(Gravity.BOTTOM)
        }
        setCanceledOnTouchOutside(true)
    }

    fun setAdapter(adapter: RecyclerView.Adapter<*>) {
        val view = findViewById<RecyclerView>(R.id.recycler)
        val manager = LinearLayoutManager(context)
        view.layoutManager = manager
        view.adapter = adapter
    }

    fun resetHeight(size: Int?) {
        if (size == null)
            return
        val attributes = window?.attributes
        if (size > 8)
            attributes?.height = (context.resources.displayMetrics.heightPixels * 2 / 3f).toInt()
        else
            attributes?.height = -2
        window?.attributes = attributes
    }

}
