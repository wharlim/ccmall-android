package com.weiju.ccmall.module.auth.model.body;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import org.json.JSONException;

import java.util.Map;

public class AddCardBody {

    @SerializedName("bankId")
    public String bankId;
    @SerializedName("bankAccount")
    public String bankAccount;
    @SerializedName("bankUser")
    public String bankUser;
    @SerializedName("bankcardFrontImg")
    public String bankcardFrontImg;
    @SerializedName("bankcardProvince")
    public String bankcardProvince;
    @SerializedName("bankcardCity")
    public String bankcardCity;
    @SerializedName("bankcardArea")
    public String bankcardArea;
    @SerializedName("bankcardAddress")
    public String bankcardAddress;
    @SerializedName("bankReservedPhone")
    public String bankReservedPhone;
    @SerializedName("checkNumber")
    public String checkNumber;
    @SerializedName("userName")
    public String userName;
    @SerializedName("identityCard")
    public String identityCard;

    @SerializedName("name")
    public String name;
    @SerializedName("mobile")
    public String mobile;
    @SerializedName("idNumber")
    public String idNumber;
    @SerializedName("bankNum")
    public String bankNum;
    @SerializedName("bankName")
    public String bankName;
    @SerializedName("reserveMobile")
    public String reserveMobile;
    @SerializedName("token")
    public String token;
    @SerializedName("idcardFrontImg")
    public String idcardFrontImg;
    @SerializedName("idcardBackImg")
    public String idcardBackImg;
    @SerializedName("idcardHeadImg")
    public String idcardHeadImg;


    public AddCardBody(String bankId, String bankAccount, String bankUser, String bankcardFrontImg, String bankcardProvince,
                       String bankcardCity, String bankcardArea, String bankcardAddress, String bankReservedPhone,
                       String checkNumber, String userName, String identityCard,
                       String idcardFrontImg,String idcardBackImg,String idcardHeadImg,String bankName) {
        this.bankId = bankId;
        this.bankAccount = bankAccount;
        this.bankUser = bankUser;
        this.bankcardFrontImg = bankcardFrontImg;
        this.bankcardProvince = bankcardProvince;
        this.bankcardCity = bankcardCity;
        this.bankcardArea = bankcardArea;
        this.bankcardAddress = bankcardAddress;
        this.bankReservedPhone = bankReservedPhone;
        this.checkNumber = checkNumber;
        this.userName = userName;
        this.identityCard = identityCard;
        this.idcardFrontImg = idcardFrontImg;
        this.idcardBackImg = idcardBackImg;
        this.idcardHeadImg = idcardHeadImg;
//        this.bankName=bankName;
    }

    //bankcardFrontImg,bankId,bankcardAddress,bankAccount,bankcardProvince,bankcardCity,bankcardArea,checkNumber,bankReservedPhone,idcardFrontImg,idcardBackImg,idcardHeadImg,identityCard,userName
    public AddCardBody(String bankcardFrontImg, String bankId, String bankcardAddress, String bankAccount, String bankcardProvince, String bankcardCity, String bankcardArea,
                       String checkNumber, String bankReservedPhone, String idcardFrontImg, String idcardBackImg, String idcardHeadImg, String identityCard, String userName) {
        this.bankcardFrontImg = bankcardFrontImg;
        this.bankId = bankId;
        this.bankcardAddress = bankcardAddress;
        this.bankAccount = bankAccount;
        this.bankcardProvince = bankcardProvince;
        this.bankcardCity = bankcardCity;
        this.bankcardArea = bankcardArea;
        this.checkNumber = checkNumber;
        this.bankReservedPhone = bankReservedPhone;
        this.idcardFrontImg = idcardFrontImg;
        this.idcardBackImg = idcardBackImg;
        this.idcardHeadImg = idcardHeadImg;
        this.identityCard = identityCard;
        this.userName = userName;

    }

    public AddCardBody(String name, String cardName, String bankAccount, String phone, String code, String identityCard) {
        this.name = name;
        this.bankName = cardName;
        this.bankNum = bankAccount;
        this.reserveMobile = phone;
        this.mobile = code;
        this.idNumber = identityCard;
        this.token = SessionUtil.getInstance().getOAuthToken();
    }

    public Map<String, String> toMap() {
        Map<String, String> map = null;
        try {
            map = ConvertUtil.objToMap(this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return map;
    }
}
