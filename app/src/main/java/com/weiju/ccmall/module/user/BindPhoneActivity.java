package com.weiju.ccmall.module.user;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.BindPhoneMsgModel;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.component.CaptchaBtn;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.ICaptchaService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.ValidateUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class BindPhoneActivity extends BaseActivity {

    @BindView(R.id.newPhoneEt)
    EditText mNewPhoneEt;
    @BindView(R.id.captchaEt)
    EditText mCaptchaEt;
    @BindView(R.id.captchaBtn)
    CaptchaBtn mCaptchaBtn;
    @BindView(R.id.passwordEt)
    EditText mPasswordEt;
    @BindView(R.id.rePasswordEt)
    EditText mRePasswordEt;
    @BindView(R.id.bindBtn)
    TextView mBindBtn;

    private IUserService mUserService;
    private ICaptchaService mCaptchaService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_phone);
        ButterKnife.bind(this);
        initTitle();
        initService();
    }

    private void initService() {
        mCaptchaService = ServiceManager.getInstance().createService(ICaptchaService.class);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
    }

    private void initTitle() {
        showHeader("绑定手机号", true);
    }

    private Observable<RequestResult<BindPhoneMsgModel>> handingData() {
        final String phone = mNewPhoneEt.getText().toString();
        String captcha = mCaptchaEt.getText().toString();
        String password = mPasswordEt.getText().toString();
        if (Strings.isNullOrEmpty(password)) {
            ToastUtil.error("请输入登录密码");
            mPasswordEt.requestFocus();
            return null;
        }
        if (Strings.isNullOrEmpty(phone)) {
            ToastUtil.error("请输入手机号");
            mNewPhoneEt.requestFocus();
            return null;
        }
        if (!ValidateUtil.isPhone(phone)) {
            ToastUtil.error("手机号格式不正确");
            mNewPhoneEt.requestFocus();
            return null;
        }
        if (Strings.isNullOrEmpty(captcha)) {
            ToastUtil.error("请输入验证码");
            mCaptchaEt.requestFocus();
            return null;
        }
        return mUserService.firstBindPhone(phone, captcha,password);
    }

    @SuppressLint("CheckResult")
    @OnClick(R.id.captchaBtn)
    protected void getCaptcha() {
        String phone = mNewPhoneEt.getText().toString();
        if (Strings.isNullOrEmpty(phone)) {
            ToastUtil.error("请输入手机号");
            mNewPhoneEt.requestFocus();
            return;
        }
        if (!ValidateUtil.isPhone(phone)) {
            ToastUtil.error("手机号格式不正确");
            mNewPhoneEt.requestFocus();
            return;
        }
        String token = StringUtil.md5(BuildConfig.TOKEN_SALT + phone);
        mCaptchaService.getFirstPhoneMsg(token, phone)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<RequestResult<BindPhoneMsgModel>>() {
                    @Override
                    public void accept(RequestResult<BindPhoneMsgModel> result) throws Exception {
                        if (result.isSuccess()) {
                            switch (result.data.status) {
                                case 0:
                                    mCaptchaBtn.start();
                                    break;
                                case 1:
                                    ToastUtil.error(result.data.msg);
                                    break;
                                case 2:
                                    showBindRepeat();
                                    break;
                                default:
                            }
                        } else {
                            ToastUtil.error(result.message);
                        }
                    }
                });
    }

    @SuppressLint("CheckResult")
    @OnClick(R.id.bindBtn)
    protected void onBind() {
        if (handingData() == null) {
            return;
        }
        final String phone = mNewPhoneEt.getText().toString();
        handingData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<RequestResult<BindPhoneMsgModel>>() {
                    @Override
                    public void accept(RequestResult<BindPhoneMsgModel> result) throws Exception {
                        if (result.isSuccess()) {
                            switch (result.data.status) {
                                case 0:
                                    onBindSucceed();
                                    break;
                                case 1:
                                    ToastUtil.error(result.data.msg);
                                    break;
                                case 2:
                                    showBindRepeat();
                                    break;
                                default:
                            }
                        } else {
                            ToastUtil.error(result.message);
                        }
                    }

                    private void onBindSucceed() {
                        ToastUtil.success("绑定成功");
                        EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_USER_CHANGE));

                        User loginUser = SessionUtil.getInstance().getLoginUser();
                        loginUser.phone = phone;
                        SessionUtil.getInstance().setLoginUser(loginUser);
                        finish();
                    }
                });
    }

    private void showBindRepeat() {
        final WJDialog wjDialog = new WJDialog(BindPhoneActivity.this);
        wjDialog.show();
        wjDialog.setTitle("绑定手机");
        wjDialog.setContentText("该手机已注册，可直接使用手机号登录");
        wjDialog.setCancelText("取消");
        wjDialog.setConfirmText("去登录");
        wjDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wjDialog.dismiss();
                UserService.logout(BindPhoneActivity.this);
                startActivity(new Intent(BindPhoneActivity.this, LoginActivity.class));
                finish();
            }
        });
    }


}
