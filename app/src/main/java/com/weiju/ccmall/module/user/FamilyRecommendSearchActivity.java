package com.weiju.ccmall.module.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.adapter.FamilyAdapter;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Family;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FamilyRecommendSearchActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.backBtn)
    ImageView backBtn;
    @BindView(R.id.keywordEt)
    EditText keywordEt;
    @BindView(R.id.cleanBtn)
    ImageView cleanBtn;
    @BindView(R.id.recyclerView)
    RecyclerView mRvList;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mLayoutRefresh;

    private String keyWords;
    private ArrayList<Family.DatasEntity> mDatas = new ArrayList<>();
    private FamilyAdapter mAdapter = new FamilyAdapter(mDatas);
    IUserService service;
    private int curPage = 1;
    private static final int pageSize = 10;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_family_recommend);
        ButterKnife.bind(this);
        service = ServiceManager.getInstance().createService(IUserService.class);
        initView();

        keywordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                keyWords = s.toString().trim();
                onRefresh();
            }
        });
    }

    private void initView() {

        mRvList.setAdapter(mAdapter);
        mRvList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter.setEmptyView(new NoData(this));
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                loadData(curPage+1);
            }
        });
        mLayoutRefresh.setOnRefreshListener(this);
        setLayoutRefresh(mLayoutRefresh);
    }

    @OnClick({R.id.backBtn, R.id.cleanBtn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
            case R.id.cleanBtn:
                keywordEt.setText("");
                break;
        }
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, FamilyRecommendSearchActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void onRefresh() {
        loadData(1);
    }

    private void loadData(int page) {
        if (TextUtils.isEmpty(keyWords)) {
            return;
        }
        BaseRequestListener<Family> listener = new BaseRequestListener<Family>() {
            @Override
            public void onSuccess(Family result) {
                super.onSuccess(result);
                if (page == 1) {
                    mDatas.clear();
                }
                mDatas.addAll(result.datas);
                mAdapter.notifyDataSetChanged();
                curPage = page;
                mLayoutRefresh.setRefreshing(false);
                if (result.datas == null || result.datas.size() < pageSize) {
                    mAdapter.loadMoreEnd();
                } else {
                    mAdapter.loadMoreComplete();
                }
            }
        };
        APIManager.startRequest(service.searchClientList(page, pageSize, keyWords), listener, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ChangeCustomerNoteNameActivity.REQUEST_CODE && data != null) {
                String memberId = data.getStringExtra("memberId");
                String noteName = data.getStringExtra("noteName");
                for(int i = 0; i < mDatas.size(); ++i) {
                    if (Objects.equals(mDatas.get(i).memberId, memberId)) {
                        mDatas.get(i).noteName = noteName;
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }
}
