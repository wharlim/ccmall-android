package com.weiju.ccmall.module.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/6/22.
 */
public class PasswordOptionActivity extends BaseActivity {
    @BindView(R.id.tvAccount)
    TextView tvAccount;
    @BindView(R.id.tvPassword)
    TextView tvPassword;
    private boolean mIsEditPayPassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_option);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        mIsEditPayPassword = getIntent().getBooleanExtra("isEditPayPassword", false);
        setTitle(mIsEditPayPassword ? "修改支付密码" : "修改登录密码");
        setLeftBlack();
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser == null) {
            ToastUtil.error("用户未登录");
            finish();
            return;
        }
        tvAccount.setText(Html.fromHtml(String.format("您是否记得账号 <font color =\"#000000\">%s</font>", loginUser.phone)));
        tvPassword.setText(mIsEditPayPassword ? "当前使用的支付密码？" : "当前使用的登录密码？");
    }

    @OnClick({R.id.tvForget, R.id.tvRemember})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvForget:
                if (mIsEditPayPassword) {
                    ResetPayPasswordActivity.start(PasswordOptionActivity.this);
                } else {
                    SetPasswordActivity.start(PasswordOptionActivity.this, true);
                }
                finish();
                break;
            case R.id.tvRemember:
                if (mIsEditPayPassword) {
                    EditPayPasswordActivity.start(PasswordOptionActivity.this);
                } else {
                    startActivity(new Intent(PasswordOptionActivity.this, EditPasswordActivity.class));
                }
                finish();
                break;
        }
    }

    public static void start(Context context, boolean isEditPayPassword) {
        Intent intent = new Intent(context, PasswordOptionActivity.class);
        intent.putExtra("isEditPayPassword", isEditPayPassword);
        context.startActivity(intent);
    }

}
