package com.weiju.ccmall.module.cart.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;

import java.util.List;

public class CartPresentsAdapter extends BaseQuickAdapter<SkuInfo, BaseViewHolder> {

    public CartPresentsAdapter(@Nullable List<SkuInfo> data) {
        super(R.layout.item_cart_presents, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SkuInfo item) {
        helper.setText(R.id.tvName, item.name);
        helper.setText(R.id.tvQuantity, "x" + item.quantity);
    }
}
