package com.weiju.ccmall.module.auth;

import android.os.Bundle;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.google.common.base.Strings;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.model.CardDetailModel;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.StringUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

/**
 * 银行卡绑定成功
 */
public class AuthCardSuccssdGongCatActivity extends BaseActivity {
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.tvIdCard)
    TextView mTvIdCard;
    @BindView(R.id.tvBankAccount)
    TextView mTvBankAccount;
    @BindView(R.id.tvPhone)
    TextView mTvPhone;
    @BindView(R.id.tvBankName)
    TextView mTvBankName;
    private CardDetailModel mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_card_succssd_gongcat);
        ButterKnife.bind(this);
        initData();
        setTitle("绑定银行卡");
        setLeftBlack();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtils.e("回来了,清除 event");
        EventBus.getDefault().removeStickyEvent(CardDetailModel.class);
    }

    private void initData() {
        IUserService service = ServiceManager.getInstance().createService(IUserService.class);
        Observable<RequestResult<CardDetailModel>> gongCatCard = MyApplication.isGongCat?
                service.getGongCatCard(SessionUtil.getInstance().getOAuthToken()):
                service.getCard();
        APIManager.startRequest(gongCatCard, new BaseRequestListener<CardDetailModel>() {
            @Override
            public void onSuccess(CardDetailModel model) {
                mModel = model;
                setData();
            }

        },this);
    }

    private void setData() {
        mTvName.setText(mModel.name);
        mTvBankName.setText(mModel.bankName);
        String identityCard = mModel.idNumber;
        if (!StringUtils.isEmpty(identityCard)) {
            String mark = Strings.repeat("*", identityCard.length() - 2);
            mTvIdCard.setText(identityCard.substring(0, 1) + mark + identityCard.substring(identityCard.length() - 1));
        }


        String cardNumber = mModel.bankNum;
        if (cardNumber.length() > 4) {
            cardNumber = cardNumber.substring(cardNumber.length() - 4);
        }
        mTvBankAccount.setText(String.format("**** **** **** %s", cardNumber));

        mTvPhone.setText(StringUtil.maskPhone(mModel.mobile));

    }

}
