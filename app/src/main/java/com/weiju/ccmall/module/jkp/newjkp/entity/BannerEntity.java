package com.weiju.ccmall.module.jkp.newjkp.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Ben
 * @date 2020/6/1.
 */
public class BannerEntity {


    @SerializedName("bannerList")
    public List<Bean> bannerList;
    @SerializedName("relationList")
    public List<Bean> relationList;

    public static class Bean {
        /**
         * target :
         * image :
         * pageId :
         * pageName :
         */

        @SerializedName("target")
        public String target;
        @SerializedName("image")
        public String image;
        @SerializedName("pageId")
        public String pageId;
        @SerializedName("pageName")
        public String pageName;
    }
}
