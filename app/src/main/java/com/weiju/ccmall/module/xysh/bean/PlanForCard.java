package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlanForCard {

    public static class PlanForCardContainer {
        @SerializedName("data")
        public List<PlanForCard> data;
    }

    /**
     * bankName : 建设银行
     * planNo : ZXHK20200105221251159493
     * tmSmp : 1578233608000
     * tmSmpStr : 2020-01-05 00:00:00
     * planSts : 1
     * cardNo : 6253624079454375
     * payBackAmt : 200
     * payBackCnt : 1
     * payBackcntPerday : null
     * frozenAmt : 210
     * feeAmt : 3.36
     * startDt : 1579190400000
     * startDtStr : 2020-01-17 00:00:00
     * endDt : 1579190400000
     * endDtStr : 2020-01-17 00:00:00
     * planTm : 1578153600000
     * usrNo : dac68adea0f44385ae2b84e03609a285
     * cntPerday : 1
     * cnlNo : GftP3
     */

    @SerializedName("bankName")
    public String bankName;
    @SerializedName("planNo")
    public String planNo;
    @SerializedName("tmSmp")
    public long tmSmp;
    @SerializedName("tmSmpStr")
    public String tmSmpStr;
    @SerializedName("planSts")
    public String planSts;
    public boolean isRunning() {
        return "1".equals(planSts);
    }
    public boolean isSuccess() {
        return "2".equals(planSts);
    }
    public boolean isFail() {
        return "3".equals(planSts);
    }
    @SerializedName("cardNo")
    public String cardNo;
    @SerializedName("payBackAmt")
    public float payBackAmt;
//    @SerializedName("payBackCnt")
//    public String payBackCnt;
//    @SerializedName("payBackcntPerday")
//    public Object payBackcntPerday;
//    @SerializedName("frozenAmt")
//    public int frozenAmt;
//    @SerializedName("feeAmt")
//    public double feeAmt;
//    @SerializedName("startDt")
//    public long startDt;
//    @SerializedName("startDtStr")
//    public String startDtStr;
//    @SerializedName("endDt")
//    public long endDt;
//    @SerializedName("endDtStr")
//    public String endDtStr;
//    @SerializedName("planTm")
//    public long planTm;
//    @SerializedName("usrNo")
//    public String usrNo;
//    @SerializedName("cntPerday")
//    public String cntPerday;
//    @SerializedName("cnlNo")
//    public String cnlNo;
}
