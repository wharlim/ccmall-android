package com.weiju.ccmall.module.community;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class GetCourseRewardDialog extends BottomSheetDialogFragment {

    @BindView(R.id.tv_course_title)
    TextView tvCourseTitle;
    @BindView(R.id.tv_get_reward_btn)
    TextView tvGetRewardBtn;
    @BindView(R.id.ll_get_reward_container)
    LinearLayout llGetRewardContainer;
    Unbinder unbinder;
    Course course;

    ICommunityService service = ServiceManager.getInstance().createService(ICommunityService.class);

    public static GetCourseRewardDialog newInstance(Course course) {
        GetCourseRewardDialog dialog = new GetCourseRewardDialog();
        Bundle args = new Bundle();
        args.putSerializable("course", course);
        dialog.setArguments(args);
        return dialog;
    }

    public GetCourseRewardDialog show(FragmentManager fm) {
        show(fm, "GetCourseRewardDialog");
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        course = (Course) getArguments().getSerializable("course");
        View view = inflater.inflate(R.layout.dialog_get_course_reward, container, false);
        unbinder = ButterKnife.bind(this, view);
        tvCourseTitle.setText(course.title);
        if (course.hasGotReward()) {
            tvGetRewardBtn.setText("已领取");
            tvGetRewardBtn.setEnabled(false);
        }
        initRewardList();
        return view;
    }

    private void initRewardList() {
        if (course.rewordMap == null || course.rewordMap.isEmpty()) {
            return;
        }
        LayoutInflater inflater = LayoutInflater.from(getContext());
        for (Course.RewordMapItem item: course.rewordMap) {
            View itemView = inflater.inflate(R.layout.item_get_course_reward, llGetRewardContainer, false);
            TextView rewardContent = itemView.findViewById(R.id.tv_reward_content);
            TextView rewardValue = itemView.findViewById(R.id.tv_reward_value);
            rewardContent.setText("任务达成，获得" +item.key);
            rewardValue.setText(item.value);
            llGetRewardContainer.addView(itemView);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.tv_get_reward_btn)
    public void onViewClicked() {
        if (course.hasGotReward()) {
            return;
        }
        ToastUtil.showLoading(getContext(), true);
        APIManager.startRequest(service.addCourseReword(course.courseId), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                ToastUtil.success("领取成功！");
                if (getContext() instanceof CourseDetailActivity) {
                    ((CourseDetailActivity)getContext()).setGotReward();
                }
                dismiss();
            }

            @Override
            public void onComplete() {
                ToastUtil.hideLoading();
            }
        }, getContext());
    }
}
