package com.weiju.ccmall.module.splash;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.MainActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseSubscriber;
import com.weiju.ccmall.shared.bean.Splash;
import com.weiju.ccmall.shared.common.AdvancedCountdownTimer;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IAdService;
import com.weiju.ccmall.shared.util.FrescoUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.splash
 * @since 2017-08-03
 */
public class SplashActivity extends BaseActivity {

    @BindView(R.id.adIv)
    protected SimpleDraweeView mAdIv;
    @BindView(R.id.jumpBtn)
    protected TextView mJumpBtn;

    private AdvancedCountdownTimer mCountdownTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }

        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        mAdIv.setVisibility(View.GONE);
        mJumpBtn.setVisibility(View.GONE);
        IAdService adService = ServiceManager.getInstance().createService(IAdService.class);
        execute(adService.getSplashAd(), new BaseSubscriber<Splash>() {
            @Override
            public void onNext(@NonNull Splash Splash) {
                super.onNext(Splash);
                mAdIv.setVisibility(View.VISIBLE);
                mJumpBtn.setVisibility(View.VISIBLE);
                FrescoUtil.setImage(mAdIv, Splash.backUrl);
            }
        });
        startCountDown(3000L);
    }

    public void startCountDown(long time) {
        mCountdownTimer = new AdvancedCountdownTimer(time, 1000) {

            @SuppressLint("DefaultLocale")
            @Override
            public void onTick(long millisUntilFinished, int percent) {
                millisUntilFinished /= 1000;
                long seconds = millisUntilFinished % 60;
                mJumpBtn.setText(String.format("跳过(%d)", seconds));
            }

            @Override
            public void onFinish() {
                viewMainActivity();
            }
        };

        mCountdownTimer.start();
    }

    @Override
    protected void onDestroy() {
        if (mCountdownTimer != null) {
            mCountdownTimer.cancel();
            mCountdownTimer = null;
        }
        super.onDestroy();
    }

    @OnClick(R.id.jumpBtn)
    protected void viewMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
        mCountdownTimer.cancel();
    }
}
