package com.weiju.ccmall.module.order;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.TimeUtils;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.groupBuy.JoinGroupActivity;
import com.weiju.ccmall.module.order.adapter.OrderDetailSellerProductAdapter;
import com.weiju.ccmall.module.order.adapter.OrderItemAdapter;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.pay.PayMsg;
import com.weiju.ccmall.module.qrcode.ScanActivity;
import com.weiju.ccmall.module.store.ReceiveRefundGoodsActivity;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.common.ImageAdapter;
import com.weiju.ccmall.shared.component.dialog.OrderShipDialog;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.OrderService;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.CSUtils;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.PermissionsUtils;
import com.weiju.ccmall.shared.util.RvUtils;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import rx.functions.Action1;

import static com.weiju.ccmall.module.pay.PayActivity.initDeductData;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.order
 * @since 2017-07-05
 */
public class OrderDetailActivity extends BaseActivity {

    @BindView(R.id.tvScore)
    TextView mTvScore;
    @BindView(R.id.layoutScore)
    LinearLayout mLayoutScore;
    @BindView(R.id.tvRatio)
    TextView mTvRatio;
    @BindView(R.id.tvRatioPrice)
    TextView mTvRatioPrice;
    @BindView(R.id.layoutRatio)
    LinearLayout mLayoutRatio;
    @BindView(R.id.itemShit)
    TextView mItemShit;
    @BindView(R.id.tvAgressRefundGoods)
    TextView mTvAgressRefundGoods;
    @BindView(R.id.tvAgressRefundMoney)
    TextView mTvAgressRefundMoney;
    @BindView(R.id.tvReceiveRefundGodds)
    TextView mTvReceiveRefundGodds;
    @BindView(R.id.layoutConpon)
    LinearLayout mLayoutConpon;
    @BindView(R.id.itemCancelRefundMoney)
    TextView mItemCancelRefundMoney;
    @BindView(R.id.itemCancelRefundGoods)
    TextView mItemCancelRefundGoods;
    @BindView(R.id.itemCheckGroupBuy)
    TextView mItemCheckGroupBuy;
    @BindView(R.id.itemGoGroupBuy)
    TextView mItemGoGroupBuy;
    @BindView(R.id.layoutMoney)
    LinearLayout mLayoutMoney;
    @BindView(R.id.tvSellerBuyerPayMoney)
    TextView mTvSellerBuyerPayMoney;
    @BindView(R.id.rvSellerProduct1)
    RecyclerView mRvSellerProduct1;
    @BindView(R.id.tvSellerFeight1)
    TextView mTvSellerFeight1;
    @BindView(R.id.tvSellerRecevieMoney)
    TextView mTvSellerRecevieMoney;
    @BindView(R.id.rvSellerProduct2)
    RecyclerView mRvSellerProduct2;
    @BindView(R.id.tvSellerFeight2)
    TextView mTvSellerFeight2;
    @BindView(R.id.layoutSeller)
    LinearLayout mLayoutSeller;
    @BindView(R.id.tvMoneyTag)
    TextView mTvMoneyTag;
    @BindView(R.id.tvOrderExpressCode)
    TextView mTvOrderExpressCode;
    @BindView(R.id.layoutOrderExpressCode)
    LinearLayout mLayoutOrderExpressCode;
    @BindView(R.id.tvCS)
    TextView mTvCS;
    @BindView(R.id.tvCticket)
    TextView mTvCticket;
    @BindView(R.id.tvCC)
    TextView mTvCC;
    @BindView(R.id.orderInfo)
    TextView orderInfo;
    @BindView(R.id.orderInfoContainer)
    LinearLayout orderInfoContainer;
    @BindView(R.id.endTime)
    TextView endTime;
    @BindView(R.id.endTimeContainer)
    LinearLayout endTimeContainer;
    @BindView(R.id.goUseBtn)
    TextView goUseBtn;
    private IOrderService mOrderService;
    private Order mOrder;
    @BindView(R.id.statusTv)
    protected TextView mStatusTv;
    @BindView(R.id.refundGoodTipsTv)
    protected TextView mRefundGoodTipsTv;
    @BindView(R.id.orderCodeTv)
    protected TextView mOrderCodeTv;

    @BindView(R.id.addRefundGoodExpressBtn)
    protected TextView mAddRefundGoodInfoBtn;

    @BindView(R.id.refundLayout)
    protected LinearLayout mRefundLayout;
    @BindView(R.id.refundReasonLabelTv)
    protected TextView mRefundReasonLabelTv;
    @BindView(R.id.refundReasonValueTv)
    protected TextView refundReasonValueTv;

    @BindView(R.id.refundApplyMoneyLabelTv)
    protected TextView refundApplyMoneyLabelTv;
    @BindView(R.id.refundApplyMoneyValueTv)
    protected TextView refundApplyMoneyValueTv;
    @BindView(R.id.refundMoneyLayout)
    protected LinearLayout mRefundMoneyLayout;
    @BindView(R.id.refundMoneyLabelTv)
    protected TextView refundMoneyLabelTv;
    @BindView(R.id.refundMoneyValueTv)
    protected TextView refundMoneyValueTv;

    @BindView(R.id.refundRemarkLayout)
    protected LinearLayout refundRemarkLayout;
    @BindView(R.id.refundRemarkLabelTv)
    protected TextView refundRemarkLabelTv;
    @BindView(R.id.refundRemarkValueTv)
    protected TextView refundRemarkValueTv;
    @BindView(R.id.imageRecyclerView)
    protected RecyclerView mImageRecyclerView;
    @BindView(R.id.refundExpressCompanyLayout)
    protected LinearLayout refundExpressCompanyLayout;
    @BindView(R.id.refundExpressCompanyValueTv)
    protected TextView refundExpressCompanyValueTv;
    @BindView(R.id.refundExpressCodeLayout)
    protected LinearLayout refundExpressCodeLayout;
    @BindView(R.id.refundExpressCodeValueTv)
    protected TextView refundExpressCodeValueTv;

    @BindView(R.id.phoneTv)
    protected TextView mPhoneTv;
    @BindView(R.id.contactsTv)
    protected TextView mContactsTv;
    @BindView(R.id.addressDetailTv)
    protected TextView mAddressDetailTv;
    @BindView(R.id.storeNameTv)
    protected TextView mStoreNameTv;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.buyerRemarkLayout)
    protected LinearLayout mBuyerRemarkLayout;
    @BindView(R.id.buyerRemarkTv)
    protected TextView mBuyerRemarkTv;
    @BindView(R.id.sellerRemarkLayout)
    protected LinearLayout mSellerRemarkLayout;
    @BindView(R.id.sellerRemarkTv)
    protected TextView mSellerRemarkTv;

    @BindView(R.id.createDateTv)
    protected TextView mCreateDateTv;
    @BindView(R.id.payWayTv)
    protected TextView mPayWayTv;
    @BindView(R.id.productTotalTv)
    protected TextView mProductTotalTv;
    @BindView(R.id.freightTv)
    protected TextView mFreightTv;
    @BindView(R.id.couponTv)
    protected TextView mCouponTv;
    @BindView(R.id.payMoneyTv)
    protected TextView mPayMoneyTv;

    @BindView(R.id.orderBottomLayout)
    protected LinearLayout mOrderBottomLayout;

    @BindView(R.id.itemCancelBtn)
    protected TextView mCancelBtn;
    @BindView(R.id.itemPayBtn)
    protected TextView mPayBtn;
    @BindView(R.id.itemApplyRefundMoneyBtn)
    protected TextView mApplyRefundMoneyBtn;
    @BindView(R.id.itemApplyRefundGoodsBtn)
    protected TextView mApplyRefundGoodsBtn;
    @BindView(R.id.itemViewExpressBtn)
    protected TextView mViewExpressBtn;
    @BindView(R.id.orderFinishBtn)
    protected TextView mOrderFinishBtn;
    @BindView(R.id.goldenMemberInfoLayout)
    LinearLayout mGoldenMemberInfoLayout;
    @BindView(R.id.tvGoldenMemberInfo)
    TextView mTvGoldenMemberInfo;
    @BindView(R.id.tvGoldenNum)
    TextView mTvGoldenNum;
    @BindView(R.id.tvGoldenTicket)
    TextView mTvGoldenTicket;
    @BindView(R.id.itemGoSuperGroupBuy)
    TextView mItemGoSuperGroupBuy;
    @BindView(R.id.viewWeight)
    View mViewWeight;

    @BindView(R.id.ll_my_sell)
    LinearLayout ll_my_sell;

    @BindView(R.id.tv_client_pay)
    TextView tv_client_pay;

    @BindView(R.id.tv_client_pay_number)
    TextView tv_client_pay_number;

    @BindView(R.id.tv_me_pay)
    TextView tv_me_pay;

    @BindView(R.id.tv_me_pay_number)
    TextView tv_me_pay_number;

    @BindView(R.id.tvCC1)
    TextView tvCC1;

    @BindView(R.id.tv_timer)
    TextView tv_timer;

    @BindView(R.id.rl_up_user)
    RelativeLayout rl_up_user;

    @BindView(R.id.tv_up_name)
    TextView tv_up_name;

    @BindView(R.id.tv_confirmUpgrade)
    TextView tv_confirmUpgrade;

    @BindView(R.id.tv_thaw)
    TextView tv_thaw;

    @BindView(R.id.deductLayout)
    LinearLayout deductLayout;

    @BindView(R.id.deductTv)
    TextView deductTv;

    @BindView(R.id.llGoldenTicket)
    LinearLayout llGoldenTicket;

    @BindView(R.id.rlAddressDetail)
    RelativeLayout rlAddressDetail;

    private CountDownTimer mTimer;

    private int mMode;
    private String mOrderCode;

    IOrderService orderService = ServiceManager.getInstance().createService(IOrderService.class);

    /**
     * 礼包发货弹窗
     */
    private OrderShipDialog mOrderShipDialog;
    private int mProductType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);
        ButterKnife.bind(this);
        showHeader();
        setTitle("订单详情");
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_black);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            mOrderCode = intent.getExtras().getString("orderCode");
        }
        if (mOrderCode == null || mOrderCode.isEmpty()) {
            ToastUtil.error("参数错误");
            finish();
            return;
        }
        mMode = intent.getIntExtra("mode", 0);
        loadOrderDetail(mOrderCode);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadOrderDetail(mOrderCode);
    }

    private void initSellerBottomButtons() {
        switch (mOrder.orderMain.status) {
            case AppTypes.ORDER.STATUS_SELLER_WAIT_SHIP:
                mItemShit.setVisibility(View.VISIBLE);
                break;
            case AppTypes.ORDER.STATUS_SELLER_HAS_SHIP:
                mViewExpressBtn.setVisibility(View.VISIBLE);
                break;
            default:
                mOrderBottomLayout.setVisibility(View.GONE);
                break;
        }
    }

    // 是否显示去使用
    public static boolean showGoToUse(Order mOrder) {
        return  orderType11(mOrder) || (
                !TextUtils.isEmpty(mOrder.virOrder.orderNo) &&
                mOrder.virOrder.status != 3 &&
                (mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP ||
                        mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_SHIP ||
                        mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED)
                );
    }

    private static boolean orderType11(Order mOrder) {
        return mOrder.orderMain.orderType == 11 &&
                (mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP ||
                mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_SHIP ||
                mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED);
    }

    /**
     * 我的礼包底部按钮
     */
    private void initGiftBottomButtons() {
        for (int i = 0; i < mOrderBottomLayout.getChildCount(); i++) {
            mOrderBottomLayout.getChildAt(i).setVisibility(View.GONE);
        }
        mViewWeight.setVisibility(View.VISIBLE);
        switch (mOrder.orderMain.status) {
            case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP:
                mItemShit.setVisibility(View.VISIBLE);
                break;
            case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP:
                // mApplyRefundGoodsBtn.setVisibility(View.VISIBLE);
                mViewExpressBtn.setVisibility(View.VISIBLE);
                break;
            default:
                mOrderBottomLayout.setVisibility(View.GONE);
        }
    }

    private void initBottomButtons() {
        if (mMode == OrderListActivity.MODE_MY_GIFT) {
            initGiftBottomButtons();
            return;
        }
        if (mOrder.orderMain.status > 0 && mOrder.orderMain.status < 7) {
            mOrderBottomLayout.setVisibility(View.VISIBLE);
            for (int i = 0; i < mOrderBottomLayout.getChildCount(); i++) {
                mOrderBottomLayout.getChildAt(i).setVisibility(View.GONE);
            }
            mViewWeight.setVisibility(View.VISIBLE);
            if (mOrder.isGroupOrder()) {
                mItemCheckGroupBuy.setVisibility(View.VISIBLE);
            }
            switch (mOrder.orderMain.status) {
                case AppTypes.ORDER.STATUS_BUYER_WAIT_PAY:
                    mPayBtn.setVisibility(View.VISIBLE);
                    mCancelBtn.setVisibility(View.VISIBLE);
                    break;
                case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP:
                    if (mOrder.isShowGroupOrderStatus()) {
                        mItemCheckGroupBuy.setVisibility(View.GONE);
                        mItemGoGroupBuy.setVisibility(View.VISIBLE);
                    } else if (mOrder.orderMain.orderType == 2) {
                        mItemGoSuperGroupBuy.setVisibility(View.VISIBLE);
                    } else if (!mOrder.isGroupOrder()) {
//                        mApplyRefundMoneyBtn.setVisibility(View.VISIBLE);
                        mOrderBottomLayout.setVisibility(View.GONE);
                    }
                    if (mProductType == 28){
                        mOrderBottomLayout.setVisibility(View.VISIBLE);
                        mApplyRefundMoneyBtn.setVisibility(View.VISIBLE);
                    }
                    break;
                case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP:
                    if (mProductType == 28){
                        mApplyRefundGoodsBtn.setVisibility(View.VISIBLE);
                    }
                    // mApplyRefundGoodsBtn.setVisibility(View.VISIBLE);
                    mViewExpressBtn.setVisibility(View.VISIBLE);
                    mOrderFinishBtn.setVisibility(View.VISIBLE);
                    mItemCheckGroupBuy.setVisibility(View.GONE);
                    mItemGoSuperGroupBuy.setVisibility(mOrder.orderMain.orderType == 2 ? View.VISIBLE : View.GONE);
                    break;
//                case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED:
//                    mApplyRefundGoodsBtn.setVisibility(View.VISIBLE);
//                    break;
                case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED:
                    mItemGoSuperGroupBuy.setVisibility(mOrder.orderMain.orderType == 2 ? View.VISIBLE : View.GONE);
                    mOrderBottomLayout.setVisibility(mOrder.orderMain.orderType == 2 ? View.VISIBLE : View.GONE);
                    break;
                default:
                    mOrderBottomLayout.setVisibility(View.GONE);
                    break;
            }
        } else {
            mOrderBottomLayout.setVisibility(View.GONE);
        }
    }

    private void loadOrderDetail(String orderCode) {
        String userType = "";
        if (mMode == 3) {
            userType = "1";
        } else if (mMode == 2) {
            userType = "0";
        }
        Observable<RequestResult<Order>> orderByCode = mOrderService.getOrderByCode(orderCode, userType);
        if (mMode == OrderListActivity.MODE_LIVE_STORE) orderByCode = mOrderService.orderDetail(orderCode);
        APIManager.startRequest(orderByCode, new BaseRequestListener<Order>(this) {
            @Override
            public void onSuccess(Order result) {
                mOrder = result;
                String pay199 = getSharedPreferences(Const.PAY_199, 0).getString(Const.PAY_199, "");
                if (Const.PAY_199.equals(pay199)) {
                    mMode = 4;
                    getSharedPreferences(Const.PAY_199, 0).edit().putString(Const.PAY_199, "").commit();
                }
                initOrderProducts();
                initOrderBase();
                if (mMode == OrderListActivity.MODE_LIVE_STORE) {//直播店铺订单详情
                    initSellerBottomButtons();
                    initLiveStoreView();
                    mTvCS.setVisibility(View.VISIBLE);
                } else if (mMode == OrderListActivity.MODE_SELLER) {
                    initSellerBottomButtons();
                    initSellerRefundViews();
                    initRefundViews();
                    initSellerViews();
                } else if (0 == mMode || mMode == OrderListActivity.MODE_MY_GIFT) {
                    initRefundViews();
                    initBottomButtons();
                    initVirOrder();
                    mTvCS.setVisibility(View.VISIBLE);
                } else if (mMode == 2) {//我的店铺销售
//                    mTvCS.setVisibility(View.VISIBLE);
                    mGoldenMemberInfoLayout.setVisibility(View.GONE);
                    if (result.orderMain.orderType == 5 || result.orderMain.orderType == 7) {
                        rl_up_user.setVisibility(View.GONE);
                        tv_up_name.setText(result.upMember.nickName);
                    }
                    mLayoutMoney.setVisibility(View.GONE);
                    tvCC1.setVisibility(View.VISIBLE);
                    tvCC1.setText("*结算款项为技术服务费");
                    ll_my_sell.setVisibility(View.VISIBLE);
                    switch (mOrder.orderMain.status) {
                        case AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM://待店主确认
                            if (mOrder.orderMain.orderType == 5 && mOrder.orderMain.payTag == 1 && mOrder.orderMain.orderType != 6) {
                                tv_client_pay.setText("请确认买家已转账：");
                                tv_me_pay.setText("待我结算：");
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
                                mPayBtn.setVisibility(View.VISIBLE);
//                                tvCC1.setText(String.format("%s个CCM", "可返" + MoneyUtil.coinToYuanStrNoZero(mOrder.orderMain.coin)));
//                                mTvCC.setText("*结算款项为技术服务费");
                            } else {
                                tv_client_pay.setText("请确认买家已转账：");
                                tv_me_pay.setText("待我结算：");
                                if (mOrder.orderMain.orderType == 6) {
                                    tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                    tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
                                } else {
                                    tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.transactionFee));
                                    tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
                                }

                                mPayBtn.setVisibility(View.VISIBLE);
//                                tvCC1.setText(String.format("%s个CCM", "可返" + MoneyUtil.coinToYuanStrNoZero(mOrder.orderMain.coin)));

                            }
                            break;
                        default:
//                            if(mOrder.orderMain.orderType == 5)
                            if (mOrder.orderMain.orderType == 5 && mOrder.orderMain.payTag == 1) {
                                tv_client_pay.setText("买家已转账：");
                                tv_me_pay.setText("我已结算：");
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
                            } else if (mOrder.orderMain.orderType == 6) {
                                tv_client_pay.setText("买家已转账：");
                                tv_me_pay.setText("我已结算：");
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
                            } else {
                                tv_client_pay.setText("买家已转账：");
                                tv_me_pay.setText("我已结算：");
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.transactionFee));
                                tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
                            }


                            break;
                    }
                    if (TextUtils.isEmpty(mOrder.orderMain.paymentNumber)) {
                        tv_me_pay.setText("待我结算");
                    } else {
                        tv_me_pay.setText("我已结算");
                    }

                } else if (mMode == 3) {//我销售
                    mGoldenMemberInfoLayout.setVisibility(View.GONE);
                    if (result.orderMain.orderType == 5 || result.orderMain.orderType == 7) {
                        rl_up_user.setVisibility(View.GONE);
                        tv_up_name.setText(result.upMember.nickName);
                    }
                    ll_my_sell.setVisibility(View.VISIBLE);
                    mLayoutMoney.setVisibility(View.GONE);
                    tvCC1.setVisibility(View.GONE);
                    switch (mOrder.orderMain.status) {
                        case AppTypes.ORDER.STATUS_TO_RECOMMENDED_BY_THE_REFEREE://待推荐人转帐
                            if (mOrder.orderMain.orderType == 5) {
                                long lessTime = mOrder.orderMain.lessTime - 900000;
                                if (lessTime > 0) {
//                                    mPayBtn.setVisibility(View.VISIBLE);
                                    tv_me_pay.setText("待我付款：");
                                    tv_client_pay.setText("请确认客户已向我转账：");
                                    tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                    tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
//                                    mCancelBtn.setVisibility(View.VISIBLE);
                                } else {
                                    mPayBtn.setVisibility(View.GONE);
                                    tv_client_pay.setText("待客户向店主转账：");
                                    tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                    tv_me_pay.setVisibility(View.GONE);
                                    tv_me_pay_number.setVisibility(View.GONE);
                                    mCancelBtn.setVisibility(View.GONE);
                                }
                            } else {
                                tv_client_pay.setText("请确认客户已向我转账：");
                                tv_me_pay.setText("待我转账：");
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
//                                mPayBtn.setVisibility(View.VISIBLE);
//                                mCancelBtn.setVisibility(View.VISIBLE);
                            }
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE://关闭订单
                            if (mOrder.orderMain.orderType == 5 && mOrder.orderMain.payTag == 1) {
                                mPayBtn.setVisibility(View.GONE);
                                tv_client_pay.setText("请确认客户已向我转账：");
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                tv_me_pay.setVisibility(View.GONE);
                                tv_me_pay_number.setVisibility(View.GONE);
                            } else {

                                /*mTvPriceTag2.setText("待我付款：");
                                mTvPriceTag1.setText("客户已向我转账：");*/
                                tv_client_pay.setText("客户已向我转账：");
                                tv_me_pay.setText("待我转账：");
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
                            }


                            break;

                        case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP://待发货
                        case AppTypes.ORDER.STATUS_BUYER_WAIT_PAY://待付款
                        case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP://待收货，已发货
                        case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED://已收货，交易成功
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_MONEYING://退款中
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODING://退货中
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_MONECLOSE://退款完成
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODSCLOSE://退货完成
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_ERROR_ORDER://订单异常
                            if (mOrder.orderMain.orderType == 5 && mOrder.orderMain.payTag == 1) {
                                mPayBtn.setVisibility(View.GONE);
                                tv_client_pay.setText("客户已向店主转账：");
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                tv_me_pay.setVisibility(View.GONE);
                                tv_me_pay_number.setVisibility(View.GONE);
                            } else {
                                mPayBtn.setVisibility(View.GONE);
                                tv_client_pay.setText("客户已向我转账：");
                                tv_me_pay.setText("我已转账：");
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
                                mCancelBtn.setVisibility(View.GONE);
                            }
                            break;

                        case AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM://待店主确认
                            if (mOrder.orderMain.orderType == 5 && mOrder.orderMain.payTag == 1) {
                                tv_client_pay.setText("客户已向店主转账：");
                                tv_me_pay.setVisibility(View.GONE);
                                tv_me_pay_number.setVisibility(View.GONE);
                                mCancelBtn.setVisibility(View.GONE);
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                            } else {
                                tv_client_pay.setText("客户已向我转账：");
                                tv_me_pay.setText("我已转账：");
                                tv_client_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                tv_me_pay_number.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.memberProfitBean.profitMoney));
//                                mCancelBtn.setVisibility(View.VISIBLE);
                            }

                            if (mOrder.orderMain.orderType == 5 && mOrder.orderMain.payTag == 1) {
                                mCancelBtn.setVisibility(View.GONE);
                            } else if (mOrder.orderMain.orderType == 5 && mOrder.orderMain.payTag == 2) {
//                                mCancelBtn.setVisibility(View.VISIBLE);
                            } else if (mOrder.orderMain.orderType == 7) {
//                                mCancelBtn.setVisibility(View.VISIBLE);
                            }


                            break;
                        default:
                            break;
                    }

                    if (mOrder.memberProfitBean.status == -1) {
                        tv_me_pay.setText("预计可得提成：");
                        tv_thaw.setText("(客户未付款)");
                        tv_client_pay.setVisibility(View.GONE);
                        tv_client_pay_number.setVisibility(View.GONE);
                    } else if (mOrder.memberProfitBean.status == 0) {
                        tv_me_pay.setText("可得提成：");
                        tv_thaw.setText("(未解冻)");
                        tv_client_pay.setVisibility(View.GONE);
                        tv_client_pay_number.setVisibility(View.GONE);
                    } else if (mOrder.memberProfitBean.status == 1) {
                        tv_me_pay.setText("可得提成：");
                        tv_thaw.setText("(已解冻)");
                        tv_client_pay.setVisibility(View.GONE);
                        tv_client_pay_number.setVisibility(View.GONE);
                    }

                } else if (mMode == 4) {//我的升级订单--订单详情
                    mGoldenMemberInfoLayout.setVisibility(View.GONE);
                    if (result.orderMain.orderType == 5 || result.orderMain.orderType == 7) {
                        rl_up_user.setVisibility(View.GONE);
                        tv_up_name.setText(result.upMember.nickName);
                    }
                    mTvCS.setVisibility(View.VISIBLE);
                    if (result.orderMain.orderType != 6) {//自由订单
                        switch (mOrder.orderMain.status) {
                            case AppTypes.ORDER.STATUS_TO_RECOMMENDED_BY_THE_REFEREE://待推荐人转帐
                                mTvMoneyTag.setText("待付款：");
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                mCancelBtn.setVisibility(View.VISIBLE);

                                long lessTime = mOrder.orderMain.lessTime - 900000;
                                if (mOrder.orderMain.orderType == 5) {
                                    mPayBtn.setVisibility(View.VISIBLE);
                                    if (0 >= lessTime) {
                                        mCancelBtn.setVisibility(View.VISIBLE);
                                    } else {
                                        mCancelBtn.setVisibility(View.GONE);
                                    }
                                } else {
                                    mCancelBtn.setVisibility(View.GONE);
                                }

                                break;
                            case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE:
                                mTvMoneyTag.setText("待付款：");
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                break;

                            case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP://已发货
                                mOrderFinishBtn.setVisibility(View.VISIBLE);
                                mViewExpressBtn.setVisibility(View.VISIBLE);
                                mTvMoneyTag.setText("实付款：");
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                //TODO: 我的升级订单详情，根据后台返回的字段显示"确认升级"按钮
                                if (mOrder.orderMain.giveUpRefund == 0 && (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6))
                                    tv_confirmUpgrade.setVisibility(View.VISIBLE);
                                else
                                    tv_confirmUpgrade.setVisibility(View.GONE);
                                break;

                            case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP://待发货
                                mTvMoneyTag.setText("实付款：");
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                //TODO: 我的升级订单详情，根据后台返回的字段显示"确认升级"按钮
                                if (mOrder.orderMain.giveUpRefund == 0 && (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6))
                                    tv_confirmUpgrade.setVisibility(View.VISIBLE);
                                else
                                    tv_confirmUpgrade.setVisibility(View.GONE);
                                break;
                            case AppTypes.ORDER.STATUS_BUYER_WAIT_PAY://待付款
                                mTvMoneyTag.setText("实付款：");
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                mPayBtn.setVisibility(View.VISIBLE);
                                mCancelBtn.setVisibility(View.VISIBLE);
                                break;
                            case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED:
                            case AppTypes.ORDER.STATUS_BUYER_RETURN_MONEYING:
                            case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODING:
                            case AppTypes.ORDER.STATUS_BUYER_RETURN_MONECLOSE:
                            case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODSCLOSE:
                            case AppTypes.ORDER.STATUS_BUYER_RETURN_ERROR_ORDER:
                                mTvMoneyTag.setText("实付款：");
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                break;

                            case AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM://待店长确认

                                if (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 7) {
                                    mTvMoneyTag.setText("实付款：");
                                    mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                    mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                    mCancelBtn.setVisibility(View.VISIBLE);
                                } else if (mOrder.orderMain.orderType == 6) {
                                    mCancelBtn.setVisibility(View.VISIBLE);
                                    mTvMoneyTag.setText("实付款：");
                                    mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                    mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                }
                                if (mOrder.orderMain.orderType == 5 && mOrder.orderMain.payTag == 1) {
                                    mCancelBtn.setVisibility(View.VISIBLE);
                                } else if (mOrder.orderMain.orderType == 5 && mOrder.orderMain.payTag == 2) {
                                    mCancelBtn.setVisibility(View.GONE);
                                } else if (mOrder.orderMain.orderType == 7) {
                                    mCancelBtn.setVisibility(View.GONE);
                                }


                                mTvMoneyTag.setText("实付款：");
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                break;
                            case AppTypes.ORDER.STATUS_WAITIN_FOR_PAY_YOUR_SELF://待付款--12
                                /*long remaining = result.orderMain.lessTime;
                                if (remaining > 0) {
                                    SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
                                    if (mTimer == null) {
                                        mTimer = new CountDownTimer(remaining, 1000) {
                                            @Override
                                            public void onTick(long l) {
                                                String ms = formatter.format(l);
                                                String str = "请在" + ms + "内支付";
                                                SpannableString spannableString = new SpannableString(str);
                                                ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
                                                spannableString.setSpan(foregroundColorSpan, str.indexOf("在") + 1, str.indexOf("内"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                                tv_timer.setText(spannableString);
                                            }

                                            @Override
                                            public void onFinish() {

                                            }
                                        }.start();
                                        TimerControl.getInstance().add(mTimer);
                                    }
                                }
                                mTvMoneyTag.setText("待付款：");
                                mPayBtn.setVisibility(View.VISIBLE);
                                mPayBtn.setText("支付升级");*/
                                break;

                            default:
                                break;
                        }

                    } else {//强制订单
                        switch (mOrder.orderMain.status) {
                            case AppTypes.ORDER.STATUS_TO_RECOMMENDED_BY_THE_REFEREE://待推荐人转帐
                                mTvMoneyTag.setText("待付款：");
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                if (mOrder.vipDto.upVip == 0)
                                    mPayBtn.setVisibility(View.VISIBLE);
                                mCancelBtn.setVisibility(View.VISIBLE);
                                break;
                            case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE://已关闭
                                mTvMoneyTag.setText("待付款：");
//                                tv_timer.setVisibility(View.GONE);
                                mPayBtn.setVisibility(View.GONE);
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                break;

                            case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP:
                                mOrderFinishBtn.setVisibility(View.VISIBLE);
                                mViewExpressBtn.setVisibility(View.VISIBLE);
                                mTvMoneyTag.setText("实付款：");
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                //TODO: 我的升级订单详情，根据后台返回的字段显示"确认升级"按钮
                                if (mOrder.orderMain.giveUpRefund == 0 && (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6))
                                    tv_confirmUpgrade.setVisibility(View.VISIBLE);
                                else
                                    tv_confirmUpgrade.setVisibility(View.GONE);
                                break;

                            case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP://待发货
                                mTvMoneyTag.setText("实付款：");
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                //TODO: 我的升级订单详情，根据后台返回的字段显示"确认升级"按钮
                                if (mOrder.orderMain.giveUpRefund == 0 && (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6))
                                    tv_confirmUpgrade.setVisibility(View.VISIBLE);
                                else
                                    tv_confirmUpgrade.setVisibility(View.GONE);
                                break;
                            case AppTypes.ORDER.STATUS_BUYER_WAIT_PAY://待发货
                                mPayBtn.setVisibility(View.VISIBLE);
                                mCancelBtn.setVisibility(View.VISIBLE);
                                break;
                            case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED:
                            case AppTypes.ORDER.STATUS_BUYER_RETURN_MONEYING:
                            case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODING:
                            case AppTypes.ORDER.STATUS_BUYER_RETURN_MONECLOSE:
                            case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODSCLOSE:
                            case AppTypes.ORDER.STATUS_BUYER_RETURN_ERROR_ORDER:
                                mTvMoneyTag.setText("实付款：");
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                break;

                            case AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM://待店长确认
                                if (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 7) {
                                    mTvMoneyTag.setText("实付款：");
                                    mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                    mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                    mCancelBtn.setVisibility(View.VISIBLE);
                                } else if (mOrder.orderMain.orderType == 6) {
                                    mCancelBtn.setVisibility(View.VISIBLE);
                                    mTvMoneyTag.setText("实付款：");
                                    mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                    mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                }
                                mTvMoneyTag.setText("实付款：");
                                mPayMoneyTv.setText(ConvertUtil.centToCurrency(OrderDetailActivity.this, mOrder.orderMain.totalMoney));
                                mTvCC.setText(String.format("%s%%算率", "预计可返" + mOrder.orderMain.countRateExc));
                                break;
                            case AppTypes.ORDER.STATUS_WAITIN_FOR_PAY_YOUR_SELF://待付款--12
                                /*long remaining = result.orderMain.lessTime;
                                if (remaining > 0) {
                                    SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
                                    if (mTimer == null) {
                                        mTimer = new CountDownTimer(remaining, 1000) {
                                            @Override
                                            public void onTick(long l) {
                                                String ms = formatter.format(l);
                                                String str = "请在" + ms + "内支付";
                                                SpannableString spannableString = new SpannableString(str);
                                                ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
                                                spannableString.setSpan(foregroundColorSpan, str.indexOf("在") + 1, str.indexOf("内"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                                tv_timer.setText(spannableString);
                                            }

                                            @Override
                                            public void onFinish() {

                                            }
                                        }.start();
                                        TimerControl.getInstance().add(mTimer);
                                    }
                                } else {
                                    tv_timer.setVisibility(View.GONE);
                                }
                                mTvMoneyTag.setText("待付款：");
                                mPayBtn.setVisibility(View.VISIBLE);
                                mPayBtn.setText("支付升级");*/
                                break;

                            default:
                                break;
                        }
                    }
                }

                if (mOrder.products.size() > 0) {
                    mProductType = mOrder.products.get(0).productType;
                    if (mProductType == 28 && mMode != OrderListActivity.MODE_LIVE_STORE) {
                        mTvCS.setText("联系卖家");
                        Drawable drawable = getResources().getDrawable(R.mipmap.icon_message);
                        drawable.setBounds(0, 0, SizeUtils.dp2px(17), SizeUtils.dp2px(17));
                        mTvCS.setCompoundDrawablePadding(SizeUtils.dp2px(5));
                        mTvCS.setCompoundDrawables(drawable, null, null, null);
                        mStoreNameTv.setClickable(false);
                    }

                }
            }

        }, this);
    }

    private void initLiveStoreView() {
        mPayMoneyTv.setVisibility(View.GONE);
        llGoldenTicket.setVisibility(View.GONE);
        deductLayout.setVisibility(View.GONE);
        mGoldenMemberInfoLayout.setVisibility(View.GONE);
        String price = ConvertUtil.centToCurrency(this, mOrder.orderMain.payMoney);
        String payMoney = ConvertUtil.centToCurrency(this, mOrder.profitMoney);
        mTvMoneyTag.setText(Html.fromHtml("买家实付款：<font color=#F51861>" + price + "</font>"
        +"，你实收款：<font color=#F51861>" + payMoney + "</font>"));
        mTvCC.setTextColor(getResources().getColor(R.color.text_gray));
        mTvCC.setText("已扣除分佣");
    }

    private void initSellerViews() {
        mLayoutMoney.setVisibility(View.GONE);
        mLayoutSeller.setVisibility(View.VISIBLE);
        mTvSellerBuyerPayMoney.setText(ConvertUtil.centToCurrency(this, mOrder.orderMain.payMoney));
        mTvSellerRecevieMoney.setText(ConvertUtil.centToCurrency(this, mOrder.orderMain.payMoney + mOrder.orderMain.discountCoupon + mOrder.orderMain.score * 10));
        mTvSellerFeight1.setText(mOrder.orderMain.freight == 0 ? "¥0" : ConvertUtil.centToCurrency(this, mOrder.orderMain.freight));
        mTvSellerFeight2.setText(mOrder.orderMain.freight == 0 ? "¥0" : ConvertUtil.centToCurrency(this, mOrder.orderMain.freight));

        RvUtils.configRecycleView(this, mRvSellerProduct1);
        RvUtils.configRecycleView(this, mRvSellerProduct2);
        OrderDetailSellerProductAdapter adapter1 = new OrderDetailSellerProductAdapter(this, mOrder.products);
        adapter1.setModel(1);
        mRvSellerProduct1.setAdapter(adapter1);
        OrderDetailSellerProductAdapter adapter2 = new OrderDetailSellerProductAdapter(this, mOrder.products);
        mRvSellerProduct2.setAdapter(adapter2);
        mRvSellerProduct1.setNestedScrollingEnabled(false);
        mRvSellerProduct2.setNestedScrollingEnabled(false);
    }

    private void initSellerRefundViews() {
        if (mOrder.refundOrder != null && !StringUtils.isEmpty(mOrder.refundOrder.refundId) && mOrder.orderMain.status > 4) {
            mRefundLayout.setVisibility(View.VISIBLE);
            switch (mOrder.refundOrder.refundStatus) {
                case AppTypes.REFUNDS.BUYER_APPLY:
                    if (mOrder.refundOrder.refundType == AppTypes.REFUNDS.MONEY) {
                        //  mTvAgressRefundMoney.setVisibility(View.VISIBLE);
                    } else {
                        //    mTvAgressRefundGoods.setVisibility(View.VISIBLE);
                    }
                    break;
                case AppTypes.REFUNDS.SELLER_AGREE:
                    mOrderBottomLayout.setVisibility(View.GONE); //商家确认
                    break;
                case AppTypes.REFUNDS.BUYER_UPLOAD:
                    mViewExpressBtn.setVisibility(View.VISIBLE);
                    //    mTvReceiveRefundGodds.setVisibility(View.VISIBLE); //退货中
                    break;
                case AppTypes.REFUNDS.SELLER_RECEIVING:
                    mOrderBottomLayout.setVisibility(View.GONE); //退货完成
                    break;
                case AppTypes.REFUNDS.SELLER_REFUND_MONEY:
                    mOrderBottomLayout.setVisibility(View.GONE); //退款完成
                    break;
                default:
            }
        }
    }


    private void initOrderProducts() {
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        linearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.addItemDecoration(new ListDividerDecoration(this));
        OrderItemAdapter adapter = new OrderItemAdapter(this, mOrder.products);
        adapter.setDetailModel(true);
        adapter.setSellerModel(mMode == OrderListActivity.MODE_SELLER);
        mRecyclerView.setAdapter(adapter);
        adapter.setMemberId(mOrder.orderMain.memberId);
        adapter.setMode(mMode);
    }

    private void initVirOrder() {
        // 虚拟物品相关
        if (!showGoToUse(mOrder)) {
            goUseBtn.setVisibility(View.GONE);
            endTimeContainer.setVisibility(View.GONE);
        } else {
            if (mOrderBottomLayout.getVisibility() == View.GONE || mOrderBottomLayout.getVisibility() == View.VISIBLE) {
                mOrderBottomLayout.setVisibility(View.VISIBLE);
                for (int i = 0; i < mOrderBottomLayout.getChildCount(); ++i) {
                    mOrderBottomLayout.getChildAt(i).setVisibility(View.GONE);
                }
            }
            goUseBtn.setVisibility(View.VISIBLE);
            goUseBtn.setOnClickListener(v -> {
                Intent intent = new Intent(this, WebViewJavaActivity.class);
                intent.putExtra("url", mOrder.virOrder.url);
                intent.putExtra("hideToolbar", true);
                intent.putExtra("showWindow", true);
                startActivity(intent);
            });
            endTimeContainer.setVisibility(View.VISIBLE);
            endTime.setText(mOrder.virOrder.useEndDate);
        }
        // 虚拟物品相关 end
    }

    private void initOrderBase() {
        // 订单说明
        if (!TextUtils.isEmpty(mOrder.orderInfo)) {
            orderInfoContainer.setVisibility(View.VISIBLE);
            orderInfo.setText(mOrder.orderInfo);
        } else {
            orderInfoContainer.setVisibility(View.GONE);
        }
        mStatusTv.setText(mOrder.orderMain.orderStatusStr);
        if (mOrder.isShowGroupOrderStatus()) {
            Order.GroupInfoEntity groupInfo = mOrder.groupInfo;
            if (!TextUtils.isEmpty(groupInfo.expiresDate)) {
                mRefundGoodTipsTv.setVisibility(View.VISIBLE);
                String date = TimeUtils.date2String(TimeUtils.string2Date(groupInfo.expiresDate), "MM月dd日 HH:mm:ss");
                int min = groupInfo.joinMemberNum - groupInfo.payOrderNum;
                mRefundGoodTipsTv.setText(String.format("还差%d人成团，%s截止", min > 0 ? min : 0, date));
            }
        }
        mOrderCodeTv.setText(mOrder.orderMain.orderCode);

        if (mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_SHIP || mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED) {
            mLayoutOrderExpressCode.setVisibility(View.VISIBLE);
            mTvOrderExpressCode.setText(mOrder.orderMain.expressCode.replace(",", "\n"));
        } else {
            mLayoutOrderExpressCode.setVisibility(View.GONE);
        }

        mPhoneTv.setText(mOrder.orderMain.phone);
        mContactsTv.setText("收货人：" + mOrder.orderMain.contact);
        mAddressDetailTv.setText("收货地址：" + mOrder.orderMain.getFullAddress());
        rlAddressDetail.setOnLongClickListener(v -> {
            ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData mClipData = ClipData.newPlainText("Label", mOrder.orderMain.contact + " " +
                    mOrder.orderMain.phone + " " + mOrder.orderMain.getFullAddress());
            cm.setPrimaryClip(mClipData);
            ToastUtil.success("复制成功");
            return true;
        });
        if (mMode == OrderListActivity.MODE_LIVE_STORE) {
            mStoreNameTv.setText(mOrder.orderMain.nickName);
            mStoreNameTv.setTextColor(getResources().getColor(R.color.red));
            Drawable drawable= getResources().getDrawable(R.drawable.ic_order_message);
            drawable.setBounds(0, 0, SizeUtils.dp2px(17), SizeUtils.dp2px(17));
            mStoreNameTv.setCompoundDrawablePadding(SizeUtils.dp2px(10));
            mStoreNameTv.setCompoundDrawables(drawable,null,null,null);
        } else if (mMode == OrderListActivity.MODE_SELLER || mMode == 2) {
            mStoreNameTv.setText("买家：" + mOrder.nickName);
        } else {
            String storeName = getResources().getString(R.string.appName);
            if (mOrder.orderMain.status != 1 && !mOrder.products.isEmpty()) {
                storeName = mOrder.products.get(0).storeName;
            }
            if (mMode != 4) {

                mStoreNameTv.setText(storeName);
            } else {
                mStoreNameTv.setText(mOrder.storeName);
                mStoreNameTv.setClickable(false);
            }
            if (mMode == 3) {
                mStoreNameTv.setClickable(false);
            }
        }

        if (!(mOrder.orderMain.buyerRemark == null || mOrder.orderMain.buyerRemark.isEmpty())) {
            mBuyerRemarkLayout.setVisibility(View.VISIBLE);
            mBuyerRemarkTv.setText(mOrder.orderMain.buyerRemark);
        }
        if (!(mOrder.orderMain.sellerRemark == null || mOrder.orderMain.sellerRemark.isEmpty())) {
            mSellerRemarkLayout.setVisibility(View.VISIBLE);
            mSellerRemarkTv.setText(mOrder.orderMain.sellerRemark);
        }
        mCreateDateTv.setText(mOrder.orderMain.createDate);
        mPayWayTv.setText(mOrder.orderMain.payTypeStr);
        mProductTotalTv.setText(ConvertUtil.centToCurrency(this, mOrder.orderMain.totalProductMoney));
        mFreightTv.setText(mOrder.orderMain.freight == 0 ? "¥0" : ConvertUtil.centToCurrency(this, mOrder.orderMain.freight));

        String ccmTipStr = "预计可返";
        mGoldenMemberInfoLayout.setVisibility(View.VISIBLE);
        if (mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_WAIT_PAY || (mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE && mOrder.orderMain.payMoney < mOrder.orderMain.totalMoney)) {
            mTvMoneyTag.setText("待付款：");
            ccmTipStr = "预计可返";
            mGoldenMemberInfoLayout.setVisibility(View.GONE);
        }
        if (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6 || mOrder.orderMain.orderType == 7) {
            mGoldenMemberInfoLayout.setVisibility(View.GONE);
        }
        mTvGoldenMemberInfo.setText(mOrder.orderMain.goldenMemberInfo);
        mTvGoldenTicket.setText(mOrder.orderMain.useGoldenTicket == 0 ? "-¥0" : String.format("-%s", MoneyUtil.centToYuan¥StrNoZero(mOrder.orderMain.useGoldenTicket)));
        mTvGoldenNum.setText(mOrder.orderMain.goldenTicket == 0 ? "-¥0" : String.format("-%s", MoneyUtil.centToYuan¥StrNoZero(mOrder.orderMain.goldenTicket)));

        mPayMoneyTv.setText(ConvertUtil.centToCurrency(this, mOrder.orderMain.totalMoney));

        //优惠券
        if (mOrder.orderMain.discountCoupon > 0) {
            mLayoutConpon.setVisibility(View.VISIBLE);
            mCouponTv.setText(ConvertUtil.centToCurrency(this, -mOrder.orderMain.discountCoupon));
        }

        //积分
        long score = mOrder.orderMain.score;
        if (score > 0 && Config.IS_DISCOUNT) {
            mLayoutScore.setVisibility(View.VISIBLE);
            mTvScore.setText(String.format("%d积分，抵¥%.2f", score, score * 1.0f / 10));
        }

        //打折
//        if (mOrder.orderMain.discountMoney > 0) {
//            mLayoutRatio.setVisibility(View.VISIBLE);
////            mTvRatio.setText(String.format("已享%s折", ConvertUtil.cent2yuanNoZero(mOrder.orderMain.discountRate * 10)));
//        }

//        if (Config.IS_DISCOUNT) {
        mLayoutRatio.setVisibility(View.VISIBLE);
        mTvRatio.setText("折扣");
        if (mOrder.orderMain.discountMoney == 0) {
            mTvRatioPrice.setText("-¥0");
        } else {
            mTvRatioPrice.setText(MoneyUtil.cleanZero(String.format("-¥%.2f", mOrder.orderMain.discountMoney * 1.0f / 100)));
        }
//        }

        if (mOrder.orderMain.ticket == 0) {
            mTvCticket.setText("-¥0");
        } else {
            mTvCticket.setText(String.format("-%s", MoneyUtil.centToYuan¥StrNoZero(mOrder.orderMain.ticket)));
        }
        mTvCC.setText(String.format("%s%%算率", ccmTipStr + mOrder.orderMain.countRateExc));
        // CCM抵扣活动
        initDeductData(mOrder.orderMain.deductionCoin, mOrder.orderMain.coinDeductionMoney, deductLayout, deductTv);
    }

    private void initRefundViews() {
        if (mOrder.refundOrder == null || mOrder.refundOrder.refundId == null || mOrder.orderMain.status < 5) {
            return;
        }
        mRefundLayout.setVisibility(View.VISIBLE);
        String tag = mOrder.refundOrder.refundType == 1 ? "退货" : "退款";
        if (mOrder.refundOrder.refundType == 1 && mOrder.refundOrder.refundStatus == 1 && mMode == OrderListActivity.MODE_BUYER) {
            mRefundGoodTipsTv.setVisibility(View.VISIBLE);
            mAddRefundGoodInfoBtn.setVisibility(View.VISIBLE);
        }
        mRefundReasonLabelTv.setText(tag + "原因：");
        refundReasonValueTv.setText(mOrder.refundOrder.refundReason);
        refundApplyMoneyLabelTv.setText("申请" + tag + "金额：");
        refundApplyMoneyValueTv.setText(ConvertUtil.centToCurrency(this, mOrder.refundOrder.applyRefundMoney));
        if (mOrder.refundOrder.refundStatus >= 4) {
            mRefundMoneyLayout.setVisibility(View.VISIBLE);
            refundMoneyLabelTv.setText("实际" + tag + "金额：");
            refundMoneyValueTv.setText(ConvertUtil.centToCurrency(this, mOrder.refundOrder.refundMoney));
        }
        if (!mOrder.refundOrder.refundRemark.isEmpty()) {
            refundRemarkLayout.setVisibility(View.VISIBLE);
            refundRemarkLabelTv.setText(tag + "说明：");
            refundRemarkValueTv.setText(mOrder.refundOrder.refundRemark);
        }

        if (mOrder.refundOrder.refundType == 1) {
            if (!mOrder.refundOrder.refundGoodsImage.isEmpty()) {
                mImageRecyclerView.setVisibility(View.VISIBLE);
                GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);
                gridLayoutManager.setAutoMeasureEnabled(true);
                gridLayoutManager.setSmoothScrollbarEnabled(false);
                mImageRecyclerView.setLayoutManager(gridLayoutManager);
                ImageAdapter imageAdapter = new ImageAdapter(this);
                mImageRecyclerView.setAdapter(imageAdapter);
                imageAdapter.setItems(mOrder.refundOrder.refundGoodsImage);
            }

            if (mOrder.refundOrder.refundStatus > 1) {
                refundExpressCompanyLayout.setVisibility(View.VISIBLE);
                refundExpressCodeLayout.setVisibility(View.VISIBLE);
                refundExpressCompanyValueTv.setText(mOrder.refundOrder.refundGoodsExpressName);
                refundExpressCodeValueTv.setText(mOrder.refundOrder.refundGoodsExpressCode);
            }
        }
    }

    @OnClick(R.id.itemCancelBtn)//取消订单
    public void cancelOrder() {
        /*if (mMode == 3) {
            if (mOrder.vipDto.mVip == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("请尽快升级成C1，才能开始处理销售订单哦！");
                //点击对话框以外的区域是否让对话框消失
                builder.setCancelable(false);
                builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
                return;
            }
        }

        if (mMode == 3) {
            if (mOrder.orderMain.orderType == 5 && mOrder.vipDto.mVip != 0) {
                APIManager.startRequest(orderService.checkTimeOut(mOrder.orderMain.orderCode), new BaseRequestListener<CheckTimeOutBean>() {
                    @Override
                    public void onSuccess(CheckTimeOutBean result) {
                        super.onSuccess(result);
                        if ("0000".equals(result.code)) {//没超时
//                            OrderService.cancelOrder(OrderDetailActivity.this, mOrder);
                            showCancleOrderDialog();
                        } else if ("1".equals(result.code)) {
                            ToastUtil.error("该订单已超时");
                            EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_STORE_SHIT_SUCCEED));
                        }
                    }
                }, this);
                return;
            }

        }

        if ((mMode == 4 && mOrder.orderMain.status == AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM) || (mMode == 3 && mOrder.orderMain.status == AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM)) {
            showCancleOrderDialog();
            return;
        }*/

        OrderService.cancelOrder(this, mOrder);
    }


    private void showCancleOrderDialog() {
        View view1 = LayoutInflater.from(OrderDetailActivity.this).inflate(R.layout.cancle_order_dialog, null);
        ImageView agreeIv = view1.findViewById(R.id.agreeIv);
        TextView tv_confirmed = view1.findViewById(R.id.tv_confirmed);
        TextView tv_back = view1.findViewById(R.id.tv_back);
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailActivity.this);
        AlertDialog alertDialog = builder.create();
        alertDialog.setView(view1, 0, 0, 0, 0);
        alertDialog.show();

        tv_confirmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (agreeIv.isSelected()) {
                    alertDialog.dismiss();
                    OrderService.cancelOrder(OrderDetailActivity.this, mOrder);
                } else {
                    ToastUtil.error("取消订单请勾选<我确认还没有转账给对方>");
                }
            }
        });

        agreeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (agreeIv.isSelected()) {
                    agreeIv.setSelected(false);
                } else {
                    agreeIv.setSelected(true);
                }
            }
        });

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    @OnClick(R.id.itemPayBtn)
    public void payOrder() {//支付按钮
        OrderService.viewPayActivity(this, mOrder.orderMain.orderCode, -1);
        /*if (mMode == 2) {
            Intent intent = new Intent(this, PayOrderActivity.class);
            intent.putExtra("orderCode", mOrder.orderMain.orderCode);
            intent.putExtra("selectType", -1);
            startActivity(intent);
        } else if (mMode == 3) {
            if (mOrder.orderMain.orderType == 5 && mOrder.vipDto.mVip == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("请尽快升级成C1，才能开始处理销售订单哦！");
                //点击对话框以外的区域是否让对话框消失
                builder.setCancelable(false);
                builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
                return;
            } else if (mOrder.orderMain.orderType == 5 && mOrder.vipDto.mVip != 0) {
                IOrderService orderService = ServiceManager.getInstance().createService(IOrderService.class);
                APIManager.startRequest(orderService.checkTimeOut(mOrder.orderMain.orderCode), new BaseRequestListener<CheckTimeOutBean>() {
                    @Override
                    public void onSuccess(CheckTimeOutBean result) {
                        super.onSuccess(result);
                        if ("0000".equals(result.code)) {
                            Intent intent = new Intent(OrderDetailActivity.this, PayUpgradeActivity.class);
                            intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                            startActivity(intent);
                        } else if ("1".equals(result.code)) {
                            ToastUtil.error("该订单已超时");
                            loadOrderDetail(mOrder.orderMain.orderCode);
                        }
                    }
                }, this);
            } else if (mOrder.orderMain.orderType == 7) {
                Intent intent = new Intent(this, PayUpgradeActivity.class);
                intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                startActivity(intent);
            }
        } else if (mMode == 4) {
            if (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 7) {//自由订单
                Intent intent = new Intent(this, WaitForPaymentActivity.class);
                intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                startActivity(intent);
            } else if (mOrder.orderMain.orderType == 6) {
                Intent intent = new Intent(this, PayUpgradeActivity.class);
                intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                startActivity(intent);
            }
        } else {

        }*/


    }

    @OnClick(R.id.itemApplyRefundMoneyBtn)
    public void applyRefundMoney() {
        if (mProductType == 28) {
            OrderService.applyLiveRefundMoneyActivity(this, mOrder.orderMain.orderCode, null, true);
        } else {
            OrderService.viewApplyRefundMoneyActivity(this, mOrder);
        }
    }

    @OnClick(R.id.itemApplyRefundGoodsBtn)
    public void applyRefundGoods() {
        OrderService.viewApplyRefundGoodsActivity(this, mOrder);
    }

    @OnClick({R.id.itemViewExpressBtn, R.id.tvOrderExpressCode})
    public void viewExpress() {
        OrderService.viewExpress(this, mOrder);
    }

    @OnClick(R.id.orderFinishBtn)
    public void finishOrder() {
        OrderService.finishOrder(this, mOrder);
    }

    @OnClick(R.id.tvAgressRefundGoods)
    public void onMTvAgressRefundGoodsClicked() {
//        OrderService.showRefundGoodsDialog(this, mOrder);
    }

    @OnClick(R.id.tvAgressRefundMoney)
    public void onMTvAgressRefundMoneyClicked() {
//        OrderService.showRefundMoneyDialog(this, mOrder);
    }

    @OnClick(R.id.tvReceiveRefundGodds)
    public void onMTvReceiveRefundGoddsClicked() {
        // 确认退货
        Intent intent = new Intent(this, ReceiveRefundGoodsActivity.class);
        intent.putExtra("orderCode", mOrder.orderMain.orderCode);
        intent.putExtra("maxPrice", mOrder.canRefundMoney(1));
        startActivity(intent);
    }

    @OnClick(R.id.addRefundGoodExpressBtn)
    public void addRefundGoodExpress() {
        Intent intent = new Intent(this, RefundExpressActivity.class);
        intent.putExtra("refundId", mOrder.refundOrder.refundId);
        startActivity(intent);
    }

    @OnClick(R.id.itemShit)
    public void onViewClicked() {
        if (mMode == OrderListActivity.MODE_MY_GIFT) {
            if (mOrderShipDialog == null) {
                mOrderShipDialog = new OrderShipDialog(OrderDetailActivity.this);
                mOrderShipDialog.setOnConfirmListener(new OrderShipDialog.OnConfirmListener() {
                    @Override
                    public void confirm(String expressName, String expressCode) {
                        dispatch(expressName, expressCode);
                    }

                    @Override
                    public void sacn() {
                        sacnCode();
                    }
                });
            }

            mOrderShipDialog.show();

        } else if (mMode == OrderListActivity.MODE_LIVE_STORE) {
            OrderService.shipLive(this, mOrder);
        } else {
            OrderService.ship(this, mOrder);
        }
    }

    private void dispatch(String expressName, String expressCode) {
//        HashMap<String, Object> params = new HashMap<>();
//        params.put("orderCode", mOrder.orderMain.orderCode);
//        params.put("expressName", expressName);
//        params.put("expressCode", expressCode);

        APIManager.startRequest(mOrderService.dispatch(mOrder.orderMain.orderCode, expressName, expressCode), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                ToastUtil.success("发货成功");
                EventBus.getDefault().post(new EventMessage(Event.orderChange));
                finish();
            }
        }, this);
    }

    private void sacnCode() {
        new RxPermissions(this).request(Manifest.permission.CAMERA).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (aBoolean) {
                    startActivity(new Intent(OrderDetailActivity.this, ScanActivity.class));
                } else {
                    PermissionsUtils.goPermissionsSetting(OrderDetailActivity.this);
                    ToastUtil.error("无法获得相机权限");
                }
            }
        });
    }


    @OnClick({R.id.itemCancelRefundMoney, R.id.itemCancelRefundGoods})
    public void onViewClicked(View view) {
        OrderService.showCancelRefund(this, mOrder);
    }

    @OnClick({R.id.itemGoGroupBuy, R.id.itemCheckGroupBuy})
    public void itemGoGroupBuy() {
        OrderService.goGroupBuy(this, mOrder);
    }

    @OnClick(R.id.itemGoSuperGroupBuy)
    public void itemGoSuperGroupBuy() {
        Intent intent = new Intent(OrderDetailActivity.this, JoinGroupActivity.class);
        intent.putExtra(Config.INTENT_KEY_TYPE_NAME, JoinGroupActivity.TYPE_HOST);
        intent.putExtra(Config.INTENT_KEY_ID, mOrder.groupInfo.groupCode);
        intent.putExtra("isSuperGroup", true);
        startActivity(intent);
    }


    @OnClick(R.id.tvCS)
    public void onGoCS() {
        int productType = 0;
        if (mOrder.products.size() > 0) {
            productType = mOrder.products.get(0).productType;
        }
        if (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6 || (productType == 28 && mMode != OrderListActivity.MODE_LIVE_STORE)) {
//            mOrder.

            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (loginUser != null) {
                TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                    @Override
                    public void onError(int i, String s) {
                    }

                    @Override
                    public void onSuccess() {
                        IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                        APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                        }, OrderDetailActivity.this);
                        ChatInfo chatInfo = new ChatInfo();
                        chatInfo.setType(TIMConversationType.C2C);
                        chatInfo.setId(mOrder.storeUser.memberId);
                        chatInfo.setChatName(mOrder.storeUser.storeName);
                        Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                        intent.putExtra("chatInfo", chatInfo);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
            }

            /*ChatInfo chatInfo = new ChatInfo();
            chatInfo.setType(TIMConversationType.C2C);
            chatInfo.setId(mOrder.storeUser.memberId);
            chatInfo.setChatName(mOrder.storeName);
            Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
            intent.putExtra("chatInfo", chatInfo);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);*/
        } else {
            CSUtils.start(this);
        }
    }

    @OnClick(R.id.tvCopy)
    public void copyOrderCode() {
        ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData mClipData = ClipData.newPlainText("Label", mOrder.orderMain.orderCode);
        cm.setPrimaryClip(mClipData);
        ToastUtil.success("复制成功");
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void orderHandler(EventMessage message) {
        if (message.getEvent().equals(Event.cancelOrder)) {
            mOrder.orderMain.status = 0;
            mOrder.orderMain.orderStatusStr = "已关闭";
            initBottomButtons();
        } else if (message.getEvent().equals(Event.finishOrder)) {
            mOrder.orderMain.status = 4;
            mOrder.orderMain.orderStatusStr = "已收货";
            initBottomButtons();
        } else if (message.getEvent().equals(Event.refundExpressSubmit)) {
            loadOrderDetail(mOrder.orderMain.orderCode);
        } else if (message.getEvent().equals(Event.refundOrder)) {
            finish();
            EventUtil.viewOrderDetail(this, mOrder.orderMain.orderCode, false);
        } else if (message.getEvent().equals(Event.newRetailPaySuccess)) {
            mMode = 4;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(PayMsg msgStatus) {
        switch (msgStatus.getAction()) {
            case PayMsg.ACTION_ALIPAY_SUCCEED:
            case PayMsg.ACTION_WEBPAY_SUCCEED:
            case PayMsg.ACTION_WXPAY_SUCCEED:
                finish();
                break;
            case PayMsg.ACTION_BALANCE_SUCCEED:
                finish();
                break;
            case PayMsg.ACTION_WXPAY_FAIL:
//                ToastUtils.showShortToast(msgStatus.message);
                break;
            case PayMsg.ACTION_ALIPAY_FAIL:
//                ToastUtils.showShortToast(msgStatus.message);
                break;
            case PayMsg.ACTION_WEBPAY_FAIL:
//                finish();
//                EventUtil.viewHome(PayOrderActivity.this);
                break;
            default:
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(MsgStatus msgStatus) {
        switch (msgStatus.getAction()) {
            case MsgStatus.ACTION_REFUND_CHANGE:
                loadOrderDetail(mOrderCode);
                break;
            case MsgStatus.ACTION_STORE_SHIT_SUCCEED:
                finish();
                if (mMode == OrderListActivity.MODE_LIVE_STORE) {
                    Intent intent = new Intent(this, OrderDetailActivity.class);
                    intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                    intent.putExtra("mode", mMode);
                    startActivity(intent);
                } else {
                    EventUtil.viewOrderDetail(this, mOrder.orderMain.orderCode, false);
                }
                break;
            case MsgStatus.ACTION_SCAN_SUCCEED:
                if (null != mOrderShipDialog) {
                    mOrderShipDialog.setExpressCode(msgStatus.getCode());
                }
                break;
            default:
        }
    }

    public void showConfirmUpgradeDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailActivity.this);
        builder.setMessage("请收到货后确认对产品满意后再点确认收货哦，直接确认升级表示您将放弃消费者退货权益，请慎重考虑");
        //点击对话框以外的区域是否让对话框消失
        builder.setCancelable(false);
        builder.setNegativeButton("我再想想", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("考虑好了，确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                //TODO: 请求"确认升级"接口后，刷新界面

                APIManager.startRequest(orderService.giveUpRefund(mOrder.orderMain.orderCode), new BaseRequestListener<Object>() {
                    @Override
                    public void onSuccess(Object result) {
                        super.onSuccess(result);
                        loadOrderDetail(mOrder.orderMain.orderCode);
                        EventBus.getDefault().post(new EventMessage(Event.confirmUpgrade));


                    }
                }, OrderDetailActivity.this);

            }
        });
        builder.show();
    }

    @OnClick(R.id.tv_confirmUpgrade)
    public void confirmUpgrade() {
        //TODO: 快速收货按钮
        showConfirmUpgradeDialog();
    }

    @OnClick(R.id.storeNameTv)
    public void chat() {//去聊天

        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, OrderDetailActivity.this);
                    ChatInfo chatInfo = new ChatInfo();
                    chatInfo.setType(TIMConversationType.C2C);
                    chatInfo.setId(mOrder.orderMain.memberId);
                    chatInfo.setChatName(mOrder.orderMain.nickName);
                    Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                    intent.putExtra("chatInfo", chatInfo);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });
        }


    }

    @OnClick(R.id.goUseBtn)
    public void goUseBtn() {
    }
}
