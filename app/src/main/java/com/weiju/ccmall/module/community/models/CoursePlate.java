package com.weiju.ccmall.module.community.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CoursePlate {

    /**
     * plateId : 1
     * iconUrl : 22www
     * title : 区块链
     * memberType : 0
     * sortType :
     * sortIndex : 22
     * status : 1
     * createDate : 2019-12-21 15:34:32
     * updateDate : 2019-12-21 15:34:32
     * courseCats : [{"categoryId":"42","iconUrl":"fsdf","title":"语音","plateId":"1","type":0,"sortIndex":"0","status":"","createDate":"2019-12-21 16:19:58","updateDate":"2019-12-21 16:20:05"}]
     */

    @SerializedName("plateId")
    public String plateId;
    @SerializedName("iconUrl")
    public String iconUrl;
    @SerializedName("title")
    public String title;
    @SerializedName("memberType")
    public String memberType;
    @SerializedName("sortType")
    public String sortType;
    @SerializedName("sortIndex")
    public String sortIndex;
    @SerializedName("status")
    public String status;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("courseCats")
    public List<CourseCatsBean> courseCats;

    public static class CourseCatsBean implements Serializable {
        /**
         * categoryId : 42
         * iconUrl : fsdf
         * title : 语音
         * plateId : 1
         * type : 0
         * sortIndex : 0
         * status :
         * createDate : 2019-12-21 16:19:58
         * updateDate : 2019-12-21 16:20:05
         */

        @SerializedName("categoryId")
        public String categoryId;
        @SerializedName("iconUrl")
        public String iconUrl;
        @SerializedName("title")
        public String title;
        @SerializedName("plateId")
        public String plateId;
        @SerializedName("type")
        public int type;
        @SerializedName("sortIndex")
        public String sortIndex;
        @SerializedName("status")
        public String status;
        @SerializedName("createDate")
        public String createDate;
        @SerializedName("updateDate")
        public String updateDate;
    }
}
