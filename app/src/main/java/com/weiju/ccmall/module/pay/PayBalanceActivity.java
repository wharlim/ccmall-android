package com.weiju.ccmall.module.pay;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.CaptchaBtn;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICaptchaService;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.pay
 * @since 2017-07-12
 */
public class PayBalanceActivity extends BaseActivity {

    @BindView(R.id.phoneTv)
    protected TextView mPhoneTv;
    @BindView(R.id.captchaEt)
    protected EditText mCaptchaEt;
    @BindView(R.id.captchaBtn)
    protected CaptchaBtn mCaptchaBtn;
    @BindView(R.id.passwordEt)
    protected EditText mPasswordEt;
    @BindView(R.id.layoutCode)
    LinearLayout mLayoutCode;
    private ICaptchaService mCaptchaService;
    private IUserService mUserService;
    private User mUser;
    private IOrderService mOrderService;
    private Order mOrder;
    private int mPayType;
    private boolean viewDetailAfterPaySuccess = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_balance);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            mOrder = (Order) intent.getExtras().get("order");
            mPayType = intent.getIntExtra("payType", 0);
            viewDetailAfterPaySuccess = intent.getBooleanExtra("viewDetailAfterPaySuccess", true);
        }
        if (mOrder == null) {
            ToastUtil.error("参数错误");
            finish();
            return;
        }
        showHeader();
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_black);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askUserWhetherGiveUp();
            }
        });
        mLayoutCode.setVisibility(View.GONE);
        setTitle(mPayType == AppTypes.PAY_TYPE.BALANCE ? "余额支付" : "购物券支付");
        mCaptchaService = ServiceManager.getInstance().createService(ICaptchaService.class);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
        getUserInfo();
    }

    private void getUserInfo() {
        APIManager.startRequest(mUserService.getUserInfo(), new BaseRequestListener<User>(this) {
            @Override
            public void onSuccess(User result) {
                mUser = result;
                mPhoneTv.setText(StringUtil.maskPhone(mUser.phone));
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.error("获取用户信息失败");
            }
        }, this);
    }

    @OnClick(R.id.captchaBtn)
    protected void getCaptcha() {
        String token = StringUtil.md5(Constants.API_SALT + mUser.phone);
        APIManager.startRequest(mCaptchaService.getCaptchaForCheck(token, mUser.phone), new BaseRequestListener<Object>(this) {

            @Override
            public void onSuccess(Object result) {
                mCaptchaBtn.start();
            }
        }, this);
    }

    @OnClick(R.id.confirmBtn)
    protected void confirmToPay() {
//        String captcha = mCaptchaEt.getText().toString();
//        if (captcha.isEmpty()) {
//            ToastUtil.error("验证码不能为空");
//            return;
//        }
        String password = mPasswordEt.getText().toString();
        if (password.isEmpty()) {
            ToastUtil.error("密码不能为空");
            return;
        }
        APIManager.startRequest(mOrderService.payBalance(mOrder.orderMain.orderCode, password, null, mPayType), new BaseRequestListener<Object>(this) {

            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("支付成功");
                EventBus.getDefault().post(new EventMessage(Event.paySuccess, mOrder));
                EventBus.getDefault().post(new EventMessage(Event.newRetailPaySuccess));
                if (viewDetailAfterPaySuccess) {
                    EventUtil.viewOrderDetail(PayBalanceActivity.this, mOrder.orderMain.orderCode, false);
                }
                EventBus.getDefault().post(new PayMsg(PayMsg.ACTION_BALANCE_SUCCEED));
                finish();
            }
//            @Override
//            public void onError(Throwable e) {
//                super.onError(e);
//                mCaptchaBtn.stop();
//            }
        }, this);
    }

    @Override
    public void onBackPressed() {
        askUserWhetherGiveUp();
    }

    private void askUserWhetherGiveUp() {
        final WJDialog dialog = new WJDialog(this);
        dialog.show();
        dialog.setTitle("提示");
        dialog.setContentText("是否放弃交易?");
        dialog.setCancelText("否");
        dialog.setConfirmText("是");
        dialog.setOnConfirmListener((v) -> {
            EventBus.getDefault().post(new PayMsg(PayMsg.ACTION_BALANCE_CANCEL, "支付取消"));
            finish();
        });
    }
}
