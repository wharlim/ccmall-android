package com.weiju.ccmall.module.instant;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.instant.adapter.InstantAdapter;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.GetSecondKillProductListModel;
import com.weiju.ccmall.shared.bean.InstantData;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.decoration.SpacesItemDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IInstantService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.instant
 * @since 2017-08-01
 */
public class InstantFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recyclerView)
    RecyclerView mRvList;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mLayoutRefresh;

    private String mId;
    private InstantData.SecondKill mSecondKill;
    private ArrayList<InstantData.Product> mDatas;
    private InstantAdapter mAdapter;

    public static InstantFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString("id", id);
        InstantFragment fragment = new InstantFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.base_list_layout, container, false);
        ButterKnife.bind(this, view);
        getIntentData();
        EventBus.getDefault().register(this);
        LogUtils.e("开始注册");
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }

    private void getData(boolean isRefresh) {
        if (isRefresh) {
            mDatas.clear();
            mAdapter.notifyDataSetChanged();
        }
        IInstantService service = ServiceManager.getInstance().createService(IInstantService.class);
        APIManager.startRequest(
                service.getSecondKillProductList(
                        mDatas.size() / Constants.PAGE_SIZE + 1,
                        Constants.PAGE_SIZE,
                        mId)
                , new BaseRequestListener<GetSecondKillProductListModel>(mLayoutRefresh) {
                    @Override
                    public void onSuccess(GetSecondKillProductListModel result) {
                        mDatas.addAll(result.datas);
                        mAdapter.notifyDataSetChanged();

                        if (result.datas.size() < Constants.PAGE_SIZE) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mAdapter.loadMoreFail();
                    }
                },getContext());
    }

    private void initView() {
        mRvList.setLayoutManager(new LinearLayoutManager(getContext()));
        mDatas = new ArrayList<>();
        mAdapter = new InstantAdapter(mDatas);
        InstantBannerView instantBannerView = new InstantBannerView(getContext());
        instantBannerView.setData(mSecondKill);
        mAdapter.addHeaderView(instantBannerView);
        mRvList.setAdapter(mAdapter);
        mLayoutRefresh.setOnRefreshListener(this);
        mAdapter.setEmptyView(new NoData(getContext()).setImgRes(R.mipmap.no_data_order));
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getData(false);
            }
        });
        mRvList.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                goProductDetail(mAdapter.getItem(position).skuId, false);
            }
        });
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                goProductDetail(mAdapter.getItem(position).skuId, true);
            }
        });
        mRvList.setBackgroundColor(getResources().getColor(R.color.bg_gray));
        mRvList.addItemDecoration(new SpacesItemDecoration(SizeUtils.dp2px(10)));
    }

    private void goProductDetail(String skuId, boolean isBuy) {
        Intent intent = new Intent(getContext(), NewProductDetailActivity.class);
        intent.putExtra(Key.SKU_ID, skuId);
        startActivity(intent);
        if (isBuy) {
            EventBus.getDefault().postSticky(new MsgInstant(MsgInstant.ACTION_BUY));
        }
    }

    private void getIntentData() {
        mId = getArguments().getString("id");
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getMsg(MsgInstant msgInstant) {
        switch (msgInstant.getAction()) {
            case MsgInstant.ACTION_SEND_SECOND_KILL:
                LogUtils.e(mId + "接收到一个");
                for (InstantData.SecondKill secondKill : msgInstant.getSecondKills()) {
                    if (secondKill.id.equals(mId)) {
                        mSecondKill = secondKill;
                        initView();
                        getData(true);
                        LogUtils.e("匹配成功");
                        return;
                    }
                }
                break;
        }
    }

    @Override
    public void onRefresh() {
        getData(true);
    }
}
