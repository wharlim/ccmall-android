package com.weiju.ccmall.module.world.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/5/8.
 */
public class WxMiniCallBackEntity {

    /**
     * shopCode : CCMALL-GPSL
     * amount : 0.01
     * desc : 等待支付
     * merchantOrderNo : 2311589157006554
     * status : 1
     * time : 2020-05-08 17:20:34
     */

    @SerializedName("shopCode")
    public String shopCode;
    @SerializedName("amount")
    public String amount;
    @SerializedName("desc")
    public String desc;
    @SerializedName("merchantOrderNo")
    public String merchantOrderNo;
    @SerializedName("status")
    public String status;   // 交易状态 0-成功 1-失败
    @SerializedName("time")
    public String time;

    @Override
    public String toString() {
        return "WxMiniCallBackEntity{" +
                "shopCode='" + shopCode + '\'' +
                ", amount='" + amount + '\'' +
                ", desc='" + desc + '\'' +
                ", merchantOrderNo='" + merchantOrderNo + '\'' +
                ", status='" + status + '\'' +
                ", time='" + time + '\'' +
                '}';
    }
}
