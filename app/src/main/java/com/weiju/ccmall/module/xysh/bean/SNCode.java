package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SNCode {

    /**
     * updateDate : 2019-11-26 17:37:54
     * snCode : SN60230002250
     */

    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("snCode")
    public String snCode;

    public static class SNCodeResult {
        @SerializedName("count")
        public int count;
        @SerializedName("list")
        public List<SNCode> list;
    }
}
