package com.weiju.ccmall.module.xysh.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.ToastUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.page.CustomPageActivity;
import com.weiju.ccmall.module.page.WebViewCreditLifeActivity;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.user.QRCodeActivity;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.BindITSNActivity;
import com.weiju.ccmall.module.xysh.activity.CardDoctorActivity;
import com.weiju.ccmall.module.xysh.activity.CreditCardApplyH5Activity;
import com.weiju.ccmall.module.xysh.activity.QuickPaymentsActivity;
import com.weiju.ccmall.module.xysh.activity.RealNameAuthActivity;
import com.weiju.ccmall.module.xysh.activity.RepaymentListActivity;
import com.weiju.ccmall.module.xysh.activity.XinYongUserActivity;
import com.weiju.ccmall.module.xysh.adapter.MyPagerAdapter;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.bean.XyshNotice;
import com.weiju.ccmall.module.xysh.bean.XyshUser;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.ILotteryService;
import com.weiju.ccmall.shared.util.BaseUrl;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.zhy.magicviewpager.transformer.AlphaPageTransformer;
import com.zhy.magicviewpager.transformer.ScaleInTransformer;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * @author chenyanming
 * @time 2019/12/9 on 18:01
 * @desc
 */
public class XinYongHomeFragment extends BaseFragment {

    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.viewPagerMiddle)
    ViewPager mViewPagerMiddle;
    @BindView(R.id.tvTipText)
    TextView tvTipText;
    @BindView(R.id.llTipContainer)
    LinearLayout llTipContainer;
    private ArrayList<View> mViews;

    @BindView(R.id.flMessage)
    FrameLayout flMessage;

    @BindView(R.id.tvMessageBody)
    TextView tvMessageBody;

    @BindView(R.id.ivMsgClose)
    ImageView ivMsgClose;

    @BindView(R.id.barPading)
    View mBarPading;

    // 轮播图控制器
    CountDownTimer mCountDownTimer;
    private XyshService xyshService = ServiceManager.getInstance().createService2(XyshService.class);
    private ILotteryService iLotteryService = ServiceManager.getInstance().createService(ILotteryService.class);
    boolean hasRealNameAuth;

    private XyshUser xyshUser;
    /**
     * 在sp里面保存信用生活的tip是否关闭的key, "1"意味着已读并关闭 null意味着未关闭
     */
    private static final String KEY_XYSH_TIP = "xysh_tip";
    public static boolean shouldGetNotice = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_xinyong_home, container, false);
        ButterKnife.bind(this, rootView);
        tvTipText.setText(Html.fromHtml("点击我的查看<span style=\"color:#D0BA89\">" +
                "「 还款计划 」</span>与<span style=\"color:#D0BA89\">「 收款计划 」</span>"));
        initView();
        return rootView;
    }

    private void initView() {
        //导航栏高度
        int height = QMUIStatusBarHelper.getStatusbarHeight(getContext());
        mBarPading.getLayoutParams().height = height;
        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams)llTipContainer.getLayoutParams();
        mlp.topMargin += height;
        llTipContainer.setLayoutParams(mlp);

        int[] banners = new int[]{
                R.drawable.ic_credit_life_banner,
        };
        int[] bannersMiddle = new int[]{
                R.drawable.ic_credit_life_banner_middle,
//                R.drawable.xysh_banner1,
//                R.drawable.xysh_banner2,
//                R.drawable.xysh_banner3,
        };

        initViewPage(mViewPager, banners, true);
        initViewPage(mViewPagerMiddle, bannersMiddle, false);
    }

    private void initViewPage(ViewPager viewPager, int[] banners, boolean isUpgrade) {
        ArrayList<View> views = new ArrayList<>();
        for (int i = 0; i < banners.length; i++) {
            View inflate = View.inflate(getContext(), R.layout.viewpager_qrcode, null);
            SimpleDraweeView ivPoster = inflate.findViewById(R.id.ivPoster);
//            FrescoUtil.setImage(ivPoster, list.get(i));
            ivPoster.setImageResource(banners[i]);
            inflate.setOnClickListener(v -> {
                if (isUpgrade) {
                    if (xyshUser != null) {
                        String host;
                        if (BuildConfig.DEBUG) {
                            host = "http://" + BaseUrl.getInstance().getCCPayHostPost();
                        } else {
                            host = "https://" + BuildConfig.XYSH_HOST + "/customizYTH";
                        }
                        WebViewCreditLifeActivity.start(getContext(), String.format("%s/#/UpServer", host), false, xyshUser.usrLvl, 1, xyshUser.usrTel);
                    }
                } else {
                    // 黑科技页面
                    Intent intent = new Intent(getContext(), CustomPageActivity.class);
                    intent.putExtra("pageId", "edd433aa58a94a468fea6b20ad399810 ");
                    startActivity(intent);
                }
            });
            views.add(inflate);
        }

        viewPager.setPageMargin(25);//设置page间间距，自行根据需求设置
        viewPager.setOffscreenPageLimit(3);
        viewPager.setPageTransformer(true, new
                AlphaPageTransformer(new ScaleInTransformer()));
        viewPager.setAdapter(new MyPagerAdapter(views));

        // 无限执行
//        mCountDownTimer = new CountDownTimer(10000000000L, 3000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//                int count = mViewPager.getAdapter().getCount();
//                int current = mViewPager.getCurrentItem();
//                current = (current + 1) % count;
//                mViewPager.setCurrentItem(current, true);
//            }
//
//            @Override
//            public void onFinish() {
//
//            }
//        }.start();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
//        Log.d("Seven", "isVisibleToUser -> " +isVisibleToUser);
        if (isVisibleToUser) {
            BankUtils.downSupportBanks();
            showTipOrNot();
            getCreditLifeNotice();
            updateRealNameInfo();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint()) {
            updateRealNameInfo();
        }
    }

    private void updateRealNameInfo() {
        if (!SessionUtil.getInstance().isLogin()) {
            return;
        }
        // 获取实名信息
        APIManager.startRequest(xyshService.userPersonCenter(SessionUtil.getInstance().getOAuthToken()), new Observer<XYSHCommonResult<XyshUser>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<XyshUser> ret) {
                if (ret.success) {
                    xyshUser = ret.data;
                    hasRealNameAuth = ret.data.hasRealNameAuth();
                    SessionUtil.getInstance().setXyshUserInfo(ret.data);
                } else {
                    // ToastUtil.error(ret.resMsg + "");
                    UserService.logout(getContext());
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {
            }
        });
    }

    private void getCreditLifeNotice() {
        if (!shouldGetNotice) {
            return;
        }
        flMessage.setVisibility(View.GONE);
        shouldGetNotice = false;
        APIManager.startRequest(iLotteryService.getCcpayBulletin(), new BaseRequestListener<XyshNotice>() {
            @Override
            public void onSuccess(XyshNotice result) {
                super.onSuccess(result);
                if (result.displayStatus == 1) {
                    flMessage.setVisibility(View.VISIBLE);
                    tvMessageBody.setText(result.getContent().content);
                }
            }
        }, getContext());
    }


    @OnClick(R.id.ivUser)
    protected void user() {
        onTipClose();
        if (SessionUtil.getInstance().getXyshUserInfo() != null) {
            startActivity(new Intent(getContext(), XinYongUserActivity.class));
        }
    }

    @OnClick(R.id.tvCreditCardApply)
    protected void apply() {
        if (!hasRealNameAuth) {
            showRealNameAuthDialog();
            return;
        }
        CreditCardApplyH5Activity.start(getContext());
//        startActivity(new Intent(getContext(), CreditCardApplyListActivity.class));
    }

    @OnClick(R.id.tv_card_doctor)
    public void onCardDoctor() {
        if (!hasRealNameAuth) {
            showRealNameAuthDialog();
            return;
        }
        CardDoctorActivity.start(getContext());
    }

    @OnClick(R.id.tv_bind_now)
    public void onBindNow() {
        BindITSNActivity.start(getContext());
    }

    @OnClick(R.id.ll_quick_payment)
    public void onQuickPayment() {
//        ToastUtils.showShortToast("系统升级中");

        if (!hasRealNameAuth) {
            showRealNameAuthDialog();
            return;
        }
        QuickPaymentsActivity.start(getContext());
    }

    @OnClick(R.id.tv_integral_exchange)
    public void onIntegralExchange() {
//        if (BuildConfig.DEBUG) {
//            if (!hasRealNameAuth) {
//                showRealNameAuthDialog();
//                return;
//            }
//            IntegralExchangeActivity.start(getContext());
//        } else {
//            if (!hasRealNameAuth) {
//                showRealNameAuthDialog();
//                return;
//            }
//            IntegralExchangeActivity.start(getContext());
//        }
        ToastUtils.showShortToast("升级中，敬请期待");
    }

    @OnClick(R.id.ll_intelligence_repay)
    public void onIntelligenceRepay() {
        if (!hasRealNameAuth) {
            showRealNameAuthDialog();
            return;
        }
        // IntelligenceRepaymentActivity.start(getContext());
        RepaymentListActivity.start(getContext(), true, RepaymentListActivity.FROM_REPAY);
    }

    @OnClick(R.id.ivAd)
    public void inviteFriend() {
        QRCodeActivity.start(getContext(), QRCodeActivity.TYPE_InvitationCodeByIconCreditCard);
    }

    @OnClick(R.id.tvInsurance)
    public void insurance() {
        insurance(getContext());
    }

    @OnClick(R.id.ivMsgClose)
    public void onMsgClose() {
        flMessage.setVisibility(View.GONE);
    }

    public static void insurance(Context context) {
        Intent intent = new Intent(context, WebViewJavaActivity.class);
        intent.putExtra("url", "https://ztg.zhongan.com/promote/entrance/promoteEntrance.do?" +
                "redirectType=h5&promotionCode=INST190499339001" +
                "&productCode=PRD181140121002&promoteCategory=single_product&token=");
        intent.putExtra("hideToolbar", true);
        intent.putExtra("showWindow", true);
        intent.putExtra("isXYSH", true);
        context.startActivity(intent);
    }

    @OnClick(R.id.ivTipClose)
    public void onTipClose() {
        llTipContainer.setVisibility(View.GONE);
        SessionUtil.getInstance().putString(KEY_XYSH_TIP, "1");
    }

    /*class MyPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mViews.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            container.addView(mViews.get(position));
            return mViews.get(position);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }
    }*/

    private void showRealNameAuthDialog() {
        showRealNameAuthDialog(getContext());
    }

    public static void showRealNameAuthDialog(Context context) {
        WJDialog wjDialog = new WJDialog(context);
        wjDialog.show();
        wjDialog.setTitle("温馨提示");
        wjDialog.setContentText("尊敬的用户，您还没有实名，请前往实名方可使用！");
        wjDialog.setCancelText("残忍拒绝");
        wjDialog.setConfirmText("立即前往");
        wjDialog.setConfirmTextColor(R.color.blue);
        wjDialog.setOnConfirmListener(v -> {
            wjDialog.dismiss();
            RealNameAuthActivity.start(context);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        shouldGetNotice = true;
    }

    /**
     * 1. 第一次进入信用生活，会在右上角进行提示
     * 2. 除非用户进入过个人中心，或者主动点击了关闭，否则下次进入信用生活同样会提示
     */
    private void showTipOrNot() {
        if (!SessionUtil.getInstance().isLogin()) {
            return;
        }
        String tipState = SessionUtil.getInstance().getString(KEY_XYSH_TIP);
        llTipContainer.setVisibility("1".equals(tipState) ? View.GONE : View.VISIBLE);
    }

    /*@Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case creditLifeUpgradeSuccess:
                int fromPage = (int) message.getData();
                if (fromPage == 1) updateRealNameInfo();
                break;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }*/
}
