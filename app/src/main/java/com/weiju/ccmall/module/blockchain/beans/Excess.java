package com.weiju.ccmall.module.blockchain.beans;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/6/24.
 */
public class Excess {


    /**
     * limitCcm : 单日限额1000
     * limitInfo : 目前单日限额为1000CCM，如有急需可联系客服申请提额。后续系统将尽快上线常用设备管理功能，被信任的设备可不限额转账
     */

    @SerializedName("limitCcm")
    public String limitCcm;
    @SerializedName("limitInfo")
    public String limitInfo;

    /**
     * isAllow : 1
     * msg : 未达到限额
     */

    @SerializedName("isAllow")
    public int isAllow;
    @SerializedName("msg")
    public String msg;
}
