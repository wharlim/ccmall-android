package com.weiju.ccmall.module.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.pay.PayResultCountDownView;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Contribution;
import com.weiju.ccmall.shared.bean.ContributionType;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * @author chenyanming
 * @time 2019/2/22 on 15:33
 * @desc 我的贡献值
 */
public class MyContributionActivity extends BaseActivity {

    @BindView(R.id.payDownViewPSP)
    PayResultCountDownView mPayDownViewPSP;
    @BindView(R.id.payDownViewTSP)
    PayResultCountDownView mPayDownViewTSP;
    @BindView(R.id.payDownViewTSE)
    PayResultCountDownView mPayDownViewTSE;
    @BindView(R.id.payDownViewTSL)
    PayResultCountDownView mPayDownViewTSL;
    @BindView(R.id.payDownViewPCP)
    PayResultCountDownView mPayDownViewPCP;
    @BindView(R.id.tvPSP)
    TextView mTvPSP;
    @BindView(R.id.tvTSP)
    TextView mTvTSP;
    @BindView(R.id.tvTSE)
    TextView mTvTSE;
    @BindView(R.id.tvTSL)
    TextView mTvTSL;
    @BindView(R.id.tvPCP)
    TextView mTvPCP;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contribution);
        ButterKnife.bind(this);
        initView();
        initData();

    }

    private void initView() {
        setTitle("我的贡献值");
        setLeftBlack();
        setTitleNoLine();

        mPayDownViewPSP.setPay(false);
        mPayDownViewTSP.setPay(false);
        mPayDownViewTSE.setPay(false);
        mPayDownViewTSL.setPay(false);
        mPayDownViewPCP.setPay(false);

        mPayDownViewPSP.setRingWidth( SizeUtils.dp2px(4.0f));
        mPayDownViewTSP.setRingWidth(SizeUtils.dp2px(4.0f));
        mPayDownViewTSE.setRingWidth(SizeUtils.dp2px(4.0f));
        mPayDownViewTSL.setRingWidth(SizeUtils.dp2px(4.0f));
        mPayDownViewPCP.setRingWidth(SizeUtils.dp2px(4.0f));

    }

    private void initData() {
        IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(mUserService.getContribution(), new BaseRequestListener<Contribution>(this) {
            @Override
            public void onSuccess(Contribution contribution) {
                super.onSuccess(contribution);
                setData(contribution);
            }
        },this);
    }

    private void setData(Contribution contribution) {
        mPayDownViewPSP.setmCurrentProgress(getProgress(contribution.psp));
        mPayDownViewTSP.setmCurrentProgress(getProgress(contribution.tsp));
        mPayDownViewTSE.setmCurrentProgress(getProgress(contribution.tse));
        mPayDownViewTSL.setmCurrentProgress(getProgress(contribution.tsl));
        mPayDownViewPCP.setmCurrentProgress(getProgress(contribution.pcp));

        mTvPSP.setText(contribution.psp.value);
        mTvTSP.setText(contribution.tsp.value);
        mTvTSE.setText(contribution.tse.value);
        mTvTSL.setText(contribution.tsl.value);
        mTvPCP.setText(contribution.pcp.value);
    }

    private float getProgress(ContributionType contributionType) {
        if (contributionType!=null){
            float f =contributionType.minFillValue / Float.parseFloat( contributionType.value);
            return  (360 / f);
        }
        return 0;
    }

}
