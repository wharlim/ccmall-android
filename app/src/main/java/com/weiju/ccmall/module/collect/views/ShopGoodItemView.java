package com.weiju.ccmall.module.collect.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

public class ShopGoodItemView extends LinearLayout {

    private SimpleDraweeView goodIcon;
    private TextView goodPrice;
    private TextView goodCCM;

    public ShopGoodItemView(Context context) {
        super(context);
    }

    public ShopGoodItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ShopGoodItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        goodIcon = findViewById(R.id.goodIcon);
        goodPrice = findViewById(R.id.goodPrice);
        goodCCM = findViewById(R.id.goodCCM);
    }

    public void setSkuInfo(SkuInfo skuInfo) {
        FrescoUtil.setSkuImgMaskSmall(goodIcon, skuInfo);
        goodPrice.setText(MoneyUtil.centToYuan¥StrNoZero(skuInfo.retailPrice));
        goodCCM.setText(String.format("奖%s%%算率", skuInfo.countRateExc));
    }
}
