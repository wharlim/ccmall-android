package com.weiju.ccmall.module.auth.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class AuthModel {

    public MemberAuthBean memberAuthBean;

    public MemberAccountBean memberAccountBean;

    public class MemberAuthBean implements Serializable {
        @SerializedName("memberId")
        public String memberId;
        @SerializedName("authStatus")
        public int authStatus;
        @SerializedName("userName")
        public String userName;
        @SerializedName("identityCard")
        public String identityCard;
        @SerializedName("idcardFrontImg")
        public String idcardFrontImg;
        @SerializedName("idcardBackImg")
        public String idcardBackImg;
        @SerializedName("idcardHeadImg")
        public String idcardHeadImg;
        @SerializedName("authRemark")
        public String authRemark;
        @SerializedName("checkRemark")
        public String checkRemark;
        @SerializedName("autoAuthStatus")
        public int autoAuthStatus;

        public String getMemberId() {
            return memberId;
        }

        public int getAuthStatus() {
            return authStatus;
        }

        public String getUserName() {
            return userName;
        }

        public String getIdentityCard() {
            return identityCard;
        }

        public String getIdcardFrontImg() {
            return idcardFrontImg;
        }

        public String getIdcardBackImg() {
            return idcardBackImg;
        }

        public String getIdcardHeadImg() {
            return idcardHeadImg;
        }

        public String getAuthRemark() {
            return authRemark;
        }

        public String getCheckRemark() {
            return checkRemark;
        }

        public int getAutoAuthStatus(){return autoAuthStatus;}
    }

    public class MemberAccountBean implements Serializable{
        @SerializedName("accountId")
        public String accountId;
        @SerializedName("phone")
        public String phone;
        @SerializedName("accountType")
        public int accountType;
        @SerializedName("bankId")
        public String bankId;
        @SerializedName("bankAccount")
        public String bankAccount;
        @SerializedName("bankUser")
        public String bankUser;
        @SerializedName("alipayAccount")
        public String alipayAccount;
        @SerializedName("alipayUser")
        public String alipayUser;
        @SerializedName("wechatAccount")
        public String wechatAccount;
        @SerializedName("wechatUser")
        public String wechatUser;
        @SerializedName("bankName")
        public String bankName;
        @SerializedName("bankLogo")
        public String bankLogo;
        @SerializedName("accountStatus")
        public int accountStatus;
        @SerializedName("idcardFrontImg")
        public String idcardFrontImg;
        @SerializedName("idcardBackImg")
        public String idcardBackImg;
        @SerializedName("idcardHeadImg")
        public String idcardHeadImg;
        @SerializedName("bankcardFrontImg")
        public String bankcardFrontImg;
        @SerializedName("bankcardProvince")
        public String bankcardProvince;
        @SerializedName("bankcardCity")
        public String bankcardCity;
        @SerializedName("bankcardArea")
        public String bankcardArea;
        @SerializedName("bankcardAddress")
        public String bankcardAddress;
        @SerializedName("bankcardCode")
        public String bankcardCode;
        @SerializedName("checkDate")
        public String checkDate;
        @SerializedName("checkResult")
        public String checkResult;
        @SerializedName("remark")
        public String remark;
        @SerializedName("bankReservedPhone")
        public String bankReservedPhone;
        @SerializedName("identityCard")
        public String identityCard;
    }

}
