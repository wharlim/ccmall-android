package com.weiju.ccmall.module.community;


import android.content.Intent;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.weiju.ccmall.R;
import com.weiju.ccmall.databinding.ViewImageBinding;
import com.weiju.ccmall.module.community.models.CoursePlate;
import com.weiju.ccmall.newRetail.activity.CommunityActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.Timber;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * @author Stone
 * @time 2018/3/16  14:21
 * @desc ${TODD}
 */

public class CommunityCourseFragment extends BaseFragment {


    @BindView(R.id.bannerView)
    BannerView mBannerView;
    @BindView(R.id.tabLayout)
    SmartTabLayout mTabLayout;
    @BindView(R.id.viewpgager_content)
    ViewPager mViewpager;
    private CourseBannerAdapter adapter = new CourseBannerAdapter();
    private ICommunityService mPageService;

    private int plateIndex;

    @Override
    protected int getFragmentResId() {
        return R.layout.activity_course;
    }

    @Override
    protected void initViewConfig() {
        super.initViewConfig();
        plateIndex = getArguments().getInt("plateIndex");
        //标题字体颜色
        mTabLayout.setTitleTextColor(ContextCompat.getColor(mActivity, R.color.colorPrimary),
                ContextCompat.getColor(mActivity, R.color.color_33));
        //滑动条宽度
        mTabLayout.setTabTitleTextSize(14);
        mTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(mActivity, R.color.colorPrimary));
        mTabLayout.setDistributeEvenly(true);
    }

    public TabViewPagerAdapter getTabAdapter() {
        return new TabViewPagerAdapter(mActivity, getChildFragmentManager(),
                TabViewPagerFragment.TabViewPageAdapterTag.SCHOOL, coursePlate().courseCats);
    }

    @Override
    protected void lazyLoadData() {
        super.lazyLoadData();
        mPageService = ServiceManager.getInstance().createService(ICommunityService.class);
        if (!SessionUtil.getInstance().isLogin()) {
            isDataInitiated = false;
        } else {
            TabViewPagerAdapter tabAdapter = getTabAdapter();
            mViewpager.setAdapter(tabAdapter);
            mTabLayout.setTabStripWidth(tabAdapter.getBottomLineWidth());
            mViewpager.setOffscreenPageLimit(tabAdapter.getCount());
            mTabLayout.setViewPager(mViewpager);
            requestBanner();
        }
    }

    @Override
    protected void initListener() {
        super.initListener();
    }

    private void requestBanner() {
        APIManager.startRequest(mPageService.getCourseBannerList(coursePlate().plateId), new BaseRequestListener<ArrayList<Course>>() {
            @Override
            public void onSuccess(ArrayList<Course> result) {
                super.onSuccess(result);
                if (result != null) {
                    adapter.setDatalist(result);
                    mBannerView.setAdapter(adapter);
                }
            }
        },getContext());
    }

    @Override
    public void onResume() {
        super.onResume();
        Timber.INSTANCE.e("onResume");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        Timber.INSTANCE.e("isVisibleToUser = " + isVisibleToUser);
        super.setUserVisibleHint(isVisibleToUser);
    }

    public class CourseBannerAdapter extends SimplePagerAdapter<Course> {
        @Override
        protected int getLayoutId(int postion) {
            return R.layout.view_image;
        }

        @Override
        protected void onBindData(ViewDataBinding binding, final Course data, int position) {
            ViewImageBinding b = (ViewImageBinding) binding;
            b.ivImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toDetail(data);
                }
            });
            String url = data.banner;
            ImageView iv = b.ivImage;
            Glide.with(mActivity).load(url).into(iv);
            binding.executePendingBindings();
        }
    }

    private void toDetail(Course course) {
        Intent intent = new Intent();
        if (course.courseType == Course.MEDIA) {
            intent.setClass(mActivity, VoiceDetailActivity.class);
        } else if (course.courseType == Course.TEXT) {
            intent.setClass(mActivity, ArticleDetailActivity.class);
        } else {
            intent.setClass(mActivity, VideoDetailActivity.class);
        }
        intent.putExtra(Constants.Extras.COURSE, course);
        startActivity(intent);
    }

    @Override
    public boolean needLogin() {
        return true;
    }

    private CoursePlate coursePlate() {
        return CommunityActivity.plates.get(plateIndex);
    }

    public static CommunityCourseFragment newInstance(int plateIndex) {
        Bundle args = new Bundle();
        args.putInt("plateIndex", plateIndex);
        CommunityCourseFragment fragment = new CommunityCourseFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
