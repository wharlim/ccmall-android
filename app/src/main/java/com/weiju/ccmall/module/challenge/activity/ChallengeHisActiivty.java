package com.weiju.ccmall.module.challenge.activity;

import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.challenge.adapter.ChallengeHisAdapter;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Challenge;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IChallengeService;

/**
 * @author chenyanming
 * @time 2019/1/28 on 17:19
 * @desc 挑战记录
 */
public class ChallengeHisActiivty extends BaseListActivity {
    private ChallengeHisAdapter mAdapter = new ChallengeHisAdapter(null);

    private IChallengeService mService = ServiceManager.getInstance().createService(IChallengeService.class);

    @Override
    public String getTitleStr() {
        return "挑战记录";
    }

    @Override
    public void initView() {
        super.initView();
//        mAdapter.setEmptyView(getEmptyView());
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.background));
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        Challenge item = mAdapter.getItem(position);
        if (null != item && item.type > 1) {
            Intent intent = new Intent(ChallengeHisActiivty.this, ChallengePKDetailActivity.class);
            intent.putExtra("activityId", item.activityId);
            intent.putExtra("challengeId",item.challengeId);
            intent.putExtra("isHis",true);
            startActivity(intent);

        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(final boolean isRefresh) {
        APIManager.startRequest(mService.getLogList(mCurrentPage, 15),
                new BaseRequestListener<PaginationEntity<Challenge, Object>>(this) {
                    @Override
                    public void onSuccess(PaginationEntity<Challenge, Object> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && result.list.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mRefreshLayout.setRefreshing(false);
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        mRefreshLayout.setRefreshing(false);
                    }
                },this);
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }
}
