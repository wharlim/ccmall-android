package com.weiju.ccmall.module.xysh.adapter;

import android.util.Log;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.ReceiptSupportBankEntity;
import com.weiju.ccmall.module.xysh.fragment.ReceiptSupportBanksFragment;

public class ReceiptSupportBankAdapter extends BaseQuickAdapter<ReceiptSupportBankEntity, BaseViewHolder> {

    private int type;

    public ReceiptSupportBankAdapter(int type) {
        super(R.layout.item_receipt_support_banks);
        this.type = type;
    }

    @Override
    protected void convert(BaseViewHolder helper, ReceiptSupportBankEntity item) {
        if (type == ReceiptSupportBanksFragment.TYPE_PAY) {
            ReceiptSupportBanksFragment.setHeadView((ViewGroup) helper.itemView,
                    item.bankName,
                    "信用卡",
                    item.single+"元",
                    item.currentDay+"元"
            );
        } else if (type == ReceiptSupportBanksFragment.TYPE_RECEIPT) {
            ReceiptSupportBanksFragment.setHeadView((ViewGroup) helper.itemView,item.bankName, "储蓄卡");
        }
    }
}
