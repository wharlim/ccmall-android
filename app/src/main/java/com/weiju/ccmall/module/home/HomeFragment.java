package com.weiju.ccmall.module.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.cart.CartActivity;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.search.SearchActivity;
import com.weiju.ccmall.module.user.ChangeNetWorkTypeActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.contracts.RequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.page.CustomPageFragment;
import com.weiju.ccmall.shared.page.PageType;
import com.weiju.ccmall.shared.service.contract.IPageService;
import com.weiju.ccmall.shared.util.BaseUrl;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

public class HomeFragment extends BaseFragment {

    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;
    @BindView(R.id.magicIndicator)
    protected MagicIndicator mMagicIndicator;
    @BindView(R.id.linkToBeShopkeeper)
    LinearLayout mLinkToBeShopkeeper;
    @BindView(R.id.ivNoData)
    ImageView mIvNoData;
    @BindView(R.id.tvNoData)
    TextView mTvNoData;
    @BindView(R.id.tvGoMain)
    TextView mTvNoDataBtn;
    @BindView(R.id.layoutNodata)
    LinearLayout mLayoutNodata;
    @BindView(R.id.btChangeNetworkType)
    Button btChangeNetworkType;
    /*@BindView(R.id.bt_in)
    Button bt_in;*/
    @BindView(R.id.ivCart)
    ImageView ivCard;

    @BindView(R.id.barPading)
    View mBarPading;

    @BindView(R.id.tvNowType)
    TextView tvNowType;

    @BindView(R.id.searchLayout)
    public TextView searchLayout;

    protected ArrayList<Page> pages = new ArrayList<>();
    protected HashMap<String, CustomPageFragment> fragments = new HashMap<>();

    private FragmentStatePagerAdapter fragmentStatePagerAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootView);
        EventBus.getDefault().register(this);

        initView();
        initNoData();
        initPages();
        initData();

        btChangeNetworkType.setVisibility(shouldSwitchEnv() ?  View.VISIBLE: View.GONE);
        tvNowType.setVisibility(shouldSwitchEnv() ? View.VISIBLE : View.GONE);
        return rootView;
    }

    protected boolean shouldSwitchEnv() {
        return BuildConfig.DEBUG;
    }

    protected PageType getPageType() {
        return PageType.HOME;
    }

    private void initView() {
        //导航栏高度
        int height = QMUIStatusBarHelper.getStatusbarHeight(getContext());
        mBarPading.getLayoutParams().height = height;
        ivCard.setVisibility(getPageType() == PageType.HOME ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initData() {
        tvNowType.setText("当前环境：" + BaseUrl.getInstance().getNetworkTypeString());
        IPageService pageService = ServiceManager.getInstance().createService(IPageService.class);
        Observable<RequestResult<List<Page>>> api = pageService.getPageList();
        if (getPageType() == PageType.JKP) {
            api = pageService.getTkPageList();
        }
        APIManager.startRequest(api, new RequestListener<List<Page>>() {
            @Override
            public void onStart() {
                ToastUtil.showLoading(getActivity());
            }

            @Override
            public void onSuccess(List<Page> list) {
                mLayoutNodata.setVisibility(View.GONE);
                pages.addAll(list);
                if (fragmentStatePagerAdapter != null) {
                    fragmentStatePagerAdapter.notifyDataSetChanged();
                }

                initViewPager();
//                if (isAdded()) {
//                    initViewPager();
//                }
                initIndicator();
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.hideLoading();
                mLayoutNodata.setVisibility(View.VISIBLE);
            }

            @Override
            public void onComplete() {
                ToastUtil.hideLoading();
            }
        }, getContext());
    }

    private void initNoData() {
        mIvNoData.setImageResource(R.mipmap.no_data_normal);
        mTvNoData.setText("这个页面去火星了");
        mTvNoDataBtn.setText("刷新看看");
        mTvNoDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initPages();
                initData();
            }
        });
    }

    private void initPages() {
        pages.clear();
        if (fragmentStatePagerAdapter != null) {
            fragmentStatePagerAdapter.notifyDataSetChanged();
        }
        fragments.clear();


        Page homePage = new Page();
        homePage.id = Key.PAGE_HOME;
        homePage.name = "首页";
        CustomPageFragment homePageFragment = CustomPageFragment.newInstance(Key.PAGE_HOME, getPageType());
        homePageFragment.setOnLoadPageConfigListener(getOnLoadPageConfigListener());
        fragments.put(Key.PAGE_HOME, homePageFragment);
        pages.add(homePage);

        if (fragmentStatePagerAdapter != null) {
            fragmentStatePagerAdapter.notifyDataSetChanged();
        }
    }

    private void initViewPager() {
        if (!isAdded()) return;
        fragmentStatePagerAdapter = new FragmentStatePagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                Page page = pages.get(position);
                CustomPageFragment fragment = fragments.get(page.id);
                if (fragment == null) {
                    fragment = CustomPageFragment.newInstance(page.id, getPageType());
                    fragments.put(page.id, fragment);
                }
                return fragment;
            }

            @Override
            public int getCount() {
                return pages.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return pages.get(position).name;
            }
        };

        mViewPager.setAdapter(fragmentStatePagerAdapter);
        mViewPager.setOffscreenPageLimit(pages.size());
        mViewPager.setCurrentItem(0);
    }

    @Override
    public void onResume() {
        super.onResume();
        tvNowType.setText("当前环境：" + BaseUrl.getInstance().getNetworkTypeString());
    }

    private void initIndicator() {
        // 当前fragment已经和Activity解绑，不应该再做UI处理
        if (getActivity() == null || !isAdded()) {
            return;
        }
        CommonNavigator commonNavigator = new CommonNavigator(getActivity());
        commonNavigator.setLeftPadding(ConvertUtil.dip2px(20));
        commonNavigator.setRightPadding(ConvertUtil.dip2px(20));
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return pages.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(pages.get(index).name);
                titleView.setNormalColor(getResources().getColor(R.color.default_text_color));
                titleView.setSelectedColor(getResources().getColor(R.color.red));
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(14);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getResources().getColor(R.color.red));
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                return indicator;
            }
        });
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    @OnClick(R.id.searchLayout)
    protected void clickSearchLayout() {
        Intent intent = new Intent(getContext(), SearchActivity.class);
        intent.putExtra("pageType", getPageType().name());
        startActivity(intent);
    }

    @OnClick(R.id.ivCart)
    protected void cart() {
        if (UiUtils.checkUserLogin(getContext())) {
            startActivity(new Intent(getContext(), CartActivity.class));
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case logout:
            case loginSuccess:
                initPages();
                initData();
                break;
            default:
        }
    }

    @OnClick(R.id.btChangeNetworkType)
    public void changeNetWorkType() {
//        startActivity(new Intent(getContext(), IMTestActivity.class));
        startActivity(new Intent(getContext(), ChangeNetWorkTypeActivity.class));
    }

    protected CustomPageFragment.OnLoadPageConfigListener getOnLoadPageConfigListener() {
        return null;
    }

    @OnClick(R.id.bt_test)
    public void in() {
        Intent intent = new Intent(getContext(), WebViewJavaActivity.class);
        intent.putExtra("url", "http://credit.ccmallv2.create-chain.net/customizYTH/#/SmartLink");
        startActivity(intent);
    }
}
