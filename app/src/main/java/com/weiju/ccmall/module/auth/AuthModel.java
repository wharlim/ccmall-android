package com.weiju.ccmall.module.auth;

import com.google.gson.annotations.SerializedName;

public class AuthModel {


    @SerializedName("memberId")
    public String memberId;
    @SerializedName("authStatus")
    public int authStatus;
    @SerializedName("userName")
    public String userName;
    @SerializedName("identityCard")
    public String identityCard;
    @SerializedName("idcardFrontImg")
    public String idcardFrontImg;
    @SerializedName("idcardBackImg")
    public String idcardBackImg;
    @SerializedName("idcardHeadImg")
    public String idcardHeadImg;
    @SerializedName("authRemark")
    public String authRemark;
    @SerializedName("checkRemark")
    public String checkRemark;

    public String getMemberId() {
        return memberId;
    }

    public int getAuthStatus() {
        return authStatus;
    }

    public String getUserName() {
        return userName;
    }

    public String getIdentityCard() {
        return identityCard;
    }

    public String getIdcardFrontImg() {
        return idcardFrontImg;
    }

    public String getIdcardBackImg() {
        return idcardBackImg;
    }

    public String getIdcardHeadImg() {
        return idcardHeadImg;
    }

    public String getAuthRemark() {
        return authRemark;
    }

    public String getCheckRemark() {
        return checkRemark;
    }
}
