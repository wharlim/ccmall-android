package com.weiju.ccmall.module.live.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.entity.ProductEntity;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

import java.util.List;

public class GoodsManagementAdapter extends BaseQuickAdapter<ProductEntity, BaseViewHolder> {

    private int mType;

    public GoodsManagementAdapter(@Nullable List<ProductEntity> data, int type) {
        super(R.layout.item_goods_management, data);
        mType = type;
    }

    @Override
    protected void convert(BaseViewHolder helper, ProductEntity item) {
        helper.addOnClickListener(R.id.tvDelete);
        helper.addOnClickListener(R.id.tvEdit);
        helper.addOnClickListener(R.id.tvStandUpOrDown);
        helper.addOnClickListener(R.id.tvPreview);
        FrescoUtil.loadRvItemImg(helper, R.id.ivImage, item.thumbUrl);
        helper.setText(R.id.tvStandUpOrDown, mType == 1 ? "下架" : "上架");

        helper.setText(R.id.tvTitle, item.productName)
                .setText(R.id.tvPrice, MoneyUtil.centToYuan¥Str(item.retailPrice))
//                .setText(R.id.tvMarketPrice, MoneyUtil.centToYuan¥Str(item.marketPrice))
                .setText(R.id.tvStock, "库存：" + item.stock)
                .setText(R.id.tvCount, "销量：" + item.saleCount);
    }
}
