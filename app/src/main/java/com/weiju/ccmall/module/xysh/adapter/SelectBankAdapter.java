package com.weiju.ccmall.module.xysh.adapter;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.BankItem;

import java.util.List;

/**
 * @author chenyanming
 * @time 2019/6/3 on 15:47
 * @desc ${TODD}
 */
public class SelectBankAdapter extends BaseMultiItemQuickAdapter<BankItem, BaseViewHolder> {

    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public SelectBankAdapter(List<BankItem> data) {
        super(data);
        addItemType(BankItem.TITLE, R.layout.item_bank_title);
        addItemType(BankItem.ITEM, R.layout.item_bank);
    }

    @Override
    protected void convert(BaseViewHolder helper, BankItem item) {
        switch (helper.getItemViewType()) {
            case BankItem.TITLE:
                helper.setText(R.id.tvTitle, item.title);
                break;
            case BankItem.ITEM:
                helper.setText(R.id.tvBankName,item.mItem.bankName);
                break;
            default:
        }
    }
}
