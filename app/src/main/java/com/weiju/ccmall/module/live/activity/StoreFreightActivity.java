package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.adapter.StoreFreightAdapter;
import com.weiju.ccmall.module.live.entity.LiveStoreFreightEntity;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.StoreFreight;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/4/16.
 */
public class StoreFreightActivity extends BaseListActivity {

    @BindView(R.id.tvSave)
    TextView mTvSave;
    @BindView(R.id.tvTemplatedTips)
    TextView mTvTemplatedTips;
    private boolean mIsEdit;
    private List<StoreFreight> mData = new ArrayList<>();
    private StoreFreightAdapter mAdapter;
    ILiveStoreService mService;

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_store_freight;
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mIsEdit = getIntent().getBooleanExtra("isEdit", false);
    }

    @Override
    public void initView() {
        mAdapter = new StoreFreightAdapter(mData, mIsEdit);
        super.initView();
        mRefreshLayout.setRefreshing(false);
        mRefreshLayout.setEnabled(false);
        mService = ServiceManager.getInstance().createService(ILiveStoreService.class);
        mTvSave.setVisibility(mIsEdit ? View.VISIBLE : View.GONE);
        if (!mIsEdit) {
            getHeaderLayout().setRightText("去修改运费");
            getHeaderLayout().setRightTextColor(R.color.color_2094FE);
            getHeaderLayout().setOnRightClickListener(v -> {
                if (mIsEdit) return;
                getHeaderLayout().setRightText("");
                mIsEdit = true;
                mAdapter = new StoreFreightAdapter(mData, mIsEdit);
                mRecyclerView.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
                mTvSave.setVisibility(View.VISIBLE);
            });
        }
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public boolean isLoadMore() {
        return false;
    }

    @Override
    public String getTitleStr() {
        return "店铺运费配置";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mService.getStoreFreight(), new BaseRequestListener<LiveStoreFreightEntity>(this) {
            @Override
            public void onSuccess(LiveStoreFreightEntity result) {
                super.onSuccess(result);
                if (result.templated) {
                    mTvTemplatedTips.setVisibility(View.VISIBLE);
                    mTvTemplatedTips.setText(Html.fromHtml(result.templatedTips));
                }
                mData.clear();
                mData.addAll(result.freightList);
                mAdapter.notifyDataSetChanged();
            }
        }, this);
    }

    @OnClick(R.id.tvSave)
    public void onViewClicked() {
        APIManager.startRequest(mService.saveStoreFreightBatch(mAdapter.getData()), new BaseRequestListener<Object>(this) {

            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("保存成功");
                finish();
//                getHeaderLayout().setRightText("去修改运费");
//                mIsEdit = false;
//                mAdapter = new StoreFreightAdapter(mData, mIsEdit);
//                mRecyclerView.setAdapter(mAdapter);
//                getData(true);
//                mTvSave.setVisibility(View.GONE);
            }
        }, this);
    }

    public static void start(Context context, boolean isEdit) {
        Intent intent = new Intent(context, StoreFreightActivity.class);
        intent.putExtra("isEdit", isEdit);
        context.startActivity(intent);
    }
}
