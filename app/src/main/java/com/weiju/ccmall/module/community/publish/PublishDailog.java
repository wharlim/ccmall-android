package com.weiju.ccmall.module.community.publish;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.util.CommonUtil;


/**
 * @author Stone
 * @time 2018/4/12  11:18
 * @desc ${TODD}
 */

public class PublishDailog extends Dialog {
    private onClickCallBack callBack;
    private View mVideo;
    private View mLinke;
    private boolean mIsLinkModel;
    private View mTakePie;
    private View mTakePieLink;
    private View mTakeLiveLayout;
    private View mLiveHistory;
    private View mLiveNotice;
    private TextView mTvCount;
    private OnLiveCallBack mOnLiveCallBack;
    private View mPublishHistoryLayout;

    public PublishDailog(@NonNull Context context) {
        super(context, R.style.Theme_Light_Dialog);
    }

    public PublishDailog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected PublishDailog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_publish_new);
        setCanceledOnTouchOutside(false);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        findViewById(R.id.iv_cancel_dialog).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        mTakePie = findViewById(R.id.take_pic_layout);
        mTakePie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (callBack != null) {
                    callBack.onTakePic(false);
                }
            }
        });

        mTakePieLink = findViewById(R.id.take_pic_link_layout);
        mTakePieLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (callBack != null) {
                    callBack.onTakePic(true);
                }
            }
        });

        mVideo = findViewById(R.id.take_video_layout);
        mVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (callBack != null) {
                    callBack.onTakeVideo();
                }
            }
        });
        mPublishHistoryLayout = findViewById(R.id.publish_history_layout);
        findViewById(R.id.publish_history_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (callBack != null) {
                    callBack.onHistoryClick(mIsLinkModel);
                }
            }
        });
        mLinke = findViewById(R.id.take_link_layout);
        mLinke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                if (callBack != null) {
                    callBack.onTakeLink();
                }
            }
        });

        mTakeLiveLayout = findViewById(R.id.take_live_layout);
        mTakeLiveLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (null != mOnLiveCallBack) {
                    mOnLiveCallBack.onTakeLive();
                }
            }
        });

        mLiveHistory = findViewById(R.id.live_history_layout);
        mLiveHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (null != mOnLiveCallBack) {
                    mOnLiveCallBack.onLiveHistory();
                }
            }
        });
        mLiveNotice = findViewById(R.id.live_notice);
        mTvCount = findViewById(R.id.tvCount);
        mLiveNotice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (null != mOnLiveCallBack) {
                    mOnLiveCallBack.onLiveNotice();
                }
            }
        });
    }

    public void setOnClickCallBack(onClickCallBack callBack) {
        this.callBack = callBack;
    }

    public void setOnLiveCallBack(OnLiveCallBack onLiveCallBack) {
        mOnLiveCallBack = onLiveCallBack;
    }

    public void setLinkModel(boolean isTrue) {
        mIsLinkModel = isTrue;
        setView();
    }

    public void setNoticeCount(int num) {
        mTvCount.setVisibility(num > 0 ? View.VISIBLE : View.GONE);
        mTvCount.setText(num > 99 ? "99+" : "" + num);
    }

    public void setLiveView() {
        mLinke.setVisibility(View.GONE);
        mTakePieLink.setVisibility(View.GONE);
        mVideo.setVisibility(View.GONE);
        mTakePie.setVisibility(View.GONE);
        mTakeLiveLayout.setVisibility(View.VISIBLE);
        mLiveHistory.setVisibility(View.GONE);
        mLiveNotice.setVisibility(View.VISIBLE);
        mPublishHistoryLayout.setVisibility(View.GONE);
    }

    private void setView() {
        mLinke.setVisibility(mIsLinkModel ? View.VISIBLE : View.GONE);
        mTakePieLink.setVisibility(mIsLinkModel ? View.VISIBLE : View.GONE);
        mVideo.setVisibility(mIsLinkModel ? View.GONE : View.VISIBLE);
        mTakePie.setVisibility(mIsLinkModel ? View.GONE : View.VISIBLE);
    }

    public interface onClickCallBack {
        void onTakePic(boolean linkeModel);

        void onTakeVideo();

        void onHistoryClick(boolean likeModel);

        void onTakeLink();
    }

    public interface OnLiveCallBack {
        void onTakeLive();

        void onLiveHistory();
        void onLiveNotice();
    }
}
