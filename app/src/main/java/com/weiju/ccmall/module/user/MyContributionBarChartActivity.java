package com.weiju.ccmall.module.user;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.balance.BalanceListActivity;
import com.weiju.ccmall.module.community.DateUtils;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Contribution;
import com.weiju.ccmall.shared.bean.ContributionData;
import com.weiju.ccmall.shared.enums.AccountType;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/6/20 on 14:29
 * @desc ${TODD}
 */
public class MyContributionBarChartActivity extends BaseActivity {

    @BindView(R.id.tvPSPVaule)
    TextView mTvPSPVaule;
    @BindView(R.id.barChartPSP)
    BarChart mBarChartPSP;
    @BindView(R.id.barChartTSP)
    BarChart mBarChartTSP;
    @BindView(R.id.tvTSP)
    TextView mTvTSP;
    @BindView(R.id.tvTSPVaule)
    TextView mTvTSPVaule;
    @BindView(R.id.tvTSE)
    TextView mTvTSE;
    @BindView(R.id.tvTSL)
    TextView mTvTSL;
    @BindView(R.id.tvPCP)
    TextView mTvPCP;

    private XAxis xAxisPSP;
    private XAxis xAxisTSP;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contribution_barchart);
        ButterKnife.bind(this);
        initView();
        initData();

    }

    private void initView() {
        setLeftBlack();
        setTitle("我的贡献值");
        getHeaderLayout().setRightText("排行榜");
        getHeaderLayout().setOnRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyContributionBarChartActivity.this, MyContributionSortListActivity.class));

            }
        });

        xAxisPSP = mBarChartPSP.getXAxis();
        xAxisTSP = mBarChartTSP.getXAxis();
        initBarChart(mBarChartPSP, xAxisPSP);
        initBarChart(mBarChartTSP, xAxisTSP);
    }

    private void initData() {
        IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(mUserService.getContribution(), new BaseRequestListener<Contribution>(this) {
            @Override
            public void onSuccess(Contribution contribution) {
                super.onSuccess(contribution);
                showBarChart(mBarChartPSP, contribution.psp.list, R.color.barchart_psp, xAxisPSP);
                showBarChart(mBarChartTSP, contribution.tsp.list, R.color.barchart_tsp, xAxisTSP);
                mTvPSPVaule.setText(contribution.psp.value);
                mTvTSPVaule.setText(contribution.tsp.value);
                mTvTSE.setText(contribution.tse.value);
                mTvTSL.setText(contribution.tsl.value);
                mTvPCP.setText(contribution.pcp.value);
            }
        },this);

    }

    /**
     * 初始化BarChart图表
     */
    private void initBarChart(BarChart barChart, XAxis xAxisPSP) {
        /***图表设置***/
        //背景颜色
        barChart.setBackgroundColor(Color.WHITE);
        //不显示图表网格
        barChart.setDrawGridBackground(false);
        //背景阴影
        barChart.setDrawBarShadow(false);
        barChart.setHighlightFullBarEnabled(false);
        barChart.setDoubleTapToZoomEnabled(false);
        //禁止拖拽
        barChart.setDragEnabled(false);
        //不显示边框
        barChart.setDrawBorders(false);

        //不显示右下角描述内容
        Description description = new Description();
        description.setEnabled(false);
        barChart.setDescription(description);


        /***XY轴的设置***/
        //X轴设置显示位置在底部
        xAxisPSP.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxisPSP.setGranularity(1f);

        YAxis rightAxis = barChart.getAxisRight();
        rightAxis.setEnabled(false);
        rightAxis.setDrawAxisLine(false);
        rightAxis.setAxisMinimum(0);

        YAxis leftAxis = barChart.getAxisLeft();

        //不绘制X Y轴线条
        leftAxis.setDrawAxisLine(false);
        leftAxis.setAxisMinimum(0);

        //不显示X轴网格线
        xAxisPSP.setDrawGridLines(false);
        //左侧Y轴网格线设置为虚线
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setTextColor(getResources().getColor(R.color.text_gray));

        //禁止所有事件
        barChart.setTouchEnabled(false);
    }


    /**
     * //     * @param xValues   X轴的值
     * //     * @param dataLists LinkedHashMap<String, List<Float>>
     * //     *                  key对应柱状图名字  List<Float> 对应每类柱状图的Y值
     *
     * @param barChart
     * @param list
     * @param color
     * @param xAxisPSP
     */
    public void showBarChart(BarChart barChart, List<ContributionData> list, int color, XAxis xAxisPSP) {
        Collections.sort(list, (a, b) -> a.createDate.compareToIgnoreCase(b.createDate));
        List<IBarDataSet> dataSets = new ArrayList<>();

        List<BarEntry> entries = new ArrayList<>();
        int index = 0;
        List<String> dates = new ArrayList<>();

        for (ContributionData contributionData : list) {
            dates.add(DateUtils.getMonthDay(contributionData.createDate + " 00:00:00"));
            BarEntry barEntry = new BarEntry(index, contributionData.point / 100);
            entries.add(barEntry);
            index++;
        }
        BarDataSet barDataSet = new BarDataSet(entries, "");
        barDataSet.setDrawValues(false);
        barDataSet.setColor(getResources().getColor(color));
        barDataSet.setFormLineWidth(0f);
        barDataSet.setFormSize(0f);
        barDataSet.setAxisDependency(YAxis.AxisDependency.RIGHT);

        dataSets.add(barDataSet);
//
//        xAxisPSP.setValueFormatter(new ValueFormatter() {
//            @Override
//            public String getAxisLabel(float value, AxisBase axis) {
//                return super.getAxisLabel(value, axis);
//            }
//        });

        //X轴自定义值
        xAxisPSP.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                int position = (int) (value + 0.5);
                Log.v("value", position + "  ");
                if (position < list.size() && position >= 0 && position % 2 == 0) {
                    return DateUtils.getMonthDay(list.get(position).createDate + " 00:00:00");
                }
                return "";
            }
        });
        BarData data = new BarData(dataSets);

//        //设置柱状图宽度
        data.setBarWidth(0.5f);
        barChart.setData(data);


//        xAxisPSP.setAxisMinimum(0f);
//        xAxis.setAxisMaximum(list.size() );
        //将X轴的值显示在中央
        xAxisPSP.setCenterAxisLabels(true);
        xAxisPSP.setLabelCount(list.size() + 1, true);
        xAxisPSP.setTextColor(getResources().getColor(R.color.text_gray));

        barChart.notifyDataSetChanged();
        barChart.invalidate();
    }

    @OnClick(R.id.tvPSPDetail)
    protected void PSPDetail() {
        Intent intent = new Intent(MyContributionBarChartActivity.this, BalanceListActivity.class);
        intent.putExtra("type", AccountType.PSP);
        startActivity(intent);
    }

    @OnClick(R.id.tvTSPDetail)
    protected void TSPDetail() {
        Intent intent = new Intent(MyContributionBarChartActivity.this, BalanceListActivity.class);
        intent.putExtra("type", AccountType.TSP);
        startActivity(intent);
    }


    @OnClick(R.id.tvPCPDetail)
    protected void PCPDetail() {
//        Intent intent = new Intent(MyContributionBarChartActivity.this, BalanceListActivity.class);
//        intent.putExtra("type", AccountType.PCP);
//        startActivity(intent);
    }
}
