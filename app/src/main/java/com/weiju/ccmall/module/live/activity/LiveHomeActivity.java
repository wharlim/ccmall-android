package com.weiju.ccmall.module.live.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.publish.PublishDailog;
import com.weiju.ccmall.module.live.fragment.LiveHomeListFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashSet;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/11/20 on 18:20
 * @desc ${TODD}
 */
public class LiveHomeActivity extends BaseActivity {
    @BindView(R.id.layoutTopTitle)
    RelativeLayout mLayoutTitle;
    @BindView(R.id.ic_publish_new)
    ImageView mIcPublishNew;
    @BindView(R.id.avatarIv)
    ImageView avatarIv;
    @BindView(R.id.avatarIvContainer)
    View avatarIvContainer;

    @BindView(R.id.viewPager)
    ViewPager viewPager;

    @BindView(R.id.tvLiveTabRecommend)
    TextView tvLiveTabRecommend;
    @BindView(R.id.tvLiveTabFocus)
    TextView tvLiveTabFocus;

    private LiveUser liveUser;

    private Set<LiveHomeListFragment> listFragmentSet = new HashSet<>();
    private PublishDailog mDailog;

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_home);
        ButterKnife.bind(this);
        initView();
        initData();
        initViewPage();
    }

    private void initView() {
        EventBus.getDefault().register(this);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            QMUIStatusBarHelper.translucent(LiveHomeActivity.this);
//            QMUIStatusBarHelper.setStatusBarLightMode(LiveHomeActivity.this);
//            QMUIStatusBarHelper.setStatusBarDarkMode(LiveHomeActivity.this);
//        }
        getHeaderLayout().setVisibility(View.GONE);
//        int height = QMUIStatusBarHelper.getStatusbarHeight(LiveHomeActivity.this);
//        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mLayoutTitle.getLayoutParams();
//        layoutParams.setMargins(0, height, 0, 0);


        User user = SessionUtil.getInstance().getLoginUser();
        if (user != null) {
            if (!TextUtils.isEmpty(user.avatar)) {
                Glide.with(this).load(Uri.parse(user.avatar))
                        .apply(new RequestOptions().placeholder(R.mipmap.default_avatar))
                        .into(avatarIv);
            }
            avatarIv.setVisibility(View.VISIBLE);
        } else {
            avatarIv.setVisibility(View.INVISIBLE);
        }
    }

    private void initData() {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (null == loginUser) {
            return;
        }
        APIManager.startRequest(mService.getLiveUserInfo(loginUser.id), new BaseRequestListener<LiveUser>() {
            @Override
            public void onSuccess(LiveUser result) {
                liveUser = result;
                SessionUtil.getInstance().setLiveUser(result);
                mIcPublishNew.setVisibility(result.liveLimit == 1 ? View.VISIBLE : View.GONE);
                avatarIvContainer.setVisibility(result.liveLimit == 1? View.VISIBLE: View.GONE);
            }
        }, LiveHomeActivity.this);
    }

    private void initViewPage() {
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                return LiveHomeListFragment.newInstance(i);
            }

            @Override
            public int getCount() {
                return 2;
            }
        });
        viewPager.setCurrentItem(0);
        unselectAllTab();
        tvLiveTabRecommend.setSelected(true);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                unselectAllTab();
                switch (i) {
                    case 0:
                        tvLiveTabRecommend.setSelected(true);
                        break;
                    case 1:
                        tvLiveTabFocus.setSelected(true);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        Log.d("Seven", "attach fragment " + fragment);
        if (fragment instanceof LiveHomeListFragment) {
            listFragmentSet.add((LiveHomeListFragment) fragment);
        }
    }

    @OnClick(R.id.back)
    protected void back() {
        finish();
    }

    @OnClick(R.id.ic_publish_new)
    protected void publish() {
        if (UiUtils.checkUserLogin(this)) {
            if (mDailog == null) {
                mDailog = new PublishDailog(this);
                mDailog.setOnLiveCallBack(onClickCallBack);
            }
            mDailog.show();
            mDailog.setLiveView();
        }
    }
    @OnClick(R.id.avatarIvContainer)
    protected void myLive() {
        Log.d("Seven", "myLive " + liveUser);
        if (liveUser == null) {
            return;
        }
        MyLivesActivity.start(this, liveUser, false);
    }

    private void unselectAllTab() {
        tvLiveTabFocus.setSelected(false);
        tvLiveTabRecommend.setSelected(false);
    }

    @OnClick({R.id.tvLiveTabFocus, R.id.tvLiveTabRecommend})
    protected void onTab(View view) {
        unselectAllTab();
        view.setSelected(true);
        int id = view.getId();
        if (id == R.id.tvLiveTabFocus) {
            viewPager.setCurrentItem(1, true);
        } else if (id == R.id.tvLiveTabRecommend){
            viewPager.setCurrentItem(0, true);
        }
    }


    private PublishDailog.OnLiveCallBack onClickCallBack = new PublishDailog.OnLiveCallBack() {
        @Override
        public void onTakeLive() {
            startActivity(new Intent(LiveHomeActivity.this, CreatLiveRoomActivity.class));
        }

        @Override
        public void onLiveHistory() {
            startActivity(new Intent(LiveHomeActivity.this, LiveHistoryListActivity.class));
        }

        @Override
        public void onLiveNotice() {

        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case liveRoomChange:
                for (LiveHomeListFragment f :
                        listFragmentSet) {
                    f.getData(true);
                }
                break;
            case logout:
                finish();
                break;
            case ApplyPaySuccess:
                initData();
                break;
            default:
        }
    }
}
