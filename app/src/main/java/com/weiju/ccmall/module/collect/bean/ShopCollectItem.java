package com.weiju.ccmall.module.collect.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.bean.SkuInfo;

import java.util.List;

public class ShopCollectItem {

    @SerializedName("storeName")
    public String storeName;
    @SerializedName("storeId")
    public String storeId;
    @SerializedName("thumbUrl")
    public String thumbUrl;
    @SerializedName("collectStatus")
    public int collectStatus;
    @SerializedName("productList")
    public List<SkuInfo> productList;
}
