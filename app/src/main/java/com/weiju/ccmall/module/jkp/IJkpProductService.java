package com.weiju.ccmall.module.jkp;

import com.weiju.ccmall.module.jkp.newjkp.entity.BannerEntity;
import com.weiju.ccmall.module.jkp.newjkp.CategoryList;
import com.weiju.ccmall.module.jkp.newjkp.GoodsListByCategory;
import com.weiju.ccmall.shared.bean.JkpLevel;
import com.weiju.ccmall.shared.bean.JkpOriginalProduct;
import com.weiju.ccmall.shared.bean.JkpSearchResult;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IJkpProductService {

    String event = "native-jpk";

    @GET("tkproduct/goodsDetail")
    Observable<RequestResult<SkuInfo>> goodsDetail(@Query("goodsId") String goodsId);
    @GET("tkproduct/getUrl")
    Observable<RequestResult<JkpOriginalProduct>> getUrl(@Query("originId") String originId);

    @FormUrlEncoded
    @POST("tkproduct/superSearch")
    Observable<RequestResult<JkpSearchResult>> search(@Field("para")String para,
                                                      @Field("page")int page,
                                                      @Field("pagesize")int pagesize,
                                                      @Field("sort")String sort);

    @GET("user/checktkLevelStatus")
    Observable<RequestResult<JkpLevel>> checktkLevelStatus(@Query("memberId") String memberId);

    @GET("user/tkLevelTransform")
    Observable<RequestResult<JkpLevel>> tkLevelTransform(@Query("memberId") String memberId);

    @GET("tkproduct/getRelationInfo")
    Observable<RequestResult<Object>> getRelationInfo();

    @GET("tkproduct/saveRelationId")
    Observable<RequestResult<Object>> saveRelationId(@Query("sessionKey") String sessionKey);

    /** 新集靠谱商品分类接口*/
    @GET("tkProductNew/getCategoryList")
    Observable<RequestResult<List<CategoryList>>> getCategoryList();

    @GET("tkProductNew/getGoodsListByCategory")
    Observable<RequestResult<GoodsListByCategory>> getGoodsListByCategory(@Query("categoryId") String cid,
                                                                          @Query("pageOffset") int minId,
                                                                          @Query("pageSize") int pageSize);

    @GET("tkPage/activityHome")
    Observable<RequestResult<BannerEntity>> activityHome();

    @GET("tkPage/activityList")
    Observable<RequestResult<BannerEntity>> activityList();
}
