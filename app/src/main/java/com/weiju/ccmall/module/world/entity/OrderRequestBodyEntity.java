package com.weiju.ccmall.module.world.entity;

import java.util.List;

/**
 * @author Ben
 * @date 2020/5/8.
 */
public class OrderRequestBodyEntity {
    public String addressId;
    public int originType;  //1:app,2微信
    public List<Item> item;

    public static class Item {
        public String itemId;
        public int quantity;
    }
}
