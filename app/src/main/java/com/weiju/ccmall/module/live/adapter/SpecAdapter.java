package com.weiju.ccmall.module.live.adapter;

import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.entity.SpecEntity;
import com.weiju.ccmall.shared.component.DecimalEditText;
import com.weiju.ccmall.shared.util.MoneyUtil;

import java.util.List;

/**
 * @author Ben
 * @date 2020/4/15.
 */
public class SpecAdapter extends BaseQuickAdapter<SpecEntity, BaseViewHolder> {
    public SpecAdapter(@Nullable List<SpecEntity> data) {
        super(R.layout.item_spec, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, SpecEntity item) {
        helper.setText(R.id.tvName, item.getTitle());
        if (item.getRetailPrice() != null)
            helper.setText(R.id.etSellingPrice, MoneyUtil.centToYuanStrNoZero(item.getRetailPrice()));
        if (item.getMarketPrice() != null)
            helper.setText(R.id.etMarketPrice, MoneyUtil.centToYuanStrNoZero(item.getMarketPrice()));
        if (!TextUtils.isEmpty(item.getStock()))
            helper.setText(R.id.etStock, item.getStock());
        if (!TextUtils.isEmpty(item.getWeight()))
            helper.setText(R.id.etWeight, item.getWeight());

        DecimalEditText etSellingPrice = helper.getView(R.id.etSellingPrice);
        DecimalEditText etMarketPrice = helper.getView(R.id.etMarketPrice);
        EditText etStock = helper.getView(R.id.etStock);
        DecimalEditText etWeight = helper.getView(R.id.etWeight);

        initEdit(etSellingPrice, item, 0);
        initEdit(etMarketPrice, item, 1);
        initEdit(etStock, item, 2);
        initEdit(etWeight, item, 3);
        helper.setIsRecyclable(false);//禁止复用
    }

    private void initEdit(EditText editText, SpecEntity item, int type) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String inputValue = s.toString();
                switch (type) {
                    case 0:
                        item.setRetailPrice(inputValue);
                        break;
                    case 1:
                        item.setMarketPrice(inputValue);
                        break;
                    case 2:
                        item.setStock(inputValue);
                        break;
                    case 3:
                        item.setWeight(inputValue);
                        break;
                }
                if (listener != null) {
                    listener.afterTextChanged(inputValue);
                }
            }
        });
    }


    public interface OnEditTextAfterTextChangedListener {
        void afterTextChanged(String s);
    }

    private OnEditTextAfterTextChangedListener listener;

    public void setOnEditTextAfterTextChangedListener(OnEditTextAfterTextChangedListener listener) {
        this.listener = listener;
    }


}
