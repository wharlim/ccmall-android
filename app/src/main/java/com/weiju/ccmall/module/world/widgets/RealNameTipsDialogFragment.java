package com.weiju.ccmall.module.world.widgets;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.weiju.ccmall.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class RealNameTipsDialogFragment extends DialogFragment {

    @BindView(R.id.tvMsgContent)
    TextView tvMsgContent;
    Unbinder unbinder;

    public static RealNameTipsDialogFragment newInstance() {
        RealNameTipsDialogFragment fragment = new RealNameTipsDialogFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_realname_tips, container, false);
        unbinder = ButterKnife.bind(this, view);

        initMsgContent();
        return view;
    }

    private void initMsgContent() {
        Spanned spanned = Html.fromHtml("根据海关总署消息：为了便于过关手续办理，请在支付时，务必使用" +
                "<span style=\"color:#F10909;\">商城实名认证的本人微信号</span>进行支付。");
        tvMsgContent.setText(spanned);
    }

    @Override
    public void onStart() {
        super.onStart();
        // dialog was shown
        WindowManager.LayoutParams lp = getDialog().getWindow().getAttributes();
        // 设置背景颜色，去掉padding
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // 设置宽高全屏
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setAttributes(lp);
        setCancelable(true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    protected OnConfirmListener mConfirmListener;

    public interface OnConfirmListener {
        void confirm();
    }

    public void setOnConfirmListener(OnConfirmListener listener) {
        mConfirmListener = listener;
    }

    @OnClick(R.id.tvKnownIt)
    public void onViewClicked() {
        if (mConfirmListener != null) {
            mConfirmListener.confirm();
        }
        dismiss();
    }
}
