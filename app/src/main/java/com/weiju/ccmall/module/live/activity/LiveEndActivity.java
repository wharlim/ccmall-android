package com.weiju.ccmall.module.live.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.TimeUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/25 on 14:37
 * @desc
 */
public class LiveEndActivity extends BaseActivity {


    @BindView(R.id.ivAvatar)
    SimpleDraweeView mIvAvatar;
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.tvTime)
    TextView mTvTime;
    @BindView(R.id.layoutPlayLive)
    LinearLayout mLayoutPlayLive;
    @BindView(R.id.layoutLive)
    LinearLayout mLayoutLive;
    @BindView(R.id.tvOnLineNum)
    TextView mTvOnLineNum;
    @BindView(R.id.tvLikeNum)
    TextView mTvLikeNum;
    @BindView(R.id.tvFollow)
    TextView mTvFollow;

    private boolean mIsPlay;
    private String mLiveId;
    private String mHeadImage;

    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);
    private String mName;
    private int mFollowStatus;
    private LiveRoom mLiveRoom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_end);
        ButterKnife.bind(this);

        mIsPlay = getIntent().getBooleanExtra("isPlay", false);
        mLiveId = getIntent().getStringExtra("liveId");
        mHeadImage = getIntent().getStringExtra("headImage");
        mName = getIntent().getStringExtra("name");
        mFollowStatus = getIntent().getIntExtra("followStatus", 0);
        initView();
        initData();
    }

    private void initData() {
        APIManager.startRequest(mILiveService.getLive(mLiveId), new BaseRequestListener<LiveRoom>(LiveEndActivity.this) {
            @Override
            public void onSuccess(LiveRoom result) {
                mLiveRoom = result;
                mTvTime.setText(mIsPlay ? String.format("%s人看过", result.maxOnlineNum) :
                        String.format("直播时长%s", getLiveTime(result)));

                mTvOnLineNum.setText(String.valueOf(result.popularity));
                mTvLikeNum.setText(String.valueOf(result.likeNum));
            }
        }, LiveEndActivity.this);
    }

    private String getLiveTime(LiveRoom liveRoom) {
        liveRoom.endTime = TextUtils.isEmpty(liveRoom.endTime) ? TimeUtils.millis2String(TimeUtils.getNowTimeMills()) : liveRoom.endTime;
        long end = TimeUtils.string2Millis(liveRoom.endTime);
        long start = TimeUtils.string2Millis(liveRoom.startTime);
        long time = Math.abs(end - start);
        int hours = (int) (time / (1000 * 60 * 60));
        int minute = (int) (time / (1000 * 60) - (hours * 60));
        int seconds = (int) (time / (1000) - (minute * 60 + hours * 60 * 60));
        return String.format("%s:%s:%s", hours > 9 ? hours : "0" + hours, minute > 9 ?
                minute : "0" + minute, seconds > 9 ? seconds : "0" + seconds);

    }

    private void initView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            QMUIStatusBarHelper.translucent(LiveEndActivity.this);
            QMUIStatusBarHelper.setStatusBarLightMode(LiveEndActivity.this);
            QMUIStatusBarHelper.setStatusBarDarkMode(LiveEndActivity.this);
        }

        mLayoutPlayLive.setVisibility(mIsPlay ? View.VISIBLE : View.GONE);
        mLayoutLive.setVisibility(mIsPlay ? View.GONE : View.VISIBLE);
        FrescoUtil.setImage(mIvAvatar, mHeadImage);
        mTvFollow.setVisibility(mFollowStatus == 0 ? View.VISIBLE : View.GONE);

        mTvName.setText(mName);

    }

    @OnClick(R.id.tvFollow)
    protected void follow() {
        if (null != mLiveRoom) {
            APIManager.startRequest(mILiveService.followAnchor(mLiveRoom.memberId, mLiveRoom.liveId),
                    new BaseRequestListener<Object>(LiveEndActivity.this) {
                        @Override
                        public void onSuccess(Object result) {
                            mTvFollow.setVisibility(View.GONE);
                        }
                    },LiveEndActivity.this);
        }
    }

    @OnClick({R.id.tvFinish})
    protected void toMyLives() {
//        MyLivesActivity.start(this);
//        finish();
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (null == loginUser) {
            finish();
            return;
        }
        ToastUtil.showLoading(this, true);
        APIManager.startRequest(mILiveService.getLiveUserInfo(loginUser.id), new BaseRequestListener<LiveUser>() {
            @Override
            public void onSuccess(LiveUser result) {
                ToastUtil.hideLoading();
                SessionUtil.getInstance().setLiveUser(result);
                finish();
                MyLivesActivity.start(LiveEndActivity.this, result, true);
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.hideLoading();
                finish();
            }
        }, this);
    }

    @OnClick({R.id.tvFinish1})
    protected void toList() {
        finish();
    }
}
