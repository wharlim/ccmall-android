package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ApplyCreditCommonResult<T> {

    /**
     * extra : null
     * message : 获取成功
     * status : 200
     * result : [{"id":"1129292681537650689","code":"zhongyuanyinhang","name":"中原银行","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-a3e7d5f6-4290-4750-a06e-678004796fed.jpg","stationChannelId":"1129301893948899329","searchUrl":"","applyNumber":"16.6","pastProportion":"55","tradeStationChannelId":null,"hot":false},{"id":"1202484263522402304","code":"CEB","name":"光大银行-中端卡","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-93a9d918-a90f-4743-967a-869b9140e3b8.jpg","stationChannelId":"1202485025329643521","searchUrl":"","applyNumber":"6.6","pastProportion":"94","tradeStationChannelId":null,"hot":false},{"id":"1138393813660729344","code":"guangzhouyinhang","name":"广州银行","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-c3403190-6a61-41e4-b565-d3438edb1ac1.jpg","stationChannelId":"1138394362825146368","searchUrl":"https://onlinepay.cupdata.com/weixin/apply.do?action=applyProgressInit&bankNum=6413&userId=oSJt4jkS8a5TXl_z9vnb6uD5jGB0","applyNumber":"6.1","pastProportion":"88","tradeStationChannelId":null,"hot":false},{"id":"1208219070764154880","code":"JSYH","name":"晋商银行","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-f945555c-fc2c-4ebb-ac16-f5e1b77b49c4.jpg","stationChannelId":"1208219430153093120","searchUrl":"","applyNumber":"8.5","pastProportion":"36","tradeStationChannelId":null,"hot":false},{"id":"1208213781507735552","code":"SJYH","name":"盛京银行","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-79261475-47d0-43f9-8bb4-2a9960214606.jpg","stationChannelId":"1208214202141900801","searchUrl":"","applyNumber":"3.5","pastProportion":"97","tradeStationChannelId":null,"hot":false},{"id":"1103897482280042497","code":"ZHAOSHANG","name":"招商储蓄卡","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-99288063-6197-4f7d-9e18-92e4da06bac0.jpg","stationChannelId":"1103898783286034432","searchUrl":"","applyNumber":"8.7","pastProportion":"45","tradeStationChannelId":null,"hot":false},{"id":"1066884068211163136","code":"gdgdk","name":"光大银行高端卡","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-c42bae42-a2e4-4891-849c-c9eb85032509.jpg","stationChannelId":"1066887292074852352","searchUrl":"https://xyk.cebbank.com/cebmms/apply/fz/card-app-status.htm?from=groupmessage&isappinstalled=0","applyNumber":"12.3","pastProportion":"43","tradeStationChannelId":null,"hot":false},{"id":"1037256644343365633","code":"zsyhxsk","name":"招商学生卡","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-517bb440-82f8-4e98-a751-d9ce7034dc2c.jpg","stationChannelId":"1037951755662393344","searchUrl":"http://ccclub.cmbchina.com/CrdCardApply/QSchedule.aspx","applyNumber":"8.2","pastProportion":"78","tradeStationChannelId":null,"hot":false},{"id":"928519625187000320","code":"ECITIC","name":"中信银行","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-7db225e1-dc36-43e3-93db-969b26690b94.jpg","stationChannelId":"974490466055618561","searchUrl":"https://creditcard.ecitic.com/citiccard/wap/cardappquery/app_inq.jsp?from=groupmessage&isappinstalled=0","applyNumber":"14.2","pastProportion":"43","tradeStationChannelId":null,"hot":false},{"id":"928517164074270720","code":"PAQK","name":"平安银行","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-f29bc078-ad16-4432-b4bb-2519fb54619d.jpg","stationChannelId":"974490293158019073","searchUrl":"https://c.pingan.com/apply/mobile/modules/queryApp/index.html#queryf","applyNumber":"11.8","pastProportion":"43","tradeStationChannelId":null,"hot":false},{"id":"907080162711961601","code":"CEB ","name":"光大银行","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-a32e9560-2c00-4627-a7d2-a55e4f4a7d86.jpg","stationChannelId":"974490231652745216","searchUrl":"https://xyk.cebbank.com/cebmms/apply/fz/card-app-status.htm?from=groupmessage&isappinstalled=0","applyNumber":"8.3","pastProportion":"95","tradeStationChannelId":null,"hot":false},{"id":"907080045854457857","code":"CIB","name":"兴业银行","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-f9be0d5f-b39c-43ca-ae0f-fc3f17fae5d6.jpg","stationChannelId":"974490177466531841","searchUrl":"https://3g.cib.com.cn/app/00282.html?from=groupmessage&isappinstalled=0","applyNumber":"9.1","pastProportion":"59","tradeStationChannelId":null,"hot":false},{"id":"907080006306365441","code":"CMBCHINA","name":"招商银行","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-508ff99e-4bdc-49d7-9f26-259ebe028874.jpg","stationChannelId":"974489619980615681","searchUrl":"http://ccclub.cmbchina.com/CrdCardApply/QSchedule.aspx","applyNumber":"11.6","pastProportion":"48","tradeStationChannelId":null,"hot":true},{"id":"907079962610106368","code":"CMBC","name":"民生银行","iconPath":"https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-4298529a-3c3b-424b-a545-b6d768f5d43a.jpg","stationChannelId":"974489553324736512","searchUrl":"https://creditcard.cmbc.com.cn/home/cn/wap/business/account/progress/index.shtml","applyNumber":"9.2","pastProportion":"94","tradeStationChannelId":null,"hot":false}]
     */

    @SerializedName("extra")
    public Object extra;
    @SerializedName("message")
    public String message;
    @SerializedName("status")
    public int status;
    public boolean isSuccess() {
        return status == 200;
    }
    @SerializedName("result")
    public T result;

    public static class BankItem implements Serializable {
        /**
         * id : 1129292681537650689
         * code : zhongyuanyinhang
         * name : 中原银行
         * iconPath : https://finance-increment.oss-cn-hangzhou.aliyuncs.com/platform/bankIcon-a3e7d5f6-4290-4750-a06e-678004796fed.jpg
         * stationChannelId : 1129301893948899329
         * searchUrl :
         * applyNumber : 16.6
         * pastProportion : 55
         * tradeStationChannelId : null
         * hot : false
         */

        @SerializedName("id")
        public String id;
        @SerializedName("code")
        public String code;
        @SerializedName("name")
        public String name;
        @SerializedName("iconPath")
        public String iconPath;
        @SerializedName("stationChannelId")
        public String stationChannelId;
        @SerializedName("searchUrl")
        public String searchUrl;
        @SerializedName("applyNumber")
        public String applyNumber;
        @SerializedName("pastProportion")
        public String pastProportion;
        @SerializedName("tradeStationChannelId")
        public Object tradeStationChannelId;
        @SerializedName("hot")
        public boolean hot;
    }
}
