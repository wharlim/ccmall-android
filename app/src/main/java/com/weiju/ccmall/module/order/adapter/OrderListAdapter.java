package com.weiju.ccmall.module.order.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderDetailActivity;
import com.weiju.ccmall.module.order.OrderListActivity;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.basic.BaseCallback;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.OrderProduct;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.OrderService;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.order.adapter
 * @since 2017-07-06
 */
public class OrderListAdapter extends BaseAdapter<Order, RecyclerView.ViewHolder> {

    private Activity mContext;
    private int mModel;

    private int mWaitForUpdateOrder = -1; // 在resume的时候需要更新虚拟物品的到期时间

    public CountDownTimer mTimer;

    IOrderService orderService = ServiceManager.getInstance().createService(IOrderService.class);

    public OrderListAdapter(Activity context, int model) {
        super(context);
        mContext = context;
        mModel = model;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (mModel == OrderListActivity.MODE_MY_SALE) {
            return SaleViewHolder.newItem(parent);
        }
        return new ViewHolder(layoutInflater.inflate(R.layout.item_order, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
        Log.d("Seven", "model -> " + mModel + ", pos -> " + position);
        final Order order = items.get(position);
        if (mModel == OrderListActivity.MODE_MY_SALE) {
            ((SaleViewHolder) h).bindView(order, (v) -> {mWaitForUpdateOrder = position;});
            return;
        } else {
            ((ViewHolder) h).setOrder(order, position);
        }
        h.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mModel == OrderListActivity.MODE_MY_GIFT || mModel == OrderListActivity.MODE_LIVE_STORE) {
                    //订单礼包
                    Intent intent = new Intent(context, OrderDetailActivity.class);
                    intent.putExtra("orderCode", order.orderMain.orderCode);
                    intent.putExtra("mode", mModel);
                    mContext.startActivity(intent);
                } else if (mModel == OrderListActivity.MODE_SELLER) {
                    EventUtil.viewOrderDetailBySeller(context, order.orderMain.orderCode);
                } else if (mModel == 0) {
                    EventUtil.viewOrderDetail(context, order.orderMain.orderCode, false);
                } else if (mModel == 2) {
                    Intent intent = new Intent(context, OrderDetailActivity.class);
                    intent.putExtra("orderCode", order.orderMain.orderCode);
                    intent.putExtra("mode", mModel);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                } else if (mModel == 3) {
                    /*if (order.vipDto.mVip == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setMessage("请尽快升级成C1，才能开始处理销售订单哦！");
                        //点击对话框以外的区域是否让对话框消失
                        builder.setCancelable(false);
                        builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.show();
                        return;
                    }*/

                    Intent intent = new Intent(context, OrderDetailActivity.class);
                    intent.putExtra("orderCode", order.orderMain.orderCode);
                    intent.putExtra("mode", mModel);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                } else if (mModel == 4) {
                    Intent intent = new Intent(context, OrderDetailActivity.class);
                    intent.putExtra("orderCode", order.orderMain.orderCode);
                    intent.putExtra("mode", mModel);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                }
            }
        });
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // 虚拟物品相关
        @BindView(R.id.virOrderContainer)
        protected LinearLayout mVirOrderContainer;
        @BindView(R.id.endTimeContainer)
        protected LinearLayout mEndTimeContainer;
        @BindView(R.id.endTime)
        protected TextView mEndTime;
        @BindView(R.id.goUseBtn)
        protected TextView mGoUseBtn;

        @BindView(R.id.itemStoreNameTv)
        protected TextView mStoreNameTv;
        @BindView(R.id.itemStatusTv)
        protected TextView mStatusTv;
        @BindView(R.id.itemRecyclerView)
        protected RecyclerView mRecyclerView;
        @BindView(R.id.itemDescTv)
        protected TextView mDescTv;
        @BindView(R.id.itemPriceTv)
        protected TextView mPriceTv;
        @BindView(R.id.bottomLayout)
        protected LinearLayout mBottomLayout;
        @BindView(R.id.itemCancelBtn)
        protected TextView mCancelBtn;
        @BindView(R.id.itemPayBtn)
        protected TextView mPayBtn;
        @BindView(R.id.itemApplyRefundMoneyBtn)
        protected TextView mApplyRefundMoneyBtn;
        @BindView(R.id.itemApplyRefundGoodsBtn)
        protected TextView mApplyRefundGoodsBtn;
        @BindView(R.id.orderFinishBtn)
        protected TextView orderFinishBtn;
        @BindView(R.id.tv_confirmUpgrade)
        protected TextView tv_confirmUpgrade;
        @BindView(R.id.itemDetailBtn)
        protected TextView mDetailBtn;
        @BindView(R.id.itemViewExpressBtn)
        protected TextView mViewExpressBtn;
        @BindView(R.id.itemCsBtn)
        protected TextView mItemCsBtn;
        @BindView(R.id.itemShit)
        protected TextView mItemShit;
        @BindView(R.id.itemRefundMony)
        protected TextView itemRefundMony;
        @BindView(R.id.itemRefundGoods)
        protected TextView itemRefundGoods;
        @BindView(R.id.itemPayMoney)
        protected TextView itemPayMoney;
        @BindView(R.id.itemCancelRefundGoods)
        protected TextView itemCancelRefundGoods;
        @BindView(R.id.itemCancelRefundMoney)
        protected TextView itemCancelRefundMoney;
        @BindView(R.id.itemGoGroupBuy)
        protected TextView mItemGoGroupBuy;
        @BindView(R.id.itemCheckGroupBuy)
        protected TextView mItemCheckGroupBuy;
        @BindView(R.id.itemComment)
        protected TextView mItemComment;
        @BindView(R.id.itemEditRefund)
        protected TextView mItemEditRefund;
        @BindView(R.id.itemCancelRefund)
        protected TextView mItemCancelRefund;
        @BindView(R.id.tvPriceTag1)
        protected TextView mTvPriceTag1;
        @BindView(R.id.tvPriceTag2)
        protected TextView mTvPriceTag2;
        @BindView(R.id.tvCS)
        protected TextView mTvCS;
        @BindView(R.id.tv_time_out)
        protected TextView tv_time_out;
        @BindView(R.id.iv_im_icon)
        protected ImageView iv_im_icon;

        @BindView(R.id.tv_thaw)
        TextView tv_thaw;

        private Order mOrder;
        private int position;
        private int mProductType;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        // 是否显示去使用
        private boolean showGoToUse() {
            return OrderDetailActivity.showGoToUse(mOrder);
        }

        @SuppressLint("DefaultLocale")
        public void setOrder(Order order, int position) {
            mOrder = order;
            this.position = position;
            if (mOrder.products.size() > 0) {
                mProductType = mOrder.products.get(0).productType;
            }
            // 虚拟物品相关
            if (!showGoToUse()) {
                mVirOrderContainer.setVisibility(View.GONE);
            } else {
                mVirOrderContainer.setVisibility(View.VISIBLE);
                mGoUseBtn.setOnClickListener(v -> {
                    mWaitForUpdateOrder = position;
                    Intent intent = new Intent(context, WebViewJavaActivity.class);
                    intent.putExtra("url", mOrder.virOrder.url);
                    intent.putExtra("hideToolbar", true);
                    intent.putExtra("showWindow", true);
                    context.startActivity(intent);
                });
                mEndTime.setText(mOrder.virOrder.useEndDate);
            }
            // 虚拟物品相关 end

            mStatusTv.setText(order.orderMain.orderStatusStr);

            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            layoutManager.setAutoMeasureEnabled(true);
            mRecyclerView.setEnabled(false);
            mRecyclerView.addItemDecoration(new ListDividerDecoration(context));
            mRecyclerView.setLayoutManager(layoutManager);
            OrderItemAdapter orderItemAdapter = new OrderItemAdapter(context, order.products);
            orderItemAdapter.setSellerModel(mModel != 0);
            orderItemAdapter.setCallback(new BaseCallback<Object>() {
                @Override
                public void callback(Object data) {
                    mWaitForUpdateOrder = position;
                    if (mModel == OrderListActivity.MODE_MY_GIFT || mModel == OrderListActivity.MODE_LIVE_STORE) {
                        //订单礼包
                        Intent intent = new Intent(context, OrderDetailActivity.class);
                        intent.putExtra("orderCode", order.orderMain.orderCode);
                        intent.putExtra("mode", mModel);
                        mContext.startActivity(intent);
                    } else if (mModel == OrderListActivity.MODE_SELLER) {
                        EventUtil.viewOrderDetailBySeller(context, mOrder.orderMain.orderCode);
                    } else if (0 == mModel) {
                        EventUtil.viewOrderDetail(context, mOrder.orderMain.orderCode, false);
                    } else if (mModel == 2) {
                        Intent intent = new Intent(context, OrderDetailActivity.class);
                        intent.putExtra("orderCode", order.orderMain.orderCode);
                        intent.putExtra("mode", mModel);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    } else if (mModel == 3) {
                        /*if (mOrder.vipDto.mVip == 0) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setMessage("C1，才能开始处理销售订单哦！");
                            //点击对话框以外的区域是否让对话框消失
                            builder.setCancelable(false);
                            builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            builder.show();
                            return;
                        }*/
                        Intent intent = new Intent(context, OrderDetailActivity.class);
                        intent.putExtra("orderCode", order.orderMain.orderCode);
                        intent.putExtra("mode", mModel);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    } else if (mModel == 4) {
                        Intent intent = new Intent(context, OrderDetailActivity.class);
                        intent.putExtra("orderCode", order.orderMain.orderCode);
                        intent.putExtra("mode", mModel);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }
                }
            });
            orderItemAdapter.setMemberId(mOrder.orderMain.memberId);
            mRecyclerView.setAdapter(orderItemAdapter);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setClickable(false);
            mDescTv.setText(String.format("共 %d 件商品 合计：", order.products.size()));


            for (int i = 0; i < mBottomLayout.getChildCount(); i++) {
                mBottomLayout.getChildAt(i).setVisibility(View.GONE);
            }


            if (mModel == OrderListActivity.MODE_MY_GIFT) {
                //礼包订单
                mStoreNameTv.setText(order.storeName);
                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalProductMoney));
                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                switch (order.orderMain.status) {
                    case AppTypes.ORDER.STATUS_SELLER_WAIT_SHIP:
                        mItemShit.setVisibility(View.VISIBLE);
                        break;
                    case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP:
                        mViewExpressBtn.setVisibility(View.VISIBLE);
                        break;
                    default:
                }

            } else if (mModel == 0) {
                //买家
                mStoreNameTv.setText(order.storeName);
//                mStoreNameTv.setText("");
                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalProductMoney));
                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));

                switch (order.orderMain.status) {
                    case AppTypes.ORDER.STATUS_BUYER_WAIT_PAY:
                        mPayBtn.setVisibility(View.VISIBLE);
                        mCancelBtn.setVisibility(View.VISIBLE);
                        mTvPriceTag2.setText("待付款：");
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                        break;
                    case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP:
                        if (mProductType == 28) {
                            mApplyRefundMoneyBtn.setVisibility(View.GONE);
                        }
                        break;
                    case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP:
                        mViewExpressBtn.setVisibility(View.VISIBLE);
                        orderFinishBtn.setVisibility(View.VISIBLE);
                        if (mProductType == 28){
                            mApplyRefundGoodsBtn.setVisibility(View.GONE);
                        }
                        break;
                    case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED:
//                        mItemComment.setVisibility(View.VISIBLE);
                        break;
                    case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODING:
                    case AppTypes.ORDER.STATUS_BUYER_RETURN_MONEYING:
//                        mItemCsBtn.setVisibility(View.VISIBLE);
//                        mItemEditRefund.setVisibility(View.VISIBLE);
//                        mItemCancelRefund.setVisibility(View.VISIBLE);
//
//
//                        mStoreNameTv.setText("售后单号：" + mOrder.refundOrder.refundId);
                        break;
                    case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE:
                        if (order.orderMain.payMoney < order.orderMain.totalMoney) {
                            mTvPriceTag2.setText("待付款：");
                        }
                        break;
                    default:
                }
            } else if (mModel == OrderListActivity.MODE_LIVE_STORE) {//卖家
                mStoreNameTv.setText(order.orderMain.nickName);
                mStoreNameTv.setTextColor(mContext.getResources().getColor(R.color.red));
                Drawable drawable= mContext.getResources().getDrawable(R.drawable.ic_order_message);
                drawable.setBounds(0, 0, SizeUtils.dp2px(17), SizeUtils.dp2px(17));
                mStoreNameTv.setCompoundDrawablePadding(SizeUtils.dp2px(10));
                mStoreNameTv.setCompoundDrawables(drawable,null,null,null);
                mTvPriceTag1.setText("买家实付款：");
                mTvPriceTag2.setText("你实收款：");
                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.payMoney));
                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.profitMoney));
                mStoreNameTv.setOnClickListener(view -> {//去聊天
                    goToChat();
                });
                switch (order.orderMain.status) {
                    case AppTypes.ORDER.STATUS_SELLER_WAIT_SHIP:
                        mItemShit.setVisibility(View.VISIBLE);
                        break;
                    case AppTypes.ORDER.STATUS_SELLER_HAS_SHIP:
                        mViewExpressBtn.setVisibility(View.VISIBLE);
                        break;
                    case AppTypes.ORDER.STATUS_SELLER_HAS_COMPLETE:
                        break;
                    case AppTypes.ORDER.STATUS_SELLER_HAS_CLOSE:
                    case 8:
                    case 7:
                        break;
                    default:
                        break;
                }
            } else if (mModel == 1) {//卖家
                mStoreNameTv.setText("买家：" + order.nickName);
                mTvPriceTag1.setText("买家实付款：");
                mTvPriceTag2.setText("你实收款：");
                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.payMoney));
                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.payMoney + order.orderMain.discountCoupon + order.orderMain.score * 10));
//                long totalPrice = 0;
//                for (OrderProduct product : order.products) {
//                    totalPrice += product.price * product.quantity;
//                }
//                itemPayMoney.setText(ConvertUtil.centToCurrency(context, totalPrice));

                switch (order.orderMain.status) {
                    case AppTypes.ORDER.STATUS_SELLER_WAIT_SHIP:
                        mItemShit.setVisibility(View.VISIBLE);
                        break;
                    case AppTypes.ORDER.STATUS_SELLER_HAS_SHIP:
                        mViewExpressBtn.setVisibility(View.VISIBLE);
                        break;
                    case AppTypes.ORDER.STATUS_SELLER_HAS_COMPLETE:
//                        mDetailBtn.setVisibility(View.VISIBLE);
                        break;
                    case AppTypes.ORDER.STATUS_SELLER_HAS_CLOSE:
                    case 8:
                    case 7:
                        break;
                    default:
                        break;
                }
            } else if (mModel == 2) {
                //我的商店--5种状态
                //待处理，待发货，待收货，交易成功，交易失败
//                mStatusTv.setText("");
//                mStoreNameTv.setText("");
                mStoreNameTv.setVisibility(View.VISIBLE);
                mStoreNameTv.setText(mOrder.orderMain.nickName);
                iv_im_icon.setVisibility(View.VISIBLE);
                mStoreNameTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {//去聊天
                        goToChat();

                    }
                });
                if (order.orderMain.orderType == 6) {//强制订单
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                    switch (order.orderMain.status) {
                        case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP://待收货
//                        mStatusTv.setText("待收货");
                            mTvPriceTag2.setText("我已结算：");
                            mPayBtn.setVisibility(View.GONE);
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE://交易关闭
//                        mStatusTv.setText("交易关闭");
                            mTvPriceTag2.setText("我已结算：");
                            mPayBtn.setVisibility(View.GONE);
                            break;

                        case AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM://待处理
                            mPayBtn.setVisibility(View.VISIBLE);
//                        mStatusTv.setText("待处理");
                            mTvPriceTag2.setText("待我结算：");
                            break;

                        case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP://待发货
//                        mStatusTv.setText("待发货");
                            mTvPriceTag2.setText("我已结算：");
                            mPayBtn.setVisibility(View.GONE);
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED://交易成功
//                        mStatusTv.setText("交易成功");
                            mTvPriceTag2.setText("我已结算：");
                            mPayBtn.setVisibility(View.GONE);
                            break;

                        case AppTypes.ORDER.STATUS_BUYER_RETURN_MONECLOSE://交易关闭，退款成功
                            mTvPriceTag2.setText("我已结算：");
                            mTvPriceTag1.setText("买家已转账：");
                            mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                            break;

                        default:
                            break;
                    }

                    if (TextUtils.isEmpty(order.orderMain.paymentNumber)) {
                        mTvPriceTag2.setText("待我结算：");
                    } else {
                        mTvPriceTag2.setText("我已结算：");
                    }

                    mTvPriceTag1.setText("买家已付款：");
                } else {
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.transactionFee));
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                    switch (order.orderMain.status) {
                        case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP://待收货
                            if (order.orderMain.payTag == 1) {
                                mTvPriceTag2.setText("我已结算：");
                                mTvPriceTag1.setText("买家已转账：");
                                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                            } else {
                                mTvPriceTag2.setText("我已结算：");
                                mPayBtn.setVisibility(View.GONE);
                                mTvPriceTag1.setText("买家已转账：");
                            }
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE://交易关闭
                            if (order.orderMain.payTag == 1) {
                                mTvPriceTag2.setText("我已结算：");
                                mTvPriceTag1.setText("买家已转账：");
                                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                            } else {
                                mTvPriceTag2.setText("我已结算：");
                                mPayBtn.setVisibility(View.GONE);
                                mTvPriceTag1.setText("买家已转账：");
                            }
                            break;

                        case AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM://待处理，待店主确认
                            if (order.orderMain.payTag == 1) {
                                mTvPriceTag2.setText("待我结算：");
                                mTvPriceTag1.setText("请确认买家已转账：");
                                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
//                                mPayBtn.setVisibility(View.VISIBLE);
                            } else {
//                                mPayBtn.setVisibility(View.VISIBLE);
                                mTvPriceTag2.setText("待我结算：");
                                mTvPriceTag1.setText("请确认买家已转账：");
                            }
                            break;

                        case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP://待发货
                            if (order.orderMain.payTag == 1) {
                                mTvPriceTag2.setText("我已结算：");
                                mTvPriceTag1.setText("买家已转账：");
                                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                            } else {
                                mTvPriceTag2.setText("我已结算：");
                                mPayBtn.setVisibility(View.GONE);
                                mTvPriceTag1.setText("买家已转账：");
                            }
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED://交易成功
//                        mStatusTv.setText("交易成功");
                            if (order.orderMain.payTag == 1) {
                                mTvPriceTag2.setText("我已结算：");
                                mTvPriceTag1.setText("买家已转账：");
                                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                            } else {
                                mTvPriceTag2.setText("我已结算：");
                                mPayBtn.setVisibility(View.GONE);
                                mTvPriceTag1.setText("买家已转账：");
                            }
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_MONECLOSE://交易关闭，退款成功
                            if (order.orderMain.payTag == 1) {
                                mTvPriceTag2.setText("我已结算：");
                                mTvPriceTag1.setText("买家已转账：");
                                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                            } else {
                                mTvPriceTag2.setText("我已结算：");
                                mTvPriceTag1.setText("买家已转账：");
                                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.transactionFee));
                                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                            }
                            break;

                        default:
                            break;
                    }

                    if (TextUtils.isEmpty(order.orderMain.paymentNumber)) {
                        mTvPriceTag2.setText("待我结算：");
                    } else {
                        mTvPriceTag2.setText("我已结算：");
                    }
                }










                /*mStoreNameTv.setText("");
                mStoreNameTv.setVisibility(View.INVISIBLE);
                if (order.orderMain.orderType == 6) {
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                } else {
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.transactionFee));
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.serviceFee));
                }*/


            } else if (mModel == 3) {
                //我销售的
                mStoreNameTv.setText("");
                mStoreNameTv.setVisibility(View.INVISIBLE);
                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.memberProfitBean.profitMoney));

                switch (order.orderMain.status) {

                    case AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM://待店主确认
                        if (order.orderMain.orderType == 5 && order.orderMain.payTag == 1) {
                            mTvPriceTag1.setText("客户已向店主转账：");
                            itemPayMoney.setVisibility(View.GONE);
                            mTvPriceTag2.setVisibility(View.GONE);
                            mCancelBtn.setVisibility(View.GONE);
                        } else {
                            mPayBtn.setVisibility(View.GONE);
                            mTvPriceTag2.setText("我已付款：");
                            mTvPriceTag1.setText("客户已向我转账：");
//                            mCancelBtn.setVisibility(View.VISIBLE);
                        }

                        if (order.orderMain.orderType == 5 && order.orderMain.payTag == 1) {
                            mCancelBtn.setVisibility(View.GONE);
                        } else {
//                            mCancelBtn.setVisibility(View.VISIBLE);
                        }
                        break;

                    case AppTypes.ORDER.STATUS_TO_RECOMMENDED_BY_THE_REFEREE://待推荐人确认
                        if (order.orderMain.orderType == 5) {
                            long lessTime = order.orderMain.lessTime - 900000;
                            if (lessTime > 0) {
//                                mPayBtn.setVisibility(View.VISIBLE);
                                mTvPriceTag2.setText("待我付款：");
                                mTvPriceTag1.setText("请确认客户已向我转账：");
//                                mCancelBtn.setVisibility(View.VISIBLE);
                            } else {
                                mPayBtn.setVisibility(View.GONE);
                                mTvPriceTag1.setText("待客户向店主转账：");
                                itemPayMoney.setVisibility(View.GONE);
                                mTvPriceTag2.setVisibility(View.GONE);
                                mCancelBtn.setVisibility(View.GONE);
                            }
                        } else {
//                            mPayBtn.setVisibility(View.VISIBLE);
                            mTvPriceTag2.setText("待我付款：");
                            mTvPriceTag1.setText("请确认客户已向我转账：");
//                            mCancelBtn.setVisibility(View.VISIBLE);
                        }
                        break;

                    case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP://待发货
                    case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP://待收货，已发货
                    case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED://已收货，交易成功
                    case AppTypes.ORDER.STATUS_BUYER_RETURN_MONEYING://退款中
                    case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODING://退货中
                        if (order.orderMain.orderType == 5 && order.orderMain.payTag == 1) {
                            mPayBtn.setVisibility(View.GONE);
                            mTvPriceTag1.setText("客户已向店主转账：");
                            itemPayMoney.setVisibility(View.GONE);
                            mTvPriceTag2.setVisibility(View.GONE);
                        } else {
                            mPayBtn.setVisibility(View.GONE);
                            mTvPriceTag2.setText("我已付款：");
                            mTvPriceTag1.setText("客户已向我转账：");
                            mCancelBtn.setVisibility(View.GONE);

                        }

                        break;
                    case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE://关闭订单
                        if (order.orderMain.orderType == 5 && order.orderMain.payTag == 1) {
                            mPayBtn.setVisibility(View.GONE);
                            mTvPriceTag1.setText("待客户向店主转账：");
                            itemPayMoney.setVisibility(View.GONE);
                            mTvPriceTag2.setVisibility(View.GONE);
                        } else {

                            mTvPriceTag2.setText("待我付款：");
                            mTvPriceTag1.setText("请确认客户已向我转账：");
                        }


                        break;
                    case AppTypes.ORDER.STATUS_BUYER_RETURN_MONECLOSE://退款完成
                    case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODSCLOSE://退货完成
                        if (order.orderMain.orderType == 5 && order.orderMain.payTag == 1) {
                            mPayBtn.setVisibility(View.GONE);
                            mTvPriceTag1.setText("客户已向店主转账：");
                            itemPayMoney.setVisibility(View.GONE);
                            mTvPriceTag2.setVisibility(View.GONE);
                        } else {

                            mTvPriceTag2.setText("我已付款：");
                            mTvPriceTag1.setText("客户已向我转账：");
                        }
                        break;

                    /*case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE:
                        break;*/

                    default:
                        break;
                }
                if (order.memberProfitBean.status == -1) {
                    mTvPriceTag2.setText("预计可得提成：");
                    tv_thaw.setText("(客户未付款)");
                    mTvPriceTag1.setVisibility(View.GONE);
                    mPriceTv.setVisibility(View.GONE);
                } else if (order.memberProfitBean.status == 0) {
                    mTvPriceTag2.setText("可得提成：");
                    tv_thaw.setText("(未解冻)");
                    mTvPriceTag1.setVisibility(View.GONE);
                    mPriceTv.setVisibility(View.GONE);
                } else if (order.memberProfitBean.status == 1) {
                    mTvPriceTag2.setText("可得提成：");
                    tv_thaw.setText("(已解冻)");
                    mTvPriceTag1.setVisibility(View.GONE);
                    mPriceTv.setVisibility(View.GONE);
                }

            } else if (mModel == 4) {//我的升级订单列表--订单列表
                if (order.orderMain.orderType != 6) {//自由订单

                    mStoreNameTv.setVisibility(View.VISIBLE);
                    mStoreNameTv.setText(mOrder.storeName);
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalProductMoney));
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                    switch (order.orderMain.status) {
                        case AppTypes.ORDER.STATUS_BUYER_WAIT_PAY://待付款
//                            mPayBtn.setVisibility(View.GONE);
//                        mStatusTv.setText("待付款");
                            mTvPriceTag1.setText("商品总价：");
                            mTvPriceTag2.setText("待付款：");
                            mCancelBtn.setVisibility(View.VISIBLE);
                            mPayBtn.setVisibility(View.VISIBLE);
//                            mPayBtn.setVisibility(View.VISIBLE);
                            break;

                        case AppTypes.ORDER.STATUS_TO_RECOMMENDED_BY_THE_REFEREE://待推荐人确认
//                        mStatusTv.setText("待推荐人确认");
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
                            /*long lessTime = mOrder.orderMain.lessTime - 900000;
                            if (order.upMember != null) {
                                if (order.orderMain.orderType == 5) {
                                    if (lessTime <= 0) {//超过15分钟，显示
                                        mCancelBtn.setVisibility(View.VISIBLE);
                                    } else {//15分钟前，隐藏
                                        mCancelBtn.setVisibility(View.VISIBLE);
                                    }
                                    mPayBtn.setVisibility(View.VISIBLE);
                                    return;
                                } else {
                                    mPayBtn.setVisibility(View.GONE);
                                }

                            }*/
                            /*long lessTime = mOrder.orderMain.lessTime - 900000;
                            if (mOrder.orderMain.orderType == 5) {
                                mPayBtn.setVisibility(View.VISIBLE);
                                if (0 >= lessTime) {
                                    mCancelBtn.setVisibility(View.VISIBLE);
                                } else {
                                    mCancelBtn.setVisibility(View.GONE);
                                }
                            } else {
                                mCancelBtn.setVisibility(View.GONE);
                            }*/
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                            mCancelBtn.setVisibility(View.GONE);
                            break;
                        case AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM://待店主确认
//                        mStatusTv.setText("待店主确认");
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
                            if (order.orderMain.orderType == 5 && order.orderMain.payTag == 1) {
                                mCancelBtn.setVisibility(View.VISIBLE);
                            } else {
                                mCancelBtn.setVisibility(View.GONE);
                            }
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            break;

                        case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP://待发货
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
                            //TODO: 我的升级订单详情，根据后台返回的字段显示"确认升级"按钮
                            if (mOrder.orderMain.giveUpRefund == 0 && (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6))
                                tv_confirmUpgrade.setVisibility(View.VISIBLE);
                            else
                                tv_confirmUpgrade.setVisibility(View.GONE);
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_MONEYING:
//                        mStatusTv.setText("待发货");
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            break;

                        case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP://已发货，待收货
                            orderFinishBtn.setVisibility(View.VISIBLE);
                            mViewExpressBtn.setVisibility(View.VISIBLE);
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
                            //TODO: 我的升级订单详情，根据后台返回的字段显示"确认升级"按钮
                            if (mOrder.orderMain.giveUpRefund == 0 && (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6))
                                tv_confirmUpgrade.setVisibility(View.VISIBLE);
                            else
                                tv_confirmUpgrade.setVisibility(View.GONE);
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODING:
//                        mStatusTv.setText("待收货");
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
                        /*orderFinishBtn.setVisibility(View.VISIBLE);
                        mViewExpressBtn.setVisibility(View.VISIBLE);*/
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            break;

                        case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED://交易成功，已收货
//                        mStatusTv.setText("交易成功");
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_MONECLOSE://交易失败，退款成功，退货成功，已关闭
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODSCLOSE:
                        case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE:
//                        mStatusTv.setText("交易关闭");
                            mTvPriceTag2.setText("待付款：");
                            mTvPriceTag1.setText("商品总价：");
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            break;
                        case AppTypes.ORDER.STATUS_WAITIN_FOR_PAY_YOUR_SELF://待付款
                            if (order.orderMain.orderType == 6) {
                                mPayBtn.setVisibility(View.VISIBLE);
                                mPayBtn.setText("支付升级");
//                                tv_time_out.setVisibility(View.VISIBLE);
                                /*long remaining = order.orderMain.lessTime;
                                if (remaining > 0) {
                                    SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
                                    if (mTimer == null) {
                                        mTimer = new CountDownTimer(remaining, 1000) {
                                            @Override
                                            public void onTick(long l) {
                                                String ms = formatter.format(l);
                                                String str = "请在" + ms + "内支付";
                                                SpannableString spannableString = new SpannableString(str);
                                                ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
                                                spannableString.setSpan(foregroundColorSpan, str.indexOf("在") + 1, str.indexOf("内"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                                tv_time_out.setText(spannableString);
                                            }

                                            @Override
                                            public void onFinish() {

                                            }
                                        }.start();
                                        TimerControl.getInstance().add(mTimer);
                                    }
                                }*/
                                mCancelBtn.setVisibility(View.GONE);
                            }
                            break;
                        default:
                            break;
                    }
                } else {
                    mStoreNameTv.setVisibility(View.VISIBLE);
                    mStoreNameTv.setText(mOrder.storeName);
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalProductMoney));
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                    switch (order.orderMain.status) {
                        case AppTypes.ORDER.STATUS_BUYER_WAIT_PAY://待付款
//                            mPayBtn.setVisibility(View.GONE);
//                        mStatusTv.setText("待付款");
                            mTvPriceTag1.setText("商品总价：");
                            mTvPriceTag2.setText("待付款：");
                            mCancelBtn.setVisibility(View.VISIBLE);
                            mPayBtn.setVisibility(View.VISIBLE);
                            /*mCancelBtn.setVisibility(View.VISIBLE);
                            mPayBtn.setVisibility(View.VISIBLE);*/
                            break;

                        case AppTypes.ORDER.STATUS_TO_RECOMMENDED_BY_THE_REFEREE://待推荐人确认
//                        mStatusTv.setText("待推荐人确认");
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
                            /*if (order.upMember != null) {
                                if (0 == order.vipDto.upVip) {
                                    mPayBtn.setVisibility(View.VISIBLE);
                                } else {
                                    mPayBtn.setVisibility(View.GONE);
                                }
                            }*/

                            long lessTime = mOrder.orderMain.lessTime - 900000;
                            if (order.upMember != null) {
                                if (order.orderMain.orderType == 5) {
                                    if (lessTime <= 0) {//超过20分钟，显示
                                        mCancelBtn.setVisibility(View.VISIBLE);
                                    } else {//20分钟前，隐藏
                                        mCancelBtn.setVisibility(View.VISIBLE);
                                    }
                                    mPayBtn.setVisibility(View.VISIBLE);
                                    return;
                                } else {
                                    mPayBtn.setVisibility(View.GONE);
                                }

                            }
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            mCancelBtn.setVisibility(View.VISIBLE);
                            break;
                        case AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM://待店主确认
//                        mStatusTv.setText("待店主确认");
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
                            mCancelBtn.setVisibility(View.VISIBLE);
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            break;

                        case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP://待发货
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
                            //TODO: 我的升级订单详情，根据后台返回的字段显示"确认升级"按钮
                            if (mOrder.orderMain.giveUpRefund == 0 && (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6))
                                tv_confirmUpgrade.setVisibility(View.VISIBLE);
                            else
                                tv_confirmUpgrade.setVisibility(View.GONE);
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_MONEYING:
//                        mStatusTv.setText("待发货");
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");

//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            break;

                        case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP://已发货，待收货
                            orderFinishBtn.setVisibility(View.VISIBLE);
                            mViewExpressBtn.setVisibility(View.VISIBLE);
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
                            //TODO: 我的升级订单详情，根据后台返回的字段显示"确认升级"按钮
                            if (mOrder.orderMain.giveUpRefund == 0 && (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6))
                                tv_confirmUpgrade.setVisibility(View.VISIBLE);
                            else
                                tv_confirmUpgrade.setVisibility(View.GONE);
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODING:
//                        mStatusTv.setText("待收货");
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
                        /*orderFinishBtn.setVisibility(View.VISIBLE);
                        mViewExpressBtn.setVisibility(View.VISIBLE);*/
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            break;

                        case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED://交易成功，已收货
//                        mStatusTv.setText("交易成功");
                            mTvPriceTag2.setText("实付款：");
                            mTvPriceTag1.setText("商品总价：");
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            break;
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_MONECLOSE://交易失败，退款成功，退货成功，已关闭
                        case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODSCLOSE:
                        case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE:
//                        mStatusTv.setText("交易关闭");
                            mTvPriceTag2.setText("待付款：");
                            mTvPriceTag1.setText("商品总价：");
//                        mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
//                        itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                            break;
                        case AppTypes.ORDER.STATUS_WAITIN_FOR_PAY_YOUR_SELF://待付款
                            if (order.orderMain.orderType == 6) {
                                mPayBtn.setVisibility(View.VISIBLE);
                                mPayBtn.setText("支付升级");
                                /*tv_time_out.setVisibility(View.VISIBLE);
                                long remaining = order.orderMain.lessTime - 900000;
                                if (remaining <= 0) {
                                    SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
                                    if (mTimer == null) {
                                        mTimer = new CountDownTimer(remaining, 1000) {
                                            @Override
                                            public void onTick(long l) {
                                                String ms = formatter.format(l);
                                                String str = "请在" + ms + "内支付";
                                                SpannableString spannableString = new SpannableString(str);
                                                ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
                                                spannableString.setSpan(foregroundColorSpan, str.indexOf("在") + 1, str.indexOf("内"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                                tv_time_out.setText(spannableString);
                                            }

                                            @Override
                                            public void onFinish() {

                                            }
                                        }.start();
                                        TimerControl.getInstance().add(mTimer);
                                    }
                                }*/
                                mCancelBtn.setVisibility(View.GONE);
                            }
                            break;


                        default:
                            break;
                    }
                }
            }
            // 单独处理viewType=5/6的快速升级的逻辑
            if (order.orderMain.status == AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP ||
                    order.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_SHIP) {
                if (mOrder.orderMain.giveUpRefund == 0 && (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6))
                    tv_confirmUpgrade.setVisibility(View.VISIBLE);
                else
                    tv_confirmUpgrade.setVisibility(View.GONE);
            }
        }

        private void goToChat() {
            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (loginUser != null) {
                TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                    @Override
                    public void onError(int i, String s) {
                    }

                    @Override
                    public void onSuccess() {
                        IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                        APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                        }, mContext);
                        ChatInfo chatInfo = new ChatInfo();
                        chatInfo.setType(TIMConversationType.C2C);
                        chatInfo.setId(mOrder.orderMain.memberId);
                        chatInfo.setChatName(mOrder.orderMain.nickName);
                        Intent intent = new Intent(context, ChatActivity.class);
                        intent.putExtra("chatInfo", chatInfo);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                });
            }
        }

        @OnClick(R.id.itemRefundMony)
        public void itemRefundMony() {
//            OrderService.showRefundMoneyDialog(mContext, mOrder);
        }

        @OnClick(R.id.itemRefundGoods)
        public void itemRefundGoods() {
//            OrderService.showRefundGoodsDialog(mContext, mOrder);
        }

        @OnClick(R.id.itemShit)
        public void shit() {
            if (mModel == OrderListActivity.MODE_LIVE_STORE) {
                OrderService.shipLive(context, mOrder);
            } else if (mModel == OrderListActivity.MODE_MY_GIFT) {
                //订单礼包
                Intent intent = new Intent(context, OrderDetailActivity.class);
                intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                intent.putExtra("mode", mModel);
                mContext.startActivity(intent);
            } else {
                OrderService.ship(context, mOrder);
            }
        }

        @OnClick(R.id.itemCsBtn)
        public void contactCs() {
            OrderService.contactCs(context, mOrder);
        }

        @OnClick(R.id.itemCancelBtn)
        public void cancelOrder() {//取消订单
            /*if (mOrder.vipDto.mVip == 0 && mModel == 3) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setMessage("请尽快升级成C1，才能开始处理销售订单哦！");
                //点击对话框以外的区域是否让对话框消失
                builder.setCancelable(false);
                builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
                return;
            }

            if (mModel == 3) {

                if (mOrder.orderMain.orderType == 5 && mOrder.vipDto.mVip != 0) {
                    APIManager.startRequest(orderService.checkTimeOut(mOrder.orderMain.orderCode), new BaseRequestListener<CheckTimeOutBean>() {
                        @Override
                        public void onSuccess(CheckTimeOutBean result) {//超时校验
                            super.onSuccess(result);
                            if ("0000".equals(result.code)) {
                                showCancleOrderDialog(mOrder);
//                                OrderService.cancelOrder(mContext, mOrder);
                            } else if ("1".equals(result.code)) {
                                ToastUtil.error("该订单已超时");
                                EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_STORE_SHIT_SUCCEED));
                            }
                        }
                    }, context);
                    return;
                }
            }

            if ((mModel == 4 && mOrder.orderMain.status == AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM) || (mModel == 3 && mOrder.orderMain.status == AppTypes.ORDER.STATUS_WAITING_FOR_THE_OWNER_TO_CONFIRM)) {
                showCancleOrderDialog(mOrder);
                return;
            }*/

            OrderService.cancelOrder(mContext, mOrder);
        }

        @OnClick(R.id.itemPayBtn)
        public void payOrder() {//支付按钮
            /*if (mModel == 2) {
                Intent intent = new Intent(context, PayOrderActivity.class);
                intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                intent.putExtra("selectType", -1);
                context.startActivity(intent);
            } else if (mModel == 3) {//我销售的
                if (mOrder.orderMain.orderType == 5 && mOrder.vipDto.mVip == 0) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("请尽快升级成C1，才能开始处理销售订单哦！");
                    //点击对话框以外的区域是否让对话框消失
                    builder.setCancelable(false);
                    builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.show();
                    return;
                } else if (mOrder.orderMain.orderType == 5 && mOrder.vipDto.mVip != 0) {

                    APIManager.startRequest(orderService.checkTimeOut(mOrder.orderMain.orderCode), new BaseRequestListener<CheckTimeOutBean>() {//超时校验
                        @Override
                        public void onSuccess(CheckTimeOutBean result) {
                            super.onSuccess(result);
                            if ("0000".equals(result.code)) {
                                Intent intent = new Intent(context, PayUpgradeActivity.class);
                                intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                                context.startActivity(intent);
                            } else if ("1".equals(result.code)) {
                                ToastUtil.error("该订单已超时");
                                EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_STORE_SHIT_SUCCEED));
                            }
                        }
                    }, context);
                    return;
                } else if (mOrder.orderMain.orderType == 7) {
                    Intent intent = new Intent(context, PayUpgradeActivity.class);
                    intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                    context.startActivity(intent);
                }


            } else if (mModel == 4) {
                if (mOrder.orderMain.status == AppTypes.ORDER.STATUS_WAITIN_FOR_PAY_YOUR_SELF) {//待付款
                    Intent intent = new Intent(context, PayUpgradeActivity.class);
                    intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                    context.startActivity(intent);
                } else if (mOrder.orderMain.status == AppTypes.ORDER.STATUS_TO_RECOMMENDED_BY_THE_REFEREE) {//待推荐人确认
                    Intent intent = new Intent(mContext, WaitForPaymentActivity.class);
                    intent.putExtra("orderCode", mOrder.orderMain.orderCode);
                    context.startActivity(intent);
                }
            } else {
                OrderService.viewPayActivity(context, mOrder.orderMain.orderCode, -1);
            }*/
            OrderService.viewPayActivity(context, mOrder.orderMain.orderCode, -1);
            mWaitForUpdateOrder = this.position;

        }

        @OnClick(R.id.itemApplyRefundMoneyBtn)
        public void applyRefundMoney() {
            if (mProductType == 28) {
                OrderService.applyLiveRefundMoneyActivity(context, mOrder.orderMain.orderCode, null, true);
            } else {
                OrderService.viewApplyRefundMoneyActivity(context, mOrder);
            }
        }
        @OnClick(R.id.itemApplyRefundGoodsBtn)
        public void applyRefundGoods() {
            ArrayList<OrderProduct> orderProducts = new ArrayList<>();
            for (OrderProduct product : mOrder.products) {
                if (product.refundStatus == 0 || product.refundStatus >= 10) {
                    orderProducts.add(product);
                }
            }
            OrderService.addOrEditRefundOrder(context, mOrder.orderMain.orderCode, orderProducts, null);
        }

        @OnClick(R.id.orderFinishBtn)
        public void orderFinishBtn() {
            OrderService.finishOrder(context, mOrder);
        }

        @OnClick(R.id.itemViewExpressBtn)
        public void viewExpress() {
            OrderService.viewExpress(context, mOrder);
        }

        @OnClick(R.id.itemDetailBtn)
        public void viewOrderDetail() {
            if (mModel == OrderListActivity.MODE_SELLER) {
                EventUtil.viewOrderDetailBySeller(context, mOrder.orderMain.orderCode);
            } else {
                EventUtil.viewOrderDetail(context, mOrder.orderMain.orderCode, false);
            }
        }

        @OnClick({R.id.itemCancelRefundGoods, R.id.itemCancelRefundMoney})
        public void viewCancelRefund() {
            OrderService.showCancelRefund(mContext, mOrder);
        }

        @OnClick({R.id.itemGoGroupBuy, R.id.itemCheckGroupBuy})
        public void goGroupBuy() {
            OrderService.goGroupBuy(context, mOrder);
        }

        @OnClick(R.id.itemComment)
        public void comment() {
            ToastUtil.error("评价订单");
//            OrderService.viewPayActivity(context, mOrder.orderMain.orderCode);
        }

        @OnClick(R.id.tv_confirmUpgrade)
        public void confirmUpgrade() {
            //TODO: 确认收货按钮
            showConfirmUpgradeDialog(mOrder.orderMain.orderCode);
        }
    }


    public void showConfirmUpgradeDialog(String orderCode) {//确认快速按钮
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("请收到货后确认对产品满意后再点确认收货哦，直接确认升级表示您将放弃消费者退货权益，请慎重考虑");
        //点击对话框以外的区域是否让对话框消失
        builder.setCancelable(false);
        builder.setNegativeButton("我再想想", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.setPositiveButton("考虑好了，确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                //TODO: 请求"确认升级"接口后，刷新界面
                APIManager.startRequest(orderService.giveUpRefund(orderCode), new BaseRequestListener<Object>() {
                    @Override
                    public void onSuccess(Object result) {
                        super.onSuccess(result);
                        EventBus.getDefault().post(new EventMessage(Event.confirmUpgrade));

                    }
                }, context);
            }
        });
        builder.show();
    }

    private void showCancleOrderDialog(Order order) {//取消订单弹框
        View view1 = LayoutInflater.from(context).inflate(R.layout.cancle_order_dialog, null);
        ImageView agreeIv = view1.findViewById(R.id.agreeIv);
        TextView tv_confirmed = view1.findViewById(R.id.tv_confirmed);
        TextView tv_back = view1.findViewById(R.id.tv_back);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog alertDialog = builder.create();
        alertDialog.setView(view1, 0, 0, 0, 0);
        alertDialog.show();

        tv_confirmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (agreeIv.isSelected()) {
                    alertDialog.dismiss();
                    OrderService.cancelOrder(mContext, order);
                } else {
                    ToastUtil.error("取消订单请勾选<我确认还没有转账给对方>");
                }
            }
        });

        agreeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (agreeIv.isSelected()) {
                    agreeIv.setSelected(false);
                } else {
                    agreeIv.setSelected(true);
                }
            }
        });

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    public void onResume() {
        if (mWaitForUpdateOrder != -1) {
            Log.d("Seven", "update order index: " + mWaitForUpdateOrder);
            IOrderService mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
            Order order = items.get(mWaitForUpdateOrder);
            Observable<RequestResult<Order>> orderByCode = mOrderService.getOrderByCode(order.orderMain.orderCode, "");
            if (mModel == OrderListActivity.MODE_LIVE_STORE) orderByCode = mOrderService.orderDetail(order.orderMain.orderCode);
            APIManager.startRequest(orderByCode, new BaseRequestListener<Order>(mContext) {
                @Override
                public void onSuccess(Order result) {
                    super.onSuccess(result);
                    if (mContext == null || mContext.isDestroyed()) {
                        return;
                    }
                    items.set(mWaitForUpdateOrder, result);
                    mWaitForUpdateOrder = -1;
                    notifyDataSetChanged();
                }
            }, mContext);
        }
    }
}
