package com.weiju.ccmall.module.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.model.AuthModel;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.user.SetPayPasswordActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.GongMaoAuth;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ActivityControl;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 实名认证成功
 */
public class AuthIdentitySuccssdActivity extends BaseActivity {

    @BindView(R.id.ivStatus)
    ImageView mIvStatus;
    @BindView(R.id.tvStatus)
    TextView mTvStatus;
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.tvCardNumber)
    TextView mTvCardNumber;
    @BindView(R.id.tvReAuth)
    TextView mTvReAuth;
    @BindView(R.id.tvBankName)
    TextView tvBankName;
    @BindView(R.id.tvBankNum)
    TextView tvBankNum;
    @BindView(R.id.tvGoSetPwd)
    TextView tvGoSetPwd;



    private com.weiju.ccmall.module.auth.model.AuthModel mModel;
    private String mIdCard;
    private String mBankCardId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_identity_succssd);
        ButterKnife.bind(this);
        initData();
        setLeftBlack();
        setTitle("实名认证");
    }

    private void initData() {
        /*
        String str = "<font color='#f51861'>前往签约</font>"+ "<font color= 'gray'>(提现所需)</font>";
        mTvReAuth.setText(Html.fromHtml(str));
        SharedPreferences authType = getSharedPreferences("authType", 0);
        String authType1 = authType.getString("authType", "");
        if(!TextUtils.isEmpty(authType1)){
            if("balance".equals(authType1)){
                mTvReAuth.setVisibility(View.VISIBLE);
            }else if("UserCenter".equals(authType1)){
                mTvReAuth.setVisibility(View.GONE);
            }
        }
         */
        mIdCard = getIntent().getStringExtra("idCard");
        mBankCardId = getIntent().getStringExtra("bankCardId");
        IUserService service = ServiceManager.getInstance().createService(IUserService.class);

        APIManager.startRequest(service.getAuth(), new BaseRequestListener<AuthModel>(this) {
            @Override
            public void onSuccess(com.weiju.ccmall.module.auth.model.AuthModel model) {
                mModel = model;
                setData(model);
            }
        },this);
        tvGoSetPwd.setVisibility(MyApplication.isShowPayPwd ? View.VISIBLE : View.GONE);
    }

    private void setData(com.weiju.ccmall.module.auth.model.AuthModel model) {
        mTvName.setText(model.memberAuthBean.getUserName());
        String identityCard = model.memberAuthBean.getIdentityCard();
//        StringBuffer stringBuffer = new StringBuffer(identityCard);
//        for (int i = 0; i < stringBuffer.length(); i++) {
//            if (i > 1 && i < stringBuffer.length()) {
//                stringBuffer.replace(i-1,i, "*");
//            }
//        }
        mTvCardNumber.setText(identityCard);
        if(!TextUtils.isEmpty(model.memberAccountBean.bankName)&& !TextUtils.isEmpty(model.memberAccountBean.bankAccount)){
            tvBankName.setText(model.memberAccountBean.bankName);
            StringBuffer bankAccount= new StringBuffer(model.memberAccountBean.bankAccount);
//            for (int i = 0; i < bankAccount.length()-5; i++) {
//                bankAccount.replace(i , i+1, "*");
//            }
            tvBankNum.setText(bankAccount);
        }
    }


    @OnClick(R.id.tvGoSetPwd)
    public void onGoSetPwd() {
        SetPayPasswordActivity.start(this, true, mIdCard, mBankCardId);
        finish();
    }

    @OnClick(R.id.tvReAuth)
    public void onViewClicked() {
        /*Intent intent = new Intent(this, AuthPhoneActivity.class);
        intent.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
        intent.putExtra("isEdit", true);
        startActivity(intent);*/
        IUserService service = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(service.getGongMallAuth(), new BaseRequestListener<GongMaoAuth>() {
            @Override
            public void onSuccess(GongMaoAuth model) {
                ToastUtil.hideLoading();
                Intent intent = new Intent(AuthIdentitySuccssdActivity.this, WebViewJavaActivity.class);
                intent.putExtra("url", model.url);
                startActivity(intent);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.hideLoading();
            }
        },this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MyApplication.isShowPayPwd = false;
        ActivityControl.getInstance().closeAll();
    }

    public static void start(Context context, String idCard, String bankCardId) {
        Intent intent = new Intent(context, AuthIdentitySuccssdActivity.class);
        intent.putExtra("idCard", idCard);
        intent.putExtra("bankCardId", bankCardId);
        context.startActivity(intent);
    }

}
