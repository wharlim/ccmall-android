package com.weiju.ccmall.module.balance;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.pay.PayBalanceActivity;
import com.weiju.ccmall.module.pay.PayMsg;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.CaptchaBtn;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBalanceService;
import com.weiju.ccmall.shared.service.contract.ICaptchaService;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/5/8 on 14:21
 * @desc 兑换金券验证密码
 */
public class ExchGoldenTicketPassWordActivity extends BaseActivity {

    @BindView(R.id.phoneTv)
    protected TextView mPhoneTv;
    @BindView(R.id.captchaEt)
    protected EditText mCaptchaEt;
    @BindView(R.id.captchaBtn)
    protected CaptchaBtn mCaptchaBtn;
    @BindView(R.id.passwordEt)
    protected EditText mPasswordEt;
    private ICaptchaService mCaptchaService;
    private IUserService mUserService;
    private User mUser;

    private long mCoinL;
    private IBalanceService mBalanceService = ServiceManager.getInstance().createService(IBalanceService.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_balance);
        ButterKnife.bind(this);
        mCoinL = getIntent().getLongExtra("coin", 0);

        setLeftBlack();
        setTitle("兑换金券");
        mCaptchaService = ServiceManager.getInstance().createService(ICaptchaService.class);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        getUserInfo();
    }

    private void getUserInfo() {
        APIManager.startRequest(mUserService.getUserInfo(), new BaseRequestListener<User>(this) {
            @Override
            public void onSuccess(User result) {
                mUser = result;
                mPhoneTv.setText(StringUtil.maskPhone(mUser.phone));
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.error("获取用户信息失败");
            }
        },this);
    }

    @OnClick(R.id.captchaBtn)
    protected void getCaptcha() {
        String token = StringUtil.md5(Constants.API_SALT + mUser.phone);
        APIManager.startRequest(mCaptchaService.getCaptchaForCheck(token, mUser.phone), new BaseRequestListener<Object>(this) {

            @Override
            public void onSuccess(Object result) {
                mCaptchaBtn.start();
            }
        },this);
    }

    @OnClick(R.id.confirmBtn)
    protected void confirmToPay() {
        String captcha = mCaptchaEt.getText().toString();
        if (captcha.isEmpty()) {
            ToastUtil.error("验证码不能为空");
            return;
        }
        String password = mPasswordEt.getText().toString();
        if (password.isEmpty()) {
            ToastUtil.error("密码不能为空");
            return;
        }

        APIManager.startRequest(mBalanceService.exchGoldenTicket(mCoinL,password,captcha), new BaseRequestListener<Object>(this) {
            @SuppressLint("DefaultLocale")
            @Override
            public void onSuccess(Object exchGoldTicket) {
                ToastUtil.hideLoading();
                ToastUtil.success("兑换成功");
                EventBus.getDefault().post(new EventMessage(Event.exchSuccess));
                finish();
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.hideLoading();
            }
        },this);
    }


}
