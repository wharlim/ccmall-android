package com.weiju.ccmall.module.face

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import com.linkface.liveness.LFLivenessSDK
import com.weiju.ccmall.shared.util.ToastUtil
import java.io.File

class LivenessHelper {

    companion object {
        var isInitSDK = false
    }

    fun start(a: Activity) {
        if (!isInitSDK)
            initSDK(a)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (a.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                a.requestPermissions(
                        arrayOf(Manifest.permission.CAMERA),
                        11)
            } else if (a.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                a.requestPermissions(
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        12)
            } else
                startActivity(a)
        } else
            startActivity(a)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun onRequestPermissionsResult(a: Activity, requestCode: Int, permissions: Array<out String>,  grantResults: IntArray) {
        if (requestCode == 11) {
            // Request for camera permission.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (a.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    a.requestPermissions(
                            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                            12)
                } else
                    startActivity(a)
            } else {
                ToastUtil.success("相机权限未获得")
            }
        }
        if (requestCode == 12) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (a.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    a.requestPermissions(
                            arrayOf(Manifest.permission.CAMERA), 11)
                } else
                    ToastUtil.success("相机权限未获得")
            } else {
                ToastUtil.success("存储权限未获得")
            }
        }
    }

    private fun startActivity(a: Activity) {
        try {
            val bundle = Bundle()
            /**
             * OUTPUT_TYPE 配置, 传入的outputType类型为singleImg （单图），multiImg （多图），video（低质量视频），fullvideo（高质量视频）
             */
            bundle.putString(LivenessActivity.OUTTYPE, "singleImg")
            /**
             * EXTRA_MOTION_SEQUENCE 动作检测序列配置，支持四种检测动作， BLINK(眨眼), MOUTH（张嘴）, NOD（点头）, YAW（摇头）, 各个动作以空格隔开。 推荐第一个动作为BLINK。
             * 默认配置为"BLINK MOUTH NOD YAW"
             */
            bundle.putString(LivenessActivity.EXTRA_MOTION_SEQUENCE, "BLINK MOUTH NOD YAW")
            /**
             * SOUND_NOTICE 配置, 传入的soundNotice为boolean值，true为打开, false为关闭。
             * LFSpUtils.getMusicTipSwitch(this)
             */
            bundle.putBoolean(LivenessActivity.SOUND_NOTICE, true)
            /**
             * COMPLEXITY 配置, 传入的complexity类型为normal,支持四种难度，easy, normal, hard, hell.
             */
            bundle.putString(LivenessActivity.COMPLEXITY, "normal")

            val intent = Intent()
            intent.setClass(a, LivenessActivity::class.java)
            intent.putExtras(bundle)
            //设置返回图片结果
            intent.putExtra(LivenessActivity.KEY_DETECT_IMAGE_RESULT, true)
            //设置返回protobuf结果
            intent.putExtra(LivenessActivity.KEY_DETECT_PROTO_BUF_RESULT, true)
            //设置返回video结果，video模式才会返回
            intent.putExtra(LivenessActivity.KEY_DETECT_VIDEO_RESULT, true)
            a.startActivityForResult(intent, 712)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun initSDK(a: Activity) {
        //初始化Liveness SDK的License路径
        val licPath = File.separator + "liveness_license" + File.separator + "LinkfaceID.lic"
        LFLivenessSDK.getInstance(a).initLicPath(licPath, "LinkfaceID.lic")
    }

}
