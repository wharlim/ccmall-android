package com.weiju.ccmall.module.page;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.LinearLayout;

import com.blankj.utilcode.utils.StringUtils;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.cart.CartActivity;
import com.weiju.ccmall.module.category.CategoryActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.PageConfig;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.contracts.RequestListener;
import com.weiju.ccmall.shared.factory.PageElementFactory;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.page.PageType;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.service.contract.IPageService;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

public class CustomPageActivity extends BaseActivity {

    @BindView(R.id.layoutElement)
    LinearLayout mLayoutElement;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout mRefreshLayout;

    private IPageService mPageService = ServiceManager.getInstance().createService(IPageService.class);
    private String mPageId;
    private List<Element> mElements;
    private static int PAGE_SIZE = 10;
    private int mCurrentPage;

    private PageType pageType = PageType.HOME;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_custom_page);
        ButterKnife.bind(this);

        getIntentData();
        initView();
    }

    private void getIntentData() {
        mPageId = getIntent().getStringExtra("pageId");
        if (StringUtils.isEmpty(mPageId)) {
            ToastUtil.error("参数错误 ");
            finish();
        }
        try {
            pageType = PageType.valueOf(getIntent().getStringExtra("pageType"));
        } catch (Exception e) {
            pageType = PageType.HOME;
        }
    }

    private void initView() {
        setTitle("页面加载中...");
        setLeftBlack();

        mRefreshLayout.setEnableHeaderTranslationContent(false);
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                initData();
            }
        });
        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshLayout) {
                addViews(false);
            }
        });
        mRefreshLayout.setEnableAutoLoadMore(true);//是否启用列表惯性滑动到底部时自动加载更多
        mRefreshLayout.setEnableOverScrollBounce(false);//是否启用越界回弹
        mRefreshLayout.autoRefresh();
    }

    private void initData() {
        switch (mPageId) {
            case Key.PAGE_HOME: {
                Observable<RequestResult<List<Element>>> api = mPageService.getHomePageConfig();
                if (pageType == PageType.JKP) {
                    api = mPageService.getTkHomePageConfig();
                }
                APIManager.startRequest(
                        flatMapElement(api, "首页"),
                        getPageConfigResponseListener(), this
                );
            }
                break;
            default: {
                Observable<RequestResult<PageConfig>> api = mPageService.getPageConfig(mPageId);
                if (pageType == PageType.JKP) {
                    api = mPageService.getTkPageConfig(mPageId);
                }
                APIManager.startRequest(
                        api, getPageConfigResponseListener(),this
                );
            }
                break;
        }
    }

    private void addViews(boolean isRefresh) {
        if (mElements == null) {
            return;
        }
        if (isRefresh) {
            mCurrentPage = 0;
            mLayoutElement.removeAllViews();
            mRefreshLayout.finishRefresh();
            mRefreshLayout.setNoMoreData(false);
        } else {
            mCurrentPage++;
            mRefreshLayout.finishLoadMore();
        }
        int start = mCurrentPage * PAGE_SIZE;
        int end = start + PAGE_SIZE;
        if (end > mElements.size()) {
            end = mElements.size();
            mRefreshLayout.finishLoadMoreWithNoMoreData();
            mRefreshLayout.setNoMoreData(true);
        }
        for (int i = start; i < end; i++) {
            Element element = mElements.get(i);
            mLayoutElement.addView(PageElementFactory.make(this, element));
        }
    }

    private Observable<RequestResult<PageConfig>> flatMapElement(Observable<RequestResult<List<Element>>> elementObservable, final String title) {
        return elementObservable.flatMap(new Function<RequestResult<List<Element>>, Observable<RequestResult<PageConfig>>>() {
            @Override
            public Observable<RequestResult<PageConfig>> apply(RequestResult<List<Element>> result) throws Exception {
                RequestResult<PageConfig> pageConfigRequestResult = new RequestResult<>();
                pageConfigRequestResult.code = result.code;
                pageConfigRequestResult.message = result.message;
                PageConfig pageConfig = new PageConfig();
                pageConfig.title = title;
                pageConfig.elements = result.data;
                pageConfigRequestResult.data = pageConfig;
                return Observable.just(pageConfigRequestResult);
            }
        });
    }


    private RequestListener<PageConfig> getPageConfigResponseListener() {
        return new RequestListener<PageConfig>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(PageConfig config) {
                setTitle(config.title);
//                ArrayList<Element> elements = new ArrayList<>();
//                for (Element datum : result.data) {
//                    datum.type.equals("")
//                }
                mElements = config.elements;
                addViews(true);
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.error(e.getMessage());
                mRefreshLayout.finishRefresh();
            }

            @Override
            public void onComplete() {
//                mRefreshLayout.setRefreshing(false);
            }
        };
    }

    @OnClick(R.id.ivCart)
    protected  void cart(){
        if (UiUtils.checkUserLogin(CustomPageActivity.this)){
            startActivity( new Intent(CustomPageActivity.this, CartActivity.class));
        }
    }


}
