package com.weiju.ccmall.module.community;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.LoginActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.bean.event.MsgFViewPager;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.util.SessionUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * @author Stone
 * @time 2018/3/11  17:23
 * @desc 通用的tabLayout+ViewPager的fragment,adapter的设置是抽象的方法
 */

public class TabViewPagerFragment extends BaseFragment {
    @BindView(R.id.tabLayout)
    SmartTabLayout mTabLayout;
    @BindView(R.id.viewpager)
    ViewPager mViewpager;
    @BindView(R.id.ic_publish_new)
    ImageView mIcPublishNew;
    @BindView(R.id.tab_bottom_line)
    View tabBottonLine;

    @BindView(R.id.tv_finish)
    ImageView tv_finish;

    private int selectTab;

    private TabViewPagerAdapter mTabAdapter;
    private TabViewPagerAdapter mAdapter;

    public static TabViewPagerFragment newInstance(TabViewPageAdapterTag adapterTag) {
        return newInstance(adapterTag, 0);
    }

    public static TabViewPagerFragment newInstance(TabViewPageAdapterTag adapterTag, int selectTab) {
        TabViewPagerFragment tabViewPagerFragment = new TabViewPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.KEY_EXTROS, adapterTag);
        bundle.putInt("selectTab", selectTab);
        tabViewPagerFragment.setArguments(bundle);
        return tabViewPagerFragment;
    }

    @Override
    protected int getFragmentResId() {
        return R.layout.fragment_tab_viewpager;
    }

    @Override
    protected void initViewConfig() {
        super.initViewConfig();
        selectTab = getArguments().getInt("selectTab");

        //标题字体颜色
        mTabLayout.setTitleTextColor(ContextCompat.getColor(mActivity, R.color.colorPrimary),
                ContextCompat.getColor(mActivity, R.color.color_33));
        //滑动条宽度
        mTabLayout.setTabTitleTextSize(16);
        mTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(mActivity, R.color.colorPrimary));
        //均匀平铺选项卡
        mTabAdapter = getTabAdapter();
        mTabLayout.setDistributeEvenly(false);
        mTabLayout.setPadding(mTabAdapter.getTextPadding());
        mTabLayout.setTabStripWidth(mTabAdapter.getBottomLineWidth());
        mViewpager.setAdapter(mTabAdapter);
        mViewpager.setOffscreenPageLimit(mTabAdapter.getCount());
        mTabLayout.setViewPager(mViewpager);
        tabBottonLine.setVisibility(mTabAdapter.showLine() ? View.VISIBLE : View.GONE);
        mViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, float positionOffset, int positionOffsetPixels) {
                mIcPublishNew.setVisibility(mTabAdapter.getVisiableAdd(position) ? View.VISIBLE : View.GONE);
                mIcPublishNew.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
//                EventBus.getDefault().post(new EventMessage(Event.PUBLISH_NEW, null));
                        EventBus.getDefault().post(new MsgFViewPager(mTabAdapter.getAddClickAction(position)));
                    }
                });
                if (mTabAdapter.getNeedLogin(position) && !SessionUtil.getInstance().isLogin()) {
                    mViewpager.setCurrentItem(0, false);
                    startActivity(new Intent(mActivity, LoginActivity.class));
                }
            }

            @Override
            public void onPageSelected(int position) {
                if (mTabAdapter.getSkipPage(position) != null) {
                    Class clazz = mTabAdapter.getSkipPage(position);
                    mViewpager.setCurrentItem(0, false);
                    startActivity(new Intent(mActivity, clazz));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewpager.setCurrentItem(selectTab);
    }

    public TabViewPagerAdapter getTabAdapter() {
        if (mAdapter == null) {
            mAdapter = new TabViewPagerAdapter(mActivity, getChildFragmentManager(),
                    (TabViewPageAdapterTag) getArguments().getSerializable(Constants.KEY_EXTROS));
        }
        return mAdapter;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(EventMessage message) {
        if (message.getEvent().equals(Event.loginSuccess) || message.getEvent().equals(Event.logout)) {
            mIcPublishNew.setVisibility((mTabAdapter.getVisiableAdd(mViewpager.getCurrentItem()) ? View.VISIBLE : View.GONE));
        }
    }

    @Override
    protected void initListener() {
        super.initListener();
    }

    public enum TabViewPageAdapterTag {
        /**
         * 社区和会员的
         */
        COMMUNITY, MEMBERS, SCHOOL, STATISTICS, INTEGRAL_DETAIL, INTEGRAL_LIST,
    }

    @OnClick({R.id.tv_finish})
    public void allViewClick(View view) {
        if (view.getId() == R.id.tv_finish) {
            getActivity().finish();
        }
    }
}
