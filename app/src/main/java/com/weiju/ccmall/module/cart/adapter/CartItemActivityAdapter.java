package com.weiju.ccmall.module.cart.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.bean.CartItem;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.util.MoneyUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.cart.adapter
 * @since 2017-06-19
 */
public class CartItemActivityAdapter extends BaseAdapter<SkuInfo.ActivityTagEntity, CartItemActivityAdapter.ViewHolder> {
    private CartItem mCartItem;

    public CartItemActivityAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_cart_item_present_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SkuInfo.ActivityTagEntity entity = items.get(position);
        String end = "";
        if (mCartItem.isHalfActivity() && entity.type == 0 && mCartItem.amount > 1) {
            end = "，省" + MoneyUtil.centToYuan¥StrNo00End(mCartItem.retailPrice / 2 * (mCartItem.amount / 2));
        }
        holder.mItemTitleTv.setText(entity.getTypeActivityText() + end);
        holder.mItemAmountTv.setVisibility(View.GONE);
    }

    public void setCartItem(CartItem cartItem) {
        mCartItem = cartItem;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemTitleTv)
        protected TextView mItemTitleTv;
        @BindView(R.id.itemAmountTv)
        protected TextView mItemAmountTv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
