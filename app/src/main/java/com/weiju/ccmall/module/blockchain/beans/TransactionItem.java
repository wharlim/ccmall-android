package com.weiju.ccmall.module.blockchain.beans;

import com.google.gson.annotations.SerializedName;

// 交易记录
public class TransactionItem {

    /**
     * id : 41
     * memberId : 2d964a362d8449b9ba887694e09cdf59
     * owner : 1cb3446df9153a0a6e71f82eae8cda37bbf398d18c
     * respStr :
     * target : 1cd869860f02244fcfc1a9cdab1b56a5a5e34bbb30
     * txId :
     * errorStr :
     * targetMemberId :
     * createTime : 2019-11-13 18:36:47
     * updateTime :
     * status : 1
     * type : 4
     * typeStr : 转帐转入
     * amout : 1460
     */

    @SerializedName("id")
    public int id;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("owner")
    public String owner;
    @SerializedName("respStr")
    public String respStr;
    @SerializedName("target")
    public String target;
    @SerializedName("txId")
    public String txId;
    @SerializedName("errorStr")
    public String errorStr;
    @SerializedName("targetMemberId")
    public String targetMemberId;
    @SerializedName("createTime")
    public String createTime;
    @SerializedName("updateTime")
    public String updateTime;
    @SerializedName("status")
    public int status;
    @SerializedName("type")
    public int type;
    @SerializedName("typeStr")
    public String typeStr;
    @SerializedName("amout")
    public String amout;

    public String getAmount() {
        if (amout.startsWith("-")) {
            return amout;
        }
        return "+" + amout;
    }

    public boolean isAmountNegative() {
        return amout.startsWith("-");
    }
}
