package com.weiju.ccmall.module.foot;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.module.product.event.MsgProduct;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Chan on 2017/6/15.
 */

public class FootAdapter extends BaseAdapter<SkuInfo, FootAdapter.ViewHolder> {


    public FootAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_foot, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.setFoot(items.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), NewProductDetailActivity.class);
                intent.putExtra(Key.SKU_ID, items.get(position).skuId);
                holder.itemView.getContext().startActivity(intent);
            }
        });
        holder.itemView.findViewById(R.id.itemTrashBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MsgProduct msgProduct = new MsgProduct(MsgProduct.DEL_VIEW_HOSTORY);
                msgProduct.setSkuInfo(items.get(position));
                EventBus.getDefault().post(msgProduct);
            }
        });
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemThumbIv)
        protected SimpleDraweeView mItemThumbIv;
        @BindView(R.id.itemTitleTv)
        protected TextView mItemTitleTv;
        @BindView(R.id.itemMoneyTv)
        protected TextView mItemMoneyTv;
        @BindView(R.id.itemMoneyTv2)
        protected TextView mItemMoneyTv2;
        @BindView(R.id.tvCount)
        protected TextView mtvCount;
        @BindView(R.id.ivBanjia)
        protected ImageView mBanjiaView;
        @BindView(R.id.tvCCM)
        protected TextView mTvCCM;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void setFoot(final SkuInfo skuInfo) {
            mItemTitleTv.setText(skuInfo.name);
            mItemMoneyTv.setText(String.valueOf(MoneyUtil.centToYuan¥StrNoZero(skuInfo.retailPrice)));
            mItemMoneyTv2.setText(ConvertUtil.centToCurrency(mItemMoneyTv.getContext(), skuInfo.marketPrice));
            TextViewUtil.addThroughLine(mItemMoneyTv2);
//            mItemMoneyTv2.setVisibility(View.VISIBLE);
            mtvCount.setText("销量：" + skuInfo.totalSaleCount);
            mTvCCM.setText(String.format("奖%s%%算率", skuInfo.countRateExc));
            FrescoUtil.setSkuImgSmallMask(mItemThumbIv, skuInfo);

            mBanjiaView.setVisibility(skuInfo.isBanjia() ? View.VISIBLE : View.GONE);
        }
    }

}
