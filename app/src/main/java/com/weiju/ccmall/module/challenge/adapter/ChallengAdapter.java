package com.weiju.ccmall.module.challenge.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Challenge;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;

/**
 * @author chenyanming
 * @time 2019/1/28 on 15:50
 * @desc 挑战
 */
public class ChallengAdapter extends BaseQuickAdapter<Challenge, BaseViewHolder> {
    public ChallengAdapter(@Nullable List<Challenge> data) {
        super(R.layout.item_challeng_layout, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Challenge item) {
        long downTime = ((TimeUtils.string2Millis(item.expiresDate)) - System.currentTimeMillis()) / 1000;
        TextView textView = helper.getView(R.id.tvTitle);
        TextView textViewTime = helper.getView(R.id.tvTitleTime);
        if (item.activityStatus == 1 && downTime > 0) {
            textViewCountDown(textViewTime, (int) downTime, item);
            textViewTime.setVisibility(View.VISIBLE);
            textView.setVisibility(View.GONE);
        } else {
            textViewTime.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
            String format;
            switch (item.type) {
                case 0:
                    format = String.format("<font color=\"#333333\">挑战开始后%s小时内成功销售</font> " +
                                    "<font color=\"#FFA800\"> %s </font> <font color=\"#333333\">份大礼包，" +
                                    "可获得</font> <font color=\"#FFA800\"> %s </font> <font color=\"#333333\">" +
                                    "幸运积分</font><font color=\"#EF0000\"> %s </font>",
                            item.lifeCycle, item.target, item.score, getActivityStatusStr(item.activityStatus));
                    break;
                case 1:
                    format = String.format("<font color=\"#333333\">%s小时内成功升级至</font> " +
                            "<font color=\"#29D6D3\"> %s </font> <font color=\"#333333\">,可获得" +
                            "</font> <font color=\"#29D6D3\"> %s </font> <font color=\"#333333\">" +
                            "幸运积分</font><font color=\"#EF0000\">   %s </font>", item.lifeCycle, item.target, item.score, getActivityStatusStr(item.activityStatus));

                    break;
                case 2:
                case 3:
                case 4:
                    format = mContext.getResources().getString(R.string.challeng_item_pk_tip,
                            item.lifeCycle, item.getTypeString(), "奖励");
                    break;
                default:
                    format = mContext.getResources().getString(R.string.challeng_item_pk_tip,
                            item.lifeCycle, item.getTypeString(), "奖励");
            }
            helper.setText(R.id.tvTitle, Html.fromHtml(format));
        }
        helper.setImageResource(R.id.ivAvatar, item.type == 0 ? R.drawable.ic_challeng_sell :
                (item.type == 1 ? R.drawable.ic_challeng_up : R.drawable.ic_challeng_pk));
    }

    private String getActivityStatusStr(int activityStatus) {
        switch (activityStatus) {
            case 0:
            case 1:
                return "";
            case 2:
                return "(挑战成功)";
            case 3:
                return "(挑战失败)";
            default:
                return "";
        }
    }

    public void textViewCountDown(final TextView textView, final int timeCount, final Challenge item) {
        Observable
                .interval(0, 1, TimeUnit.SECONDS)
                .take(timeCount)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<Long, Long>() {
                    @Override
                    public Long apply(@NonNull Long aLong) throws Exception {
                        return timeCount - aLong;

                    }
                })
                .subscribe(new Observer<Long>() {

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Long aLong) {
                        long downTime = ((TimeUtils.string2Millis(item.expiresDate)) - System.currentTimeMillis()) / 1000;
                        long hours = downTime / 3600;
                        long minutes = downTime / 60 % 60;
                        long seconds = downTime % 60;
                        String time = String.format("%s:%s:%s", hours < 10 ? "0" + hours : hours, minutes < 10 ? "0" + minutes : minutes,
                                seconds < 10 ? "0" + seconds : seconds);

                        String format;
                        switch (item.type) {
                            case 0:
                                format = String.format("<font color=\"#333333\">挑战开始后%s小时内成功销售</font> " +
                                                "<font color=\"#FFA800\"> %s </font> <font color=\"#333333\">份大礼包，" +
                                                "可获得</font> <font color=\"#FFA800\"> %s </font> <font color=\"#333333\">" +
                                                "幸运积分</font><font color=\"#EF0000\"> (%s) </font>",
                                        item.lifeCycle, item.target, item.score, time);
                                break;
                            case 1:
                                format = String.format("<font color=\"#333333\">%s小时内成功升级至</font> " +
                                                "<font color=\"#29D6D3\"> %s </font> <font color=\"#333333\">,可获得" +
                                                "</font> <font color=\"#29D6D3\"> %s </font> <font color=\"#333333\">" +
                                                "幸运积分</font><font color=\"#EF0000\">   (%s) </font>", item.lifeCycle,
                                        item.target, item.score, time);
                                break;
                            case 2:
                                format = String.format("对决开始后%s小时后，PSP贡献值较高的可获得奖励</font><font color=\"#EF0000\">   (%s) </font>", item.lifeCycle, time);
                                break;
                            case 3:
                                format = String.format("对决开始后%s小时后，TSP贡献值较高的可获得奖励</font><font color=\"#EF0000\">   (%s) </font>", item.lifeCycle, time);
                                break;
                            case 4:
                                format = String.format("对决开始后%s小时后，TSE贡献值较高的可获得奖励</font><font color=\"#EF0000\">   (%s) </font>", item.lifeCycle, time);
                                break;
                            default:
                                format = String.format("对决开始后%s小时后，TSE贡献值较高的可获得奖励</font><font color=\"#EF0000\">   (%s) </font>", item.lifeCycle, time);
                        }
                        textView.setText(Html.fromHtml(format));

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        textView.setText("进行中");
                        EventBus.getDefault().post((new EventMessage(Event.countDownComplete)));
                    }
                });
    }
}
