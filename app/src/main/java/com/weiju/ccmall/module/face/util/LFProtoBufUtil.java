package com.weiju.ccmall.module.face.util;

/**
 * Copyright (c) 2017-2018 LINKFACE Corporation. All rights reserved
 */
public class LFProtoBufUtil {
    private static byte[] protoBuf = null;

    public static byte[] getProtoBuf() {
        return protoBuf;
    }

    public static void setProtoBuf(byte[] protoBuf) {
        LFProtoBufUtil.protoBuf = protoBuf;
    }
}
