package com.weiju.ccmall.module.auth;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.auth.model.AuthModel;
import com.weiju.ccmall.module.pickUp.adapter.PickUpAdapter;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.PickUp;
import com.weiju.ccmall.shared.bean.VouchersTransfer;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ActivityControl;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * 所有提交状态的页面
 */
public class SubmitStatusActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.ivStatus)
    ImageView mIvStatus;
    @BindView(R.id.tvStatus)
    TextView mTvStatus;
    @BindView(R.id.tvTips)
    TextView mTvTips;
    @BindView(R.id.tvSubmit)
    TextView mTvSubmit;
    @BindView(R.id.tvBigText)
    TextView mTvBigText;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.llTransfer)
    LinearLayout llTransfer;
    private int mAction;

    String remark;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_status);
        ButterKnife.bind(this);
        mTvSubmit.setOnClickListener(this);
        showHeader();
        setLeftBlack();
        setTitle("实名认证");
        EventBus.getDefault().register(this);

    }

    private void initData() {
        IUserService service = ServiceManager.getInstance().createService(IUserService.class);

        APIManager.startRequest(service.getAuth(), new BaseRequestListener<AuthModel>(this) {
            @Override
            public void onSuccess(AuthModel model) {

                remark = model.memberAuthBean.getAuthRemark();
                mTvTips.setText("失败原因:" + remark);
            }
        }, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void getMsg(MsgStatus msgStatus) {
        mAction = msgStatus.getAction();
        switch (mAction) {
            case Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_SUBMIT_SUCCESS:
                EventBus.getDefault().removeStickyEvent(MsgStatus.class);
                mTvStatus.setText("提交成功");
                mTvTips.setText("我们将在1~2个工作日内审核完成");
                mTvSubmit.setText("完成");
                mIvStatus.setImageResource(R.drawable.ic_user_auth_submit_succeed);
                break;
            case Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_WAIT:
                EventBus.getDefault().removeStickyEvent(MsgStatus.class);
                mTvStatus.setText("审核中");
                mTvTips.setText("我们将在1~2个工作日内审核完成");
                mIvStatus.setImageResource(R.drawable.ic_user_auth_submit_wait);
                mTvSubmit.setVisibility(View.GONE);
                break;
            case Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_FAIL:
                EventBus.getDefault().removeStickyEvent(MsgStatus.class);
                mTvStatus.setText("审核失败");
                initData();
                mTvSubmit.setText("返回修改");
                mIvStatus.setImageResource(R.drawable.ic_user_auth_submit_fail);
                break;
            case Config.USER.INTENT_KEY_TYPE_AUTH_CARD_SUBMIT_SUCCESS:
                setTitle("绑定银行卡");
                EventBus.getDefault().removeStickyEvent(MsgStatus.class);
                mTvStatus.setText("提交成功");
                mTvTips.setText("我们将在1~2个工作日内审核完成");
                mTvSubmit.setText("完成");
                mIvStatus.setImageResource(R.drawable.ic_user_auth_submit_succeed);
                break;
            case Config.USER.INTENT_KEY_TYPE_AUTH_CARD_WAIT:
                setTitle("绑定银行卡");
                EventBus.getDefault().removeStickyEvent(MsgStatus.class);
                mTvStatus.setText("审核中");
                mTvTips.setText("我们将在1~2个工作日内审核完成");
                mIvStatus.setImageResource(R.drawable.ic_user_auth_submit_wait);
                mTvSubmit.setVisibility(View.GONE);
                break;
            case Config.USER.INTENT_KEY_TYPE_AUTH_CARD_FAIL:
                setTitle("绑定银行卡");
                mTvStatus.setText("审核失败");
                initData();
//                mTvTips.setText("失败原因:" + remark);
                mTvSubmit.setText("返回修改");
                mIvStatus.setImageResource(R.drawable.ic_user_auth_submit_fail);
                break;
            case AppTypes.TRANSFER.TRANSFER_MONEY_SUCESS:
                EventBus.getDefault().removeStickyEvent(MsgStatus.class);
                setTitle("转账成功");
                mTvStatus.setText("转账成功");
                mTvTips.setText(msgStatus.getTips());
                mTvSubmit.setText("完成");
                mIvStatus.setImageResource(R.drawable.ic_user_auth_submit_succeed);
                mTvBigText.setText(String.format("%.2f", msgStatus.getMoney()));
                mTvBigText.setVisibility(View.VISIBLE);
                break;
            case AppTypes.TRANSFER.TRANSFER_SCORE_SUCESS:
                EventBus.getDefault().removeStickyEvent(MsgStatus.class);
                setTitle("转赠成功");
                mTvStatus.setText("转赠成功");
                mTvTips.setText(msgStatus.getTips());
                mTvSubmit.setText("完成");
                mIvStatus.setImageResource(R.drawable.ic_user_auth_submit_succeed);
                mTvBigText.setText(String.format("%d积分", msgStatus.getMoney()));
                mTvBigText.setVisibility(View.VISIBLE);
                break;
            case AppTypes.TRANSFER.DEAL_SUCESS:
                EventBus.getDefault().removeStickyEvent(MsgStatus.class);
                setTitle("申请成功");
                mTvStatus.setText("申请成功");
                mTvTips.setText("审核完成后，将在5个工作日内打款到您的银行卡");
                mTvSubmit.setText("完成");
                mIvStatus.setImageResource(R.drawable.ic_user_auth_submit_wait);
                break;
            case AppTypes.TRANSFER.DEAL_ALI_SUCESS:
                EventBus.getDefault().removeStickyEvent(MsgStatus.class);
                setTitle("申请成功");
                mTvStatus.setText("申请成功");
                mTvTips.setText("无需手续费，我们会在1～3个工作日内审核打款");
                mTvSubmit.setText("完成");
                mIvStatus.setImageResource(R.drawable.ic_user_auth_submit_succeed);
                break;
            case AppTypes.TRANSFER.PICKUP_COUPON:
                EventBus.getDefault().removeStickyEvent(MsgStatus.class);
                VouchersTransfer transfer = msgStatus.getVouchersTransfer();
                VouchersTransfer.DataBean data = transfer.data;
                if (data != null && data.successCount > 0) {
                    setTitle("赠送成功");
                    mTvStatus.setText("赠送成功");
                    if (data.failCount > 0) {
                        llTransfer.setVisibility(View.VISIBLE);
                        initPickUpView(data.failList);
                    }
                } else {
                    setTitle("赠送失败");
                    mTvStatus.setText("赠送失败");
                }
                mTvStatus.setTextColor(getResources().getColor(R.color.text_black));
                mTvTips.setText(msgStatus.getTips());
                mTvSubmit.setText("完成");
                mTvSubmit.setBackgroundResource(R.drawable.btn_pickup_common);
                Drawable d = getResources().getDrawable(R.drawable.ic_user_auth_submit_succeed).mutate();
                DrawableCompat.setTint(d, Color.parseColor("#3867FF"));
                mIvStatus.setImageDrawable(d);
                break;
        }
    }

    private void initPickUpView(List<PickUp> transfer) {
        PickUpAdapter adapter = new PickUpAdapter(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(adapter);
        adapter.setNewData(transfer);
    }

    @Override
    public void onClick(View v) {
        switch (mAction) {
            case Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_SUBMIT_SUCCESS:
                finish();
                break;
            case Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_WAIT:
                finish();
                break;
            case Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_FAIL:
                /*Intent intent = new Intent(this, AuthPhoneActivity.class);
                intent.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
                intent.putExtra("isEdit", true);
                startActivity(intent);*/
                Intent intent = new Intent(this, AuthIdentityActivity.class);
                startActivity(intent);
                finish();
                break;
            case Config.USER.INTENT_KEY_TYPE_AUTH_CARD_FAIL:
                /*Intent intent2 = new Intent(this, MyApplication.isGongCat?AuthPhoneActivity2.class:AuthPhoneActivity.class);
                intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_CARD);
                intent2.putExtra("isEdit", true);
                startActivity(intent2);*/
                Intent intent2 = new Intent(this, AuthIdentityActivity.class);
                startActivity(intent2);
                finish();
                break;

            default:
                finish();
                break;
        }
        ActivityControl.getInstance().closeAll();
    }
}
