package com.weiju.ccmall.module.live.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.balance.BalanceDetailActivity;
import com.weiju.ccmall.module.deal.DealFirstActivity;
import com.weiju.ccmall.module.live.activity.BindAlipayActivity;
import com.weiju.ccmall.module.live.activity.FundsManagementActivity;
import com.weiju.ccmall.module.live.adapter.FundsManagementAdapter;
import com.weiju.ccmall.module.live.entity.AlipayAccountEntity;
import com.weiju.ccmall.module.live.entity.SettleAccountEntity;
import com.weiju.ccmall.module.live.entity.StoreHomeEntity;
import com.weiju.ccmall.module.live.widgets.LoanTypePopupMenu;
import com.weiju.ccmall.module.order.OrderDetailActivity;
import com.weiju.ccmall.module.order.OrderListActivity;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class FundsManagementFragment extends BaseListFragment {

    @BindView(R.id.tvAmountTitle)
    TextView tvAmountTitle;
    @BindView(R.id.tvAmount)
    TextView tvAmount;
    @BindView(R.id.tvWithdraw)
    TextView tvWithdraw;
    @BindView(R.id.tvType)
    TextView tvType;
    @BindView(R.id.rlType)
    RelativeLayout rlType;

    private List<SettleAccountEntity> mData = new ArrayList<>();
    private FundsManagementAdapter mAdapter;

    private int mType;
    private int mLoanType;
    private int mPage;
    ILiveStoreService mService;
    private LoanTypePopupMenu mPopupMenu;
    private String mMoney;
    private long mCent;

    @Override
    public int getContentLayoutRes() {
        return R.layout.fragment_funds_management;
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mType = getArguments().getInt("type");
        mAdapter = new FundsManagementAdapter(mData, mType);
    }

    @Override
    public void initView() {
        super.initView();
        if (mType == FundsManagementActivity.TYPE_SETTLE) {
            rlType.setVisibility(View.GONE);
            tvWithdraw.setVisibility(View.GONE);
            tvAmountTitle.setText("待结算金额");
        } else {
            rlType.setVisibility(View.VISIBLE);
            tvWithdraw.setVisibility(View.VISIBLE);
            tvAmountTitle.setText("货款金额");
            mPopupMenu = new LoanTypePopupMenu(getContext(), (menuIndex, title) -> {
                tvType.setText(title);
                mLoanType = menuIndex;
                getData(true);
            });
            mPopupMenu.setOnDismissListener(() -> tvType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0));
        }
    }

    @Override
    public void initData() {
        mService = ServiceManager.getInstance().createService(ILiveStoreService.class);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.bg_gray));
        super.initData();
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        SettleAccountEntity item = mAdapter.getItem(position);
        if (item != null) {
            if (item.type == 2) {
                Intent intent = new Intent(getContext(), BalanceDetailActivity.class);
                intent.putExtra("did", item.dealId);
                intent.putExtra("typeId", 6L);
                getContext().startActivity(intent);
            } else {
                Intent intent = new Intent(getContext(), OrderDetailActivity.class);
                intent.putExtra("orderCode", item.orderCode);
                intent.putExtra("mode", OrderListActivity.MODE_LIVE_STORE);
                getContext().startActivity(intent);
            }
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(final boolean isRefresh) {
        if (isRefresh) {
            mPage = 1;
        } else {
            mPage++;
        }
        APIManager.startRequest(mType == FundsManagementActivity.TYPE_SETTLE ? mService.getSettleAccounts(mPage, 15)
                        : mService.getBalanceStream(mLoanType, mPage, 15),
                new BaseRequestListener<PaginationEntity<SettleAccountEntity, StoreHomeEntity>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<SettleAccountEntity, StoreHomeEntity> result) {
                        super.onSuccess(result);
                        StoreHomeEntity ex = result.ex;
                        if (ex != null){
                            mCent = mType == FundsManagementActivity.TYPE_SETTLE ? ex.waitProfitMoney : ex.totalMoney;
                            mMoney = MoneyUtil.centToYuanStrNoZero(mCent);
                            tvAmount.setText(String.format("¥%s", mMoney));
                        }
                        if (mPage == 1) {
                            mData.clear();
                        }

                        String time = "";
                        ArrayList<SettleAccountEntity> entities = result.list;
                        for (SettleAccountEntity entity : entities) {
                            String s = parseYM(entity.payDate);
                            if (!time.equals(s)) {
                                time = s;
                                entity.month = time;
                            }
                        }

                        mData.addAll(entities);
                        mAdapter.notifyDataSetChanged();
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && mData.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, getContext()
        );

    }

    public static String parseYM(String time) {
        String year = time.substring(0, 4);
        String month = time.substring(5, 7);
        return String.format("%s年%s月", year, month);
    }

    @OnClick(R.id.tvType)
    public void onViewClicked() {
        tvType.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
        mPopupMenu.showAsDropDown(tvType);
    }

    @OnClick(R.id.tvWithdraw)
    public void onWithdrawClicked() {
        if (mCent <= 0) {
            ToastUtil.error("货款金额必须大于0");
            return;
        }
        APIManager.startRequest(mService.checkAlipayAccount(), new BaseRequestListener<AlipayAccountEntity>(getActivity()) {
            @Override
            public void onSuccess(AlipayAccountEntity result) {
                super.onSuccess(result);
                result.totalMoney = mMoney;
                if (result.accountStatus != 1) {
                    BindAlipayActivity.start(getContext(), result);
                } else {
                    DealFirstActivity.start(getContext(), result);
                }
            }
        }, getContext());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(MsgStatus status) {
        switch (status.getAction()) {
            case MsgStatus.ACTION_DEAL_SUCESS:
                if (mType == FundsManagementActivity.TYPE_LOAN) {
                    getData(true);
                }
                break;
            default:
                break;
        }
    }

    /**
     * @param type 待结算：0    贷款账户：1
     * @return
     */
    public static FundsManagementFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        FundsManagementFragment fragment = new FundsManagementFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
