package com.weiju.ccmall.module.balance;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.utils.BlockChainUtil;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.bean.Balance;
import com.weiju.ccmall.shared.bean.CommonExtra;
import com.weiju.ccmall.shared.bean.DayProfit;
import com.weiju.ccmall.shared.enums.AccountType;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.weiju.ccmall.shared.enums.AccountType.PCP;
import static com.weiju.ccmall.shared.enums.AccountType.PSP;
import static com.weiju.ccmall.shared.enums.AccountType.TSP;

/**
 * Created by Chan on 2017/6/17.
 *
 * @author Chan
 * @package com.weiju.ccmall.module.balance
 * @since 2017/6/17 下午3:12
 */

public class BalanceAdapter extends BaseAdapter<Balance, RecyclerView.ViewHolder> {

    private CommonExtra mExtra;
    private DayProfit mDayProfit;

    public BalanceAdapter(Context context) {
        super(context);
        hasHeader = true;
    }

    private AccountType type;

    public void setType(AccountType type) {
        this.type = type;
    }

    public void setDayProfit(DayProfit dayProfit) {
        mDayProfit = dayProfit;
        notifyItemChanged(0);
    }

    public DayProfit getDayProfit() {
        return mDayProfit;
    }

    private View.OnClickListener mOnClickListener;

    public void setGetDayProfitOnClickListener(View.OnClickListener onClickListener) {
        mOnClickListener = onClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 0) {
            return new HeaderViewHolder(layoutInflater.inflate(R.layout.common_header_layout, parent, false));
        }
        return new ViewHolder(layoutInflater.inflate(R.layout.item_balance, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position == 0) {
            ((HeaderViewHolder) holder).setData(mExtra, mDayProfit, mOnClickListener);

        } else {
            ((ViewHolder) holder).setBalance(items.get(position - 1));
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? 0 : 1;
    }

    public void setHeaderData(CommonExtra ex) {
        mExtra = ex;
        notifyItemChanged(0);
    }

    protected class HeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.titleTv)
        protected TextView mTitleTv;
        @BindView(R.id.totalProfitTv)
        protected TextView mTotalProfitTv;
        @BindView(R.id.leftProfitTv)
        protected TextView mLeftProfitTv;
        @BindView(R.id.textView)
        protected TextView mTextView;
        @BindView(R.id.CCMTv)
        protected TextView mCCMTv;
        @BindView(R.id.layoutDayProfit)
        protected RelativeLayout mLayoutDayProfit;
        @BindView(R.id.tvDayProfit)
        protected TextView mTvDayProfit;
        @BindView(R.id.tvGetDayProfit)
        protected TextView mTvGetDayProfit;
        @BindView(R.id.tvGetDayProfiting)
        protected TextView mTvGetDayProfiting;
        @BindView(R.id.tvGetDayProfitSuccess)
        protected TextView mTvGetDayProfitSuccess;
        @BindView(R.id.tvDayProfiting)
        protected TextView mTvDayProfiting;


        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setData(CommonExtra mExtras, DayProfit dayProfit, View.OnClickListener onClickListener) {
            if (dayProfit != null && type == AccountType.Profit) {
                mLayoutDayProfit.setVisibility(View.VISIBLE);
                mTvGetDayProfitSuccess.setVisibility(dayProfit.status == 1 ? View.VISIBLE : View.GONE);
                mTvGetDayProfit.setVisibility(dayProfit.status != 1 && dayProfit.status != 5 ? View.VISIBLE : View.GONE);
                mTvGetDayProfiting.setVisibility(dayProfit.status == 5 ? View.VISIBLE : View.GONE);
                mTvDayProfit.setText(MoneyUtil.centToYuanStrNoZero(dayProfit.money));
                mTvDayProfiting.setVisibility(dayProfit.status == 5 ? View.VISIBLE : View.INVISIBLE);
                mTvDayProfit.setVisibility(dayProfit.status != 5 ? View.VISIBLE : View.GONE);

                mTvGetDayProfit.setOnClickListener(onClickListener);
                if (dayProfit.money == 0) {
                    mTvGetDayProfitSuccess.setText("CCM达到200方可分红");
                    mTvGetDayProfitSuccess.setVisibility(View.VISIBLE);
                    mTvGetDayProfit.setVisibility(View.GONE);
                    mTvGetDayProfiting.setVisibility(View.GONE);
                    mTvGetDayProfitSuccess.setTextColor(Color.parseColor("#666666"));
                }

            }

            if (mExtras == null) {
                mExtras = new CommonExtra();
                mExtras.profitSumCoin = 0;
                mExtras.freezeSumCoin = 0;
                mExtras.todayUnfreezeCoin = 0;
                mExtras.sumTicket = 0;
                mExtras.availableMoney = 0;
                mExtras.profitSumMoney = 0;
                mExtras.profitSumGold = 0;
                mExtras.freezeSumGold = 0;
                mExtras.unfreezeSumGold = 0;

            }
            mLeftProfitTv.setVisibility(View.GONE);
            mTextView.setText(String.format("%s明细", type.getName()));
            mTitleTv.setText(String.format("%s合计", type.getName()));
            switch (type) {
                case CCM:
                    mTotalProfitTv.setText(String.valueOf(MoneyUtil.coinToYuanStrNoZero(mExtras.availableCoin)));
                    mCCMTv.setVisibility(View.VISIBLE);
                    mCCMTv.setText(String.format("有%1$s个%2$s待解冻，今日已解冻%3$s个", MoneyUtil.coinToYuanStrNoZero(mExtras.freezeSumCoin), type.getName(),
                            MoneyUtil.coinToYuanStrNoZero(mExtras.todayUnfreezeCoin)));
                    break;
                case CB:
                    mTotalProfitTv.setText(String.valueOf(ConvertUtil.cent2yuanNoZero(mExtras.sumTicket)));
                    break;
                case Balance:
                    mTotalProfitTv.setText(String.valueOf(ConvertUtil.cent2yuanNoZero(mExtras.availableMoney)));
                    mTextView.setText("收支明细");
                    mTitleTv.setText(String.format("您的账户%s（元）", type.getName()));
                    break;
                case Profit:
                    mTotalProfitTv.setText(String.valueOf(ConvertUtil.cent2yuanNoZero(mExtras.profitSumMoney)));
                    break;
                case ShopMoney:
                    mCCMTv.setVisibility(View.VISIBLE);
                    mCCMTv.setText(String.format("有%1$s%2$s待解冻，今日已解冻%3$s", ConvertUtil.cent2yuanNoZero(mExtras.freezeSumGold), type.getName(),
                            ConvertUtil.cent2yuanNoZero(mExtras.unfreezeSumGold)));
                    mTotalProfitTv.setText(String.valueOf(ConvertUtil.cent2yuanNoZero(mExtras.availableGold)));
                    mTitleTv.setText(String.format("您的%s", type.getName()));
                    mTextView.setText("收支明细");
                    break;
                case GoldenTicket:
                    mTotalProfitTv.setText(String.valueOf(ConvertUtil.cent2yuanNoZero(mExtras.goldenAvlTicket)));
                    mTextView.setText("金券明细");
                    mTitleTv.setText("金券合计");
                    break;
                case PSP:
                case PCP:
                case TSP:
                    mTextView.setText("收支明细");
                    mTitleTv.setText(String.format("您的%s", type.getName()));
                    mTotalProfitTv.setText(type.getName().equals("PCP值") ? ConvertUtil.cent2yuanNoZero(mExtras.totalPoint / 10)
                            : ConvertUtil.cent2yuanNoZero(mExtras.totalPoint));
                    break;
                default:
            }

        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.itemTimeTv)
        protected TextView mItemTimeTv;
        @BindView(R.id.itemTypeTv)
        protected TextView mItemTypeTv;
        @BindView(R.id.itemProfitTv)
        protected TextView mItemProfitTv;
        @BindView(R.id.itemStatusTv)
        protected TextView mItemStatusTv;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void setBalance(final Balance balance) {
            long money = balance.getMoney(type);
            if (money < 0) {
                mItemProfitTv.setText(type == AccountType.CCM ?
                        MoneyUtil.coinToYuanStrNoZero(money) : ConvertUtil.cent2yuanNoZero(money));
                mItemProfitTv.setTextColor(context.getResources().getColor(R.color.green));
            } else {
                mItemProfitTv.setText(String.format(Locale.CHINA, "+%s", type == AccountType.CCM ?
                        MoneyUtil.coinToYuanStrNoZero(money) : ConvertUtil.cent2yuanNoZero(money)));
                mItemProfitTv.setTextColor(context.getResources().getColor(R.color.red));
            }
            if (type == PCP || type == TSP || type == PSP) {
                mItemProfitTv.setText((balance.formatPoint>=0 ? "+" : "") +
                        BlockChainUtil.formatCCMCoin(balance.formatPoint));
                if (balance.formatPoint >= 0) {
                    mItemProfitTv.setTextColor(context.getResources().getColor(R.color.red));
                } else {
                    mItemProfitTv.setTextColor(context.getResources().getColor(R.color.green));
                }
            }
            mItemStatusTv.setText(String.format("状态：%s", balance.statusStr));
            mItemTimeTv.setText(String.format("时间：%s", balance.getCreateDate(type)));
            mItemTypeTv.setText(String.format("类型：%s", balance.getTypeName(type)));
            switch (type) {
                case PCP:
                case TSP:
                case PSP:
                    mItemStatusTv.setVisibility(View.GONE);
                    mItemTypeTv.setVisibility(View.GONE);
                    break;
                default:
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (balance.typeId < 6 && type == AccountType.Balance) {
                        Intent intent = new Intent(context, BalanceDetailActivity.class);
                        intent.putExtra("typeId", balance.typeId);
                        intent.putExtra("did", balance.did);
                        context.startActivity(intent);
                    }

                }
            });
        }
    }
}
