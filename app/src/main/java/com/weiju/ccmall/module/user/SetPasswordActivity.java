package com.weiju.ccmall.module.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.base.Strings;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.transferout.SimpleTextWatcher;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.component.CaptchaBtn;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICaptchaService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 *
 *
 *
 * @author zjm
 * @date 2017/12/14
 */
public class SetPasswordActivity extends BaseActivity {

    @BindView(R.id.newPasswordEt)
    protected EditText mNewPasswordEt;

    @BindView(R.id.surePasswordEt)
    protected EditText mSurePasswordEt;
    @BindView(R.id.tvPhone)
    TextView mTvPhone;
    @BindView(R.id.captchaEt)
    EditText mCaptchaEt;
    @BindView(R.id.captchaBtn)
    CaptchaBtn mCaptchaBtn;
    @BindView(R.id.editBtn)
    TextView mEditBtn;
    private IUserService mUserService;
    private User mLoginUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
        ButterKnife.bind(this);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        initView();
        initData();
    }

    private void initView() {
        boolean isReset = getIntent().getBooleanExtra("isReset", false);
        setTitle(isReset ? "重置登录密码" : "设置登录密码");
        setLeftBlack();
    }

    private void initData() {
        if (!SessionUtil.getInstance().isLogin()) {
            ToastUtil.error("用户未登录");
            finish();
        }
        mLoginUser = SessionUtil.getInstance().getLoginUser();
        mTvPhone.setText(ConvertUtil.maskPhone(mLoginUser.phone));

        initEditText(mCaptchaEt);
        initEditText(mNewPasswordEt);
        initEditText(mSurePasswordEt);
    }

    private void initEditText(EditText editText) {
        editText.addTextChangedListener(new SimpleTextWatcher(){
            @Override
            public void afterTextChanged(Editable s) {
                String captcha = mCaptchaEt.getText().toString().trim();
                String newPass = mNewPasswordEt.getText().toString();
                String surePass = mSurePasswordEt.getText().toString();
                if (captcha.length() > 0 && newPass.length() > 0 && surePass.length() > 0) {
                    mEditBtn.setEnabled(true);
                } else {
                    mEditBtn.setEnabled(false);
                }
            }
        });
    }

    @OnClick(R.id.captchaBtn)
    public void onViewClicked() {
        ICaptchaService service = ServiceManager.getInstance().createService(ICaptchaService.class);
        String phone = mLoginUser.phone;
        String token = StringUtil.md5(BuildConfig.TOKEN_SALT + phone);
        APIManager.startRequest(service.getCaptchaForUpdate(token, phone), new BaseRequestListener<Object>(this) {

            @Override
            public void onSuccess(Object result) {
                mCaptchaBtn.start();
            }

        },this);
    }

    @OnClick(R.id.editBtn)
    protected void onEdit() {
        String newPass = mNewPasswordEt.getText().toString();
        String surePass = mSurePasswordEt.getText().toString();

        if (Strings.isNullOrEmpty(newPass)) {
            ToastUtil.error("请输入新密码");
            mNewPasswordEt.requestFocus();
            return;
        }
        if (Strings.isNullOrEmpty(surePass)) {
            ToastUtil.error("请确认新密码");
            mSurePasswordEt.requestFocus();
            return;
        }
        if (!newPass.equals(surePass)) {
            ToastUtil.error("两次密码输入不一致");
            mSurePasswordEt.requestFocus();
            return;
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("phone", mLoginUser.phone);
        params.put("newPass", newPass);
        params.put("checkNumber", mCaptchaEt.getText().toString());
        ToastUtil.showLoading(this);
        APIManager.startRequest(mUserService.putPassword(params), new BaseRequestListener<Object>(this) {

            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("操作成功");
                finish();
            }
        },this);

    }

    public static void start(Context context, boolean isReset) {
        Intent intent = new Intent(context, SetPasswordActivity.class);
        intent.putExtra("isReset", isReset);
        context.startActivity(intent);
    }

}
