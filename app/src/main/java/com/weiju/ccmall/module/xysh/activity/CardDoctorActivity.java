package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.component.dialog.AuthCodeInputDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class CardDoctorActivity extends BaseActivity {

    AuthCodeInputDialog authDialog;
    @BindView(R.id.etCreditCard)
    EditText etCreditCard;
    @BindView(R.id.ivSelectCreditCard)
    ImageView ivSelectCreditCard;
    @BindView(R.id.etPhone)
    EditText etPhone;
    @BindView(R.id.etIdCard)
    EditText etIdCard;
    @BindView(R.id.etName)
    EditText etName;

    private static final int REQ_SELECT_CREDIT = 1;
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    private String serialNumber;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_doctor);
        ButterKnife.bind(this);
        setLeftBlack();
        setTitle("信用卡评测");
        getHeaderLayout().setRightText("查看历史结果");
        getHeaderLayout().setOnRightClickListener(v -> {
            CardDoctorHistoryActivity.start(this);
        });
    }

    private void showAuthCodeDialog() {
        if (authDialog != null && authDialog.isShowing()) {
            authDialog.dismiss();
        }
        authDialog = new AuthCodeInputDialog(this, "提示",
                "请输入您收到的验证码", "", etStr -> {
            ToastUtil.showLoading(this, true);
            APIManager.startRequest(service.msgVerify(
                    etStr, serialNumber, SessionUtil.getInstance().getOAuthToken()
            ), new Observer<XYSHCommonResult<String>>() {
                @Override
                public void onSubscribe(Disposable d) {

                }

                @Override
                public void onNext(XYSHCommonResult<String> ret) {
                    ToastUtil.hideLoading();
                    if (ret.success) {
                        getAppraisalResult(etStr, ret.data);
                    } else {
                        ToastUtil.error(ret.message+"");
                    }
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });
        });
        authDialog.show();
    }

    private void getAppraisalResult(String identifyingCode, String orderNo) {
        String cardNo = etCreditCard.getText().toString();
        String phone = etPhone.getText().toString();
        String idNo = etIdCard.getText().toString();
        String name = etName.getText().toString();
        APIManager.startRequest(service.getAppraisalResult(
                identifyingCode, serialNumber, cardNo, phone, orderNo, name, idNo, SessionUtil.getInstance().getOAuthToken()
        ), new Observer<XYSHCommonResult<String>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<String> ret) {
                if (ret.success) {
                    Intent intent = new Intent(CardDoctorActivity.this, WebViewJavaActivity.class);
                    intent.putExtra("url", ret.data);
                    startActivity(intent);
                } else {
                    ToastUtil.error("" + ret.message);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public static void start(Context context) {
        Intent activity = new Intent(context, CardDoctorActivity.class);
        context.startActivity(activity);
    }

    @OnClick({R.id.ivSelectCreditCard, R.id.tvAdd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivSelectCreditCard:
                BankAdminActivity.startForSelectCredit(this, REQ_SELECT_CREDIT);
                break;
            case R.id.tvAdd:
                confirm();
                break;
        }
    }

    private void confirm() {
        String cardNo = etCreditCard.getText().toString();
        if (TextUtils.isEmpty(cardNo)) {
            ToastUtil.error("请输入信用卡号！");
            return;
        }
        if (!UiUtils.checkETPhone(etPhone)) {
            return;
        }
        String phone = etPhone.getText().toString();
        String idNo = etIdCard.getText().toString();
        if (TextUtils.isEmpty(idNo)) {
            ToastUtil.error("请输入身份证！");
            return;
        }
        String name = etName.getText().toString();
        if (TextUtils.isEmpty(name)) {
            ToastUtil.error("请输入姓名！");
            return;
        }
        ToastUtil.showLoading(this, true);
        BankUtils.checkCard(this, cardNo, BankUtils.CARD_CHECK_ACCEPT_CREDIT, (pass, type) -> {
            if (pass) {
                APIManager.startRequest(service.creditCardAppraisalSendMsg(
                        cardNo, phone, name, idNo, SessionUtil.getInstance().getOAuthToken()
                ), new Observer<XYSHCommonResult<String>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(XYSHCommonResult<String> ret) {
                        ToastUtil.hideLoading();
                        if (ret.success) {
                            serialNumber = ret.data;
                            showAuthCodeDialog();
                        } else {
                            ToastUtil.error(ret.message+"");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
            } else {
                if ("DC".equals(type)) {
                    ToastUtil.error("请输入信用卡！");
                } else {
                    ToastUtil.error("卡号不正确!");
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case REQ_SELECT_CREDIT:
                    QueryUserBankCardResult.BankInfListBean card = BankAdminActivity.getSelectResult(data);
                    etCreditCard.setText(card.cardNo);
                    etPhone.setText(card.resTel);
                    break;
            }
        }
    }
}
