package com.weiju.ccmall.module.blockchain.computingpower;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;

import com.weiju.ccmall.module.blockchain.beans.AllCCm;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.util.ToastUtil;

public class TransactionFragment extends BlockFragment{

    TransactionAdapter transactionAdapter;
    AllCCm.AccountTransctionBean accountTransctionBean;
    @Override
    protected void initView() {
        accountTransctionBean = (AllCCm.AccountTransctionBean) getArguments().getSerializable("accountTransctionBean");
        transactionAdapter = new TransactionAdapter(accountTransctionBean.content());
        recyclerView.setAdapter(transactionAdapter);
        try {
            pageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(recyclerView)
                    .setRequestListener(this)
                    .setSwipeRefreshLayout(refreshLayout)
                    .setNoDataLayout(noDataLayout)
                    .build(getContext());
            pageManager.onRefresh(); // 初始化数据
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }
        refreshLayout.setEnabled(false);
    }

    public static TransactionFragment newInstance(AllCCm.AccountTransctionBean accountTransctionBean) {
        Bundle args = new Bundle();
        args.putSerializable("accountTransctionBean", accountTransctionBean);
        TransactionFragment fragment = new TransactionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void nextPage(int page) {

    }
}
