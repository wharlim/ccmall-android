package com.weiju.ccmall.module.world.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.world.entity.OrderEntity;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

import java.util.List;

/**
 * @author Ben
 * @date 2020/4/10.
 */
public class WorldOrderListAdapter extends BaseQuickAdapter<OrderEntity, BaseViewHolder> {

    public WorldOrderListAdapter(@Nullable List<OrderEntity> data) {
        super(R.layout.item_world_order_list, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, OrderEntity product) {
        FrescoUtil.setImageSmall(helper.getView(R.id.sdvGoodsIcon), product.mainImagesUrl);
        helper.setText(R.id.tvGoodsName, product.skuName)
                .setText(R.id.tvGoodsDesc, product.properties)
                .setText(R.id.tvGoodsCount, "x" + product.count)
                .setText(R.id.tvStatus, product.statusStr)
                .setText(R.id.tvGoodsPrice, MoneyUtil.centToYuan¥Str(product.price))
                .setText(R.id.tvActualPrice, MoneyUtil.centToYuan¥Str(product.actualPrice))
                .setText(R.id.tvTotalPrice, MoneyUtil.centToYuan¥Str(product.totalPrice))
                .addOnClickListener(R.id.tvCheckLogistics)
                .addOnClickListener(R.id.tvConfirmReceipt)
                .addOnClickListener(R.id.tvBuyAgain);
        if (product.orderStatus == 0 || product.orderStatus == 4) {
            helper.setVisible(R.id.llClick, true);
            if (product.orderStatus == 0) {
                helper.setVisible(R.id.tvCheckLogistics, false)
                        .setVisible(R.id.tvConfirmReceipt, false)
                        .setVisible(R.id.tvBuyAgain, true);
            } else {
                helper.setVisible(R.id.tvCheckLogistics, true)
                        .setVisible(R.id.tvConfirmReceipt, true)
                        .setVisible(R.id.tvBuyAgain, false);
            }
        } else {
            helper.setVisible(R.id.llClick, false);
        }

        if (product.orderStatus == 0 || product.orderStatus == 1) {
            helper.setText(R.id.tvPayTag, "待付款：");
        } else {
            helper.setText(R.id.tvPayTag, "实付款：");
        }

        if (product.refundStatus != 0) {
            helper.setVisible(R.id.tvRefundStatus, true)
                    .setText(R.id.tvRefundStatus, product.refundStatusStr);
        } else {
            helper.setVisible(R.id.tvRefundStatus, false);
        }
    }

}
