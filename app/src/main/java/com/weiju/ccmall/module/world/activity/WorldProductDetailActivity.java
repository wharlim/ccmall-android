package com.weiju.ccmall.module.world.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.bigkoo.convenientbanner.listener.OnItemClickListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.world.IWorldService;
import com.weiju.ccmall.module.world.entity.CatgoryItemEntity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Tag;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.common.CarouselItemViewHolder;
import com.weiju.ccmall.shared.component.DragScrollDetailsLayout;
import com.weiju.ccmall.shared.component.WorldTagTextView;
import com.weiju.ccmall.shared.component.dialog.WorldSkuDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.CSUtils;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.WebViewUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorldProductDetailActivity extends BaseActivity {
    @BindView(R.id.bannerView)
    ConvenientBanner bannerView;
    @BindView(R.id.tvTagTitle)
    WorldTagTextView tvTagTitle;
    @BindView(R.id.tvGoodsDesc)
    TextView tvGoodsDesc;
    @BindView(R.id.tvGoodsPrice)
    TextView tvGoodsPrice;
    @BindView(R.id.tvStock)
    TextView tvStock;
    @BindView(R.id.tvWarehouse)
    TextView tvWarehouse;
    @BindView(R.id.tvModel)
    TextView tvModel;
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvUpTitle)
    TextView tvUpTitle;
    @BindView(R.id.tvService)
    TextView tvService;
    @BindView(R.id.layoutProductBottom)
    LinearLayout layoutProductBottom;
    @BindView(R.id.tvBuyNow)
    TextView mTvBuyNow;
    @BindView(R.id.layoutDragScroll)
    DragScrollDetailsLayout mLayoutDragScroll;
    @BindView(R.id.ivBack2)
    ImageView mIvBack2;
    @BindView(R.id.layoutTitle)
    RelativeLayout mLayoutTitle;
    @BindView(R.id.tvRate)
    TextView tvRate;
    @BindView(R.id.ivRate)
    ImageView ivRate;
    @BindView(R.id.tvCCM)
    TextView tvCCM;
    private boolean mCanLoop = false;

    private CatgoryItemEntity mCatgoryItemEntity;

    private IWorldService mService = ServiceManager.getInstance().createService(IWorldService.class);
    private WorldSkuDialog mSkuDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_product_detail);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    private void initView() {
//        QMUIStatusBarHelper.translucent(this);
        bannerView.stopTurning();
        initScrollView();
    }

    private void initData() {
        String itemId = getIntent().getStringExtra("itemId");
        APIManager.startRequest(mService.getGoodsDetail(itemId), new BaseRequestListener<CatgoryItemEntity>(this) {
            @Override
            public void onSuccess(CatgoryItemEntity result) {
                super.onSuccess(result);
                mCatgoryItemEntity = result;
//                CarouselUtil.initCarousel(bannerView, result.mainImages, R.drawable.icon_page_indicator_focused);

                bannerView.setPages(new CBViewHolderCreator<CarouselItemViewHolder<String>>() {
                            @Override
                            public CarouselItemViewHolder<String> createHolder() {
                                return new CarouselItemViewHolder<>();
                            }
                        }, result.mainImages)
                        .setPageIndicator(new int[]{R.drawable.icon_page_indicator, R.drawable.icon_page_indicator_focused})
                        .setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL)
                        .setOnItemClickListener(new OnItemClickListener() {
                            @Override
                            public void onItemClick(int position) {

                            }
                        })
                        .setPointViewVisible(result.mainImages.size() > 1)
                        .setCanLoop(mCanLoop);
                if (result.tax > 0) {
                    double tax = MoneyUtil.keepTwoDecimals(result.tax * 100);
                    tvRate.setText(MessageFormat.format("税率{0}%", tax));
                    tvRate.setVisibility(View.VISIBLE);
                    ivRate.setVisibility(View.GONE);
                } else {
                    tvRate.setVisibility(View.GONE);
                    ivRate.setVisibility(View.VISIBLE);
                }
                List<Tag> tags = new ArrayList<>();
                Tag tag = new Tag();
                tag.name = "全球仓";
                tags.add(tag);
                tvTagTitle.setTags(tags);
                tvTagTitle.setText(result.skuName);
                tvStock.setText(MessageFormat.format("库存：{0}", result.stockQuantity));
                if (result.stockQuantity <= 0) {
                    mTvBuyNow.setEnabled(false);
                    mTvBuyNow.setText("已售罄");
                }
                if (TextUtils.isEmpty(result.title)) {
                    tvGoodsDesc.setVisibility(View.GONE);
                } else {
                    tvGoodsDesc.setVisibility(View.VISIBLE);
                    tvGoodsDesc.setText(result.title);
                }
                tvGoodsPrice.setText(MoneyUtil.centToYuan¥Str(result.price));
                tvModel.setText(result.attr);

//                if (result.countRateExc == 0) {
//                    tvCCM.setVisibility(View.GONE);
//                } else {
                    tvCCM.setVisibility(View.VISIBLE);
                    tvCCM.setText(Html.fromHtml(String.format("预计可返<font color=#8B57F4>%s%%</font>算率", result.countRateExc)));
//                }
                List<String> detailImages = result.detailImages;
                if (detailImages.size() > 0) {
                    StringBuilder htmlUrl = new StringBuilder();
                    for (int i = 0; i < detailImages.size(); i++) {
                        htmlUrl.append(String.format("<img src=\"%s\"/>", detailImages.get(i)));
                        if (i == detailImages.size() - 1) {
                            htmlUrl.append("<br/>");
                        }
                    }
                    WebViewUtil.loadDataToWebView(webView, String.valueOf(htmlUrl));
                }
            }
        }, this);

    }

    /**
     * 设置那些滑动变换的 view
     */
    private void initScrollView() {
        mLayoutDragScroll.setOnSlideDetailsListener(new DragScrollDetailsLayout.OnSlideFinishListener() {
            @Override
            public void onStatueChanged(DragScrollDetailsLayout.CurrentTargetIndex status) {
                switch (status) {
                    case UPSTAIRS:  //滑到上面
                        ivBack.setVisibility(View.VISIBLE);
                        tvUpTitle.setVisibility(View.VISIBLE);
                        mLayoutTitle.setVisibility(View.GONE);
                        break;
                    case DOWNSTAIRS:    //滑到下面
                        ivBack.setVisibility(View.GONE);
                        tvUpTitle.setVisibility(View.GONE);
                        mLayoutTitle.setVisibility(View.VISIBLE);
                        break;
                    default:

                }
            }
        });
    }

    @OnClick({R.id.ivBack, R.id.ivBack2, R.id.tvService, R.id.tvBuyNow})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
            case R.id.ivBack2:
                finish();
                break;
            case R.id.tvService:
                if (mCatgoryItemEntity == null) {
                    return;
                }
                CSUtils.start(this, "从产品详情点进来的，正在查看 " + mCatgoryItemEntity.brandName);
                break;
            case R.id.tvBuyNow:
                if (mSkuDialog == null) {
                    mSkuDialog = new WorldSkuDialog(this, mCatgoryItemEntity);
                }
                mSkuDialog.show();
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(EventMessage message) {
        if (message.getEvent() == Event.worldAddOrderSuccess) {
            finish();
        }
    }

    public static void start(Context context, String itemId) {
        Intent intent = new Intent(context, WorldProductDetailActivity.class);
        intent.putExtra("itemId", itemId);
        context.startActivity(intent);
    }

}
