package com.weiju.ccmall.module.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.util.CSUtils;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/6/23.
 */
public class ResetPayPasswordActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pay_password);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        setTitle("重置支付密码");
        setLeftBlack();
    }

    @OnClick(R.id.tvContact)
    public void onViewClicked() {
        CSUtils.start(this, "");
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, ResetPayPasswordActivity.class);
        context.startActivity(intent);
    }

}
