package com.weiju.ccmall.module.live.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.RefundType;

public class ListPopupAdapter extends BaseQuickAdapter<RefundType, BaseViewHolder> {

    public ListPopupAdapter() {
        super(R.layout.item_simple_text);
    }

    @Override
    protected void convert(BaseViewHolder helper, RefundType item) {
        helper.setText(R.id.text, item.values);
    }
}
