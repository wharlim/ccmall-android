package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebView;
import android.widget.ImageView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.utils.TemplateWebUtil;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.util.CSUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HelpWebActivity extends BaseActivity {

    @BindView(R.id.webview)
    WebView webview;
    @BindView(R.id.ivCs)
    ImageView ivCs;

    public static void start(Context context) {
        Intent activity = new Intent(context, HelpWebActivity.class);
        context.startActivity(activity);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_web);
        ButterKnife.bind(this);
        setTitle("帮助中心");
        setLeftBlack();
        TemplateWebUtil.setUpWebView(webview);
        webview.getSettings().setTextZoom(200);
        webview.loadUrl("http://credit.ccmallv2.create-chain.net/ccpay/ccpay_help.html");
    }

    @OnClick(R.id.ivCs)
    public void onViewClicked() {
        CSUtils.start(this, "客服", "102a49c45bb54dbbb2394becdaffbce2");
    }
}
