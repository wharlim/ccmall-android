package com.weiju.ccmall.module.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangeCustomerNoteNameActivity extends BaseActivity {

    public static final int REQUEST_CODE = 908;
    @BindView(R.id.noteNameEt)
    EditText noteNameEt;
    private IUserService mService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_note);
        ButterKnife.bind(this);
        getHeaderLayout().setTitle("修改备注");
        setLeftBlack();
        noteNameEt.setText(getIntent().getStringExtra("noteName"));
        mService = ServiceManager.getInstance().createService(IUserService.class);
    }

    public void setLeftBlack() {
        mHeaderLayout.setLeftDrawable(R.mipmap.icon_back_black);
        mHeaderLayout.setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public static void start(Activity context, String memberId, String noteName) {
        Intent intent = new Intent(context, ChangeCustomerNoteNameActivity.class);
        intent.putExtra("memberId", memberId);
        intent.putExtra("noteName", noteName);
        context.startActivityForResult(intent, REQUEST_CODE);
    }

    @OnClick(R.id.confirmBtn)
    public void onViewClicked() {
        String memberId = getIntent().getStringExtra("memberId");
        String noteName = noteNameEt.getText().toString().trim();
        if (TextUtils.isEmpty(noteName)) {
            ToastUtil.error("备注不能为空！");
            return ;
        }
        BaseRequestListener listener = new BaseRequestListener() {
            @Override
            public void onSuccess(Object result) {
                //super.onSuccess(result);
                Intent data = new Intent();
                data.putExtra("memberId", memberId);
                data.putExtra("noteName", noteName);
                setResult(RESULT_OK, data);
                finish();
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.error("修改失败！");
            }
        };
        APIManager.startRequest(mService.saveMemberNoteName(memberId, noteName),listener, this);
    }
}
