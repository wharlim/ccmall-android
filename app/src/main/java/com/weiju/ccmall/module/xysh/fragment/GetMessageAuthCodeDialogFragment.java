package com.weiju.ccmall.module.xysh.fragment;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.AuthChannelActivity;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class GetMessageAuthCodeDialogFragment extends DialogFragment {

    @BindView(R.id.et_input_authcode)
    EditText etInputAuthcode;
    @BindView(R.id.tv_get_authcode)
    TextView tvGetAuthcode;
    @BindView(R.id.tv_confirm)
    TextView tvConfirm;
    Unbinder unbinder;
    QueryUserBankCardResult.BankInfListBean creditCard;
    ChannelItem channel;
    private String phone;
    private OnConfirmListener mOnConfirmListener;
    private CountDownTimer mCountDownTimer;
    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    private int authType = AuthChannelActivity.AUTH_TYPE_REPAY;

    public GetMessageAuthCodeDialogFragment show(FragmentManager fm) {
        show(fm, "GetMessageAuthCodeDialogFragment");
        return this;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_get_message_authcode, container, false);
        unbinder = ButterKnife.bind(this, view);
        authType = getArguments().getInt("authType", AuthChannelActivity.AUTH_TYPE_REPAY);
        phone = getArguments().getString("phone");
        channel = (ChannelItem) getArguments().getSerializable("channel");
        creditCard = (QueryUserBankCardResult.BankInfListBean) getArguments().getSerializable("creditCard");
        if (phone != null && phone.length() >= 11) {
            phone = phone.substring(0, 3) + "****" + phone.substring(7, 11);
        }
//        etInputAuthcode.setHint(phone);
        etInputAuthcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                tvConfirm.setEnabled(!TextUtils.isEmpty(etInputAuthcode.getText().toString().trim()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
//        tvConfirm.setEnabled(false);
        setCancelable(false);
//        getAuthCode();
        if (authType == AuthChannelActivity.AUTH_TYPE_REPAY && channel.authCodeStatus == 2){
            tvGetAuthcode.setEnabled(false);
            ToastUtil.success("验证码已发送到手机:"+phone);
            startTimeCounter();
        }
        return view;
    }

    private void getAuthCode() {
        Observable<XYSHCommonResult<Object>> req = null;
        if (authType == AuthChannelActivity.AUTH_TYPE_REPAY) {
            req = service.authChannelSendSMS(channel.channelShortName, creditCard.cardNo);
        } else if (authType == AuthChannelActivity.AUTH_TYPE_RECEIPT) {
            req = service.gftqPay_SendSMS(creditCard.cardNo);
        } else {
            throw new IllegalArgumentException("authType error: " + authType);
        }
        tvGetAuthcode.setEnabled(false);
        APIManager.startRequest(req,
                new Observer<XYSHCommonResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(XYSHCommonResult<Object> ret) {
                        if (!isAdded()) {
                            return;
                        }
                        if (ret.code == 1080000) {
                            ToastUtil.success(ret.msg);
                            tvGetAuthcode.postDelayed(() -> getActivity().finish(),500);
                            return;
                        }
                        if (ret.code != 1) {
                            ToastUtil.error(ret.msg);
                            tvGetAuthcode.setEnabled(true);
                            return;
                        }
                        ToastUtil.success("验证码已发送到手机:"+phone);
                        startTimeCounter();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtil.error(e.getMessage());
                        tvGetAuthcode.setEnabled(true);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private void startTimeCounter() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        mCountDownTimer = new CountDownTimer(60000, 1000){

            @Override
            public void onTick(long millisUntilFinished) {
                tvGetAuthcode.setText((millisUntilFinished / 1000) + "s");
                tvGetAuthcode.setEnabled(false);
            }

            @Override
            public void onFinish() {
                tvGetAuthcode.setText("短信验证码");
                tvGetAuthcode.setEnabled(true);
            }
        }.start();
    }

    public static GetMessageAuthCodeDialogFragment newInstance(
            String phone,
            QueryUserBankCardResult.BankInfListBean creditCard,
            ChannelItem channel,
            int authType) {
        GetMessageAuthCodeDialogFragment fragment = new GetMessageAuthCodeDialogFragment();
        Bundle args = new Bundle();
        args.putString("phone", phone);
        args.putSerializable("creditCard", creditCard);
        args.putSerializable("channel", channel);
        args.putInt("authType", authType);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    @OnClick(R.id.tv_confirm)
    public void onConfirm() {
        if (TextUtils.isEmpty(etInputAuthcode.getText().toString().trim())) {
            ToastUtil.error("请输入验证码！");
            return;
        }
        dismiss();
        if (mOnConfirmListener != null) {
            mOnConfirmListener.onConfirm(etInputAuthcode.getText().toString().trim());
        }
    }

    @OnClick(R.id.tv_get_authcode)
    public void onGetAuthCode() {
        getAuthCode();
    }

    @OnClick(R.id.ivClose)
    public void onClose() {
        dismiss();
    }

    public interface OnConfirmListener {
        void onConfirm(String authCode);
    }

    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        mOnConfirmListener = onConfirmListener;
    }
}
