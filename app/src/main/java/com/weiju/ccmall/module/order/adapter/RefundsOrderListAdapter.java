package com.weiju.ccmall.module.order.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.OrderProduct;
import com.weiju.ccmall.shared.bean.RefundsOrder;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;

import java.util.List;

public class RefundsOrderListAdapter extends BaseQuickAdapter<RefundsOrder, BaseViewHolder> {

    private Context mContext;
    private boolean mSellerModel;
    private String mRefundType;
    private int mProductType;

    public RefundsOrderListAdapter(Context context, @Nullable List<RefundsOrder> data) {
        super(R.layout.item_refunds_order, data);
        mContext = context;
    }

    @Override
    protected void convert(BaseViewHolder helper, RefundsOrder item) {
        helper.addOnClickListener(R.id.tvItemCS);
        helper.addOnClickListener(R.id.tvItemEdit);
        helper.addOnClickListener(R.id.tvItemCancel);
        helper.addOnClickListener(R.id.tvItemInput);
        helper.addOnClickListener(R.id.tvItemStoreRefuse);
        helper.addOnClickListener(R.id.tvItemStoreAgree);
        helper.addOnClickListener(R.id.tvItemStoreFinish);

        helper.setText(R.id.tvRefundStatus, item.apiRefundOrderBean.refundStatusStr);

        RecyclerView rvProduch = helper.getView(R.id.rvProduch);
        rvProduch.setEnabled(false);
        rvProduch.addItemDecoration(new ListDividerDecoration(mContext));
        rvProduch.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        OrderItemAdapter orderItemAdapter = new OrderItemAdapter(mContext, item.orderProducts);
        orderItemAdapter.setRefundModel(true);
        orderItemAdapter.setSellerModel(mSellerModel);
        rvProduch.setAdapter(orderItemAdapter);
        rvProduch.setHasFixedSize(true);
        rvProduch.addItemDecoration(new ListDividerDecoration(mContext));
        rvProduch.setClickable(false);
        LinearLayout layout = helper.getView(R.id.layoutBottom);
        for (int i = 0; i < layout.getChildCount(); i++) {
            layout.getChildAt(i).setVisibility(View.GONE);
        }
        if (mSellerModel) {
            helper.setText(R.id.tvRefundsCode, "售后单号：" + item.apiRefundOrderBean.refundCode);
            helper.setText(R.id.tvPrompt, item.apiRefundOrderBean.storePromptList);
            helper.setText(R.id.tvItemStoreRefuse, item.apiRefundOrderBean.isRefundMoney() ? "拒绝退款" : "拒绝退货");
            helper.setText(R.id.tvItemStoreAgree, item.apiRefundOrderBean.isRefundMoney() ? "同意退款" : "同意退货");
            if (mProductType == 28){
                helper.addOnClickListener(R.id.tvRefundsCode);
                TextView mStoreNameTv = helper.getView(R.id.tvRefundsCode);
                mStoreNameTv.setText(item.apiRefundOrderBean.nickName);
                mStoreNameTv.setTextColor(mContext.getResources().getColor(R.color.red));
                Drawable drawable= mContext.getResources().getDrawable(R.drawable.ic_order_message);
                drawable.setBounds(0, 0, SizeUtils.dp2px(17), SizeUtils.dp2px(17));
                mStoreNameTv.setCompoundDrawablePadding(SizeUtils.dp2px(10));
                mStoreNameTv.setCompoundDrawables(drawable,null,null,null);
                switch (item.apiRefundOrderBean.refundStatus) {
                    case 0:
                        helper.setVisible(R.id.tvItemStoreRefuse, true);
                        helper.setVisible(R.id.tvItemStoreAgree, true);
                        break;
                    case 2:
                        helper.setVisible(R.id.tvItemStoreFinish, true);
                        break;
                }
            }
        } else {
            OrderProduct orderProduct = item.orderProducts.get(0);
            if (orderProduct.productType == 28){
                helper.setText(R.id.tvItemCS, "联系店主");
            }
            helper.setText(R.id.tvRefundsCode, "售后单号：" + item.apiRefundOrderBean.refundCode);
            helper.setText(R.id.tvPrompt, item.apiRefundOrderBean.mePromptList);
            if (item.apiRefundOrderBean.isRefundMoney()) {
                switch (item.apiRefundOrderBean.refundStatus) {
                    case 0:
                        helper.setVisible(R.id.tvItemCS, true);
                        helper.setVisible(R.id.tvItemEdit, true);
                        helper.setVisible(R.id.tvItemCancel, true);
                        break;
//                    case -1:
//                        break;
                    default:
                        helper.setVisible(R.id.tvItemCS, true);
                        break;
                }
            } else {
                switch (item.apiRefundOrderBean.refundStatus) {
                    case 0:
                        helper.setVisible(R.id.tvItemCS, true);
                        helper.setVisible(R.id.tvItemEdit, true);
                        helper.setVisible(R.id.tvItemCancel, true);
                        break;
                    case 1:
                        helper.setVisible(R.id.tvItemCS, true);
                        helper.setVisible(R.id.tvItemInput, true);
                        helper.setVisible(R.id.tvItemCancel, true);
                        break;
                    case 2:
                        helper.setVisible(R.id.tvItemCS, true);
                        helper.setVisible(R.id.tvItemCancel, true);
                        break;
                    default:
                        helper.setVisible(R.id.tvItemCS, true);
                        break;
                }
            }
        }
    }

    public void setSellerModel(boolean sellerModel) {
        mSellerModel = sellerModel;
    }

    /**
     * @param refundType
     */
    public void setRefundType(String refundType) {
        mRefundType = refundType;
    }

    public void setProductType(int productType) {
        mProductType = productType;
    }
}
