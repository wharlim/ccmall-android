package com.weiju.ccmall.module.auth.event;

public class BaseMsg {
    private int action;

    public BaseMsg(int action) {
        this.action = action;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }
}
