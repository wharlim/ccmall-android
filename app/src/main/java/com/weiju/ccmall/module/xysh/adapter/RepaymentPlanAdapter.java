package com.weiju.ccmall.module.xysh.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.PlanForCard;

public class RepaymentPlanAdapter extends BaseQuickAdapter<PlanForCard, BaseViewHolder> {
    public RepaymentPlanAdapter() {
        super(R.layout.item_repayment_plan);
    }

    @Override
    protected void convert(BaseViewHolder helper, PlanForCard item) {
        helper.setText(R.id.tv_bank, item.bankName);
        helper.setText(R.id.tv_money, String.format("%.2f元", item.payBackAmt));
        helper.setText(R.id.tv_time, item.tmSmpStr.substring(0, 10));
        String state = "";
        if (item.isRunning()) {
            state = "进行中";
            helper.setTextColor(R.id.tv_state, helper.itemView.getResources().getColor(R.color.text_gold));
        } else if (item.isSuccess()) {
            state = "交易成功";
            helper.setTextColor(R.id.tv_state, helper.itemView.getResources().getColor(R.color.text_black));
        } else if (item.isFail()) {
            state = "交易失败";
            helper.setTextColor(R.id.tv_state, helper.itemView.getResources().getColor(R.color.color_text_red));
        }
        helper.setText(R.id.tv_state, state);
    }
}
