package com.weiju.ccmall.module.product;

import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.adapter.ProductCommentAdapter;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.ProductComment;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.FrescoUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 查看全部评论或者查看单个评论详情
 */
public class ProductCommentListActivity extends BaseListActivity {

    private ArrayList<ProductComment> mDatas = new ArrayList<>();

    private ProductCommentAdapter mAdapter;

    private ProductCommentAdapter mAdapterCanDeleteComment;
    private IProductService mService;
    private String mId;
    private String mOrder1Id;
    private String mName;
    private String mImg;
    private boolean mIsDetail;

    private String deleteComment = "";

    @Override
    public void getIntentData() {
        mIsDetail = getIntent().getBooleanExtra("isDetail", false);
        mId = getIntent().getStringExtra("id");
        mOrder1Id = getIntent().getStringExtra("order1Id");
        mImg = getIntent().getStringExtra("img");
        mName = getIntent().getStringExtra("name");
        deleteComment = getIntent().getStringExtra("deleteComment");
        mAdapterCanDeleteComment = new ProductCommentAdapter(mDatas, "",mId); // 删除按钮的状态有内部数据决定
        mAdapter = new ProductCommentAdapter(mDatas, "", mId);
    }

    @Override
    public String getTitleStr() {
        return mIsDetail ? "评价详情" : "查看全部评价";
    }

    @Override
    public List<View> getHeaderViews() {
        LinearLayout layoutHead = (LinearLayout) View.inflate(this, R.layout.view_product_comment_head, null);
        SimpleDraweeView ivProduct = layoutHead.findViewById(R.id.ivProduct);
        TextView tvProduct = layoutHead.findViewById(R.id.tvProductName);
        tvProduct.setText(mName);
        FrescoUtil.setImageSmall(ivProduct, mImg);

        List<View> views = new ArrayList<>();
        views.add(layoutHead);
        return views;
    }

    @Override
    public boolean isLoadMore() {
        return !mIsDetail;
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    public BaseQuickAdapter getAdapter() {
        if (TextUtils.isEmpty(deleteComment))
            return mAdapter;
        else
            return mAdapterCanDeleteComment;
    }

    @Override
    public void initData() {
        mService = ServiceManager.getInstance().createService(IProductService.class);
        mRecyclerView.setBackgroundResource(R.color.bg_gray);
        super.initData();
    }

    @Override
    public void getData(boolean isRefresh) {
        if (isRefresh) {
            if (TextUtils.isEmpty(deleteComment)) {
                mDatas.clear();
                mAdapter.notifyDataSetChanged();
            } else {
                mDatas.clear();
                mAdapterCanDeleteComment.notifyDataSetChanged();
            }
        }
        if (mIsDetail) {
            loadDetail(isRefresh);
        } else {
            loadList(isRefresh);
        }
    }

    private void loadDetail(boolean isRefresh) {
        APIManager.startRequest(
                mService.getProductCommentDetail(
                        mId,
                        mOrder1Id,
                        mDatas.size() / Constants.PAGE_SIZE + 1,
                        Constants.PAGE_SIZE
                ),
                new BaseRequestListener<List<ProductComment>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(List<ProductComment> result) {
                        super.onSuccess(result);

                        setData(result);
                    }
                }, this
        );
    }

    private void setData(List<ProductComment> result) {
        if (TextUtils.isEmpty(deleteComment)) {
            mDatas.addAll(result);
            mAdapter.notifyDataSetChanged();
            if (result.size() < Constants.PAGE_SIZE) {
                mAdapter.loadMoreEnd();
            } else {
                mAdapter.loadMoreComplete();
            }
        } else {
            mDatas.addAll(result);
            mAdapterCanDeleteComment.notifyDataSetChanged();
            if (result.size() < Constants.PAGE_SIZE) {
                mAdapterCanDeleteComment.loadMoreEnd();
            } else {
                mAdapterCanDeleteComment.loadMoreComplete();
            }
        }

    }

    private void loadList(boolean isRefresh) {
        APIManager.startRequest(
                mService.getProductComment(mId, mDatas.size() / Constants.PAGE_SIZE + 1, Constants.PAGE_SIZE),
                new BaseRequestListener<PaginationEntity<ProductComment, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<ProductComment, Object> result) {
                        setData(result.list);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        if (!TextUtils.isEmpty(deleteComment)) {
                            mAdapterCanDeleteComment.loadMoreFail();
                        } else
                            mAdapter.loadMoreFail();
                    }
                }, this

        );
    }

}
