package com.weiju.ccmall.module.xysh.adapter;

import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.PrestoreType;

/**
 * @author chenyanming
 * @time 2019/12/17 on 14:46
 * @desc
 */
public class PopupSelectAdapter extends BaseQuickAdapter<PrestoreType, BaseViewHolder> {
    public PopupSelectAdapter() {
        super(R.layout.item_receivables_status);
    }

    private int mSelectPosition;

    public void setSelectPosition(int selectPosition) {
        mSelectPosition = selectPosition;
        notifyDataSetChanged();
    }

    @Override
    protected void convert(BaseViewHolder helper, PrestoreType item) {
        helper.setText(R.id.tvTitle, item.name);
        TextView textView = helper.getView(R.id.tvTitle);
        textView.setSelected(mSelectPosition == helper.getPosition());
        textView.setTextSize(SizeUtils.px2dp(mContext.getResources().getDimension(
                mSelectPosition == helper.getPosition() ? R.dimen.text_middle : R.dimen.text_size_14sp)));
    }
}
