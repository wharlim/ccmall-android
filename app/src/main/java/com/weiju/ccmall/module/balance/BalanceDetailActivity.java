package com.weiju.ccmall.module.balance;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.DealDetail;
import com.weiju.ccmall.shared.bean.OrderProduct;
import com.weiju.ccmall.shared.bean.PayDetail;
import com.weiju.ccmall.shared.bean.Transfer;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBalanceService;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.ConvertUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Chan on 2017/6/20.
 *
 * @author Chan
 * @package com.weiju.ccmall.module.balance
 * @since 2017/6/20 上午11:07
 */

public class BalanceDetailActivity extends BaseActivity {

    @BindView(R.id.titleTv)
    protected TextView mTitleTv;
    @BindView(R.id.moneyTv)
    protected TextView mMoneyTv;
    @BindView(R.id.timeTitleTv)
    protected TextView mTimeTitleTv;
    @BindView(R.id.timeTv)
    protected TextView mTimeTv;
    @BindView(R.id.typeTitleTv)
    protected TextView mTypeTitleTv;
    @BindView(R.id.typeTv)
    protected TextView mTypeTv;
    @BindView(R.id.transferTitleTv)
    protected TextView mTransferTitleTv;
    @BindView(R.id.transferTv)
    protected TextView mTransferTv;
    @BindView(R.id.remarkTitleTv)
    protected TextView mRemarkTitleTv;
    @BindView(R.id.remarkTv)
    protected TextView mRemarkTv;
    @BindView(R.id.transferLayout)
    protected LinearLayout mTransferLayout;
    @BindView(R.id.remarkLayout)
    protected LinearLayout mRemarkLayout;
    @BindView(R.id.orderDetailLayout)
    protected LinearLayout mOrderDetailLayout;

    private IBalanceService mBalanceService;
    private long mTypeId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance_detail_layout);
        mBalanceService = ServiceManager.getInstance().createService(IBalanceService.class);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        String id = intent.getExtras().getString("did");
        mTypeId = intent.getExtras().getLong("typeId");
        if (mTypeId == 1 || mTypeId == 3) {
            mTransferLayout.setVisibility(View.GONE);
            getPayDetail(id);
        }
        if (mTypeId == 2) {
            getDealDetail(id);
        }
        if (mTypeId == 4 || mTypeId == 5) {
            getTransferDetail(id);
        }
        if (mTypeId == 6) {
            getCashOutDetail(id);
        }

    }

    private void getCashOutDetail(String id) {
        ILiveStoreService mService = ServiceManager.getInstance().createService(ILiveStoreService.class);
        APIManager.startRequest(mService.getCashOutDetail(id), new BaseRequestListener<DealDetail>(this) {
            @Override
            public void onSuccess(DealDetail dealDetail) {
                super.onSuccess(dealDetail);
                mTitleTv.setText(dealDetail.dealStatusStr);
//                if (dealDetail.applyMoney < 0) {
                    mMoneyTv.setText(String.format("-%.2f", ConvertUtil.cent2yuan(dealDetail.applyMoney)));
                    mMoneyTv.setTextColor(getResources().getColor(R.color.green));
                    mRemarkLayout.setVisibility(View.GONE);
//                } else {
//                    mMoneyTv.setText(String.format("+%.2f", ConvertUtil.cent2yuan(dealDetail.applyMoney)));
//                    mMoneyTv.setTextColor(getResources().getColor(R.color.red));
//                    mRemarkLayout.setVisibility(View.GONE);
//                }
                mTimeTitleTv.setText("提现时间");
                mTimeTv.setText(dealDetail.applyDate);
                mTypeTv.setText("提现到支付宝");
                mTransferTitleTv.setText("提现到:");
                mTransferTv.setText(dealDetail.alipayAccount);
            }
        },this);
    }


    @Override
    protected void onStart() {
        super.onStart();
        showHeader();
        setTitle(mTypeId == 6 ? "货款详情" : "余额详情");
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_black);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getPayDetail(final String did) {
        APIManager.startRequest(mBalanceService.getPayDetail(did), new BaseRequestListener<PayDetail>(this) {
            @SuppressLint("DefaultLocale")
            @Override
            public void onSuccess(PayDetail payDetail) {

                if (payDetail.payMoney < 0) {
                    initPayDetailView(payDetail);

                } else {
                    mTitleTv.setText("退款成功");
                    mMoneyTv.setText(String.format("+%.2f", ConvertUtil.cent2yuan(payDetail.payMoney)));
                    mMoneyTv.setTextColor(getResources().getColor(R.color.red));
                    mTimeTitleTv.setText("退款时间");
                    mTypeTv.setText("余额支付退款");

                }

            }
        },this);
    }

    @SuppressLint("DefaultLocale")
    private void initPayDetailView(PayDetail payDetail) {
        mTitleTv.setText("支付成功");
        mMoneyTv.setText(String.format("%.2f", ConvertUtil.cent2yuan(payDetail.payMoney)));
        mMoneyTv.setTextColor(getResources().getColor(R.color.green));
        mTimeTitleTv.setText("支付时间");
        mTypeTv.setText("余额支付");
        mRemarkLayout.setVisibility(View.GONE);
        mOrderDetailLayout.setVisibility(View.VISIBLE);
        LinearLayout orderItemLayout = (LinearLayout) mOrderDetailLayout.findViewById(R.id.orderItemLayout);
        TextView mOrderCodeTv = (TextView) mOrderDetailLayout.findViewById(R.id.orderCodeTv);
        TextView mOrderTimeTv = (TextView) mOrderDetailLayout.findViewById(R.id.orderTimeTv);
        mOrderCodeTv.setText(payDetail.order.orderMain.orderCode);
        mOrderTimeTv.setText(payDetail.order.orderMain.createDate);
        orderItemLayout.removeAllViews();
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        for (OrderProduct product : payDetail.order.products) {
            LinearLayout view = (LinearLayout) layoutInflater.inflate(R.layout.cmp_order_item, orderItemLayout, false);
            TextView mItemProductNameTv = (TextView) view.findViewById(R.id.itemProductNameTv);
            TextView mItemNumTv = (TextView) view.findViewById(R.id.itemNumTv);
            TextView mItemMoneyTv = (TextView) view.findViewById(R.id.itemMoneyTv);
            mItemProductNameTv.setText(product.name);
            mItemNumTv.setText(product.getAmountString());
            mItemMoneyTv.setText(String.format("单价:%s", ConvertUtil.centToCurrency(this, product.price)));
            orderItemLayout.addView(view);
        }

    }

    private void getDealDetail(final String did) {
        APIManager.startRequest(mBalanceService.getDealDetail(did), new BaseRequestListener<DealDetail>(this) {
            @SuppressLint("DefaultLocale")
            @Override
            public void onSuccess(DealDetail dealDetail) {
                mTitleTv.setText(dealDetail.dealStatusStr);
                if (dealDetail.applyMoney < 0) {
                    mMoneyTv.setText(String.format("%.2f", ConvertUtil.cent2yuan(dealDetail.applyMoney)));
                    mMoneyTv.setTextColor(getResources().getColor(R.color.green));
                    mRemarkLayout.setVisibility(View.GONE);
                } else {
                    mMoneyTv.setText(String.format("+%.2f", ConvertUtil.cent2yuan(dealDetail.applyMoney)));
                    mMoneyTv.setTextColor(getResources().getColor(R.color.red));
                    mRemarkLayout.setVisibility(View.GONE);
                }
                mTimeTitleTv.setText("提现时间");
                mTimeTv.setText(dealDetail.applyDate);
                if (dealDetail.account.accountType == 3) {
                    mTypeTv.setText("提现到微信");
                    mTransferTitleTv.setText("微信昵称");
                    mTransferTv.setText(dealDetail.account.wechatUser);
                } else {
                    mTypeTv.setText("提现到银行");
                    mTransferTitleTv.setText("银行");
                    mTransferTv.setText(dealDetail.account.bankName);
                }

            }
        },this);
    }

    private void getTransferDetail(final String did) {
        APIManager.startRequest(mBalanceService.getTransferDetail(did), new BaseRequestListener<Transfer>(this) {
            @SuppressLint("DefaultLocale")
            @Override
            public void onSuccess(Transfer transfer) {
                mTitleTv.setText(transfer.statusStr);
                if (transfer.transferMoney < 0) {
                    mMoneyTv.setText(String.format("%.2f", ConvertUtil.cent2yuan(transfer.transferMoney)));
                    mMoneyTv.setTextColor(getResources().getColor(R.color.green));
                    mTimeTitleTv.setText("转出时间");
                    mTransferTv.setText(String.format("%s(%s)", transfer.inNickName, transfer.inPhone));
                } else {
                    mMoneyTv.setText(String.format("+%.2f", ConvertUtil.cent2yuan(transfer.transferMoney)));
                    mMoneyTv.setTextColor(getResources().getColor(R.color.red));
                    mTimeTitleTv.setText("转入时间");
                    mTransferTv.setText(String.format("%s(%s)", transfer.outNickName, transfer.outPhone));
                }

                mTimeTv.setText(transfer.createDate);
                mTypeTv.setText("转账");
                mTransferTitleTv.setText("对方账户");
                mRemarkTv.setText(transfer.transferMemo);
            }
        },this);
    }
}
