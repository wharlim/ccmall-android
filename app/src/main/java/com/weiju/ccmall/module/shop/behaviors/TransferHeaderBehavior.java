package com.weiju.ccmall.module.shop.behaviors;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import static com.weiju.ccmall.module.shop.views.Utils.dpToPx;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.shop.views.ShopTitleView;
import com.weiju.ccmall.module.shop.views.Utils;
import com.weiju.ccmall.shared.component.HeaderLayout;

public class TransferHeaderBehavior extends CoordinatorLayout.Behavior<View> {

    private Toolbar mToolBar;
    private ArgbEvaluator mArgbEvaluator = new ArgbEvaluator();
    private int startColor = -1;
    private int endColor = -1;

    private String title = "";
    private TextView titleTv;

    private float startTop;
    private float endTop;

    public void setTitle(String title) {
        this.title = title;
    }


    public TransferHeaderBehavior(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean layoutDependsOn(CoordinatorLayout parent, View child, View dependency) {
        return dependency instanceof ImageView;
    }

    @Override
    public boolean onDependentViewChanged(CoordinatorLayout parent, View child, View dependency) {
        if (mToolBar == null) {
            mToolBar = parent.findViewById(R.id.toolBar);
            titleTv = parent.findViewById(R.id.title);
        }
//        Log.d("Seven", "dep " + mToolBar);
        if (mToolBar != null) {
            if (startColor == -1) {
                startColor = child.getResources().getColor(R.color.red);
                endColor = child.getResources().getColor(R.color.gray);
                startTop = Utils.dpToPx(45 + 40);
                endTop = Utils.dpToPx(179 + 40);
            }

            float posOffset = (dependency.getTop() - startTop) / (endTop - startTop);
            int curColor = (int)mArgbEvaluator.evaluate(posOffset, startColor, endColor);
            mToolBar.setBackgroundColor(curColor);
            if (posOffset == 1f) {
                titleTv.setText("");
            } else {
                titleTv.setText(title);
            }
        }
        return true;
    }
}
