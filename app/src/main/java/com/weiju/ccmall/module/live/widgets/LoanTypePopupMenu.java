package com.weiju.ccmall.module.live.widgets;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.util.ConvertUtil;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoanTypePopupMenu extends PopupWindow {
    private OnMenuClickListener mOnMenuClickListener;
    private String[] title = new String[]{"全部类型", "销售收入", "提现"};

    public LoanTypePopupMenu(Context context, OnMenuClickListener onMenuClickListener) {
        super(ConvertUtil.dip2px(120), ViewGroup.LayoutParams.WRAP_CONTENT);
        View contentView = LayoutInflater.from(context).inflate(R.layout.view_funds_type, null);
        ButterKnife.bind(this, contentView);
        setContentView(contentView);
        mOnMenuClickListener = onMenuClickListener;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setElevation(10);
        }
        setOutsideTouchable(true);
        setFocusable(true);
    }

    @OnClick({R.id.tvAll, R.id.tvSales, R.id.tvWithdraw})
    public void onViewClicked(View view) {
        dismiss();
        if (mOnMenuClickListener != null) {
            mOnMenuClickListener.onMenuClick(getMenuIndex(view.getId()), title[getMenuIndex(view.getId())]);
        }
    }

    private int getMenuIndex(int menuId) {//列表数据类型（0：全部；1：销售订单；2：提现记录）
        switch (menuId) {
            case R.id.tvAll:
                return 0;
            case R.id.tvSales:
                return 1;
            case R.id.tvWithdraw:
                return 2;
        }
        return 0;
    }

    public interface OnMenuClickListener {
        void onMenuClick(int menuIndex, String title);
    }
}
