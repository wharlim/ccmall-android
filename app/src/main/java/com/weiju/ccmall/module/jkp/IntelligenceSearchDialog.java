package com.weiju.ccmall.module.jkp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.search.SearchActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.JkpSearchResult;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.page.PageType;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class IntelligenceSearchDialog extends DialogFragment {
    @BindView(R.id.tvContent)
    TextView tvContent;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.tvConfirm)
    TextView tvConfirm;
    Unbinder unbinder;

    private String clipboard;

    private IJkpProductService jkpProductService = ServiceManager.getInstance().createService(IJkpProductService.class);

    public static IntelligenceSearchDialog newInstance(String clipboard) {
        IntelligenceSearchDialog dialog = new IntelligenceSearchDialog();
        Bundle args = new Bundle();
        args.putString("clipboard", clipboard);
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_intelligence_search, container, false);
        clipboard = getArguments().getString("clipboard");
        unbinder = ButterKnife.bind(this, view);
        tvContent.setText(clipboard);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.tvCancel, R.id.tvConfirm})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvCancel:
                dismiss();
                break;
            case R.id.tvConfirm:
                startSearch();
                dismiss();
                break;
        }
    }

    private void startSearch() {
        ToastUtil.showLoading(getContext());
        final Context context = getContext();
        APIManager.startRequest(jkpProductService.search(clipboard, 1, 15, ""), new BaseRequestListener<JkpSearchResult>() {
            @Override
            public void onSuccess(JkpSearchResult result) {
                ToastUtil.hideLoading();
                if ("1".equals(result.searchType)) {
                    // 搜索列表
                    Intent intent = new Intent(context, SearchActivity.class);
                    intent.putExtra("pageType", PageType.JKP.name());
                    intent.putExtra("keyword", clipboard);
                    context.startActivity(intent);
                } else if ("2".equals(result.searchType)) {
                    // 详情
                    if (result.data != null) {
                        result.data.parseCouponAmountFromCouponInfo();
                        JiKaoPuProductDetailActivity.start(context, result.data.toSkuInfo());
                    }
                } else {
                    ToastUtil.error("搜索不到该商品");
                }
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.hideLoading();
            }
        }, getContext());
    }
}
