package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/4/28.
 */
public class Translation {

    /**
     * moveTag : false
     * startVip : 0
     * startVipStr : 普通用户
     * endVip :
     * endVipStr : 大众会员
     */

    @SerializedName("moveTag")
    public boolean moveTag;  //false 不可升级，true 可升级
    @SerializedName("startVip")
    public int startVip;
    @SerializedName("startVipStr")
    public String startVipStr;
    @SerializedName("endVip")
    public String endVip;
    @SerializedName("endVipStr")
    public String endVipStr;
}
