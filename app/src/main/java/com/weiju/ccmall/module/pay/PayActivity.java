package com.weiju.ccmall.module.pay;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;
import com.suke.widget.SwitchButton;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.address.AddressListActivity;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.blockchain.utils.BlockChainUtil;
import com.weiju.ccmall.module.coupon.CouponListActivity;
import com.weiju.ccmall.module.pay.adapter.PayAdapter;
import com.weiju.ccmall.module.pay.fragment.PostageDetailsFragment;
import com.weiju.ccmall.module.pickUp.activity.PickUpDetailListActivity;
import com.weiju.ccmall.module.user.InvitaionUserDialog;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.bean.UpMemberInfoBean;
import com.weiju.ccmall.shared.basic.AgentFActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Address;
import com.weiju.ccmall.shared.bean.CartItem;
import com.weiju.ccmall.shared.bean.CartStore;
import com.weiju.ccmall.shared.bean.Coupon;
import com.weiju.ccmall.shared.bean.Freight;
import com.weiju.ccmall.shared.bean.MemberRatio;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.OrderResponse;
import com.weiju.ccmall.shared.bean.OrderVouchers;
import com.weiju.ccmall.shared.bean.PickUp;
import com.weiju.ccmall.shared.bean.RegionCity;
import com.weiju.ccmall.shared.bean.RegionDistinct;
import com.weiju.ccmall.shared.bean.RegionProvince;
import com.weiju.ccmall.shared.bean.ScoreStat;
import com.weiju.ccmall.shared.bean.SkuAmount;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.Vouchers;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.body.CalcOrderCouponListBody;
import com.weiju.ccmall.shared.bean.body.SaveIdentityCardBody;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.NumRangeInputFilter;
import com.weiju.ccmall.shared.component.PayTypeLayout;
import com.weiju.ccmall.shared.component.dialog.RegionSelectDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Action;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.contracts.IRegion;
import com.weiju.ccmall.shared.contracts.OnSelectRegionLister;
import com.weiju.ccmall.shared.decoration.SpacesItemDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.CartManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.IAddressService;
import com.weiju.ccmall.shared.service.contract.ICartService;
import com.weiju.ccmall.shared.service.contract.ICouponService;
import com.weiju.ccmall.shared.service.contract.IFreightService;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.service.contract.IRegionService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.AliPayUtils;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.PayUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.WePayUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.pay
 * @since 2017-06-09
 */
public class PayActivity extends BaseActivity {

    @BindView(R.id.addressArrowIv)
    ImageView mAddressArrowIv;
    @BindView(R.id.addressLayout)
    LinearLayout mAddressLayout;
    @BindView(R.id.couponLayout)
    LinearLayout mCouponLayout;
    @BindView(R.id.tvScoreTips)
    TextView mTvScoreTips;
    @BindView(R.id.switchScore)
    SwitchButton mSwitchScore;
    @BindView(R.id.etScore)
    EditText mEtScore;
    @BindView(R.id.tvScoreMoney)
    TextView mTvScoreMoney;
    @BindView(R.id.layoutScore)
    LinearLayout mLayoutScore;
    @BindView(R.id.addressInfoLayout)
    protected RelativeLayout mAddressInfoLayout;
    @BindView(R.id.selectAddressTv)
    protected TextView mSelectAddressTv;
    @BindView(R.id.contactsTv)
    protected TextView mContactsTv;
    @BindView(R.id.phoneTv)
    protected TextView mPhoneTv;
    @BindView(R.id.addressDetailTv)
    protected TextView mDetailTv;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.remarkEt)
    protected EditText mRemarkEt;
    @BindView(R.id.couponTv)
    protected TextView mCouponTv;
    @BindView(R.id.freightTv)
    protected TextView mFreightTv;
    @BindView(R.id.totalTv)
    protected TextView mTotalTv;
    @BindView(R.id.confirmBtn)
    protected TextView mConfirmBtn;
    @BindView(R.id.tvRatio)
    TextView mTvRatio;
    @BindView(R.id.tvRatioPrice)
    TextView mTvRatioPrice;
    @BindView(R.id.layoutRatio)
    LinearLayout mLayoutRatio;
    @BindView(R.id.tvVipUpdate)
    TextView mTvVipUpdate;
    @BindView(R.id.layoutVipUpdate)
    LinearLayout mLayoutVipUpdate;
    @BindView(R.id.etIdCard)
    EditText mEtIdCard;
    @BindView(R.id.ivIdCardClean)
    ImageView mIvIdCardClean;
    @BindView(R.id.tvCardSave)
    TextView mTvCardSave;
    @BindView(R.id.layoutIdCard)
    FrameLayout mLayoutIdCard;
    @BindView(R.id.ivVipUpdate)
    ImageView mIvVipUpdate;
    @BindView(R.id.pbIdCart)
    ProgressBar mPbIdCart;
    @BindView(R.id.layoutIdCardEdit)
    LinearLayout mLayoutIdCardEdit;
    @BindView(R.id.tvIdCard)
    TextView mTvIdCard;
    @BindView(R.id.ivIdCardEdit)
    ImageView mIvIdCardEdit;
    @BindView(R.id.layoutIdCardRead)
    LinearLayout mLayoutIdCardRead;
    @BindView(R.id.layoutScoreClick)
    LinearLayout mLayoutScoreClick;
    @BindView(R.id.layoutPayType)
    PayTypeLayout mLayoutPayType;
    @BindView(R.id.tvCc)
    TextView mTvCc;
    @BindView(R.id.tvCticket)
    TextView mTvCticket;
    @BindView(R.id.tvGoldenTicket)
    TextView mTvGoldenTicket;
    @BindView(R.id.lyPayType)
    LinearLayout mLyPayType;
    @BindView(R.id.postageDet)
    View postageDet;
    @BindView(R.id.pickUpLayout)
    LinearLayout mPickUpLayout;
    @BindView(R.id.pickUpTv)
    TextView mTvPickUp;

    @BindView(R.id.ll_new_retail)
    LinearLayout ll_new_retail;
    @BindView(R.id.tv_shipping_address)
    TextView tv_shipping_address;
    @BindView(R.id.ll_area)
    LinearLayout ll_area;
    @BindView(R.id.tv_area)
    TextView tv_area;

    @BindView(R.id.et_phone)
    EditText et_phone;

    @BindView(R.id.et_contact)
    EditText et_contact;

    @BindView(R.id.et_address)
    EditText et_address;

    @BindView(R.id.et_auto_address)
    EditText et_auto_address;

    @BindView(R.id.deductTv)
    TextView deductTv;
    @BindView(R.id.deductLayout)
    LinearLayout deductLayout;

    private ICartService mCartService;
    private IProductService mProductService;
    private IAddressService mAddressService;
    private IOrderService mOrderService;
    private IFreightService mFreightService;
    private Address mAddress;
    private PayAdapter mPayAdapter;

    SkuInfo skuInfo;

    User user;

    private String liveId;

    private int orderForm = 1;


    /**
     * 邮费
     */
    private long mFreight = 0L;
    /**
     * 当前使用的优惠券
     */
    private Coupon mUseCoupon;
    /**
     * 可用优惠券
     */
    private Coupon mAvaCoupon;
    private String mOrderCode;
    private ScoreStat mScoreStat;
    /**
     * 用户要使用的积分
     */
    private long mUseScore;
    //    private CartManager.OrderRadio mOrderRadio;
    private ArrayList<MemberRatio> mUpdateMemberRatios = new ArrayList<>();
    private IUserService mUserService;
    //    private int mIndex;
    private String mFrom;
    private String mGroupCode;
    private SkuInfo mSkuInfo;
    private long mTotalPrice;
    private int productType;

    private User mUser;
    private InvitaionUserDialog mInvitaionUserDialog;
    private String mInvitaionPhone;
    private boolean isBinding;
    private boolean mPayByMoney;
    private String orderType = "";

    private CalcOrderCouponListBody mCalcOrderCouponListBody;
    private ArrayList<SkuAmount> mCalcOrderListBody;
    private ArrayList<PickUp> mPickUpList;
    private long mTotalCost;

    public static final int SELECT_VOUCHER = 1;
    public static final int NO_SELECT_VOUCHER = 0;
    //默认选择使用提货券
    private int mSelectVoucher = SELECT_VOUCHER;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay);
        ButterKnife.bind(this);

        showHeader();
        setTitle("结算付款");
        setLeftBlack();
//        if (1 == orderForm)
//            mLayoutPayType.freeOrderPayMethod(true);
        liveId = getIntent().getStringExtra("liveId");

        orderType = getSharedPreferences(Const.ORDER_TYPE, 0).getString(Const.ORDER_TYPE, "");
        String passOrderType = getIntent().getStringExtra(Const.ORDER_TYPE);
        if (!TextUtils.isEmpty(passOrderType)) {
            orderType = passOrderType;
        }

//        if (!Config.IS_DISCOUNT) {
//            mLayoutRatio.setVisibility(View.GONE);
//            mLayoutScoreClick.setVisibility(View.GONE);
//        }

        WePayUtils.initWePay(this);

        mCartService = ServiceManager.getInstance().createService(ICartService.class);
        mProductService = ServiceManager.getInstance().createService(IProductService.class);
        mAddressService = ServiceManager.getInstance().createService(IAddressService.class);
        mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
        mFreightService = ServiceManager.getInstance().createService(IFreightService.class);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);

        mPayAdapter = new PayAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(ConvertUtil.dip2px(10)));
        mRecyclerView.setAdapter(mPayAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);

//        selectPayWay(0);
        checkParamsAndLoadData();
        loadDefaultAddress();
        loadUserInfo();

        et_auto_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String string = editable.toString();
                analysisAddress(string);
            }
        });

    }


    /**
     * 获取积分相关数据
     */
    private void loadScoreData() {
        final IUserService service = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(service.getMemberRatio(), new BaseRequestListener<List<MemberRatio>>(this) {
            @Override
            public void onSuccess(List<MemberRatio> memberRatio) {
                mUpdateMemberRatios.clear();
                mUpdateMemberRatios.addAll(memberRatio);
                APIManager.startRequest(service.getScoreStat(), new BaseRequestListener<ScoreStat>(PayActivity.this) {
                    @Override
                    public void onSuccess(ScoreStat result) {
                        mScoreStat = result;
                        initRadioView();
                        initScoreView();
                        initTotalView();
                    }
                }, PayActivity.this);
            }
        }, this);

    }

    private void loadUserInfo() {
        APIManager.startRequest(mUserService.getUserInfo(), new BaseRequestListener<User>(this) {
            @Override
            public void onSuccess(User user) {
                SessionUtil.getInstance().setLoginUser(user);
            }
        }, this);
    }

    private void initRadioView() {
        long orderRadioMoney = CartManager.getOrderRadioMoney(mPayAdapter.getItems(), mUpdateMemberRatios, true, isGroupBuy());
        if (orderRadioMoney > 0 && !isGroupBuy()) {
            mTvRatioPrice.setText("-¥" + ConvertUtil.cent2yuanNoZero(orderRadioMoney));
            mTvRatioPrice.setTextColor(getResources().getColor(R.color.red));
        } else {
            mTvRatioPrice.setText("无折扣");
            mTvRatioPrice.setTextColor(getResources().getColor(R.color.text_gray));
        }
    }

    private void initScoreView() {
        if (!Config.IS_DISCOUNT) {
            mLayoutScoreClick.setVisibility(View.GONE);
            return;
        }
        if (isGroupBuy()) {
            mSwitchScore.setEnabled(false);
            mSwitchScore.setFocusable(false);
            mTvScoreTips.setText(" 团购产品不可使用积分");
            mLayoutScoreClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            return;
        }
        long scoreTotalMoney = CartManager.getScoreTotalMoney(mPayAdapter.getItems(), mUpdateMemberRatios);
        if (mUseCoupon != null) {
            scoreTotalMoney -= mUseCoupon.cost;
        }
        if (scoreTotalMoney < mScoreStat.scoreUseCofig.minOrderMoney) {
            mEtScore.setText("0");
            mTvScoreTips.setText("最多可用0积分");
            mLayoutScoreClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ToastUtil.error("当前订单无法使用积分");
                }
            });
            mSwitchScore.setEnabled(false);
        } else {
            final long maxScore;//最大可用积分
            long ratioScore = scoreTotalMoney * (mScoreStat.scoreUseCofig.ratio) / 100 / 10;
            long userMaxScore = mScoreStat.memberScore.totalScore;
            if (ratioScore < userMaxScore) {
                maxScore = ratioScore;
            } else {
                maxScore = userMaxScore;
            }
            mTvScoreTips.setText(String.format("最多可用%s积分", maxScore));
            mSwitchScore.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                    mLayoutScore.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                }
            });
            mEtScore.setLongClickable(false);
            mEtScore.setFilters(new InputFilter[]{new NumRangeInputFilter(maxScore + 1)});
            mEtScore.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().isEmpty()) {
                        long aLong = Long.valueOf(s.toString());
                        mUseScore = aLong;
                    } else {
                        mUseScore = 0;
                    }
                    mTvScoreMoney.setText(String.format("抵¥%.2f", mUseScore * 1.0f / 10));
                    initTotalView();
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        getSharedPreferences("pushId", 0).edit().putInt("pushId", 0).commit();
    }

    private void checkParamsAndLoadData() {
        if (getIntent() == null || getIntent().getExtras() == null) {
            ToastUtil.error("请选择产品");
            getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
            finish();
        }

        Bundle extras = getIntent().getExtras();

        skuInfo = (SkuInfo) extras.getSerializable("skuInfo");
        user = (User) extras.getSerializable("user");
        if (user != null) {
            tv_shipping_address.setText(user.nickname + "收货地址");
        }

        if (skuInfo != null)
            productType = skuInfo.productType;
        if (18 == productType) {
            mAddressLayout.setVisibility(View.VISIBLE);
            ll_new_retail.setVisibility(View.GONE);
//            mLayoutPayType.setVisibility(View.GONE);

//            mLyPayType.setVisibility(View.GONE);
        }
        mFrom = extras.getString("from", "");
        mGroupCode = extras.getString("groupCode");
        mLayoutPayType.setProductType(productType);
        try {
            switch (mFrom) {
                case "cart": // 购物车结算下单
                    ArrayList<String> list = extras.getStringArrayList("skuIds");
                    if (list == null) {
                        throw new Exception();
                    }
                    loadCartListBySkuIds(list);
                    break;
                case "buy": // 立刻购买下单
                case AppTypes.GROUP_BUY.FORM_CREATE_GROUP://创建团购
                case AppTypes.GROUP_BUY.FORM_JOIN_GROUP://加入团购
                    mPayByMoney = extras.getBoolean("payByMoney");
                    mLyPayType.setVisibility(mPayByMoney || mFreight > 0 ? View.VISIBLE : View.GONE);

                    SkuAmount skuAmount = (SkuAmount) extras.getSerializable("skuAmount");
                    if (skuAmount == null) {
                        throw new Exception();
                    }
                    loadCartListBySkuAmount(skuAmount);
                    break;
                default: //
                    throw new Exception();
            }
        } catch (Exception e) {
            ToastUtil.error("请选择产品");
            finish();
        }
    }


    private void loadCartListBySkuIds(ArrayList<String> list) {
        APIManager.startRequest(mCartService.getListBySkuIds(Joiner.on(",").join(list)), new BaseRequestListener<List<CartStore>>(this) {
            @Override
            public void onSuccess(List<CartStore> result) {
                mPayAdapter.setItems(result);
                Logger.e("从购物车进来，当前列表：" + mPayAdapter.getItems().size());
                initAddressView();
                loadFreight();
                initTotalView();
                initCCM();
                initGoldenTicket();
                loadScoreData();

                ArrayList<SkuAmount> skuAmounts = new ArrayList<>();
                for (CartStore cartStore : result) {
                    for (CartItem product : cartStore.products) {
                        skuAmounts.add(new SkuAmount(product.skuId, product.amount));
                    }
                }
                initCouponData(skuAmounts);
                loadCalcOrderVouchersList(skuAmounts);

                long t = 0;
                for (CartStore cs : result) {
                    List<CartItem> products = cs.products;
                    if (products != null) {
                        for (CartItem p : products) {
                            t += p.shippingPrice * p.amount;
                        }
                    }
                }
                if (t > 0) {
                    final long ll = t;
                    postageDet.setVisibility(View.VISIBLE);
                    postageDet.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Bundle bundle = new Bundle();
                            bundle.putLong("mFreight", ll);
                            AgentFActivity.start2(PayActivity.this, PostageDetailsFragment.class, bundle);
                        }
                    });
                }
            }
        }, this);
    }

    /**
     * 提货券
     *
     * @param datas
     */
    private void loadCalcOrderVouchersList(ArrayList<SkuAmount> datas) {
//        CalcOrderCouponListBody calcOrderCouponListBody = new CalcOrderCouponListBody();
//        calcOrderCouponListBody.products = datas;
//        mCalcOrderCouponListBody = calcOrderCouponListBody;
        mCalcOrderListBody = datas;
        APIManager.startRequest(
                mOrderService.calcOrderVouchersList(datas),
                new BaseRequestListener<OrderVouchers>(this) {
                    @Override
                    public void onSuccess(OrderVouchers result) {
                        mTotalCost = result.totalBean.totalCost;
                        mPickUpList = result.list;
                        initTotalView();
                        initCCM();
                        mPickUpLayout.setVisibility(result.list.size() > 0 ? View.VISIBLE : View.GONE);
                        mTvPickUp.setText(String.format("-%s", MoneyUtil.centToYuan¥StrNoZero(mTotalCost)));
                    }
                }, this
        );


    }

    private void loadCartListBySkuAmount(final SkuAmount skuAmount) {
        APIManager.startRequest(mProductService.getSkuById(skuAmount.skuId), new BaseRequestListener<SkuInfo>(this) {
            @Override
            public void onSuccess(SkuInfo skuInfo) {
                mSkuInfo = skuInfo;
                CartStore cartStore = new CartStore();
                cartStore.id = skuInfo.storeId;
                cartStore.name = skuInfo.storeName;

                Gson gson = new Gson();
                CartItem cartItem = gson.fromJson(gson.toJson(skuInfo), CartItem.class);
                cartItem.amount = skuAmount.amount;
                cartStore.products.add(cartItem);
                cartItem.amount = skuAmount.amount;
                ArrayList<CartStore> cartStores = new ArrayList<>();
                cartStores.add(cartStore);
                mPayAdapter.setItems(cartStores);
                Logger.e("从立即购买进来，当前列表：" + mPayAdapter.getItems().size());

                if (isGroupBuy()) {
                    cartItem.tempPrice = cartItem.retailPrice;
                    cartItem.retailPrice = skuInfo.groupSkuInfo.groupPrice;
                    cartItem.marketPrice = skuInfo.groupSkuInfo.groupPrice;
                }

                initAddressView();
                loadFreight();
                initCCM();
                initGoldenTicket();
                loadScoreData();
                ArrayList<SkuAmount> skuAmounts = new ArrayList<>();
                skuAmounts.add(skuAmount);
                loadCalcOrderVouchersList(skuAmounts);
                totalDeduct = mSkuInfo.exchangeMoney * skuAmount.amount;
                initDeductData(mSkuInfo.consumeCcm * skuAmount.amount, totalDeduct, deductLayout, deductTv);
                initTotalView();
                if (isGroupBuy()) {
                    setCouponView();
                } else {
                    initCouponData(skuAmounts);
                }
            }
        }, this);
    }

    // CCM总抵扣金额
    private long totalDeduct;

    /**
     * 初始化抵扣数据
     */
    public static void initDeductData(long consumeCcm, long exchangeMoney, LinearLayout deductLayout, TextView deductTv) {
        if (consumeCcm == 0 && exchangeMoney == 0) {
            deductLayout.setVisibility(View.GONE);
        } else {
            deductLayout.setVisibility(View.VISIBLE);
            deductTv.setText(String.format(Locale.getDefault(), "%sCCM抵%s元",
                    MoneyUtil.coinToYuanStrNoZero(consumeCcm),
                    MoneyUtil.centToYuan(exchangeMoney)));
        }
    }

    private void initCouponData(List<SkuAmount> datas) {
        if (isGroupBuy()) {
            setCouponView();
            return;
        }
        CalcOrderCouponListBody calcOrderCouponListBody = new CalcOrderCouponListBody();
        calcOrderCouponListBody.products = datas;
        ICouponService service = ServiceManager.getInstance().createService(ICouponService.class);
        APIManager.startRequest(
                service.automaticChooseCoupon(calcOrderCouponListBody),
                new BaseRequestListener<Coupon>(this) {
                    @Override
                    public void onSuccess(Coupon result) {
                        if (result != null && !StringUtils.isEmpty(result.couponId)) {
                            mUseCoupon = result;
                            mAvaCoupon = mUseCoupon;
                            initTotalView();
                        }
                        setCouponView();
                    }
                }, this
        );
    }

    private void loadDefaultAddress() {
        APIManager.startRequest(mAddressService.getDefaultAddress(), new BaseRequestListener<Address>(this) {
            @Override
            public void onSuccess(Address result) {
                if (result != null) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mRecyclerView.getLayoutParams();
                    layoutParams.setMargins(0, 0, 0, 0);
                    mRecyclerView.setLayoutParams(layoutParams);
                }
                mAddress = result;
                loadFreight();
                initAddressView();
            }

            @Override
            public void onError(Throwable e) {

            }
        }, this);
    }

    private void loadFreight() {
        if (mAddress == null || mPayAdapter.getItemCount() == 0) {
            return;
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("addressId", mAddress.addressId);
        params.put("products", convertProducts());
        params.put("isGroup", isGroupBuy() ? 1 : 0);

        APIManager.startRequest(mFreightService.calculate(APIManager.buildJsonBody(params)), new BaseRequestListener<Freight>(this) {
            @Override
            public void onSuccess(Freight result) {
                mFreight = result.freight;
                if (mFreight > 0) {
                    mFreightTv.setText(ConvertUtil.centToCurrency(PayActivity.this, mFreight));
                    mFreightTv.setTextColor(getResources().getColor(R.color.red));
                } else {
                    mFreightTv.setText("包邮");
                    mFreightTv.setTextColor(getResources().getColor(R.color.text_black));
                }
                if (mSkuInfo != null && mSkuInfo.shippingPrice > 0) {
                    postageDet.setVisibility(View.VISIBLE);
                    postageDet.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //TODO
                            Bundle bundle = new Bundle();
                            bundle.putLong("mFreight", mSkuInfo.shippingPrice);
                            AgentFActivity.start2(PayActivity.this, PostageDetailsFragment.class, bundle);
                        }
                    });
                }
                initTotalView();
            }
        }, this);
    }

    private List<SkuAmount> convertProducts() {
        List<SkuAmount> products = new ArrayList<>();
        for (CartStore cartStore : mPayAdapter.getItems()) {
            if (cartStore.products == null) {
                continue;
            }
            for (CartItem product : cartStore.products) {
                SkuAmount skuAmount = new SkuAmount(product.skuId, product.amount);
                products.add(skuAmount);
                if (!TextUtils.isEmpty(liveId)) {
                    skuAmount.liveId = liveId;
                } else if (!TextUtils.isEmpty(product.liveId)) {
                    skuAmount.liveId = product.liveId;
                }
            }
        }
        return products;
    }

    private List<Vouchers> getVouchers() {
        List<Vouchers> vouchers = new ArrayList<>();
        if (mSelectVoucher == NO_SELECT_VOUCHER) {
            return vouchers;
        }
        for (PickUp pickUp : mPickUpList) {
            vouchers.add(new Vouchers(pickUp.goodsId));
        }
        return vouchers;
    }

    private int getProductType() {
        if (mSkuInfo != null) {
            return mSkuInfo.productType;
        }

        int productType = 0;
        for (CartStore cartStore : mPayAdapter.getItems()) {
            if (cartStore.products == null) {
                continue;
            }
            for (CartItem product : cartStore.products) {
                return product.productType;
            }
        }
        return productType;

    }

    private void initAddressView() {
        if (mAddress == null) {
            mSelectAddressTv.setVisibility(View.VISIBLE);
            mAddressInfoLayout.setVisibility(View.GONE);
        } else {
            mSelectAddressTv.setVisibility(View.GONE);
            mAddressInfoLayout.setVisibility(View.VISIBLE);

            mContactsTv.setText(String.format("收货人：%s", mAddress.contacts));
            mPhoneTv.setText(mAddress.phone);
            mDetailTv.setText("收货地址：" + mAddress.getFullAddress());

            initIdCardView();
        }
    }

    private void initIdCardView() {
        if (getGoodsIsKuajing()) {
            mLayoutIdCard.setVisibility(View.VISIBLE);
            if (StringUtils.isEmpty(mAddress.identityCard)) {
                mLayoutIdCardEdit.setVisibility(View.VISIBLE);
                mLayoutIdCardRead.setVisibility(View.GONE);
            } else {
                mLayoutIdCardRead.setVisibility(View.VISIBLE);
                mLayoutIdCardEdit.setVisibility(View.GONE);

                mTvIdCard.setText(ConvertUtil.idCard2xing(mAddress.identityCard));
            }
            mEtIdCard.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    mIvIdCardClean.setVisibility(s.length() > 0 ? View.VISIBLE : View.GONE);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        } else {
            mLayoutIdCard.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.ll_area)
    public void showArea(View view) {
        new RegionSelectDialog(this, new OnSelectRegionLister() {
            @Override
            public void selected(HashMap<String, IRegion> regions) {
                IRegion province = regions.get("province");
                if (province != null) {
                    mAddress.provinceId = province.getId();
                    mAddress.provinceName = province.getName();
                }
                IRegion city = regions.get("city");
                if (city != null) {
                    mAddress.cityId = city.getId();
                    mAddress.cityName = city.getName();
                }
                IRegion distinct = regions.get("distinct");
                if (distinct != null) {
                    mAddress.districtId = distinct.getId();
                    mAddress.districtName = distinct.getName();
                }
                tv_area.setText(mAddress.getFullRegion());
            }
        }).show();
    }


    /**
     * @return 是不是团购
     */
    private boolean isGroupBuy() {
        return (AppTypes.GROUP_BUY.FORM_CREATE_GROUP.equals(mFrom) || AppTypes.GROUP_BUY.FORM_JOIN_GROUP.equals(mFrom)) && mSkuInfo.extType == 2;
    }

    /**
     * @return 是否存在海外购
     */
    private boolean getGoodsIsKuajing() {
        for (CartStore cartStore : mPayAdapter.getItems()) {
            for (CartItem product : cartStore.products) {
                if (product.isCross == 1) {
                    Logger.e("存在海外购商品");
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * 计算最后的总价，并且设置到合计
     */
    private void initTotalView() {
        mTotalPrice = 0L;
        mTotalPrice += getCartStoreTotal();
        mTotalPrice -= mUseScore * 10;
        if (mUseCoupon != null) {
            mTotalPrice -= mUseCoupon.cost;
            if (mTotalPrice < 0) {
                mTotalPrice = 0;
            }
        }

        mLayoutPayType.initData((long) (mTotalPrice * 0.05), mTotalCost, null != mPickUpList && mPickUpList.size() > 0 && mSelectVoucher == SELECT_VOUCHER, mTotalPrice + mFreight - mTotalCost);

        mTotalPrice += mFreight;
        mTotalPrice -= mSelectVoucher == SELECT_VOUCHER ? mTotalCost : 0;
        mTotalPrice -= totalDeduct;
        mTotalTv.setText(ConvertUtil.centToCurrency(this, mPayByMoney || mTotalPrice > 0 ? mTotalPrice : mFreight));
        /*if (productType == 18)
            mLyPayType.setVisibility(View.GONE);
        else*/
        mLyPayType.setVisibility(mPayByMoney || mFreight > 0 || mTotalPrice > 0 ? View.VISIBLE : View.GONE);

        initUpdateVipView();
    }

    /**
     * 设置是否显示 vip 升级的 view 之类的
     */
    private void initUpdateVipView() {
        mLayoutVipUpdate.setVisibility(View.GONE);
        if (mScoreStat == null || !Config.IS_DISCOUNT) {
            return;
        }
        User loginUser = SessionUtil.getInstance().getLoginUser();
        //如果已经是高级会员，则不显示
        if (loginUser.vipType != AppTypes.FAMILY.MEMBER_NORMAL) {
            return;
        }
        //如果有打折也不显示
        long orderRadioMoney = CartManager.getOrderRadioMoney(mPayAdapter.getItems(), mScoreStat.memberRatio, true, isGroupBuy());
        if (orderRadioMoney > 0) {
            return;
        }

        long vipUpdatePrice = getVipUpdatePrice();
        long joinPrice = mScoreStat.getZunXiangPrice();
        long money = joinPrice - vipUpdatePrice;
        if (money > 0) {
            mLayoutVipUpdate.setVisibility(View.VISIBLE);
            mTvVipUpdate.setText(String.format("再买 %s，可升级为尊享会员", ConvertUtil.centToCurrency(this, money)));
        }
    }

    /**
     * 获取一个判断 vip 升级的价格
     * 所有商品的普通价格的和
     * 减掉积分
     * 减掉优惠券
     *
     * @return
     */
    private long getVipUpdatePrice() {
        long vipUpdatePrice = 0;
        for (CartStore cartStore : mPayAdapter.getItems()) {
            for (CartItem product : cartStore.products) {
                if (isGroupBuy()) {
                    vipUpdatePrice += product.tempPrice * product.amount;
                } else {
                    vipUpdatePrice += product.retailPrice * product.amount;
                }
            }
        }
        vipUpdatePrice -= mUseScore * 10;
        if (mUseCoupon != null) {
            vipUpdatePrice -= mUseCoupon.cost;
        }
        return vipUpdatePrice;
    }

    /**
     * 获取购物车商品的总价格（折扣后的价格和，不包含积分减免和优惠券减免）
     *
     * @return
     */
    private long getCartStoreTotal() {
        long total = 0L;
        for (CartStore cartStore : mPayAdapter.getItems()) {
            total += cartStore.getTotal();
        }
        long radioMoney = CartManager.getOrderRadioMoney(mPayAdapter.getItems(), mUpdateMemberRatios, true, isGroupBuy());
        total -= radioMoney;
        return total;
    }

    private void initCCM() {
        double countRate = 0;
        long ticket = 0;
        for (CartStore cartStore : mPayAdapter.getItems()) {
            if (cartStore.products == null) {
                continue;
            }
            for (CartItem cartItem : cartStore.products) {
                countRate += cartItem.countRateExc() * cartItem.amount;
                ticket += cartItem.ticket * cartItem.amount;
            }
        }
        if (countRate > 100) {
            countRate = 100;
        }
        mTvCc.setText(Html.fromHtml(String.format("预计可返<font color =\"#f51861\">%s%%</font>算率",
                BlockChainUtil.formatCCMCoin(mTotalCost > 0 ? 0 : countRate))));
        // 隐藏C券
//        mTvCticket.setText("- " + MoneyUtil.centToYuanStrNoZero(ticket));
    }

    private void initGoldenTicket() {
        long goldenTicket = 0;
        for (CartStore cartStore : mPayAdapter.getItems()) {
            if (cartStore.products == null) {
                continue;
            }
            for (CartItem cartItem : cartStore.products) {
                if (cartItem.productType == 11) {
                    goldenTicket += cartItem.goldenTicket * cartItem.amount;
                }
            }
        }
        mTvGoldenTicket.setText("- " + MoneyUtil.centToYuanStrNoZero(goldenTicket));
    }

    private void setCouponView() {
        if (mUseCoupon != null) {
            mCouponTv.setSelected(false);
            mCouponTv.setText("-" + MoneyUtil.centToYuan¥Str(mUseCoupon.cost));
        } else if (mAvaCoupon != null && !(mSkuInfo != null && mSkuInfo.productType != 9)) {
            mCouponTv.setSelected(false);
            mCouponTv.setText("有可用优惠券");
        } else {
            mCouponTv.setSelected(true);
            mCouponTv.setText("无可用优惠券");

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case Action.SELECT_ADDRESS:
                LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) mRecyclerView.getLayoutParams();
                layoutParams.setMargins(0, 0, 0, 0);
                mRecyclerView.setLayoutParams(layoutParams);

                Address address = (Address) data.getExtras().get("address");
                if (address != null) {
                    mAddress = address;
                    mEtIdCard.setText("");
                    initAddressView();
                    loadFreight();
                }
                break;
            case Action.SELECT_COUPON:
                mUseCoupon = (Coupon) data.getExtras().get("coupon");
                setCouponView();

                initScoreView();
                initTotalView();
                break;
            case Action.SELECT_VOUCHER:
                //选择提货券
                mSelectVoucher = (int) data.getExtras().get("select");
                mTvPickUp.setText(mSelectVoucher == SELECT_VOUCHER ?
                        String.format("-%s", MoneyUtil.centToYuan¥StrNoZero(mTotalCost)) : "不使用提货券");
                mTvPickUp.setTextColor(getResources().getColor(mSelectVoucher == SELECT_VOUCHER ? R.color.red : R.color.text_black));
                initTotalView();
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.addressLayout)
    protected void selectAddress() {
        Intent intent = new Intent(PayActivity.this, AddressListActivity.class);
        intent.putExtra("action", Key.SELECT_ADDRESS);
        startActivityForResult(intent, Action.SELECT_ADDRESS);
    }

    @OnClick(R.id.couponLayout)
    protected void selectCoupon() {
        if (isGroupBuy()) {
            ToastUtil.error("团购产品不可用优惠券");
            return;
        }

        if (mSkuInfo != null && mSkuInfo.productType == 9) {
            ToastUtil.error("无可使用优惠券");
            return;
        }
        Intent intent = new Intent(PayActivity.this, CouponListActivity.class);
        intent.putExtra("action", Key.SELECT_COUPON);
        if (mUseCoupon != null) {
            intent.putExtra("coupon", mUseCoupon);
        }
        HashMap<String, Integer> params = new HashMap<>();
        for (CartStore cartStore : mPayAdapter.getItems()) {
            for (CartItem product : cartStore.products) {
                params.put(product.skuId, product.amount);
            }
        }
        intent.putExtra("products", params);
        startActivityForResult(intent, Action.SELECT_COUPON);
    }


    /**
     * 当前需要多少金券
     *
     * @return
     */
    public long getGoldenTicket() {
        long goldenTicket = 0;
        for (CartStore cartStore : mPayAdapter.getItems()) {
            if (cartStore.products == null) {
                continue;
            }
            for (CartItem cartItem : cartStore.products) {
                if (cartItem.productType == 11) {
                    goldenTicket += cartItem.goldenTicket * cartItem.amount;
                }
            }
        }
        return goldenTicket;
    }

    /**
     * 当前是否需要金券
     *
     * @return
     */
    public boolean hasGoldenTicket() {
        for (CartStore cartStore : mPayAdapter.getItems()) {
            if (cartStore.products == null) {
                continue;
            }
            for (CartItem cartItem : cartStore.products) {
                if (cartItem.productType == 11) {
                    return true;
                }
            }
        }
        return false;
    }


    /*protected void addOrder() {
        if (orderType.equals(Const.ORDER_TYPE_OBLIGED) || orderType.equals(Const.ORDER_TYPE_FREE_ORDER)) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("addressId", mAddress.addressId);
            params.put("products", convertProducts());
            params.put("orderFrom", 1);
            params.put("couponId", mUseCoupon == null ? "" : mUseCoupon.couponId);
            params.put("remark", mRemarkEt.getText().toString());
            params.put("score", mUseScore);
            String url = "order/add";


            if ("groupBuy".equals(mFrom)) {
                params.put("groupCode", mGroupCode);
                url = "groupOrder/add";
            } else if ("joinGroup".equals(mFrom)) {
                params.put("groupCode", mGroupCode);
                url = "groupOrder/addJoinGroup";
            } else if (orderType.equals(Const.ORDER_TYPE_OBLIGED)) {
                params.put("orderType", "6");
                int pushId = getSharedPreferences("pushId", 0).getInt("pushId", 0);
                params.put("pushId", pushId);
            }

            APIManager.startRequest(mOrderService.create(url, APIManager.buildJsonBody(params)), new BaseRequestListener<OrderResponse>() {
                @Override
                public void onSuccess(OrderResponse result) {
                    ToastUtil.success("创建订单成功");
                    EventBus.getDefault().post(new EventMessage(Event.createOrderSuccess));
//                    EventBus.getDefault().post();
                    if (orderType.equals(Const.ORDER_TYPE_OBLIGED)) {
                        Intent intent = new Intent(PayActivity.this, PayUpgradeActivity.class);
                        ActivityControl.getInstance().add(PayActivity.this);
                        intent.putExtra("orderCode", result.orderCode);
                        ActivityControl.getInstance().add(PayActivity.this);
                        getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
                        startActivity(intent);
                        finish();
                    } else if (orderType.equals(Const.ORDER_TYPE_FREE_ORDER)) {
                        //上级的级数为C0
                        if (0 == result.upLevel) {
                            Intent intent = new Intent(PayActivity.this, WaitForPaymentActivity.class);
                            intent.putExtra("orderCode", result.orderCode);
                            getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
                            startActivity(intent);
                            finish();
                        } else {
                            //上级的级数是C0以上
                            Intent intent = new Intent(PayActivity.this, OrderDetailActivity.class);
                            intent.putExtra("orderCode", result.orderCode);
                            intent.putExtra("mode", 4);
                            getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
                            startActivity(intent);
                            finish();
                        }
                    }
<<<<<<< HEAD
                });
        APIManager.startRequest(observable, new BaseRequestListener<Order>() {
            @Override
            public void onSuccess(Order result) {
                super.onSuccess(result);
                ToastUtil.success("下单成功");
                mOrderCode = result.orderMain.orderCode;
                EventBus.getDefault().post(new EventMessage(Event.createOrderSuccess));
                checkOrder(result);
//                if (result.orderMain.totalMoney > 0) {
//                    checkOrder(result);
//                } else {
//                    EventUtil.viewOrderDetail(PayActivity.this, mOrderCode, false);
//                    finish();
//                }
=======
                }
            }, PayActivity.this);
        } else {
            HashMap<String, Object> params = new HashMap<>();
            params.put("addressId", mAddress.addressId);
            params.put("products", convertProducts());
            params.put("orderFrom", 1);
            params.put("couponId", mUseCoupon == null ? "" : mUseCoupon.couponId);
            params.put("remark", mRemarkEt.getText().toString());
            params.put("score", mUseScore);
            String url = "order/add";
            if (null != mPickUpList && mPickUpList.size() > 0 && productType != 18) {
                params.put("vouchers", getVouchers());
            }
            if ("groupBuy".equals(mFrom)) {
                params.put("groupCode", mGroupCode);
                url = "groupOrder/add";
            } else if ("joinGroup".equals(mFrom)) {
                params.put("groupCode", mGroupCode);
                url = "groupOrder/addJoinGroup";
            }

            Observable<RequestResult<Order>> observable = mOrderService
                    .create(url, APIManager.buildJsonBody(params))
                    .flatMap(new Function<RequestResult<OrderResponse>, ObservableSource<RequestResult<Order>>>() {
                        @Override
                        public ObservableSource<RequestResult<Order>> apply(RequestResult<OrderResponse> result) throws Exception {
                            if (result.isFail()) {
                                throw new RuntimeException(result.message);
                            }
                            return mOrderService.getOrderByCode(result.data.orderCode);
                        }
                    });
            APIManager.startRequest(observable, new BaseRequestListener<Order>() {
                @Override
                public void onSuccess(Order result) {
                    super.onSuccess(result);
                    ToastUtil.success("下单成功");
                    mOrderCode = result.orderMain.orderCode;
                    EventBus.getDefault().post(new EventMessage(Event.createOrderSuccess));
                    checkOrder(result);
                }
            }, this);
        }

>>>>>>> develop_0822_backup

    }*/
    protected void addOrderAfterConfirm() {
        String dialogContent = "";
        boolean showConfirmDialog = false;
        if (isCarPi()) {
            // 密码pi商品下单
            showConfirmDialog = true;
            dialogContent = "因本产品为虚拟产品，故不享受7天无理由退货政策。介意勿拍！！！";
        } else if (getProductType() == 2) {
            // 礼包下单
            showConfirmDialog = true;
            dialogContent = "因礼包专区所有产品均属于促销产品，故不享受7天无理由退货政策。介意勿拍！！！";
        } else if (getProductType() == 18) {
            // 嗨购下单
            showConfirmDialog = true;
            dialogContent = "因嗨购礼包属于清仓产品，故不享受7天无理由退货政策，介意勿拍！！！";
        }

        if (!showConfirmDialog) {
            addOrder();
            return;
        }

        final WJDialog dialog = new WJDialog(this);
        dialog.show();
        dialog.setTitle("提示");
        dialog.setContentText(dialogContent);
        dialog.setCancelText("我想想");
        dialog.setConfirmText("我明白，继续");
        dialog.setOnConfirmListener(v -> {
            dialog.dismiss();
            addOrder();
        });
    }

    private boolean isCarPi() {
        if (mSkuInfo == null) {
            return false;
        }
        return mSkuInfo.isCatPi();
    }

    protected void addOrder() {
        /*if (orderType.equals(Const.ORDER_TYPE_OBLIGED) || orderType.equals(Const.ORDER_TYPE_FREE_ORDER)) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("addressId", mAddress.addressId);
            params.put("products", convertProducts());
            params.put("orderFrom", 1);
            params.put("couponId", mUseCoupon == null ? "" : mUseCoupon.couponId);
            params.put("remark", mRemarkEt.getText().toString());
            params.put("score", mUseScore);
            String url = "order/add";

            if (null != mPickUpList && mPickUpList.size() > 0 && productType != 18) {
                params.put("vouchers", getVouchers());
            }


            if ("groupBuy".equals(mFrom)) {
                params.put("groupCode", mGroupCode);
                url = "groupOrder/add";
            } else if ("joinGroup".equals(mFrom)) {
                params.put("groupCode", mGroupCode);
                url = "groupOrder/addJoinGroup";
            } else if (orderType.equals(Const.ORDER_TYPE_OBLIGED)) {
                params.put("orderType", "6");
                int pushId = getSharedPreferences("pushId", 0).getInt("pushId", 0);
                params.put("pushId", pushId);
            } else if (orderType.equals(Const.ORDER_TYPE_FREE_ORDER)) {
                params.put("orderType", "5");
            }

            APIManager.startRequest(mOrderService.create(url, APIManager.buildJsonBody(params)), new BaseRequestListener<OrderResponse>() {
                @Override
                public void onSuccess(OrderResponse result) {
                    ToastUtil.success("创建订单成功");
                    EventBus.getDefault().post(new EventMessage(Event.createOrderSuccess));
//                    EventBus.getDefault().post();
                    if (orderType.equals(Const.ORDER_TYPE_OBLIGED)) {
                        Intent intent = new Intent(PayActivity.this, PayUpgradeActivity.class);
                        ActivityControl.getInstance().add(PayActivity.this);
                        intent.putExtra("orderCode", result.orderCode);
                        ActivityControl.getInstance().add(PayActivity.this);
                        getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
                        startActivity(intent);
                        finish();
                    } else if (orderType.equals(Const.ORDER_TYPE_FREE_ORDER)) {
                        //上级的级数为C0
                        if (0 == result.upLevel) {
                            Intent intent = new Intent(PayActivity.this, WaitForPaymentActivity.class);
                            intent.putExtra("orderCode", result.orderCode);
                            getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
                            startActivity(intent);
                            finish();
                        } else {
                            //上级的级数是C0以上
                            Intent intent = new Intent(PayActivity.this, OrderDetailActivity.class);
                            intent.putExtra("orderCode", result.orderCode);
                            intent.putExtra("mode", 4);
                            getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
                            startActivity(intent);
                            finish();
                        }
                    }
                }
            }, PayActivity.this);
        } else {*/
        HashMap<String, Object> params = new HashMap<>();
        params.put("addressId", mAddress.addressId);
        params.put("products", convertProducts());
        params.put("orderFrom", 1);
        params.put("couponId", mUseCoupon == null ? "" : mUseCoupon.couponId);
        params.put("remark", mRemarkEt.getText().toString());
        params.put("score", mUseScore);
        String url = "order/add";
        if (null != mPickUpList && mPickUpList.size() > 0 && productType != 18) {
            params.put("vouchers", getVouchers());
        }
        if ("groupBuy".equals(mFrom)) {
            params.put("groupCode", mGroupCode);
            url = "groupOrder/add";
        } else if ("joinGroup".equals(mFrom)) {
            params.put("groupCode", mGroupCode);
            url = "groupOrder/addJoinGroup";
        } else if (orderType.equals(Const.ORDER_TYPE_OBLIGED)) {
            params.put("orderType", "6");
            int pushId = getSharedPreferences("pushId", 0).getInt("pushId", 0);
            params.put("pushId", pushId);
        } else if (orderType.equals(Const.ORDER_TYPE_FREE_ORDER)) {
            params.put("orderType", "5");
        }

        Observable<RequestResult<Order>> observable = mOrderService
                .create(url, APIManager.buildJsonBody(params))
                .flatMap(new Function<RequestResult<OrderResponse>, ObservableSource<RequestResult<Order>>>() {
                    @Override
                    public ObservableSource<RequestResult<Order>> apply(RequestResult<OrderResponse> result) throws Exception {
                        if (result.code == 90) { // CCM抵扣未授权
                            throw new CCMException(result.message);
                        }
                        if (result.isFail()) {
                            throw new RuntimeException(result.message);
                        }
                        return mOrderService.getOrderByCode(result.data.orderCode, "");
                    }
                });
        APIManager.startRequest(observable, new BaseRequestListener<Order>() {
            @Override
            public void onSuccess(Order result) {
                super.onSuccess(result);
                ToastUtil.success("下单成功");
                mOrderCode = result.orderMain.orderCode;
                EventBus.getDefault().post(new EventMessage(Event.createOrderSuccess));
                checkOrder(result);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
//                Log.d("Seven", "12 " + e.getClass());
                if (e instanceof CCMException) {
                    final WJDialog dialog = new WJDialog(PayActivity.this);
                    dialog.show();
                    dialog.setTitle("CCM支付授权");
                    dialog.setContentText("使用CCM支付，需获取CCM支付授权，后续无需您再次授权。");
                    dialog.setCancelText("取消");
                    dialog.setConfirmText("授权");
                    dialog.setConfirmTextColor(R.color.color_bule);
                    dialog.setOnConfirmListener(v -> {
                        dialog.dismiss();
                        APIManager.startRequest(mProductService.authorizeCcm(1), new BaseRequestListener<Object>() {
                            @Override
                            public void onSuccess(Object result) {
                                super.onSuccess(result);
                                addOrder();
                            }
                        }, PayActivity.this);
                    });
                }
            }
        }, this);


    }

    public static class CCMException extends RuntimeException {

        public CCMException(String msg) {
            super(msg);
        }
    }

    private void checkOrder(final Order order) {
        APIManager.startRequest(mOrderService.checkOrderToPay(order.orderMain.orderCode), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                goPayOrder(order);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                EventUtil.viewOrderDetail(PayActivity.this, mOrderCode, false);
                EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_TO_PAY));
            }
        }, this);
    }


    @OnClick(R.id.confirmBtn)
    protected void checkData() {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (mLayoutPayType.getProfitData() == null) {
            ToastUtil.error("等待数据加载");
            return;
        }
        if (mLayoutPayType.getProfitData().goldenAvlTicket < getGoldenTicket() && hasGoldenTicket()) {
            ToastUtil.error("金券不足不能下单");
            return;
        }


        if (getProductType() == 2) {
            if (TextUtils.isEmpty(loginUser.referrerMemberId) && !isBinding) {
                getInvitationCode();
                return;
            }
        }
        if (mAddress == null || mAddress.addressId == null || mAddress.addressId.isEmpty()) {
            ToastUtil.error("请选择收货地址");
            return;
        } else if (mLayoutIdCardEdit.getVisibility() == View.VISIBLE) {
            ToastUtil.error("请先填写并保存收件人的身份证信息");
            return;
        }

        if (productType == 18) {
            IUserService userService = ServiceManager.getInstance().createService(IUserService.class);
            if (orderType.equals(Const.ORDER_TYPE_FREE_ORDER)) {
                APIManager.startRequest(userService.checkIsBind(), new BaseRequestListener<UpMemberInfoBean>() {
                    @Override
                    public void onSuccess(UpMemberInfoBean result) {
                        if (result.isBind == 0) {//已绑
                            selectPayment();
                        } else if (result.isBind == 3) {
                            getInvitationCode();
                        }
                    }
                }, PayActivity.this);
            } else {
                addOrderAfterConfirm();
            }
        } else {
            selectPayment();
        }

//        if (productType == 18) {
//            mLyPayType.setVisibility(View.GONE);
            /*IUserService userService = ServiceManager.getInstance().createService(IUserService.class);
            if (orderType.equals(Const.ORDER_TYPE_FREE_ORDER)) {
                APIManager.startRequest(userService.checkIsBind(), new BaseRequestListener<UpMemberInfoBean>() {
                    @Override
                    public void onSuccess(UpMemberInfoBean result) {
                        if (result.isBind == 0) {//已绑
                            addOrder();
                        } else if (result.isBind == 3) {
                            getInvitationCode();
                        }
                    }
                }, PayActivity.this);
            } else {
                addOrder();
            }
//        } else {
        final int selectType = mLayoutPayType.getSelectType();
        if (selectType == -1 && mPayByMoney) {
            ToastUtil.error("请选择支付方式");
        } else if (selectType == AppTypes.PAY_TYPE.BALANCE || selectType == AppTypes.PAY_TYPE.BALANCE_GOLD) {
            UserService.checkHasPassword(PayActivity.this, new UserService.HasPasswordListener() {
                @Override
                public void onHasPassword() {
                    if (selectType == AppTypes.PAY_TYPE.BALANCE) {
                        if ((mPayByMoney && mLayoutPayType.getBalance() < mTotalPrice) ||
                                (!mPayByMoney && mLayoutPayType.getBalance() < mFreight)) {
                            ToastUtil.error("账户余额不足");
                        } else {
                            addOrder();
                        }
                    } else {
                        if ((mPayByMoney && mLayoutPayType.getBalanceGold() < mTotalPrice) ||
                                (!mPayByMoney && mLayoutPayType.getBalanceGold() < mFreight)) {
                            ToastUtil.error("账户购物金不足");
                        } else {
                            addOrder();
                        }
                    }

                }
            });
        } else {
            addOrder();
        }*/
    }

    private Counter counter;

    private void selectPayment() {
        final int selectType = mLayoutPayType.getSelectType();
        if (selectType == -1 && mPayByMoney) {
            ToastUtil.error("请选择支付方式");
        } else if (selectType == AppTypes.PAY_TYPE.BALANCE || selectType == AppTypes.PAY_TYPE.BALANCE_GOLD) {
            if (counter != null && !counter.shouldReq()) {
                return;
            }
            counter = Counter.startReq();
            UserService.checkHasPassword(PayActivity.this, new UserService.HasPasswordListener() {
                @Override
                public void onHasPassword() {
                    if (selectType == AppTypes.PAY_TYPE.BALANCE) {
                        if ((mPayByMoney && mLayoutPayType.getBalance() < mTotalPrice) ||
                                (!mPayByMoney && mLayoutPayType.getBalance() < mFreight)) {
                            ToastUtil.error("账户余额不足");
                        } else {
                            addOrderAfterConfirm();
                        }
                    } else {
                        if ((mPayByMoney && mLayoutPayType.getBalanceGold() < mTotalPrice) ||
                                (!mPayByMoney && mLayoutPayType.getBalanceGold() < mFreight)) {
                            ToastUtil.error("账户购物券不足");
                        } else {
                            addOrderAfterConfirm();
                        }
                    }

                }
            });
        } else {
            addOrderAfterConfirm();
        }
    }

//    }


    private void getUserInfo(final QMUIDialog dialog, String phone) {
        APIManager.startRequest(mUserService.getUserInfoByPhone(phone), new BaseRequestListener<User>(this) {
            @Override
            public void onSuccess(User result) {
                dialog.dismiss();
                mUser = result;
                showInvitationInfo(result);
            }
        }, this);
    }

    private void getInvitationCode() {
        final QMUIDialog.EditTextDialogBuilder builder = new QMUIDialog.EditTextDialogBuilder(this);
        builder.setTitle("请输入邀请人手机号")
                .setPlaceholder("请输入邀请人手机号")
                .setInputType(InputType.TYPE_CLASS_PHONE)
                .addAction("取消", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        dialog.dismiss();
                    }
                })
                .addAction("确定", new QMUIDialogAction.ActionListener() {
                    @Override
                    public void onClick(QMUIDialog dialog, int index) {
                        CharSequence text = builder.getEditText().getText();
                        mInvitaionPhone = text.toString();
                        getUserInfo(dialog, mInvitaionPhone);
                    }
                });
        EditText editText = builder.getEditText();
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(11)});
        builder.show();
    }


    private void showInvitationInfo(User user) {
        if (mInvitaionUserDialog == null) {
            mInvitaionUserDialog = new InvitaionUserDialog(this, onClickListener);
        }
        mInvitaionUserDialog.show();
        mInvitaionUserDialog.setUser(mUser);
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mInvitaionUserDialog.dismiss();
            APIManager.startRequest(mUserService.bindingMember(mInvitaionPhone), new BaseRequestListener<User>(PayActivity.this) {
                @Override
                public void onSuccess(User result) {
                    isBinding = true;
                    /*if (productType == 18) {
                        addOrder();
                    } else {

                    }*/

                    checkData();

                }
            }, PayActivity.this);
        }
    };

    private void goPayOrder(Order order) {
        int type = mLayoutPayType.getSelectType();
        Log.d("Seven", "pageType -> " + type);
        switch (type) {
            case AppTypes.PAY_TYPE.WECHAT_WEB:
            case AppTypes.PAY_TYPE.ALI_WEB:
            case AppTypes.PAY_TYPE.UN_WEB:
            case AppTypes.PAY_TYPE.WECHAT:
                WePayUtils.payByWeb(this, order, mLayoutPayType.getSelectType());
                break;
            case AppTypes.PAY_TYPE.BALANCE:
            case AppTypes.PAY_TYPE.BALANCE_GOLD:
                // 余额支付
                PayUtil.payBalance(PayActivity.this, order, mLayoutPayType.getSelectType());
                getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
//                finish();
                break;
//            case AppTypes.PAY_TYPE.WECHAT:
//                WeixinPayUtils.pay(
//                        this,
//                        order.orderMain.payMoney,
//                        order.orderMain.orderCode
//                );
//                break;
            case AppTypes.PAY_TYPE.ALI:
                AliPayUtils.pay(this, order);
                break;
            case AppTypes.PAY_TYPE.CHANJIE:
            case AppTypes.PAY_TYPE.SHANDE:
                WePayUtils.payByWeb(this, order, type);
                break;
            default:
                ToastUtil.error("请选择支付方式");
                break;
        }
    }

    @OnClick({R.id.ivIdCardClean, R.id.tvCardSave, R.id.ivIdCardEdit, R.id.layoutIdCard})
    public void onCardViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivIdCardClean:
                mEtIdCard.setText("");
                break;
            case R.id.tvCardSave:
                final String card = mEtIdCard.getText().toString();
                APIManager.startRequest(
                        mAddressService.saveIdentityCard(
                                new SaveIdentityCardBody(
                                        mAddress.addressId,
                                        card
                                )
                        ),
                        new BaseRequestListener<Object>() {
                            @Override
                            public void onStart() {
                                super.onStart();
                                mTvCardSave.setVisibility(View.GONE);
                                mPbIdCart.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onSuccess(Object result) {
                                mTvCardSave.setVisibility(View.VISIBLE);
                                mPbIdCart.setVisibility(View.GONE);

                                mEtIdCard.setText("");
                                mAddress.identityCard = card;
                                initAddressView();
                                ToastUtil.error("保存成功");
                            }

                            @Override
                            public void onError(Throwable e) {
                                super.onError(e);
                                mTvCardSave.setVisibility(View.VISIBLE);
                                mPbIdCart.setVisibility(View.GONE);
                            }
                        }, this
                );
                break;
            case R.id.ivIdCardEdit:
                mLayoutIdCardEdit.setVisibility(View.VISIBLE);
                mLayoutIdCardRead.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    @OnClick(R.id.pickUpLayout)
    protected void pickUpLayout() {
        Intent intent = new Intent(PayActivity.this, PickUpDetailListActivity.class);
        intent.putExtra("select", mSelectVoucher);
        intent.putExtra("body", mCalcOrderListBody);
        intent.putExtra("type", PickUpDetailListActivity.PAY_USE);
        getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
        startActivityForResult(intent, Action.SELECT_VOUCHER);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(PayMsg msgStatus) {
        switch (msgStatus.getAction()) {
            case PayMsg.ACTION_ALIPAY_SUCCEED:
            case PayMsg.ACTION_WEBPAY_SUCCEED:
            case PayMsg.ACTION_WXPAY_SUCCEED:
                EventBus.getDefault().post(new EventMessage(Event.paySuccess));
                EventUtil.viewOrderDetail(this, mOrderCode, false);
                ToastUtil.success("支付成功");
                getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
                finish();
                break;
            case PayMsg.ACTION_BALANCE_SUCCEED:
                getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
                finish();
                break;
            case PayMsg.ACTION_WXPAY_FAIL:
            case PayMsg.ACTION_ALIPAY_FAIL:
            case PayMsg.ACTION_WEBPAY_FAIL:
            case PayMsg.ACTION_BALANCE_CANCEL:
//                ToastUtils.showShortToast(msgStatus.message);
                ToastUtil.error(msgStatus.message);
                EventUtil.viewOrderDetail(this, mOrderCode, false);
                getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
                finish();
                break;
            default:
        }
    }

    String province = "";
    String city = "";
    String distinct = "";


    public void analysisAddress(String address) {
        IRegionService mRegionService = ServiceManager.getInstance().createService(IRegionService.class);
        APIManager.startRequest(mRegionService.getProvinceList(), new BaseRequestListener<List<RegionProvince>>(PayActivity.this) {
            @Override
            public void onSuccess(List<RegionProvince> result) {
                for (RegionProvince regionProvince : result) {
                    int index = address.indexOf(regionProvince.name);
                    if (index == 0) {
                        //
                        province = regionProvince.name;
                        String substring = removeSameStr(province, address);
                        new Thread() {
                            @Override
                            public void run() {

                                APIManager.startRequest(mRegionService.getCityList(regionProvince.id), new BaseRequestListener<List<RegionCity>>(PayActivity.this) {
                                    @Override
                                    public void onSuccess(List<RegionCity> result) {
                                        for (RegionCity regionCity : result) {
                                            int indexOf = substring.indexOf(regionCity.name);
                                            if (indexOf == 0) {
                                                city = regionCity.name;
                                                String substring1 = substring.substring(city.length(), substring.length());
                                                new Thread() {
                                                    @Override
                                                    public void run() {
                                                        APIManager.startRequest(mRegionService.getDistinctList(regionCity.id), new BaseRequestListener<List<RegionDistinct>>() {
                                                            @Override
                                                            public void onSuccess(List<RegionDistinct> result) {
                                                                for (RegionDistinct regionDistinct : result) {
                                                                    int indexOf1 = substring1.indexOf(regionDistinct.name);
                                                                    if (indexOf1 == 0) {
                                                                        distinct = regionDistinct.name;
                                                                        String substring2 = substring1.substring(distinct.length(), substring1.length());
                                                                        tv_area.setText(province + city + distinct);
                                                                        et_address.setText(substring2);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }, PayActivity.this);
                                                    }
                                                }.start();
                                                break;
                                            }
                                        }
                                    }
                                }, PayActivity.this);

                            }
                        }.start();
                        break;
                    }
                }
            }
        }, PayActivity.this);

    }


    private String removeSameStr(String strA, String strB) {
        String returnStr = "";
        char[] chars = strA.toCharArray();
        char[] chars1 = strB.toCharArray();
        List<String> list1 = Arrays.asList(strA);
        List<String> list2 = Arrays.asList(strB);
        list2.removeAll(list1);
        for (String s : list2) {
            returnStr = returnStr + s;
        }
        return returnStr;
    }


}
