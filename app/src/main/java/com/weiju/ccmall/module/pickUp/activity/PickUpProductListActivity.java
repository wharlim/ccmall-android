package com.weiju.ccmall.module.pickUp.activity;

import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.pickUp.adapter.PickUpProductAdapter;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IPickUpService;
import com.weiju.ccmall.shared.util.EventUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @author chenyanming
 * @time 2019/8/2 on 16:53
 * @desc ${TODD}
 */
public class PickUpProductListActivity extends BaseListActivity {

    private PickUpProductAdapter mAdapter = new PickUpProductAdapter();

    private String mVouchersId;

    private IPickUpService mService = ServiceManager.getInstance().createService(IPickUpService.class);

    @Override
    public void getIntentData() {
        super.getIntentData();
        mVouchersId = getIntent().getStringExtra("vouchersId");
    }

    @Override
    public String getTitleStr() {
        return "提货券产品";
    }

    @Override
    public void initView() {
        super.initView();
        EventBus.getDefault().register(this);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.background));
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (mAdapter.getItem(position) != null) {
            EventUtil.viewProductDetail(PickUpProductListActivity.this, mAdapter.getItem(position).skuId, false);
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mService.getSkuListByVouchersId(mCurrentPage, 15, mVouchersId),
                new BaseRequestListener<PaginationEntity<SkuInfo, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && result.list.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                },this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void transferHandler(EventMessage message) {
        switch (message.getEvent()) {
            case createOrderSuccess:
//                mCurrentPage = 1;
//                getData(true);
                finish();
                break;
            default:
        }
    }

}
