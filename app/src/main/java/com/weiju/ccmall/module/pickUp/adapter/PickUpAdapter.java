package com.weiju.ccmall.module.pickUp.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.PickUp;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

/**
 * @author chenyanming
 * @time 2019/8/1 on 16:34
 * @desc ${TODD}
 */
public class PickUpAdapter extends BaseQuickAdapter<PickUp, BaseViewHolder> {
    private boolean mIsTransfer;

    public PickUpAdapter() {
        this(false);
    }

    public PickUpAdapter(boolean isTransfer) {
        super(R.layout.item_pick_up);
        mIsTransfer = isTransfer;
    }

    @Override
    protected void convert(BaseViewHolder helper, PickUp item) {
        FrescoUtil.setImage(helper.getView(R.id.ivAvatar), item.iconUrl);
        helper.setText(R.id.tvTitle, item.title);
        helper.setText(R.id.tvNum, String.format("X%s", mIsTransfer ? "1" : item.num));
        helper.setText(R.id.tvMoney, MoneyUtil.centToYuan¥StrNoZero(item.cost));
        helper.addOnClickListener(R.id.tvUse);
        helper.setVisible(R.id.tvUse, !mIsTransfer);
    }

}
