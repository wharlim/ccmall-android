package com.weiju.ccmall.module.jkp.newjkp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.LinearLayout;

import com.blankj.utilcode.utils.StringUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.jkp.IJkpProductService;
import com.weiju.ccmall.module.jkp.JiKaoPuProductDetailActivity;
import com.weiju.ccmall.module.jkp.newjkp.entity.BannerEntity;
import com.weiju.ccmall.module.jkp.newjkp.widgets.BannerElement;
import com.weiju.ccmall.module.jkp.newjkp.widgets.ImagesElement;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseObserver;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.MoneyUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author Ben
 * @date 2020/4/10.
 */
public class ClassifyFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    public RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    public SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.vsEmpty)
    public ViewStub vsEmpty;
    @BindView(R.id.llElement)
    public LinearLayout llElement;


    private IJkpProductService jkpProductService = ServiceManager.getInstance().createService(IJkpProductService.class);
    private String mPageId;
    private List<GoodsListByCategory.DataBean> mData = new ArrayList<>();
    private ClassifyAdapter mAdapter = new ClassifyAdapter(mData);
    private String mKeyword;
    private int minId = 1;
    private View mNoDataView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        Fresco.initialize(this.getActivity());
        View rootView = inflater.inflate(R.layout.fragment_classify, container, false);
        ButterKnife.bind(this, rootView);
        EventBus.getDefault().register(this);
        getIntentData();
        initView();
        loadPageConfig(true);
        return rootView;
    }

    public static ClassifyFragment newInstance(String pageId, String keyword) {
        Bundle args = new Bundle();
        args.putString("id", pageId);
        args.putString("keyword", keyword);
        ClassifyFragment fragment = new ClassifyFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void getIntentData() {
        mPageId = getArguments().getString("id");
        mKeyword = getArguments().getString("keyword");
    }

    public void initView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 2);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.bg_gray));
//        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(2, SizeUtils.dp2px(5),true));
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setNestedScrollingEnabled(false);
        mAdapter.setOnItemClickListener(this::onListItemClick);
        minId = 1;
        mRefreshLayout.setOnRefreshListener(refreshLayout -> {
            loadPageConfig(true);
        });
//        mRefreshLayout.setEnableLoadMore(true);
        mRefreshLayout.setOnLoadMoreListener(refreshLayout -> {
            loadPageConfig(false);
        });
//        mRefreshLayout.setEnableHeaderTranslationContent(false);
//        mRefreshLayout.setEnableOverScrollBounce(false);//是否启用越界回弹
        mRefreshLayout.setEnableLoadMoreWhenContentNotFull(false);//在内容不满一页的时候，是否可以上拉加载更多
    }

    private void getBanner() {
        if (mPageId.equals("0")) {
            APIManager.startRequest(jkpProductService.activityList(), new BaseRequestListener<BannerEntity>() {
                @Override
                public void onSuccess(BannerEntity result) {
                    super.onSuccess(result);
                    llElement.removeAllViews();
                    List<BannerEntity.Bean> bannerList = result.bannerList;
                    if (bannerList != null && bannerList.size() > 0) {
                        llElement.addView(new BannerElement(getContext(), bannerList));
                    }
                    List<BannerEntity.Bean> relationList = result.relationList;
                    if (relationList != null && relationList.size() > 0) {
                        llElement.addView(new ImagesElement(getContext(), relationList));
                    }
                    llElement.addView(mRecyclerView);
                }
            }, getContext());
        }
    }

    boolean mVisible = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mVisible = isVisibleToUser;
        if (isVisibleToUser) {
//            loadPageConfig();
        }
    }

    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        GoodsListByCategory.DataBean keywordItem = mData.get(position);
        SkuInfo skuInfo = new SkuInfo();
        skuInfo.originId = keywordItem.itemid;
        skuInfo.name = keywordItem.itemtitle;
        skuInfo.desc = keywordItem.itemdesc;
        skuInfo.retailPrice = MoneyUtil.yuanStrToCentLong(keywordItem.itemendprice);
        skuInfo.marketPrice = MoneyUtil.yuanStrToCentLong(keywordItem.itemprice);
        skuInfo.costPrice = MoneyUtil.yuanStrToCentLong(keywordItem.couponmoney);
        JiKaoPuProductDetailActivity.start(getContext(), skuInfo);
    }

    public void loadPageConfig(boolean isRefresh) {
        if (isRefresh){
            minId = 1;
            getBanner();
        }
        if (StringUtils.isEmpty(mPageId)) {
            return;
        }

        APIManager.startRequest(jkpProductService.getGoodsListByCategory(mPageId, minId, 20),
                new BaseObserver<GoodsListByCategory>(getContext()) {
                    @Override
                    public void onHandleSuccess(GoodsListByCategory goodsListByCategory) {
                        if (minId == 1) mData.clear();
                        minId = goodsListByCategory.minId;
                        if (goodsListByCategory.data.size() < 1) {
                            mRefreshLayout.setNoMoreData(false);
                            return;
                        }
                        mData.addAll(goodsListByCategory.data);
                        mAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onComplete() {
                        super.onComplete();
                        mRefreshLayout.finishRefresh();
                        mRefreshLayout.finishLoadMore();
                        if (mData.size() > 0) {
                            hideEmptyView();
                        } else {
                            showEmptyView();
                        }
                    }
                });
    }

    public void showEmptyView() {
        if (mNoDataView == null) {
            mNoDataView = vsEmpty.inflate();
        } else {
            mNoDataView.setVisibility(View.VISIBLE);
        }
    }

    public void hideEmptyView() {
        if (mNoDataView != null) {
            mNoDataView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
//            case logout:
            case loginSuccess:
                getBanner();
                break;
            default:
        }
    }
}
