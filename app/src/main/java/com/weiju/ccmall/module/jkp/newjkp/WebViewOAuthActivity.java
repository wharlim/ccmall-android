package com.weiju.ccmall.module.jkp.newjkp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ProgressBar;

import com.alibaba.baichuan.android.trade.adapter.login.AlibcLogin;
import com.alibaba.baichuan.android.trade.config.AlibcConfig;
import com.blankj.utilcode.utils.StringUtils;
import com.orhanobut.logger.Logger;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.jkp.IJkpProductService;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.H5WebView;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WebViewOAuthActivity extends BaseActivity {

    @BindView(R.id.webView)
    H5WebView webView;
    @BindView(R.id.pb)
    ProgressBar mPb;
    private String mUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_common);
        ButterKnife.bind(this);
//        mUrl = getIntent().getStringExtra("url");
//        mUrl = "https://oauth.taobao.com/authorize?response_type=token&client_id=26026690&redirect_uri=https://testjkp.create-chain.net/oauth&state=1212&view=wap";
        String webTTID = AlibcConfig.getInstance().getWebTTID();
        mUrl = "https://oauth.taobao.com/authorize?response_type=token&client_id=26026690&redirect_uri=https://testjkp.create-chain.net/oauth&state=1212&view=wap&ttid=" + webTTID;
        setLeftBlack();
        if (StringUtils.isEmpty(mUrl)) {
            return;
        }
        openWeb();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        webView.destroy();
    }

    private void openWeb() {
        webView.setWebViewChangeTitleCallback(this::setTitle);
        webView.setProgressbar(mPb);
        webView.setWebViewInterceptUrlCallback(new H5WebView.WebViewInterceptUrlCallback() {
            @Override
            public boolean onInterceptUrl(String url) {
                if (url.contains("access_token")) {
                    String accessToken = getValueByName(url, "access_token");
                    Logger.d("getValueByName -> " + accessToken);
                    saveRelationId(accessToken);
                    return true;
                } else {
                    return false;
                }
            }
        });
        webView.loadUrl(mUrl);
    }


    /***
     * 获取url 指定name的value;
     * @param url
     * @param name
     * @return
     */
    private String getValueByName(String url, String name) {
        String result = "";
//        int index = url.indexOf("?");
        int index = url.indexOf("#");
        String temp = url.substring(index + 1);
        String[] keyValue = temp.split("&");
        for (String str : keyValue) {
            if (str.contains(name)) {
                result = str.replace(name + "=", "");
                break;
            }
        }
        return result;
    }

    private void saveRelationId(String sessionKey) {
        if (AlibcLogin.getInstance().isLogin()) {
            IJkpProductService service = ServiceManager.getInstance().createService(IJkpProductService.class);
            APIManager.startRequest(service.saveRelationId(sessionKey), new BaseRequestListener<Object>(this) {
                @Override
                public void onSuccess(Object result) {
                    ToastUtil.success("授权成功");
                    EventBus.getDefault().post(new EventMessage(Event.taobaoAuthorize));
                    finish();
                }

            }, this);
        }
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, WebViewOAuthActivity.class);
//        intent.putExtra("url", url);
        context.startActivity(intent);
    }
}
