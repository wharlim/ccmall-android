package com.weiju.ccmall.module.product.adapter;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.TextureView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderDetailActivity;
import com.weiju.ccmall.module.pay.PayActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.ProductComment;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.ImageUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import cn.bingoogolapple.photopicker.widget.BGANinePhotoLayout;

/**
 * <p>
 * Created by zjm on 2017/8/30.
 */
public class ProductCommentAdapter extends BaseQuickAdapter<ProductComment, BaseViewHolder> {

    private String mHtmlStoreFormat = "<font color=\"#333333\">商家回复：</font> %s";

    boolean mIsNewRetail = false;

    String mProductId = "";

    public ProductCommentAdapter(@Nullable List<ProductComment> data) {
        super(R.layout.item_product_comment, data);
    }


    public ProductCommentAdapter(List<ProductComment> data, boolean isNewRetail) {
        super(R.layout.item_product_comment, data);
        mIsNewRetail = isNewRetail;
    }

    public ProductCommentAdapter(List<ProductComment> data, String deleteComment, String productId) {
        super(R.layout.item_product_comment, data);
        mProductId = productId;
    }


    @Override
    protected void convert(BaseViewHolder helper, final ProductComment item) {
        setUserInfo(helper, item);
        Spanned storeReply = Html.fromHtml(String.format(mHtmlStoreFormat, item.reply));
        helper.setText(R.id.tvTimeOne, getTimeOne(item))
                .setText(R.id.tvContentOne, item.content)
                .setText(R.id.tvReplyOne, storeReply);
//        BGANinePhotoLayout layoutImagesOne = helper.getView(R.id.layoutCommentNineImagesOne);
//        setImagePhone(layoutImagesOne, item.images);
        helper.setVisible(R.id.tvReplyOne, !StringUtils.isEmpty(item.reply));

        setAddToCommentData(helper, item);

        boolean canDeleteComment = true;
        if (canDeleteComment) {
            if (item.descScore <= 10) {
                TextView tv_delete = helper.getView(R.id.tv_delete);
                if (item.deleteStatus == 0) {
                    helper.setVisible(R.id.tv_delete, true);
                    tv_delete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                            builder.setTitle("删除评论");
                            builder.setMessage("确定删除该评价吗？");
                            //点击对话框以外的区域是否让对话框消失
                            builder.setCancelable(false);
                            builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    IProductService productService = ServiceManager.getInstance().createService(IProductService.class);
                                    APIManager.startRequest(productService.deleteMyProductComment(mProductId, item.orderCode), new BaseRequestListener<Object>() {
                                        @Override
                                        public void onSuccess(Object result) {
                                            ToastUtil.success("删除成功");
                                            tv_delete.setText("[该评价已删除]");
                                            ProductCommentAdapter.this.notifyDataSetChanged();
                                        }
                                    }, mContext);
                                }
                            });
                            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            builder.show();

                        }
                    });
                } else {
                    helper.setVisible(R.id.tv_delete, true);
                    tv_delete.setText("[该评价已删除]");
                }
            }
        } else {
            helper.setVisible(R.id.tv_delete, false);
        }

        if (mIsNewRetail) {
            helper.setVisible(R.id.tv_phone, false);
            helper.setVisible(R.id.tv_check_order, true);
            if (!TextUtils.isEmpty(item.phone) && item.phone.length() >= 9) {
                String substring = item.phone.substring(4, 8);
                String temp = item.phone.replace(substring, "****");
                helper.setText(R.id.tv_phone, temp);
            } else
                helper.setText(R.id.tv_phone, item.phone);

            helper.getView(R.id.tv_check_order).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, OrderDetailActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("orderCode", item.orderCode);
                    intent.putExtras(bundle);
//                    intent.putExtra("orderCode", item.addToComment.orderCode);
                    mContext.startActivity(intent);
                }
            });
        }

    }

    /**
     * 设置追加评论相关的东西
     *
     * @param helper
     * @param item
     */
    private void setAddToCommentData(BaseViewHolder helper, ProductComment item) {
        if (!mIsNewRetail) {
            ProductComment.AddToCommentEntity addToComment = item.addToComment;
            if (addToComment != null && !StringUtils.isEmpty(addToComment.commentDate)) {

                helper.setVisible(R.id.layoutCommentTwo, true);

                String addCommentDay = addToComment.day == 0 ? "用户当天追评" : String.format("购买%d天后追评", addToComment.day);
                Spanned storeReply = Html.fromHtml(String.format(mHtmlStoreFormat, addToComment.reply));
                Date dateTwo = TimeUtils.string2Date(addToComment.commentDate);
                String timeTwo = TimeUtils.date2String(dateTwo, "yyyy-MM-dd HH:mm");

                helper.setText(R.id.tvReplyTimeTwo, addCommentDay)
                        .setText(R.id.tvTimeTwo, timeTwo)
                        .setText(R.id.tvContentTwo, addToComment.content)
                        .setText(R.id.tvReplyTwo, storeReply);

//                BGANinePhotoLayout layoutImagesTwo = helper.getView(R.id.layoutCommentNineImagesTwo);
//                setImagePhone(layoutImagesTwo, addToComment.images);
                helper.setVisible(R.id.tvReplyTwo, !StringUtils.isEmpty(addToComment.reply));
            } else {
                helper.setVisible(R.id.layoutCommentTwo, false);
            }
        } else {
            ProductComment.AddToCommentEntity addToComment = item.addToComment;
            if (addToComment != null && !StringUtils.isEmpty(addToComment.commentDate)) {

                helper.setVisible(R.id.layoutCommentTwo, true);

                String addCommentDay = addToComment.day == 0 ? "用户当天追评" : String.format("购买%d天后追评", addToComment.day);
                Spanned storeReply = Html.fromHtml(String.format(mHtmlStoreFormat, addToComment.reply));
                Date dateTwo = TimeUtils.string2Date(addToComment.commentDate);
                String timeTwo = TimeUtils.date2String(dateTwo, "yyyy-MM-dd HH:mm");

                helper.setText(R.id.tvReplyTimeTwo, addCommentDay)
                        .setText(R.id.tvTimeTwo, timeTwo)
                        .setText(R.id.tvContentTwo, addToComment.content)
                        .setText(R.id.tvReplyTwo, storeReply);

//                BGANinePhotoLayout layoutImagesTwo = helper.getView(R.id.layoutCommentNineImagesTwo);
//                setImagePhone(layoutImagesTwo, addToComment.images);
                helper.setVisible(R.id.tvReplyTwo, !StringUtils.isEmpty(addToComment.reply));

            } else {
                helper.setVisible(R.id.layoutCommentTwo, false);
            }
        }

    }

    private void setImagePhone(BGANinePhotoLayout layoutImagesOne, final List<String> images) {
        if (images == null || images.size() == 0) {
            layoutImagesOne.setVisibility(View.GONE);
        } else {
            layoutImagesOne.setVisibility(View.VISIBLE);
            layoutImagesOne.setData((ArrayList<String>) images);
            layoutImagesOne.setDelegate(new BGANinePhotoLayout.Delegate() {
                @Override
                public void onClickNinePhotoItem(BGANinePhotoLayout ninePhotoLayout, View view, int position, String model, List<String> models) {
                    ImageUtil.previewImage(ninePhotoLayout.getContext(), (ArrayList<String>) images, position, false);
                }
            });
        }
    }

    private String getTimeOne(ProductComment item) {
        Date dateOne = TimeUtils.string2Date(item.commentDate);
        String pattern = "yyyy-MM-dd HH:mm  ";
        return TimeUtils.date2String(dateOne, pattern) + item.properties;
    }

    private void setUserInfo(BaseViewHolder helper, ProductComment item) {
        FrescoUtil.loadRvItemImg(helper, R.id.ivAvatarOne, item.headImage);
        helper.setText(R.id.tvNameOne, item.nickName);

        ImageView star0 = helper.getView(R.id.star0Iv);
        ImageView star1 = helper.getView(R.id.star1Iv);
        ImageView star2 = helper.getView(R.id.star2Iv);
        ImageView star3 = helper.getView(R.id.star3Iv);
        ImageView star4 = helper.getView(R.id.star4Iv);
        List<ImageView> imageViews = Arrays.asList(star0, star1, star2, star3, star4);
        int size = imageViews.size();
        if (mIsNewRetail) {
            int score = (item.descScore + item.expressScore + item.serveScore) / 3;
            for (int i = 0; i < score / 10; i++) {
                if (i < size)
                    imageViews.get(i).setSelected(true);
            }
        } else {
            for (int i = 0; i < item.descScore / 10; i++) {
                if (i < size)
                    imageViews.get(i).setSelected(true);
            }
        }
    }
}
