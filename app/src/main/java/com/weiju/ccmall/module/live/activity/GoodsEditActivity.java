package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.category.CategoryFragment;
import com.weiju.ccmall.module.live.adapter.SpecAdapter;
import com.weiju.ccmall.module.live.consts.ConstantKey;
import com.weiju.ccmall.module.live.entity.SpecEntity;
import com.weiju.ccmall.module.live.entity.UploadProductEntity;
import com.weiju.ccmall.module.live.widgets.HeightRecyclerView;
import com.weiju.ccmall.shared.basic.AgentFActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Category;
import com.weiju.ccmall.shared.bean.UploadResponse;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.common.ImageUploadAdapter;
import com.weiju.ccmall.shared.component.dialog.SettingSpecDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.manager.UploadManager;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.zhihu.matisse.Matisse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GoodsEditActivity extends BaseActivity {

    @BindView(R.id.rvHeader)
    RecyclerView mRvHeader;
    @BindView(R.id.etTitle)
    EditText mEtTitle;
    @BindView(R.id.etGoodsClassify)
    TextView mEtGoodsClassify;
    @BindView(R.id.ivPostage)
    ImageView mIvPostage;
    @BindView(R.id.etNoPostageNumber)
    EditText mEtNoPostageNumber;
    @BindView(R.id.etColor)
    EditText mEtColor;
    @BindView(R.id.etSize)
    EditText mEtSize;
    @BindView(R.id.llVolumeSet)
    LinearLayout mLlVolumeSet;
    @BindView(R.id.rvSpec)
    HeightRecyclerView mRvSpec;
    @BindView(R.id.etCommissionRate)
    EditText mEtCommissionRate;
    @BindView(R.id.tvCommissionRange)
    TextView mTvCommissionRange;
    @BindView(R.id.tvPlatformCommission)
    TextView mTvPlatformCommission;
    @BindView(R.id.tvTotalCommissionRange)
    TextView mTvTotalCommissionRange;
    @BindView(R.id.rvFooter)
    RecyclerView mRvFooter;
    @BindView(R.id.tvUpload)
    TextView mTvUpload;
    @BindView(R.id.tvUnifiedFreight)
    TextView mTvUnifiedFreight;
    @BindView(R.id.tvVolumeSet)
    TextView mTvVolumeSet;
    @BindView(R.id.llCommissionDetail)
    LinearLayout mLlCommissionDetail;
    @BindView(R.id.llFullNoPostage)
    LinearLayout mLlFullNoPostage;
    @BindView(R.id.tvCommissionRateTips)
    TextView mTvCommissionRateTips;
    @BindView(R.id.tvPlatformComRatio)
    TextView mTvPlatformComRatio;

    private static final int REQUEST_CODE_CHOOSE_PHOTO_HEADER = 1;
    private static final int REQUEST_CODE_CHOOSE_PHOTO_FOOTER = 2;
    private ImageUploadAdapter mImageUploadHeaderAdapter;
    private ImageUploadAdapter mImageUploadFooterAdapter;
    private SpecAdapter mSpecAdapter;
    private int maxLength = 5;
    private int spanCount = 4;
    private String[] colors = new String[0];
    private String[] sizes = new String[0];
    private List<SpecEntity> mSpecEntities = new ArrayList<>();
    ILiveStoreService iLiveStoreService;
    private Category mCategory;
    private UploadProductEntity mProductEntity;
    private SettingSpecDialog mSpecDialog;
    private int platformComRatio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_edit);
        ButterKnife.bind(this);
        setTitle("直播产品");
        setLeftBlack();
        iLiveStoreService = ServiceManager.getInstance().createService(ILiveStoreService.class);
        mProductEntity = (UploadProductEntity) getIntent().getSerializableExtra("productEntity");
        initView();
        if (mProductEntity != null) {
            initProduct();
        } else {
            getComRatioSectionInfo();
        }
    }

    private void getComRatioSectionInfo() {
        APIManager.startRequest(iLiveStoreService.getComRatioSectionInfo(),
                    new BaseRequestListener<UploadProductEntity>(this) {
                        @Override
                        public void onSuccess(UploadProductEntity result) {
                            super.onSuccess(result);
                            mTvCommissionRateTips.setText(result.info);
                            platformComRatio = result.platformComRatio;
                            mTvPlatformComRatio.setText("平台佣金(" + result.platformComRatio + "%)");
                        }

                        @Override
                        public void onError(Throwable e) {
                            super.onError(e);
                            ToastUtil.error(e.getMessage());
                            finish();
                        }
                    }, this);
    }

    private void initView() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, spanCount);
        gridLayoutManager.setAutoMeasureEnabled(true);
        gridLayoutManager.setSmoothScrollbarEnabled(false);

        mImageUploadHeaderAdapter = new ImageUploadAdapter(this, maxLength);
        mRvHeader.setLayoutManager(gridLayoutManager);
        mRvHeader.setAdapter(mImageUploadHeaderAdapter);
        mImageUploadHeaderAdapter.setOnItemClickListener(position -> {
            EventBus.getDefault().post(new EventMessage(Event.selectImage2UploadHeader));
        });

        GridLayoutManager gridLayoutManagerF = new GridLayoutManager(this, spanCount);
        gridLayoutManagerF.setAutoMeasureEnabled(true);
        gridLayoutManagerF.setSmoothScrollbarEnabled(false);
        mImageUploadFooterAdapter = new ImageUploadAdapter(this, maxLength);
        mRvFooter.setLayoutManager(gridLayoutManagerF);
        mRvFooter.setAdapter(mImageUploadFooterAdapter);
        mImageUploadFooterAdapter.setOnItemClickListener(position -> {
            EventBus.getDefault().post(new EventMessage(Event.selectImage2UploadFooter));
        });

        initSpec();
        mEtCommissionRate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String s1 = s.toString();
                initCommissionRange(s1);

            }
        });
    }

    private void initSpec() {
        mSpecAdapter = new SpecAdapter(mSpecEntities);
        mRvSpec.setLayoutManager(new LinearLayoutManager(this));
        mRvSpec.addItemDecoration(new ListDividerDecoration(this));
        mRvSpec.setAdapter(mSpecAdapter);
        mRvSpec.setNestedScrollingEnabled(false);
        mEtColor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String color = s.toString();
                if (TextUtils.isEmpty(color)) colors = new String[0];
                else {
                    String replace = color.replace("；", ";");
                    colors = replace.split(";");
                }
                initSpecData();
                initCommissionRange(mEtCommissionRate.getText().toString());
            }
        });
        mEtSize.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String size = s.toString();
                if (TextUtils.isEmpty(size)) sizes = new String[0];
                else {
                    String replace = size.replace("；", ";");
                    sizes = replace.split(";");
                }
                initSpecData();
                initCommissionRange(mEtCommissionRate.getText().toString());
            }
        });
        mSpecAdapter.setOnEditTextAfterTextChangedListener(s -> {
            initCommissionRange(mEtCommissionRate.getText().toString());
        });
    }

    private void initSpecData() {
        mSpecEntities.clear();
        if (colors.length > 0 || sizes.length > 0) {
            mLlVolumeSet.setVisibility(View.VISIBLE);
        } else {
            mLlVolumeSet.setVisibility(View.GONE);
        }
        if (colors.length > 0) {
            for (String color : colors) {
                if (sizes.length > 0) {
                    for (String size : sizes) {
                        SpecEntity specEntity = new SpecEntity();
                        specEntity.setTitle(color + "+" + size);
                        mSpecEntities.add(specEntity);
                    }
                } else {
                    SpecEntity specEntity = new SpecEntity();
                    specEntity.setTitle(color);
                    mSpecEntities.add(specEntity);
                }
            }
        } else {
            for (String size : sizes) {
                SpecEntity specEntity = new SpecEntity();
                specEntity.setTitle(size);
                mSpecEntities.add(specEntity);
            }
        }
        /*if (colors.length > 0) {
            mLlVolumeSet.setVisibility(View.VISIBLE);
            for (String color : colors) {
                SpecEntity specEntity = new SpecEntity();
                specEntity.setTitle(color);
                mSpecEntities.add(specEntity);
            }
        } else {
            mLlVolumeSet.setVisibility(View.GONE);
        }*/

        mSpecAdapter.setNewData(mSpecEntities);
//        mSpecAdapter.notifyDataSetChanged();
    }


    private void initCommissionRange(String s) {
        if (!TextUtils.isEmpty(s)) {
            int range = Integer.parseInt(s);
            List<SpecEntity> list = mSpecAdapter.getData();
            boolean isHasSizeNull = true;
            for (SpecEntity specEntity : list) {
                if (specEntity.getRetailPrice() == null || specEntity.getMarketPrice() == null
                        || TextUtils.isEmpty(specEntity.getStock()) || TextUtils.isEmpty(specEntity.getWeight())) {
                    isHasSizeNull = true;
                    break;
                } else {
                    isHasSizeNull = false;
                }
            }
//            if (range >= 10 && range <= 90 && !isHasSizeNull) {
            if (!isHasSizeNull) {
                Collections.sort(list, (o1, o2) -> o1.getRetailPrice() > o2.getRetailPrice() ? 1 : -1);
                if (list.size() > 0) {
                    mLlCommissionDetail.setVisibility(View.VISIBLE);
                    SpecEntity specS = list.get(0);
                    SpecEntity specE = list.get(list.size() - 1);
                    double commissionStart = format(specS.getRetailPrice() * range/10000d, false);
                    double commissionEnd = format(specE.getRetailPrice() * range/10000d, false);

                    double platformCommissionStart = format(specS.getRetailPrice() * platformComRatio/10000d, true);
                    double platformCommissionEnd = format(specE.getRetailPrice() * platformComRatio/10000d, true);

                    long totalCommissionStart = MoneyUtil.yuanToCentLong(commissionStart) + MoneyUtil.yuanToCentLong(platformCommissionStart);
                    long totalCommissionEnd = MoneyUtil.yuanToCentLong(commissionEnd) + MoneyUtil.yuanToCentLong(platformCommissionEnd);

                    String commissionRange = commissionStart == commissionEnd ? String.format("%s元", (commissionStart)) :
                            String.format("%s~%s元", (commissionStart), (commissionEnd));

                    String platformCommission = platformCommissionStart == platformCommissionEnd ? String.format("%s元", (platformCommissionStart)) :
                            String.format("%s~%s元", (platformCommissionStart), (platformCommissionEnd));

                    String totalCommissionRange = totalCommissionStart == totalCommissionEnd ? String.format("%s元", MoneyUtil.centToYuanStr(totalCommissionStart)) :
                            String.format("%s~%s元", MoneyUtil.centToYuanStr(totalCommissionStart), MoneyUtil.centToYuanStr(totalCommissionEnd));

                    mTvCommissionRange.setText(commissionRange);
                    mTvPlatformCommission.setText(platformCommission);
                    mTvTotalCommissionRange.setText(totalCommissionRange);

                }
            } else {
                mLlCommissionDetail.setVisibility(View.GONE);
            }
        }
    }

    private void initProduct() {
        mImageUploadHeaderAdapter.addItems(mProductEntity.bannerImage);
        mEtTitle.setText(mProductEntity.productName);
        mEtGoodsClassify.setText(mProductEntity.categoryName);
        if (mProductEntity.isShippingFree == 1) {
            mIvPostage.setSelected(true);
            mTvUnifiedFreight.setVisibility(View.GONE);
            mLlFullNoPostage.setVisibility(View.GONE);
        } else {
            mIvPostage.setSelected(false);
            mTvUnifiedFreight.setVisibility(View.VISIBLE);
            mLlFullNoPostage.setVisibility(View.VISIBLE);
            mEtNoPostageNumber.setText(mProductEntity.freeQuantity);
        }
        mEtColor.setFocusable(false);
        mEtColor.setFocusableInTouchMode(false);
        mEtColor.setText(mProductEntity.color);
        mEtSize.setFocusable(false);
        mEtSize.setFocusableInTouchMode(false);
        mEtSize.setText(mProductEntity.size);
        mLlVolumeSet.setVisibility(View.VISIBLE);
        mSpecEntities = mProductEntity.skuBeans;
        mSpecAdapter.setNewData(mSpecEntities);
        mEtCommissionRate.setText(mProductEntity.comRatio);

        mTvCommissionRateTips.setText(mProductEntity.info);
        platformComRatio = mProductEntity.platformComRatio;
        mTvPlatformComRatio.setText("平台佣金(" + mProductEntity.platformComRatio + "%)");

        initCommissionRange(mEtCommissionRate.getText().toString());
        mImageUploadFooterAdapter.addItems(mProductEntity.detailImage);
    }

    @OnClick(R.id.tvUpload)
    public void onUpload() {
//        StringBuilder urlsHeader = new StringBuilder();
        List<String> urlsHeader = new ArrayList<>();
        for (int i = 0; i < mImageUploadHeaderAdapter.getItemCount(); i++) {
            if (mImageUploadHeaderAdapter.getItemViewType(i) == 1) {
//                urlsHeader.append(";").append(mImageUploadHeaderAdapter.getItems().get(i));
                urlsHeader.add(mImageUploadHeaderAdapter.getItems().get(i));
            }
        }

        if (urlsHeader.size() < 1) {
            ToastUtil.error("请上传产品主图");
            return;
        }
        String productName = mEtTitle.getText().toString();
        if (TextUtils.isEmpty(productName)) {
            ToastUtil.error("请输入产品标题");
            return;
        }
        String goodsClassify = mEtGoodsClassify.getText().toString();
        if (TextUtils.isEmpty(goodsClassify)) {
            ToastUtil.error("请选择产品分类");
            return;
        }

        String freeQuantity = mEtNoPostageNumber.getText().toString();
//        if (!mIvPostage.isSelected() && (TextUtils.isEmpty(freeQuantity) || Integer.parseInt(freeQuantity) < 2)) {
//            ToastUtil.error("满件包邮选项输入的数字需≥2");
//            return;
//        }

        String color = mEtColor.getText().toString();
        String size = mEtSize.getText().toString();
        if (TextUtils.isEmpty(color) && TextUtils.isEmpty(size)) {
            ToastUtil.error("请输入颜色或尺码");
            return;
        }
//        if (TextUtils.isEmpty(color)) {
//            ToastUtil.error("请输入规格");
//            return;
//        }

        boolean isHasSizeNull = true;
        for (SpecEntity specEntity : mSpecAdapter.getData()) {
            if (specEntity.getRetailPrice() == null || specEntity.getMarketPrice() == null
                    || TextUtils.isEmpty(specEntity.getStock()) || TextUtils.isEmpty(specEntity.getWeight())) {
                isHasSizeNull = true;
                break;
            } else {
                isHasSizeNull = false;
            }
        }

        if (isHasSizeNull) {
            ToastUtil.error("请输入完整的规格信息");
            return;
        }

        String commissionRate = mEtCommissionRate.getText().toString();
        if (TextUtils.isEmpty(commissionRate)) {
            ToastUtil.error("请输入推广佣金比例");
            return;
        } else {
            /*int range = Integer.parseInt(commissionRate);
            if (range < 10 || range > 90) {
                ToastUtil.error("推广佣金比例可填区间为10%~90%");
                return;
            }*/
        }

//        StringBuilder urlsFooter = new StringBuilder();
        List<String> urlsFooter = new ArrayList<>();
        for (int i = 0; i < mImageUploadFooterAdapter.getItemCount(); i++) {
            if (mImageUploadFooterAdapter.getItemViewType(i) == 1) {
//                urlsFooter.append(";").append(mImageUploadFooterAdapter.getItems().get(i));
                urlsFooter.add(mImageUploadFooterAdapter.getItems().get(i));
            }
        }
        if (urlsFooter.size() < 1) {
            ToastUtil.error("请上传产品主图");
            return;
        }
        UploadProductEntity entity = new UploadProductEntity();
        if (mProductEntity != null) {
            entity.productId = mProductEntity.productId;
        }
        entity.productName = productName;
        if (mCategory != null) {
            entity.categoryId = mCategory.id;
        } else if (mProductEntity != null) {
            entity.categoryId = mProductEntity.categoryId;
        }
        entity.bannerImage = urlsHeader;
//        if (!TextUtils.isEmpty(color)) entity.specs = color;
        if (!TextUtils.isEmpty(color))entity.color = color;
        if (!TextUtils.isEmpty(size)) entity.size = size;
        entity.skuBeans = mSpecAdapter.getData();
        entity.detailImage = urlsFooter;
        entity.comRatio = commissionRate;

        if (!TextUtils.isEmpty(freeQuantity) && !mIvPostage.isSelected()) {
            entity.freeQuantity = freeQuantity;
        }
        entity.isShippingFree = mIvPostage.isSelected() ? 1 : 0;
        APIManager.startRequest(mProductEntity == null ? iLiveStoreService.saveProduct(entity) : iLiveStoreService.updateProduct(entity),
                new BaseRequestListener<Object>(this) {
                    @Override
                    public void onSuccess(Object result) {
                        super.onSuccess(result);
                        EventBus.getDefault().post(new EventMessage(Event.liveStoreProductUpdate));
                        finish();
                    }
                }, this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_CHOOSE_PHOTO_HEADER:
                case REQUEST_CODE_CHOOSE_PHOTO_FOOTER:
                    List<Uri> uris = Matisse.obtainResult(data);
                    LogUtils.e(requestCode + "拿到图片" + uris.get(0).getPath());
                    uploadImage(uris.get(0), requestCode);
                    break;
            }
        }
    }

    private void uploadImage(final Uri uri, int requestCode) {
        UploadManager.uploadImage(this, uri, new BaseRequestListener<UploadResponse>(this) {
            @Override
            public void onSuccess(UploadResponse result) {
                switch (requestCode) {
                    case REQUEST_CODE_CHOOSE_PHOTO_HEADER:
                        mImageUploadHeaderAdapter.addItem(result.url);
                        break;
                    case REQUEST_CODE_CHOOSE_PHOTO_FOOTER:
                        mImageUploadFooterAdapter.addItem(result.url);
                        break;
                }
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void imageUploadHandler(EventMessage message) {
        if (message.getEvent().equals(Event.selectImage2UploadHeader)) {
            UploadManager.selectImage(this, REQUEST_CODE_CHOOSE_PHOTO_HEADER, 1);
        }
        if (message.getEvent().equals(Event.selectImage2UploadFooter)) {
            UploadManager.selectImage(this, REQUEST_CODE_CHOOSE_PHOTO_FOOTER, 1);
        }
        if (message.getEvent() == Event.selectClassify) {
            mCategory = (Category) message.getData();
            mEtGoodsClassify.setText(mCategory.name);
        }
    }

    @OnClick({R.id.etGoodsClassify, R.id.tvUnifiedFreight, R.id.ivPostage, R.id.tvVolumeSet})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etGoodsClassify:
                Bundle bundle = new Bundle();
                bundle.putBoolean(ConstantKey.KEY_IS_ONLY_SELECT_CLASSIFY, true);
                AgentFActivity.Companion.start(this, CategoryFragment.class, bundle);
                break;
            case R.id.tvUnifiedFreight:
                StoreFreightActivity.start(this, false);
                break;
            case R.id.ivPostage:
                mIvPostage.setSelected(!mIvPostage.isSelected());
                mTvUnifiedFreight.setVisibility(mIvPostage.isSelected() ? View.GONE : View.VISIBLE);
                mLlFullNoPostage.setVisibility(mIvPostage.isSelected() ? View.GONE : View.VISIBLE);
                break;
            case R.id.tvVolumeSet:
                if (mSpecDialog == null) {
                    mSpecDialog = new SettingSpecDialog(this);
                    mSpecDialog.setOnConfirmListener(entity -> {
                        List<SpecEntity> specList = new ArrayList<>();
                        for (SpecEntity specEntity : mSpecEntities) {
                            specEntity.setRetailPrice(entity.getRetailPrice() != null ? MoneyUtil.centToYuanStr(entity.getRetailPrice()) : null);
                            specEntity.setMarketPrice(entity.getMarketPrice() != null ? MoneyUtil.centToYuanStr(entity.getMarketPrice()) : null);
                            specEntity.setStock(entity.getStock());
                            specEntity.setWeight(entity.getWeight());
                            specList.add(specEntity);
                        }
                        mSpecAdapter.setNewData(specList);
                        initCommissionRange(mEtCommissionRate.getText().toString());
                    });
                }
                mSpecDialog.show();
                break;
        }
    }

    public double format(double d, boolean isUp) {
        String s = String.valueOf(d);
        String substring = s.substring(s.indexOf("."));
        if (substring.length() > 3 && isUp) {
            d += 0.005;
        }
        BigDecimal b = new BigDecimal(d);
        return b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, GoodsEditActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, UploadProductEntity productEntity) {
        Intent intent = new Intent(context, GoodsEditActivity.class);
        intent.putExtra("productEntity", productEntity);
        context.startActivity(intent);
    }
}
