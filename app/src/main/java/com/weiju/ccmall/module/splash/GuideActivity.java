package com.weiju.ccmall.module.splash;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.blankj.utilcode.utils.SPUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.MainActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.basic.BaseSubscriber;
import com.weiju.ccmall.shared.bean.GuideModel;
import com.weiju.ccmall.shared.bean.Splash;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IAdService;
import com.weiju.ccmall.shared.util.EventUtil;
import com.zjm.zviewlibrary.splash.view.BaseSkipTextView;
import com.zjm.zviewlibrary.splash.view.CountDownTextViewView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class GuideActivity extends BaseActivity {

    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    @BindView(R.id.ivAd)
    ImageView mIvAd;
    private IAdService mAdService;
    private boolean isGoMain = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        QMUIStatusBarHelper.translucent(this, 0);
        setContentView(R.layout.activity_guide);
        ButterKnife.bind(this);

        initData();
    }

    private void initData() {
        mAdService = ServiceManager.getInstance().createService(IAdService.class);
        SPUtils spUtils = new SPUtils(GuideActivity.class.getName());
        boolean ontStart = spUtils.getBoolean("ontStart", false);
        if (!ontStart) {
            // 引导页
            getGuideData();
        } else {
            // 闪屏页
            initSplash();
        }
    }

    private void getGuideData() {
        ServiceManager.getInstance().createService(IAdService.class);
        APIManager.startRequest(
                mAdService.getGuide(),
                new BaseRequestListener<GuideModel>() {
                    @Override
                    public void onSuccess(GuideModel result) {
                        super.onSuccess(result);
                        if (result.images == null || result.images.size() == 0) {
                            initSplash();
                        } else {
                            setViewPager(result);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        initSplash();
                    }
                }, this
        );
    }

    private void setViewPager(GuideModel result) {
        final ArrayList<BaseFragment> fragments = new ArrayList<>();
        final LinearLayout pointLayout = new LinearLayout(this);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(-2, -2);
        lp.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
        lp.bottomMargin = getPercentValue(20);
        pointLayout.setLayoutParams(lp);
        int i = 0;
        for (String image : result.images) {
            fragments.add(GuideFragment.newInstance(image, i == result.images.size() - 1));
            View v = new View(this);
            int size = getPercentValue(20);
            int m = getPercentValue(11);
            LinearLayout.LayoutParams l = new LinearLayout.LayoutParams(size, size);
            l.leftMargin = m;
            l.rightMargin = m;
            l.weight = 1f;
            v.setLayoutParams(l);
            v.setBackground(getResources().getDrawable(R.drawable.bg_point));
            if (i == 0) {
                v.setSelected(true);
            }
            pointLayout.addView(v);
            i++;
        }

        final ViewGroup v = findViewById(R.id.guide);
        v.addView(pointLayout);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            private int p = 0;

            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                if (p != i) {
                    pointLayout.getChildAt(p).setSelected(false);
                    pointLayout.getChildAt(i).setSelected(true);
                    p = i;
                    if (i == pointLayout.getChildCount() - 1) {
                        pointLayout.setVisibility(View.GONE);
                    } else {
                        pointLayout.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        FragmentPagerAdapter fragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public android.support.v4.app.Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }
        };
        mViewPager.setAdapter(fragmentPagerAdapter);
        mViewPager.setOffscreenPageLimit(fragments.size());
    }

    private void initSplash() {
        final SharedPreferences sp = getSharedPreferences("splash", 0);
        final Gson gson = new Gson();
        final ViewGroup v = findViewById(R.id.guide);
        final String s = sp.getString("Splash", null);
        final Splash bean;
        if (s != null) {
            bean = gson.fromJson(s, Splash.class);
            setBackground(v, bean, false);
        } else {
            bean = null;
        }

        execute(mAdService.getSplashAd(), new BaseSubscriber<Splash>() {
            @Override
            public void onNext(@NonNull Splash splash) {
                super.onNext(splash);
                sp.edit().putString("Splash", gson.toJson(splash)).commit();
                setBackground(v, splash, true);
            }

            @Override
            public void onError(Throwable e) {
                String event = null;
                String target = null;
                if (bean != null) {
                    event = bean.event;
                    target = bean.target;
                }
                showTimeView(event, target);
                super.onError(e);
            }
        });
    }

    private void showTimeView(final String event, final String tag) {
        CountDownTextViewView count = new CountDownTextViewView(this);
        count.startCountDown(5);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-2, -2);
        int width = getResources().getDisplayMetrics().widthPixels;
        int marin = getPercentValue(48);
        layoutParams.topMargin = marin;
        layoutParams.leftMargin = width - marin * 4;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            layoutParams.topMargin += QMUIStatusBarHelper.getStatusbarHeight(this);
        }
        count.setLayoutParams(layoutParams);
        count.setListener(new BaseSkipTextView.OnCountDownListener() {
            @Override
            public void onComplete() {
                if (isGoMain) {
                    isGoMain = false;
                    startActivity(new Intent(GuideActivity.this, MainActivity.class));
                    finish();
                }

            }
        });
        ViewGroup v = findViewById(R.id.guide);
        v.addView(count);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventUtil.compileEvent(GuideActivity.this, event, tag, false);
            }
        });
    }

    private void setBackground(final ViewGroup v, final Splash splash, final boolean isShowTime) {
        if (!isDestroyed()) {

            Glide.with(this).load(splash.backUrl).into(new SimpleTarget<Drawable>() {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    if (resource != null) {
                        for (int i = 0; i < v.getChildCount(); i++) {
                            v.getChildAt(i).setVisibility(View.GONE);
//                        v.setBackgroundDrawable(resource);
                            mIvAd.setVisibility(View.VISIBLE);
                            Glide.with(GuideActivity.this).load(splash.backUrl).into(mIvAd);
                        }
                        if (isShowTime) {
                            showTimeView(splash.event, splash.target);
                        }
                    }
                }

                @Override
                public void onLoadFailed(@Nullable Drawable errorDrawable) {
                    if (isShowTime) {
                        showTimeView(splash.event, splash.target);
                    }
                }
            });

            /*Glide.with(GuideActivity.this).load(splash.backUrl).into(new SimpleTarget<GlideDrawable>() {
                @Override
                public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> glideAnimation) {
                    for (int i = 0; i < v.getChildCount(); i++) {
                        v.getChildAt(i).setVisibility(View.GONE);
//                        v.setBackgroundDrawable(resource);
                        mIvAd.setVisibility(View.VISIBLE);
                        Glide.with(GuideActivity.this).load(splash.backUrl).into(mIvAd);
                    }
                    if (isShowTime) {
                        showTimeView(splash.event, splash.target);
                    }
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    if (isShowTime) {
                        showTimeView(splash.event, splash.target);
                    }
                }
            });*/
        }


    }

    private int getPercentValue(int value) {
        int width = getResources().getDisplayMetrics().widthPixels;
        float s = width / 1080f;
        return (int) (value * s);
    }
}
