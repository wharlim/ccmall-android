package com.weiju.ccmall.module.blockchain.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.beans.TemplateItem;
import com.weiju.ccmall.module.blockchain.db.TemplateDBO;
import com.weiju.ccmall.module.blockchain.utils.TemplateWebUtil;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBlockChain;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class TemplateDownloadServices extends Service {

    private static final String TAG = "TemplateDownloadService";

    public static final String KEY_COMMAND = "key_command";
    public static final int COMMAND_UPDATE_TEMPLATE = 1;

    private static final String CHANNEL_ID = "Download_Template";

    private IBlockChain blockChain;
    private TemplateDBO dbo;
    private File downloadDir;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        blockChain = ServiceManager.getInstance().createService(IBlockChain.class);
        dbo = new TemplateDBO(this);
        downloadDir = TemplateWebUtil.getDownloadDir(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mNotificationChannel = new NotificationChannel(CHANNEL_ID, "下载模板", NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationChannel.setSound(null, null);
            mNotificationChannel.enableVibration(false);
            mNotificationChannel.setDescription("下载模板通知");
            getNotificationManager().createNotificationChannel(mNotificationChannel);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
            builder.setContentTitle(getResources().getString(R.string.appName));
            builder.setContentText("正在更新模板...");

            builder.setDefaults(Notification.DEFAULT_ALL);
            builder.setAutoCancel(true);
            builder.setShowWhen(true);
            builder.setSmallIcon(R.drawable.logo);

            NotificationManagerCompat.from(this).notify(1, builder.build());
            startForeground(1, builder.build());
        }
    }

    private NotificationManager getNotificationManager() {
        return (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dbo.close();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int command = intent.getIntExtra(KEY_COMMAND, -1);
        switch (command) {
            case COMMAND_UPDATE_TEMPLATE:
                updateTemplate();
                break;
        }
        return START_NOT_STICKY;
    }

    private void updateTemplate() {
        APIManager.startRequest(blockChain.getAppTemplateConfigList(), new BaseRequestListener<List<TemplateItem>>() {
            @Override
            public void onSuccess(List<TemplateItem> result) {
                super.onSuccess(result);
                if (result != null && !result.isEmpty()) {
                    new HandleTemplateTask().execute(result);
                }
            }

            @Override
            public void onComplete() {
                super.onComplete();
            }
        }, this);
    }

    class HandleTemplateTask extends AsyncTask<List<TemplateItem>, Void, Void> {

        @Override
        protected Void doInBackground(List<TemplateItem>... templateItems) {
            if (dbo.isClosed()) {
                Log.d(TAG, "Service was terminated...");
                return null;
            }
            if (templateItems.length > 0) {
                // 更新本地数据库
                Log.d(TAG, "Start update template information...");
                List<TemplateItem> remoteItems = templateItems[0];
                for (TemplateItem remoteItem: remoteItems) {
                    TemplateItem localItem = dbo.query(remoteItem.id);
                    if (localItem == null) {
                        dbo.insert(remoteItem);
                        Log.d(TAG, "template " + remoteItem.templateId + " inserted");
                    } else {
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        String localUpdateDate = localItem.updateDate;
                        String remoteUpdateDate = remoteItem.updateDate;
                        try {
                            Date localDate = dateFormat.parse(localUpdateDate);
                            Date remoteDate = dateFormat.parse(remoteUpdateDate);
                            if (!localDate.equals(remoteDate)) {
                                dbo.update(remoteItem);
                                Log.d(TAG, "template " + remoteItem.templateId + " updated");
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                Log.d(TAG, "Update template information complete!");
                // 下载文件
                List<TemplateItem> localItems = dbo.queryByLocalState(TemplateDBO.LOCAL_STATE_READY);
                if (localItems != null && !localItems.isEmpty()) {
                    for (TemplateItem local: localItems) {
                        boolean downloaded = download(local);
                        if (downloaded) {
                            dbo.updateLocalState(local.id, TemplateDBO.LOCAL_STATE_DOWNLOADED);
                        }
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            stopForeground(true);
            stopSelf();
        }
    }

    private boolean download(TemplateItem item) {
        try {
            Log.d(TAG, "Start download template " + item.templateId + "," + item.fileUrl);
            Response<ResponseBody> response = blockChain.download(item.fileUrl).execute();
            InputStream is = response.body().byteStream();
            ZipInputStream zipInputStream = new ZipInputStream(is);
            unzip(zipInputStream, new File(downloadDir, item.templateId));
            Log.d(TAG, "Download template " + item.templateId + " success!");
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Download template " + item.templateId + " fail.");
            return false;
        }
    }

    private static void unzip(ZipInputStream in, File desFile)
            throws IOException {
        deleteDir(desFile);
        ZipEntry entry = null;
        byte[] bts = new byte[1024];
        int num = 0;
        while ((entry = in.getNextEntry()) != null) {
            if (!entry.isDirectory()) {
                File file = new File(desFile.getAbsoluteFile() + File.separator + entry.getName());
                if (!file.exists()) {
                    File parentFile = file.getParentFile();
                    parentFile.mkdirs();
                    file.createNewFile();
                }
                FileOutputStream fos = new FileOutputStream(file);
                while ((num = in.read(bts)) != -1) {
                    fos.write(bts, 0, num);
                }
                fos.close();
                in.closeEntry();
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir.isDirectory()) {
            for (File file: dir.listFiles()) {
                deleteDir(file);
            }
        }
        return dir.delete();
    }

    public static void start(Context context) {
        try {
            Intent intent = new Intent(context, TemplateDownloadServices.class);
            intent.putExtra(KEY_COMMAND, COMMAND_UPDATE_TEMPLATE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(intent);
            } else {
                context.startService(intent);
            }
        } catch (Exception e) {
            Log.d(TAG, "Start service fail: " + e.getMessage());
        }
    }
}
