package com.weiju.ccmall.module.world.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/5/8.
 */
public class CreateWxPayEntity {
    /**
     * request : {"merchantOrderNo":"2311589152473200","actualPrice":"0.01","productName":"CCMALL"}
     * sourid : gh_8e510fbec2c9
     * actualPrice : 0.01
     * appid : wx348c94fdbe2528a1
     * sign : 0c0d6128303be3b204931a56879b01ed
     * merchantOrderNo : 2311589152473200
     * shopKey : CCMALL-GPSL
     * url : /pages/pay/index
     * productName : CCMALL
     */

    @SerializedName("request")
    public String request;
    @SerializedName("sourid")
    public String sourid;
    @SerializedName("actualPrice")
    public String actualPrice;
    @SerializedName("appid")
    public String appid;
    @SerializedName("sign")
    public String sign;
    @SerializedName("merchantOrderNo")
    public String merchantOrderNo;
    @SerializedName("shopKey")
    public String shopKey;
    @SerializedName("url")
    public String url;
    @SerializedName("productName")
    public String productName;
    @SerializedName("debug")
    public int debug;   //传0则为正式环境，传1代表测试环境

}
