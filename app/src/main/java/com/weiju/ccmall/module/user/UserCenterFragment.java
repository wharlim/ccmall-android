package com.weiju.ccmall.module.user;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.ToastUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.orhanobut.logger.Logger;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.AuthCardSuccssdGongCatActivity;
import com.weiju.ccmall.module.auth.AuthIdentitySuccssdActivity;
import com.weiju.ccmall.module.auth.AuthPhoneActivity;
import com.weiju.ccmall.module.auth.AuthPhoneActivity2;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.SubmitStatusActivity;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.auth.model.AuthModel;
import com.weiju.ccmall.module.auth.model.CardDetailModel;
import com.weiju.ccmall.module.balance.BalanceListActivity;
import com.weiju.ccmall.module.blockchain.CCMMainActivity;
import com.weiju.ccmall.module.blockchain.beans.BlockChainSwitch;
import com.weiju.ccmall.module.collect.CollectListActivity;
import com.weiju.ccmall.module.coupon.CouponListActivity;
import com.weiju.ccmall.module.foot.FootListActivity;
import com.weiju.ccmall.module.live.activity.LiveHomeActivity;
import com.weiju.ccmall.module.message.MessageListActivity;
import com.weiju.ccmall.module.notice.NoticeListActivity;
import com.weiju.ccmall.module.order.NewRefundsOrderListActivity;
import com.weiju.ccmall.module.order.OrderListActivity;
import com.weiju.ccmall.module.page.WebViewCommonActivity;
import com.weiju.ccmall.module.pickUp.activity.PickUpListActivity;
import com.weiju.ccmall.module.point.PointListActivity;
import com.weiju.ccmall.module.product.MyCommentListActivity;
import com.weiju.ccmall.module.product.NewerProductDetailActivity;
import com.weiju.ccmall.module.qrcode.QrCodeShowActivity;
import com.weiju.ccmall.module.user.model.SignModel;
import com.weiju.ccmall.module.user.model.UpMemberModel;
import com.weiju.ccmall.module.user.profit.activity.ProfitListActivity;
import com.weiju.ccmall.module.world.activity.WorldHomeActivity;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.newRetail.activity.HiGouDescriptionActivity;
import com.weiju.ccmall.newRetail.activity.HighGoActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.MyStatus;
import com.weiju.ccmall.shared.bean.OrderCount;
import com.weiju.ccmall.shared.bean.ProfitData;
import com.weiju.ccmall.shared.bean.ScoreStat;
import com.weiju.ccmall.shared.bean.Unused;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.ItemWithIcon;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.enums.AccountType;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.IBalanceService;
import com.weiju.ccmall.shared.service.contract.IBlockChain;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.CSUtils;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class UserCenterFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.avatarIv)
    protected SimpleDraweeView mAvatarIv;
    @BindView(R.id.nicknameTv)
    protected TextView mNicknameTv;
    @BindView(R.id.pointTv)
    protected TextView mPointTv;
    @BindView(R.id.balanceTv)
    protected TextView mBalanceTv;
    @BindView(R.id.orderUnpayCmp)
    protected ItemWithIcon mOrderUnpayCmp;
    @BindView(R.id.orderPaidCmp)
    protected ItemWithIcon mOrderPaidCmp;
    @BindView(R.id.orderDispatchedCmp)
    protected ItemWithIcon mOrderDispatchedCmp;
    @BindView(R.id.orderCommentCmp)
    protected ItemWithIcon mOrderCommentCmp;
    @BindView(R.id.orderServiceCmp)
    protected ItemWithIcon mOrderServiceCmp;
    @BindView(R.id.linkFamilyCmp)
    protected ItemWithIcon mLinkFamilyCmp;
    @BindView(R.id.linkQrCodeCmp)
    protected ItemWithIcon mLinkQrCodeCmp;
    @BindView(R.id.levelTv)
    protected TextView mLevelTv;
    @BindView(R.id.ivSign)
    ImageView mIvSign;
    @BindView(R.id.linkVerifyCmp)
    ItemWithIcon mLinkVerifyCmp;
    @BindView(R.id.linkLive)
    View linkLive;
    //    @BindView(R.id.linkBindCardCmp)
//    ItemWithIcon mLinkBindCardCmp;
    @BindView(R.id.layoutMoney)
    LinearLayout mLayoutMoney;
    @BindView(R.id.userHeaderLayout)
    RelativeLayout mUserHeaderLayout;
    @BindView(R.id.pointLayout)
    LinearLayout mPointLayout;
    @BindView(R.id.balanceLayout)
    LinearLayout mBalanceLayout;
    @BindView(R.id.viewMoreOrderLayout)
    LinearLayout mViewMoreOrderLayout;
    @BindView(R.id.linkCouponCmp)
    ItemWithIcon mLinkCouponCmp;
    @BindView(R.id.linkMessageCmp)
    ItemWithIcon mLinkMessageCmp;
    @BindView(R.id.linkFavCmp)
    ItemWithIcon mLinkFavCmp;
    @BindView(R.id.linkHistoryCmp)
    ItemWithIcon mLinkHistoryCmp;
    @BindView(R.id.linkSubscribeCmp)
    ItemWithIcon mLinkSubscribeCmp;
    @BindView(R.id.linkWorld)
    ItemWithIcon mLinkWorldCmp;
    @BindView(R.id.tvScore)
    TextView mTvScore;
    @BindView(R.id.layoutScore)
    LinearLayout mLayoutScore;
    @BindView(R.id.linkNotice)
    ItemWithIcon mLinkNotice;
    @BindView(R.id.ccmTv)
    TextView mCcmTv;
    @BindView(R.id.ccmLayout)
    LinearLayout mCcmLayout;
    @BindView(R.id.ticketTv)
    TextView mTicketTv;
    @BindView(R.id.ticketLayout)
    LinearLayout mTicketLayout;
    @BindView(R.id.balanceGoldTv)
    TextView mBalanceGoldTv;
    @BindView(R.id.balanceGoldLayout)
    LinearLayout mBbalanceGoldLayout;
    @BindView(R.id.tvGold)
    TextView mTvGold;
    @BindView(R.id.linkPickUp)
    ItemWithIcon mLinkPickUp;
    @BindView(R.id.linkGift)
    ItemWithIcon mLinkGift;

    @BindView(R.id.barPading)
    View mBarPading;

    private MyStatus mMyStatus;
    private IUserService mUserService;
    private IBalanceService mBalanceService;
    private IOrderService mOrderService;
    private User mUser;
    private CardDetailModel mCardDetailModel;

    private boolean isBlockChainOpen;
    private WJDialog blockChainActiveDialog;

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_me, container, false);
        ButterKnife.bind(this, view);

        if (SessionUtil.getInstance().isLogin()) {
            mUser = SessionUtil.getInstance().getLoginUser();
            setUserInfoView();
        } else {
            EventBus.getDefault().post(new EventMessage(Event.viewHome));
        }

        updateLiveBtnVisible();

        //导航栏高度
        int height = QMUIStatusBarHelper.getStatusbarHeight(getContext());
        mBarPading.getLayoutParams().height = height;
//        mLinkWorldCmp.setVisibility(View.GONE);

        mRefreshLayout.setOnRefreshListener(this);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        mBalanceService = ServiceManager.getInstance().createService(IBalanceService.class);
        mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        reloadUserInfo();
        loadBlockChainSwitch();
        if (getUserVisibleHint()) {
            updateLiveBtnVisible();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    private int index = 0;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (isAdded() && SessionUtil.getInstance().isLogin() && index++ % 2 == 0) {
                onRefresh();
            }
            showBindPhoneDialog();
            updateLiveBtnVisible();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updateUserInfo(EventMessage message) {
        switch (message.getEvent()) {
            case updateAvatar:
                FrescoUtil.setImage(mAvatarIv, String.valueOf(message.getData()));
                break;
            case updateNickname:
                mNicknameTv.setText(String.valueOf(message.getData()));
                break;
            case loginSuccess:
                onRefresh();
                break;
            default:
                break;
        }
    }

    @Override
    protected boolean isNeedLogin() {
        return true;
    }

    @Override
    public void onRefresh() {
        reloadUserInfo();
        reloadOrderStats();
        reloadMyStatus();
        reloadMyAuthInfo();
        reloadScore();
        reloadUnused();
        loadBlockChainSwitch();
    }

    private void reloadUnused() {
        APIManager.startRequest(mUserService.getUnused(), new BaseRequestListener<Unused>(mRefreshLayout) {
            @Override
            public void onSuccess(Unused result) {
                mLinkPickUp.setBadgeRedBg(result.num);
            }
        }, getContext());
    }

    private void showBindPhoneDialog() {
        if (mUser == null || !StringUtils.isEmpty(mUser.phone)) {
            return;
        }
        final WJDialog wjDialog = new WJDialog(getContext());
        wjDialog.show();
        wjDialog.setContentText("请先设置账户与密码");
        wjDialog.setCancelText("取消");
        wjDialog.setConfirmText("去设置");
        wjDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wjDialog.dismiss();
                goBindPhone();
            }
        });
    }

    private void goBindPhone() {
        startActivity(new Intent(getContext(), BindPhoneActivity.class));
    }

    private void reloadScore() {
        APIManager.startRequest(mUserService.getScoreStat(), new BaseRequestListener<ScoreStat>(mRefreshLayout) {
            @Override
            public void onSuccess(ScoreStat result) {
                mTvScore.setText(result.memberScore.totalScore + "");
            }
        }, getContext());
    }

    private void reloadMyStatus() {
        APIManager.startRequest(mUserService.getMyStatus(), new BaseRequestListener<MyStatus>(mRefreshLayout) {
            @Override
            public void onSuccess(MyStatus myStatus) {
                mMyStatus = myStatus;
                mLinkCouponCmp.setBadgeRedBg(myStatus.couponCount);
                mLinkMessageCmp.setBadgeRedBg(myStatus.messageCount);
                mLinkGift.setVisibility(myStatus.isChannel == 1 ? View.VISIBLE : View.GONE);
//                if (!MyApplication.isGongCat) {
//                    if (myStatus.bindBankStatus != AppTypes.CARD_STATUS.SUCESS) {
////                        mLinkBindCardCmp.setBadge("未绑定");
//                    } else {
////                        mLinkBindCardCmp.setBadge("");
//                    }
//                }
            }
        }, getContext());
    }


    private void reloadMyAuthInfo() {
        if (!MyApplication.isGongCat) {
            return;
        }
        APIManager.startRequest(mUserService.getGongCatCard(SessionUtil.getInstance().getOAuthToken()), new BaseRequestListener<CardDetailModel>(mRefreshLayout) {
            @Override
            public void onSuccess(CardDetailModel model) {
                mCardDetailModel = model;
                int status = -1;
                if (!TextUtils.isEmpty(mCardDetailModel.status)) {
                    status = Integer.parseInt(mCardDetailModel.status);
                }
                if (status != AppTypes.CARD_STATUS.SUCESS) {
//                    mLinkBindCardCmp.setBadge("未绑定");
                } else {
//                    mLinkBindCardCmp.setBadge("");
                }
            }

            @Override
            public void onError(Throwable e) {
                mCardDetailModel = new CardDetailModel();
                mCardDetailModel.status = "-1";
//                mLinkBindCardCmp.setBadge("未绑定");
            }
        }, getContext());
    }

    private void reloadUserInfo() {
        APIManager.startRequest(mUserService.getUserInfo(), new BaseRequestListener<User>(mRefreshLayout) {
            @Override
            public void onSuccess(User user) {
                SessionUtil.getInstance().setLoginUser(user);
                mUser = user;
                setUserInfoView();
            }

            @Override
            public void onComplete() {
                reloadBalance();
            }
        }, getContext());
    }

    private void setUserInfoView() {
        mIvSign.setSelected(mUser.isSignIn == 1);
        if (mUser.isStore != 0) {
            mLevelTv.setText(mUser.storeTypeStr);
        } else {
            mLevelTv.setText(mUser.vipTypeStr);
        }
        FrescoUtil.setImage(mAvatarIv, mUser.avatar);
        mNicknameTv.setText(mUser.nickname);

        mLinkVerifyCmp.setVisibility(MyApplication.isGongCat ? View.GONE : View.VISIBLE);
        mLinkFamilyCmp.setVisibility(mUser.vipType == 0 ? View.GONE : View.VISIBLE);

        if (mUser.autoAuthStatus != AppTypes.AUTH_STATUS.SUCESS) {
            mLinkVerifyCmp.setBadge("未认证");
        } else {
            mLinkVerifyCmp.setBadge("");
        }
    }

    private void reloadBalance() {
        APIManager.startRequest(mBalanceService.get(), new BaseRequestListener<ProfitData>(mRefreshLayout) {
            @Override
            public void onSuccess(ProfitData profitData) {
                mPointTv.setText(ConvertUtil.cent2yuanNoZero(profitData.profitSumMoney));
                mBalanceTv.setText(ConvertUtil.cent2yuanNoZero(profitData.availableMoney));
                if (mUser != null && mUser.isCCMActivate()) {
                    mCcmTv.setText("");
                } else {
                    mCcmTv.setText(MoneyUtil.coinToYuanStrNoZero(profitData.availableCoin) + "(待激活)");
                }
                // 隐藏C券
//                mTicketTv.setText(ConvertUtil.cent2yuanNoZero(profitData.sumTicket));
                mBalanceGoldTv.setText(ConvertUtil.cent2yuanNoZero(profitData.availableGold));
                mTvGold.setText(ConvertUtil.cent2yuanNoZero(profitData.goldenAvlTicket));
            }
        }, getContext());
    }

    private void reloadOrderStats() {
        APIManager.startRequest(mOrderService.getOrderCount(), new BaseRequestListener<OrderCount>(mRefreshLayout) {
            @Override
            public void onSuccess(OrderCount orderCount) {
                mOrderCommentCmp.setBadge(orderCount.waitComment);
                mOrderDispatchedCmp.setBadge(orderCount.hasShip);
                mOrderPaidCmp.setBadge(orderCount.waitShip);
                mOrderUnpayCmp.setBadge(orderCount.waitPay);
                mOrderServiceCmp.setBadge(orderCount.afterSales);
            }
        }, getContext());
    }

    /**
     * 实名认证
     */
    private void goAuth() {
        if (mUser == null) {
            reloadUserInfo();
            ToastUtil.error("等待数据");
            return;
        }
        UserService.checkHasPassword(getActivity(), new UserService.HasPasswordListener() {
            @Override
            public void onHasPassword() {
                switch (mUser.autoAuthStatus) {
                    case AppTypes.AUTH_STATUS.WAIT:
                        APIManager.startRequest(mUserService.getAuth(), new BaseRequestListener<AuthModel>(mRefreshLayout) {
                            @Override
                            public void onSuccess(AuthModel model) {
                                startActivity(new Intent(getContext(), SubmitStatusActivity.class));
                                MsgStatus msgStatus = new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_FAIL);
                                msgStatus.setTips(model.memberAuthBean.checkRemark);
                                EventBus.getDefault().postSticky(msgStatus);
                            }
                        }, getContext());
                        /*startActivity(new Intent(getContext(), SubmitStatusActivity.class));
                        EventBus.getDefault().postSticky(new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_IDENTITY_WAIT));*/
                        break;
                    case AppTypes.AUTH_STATUS.SUCESS:
                        SharedPreferences sharedPreferences1 = getContext().getSharedPreferences("authType", 0);
                        sharedPreferences1.edit().putString("authType", "UserCenter").commit();
                        startActivity(new Intent(getContext(), AuthIdentitySuccssdActivity.class));
                        break;
                    /*case AppTypes.AUTH_STATUS.FAIL:

                        break;*/
                    case AppTypes.AUTH_STATUS.NO_SUBMIT:
                        Intent intent2 = new Intent(getContext(), AuthPhoneActivity.class);
                        SharedPreferences sharedPreferences = getContext().getSharedPreferences("authType", 0);
                        sharedPreferences.edit().putString("authType", "UserCenter").commit();
                        intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
                        startActivity(intent2);
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @OnClick(R.id.viewMoreOrderLayout)
    protected void viewOrderAllList() {
        Intent intent = new Intent(getContext(), OrderListActivity.class);
        intent.putExtra("type", "all");
        startActivity(intent);
    }

    @OnClick(R.id.orderUnpayCmp)
    protected void viewOrderUnpayList() {
        Intent intent = new Intent(getContext(), OrderListActivity.class);
        intent.putExtra("type", "wait-pay");
        startActivity(intent);
    }

    @OnClick(R.id.orderPaidCmp)
    protected void viewOrderPaidList() {
        Intent intent = new Intent(getContext(), OrderListActivity.class);
        intent.putExtra("type", "paid");
        startActivity(intent);
    }

    @OnClick(R.id.orderDispatchedCmp)
    protected void viewOrderDispatchedList() {
        Intent intent = new Intent(getContext(), OrderListActivity.class);
        intent.putExtra("type", "dispatched");
        startActivity(intent);
    }

    @OnClick(R.id.orderCommentCmp)
    protected void viewOrderWaitCommentList() {
        Intent intent = new Intent(getContext(), MyCommentListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.orderServiceCmp)
    protected void viewOrderServiceList() {
//        startActivity(new Intent(getContext(), ServiceOrderListActivity.class));
        startActivity(new Intent(getContext(), NewRefundsOrderListActivity.class));
    }

    @OnClick({R.id.avatarIv, R.id.avatarIvOver})
    protected void viewProfile(View v) {
        startActivity(new Intent(getContext(), ProfileActivity.class));
    }

    @OnClick(R.id.linkCouponCmp)
    protected void viewCouponList() {
        startActivity(new Intent(getContext(), CouponListActivity.class));
    }

    @OnClick(R.id.linkFavCmp)
    protected void viewCollect() {
        startActivity(new Intent(getContext(), CollectListActivity.class));
    }

    @OnClick(R.id.linkWorld)
    protected void viewWorld() {
        WorldHomeActivity.start(getContext());
    }

    @OnClick(R.id.linkHistoryCmp)
    protected void viewFoot() {
        startActivity(new Intent(getContext(), FootListActivity.class));
    }

//    @OnClick({R.id.pointLayout})
//    protected void viewProfit() {
//        startActivity(new Intent(getContext(), ProfitListActivity.class));
//    }

    @OnClick({R.id.balanceLayout, R.id.ticketLayout, R.id.ccmLayout, R.id.balanceGoldLayout, R.id.layoutGold})
    protected void viewBalance(View view) {
        Intent intent = new Intent(getContext(), BalanceListActivity.class);
        switch (view.getId()) {
            case R.id.balanceLayout:
                intent.putExtra("type", AccountType.Balance);
                break;
            case R.id.ccmLayout:
//                intent.putExtra("type", AccountType.CCM);
                openBlockChain();
                return;
            case R.id.ticketLayout:
                intent.putExtra("type", AccountType.CB);
                break;
//            case R.id.pointLayout:
//                intent.putExtra("type", AccountType.Profit);
//                break;
            case R.id.balanceGoldLayout:
                intent.putExtra("type", AccountType.ShopMoney);
                break;
            case R.id.layoutGold:
                intent.putExtra("type", AccountType.GoldenTicket);
                break;
            default:
        }
        startActivity(intent);
    }

    private void loadBlockChainSwitch() {
        IBlockChain bc = ServiceManager.getInstance().createService(IBlockChain.class);
        APIManager.startRequest(bc.reviewForPageConfig(), new BaseRequestListener<BlockChainSwitch>() {
            @Override
            public void onSuccess(BlockChainSwitch result) {
                super.onSuccess(result);
                if (!isAdded()) {
                    return;
                }
                if (result.blockchain == null) {
                    isBlockChainOpen = false;
                    return;
                }
                isBlockChainOpen = result.blockchain.isOpen == 1;
            }
        }, getContext());
    }

    private void openBlockChain() {
        if (!isBlockChainOpen) {
            return;
        }
        if (mUser == null) {
            return;
        }
        if (mUser.isCCMActivate()) {
            CCMMainActivity.start(getContext());
        } else {
            if (blockChainActiveDialog != null && blockChainActiveDialog.isShowing()) {
                blockChainActiveDialog.dismiss();
            }
            blockChainActiveDialog = new WJDialog(getContext());
            blockChainActiveDialog.show();
            blockChainActiveDialog.setTitle("CCM机制更新");
            blockChainActiveDialog.setContentText("为了感谢各位,CCM机制迎来重大更新,激活后每天可获得CCM返利分红,还有更多会员福利等着大家.");
            blockChainActiveDialog.setCancelText("取消");
            blockChainActiveDialog.setConfirmText("激活");
            blockChainActiveDialog.setConfirmTextColor(R.color.color_bule);
            blockChainActiveDialog.setOnConfirmListener((v) -> {
                blockChainActiveDialog.dismiss();
                IBlockChain bc = ServiceManager.getInstance().createService(IBlockChain.class);
                ToastUtil.showLoading(getContext());
                APIManager.startRequest(bc.ccmTransferChainActivate(), new BaseRequestListener<Object>() {
                    @Override
                    public void onSuccess(Object result) {
                        super.onSuccess(result);
                        if (!isAdded()) {
                            return;
                        }
                        mUser.ccmTransferChainActivate = 1; // 设置为激活
                        mCcmTv.setText("");
                        CCMMainActivity.start(getContext());
                    }

                    @Override
                    public void onComplete() {
                        ToastUtil.hideLoading();
                    }
                }, getContext());
            });
        }

    }

    @OnClick(R.id.pointLayout)
    protected void pointLayout() {
        startActivity(new Intent(getContext(), ProfitListActivity.class));
    }

    @OnClick(R.id.linkMessageCmp)
    protected void viewMessageList() {
        startActivity(new Intent(getContext(), MessageListActivity.class));
    }

    @OnClick(R.id.linkContribution)
    protected void viewContribution() {
        startActivity(new Intent(getContext(), MyContributionBarChartActivity.class));
    }

    @OnClick(R.id.userHeaderLayout)
    protected void test() {
//        LiveRedListDialog liveRedListDialog = new LiveRedListDialog(getActivity(), "ddd");
//        liveRedListDialog.show();
    }

    @OnClick(R.id.linkJKPOrder)
    protected void viewJKPOrder() {
        WebViewCommonActivity.start(getActivity(), String.format("%sorder/list", BuildConfig.JKP_PREFIX), false, true);
    }

    @OnClick(R.id.linkVerifyCmp)
    protected void viewVerifyCmp() {
        goAuth();
    }

    @OnClick(R.id.linkFamilyCmp)
    protected void viewFamily() {
        Intent intent = new Intent(getContext(), FamilyActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.linkQrCodeCmp)
    protected void viewQrCode() {
//        startActivity(new Intent(getContext(), QRCodeActivity.class));
        QRCodeActivity.start(getContext(), QRCodeActivity.TYPE_InvitationCodeByIcon);
    }

    @OnClick(R.id.linkSubscribeCmp)
    protected void viewSubscribe() {
        Intent intent = new Intent(getContext(), QrCodeShowActivity.class);
        intent.putExtra(Config.INTENT_KEY_TYPE_NAME, AppTypes.QRCODE.SUBSCRIBE);
        startActivity(intent);
    }

    @OnClick(R.id.layoutScore)
    protected void viewPointList() {
        startActivity(new Intent(getContext(), PointListActivity.class));
    }


    @OnClick(R.id.linkNotice)
    protected void goNotice() {
        Intent intent = new Intent(getContext(), NoticeListActivity.class);
        startActivity(intent);
    }


 /* /*
    @OnClick(R.id.creditCard)
    protected void creditCard() {
        Intent intent = new Intent(getContext(), WebViewActivity.class);
        intent.putExtra("url", "http://47.110.240.229/customizYTH/#/?phone=" + mUser.phone + "&token=" + SessionUtil.getInstance().getOAuthToken());
        intent.putExtra("hideToolbar", true);
        startActivity( intent);
    }
*/

    /**
     * 绑定银行卡
     */
    /*@OnClick(R.id.linkBindCardCmp)
    protected void bindCard() {
        if (MyApplication.isGongCat) {
            gongCatBinding();
            return;
        }

        if (StringUtils.isEmpty(mUser.phone)) {
            ToastUtils.showShortToast("请先绑定手机");
            startActivity(new Intent(getActivity(), BindPhoneActivity.class));
            return;
        }
        if (mUser == null || mUser.authStatus != AppTypes.AUTH_STATUS.SUCESS) {
            ToastUtils.showShortToast("请先实名认证");
            return;
        }
        if (mMyStatus == null) {
//            reloadMyStatus();
            return;
        }
        UserService.checkHasPassword(getActivity(), new UserService.HasPasswordListener() {
            @Override
            public void onHasPassword() {
                switch (mMyStatus.bindBankStatus) {
                    case AppTypes.CARD_STATUS.NO_SUBMIT:
                        Intent intent2 = new Intent(getContext(), AuthPhoneActivity.class);
                        intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_CARD);
                        getContext().startActivity(intent2);
                        break;
                    case AppTypes.CARD_STATUS.WAIT:
                        startActivity(new Intent(getContext(), SubmitStatusActivity.class));
                        EventBus.getDefault().postSticky(new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_CARD_WAIT));
                        break;
                    case AppTypes.CARD_STATUS.SUCESS:
                        startActivity(new Intent(getContext(), AuthCardSuccssdActivity.class));
                        break;
                    case AppTypes.CARD_STATUS.FAIL:
                        APIManager.startRequest(mUserService.getCard(), new BaseRequestListener<CardDetailModel>(mRefreshLayout) {
                            @Override
                            public void onSuccess(CardDetailModel model) {
                                startActivity(new Intent(getContext(), SubmitStatusActivity.class));
                                MsgStatus msgStatus = new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_CARD_FAIL);
                                msgStatus.setTips(model.checkResult);
                                EventBus.getDefault().postSticky(msgStatus);
                            }
                        });
                        break;
                    default:
                        break;
                }
            }
        });
    }*/

    /**
     * 工猫绑定银行卡
     */
    private void gongCatBinding() {
        if (StringUtils.isEmpty(mUser.phone)) {
            ToastUtils.showShortToast("请先绑定手机");
            startActivity(new Intent(getActivity(), BindPhoneActivity.class));
            return;
        }
//        if (mUser == null || mUser.authStatus != AppTypes.AUTH_STATUS.SUCESS) {
//            ToastUtils.showShortToast("请先实名认证");
//            return;
//        }
        if (mCardDetailModel == null) {
            reloadMyAuthInfo();
            return;
        }
        UserService.checkHasPassword(getActivity(), new UserService.HasPasswordListener() {
            @Override
            public void onHasPassword() {
                int status = -1;
                if (!TextUtils.isEmpty(mCardDetailModel.status)) {
                    status = Integer.parseInt(mCardDetailModel.status);
                }
                switch (status) {
                    case AppTypes.CARD_STATUS_GONGCAT.NO_SUBMIT:
                        Intent intent2 = new Intent(getContext(), AuthPhoneActivity2.class);
                        intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_CARD);
                        getContext().startActivity(intent2);
                        break;
                    case AppTypes.CARD_STATUS_GONGCAT.WAIT:
                        startActivity(new Intent(getContext(), SubmitStatusActivity.class));
                        EventBus.getDefault().postSticky(new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_CARD_WAIT));
                        break;
                    case AppTypes.CARD_STATUS_GONGCAT.SUCESS:
                        startActivity(new Intent(getContext(), AuthCardSuccssdGongCatActivity.class));
                        break;
                    case AppTypes.CARD_STATUS_GONGCAT.FAIL:
                        APIManager.startRequest(mUserService.getGongCatCard(SessionUtil.getInstance().getOAuthToken()), new BaseRequestListener<CardDetailModel>(mRefreshLayout) {
                            @Override
                            public void onSuccess(CardDetailModel model) {
                                startActivity(new Intent(getContext(), SubmitStatusActivity.class));
                                MsgStatus msgStatus = new MsgStatus(Config.USER.INTENT_KEY_TYPE_AUTH_CARD_FAIL);
                                msgStatus.setTips(model.checkResult);
                                EventBus.getDefault().postSticky(msgStatus);
                            }
                        }, getContext());
                        break;
                    default:
                        break;
                }
            }
        });
    }


    @OnClick(R.id.ivSign)
    public void onViewClicked() {
        if (mIvSign.isSelected()) {
            final WJDialog dialog = new WJDialog(getContext());
            dialog.show();
            dialog.hideCancelBtn();
            dialog.setContentText("今天已签到，明天再来吧");
            return;
        }
        APIManager.startRequest(mUserService.sign(), new Observer<RequestResult<SignModel>>() {

            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(RequestResult<SignModel> result) {
                // 当前fragment已经和Activity解绑，不应该再做UI处理
                if (getActivity() == null || !isAdded()) {
                    return;
                }
                String message = result.message;
                if (result.isSuccess()) {
                    final WJDialog dialog = new WJDialog(getContext());
                    dialog.show();
                    dialog.hideCancelBtn();
                    dialog.setContentText(message);
                    dialog.setOnConfirmListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mIvSign.setSelected(true);
                            dialog.dismiss();
                        }
                    });
                } else if (result.isNotLogin()) {
                    if (!MyApplication.isShowNoLogin) {
                        MyApplication.isShowNoLogin = true;
                        onError(new Exception(Config.NET_MESSAGE.NO_LOGIN));
                        EventBus.getDefault().post(new EventMessage(Event.goToLogin));
                        SessionUtil.getInstance().logout();
                    }
                } else if (result.isFail()) {
                    onError(new Exception(message));
                } else {
                    onError(new Exception("未知错误"));
                }
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.error(e.getMessage());
                Logger.e(e, "API Error.");
                if (e instanceof SocketTimeoutException) {
                    ToastUtil.error("请求超时");
                } else if (e instanceof IllegalStateException) {
                    ToastUtil.error("服务器数据异常");
                } else if (e instanceof UnknownHostException) {
                    ToastUtil.error("网络状态异常");
                } else {
                    ToastUtil.error(e.getMessage());
                }
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @OnClick(R.id.linkNewerSku)
    protected void newerSku() {
        startActivity(new Intent(getActivity(), NewerProductDetailActivity.class));
    }

    @OnClick(R.id.linkPickUp)
    protected void pickUp() {
        startActivity(new Intent(getActivity(), PickUpListActivity.class));
    }


    @OnClick(R.id.linkSuperGroup)
    protected void superGroup() {
        startActivity(new Intent(getActivity(), MySuperGroupListActivity.class));
    }

    @OnClick(R.id.mallService)
    protected void mallService() {
        CSUtils.start(getContext(), "");
    }

    @OnClick(R.id.contactInviter)
    protected void contactInviter() {
//        mUser.referrerMemberId = null;
        if (TextUtils.isEmpty(mUser.referrerMemberId)) {
            startActivity(new Intent(getContext(), InvitationEditActivity.class));
            return;
        }
        BaseRequestListener<UpMemberModel> l = new BaseRequestListener<UpMemberModel>() {
            @Override
            public void onSuccess(UpMemberModel result) {
                super.onSuccess(result);
                contactCustomer(getContext(), mUser.referrerMemberId, result.nickName);
            }
        };
        APIManager.startRequest(mUserService.getUpMember(), l, getContext());
    }

    @OnClick(R.id.linkGift)
    protected void gift() {
        startActivity(new Intent(getActivity(), MyGiftActivity.class));
    }

    @OnClick(R.id.linkLive)
    protected void live() {
        startActivity(new Intent(getActivity(), LiveHomeActivity.class));
    }

    AlertDialog alertDialog;

    @OnClick(R.id.linkHighGo)
    protected void highGo() {
        boolean isRead = getContext().getSharedPreferences(Const.READ_HIGOU_NEED_TO_KNOW, 0).getBoolean(Const.READ_HIGOU_NEED_TO_KNOW, false);
        if (!isRead) {

            View view1 = LayoutInflater.from(getContext()).inflate(R.layout.check_higou_need_to_know_dialog, null);
            RelativeLayout rl_to_go_read = view1.findViewById(R.id.rl_to_go_read);
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            if (alertDialog == null && SessionUtil.getInstance().isLogin()) {
                alertDialog = builder.create();
                alertDialog.setView(view1, 0, 0, 0, 0);
                alertDialog.setCancelable(false);
                alertDialog.show();
                alertDialog.setOnKeyListener(onKeyListener1);

                rl_to_go_read.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(getContext(), HiGouDescriptionActivity.class));
                    }
                });

            }
        } else {
            startActivity(new Intent(getActivity(), HighGoActivity.class));
        }
    }

    private DialogInterface.OnKeyListener onKeyListener1 = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if ((i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_DOWN) || i == 4) {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();

                    alertDialog = null;
                }
            }
            return true;
        }

    };

    private void updateLiveBtnVisible() {
//        if (!SessionUtil.getInstance().isLogin()) {
//            return;
//        }
//        APIManager.startRequest(mService.getLiveUserInfo(SessionUtil.getInstance().getLoginUser().id), new BaseRequestListener<LiveUser>() {
//            @Override
//            public void onSuccess(LiveUser result) {
//                linkLive.setVisibility(result.liveLimit == 1 ? View.VISIBLE : View.GONE);
//            }
//        }, getContext());
    }

    private void contactCustomer(Context context, String memberId, String nickName) {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, context);
                    ChatInfo chatInfo = new ChatInfo();
                    chatInfo.setType(TIMConversationType.C2C);
                    chatInfo.setId(memberId);
                    chatInfo.setChatName(nickName);
                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.putExtra("chatInfo", chatInfo);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            });
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatusMsg(MsgStatus message) {
        switch (message.getAction()) {
            case MsgStatus.ACTION_CARD_CHANGE:
                reloadMyAuthInfo();
                reloadMyStatus();
                break;
            case MsgStatus.ACTION_USER_CHANGE:
                reloadUserInfo();
                break;
            case MsgStatus.ACTION_CANCEL_REFUNDS:
            case MsgStatus.ACTION_REFUND_CHANGE:
                reloadOrderStats();
                break;
            case MsgStatus.ACTION_DEAL_SUCESS:
                reloadUserInfo();
                break;
            default:
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void transferHandler(EventMessage message) {
        switch (message.getEvent()) {
            case transToGoldSuccess:
                reloadBalance();
                break;
            case cancelOrder:
            case refundOrder:
            case finishOrder:
                reloadOrderStats();
                reloadBalance();
                break;
            case transferSuccess:
                reloadScore();
                break;
            case paySuccess:
                reloadUserInfo();
                reloadBalance();
                reloadOrderStats();
                break;
            case createOrderSuccess:
                reloadUnused();
                break;
            case isReadHigou:
                alertDialog.dismiss();
            default:
        }
    }
}
