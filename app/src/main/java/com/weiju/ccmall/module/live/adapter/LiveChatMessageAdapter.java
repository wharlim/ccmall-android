package com.weiju.ccmall.module.live.adapter;

import android.text.Html;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.LiveMessage;
import com.weiju.ccmall.shared.util.FrescoUtil;


/**
 * @author chenyanming
 * @time 2020/1/4 on 17:32
 * @desc
 */
public class LiveChatMessageAdapter extends BaseQuickAdapter<LiveMessage, BaseViewHolder> {
    public LiveChatMessageAdapter() {
        super(R.layout.item_live_chat_message);
    }


    @Override
    protected void convert(BaseViewHolder helper, LiveMessage item) {
        if (item.showAvatar) {
            //主播发言有头像  名字黄色
            helper.setText(R.id.tvContent, Html.fromHtml(String.format("<font color =\"#FFF958\">%s</font>:%s",
                    item.name, item.content)));
        } else if (item.isWelcome) {
            //欢迎消息没头像 名字黄色
            helper.setText(R.id.tvContent, Html.fromHtml(String.format("<font color =\"#FFF958\">%s</font>  %s",
                    item.name, item.content)));

        } else if (item.isNotice) {

            helper.setText(R.id.tvContent, Html.fromHtml(String.format("<font color =\"#32D482\">%s</font>", item.content)));
        } else {
            //普通人消息 名字绿色
            helper.setText(R.id.tvContent, Html.fromHtml(String.format("<font color =\"#32D482\">%s</font>:%s",
                    item.name, item.content)));
        }
        helper.addOnClickListener(R.id.tvContent);

        FrescoUtil.setImage(helper.getView(R.id.ivAvatar), item.headImage);
        helper.setVisible(R.id.ivAvatar, item.showAvatar);
    }
}
