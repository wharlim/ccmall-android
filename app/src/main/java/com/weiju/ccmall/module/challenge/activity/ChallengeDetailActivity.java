package com.weiju.ccmall.module.challenge.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Challenge;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IChallengeService;
import com.weiju.ccmall.shared.util.CountDownRxUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/1/28 on 17:48
 * @desc 挑战自我
 */
public class ChallengeDetailActivity extends BaseActivity {

    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.tvTime)
    TextView mTvTime;
    @BindView(R.id.tvChallenge)
    TextView mTvChallenge;
    @BindView(R.id.ivChallengeBg)
    ImageView mIvChallengeBg;

    private WJDialog mWjDialog;
    private String mActivityId;

    private IChallengeService mService = ServiceManager.getInstance().createService(IChallengeService.class);
    private Challenge mChallenge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_detail);
        ButterKnife.bind(this);
        initIntent();
        initView();
        initData();
        EventBus.getDefault().register(this);

    }

    private void initIntent() {
        mActivityId = getIntent().getStringExtra("activityId");
    }

    private void initData() {
        APIManager.startRequest(mService.checkChallenge(mActivityId),
                new BaseRequestListener<Challenge>(this) {
                    @Override
                    public void onSuccess(Challenge result) {
                        mChallenge = result;
                        setData();
                    }
                },this);
    }

    private void setData() {
        switch (mChallenge.activityStatus) {
            case 0:
                mTvTime.setText("开始挑战");
                mTvChallenge.setText("挑战");
                mTvChallenge.setVisibility(View.GONE);
                break;
            case 1:
                setCountDownText();
                break;
            case 2:
                mTvTime.setText("挑战成功");
                mTvChallenge.setText("再次挑战");
                mTvChallenge.setVisibility(View.VISIBLE);
                break;
            case 3:
                mTvTime.setText("挑战失败");
                mTvChallenge.setText("重新挑战");
                mTvChallenge.setVisibility(View.VISIBLE);
                break;
            default:
        }

        mIvChallengeBg.setImageDrawable(getResources().getDrawable(mChallenge.activityStatus == 3 ?
                R.drawable.ic_challenge_round_gray : R.drawable.ic_challenge_round_normal));
        if (mChallenge.type == 0) {
            mTvTitle.setText(String.format("挑战开始后%s小时内成功销售 " +
                    " %s 份大礼包,可获得 %s 幸运积分", mChallenge.lifeCycle, mChallenge.target, mChallenge.score));
        } else {
            mTvTitle.setText(String.format("%s小时内成功升级至 %s ,可获得 %s 幸运积分", mChallenge.lifeCycle, mChallenge.target, mChallenge.score));
        }

    }


    private void initView() {
        setTitle("挑战自我");
        setLeftBlack();
    }


    @OnClick({R.id.tvChallenge, R.id.layoutChallenge})
    public void challenge(View view) {
        if (view.getId() == R.id.layoutChallenge && mChallenge.activityStatus != 0) {
            return;
        }
        if (mWjDialog == null) {
            mWjDialog = new WJDialog(ChallengeDetailActivity.this);
        }
        mWjDialog.show();
        mWjDialog.setContentText("您准备好了吗？");
        mWjDialog.setCancelText("再想想");
        mWjDialog.setConfirmText("我能行");
        mWjDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mWjDialog.dismiss();
                startChallenr();
            }
        });
    }

    private void startChallenr() {
        APIManager.startRequest(mService.joinChallenge(mActivityId),
                new BaseRequestListener<Challenge>(this) {
                    @Override
                    public void onSuccess(Challenge result) {
                        mChallenge = result;
                        setData();
                        EventBus.getDefault().post((new EventMessage(Event.joinChallenge)));
                    }
                },this);
    }

    private void setCountDownText() {
        CountDownRxUtils.textViewCountDown(mTvTime, mChallenge.expiresDate, "距离结束还剩\n%s", "%s : %s : %s");
        mTvChallenge.setVisibility(View.GONE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(EventMessage message) {
        switch (message.getEvent()) {
            case countDownComplete:
                initData();
                break;
            default:
        }
    }
}
