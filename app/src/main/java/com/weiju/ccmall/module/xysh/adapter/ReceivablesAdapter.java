package com.weiju.ccmall.module.xysh.adapter;


import android.text.TextUtils;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.ReceivablesBill;
import com.weiju.ccmall.module.xysh.helper.BankUtils;


/**
 * @author chenyanming
 * @time 2019/12/16 on 15:51
 * @desc
 */
public class ReceivablesAdapter extends BaseQuickAdapter<ReceivablesBill, BaseViewHolder> {
    public ReceivablesAdapter() {
        super(R.layout.item_receivables);
    }

    @Override
    protected void convert(BaseViewHolder helper, ReceivablesBill item) {
        if (TextUtils.isEmpty(item.clrCardName)) {
            helper.setText(R.id.tvBankName, String.format("到账卡(%s)", BankUtils.cutBankCardNo(item.clrCardNo)));
        } else {
            helper.setText(R.id.tvBankName, String.format("到账卡 - %s(%s)", item.clrCardName, BankUtils.cutBankCardNo(item.clrCardNo)));
        }
        helper.setText(R.id.tvDesc, item.channelName);
        helper.setText(R.id.tvTime, item.payTime);
        helper.setText(R.id.tvMoney, String.format("%+.2f", item.txnAmt));
        helper.setText(R.id.tvState, item.getStateText());
        helper.setImageResource(R.id.ivAvatar, BankUtils.getBankIconByName(item.clrCardName));
    }
}
