package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.ToastUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.page.WebViewCommonActivity;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.bean.MerchantInResult;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.fragment.GetMessageAuthCodeDialogFragment;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.CityPickerDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class AuthChannelActivity extends BaseActivity
        implements GetMessageAuthCodeDialogFragment.OnConfirmListener {

    @BindView(R.id.ivCardBig)
    SimpleDraweeView ivCardBig;
    @BindView(R.id.tvBankName)
    TextView tvBankName;
    //    @BindView(R.id.tvBillDate)
//    TextView tvBillDate;
//    @BindView(R.id.tvPayDate)
//    TextView tvPayDate;
//    @BindView(R.id.tvCheckPlan)
//    TextView tvCheckPlan;
    @BindView(R.id.iv_channel_icon)
    ImageView ivChannelIcon;
    @BindView(R.id.tv_channel_name)
    TextView tvChannelName;
    @BindView(R.id.tv_auth_state)
    TextView tvAuthState;
    @BindView(R.id.llChannelContent)
    LinearLayout llChannelContent;
    @BindView(R.id.tv_charge_rate)
    TextView tvChargeRate;
    @BindView(R.id.tv_transfer_limit_one_day)
    TextView tvTransferLimitOneDay;
    @BindView(R.id.tv_transfer_limit_one_time)
    TextView tvTransferLimitOneTime;
    @BindView(R.id.tv_transferred_time)
    TextView tvTransferredTime;
    @BindView(R.id.tv_transferring_time)
    TextView tvTransferringTime;
    @BindView(R.id.ll_supported_banks)
    LinearLayout llSupportedBanks;
    @BindView(R.id.tvCardNo)
    TextView tvCardNo;
    //    @BindView(R.id.tv_bank_name)
//    TextView tvBankName;
//    @BindView(R.id.ll_bank_container)
//    LinearLayout llBankContainer;
    @BindView(R.id.et_valid_date)
    EditText etValidDate;
    @BindView(R.id.ll_valid_date_contaner)
    LinearLayout llValidDateContaner;
    @BindView(R.id.et_cvv)
    EditText etCvv;
    @BindView(R.id.tv_address)
    TextView tvAddress;
    @BindView(R.id.ll_address_container)
    LinearLayout llAddressContainer;
    @BindView(R.id.tv_channel_auth)
    TextView tvChannelAuth;
//    @BindView(R.id.iv_bank_icon)
//    SimpleDraweeView ivBankIcon;

    public static final int AUTH_TYPE_REPAY = 1; // 还款通道
    public static final int AUTH_TYPE_RECEIPT = 2; // 收款通道
    @BindView(R.id.tvChangePhone)
    TextView tvChangePhone;
    @BindView(R.id.tvPhone)
    TextView tvPhone;

    private int authType = AUTH_TYPE_REPAY;

    CityPickerDialog mCityPickerDialog;
    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    QueryUserBankCardResult.BankInfListBean creditCard;
    QueryUserBankCardResult.BankInfListBean debitCard;
    ChannelItem channel;
    String mValidDate;
    String mProvince;
    String mCity;
    String mCounty;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_channel);
        authType = getIntent().getIntExtra("authType", AUTH_TYPE_REPAY);
        creditCard = (QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("creditCard");
        debitCard = (QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("debitCard");
        channel = (ChannelItem) getIntent().getSerializableExtra("channel");
        ButterKnife.bind(this);
        setTitle("认证新通道");
        setLeftBlack();
        initView();
    }

    private void initView() {
        mCityPickerDialog = new CityPickerDialog(this);
        // card info
        ivCardBig.setImageResource(BankUtils.getBankIconByName(creditCard.bankName));
//        tvCardNameAndNo.setText(String.format("%s（%s）", creditCard.bankName, BankUtils.cutBankCardNo(creditCard.cardNo)));
//        tvBillDate.setText(String.format("账单日 %s日", creditCard.billDate));
//        tvPayDate.setText(String.format("还款日 %s日", creditCard.repayDate));
//        tvCheckPlan.setVisibility(View.GONE);
        // channel info
        tvChannelName.setText(channel.channelName);
        String charge = String.format("%.2f%%+¥%.1f/笔", channel.channelRate, channel.serviceCharge);
        tvChargeRate.setText(charge);
        tvTransferLimitOneDay.setText(channel.dailyLimit + "");
        String limitOneTime = String.format("%s-%s", channel.singleMinimum + "", channel.singleLimit + "");
        tvTransferLimitOneTime.setText(limitOneTime);
        tvTransferringTime.setText(channel.startTime + "-" + channel.endTime);
        // card list info
        tvCardNo.setText(creditCard.cardNo);
//        ivBankIcon.setImageResource(BankUtils.getBankIconByName(creditCard.bankName));
        tvBankName.setText(creditCard.bankName);
        tvPhone.setText(creditCard.resTel);
        tvAuthState.setTextColor(getResources().getColor(R.color.color_e5bb71));
        tvAuthState.setVisibility(View.VISIBLE);
        if (channel.openStatus == 0) {
            tvAuthState.setText("未认证");
        } else if (channel.openStatus == 1) {
            tvAuthState.setText("已认证");
        } else {
            tvAuthState.setText("不支持");
        }

        tvCardNo.setText(String.format("%s **** **** %s", creditCard.cardNo.substring(0, 4), creditCard.cardNo.substring(creditCard.cardNo.length() - 4)));
        llChannelContent.setBackgroundResource(R.color.white);
        etValidDate.requestFocus();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof GetMessageAuthCodeDialogFragment) {
            ((GetMessageAuthCodeDialogFragment) fragment).setOnConfirmListener(this);
        }
    }

    @OnClick({R.id.ll_supported_banks, R.id.ll_valid_date_contaner, R.id.ll_address_container, R.id.tv_channel_auth, R.id.tvChangePhone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_supported_banks:
                if (authType == AUTH_TYPE_REPAY) {
                    SupportBanksActivity.start(this, channel.channelShortName);
                } else if (authType == AUTH_TYPE_RECEIPT) {
                    ReceiptSupportBanksActivity.start(this, channel.channelShortName);
                }
                break;
//            case R.id.ll_bank_container:
//                break;
            case R.id.ll_valid_date_contaner:
                selectValidDate();
                break;
            case R.id.ll_address_container:
                selectAddress();
                break;
            case R.id.tv_channel_auth:
//                RepaymentPlanActivity.start(this, true);
//                GetMessageAuthCodeDialogFragment.newInstance().show(getSupportFragmentManager());
                authChannel();
                break;
            case R.id.tvChangePhone:
//                if (authType == AUTH_TYPE_REPAY) {
                    Intent intent = new Intent(this, AddEditXinYongActivity.class);
                    intent.putExtra("isEdit", true);
                    intent.putExtra("isFromAChangePhone", true);
                    intent.putExtra("bankInfListBean", creditCard);
                    startActivity(intent);
//                }
                /*else {
                    Intent intent = new Intent(this, AddEditDebitCardActivity.class);
                    intent.putExtra("isEdit", true);
                    intent.putExtra("isFromAChangePhone", true);
                    intent.putExtra("bankInfListBean", creditCard);
                    startActivity(intent);
                }*/
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
        switch (message.getEvent()) {
            case bankChange:
                String phone = String.valueOf(message.getData());
                tvPhone.setText(phone);
                creditCard.resTel = phone;
                break;
            default:
        }
    }

    private void authChannel() {
        String cvv = etCvv.getText().toString().trim();
        if (TextUtils.isEmpty(cvv)) {
            ToastUtil.error("cvv不能为空！");
            return;
        }
        mValidDate = etValidDate.getText().toString().trim();
        if (!BankUtils.checkPeriodOfValidity(mValidDate)) {
            ToastUtil.error("有效期格式错误，请输入正确的月年，例如：01/27，则输入：0127");
            return;
        }


        if (TextUtils.isEmpty(mProvince) || TextUtils.isEmpty(mCity) || TextUtils.isEmpty(mCounty)) {
            ToastUtil.error("地址不能为空！");
            return;
        }
        ToastUtil.showLoading(this);
        Observable<XYSHCommonResult<MerchantInResult>> req = null;
        if (authType == AUTH_TYPE_REPAY) {
            req = service.merchantIn(channel.channelShortName, creditCard.cardNo, cvv,
                    mValidDate, mProvince, mCity, mCounty, tvAddress.getText().toString(), BuildConfig.VERSION_NAME);
        } else if (authType == AUTH_TYPE_RECEIPT) {
            req = service.gftqPay_merchantIn(debitCard.cardNo, creditCard.cardNo, cvv,
                    mValidDate, mProvince, mCity, mCounty, tvAddress.getText().toString());
            if (channel.h5Verification == 1) {
                req = service.ctfPay_merchantIn(debitCard.cardNo, creditCard.cardNo, cvv,
                        mValidDate, mProvince, mCity);
            }
        } else {
            throw new IllegalArgumentException("authType error: " + authType);
        }
        APIManager.startRequest(req, new Observer<XYSHCommonResult<MerchantInResult>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<MerchantInResult> ret) {
                if (isDestroyed()) {
                    return;
                }
                ToastUtil.hideLoading();
                if (ret.code == 1) {
                    if (channel.h5Verification == 1) {
                        MerchantInResult data = ret.data;
                        if (ret.data != null) {
                            String h5Url = data.h5Url;
                            if (!TextUtils.isEmpty(h5Url)) {
                                WebViewCommonActivity.start(AuthChannelActivity.this, h5Url, true);
                            } else {
                                ToastUtil.success(ret.msg);
                                finish();
                            }
                        }
                        return;
                    }
                    if (authType == AUTH_TYPE_REPAY && channel.authCodeStatus == 2) {    //新通道SqSmall、SqLarge
                        if (ret.data.verifyType == 2) { //需要短信验证
                            GetMessageAuthCodeDialogFragment.newInstance(
                                    ret.data.creditCardPhone, creditCard, channel, authType)
                                    .show(getSupportFragmentManager());
                        } else if (ret.data.verifyType == 1) {
                            ToastUtil.success(ret.msg);
                            finish();
                        }
                        return;
                    }
                    if (channel.authCodeStatus == 0) { // 需要短信验证
                        GetMessageAuthCodeDialogFragment.newInstance(
                                ret.data.creditCardPhone, creditCard, channel, authType)
                                .show(getSupportFragmentManager());
                    } else {
                        onConfirm("");
                    }
                } else {
                    ToastUtil.error(ret.msg + "");
                }
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.hideLoading();
            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void selectValidDate() {
//        SelectDateDialogFragment.newInstanceWithSingle(
//                tvValidDate.getText().toString().trim()
//        ).setOnSelectListener(ret -> {
//            if (!ret.isEmpty()) {
//                String[] dates = ret.toArray(new String[]{});
//                tvValidDate.setText(dates[0]);
//                mValidDate = dates[0];
//            }
//        }).show(getSupportFragmentManager());
    }

    private void selectAddress() {
        mCityPickerDialog.showPickerView(new CityPickerDialog.CitySelectListener() {
            @Override
            public void select(String province, String city, String county) {
                String address = province + " " + city + " " + county;
                tvAddress.setText(address);
                mProvince = province;
                mCity = city;
                mCounty = county;
            }
        });
    }

    @Override
    public void onConfirm(String authCode) {

        Observable<XYSHCommonResult<Object>> req = null;
        if (authType == AUTH_TYPE_REPAY) {
            req = service.authChannelCardOpen(channel.channelShortName, creditCard.cardNo, authCode);
        } else if (authType == AUTH_TYPE_RECEIPT) {
            req = service.gftqPay_cardOpen(creditCard.cardNo, authCode);
        }

        ToastUtil.showLoading(this, true);
        APIManager.startRequest(req,
                new Observer<XYSHCommonResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(XYSHCommonResult<Object> ret) {
                        ToastUtil.hideLoading();
                        if (ret.code == 1) {
                            EventBus.getDefault().post(new EventMessage(Event.authChannelSuccess));
                            ToastUtils.showLongToast(ret.msg + "");
                            finish();
                        } else {
                            String msg = ret.msg;
                            if (TextUtils.isEmpty(msg)) {
                                msg = "认证失败！";
                            }
                            ToastUtil.error(msg);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtil.hideLoading();
                    }

                    @Override
                    public void onComplete() {
                        ToastUtil.hideLoading();
                    }
                });
    }

    public static void start(Context context, QueryUserBankCardResult.BankInfListBean creditCard, ChannelItem channel) {
        Intent intent = new Intent(context, AuthChannelActivity.class);
        intent.putExtra("creditCard", creditCard);
        intent.putExtra("channel", channel);
        intent.putExtra("authType", AUTH_TYPE_REPAY);
        context.startActivity(intent);
    }

    public static void start(Context context, QueryUserBankCardResult.BankInfListBean creditCard,
                             QueryUserBankCardResult.BankInfListBean debitCard, ChannelItem channel) {
        Intent intent = new Intent(context, AuthChannelActivity.class);
        intent.putExtra("creditCard", creditCard);
        intent.putExtra("debitCard", debitCard);
        intent.putExtra("channel", channel);
        intent.putExtra("authType", AUTH_TYPE_RECEIPT);
        context.startActivity(intent);
    }
}
