package com.weiju.ccmall.module.notice.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.NoticeListModel;

import java.util.List;

public class NoticeAdapter extends BaseQuickAdapter<NoticeListModel.DatasEntity, BaseViewHolder> {

    public NoticeAdapter(@Nullable List<NoticeListModel.DatasEntity> data) {
        super(R.layout.item_notice, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, NoticeListModel.DatasEntity item) {
        helper.setText(R.id.tvTitle, item.title);
    }
}
