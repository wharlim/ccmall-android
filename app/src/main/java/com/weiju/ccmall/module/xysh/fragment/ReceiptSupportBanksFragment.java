package com.weiju.ccmall.module.xysh.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.adapter.ReceiptSupportBankAdapter;
import com.weiju.ccmall.module.xysh.bean.ReceiptSupportBankEntity;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class ReceiptSupportBanksFragment extends BaseFragment {

    public static final int TYPE_PAY = 1; // 付款卡
    public static final int TYPE_RECEIPT = 2; // 收款卡
    @BindView(R.id.tvCol1)
    TextView tvCol1;
    @BindView(R.id.tvCol2)
    TextView tvCol2;
    @BindView(R.id.tvCol3)
    TextView tvCol3;
    @BindView(R.id.tvCol4)
    TextView tvCol4;
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    Unbinder unbinder;

    private int type = TYPE_PAY;
    private String channelId;
    private ReceiptSupportBankAdapter adapter;
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);


    public void getIntentData() {
        channelId = getArguments().getString("channelId");
        type = getArguments().getInt("type");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_receipt_support_bank, container, false);
        unbinder = ButterKnife.bind(this, view);
        getIntentData();
        adapter = new ReceiptSupportBankAdapter(type);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ViewGroup headerView = view.findViewById(R.id.in_header);
        if (type == TYPE_RECEIPT) {
            setHeadView(headerView, "银行名称", "支持卡种");
        }
        getData(true);
    }

    public static void setHeadView(ViewGroup headerView, String col1, String col2) {
        ((TextView) headerView.findViewById(R.id.tvCol1)).setText(col1);
        ((TextView) headerView.findViewById(R.id.tvCol2)).setText(col2);
        ((TextView) headerView.findViewById(R.id.tvCol3)).setVisibility(View.GONE);
        ((TextView) headerView.findViewById(R.id.tvCol4)).setVisibility(View.GONE);
    }

    public static void setHeadView(ViewGroup headerView, String col1, String col2, String col3, String col4) {
        ((TextView) headerView.findViewById(R.id.tvCol1)).setText(col1);
        ((TextView) headerView.findViewById(R.id.tvCol2)).setText(col2);
        ((TextView) headerView.findViewById(R.id.tvCol3)).setText(col3);
        ((TextView) headerView.findViewById(R.id.tvCol4)).setText(col4);
    }


    public void getData(boolean isRefresh) {
        Observable<XYSHCommonResult<List<ReceiptSupportBankEntity>>> api = null;
        String token = SessionUtil.getInstance().getOAuthToken();
        if (type == TYPE_PAY) {
            api = service.getSupportBank(token, channelId, null);
        } else if (type == TYPE_RECEIPT) {
            api = service.getSupportBank(token, channelId, "1");
        } else {
            throw new IllegalArgumentException("type 不正确:" + type);
        }
        APIManager.startRequest(api, new Observer<XYSHCommonResult<List<ReceiptSupportBankEntity>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<List<ReceiptSupportBankEntity>> ret) {
                adapter.setNewData(ret.data);
                adapter.loadMoreEnd(true);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public static ReceiptSupportBanksFragment newInstance(int type, String channelId) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        args.putString("channelId", channelId);
        ReceiptSupportBanksFragment fragment = new ReceiptSupportBanksFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
