package com.weiju.ccmall.module.jkp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.home.HomeFragment;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.JkpLevel;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PopupWindowManage;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.page.CustomPageFragment;
import com.weiju.ccmall.shared.page.PageType;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import butterknife.BindView;
import butterknife.OnClick;

public class JiKaoPuNativeFragment extends HomeFragment implements TransLevelDialog.OnTransferListener,
        CustomPageFragment.OnLoadPageConfigListener {

    @BindView(R.id.userAvatar)
    ImageView userAvatar;

    @BindView(R.id.ivLevel)
    ImageView ivLevel;

    @BindView(R.id.viewYinying)
    View viewYinYing;
    PopupWindowManage mPopupWindowManage;
    JkpLevel jkpLevel;

    IJkpProductService jkpProductService = ServiceManager.getInstance().createService(IJkpProductService.class);

    public static JiKaoPuNativeFragment newInstance() {
        return new JiKaoPuNativeFragment();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchLayout.setGravity(Gravity.CENTER);
        userAvatar.setVisibility(View.VISIBLE);
        loadUserIcon();
        mPopupWindowManage = PopupWindowManage.getInstance(getContext());
    }

    private void loadUserIcon() {
        if (UiUtils.checkUserLogin(getContext())) {
            User user = SessionUtil.getInstance().getLoginUser();
            Glide.with(this)
                    .load(Uri.parse(user.avatar))
                    .apply(RequestOptions.placeholderOf(R.mipmap.default_avatar))
                    .into(userAvatar);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
//        Log.d("Seven", "JiKaoPuNativeFragment " + isVisibleToUser);
        if (isVisibleToUser && UiUtils.checkUserLogin(getContext())) {
            check(SessionUtil.getInstance().getLoginUser().id);
            tryToShowIntelligenceSearchDialog();
            loadUserIcon();
        }
    }

    private void check(String memId) {
        APIManager.startRequest(jkpProductService.checktkLevelStatus(memId), new BaseRequestListener<JkpLevel>() {
            @Override
            public void onSuccess(JkpLevel result) {
                jkpLevel = result;
                updateLevelIcon();
            }
        }, getContext());
    }

    private void updateLevelIcon() {
        if (jkpLevel.memberTkBean != null) {
            // 绑定了信息
            // 集靠谱等级：0，超级会员；6，经销商；8，团队长
            if (jkpLevel.memberTkBean.jkpLevel >= 0 && jkpLevel.memberTkBean.jkpLevel < 6) {
                ivLevel.setImageResource(R.drawable.ic_svip);
            } else if (jkpLevel.memberTkBean.jkpLevel >= 6 && jkpLevel.memberTkBean.jkpLevel < 8) {
                ivLevel.setImageResource(R.drawable.ic_lv1);
            } else if (jkpLevel.memberTkBean.jkpLevel >= 8) {
                ivLevel.setImageResource(R.drawable.ic_lv2);
            }
        }
    }

    @Override
    protected CustomPageFragment.OnLoadPageConfigListener getOnLoadPageConfigListener() {
        return this;
    }

    @Override
    public void onLoadPageConfig() {
        if (SessionUtil.getInstance().isLogin()) {
            check(SessionUtil.getInstance().getLoginUser().id);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getUserVisibleHint() && SessionUtil.getInstance().isLogin()) {
            tryToShowIntelligenceSearchDialog();
            loadUserIcon();
        }
    }

    private void showMenu() {
        View view = View.inflate(getContext(), R.layout.view_trans_level, null);
        TextView tvVipLv = view.findViewById(R.id.tvVipLevel);
        tvVipLv.setOnClickListener(v -> {
            mPopupWindowManage.dismiss();
            Intent intent = new Intent(getContext(), WebViewJavaActivity.class);
            intent.putExtra("url", BuildConfig.WECHAT_URL + "jkp/benifit/" + jkpLevel.memberTkBean.jkpLevel);
//            intent.putExtra("hideToolbar", true);
            getContext().startActivity(intent);
        });
//        tvVipLv.setText("会员等级");
        View tvTransLevel = view.findViewById(R.id.tvTransLevel);
        if (jkpLevel.isUp == 1) {
            tvTransLevel.setVisibility(View.VISIBLE);
            tvTransLevel.setOnClickListener(v -> {
                mPopupWindowManage.dismiss();
                TransLevelDialog.newInstance().show(getChildFragmentManager(), "TransLevelDialog");
            });
        } else {
            tvTransLevel.setVisibility(View.GONE);
        }
        mPopupWindowManage.setYinYing(viewYinYing);
        mPopupWindowManage.showWindowNormal(userAvatar, view, -ConvertUtil.dip2px(15));
    }

    private void tryToShowIntelligenceSearchDialog() {
        String clipboard = OutLinkUtils.getClipboard(true);
        if (shouldCallSuperSearch(clipboard)) {
            IntelligenceSearchDialog.newInstance(clipboard).show(getChildFragmentManager(),
                    "IntelligenceSearchDialog");
        }
    }

    private boolean shouldCallSuperSearch(String clipboard) {
        String regEx="[\n`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。， 、？]";
        String extraSpecialTxt = clipboard.replaceAll(regEx, "");
//        String tkl = "[^a-zA-Z\\d].+[^a-zA-Z\\d]";
//        boolean isTkl = Pattern.matches(tkl, clipboard);
        return !TextUtils.isEmpty(clipboard) && !(clipboard.startsWith("http://")||clipboard.startsWith("https://"))
                && extraSpecialTxt.length() >= 10;
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        if (childFragment instanceof TransLevelDialog) {
            ((TransLevelDialog) childFragment).setOnTransferListener(this);
        }
    }

    @Override
    public void onTrans() {
        String mId = SessionUtil.getInstance().getMemberId();
        ToastUtil.showLoading(getContext());
        APIManager.startRequest(jkpProductService.tkLevelTransform(mId), new BaseRequestListener<JkpLevel>() {
            @Override
            public void onSuccess(JkpLevel result) {
                ToastUtil.success("激活成功!");
                ToastUtil.hideLoading();
                jkpLevel = result;
                updateLevelIcon();
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.hideLoading();
            }
        }, getContext());
    }

    @OnClick(R.id.userAvatar)
    public void userCenter() {
        if (jkpLevel == null || jkpLevel.memberTkBean == null) {
//            ToastUtil.error("您还没授权绑定淘宝!");
//            OutLinkUtils.openTaobao(getActivity(), "");
        } else {
            showMenu();
        }
    }

    @Override
    protected boolean shouldSwitchEnv() {
        return false;
    }

    @Override
    protected PageType getPageType() {
        return PageType.JKP;
    }
}
