package com.weiju.ccmall.module.matisse;

import android.content.Context;

import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.zhihu.matisse.internal.entity.IncapableCause;
import com.zhihu.matisse.internal.entity.Item;
import com.zhihu.matisse.internal.model.SelectedItemCollection;
import com.zhihu.matisse.internal.utils.PhotoMetadataUtils;

/**
 * Created by 林先生_lcy.
 * User: lcy
 * Date: 2019/4/2
 * Time: 下午5:34
 */
public class ISelectedItemCollection extends SelectedItemCollection {
    public ISelectedItemCollection(Context context) {
        super(context);
    }

    @Override
    public IncapableCause isAcceptable(Item item) {
        if (maxSelectableReached()) {
            return new IncapableCause("选择的数量超出范围");
        } else if (typeConflict(item)) {
            return new IncapableCause(MyApplication.getInstance().getString(R.string.error_type_conflict));
        }
        return PhotoMetadataUtils.isAcceptable(MyApplication.getInstance(), item);
    }
}
