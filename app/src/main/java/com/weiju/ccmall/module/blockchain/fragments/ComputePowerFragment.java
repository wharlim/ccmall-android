package com.weiju.ccmall.module.blockchain.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.beans.ChartDataItem;
import com.weiju.ccmall.module.blockchain.utils.TemplateWebUtil;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBlockChain;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ComputePowerFragment extends BaseFragment {

    @BindView(R.id.rv_list)
    RecyclerView rvList;
    Adapter adapter;
    List<ChartDataItem> datas = new ArrayList<>();
    Unbinder unbinder;
    IBlockChain blockChain;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_compute_power, container, false);
        unbinder = ButterKnife.bind(this, view);
        adapter = new Adapter(getContext(), datas);
        rvList.setAdapter(adapter);
        rvList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        blockChain = ServiceManager.getInstance().createService(IBlockChain.class);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    private void loadData() {
        APIManager.startRequest(blockChain.getDayCcm(), new BaseRequestListener<List<ChartDataItem>>() {
            @Override
            public void onSuccess(List<ChartDataItem> result) {
                super.onSuccess(result);
                datas.clear();
                if (result != null) {
                    datas.addAll(result);
                    adapter.notifyDataSetChanged();
                }
            }
        }, getContext());
    }

    public static ComputePowerFragment newInstance() {
        Bundle args = new Bundle();
        ComputePowerFragment fragment = new ComputePowerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public static class Adapter extends RecyclerView.Adapter<Adapter.VH>{

        List<ChartDataItem> datas;
        LayoutInflater inflater;

        public Adapter(Context context, List<ChartDataItem> datas) {
            this.datas = datas;
            inflater = LayoutInflater.from(context);
        }

        @NonNull
        @Override
        public VH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View itemView = inflater.inflate(R.layout.item_compute_power, viewGroup, false);
            return new VH(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull VH vh, int i) {
            vh.bindView(datas.get(i));
        }

        @Override
        public int getItemCount() {
            return datas.size();
        }

        public static class VH extends RecyclerView.ViewHolder {
            @BindView(R.id.tv_title)
            TextView title;
            @BindView(R.id.wv_chart)
            WebView chart;

            public VH(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                setUpWebView();
            }

            public void setUpWebView() {
                // 设置背景色
                chart.setBackgroundColor(0);
                // 设置填充透明度
//                chart.getBackground().setAlpha(0);
            }

            public void bindView(ChartDataItem dataItem) {
                title.setText(dataItem.name);
                TemplateWebUtil.loadChart(chart, dataItem.templateId, new Gson().toJson(dataItem.values), dataItem.name);
            }
        }
    }
}
