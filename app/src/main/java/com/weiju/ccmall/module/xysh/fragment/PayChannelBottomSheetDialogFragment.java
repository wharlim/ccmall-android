package com.weiju.ccmall.module.xysh.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class PayChannelBottomSheetDialogFragment extends BottomSheetDialogFragment {

    Unbinder unbinder;
    PayChannelFragment channelFragment;
    PayChannelFragment.OnChannelClickListener onChannelClickListener;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    private String txt;

    public static PayChannelBottomSheetDialogFragment newInstance(QueryUserBankCardResult.BankInfListBean payCard, QueryUserBankCardResult.BankInfListBean receiptCard) {
        PayChannelBottomSheetDialogFragment fragment = new PayChannelBottomSheetDialogFragment();
        fragment.channelFragment = PayChannelFragment.newInstance(payCard, receiptCard);
        fragment.txt = "请根据您的需求，选择收款通道";
        return fragment;
    }

    public static PayChannelBottomSheetDialogFragment newInstance(QueryUserBankCardResult.BankInfListBean creditCard, int channelType) {
        PayChannelBottomSheetDialogFragment fragment = new PayChannelBottomSheetDialogFragment();
        fragment.channelFragment = PayChannelFragment.newInstance(creditCard, channelType);
        fragment.txt = "请根据您的需求，选择还款通道";
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_pay_channel, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getChildFragmentManager().beginTransaction().add(R.id.flChannelContainer, channelFragment)
                .commit();
        tvTitle.setText(txt);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setCancelable(false);
        getDialog().setCanceledOnTouchOutside(true);
    }

    private BottomSheetBehavior<View> mBottomSheetBehavior;

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            View bottomSheet = dialog.findViewById(R.id.design_bottom_sheet);
            bottomSheet.getLayoutParams().height = SizeUtils.dp2px(503);
        }
        final View view = getView();
        view.post(() -> {
            View parent = (View) view.getParent();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) (parent).getLayoutParams();
            CoordinatorLayout.Behavior behavior = params.getBehavior();
            mBottomSheetBehavior = (BottomSheetBehavior) behavior;
            mBottomSheetBehavior.setPeekHeight(SizeUtils.dp2px(503));
            parent.setBackgroundColor(Color.TRANSPARENT);
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setOnChannelClickListener(PayChannelFragment.OnChannelClickListener onChannelClickListener) {
        this.onChannelClickListener = onChannelClickListener;
    }

    @OnClick(R.id.tvNext)
    public void onViewClicked() {
        if (channelFragment.getSelectChannel() != null) {
            onChannelClickListener.onChannelClick(-1, channelFragment.getSelectChannel());
        } else {
            ToastUtil.error("请选择通道！");
        }
    }
}
