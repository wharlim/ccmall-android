package com.weiju.ccmall.module.user;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.blankj.utilcode.utils.AppUtils;
import com.blankj.utilcode.utils.FileUtils;
import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.ScreenUtils;
import com.google.common.collect.Lists;
import com.squareup.picasso.Picasso;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.InvitationCode;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.component.dialog.ShareDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IInviteService;
import com.weiju.ccmall.shared.util.ImageUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.user
 * @since 2017-08-03
 */
public class QRCodeActivity extends BaseActivity {

    @BindView(R.id.ivQrCode)
    protected ImageView mQrCodeIv;
    @BindView(R.id.llBottom)
    LinearLayout mLlBottom;
    private InvitationCode mInvitationCode;

    public static final String TYPE_InvitationCodeByIcon = "InvitationCodeByIcon";
    public static final String TYPE_InvitationCodeByIconCreditCard = "InvitationCodeByIconCreditCard";
    private String type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qrcode);
        ButterKnife.bind(this);
        type = getIntent().getStringExtra("type");
        showHeader();
        setLeftBlack();

        int height = ScreenUtils.getScreenWidth() * 16 / 9;
        mQrCodeIv.getLayoutParams().height = height;

        IInviteService inviteService = ServiceManager.getInstance().createService(IInviteService.class);
        Observable<RequestResult<InvitationCode>> api = inviteService.getInvitationCodeByIcon();
        if (TYPE_InvitationCodeByIcon.equals(type)) {
            setTitle("我的二维码");
            api = inviteService.getInvitationCodeByIcon();
        } else if (TYPE_InvitationCodeByIconCreditCard.equals(type)) {
            setTitle("邀请好友");
            api = inviteService.getInvitationCodeByIconCreditCard();
        } else {
            setTitle("请设置类型");
        }
        APIManager.startRequest(api, new BaseRequestListener<InvitationCode>(this) {
            @Override
            public void onSuccess(InvitationCode result) {
                mInvitationCode = result;
                Picasso.with(QRCodeActivity.this).load(result.imgUrl).into(mQrCodeIv);
                if (TYPE_InvitationCodeByIconCreditCard.equals(type)) mLlBottom.setVisibility(View.VISIBLE);
            }
        }, this);
    }

    @OnClick(R.id.ivQrCode)
    protected void preview() {
        if (mInvitationCode == null) {
            return;
        }
        ArrayList<String> images = Lists.newArrayList(mInvitationCode.imgUrl);
        ImageUtil.previewImage(this, images, 0, true);
    }

    public static void start(Context context, String type) {
        Intent intent = new Intent(context, QRCodeActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }


    @OnClick(R.id.tvSave)
    protected void save() {
        ToastUtil.showLoading(this);
        Bitmap viewBitmap = getViewBitmap(mQrCodeIv);
        File savedImageFile = getSaveFile();
        saveBitmapFile(viewBitmap, savedImageFile, true);
        ToastUtil.hideLoading();
    }

    @OnClick(R.id.tvShare)
    protected void share() {
        ToastUtil.showLoading(this);
        Bitmap viewBitmap = getViewBitmap(mQrCodeIv);
        final File savedImageFile = getSaveFile();
        saveBitmapFile(viewBitmap, savedImageFile, false);
        Luban.with(this)
                .load(savedImageFile)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                        LogUtils.e("压缩图片开始");
                    }

                    @Override
                    public void onSuccess(File file) {
                        savedImageFile.delete();
                        ToastUtil.hideLoading();
                        showShareDialog(file);
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtil.error("压缩图片出错:" + e.getMessage());
                        ToastUtil.hideLoading();
                    }
                }).launch();
    }

    private void showShareDialog(final File file) {
        new ShareDialog(this, file, new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                ToastUtil.showLoading(QRCodeActivity.this);
            }

            @Override
            public void onResult(SHARE_MEDIA share_media) {
                ToastUtil.success("分享成功");
                ToastUtil.hideLoading();
                file.delete();
            }

            @Override
            public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                ToastUtil.error("分享出错:" + throwable.getMessage());
                ToastUtil.hideLoading();
                file.delete();
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media) {
                ToastUtil.hideLoading();
                file.delete();
            }
        }).show();
    }

    public static File getSaveFile() {
        String dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath()
                + "/" + AppUtils.getAppName(MyApplication.getContext());
        File file = new File(dirPath, System.currentTimeMillis() + ".jpg");
        FileUtils.createOrExistsDir(dirPath);
        return file;
    }

    public static void saveBitmapFile(Bitmap bitmap, final File saveFile, boolean isShowMsg) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(saveFile));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
            if (isShowMsg) {
                ToastUtil.success("保存成功");
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                intent.setData(Uri.fromFile(saveFile));
                MyApplication.getContext().sendBroadcast(intent);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Bitmap getViewBitmap(View v) {
        v.clearFocus();
        v.setPressed(false);
        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);
        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            return null;
        }
        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);
        return bitmap;
    }

}
