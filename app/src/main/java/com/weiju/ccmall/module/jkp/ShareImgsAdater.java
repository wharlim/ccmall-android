package com.weiju.ccmall.module.jkp;

import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ShareImgsAdater extends BaseQuickAdapter<String, BaseViewHolder> {

    private ArrayList<String> selected = new ArrayList<>();

    public ShareImgsAdater() {
        super(R.layout.item_share_img);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        ImageView iv = helper.getView(R.id.ivShareImgItem);
        Glide.with(iv)
                .load(Uri.fromFile(new File(item)))
                .into(iv);
        ImageView state = helper.getView(R.id.ivShareImgItemSelectState);
        state.setImageResource(selected.contains(item)?R.drawable.ic_share_checked : R.drawable.ic_share_uncheck);

        helper.itemView.setOnClickListener(v -> {
            if (selected.contains(item)) {
                selected.remove(item);
            } else {
                selected.add(item);
            }
            notifyDataSetChanged();
        });
    }

    public ArrayList<String> getSelected() {
        return selected;
    }
}
