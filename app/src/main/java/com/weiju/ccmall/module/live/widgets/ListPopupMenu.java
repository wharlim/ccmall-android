package com.weiju.ccmall.module.live.widgets;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.adapter.ListPopupAdapter;
import com.weiju.ccmall.shared.bean.RefundType;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.util.UiUtils;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListPopupMenu extends PopupWindow {
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    private ListPopupAdapter mListPopupAdapter;

    private OnMenuClickListener mOnMenuClickListener;

    public ListPopupMenu(Context context, OnMenuClickListener onMenuClickListener) {
        super(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        View contentView = LayoutInflater.from(context).inflate(R.layout.view_list_popup, null);
        ButterKnife.bind(this, contentView);
        setContentView(contentView);
        mOnMenuClickListener = onMenuClickListener;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setElevation(10);
        }
        setOutsideTouchable(true);
        setFocusable(true);
        initView(context);
    }

    private void initView(Context context) {
        mListPopupAdapter = new ListPopupAdapter();
        UiUtils.configRecycleView(mRecyclerView, new LinearLayoutManager(context), false);
        mRecyclerView.addItemDecoration(new ListDividerDecoration(context));
        mRecyclerView.setAdapter(mListPopupAdapter);
        mListPopupAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                RefundType item = mListPopupAdapter.getItem(position);
                if (mOnMenuClickListener != null && item != null) {
                    mOnMenuClickListener.onMenuClick(item.key, item.values);
                }
                dismiss();
            }
        });
    }

    public void setData(List<RefundType> result){
        mListPopupAdapter.setNewData(result);
    }

    public interface OnMenuClickListener {
        void onMenuClick(String menuIndex, String title);
    }
}
