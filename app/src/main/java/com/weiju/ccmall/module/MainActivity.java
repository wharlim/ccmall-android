package com.weiju.ccmall.module;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.TimeUtils;
import com.google.common.collect.Lists;
import com.orhanobut.logger.Logger;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMManager;
import com.umeng.analytics.MobclickAgent;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.blockchain.services.TemplateDownloadServices;
import com.weiju.ccmall.module.category.CategoryFragment;
import com.weiju.ccmall.module.community.publish.LinkPiblishHisActivity;
import com.weiju.ccmall.module.community.publish.PublishDailog;
import com.weiju.ccmall.module.community.publish.PublishHisActivity;
import com.weiju.ccmall.module.community.publish.PublishPicActivity;
import com.weiju.ccmall.module.home.HomeFragment;
import com.weiju.ccmall.module.jkp.OutLinkUtils;
import com.weiju.ccmall.module.jkp.newjkp.JiKaoPuFragment;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.module.live.fragment.LiveHomeFragment;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.page.WebViewShotShareActivity;
import com.weiju.ccmall.module.user.LoginActivity;
import com.weiju.ccmall.module.user.UserCenterFragment;
import com.weiju.ccmall.module.xysh.fragment.XinYongHomeFragment;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.activity.HiGouDescriptionActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.AgentFActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.MainAdModel;
import com.weiju.ccmall.shared.bean.PopupOrderList;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.bean.event.MsgFViewPager;
import com.weiju.ccmall.shared.component.MainAdView;
import com.weiju.ccmall.shared.component.NoScrollViewPager;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.CartManager;
import com.weiju.ccmall.shared.manager.PushManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.IAdService;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.PicassoEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.rong.imlib.RongIMClient;
import rx.functions.Action1;


public class MainActivity extends BaseActivity {

    @BindView(R.id.tabLayout)
    protected LinearLayout mTabLayout;
//    @BindViews({R.id.tabHomeLayout, R.id.xysh, R.id.tabNewRetailLayout, R.id.tabJKPLayout, R.id.tabMeLayout})
    @BindViews({R.id.tabHomeLayout, R.id.xysh, R.id.tabLiveHomeLayout, R.id.tabJKPLayout, R.id.tabMeLayout})
    protected List<View> mTabs;
    @BindView(R.id.viewPager)
    protected NoScrollViewPager mViewPager;
    @BindView(R.id.cartBadgeTv)
    protected TextView mCartBadgeTv;
    @BindView(R.id.tvLiveHomeBadge)
    protected TextView mTvLiveHomeBadge;

    AlertDialog alertDialog;
    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);


    protected ArrayList<Fragment> mFragments = new ArrayList<>();
    private String mCurrentTab = "home";
    //    "category",
    private List<String> mTabNames = Lists.newArrayList("home", "xinying", "jkp", "near", "user-center");
    private final int SHOW_TOAST = 0x111;
    private final int GET_TOAST_DATA = 0x222;

    private Handler mToastHandler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == SHOW_TOAST) {
                showOrderToast();
            } else if (msg.what == GET_TOAST_DATA) {
                getOrderToastList();
            }
        }
    };
    private List<PopupOrderList.DatasEntity> mToastDatas = new ArrayList<>();

    private int currentIndex = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initSplash();
        initStatusBar();
        ButterKnife.bind(this);
//        startTestScrollShotWebView();
//        startTestShareRemoteImg();

        tIMAutoLoginOrLogout();
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, MainActivity.this);
                }
            });
        }

        getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
        mFragments.add(new HomeFragment());
        mFragments.add(new XinYongHomeFragment());
//        mFragments.add(CartFragment.newInstance(AppTypes.CART.FROM_HOME));
//        mFragments.add(new NewRetailFragment());
        mFragments.add(new LiveHomeFragment());
        mFragments.add(JiKaoPuFragment.newInstance());
//        mFragments.add(JiKaoPuNativeFragment.newInstance());
        mFragments.add(new UserCenterFragment());

        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }
        });
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                onClickTabItems(mTabs.get(position));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                    Window window = MainActivity.this.getWindow();
//                    if (position == 1) {
//                        window.setStatusBarColor(getResources().getColor(R.color.xinyong_title));
//                        QMUIStatusBarHelper.setStatusBarDarkMode(MainActivity.this);
//                    } else if (position == mFragments.size() - 1) {
//                        window.setStatusBarColor(Color.parseColor("#f51861"));
//                        QMUIStatusBarHelper.setStatusBarDarkMode(MainActivity.this);
//                    } else {
//                        window.setStatusBarColor(Color.WHITE);
//                        QMUIStatusBarHelper.setStatusBarLightMode(MainActivity.this);
//                    }
                    if (position == 1 || position == mFragments.size() - 1) {
                        QMUIStatusBarHelper.setStatusBarDarkMode(MainActivity.this);
                    } else {
                        QMUIStatusBarHelper.setStatusBarLightMode(MainActivity.this);
                    }
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mViewPager.setOffscreenPageLimit(4);
        getParam();
        initTab();
        EventBus.getDefault().register(this);

        checkPermissions();
        getUserInfo();
        // 启动服务下载区块链图表模板
        TemplateDownloadServices.start(this);
//        stopLive();
        LiveManager.getRongyunToken(MainActivity.this, null, null, null, 0, false);
    }


    /**
     * 上次意外退出app没来得及结束的直播，这里执行结束
     */
//    private void stopLive() {
//        String startLiveId = SharedPreferenceUtils.getStartLiveId(MainActivity.this);
//        if (SessionUtil.getInstance().isLogin() &&
//                !TextUtils.isEmpty(startLiveId)) {
//
//            ChatroomEndMessage chatroomEndMessage = new ChatroomEndMessage();
//            ChatroomKit.sendMessage(chatroomEndMessage,false);
//            ILiveService liveService = ServiceManager.getInstance().createService(ILiveService.class);
//            APIManager.startRequest(liveService.stopLive(startLiveId), new BaseRequestListener<Object>() {
//                @Override
//                public void onSuccess(Object result) {
//                }
//
//                @Override
//                public void onError(Throwable e) {
//                }
//            },MainActivity.this);
//        }
//    }

    private void checkPermissions() {
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_PHONE_STATE
        ).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (!aBoolean) {
                    MyApplication.CAN_OPEN_LIVE = false;
                    ToastUtil.error("请允许 app 获取相关权限，否则将导致部分功能无法使用");
                } else {
                    MyApplication.CAN_OPEN_LIVE = true;
                }
            }
        });
    }

    private void initStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            QMUIStatusBarHelper.translucent(MainActivity.this);
            QMUIStatusBarHelper.setStatusBarLightMode(MainActivity.this);
        }
    }


    private void tIMAutoLoginOrLogout() {
        // TIM自动登录
        String sig = SessionUtil.getInstance().getTencentIMUserSig();
        String id = SessionUtil.getInstance().getMemberId();
        if (!TextUtils.isEmpty(sig) && !TextUtils.isEmpty(id)) {
            TIMManager.getInstance().autoLogin(id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                    TIMManager.getInstance().login(id, sig, new TIMCallBack() {
                        @Override
                        public void onError(int i, String s) {
                            Log.e("TIM", "TIM 自动登录失败!");
                            ToastUtil.error("即时聊天登录失败，请重新登录");
                        }

                        @Override
                        public void onSuccess() {
                            Log.d("TIM", "TIM 重新登录成功!");
                        }
                    });
                }

                @Override
                public void onSuccess() {
                    Log.d("TIM", "TIM 自动登录成功!");
                }
            });
        } else {
            logout();
        }
    }

    private void logout() {
        OutLinkUtils.aliLogout(this);
        if (SessionUtil.getInstance().isLogin()) {
            onClickTabItems(mTabs.get(0));
            UserService.logoutWithoutEvent(MainActivity.this);
            RongIMClient.getInstance().logout();
            MobclickAgent.onProfileSignOff();
            SessionUtil.getInstance().logout();
            PushManager.setJPushInfo(MainActivity.this, null);
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
        }
    }


    private void getUserInfo() {
        if (SessionUtil.getInstance().isLogin()) {
            APIManager.startRequest(mUserService.getUserInfo(), new BaseRequestListener<User>() {
                @Override
                public void onSuccess(User result) {
                    super.onSuccess(result);
                    SessionUtil.getInstance().setLoginUser(result);
                }
            }, this);
        }
    }

    private void initSplash() {
        initAdDialog();
        mIsAllowShowRedDialog = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtils.e("onResume!!!!!");
        getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, "").commit();
        mToastHandler.sendEmptyMessageDelayed(SHOW_TOAST, TimeUtils.getNowTimeMills() % 10 * 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LogUtils.e("onPause!!!!!");
        mToastHandler.removeMessages(SHOW_TOAST);
        mToastHandler.removeMessages(GET_TOAST_DATA);
    }

    private void initAdDialog() {
        final WJDialog wjDialog = new WJDialog(this);
        final MainAdView mainAdView = new MainAdView(this);
        IAdService service = ServiceManager.getInstance().createService(IAdService.class);
        APIManager.startRequest(service.getMainAd(), new BaseRequestListener<MainAdModel>(this) {

            @Override
            public void onSuccess(MainAdModel result) {
                if (StringUtils.isEmpty(result.backUrl)) {
                    return;
                }
                wjDialog.show();
                mainAdView.setCloseClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        wjDialog.dismiss();
                    }
                });
                mainAdView.setData(result);
                wjDialog.setContentView(mainAdView);
            }
        }, this);
    }

    private boolean mIsAllowShowRedDialog = false;
    private long mLastTime = 0;


    private void getOrderToastList() {
        IProductService service = ServiceManager.getInstance().createService(IProductService.class);
        APIManager.startRequest(service.getPopupOrderList(1, Constants.PAGE_SIZE), new BaseRequestListener<PopupOrderList>() {
            @Override
            public void onSuccess(final PopupOrderList result) {
                if (result.datas.size() > 0) {
                    mToastDatas.clear();
                    mToastDatas.addAll(result.datas);
                    mToastHandler.sendEmptyMessageDelayed(SHOW_TOAST, TimeUtils.getNowTimeMills() % 10 * 1000);
                }
            }
        }, this);
    }

    private void showOrderToast() {
        if (mToastDatas.size() > 0) {
            PopupOrderList.DatasEntity datasEntity = mToastDatas.get(0);
            ToastUtil.showOrderToast(this, datasEntity);
            mToastDatas.remove(datasEntity);

            mToastHandler.removeMessages(SHOW_TOAST);
            mToastHandler.sendEmptyMessageDelayed(SHOW_TOAST, 8 * 1000 + TimeUtils.getNowTimeMills() % 5 * 1000);
        } else {
            mToastHandler.removeMessages(GET_TOAST_DATA);
            mToastHandler.sendEmptyMessageDelayed(GET_TOAST_DATA, 10 * 1000);
        }
    }


    void initTab() {
        if (!mTabs.isEmpty()) {
            if (mTabNames.contains(mCurrentTab)) {
                onClickTabItems(mTabs.get(mTabNames.indexOf(mCurrentTab)));
            } else {
                onClickTabItems(mTabs.get(0));
            }
        }
    }

    void getParam() {
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            mCurrentTab = getIntent().getExtras().getString("tab");
            mCurrentTab = mCurrentTab == null ? "home" : mCurrentTab;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @OnClick({R.id.tabHomeLayout, R.id.xysh, R.id.tabNewRetailLayout, R.id.tabCartLayout, R.id.tabMeLayout, R.id.tabJKPLayout,R.id.tabLiveHomeLayout})
    protected void onClickTabItems(View view) {
        if (view.getId() == R.id.tabNewRetailLayout) {

            boolean isRead = getSharedPreferences(Const.READ_HIGOU_NEED_TO_KNOW, 0).getBoolean(Const.READ_HIGOU_NEED_TO_KNOW, false);
            if (!isRead) {

                View view1 = LayoutInflater.from(this).inflate(R.layout.check_higou_need_to_know_dialog, null);
                RelativeLayout rl_to_go_read = view1.findViewById(R.id.rl_to_go_read);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                if (alertDialog == null && SessionUtil.getInstance().isLogin()) {
                    alertDialog = builder.create();
                    alertDialog.setView(view1, 0, 0, 0, 0);
                    alertDialog.setCancelable(false);
                    alertDialog.show();
                    alertDialog.setOnKeyListener(onKeyListener1);

                    rl_to_go_read.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(MainActivity.this, HiGouDescriptionActivity.class));
                        }
                    });

                }
            }
        }
        int index = mTabs.indexOf(view);
        currentIndex = index;
        if ((index > 0) && !SessionUtil.getInstance().isLogin() && index != 2) {
            ToastUtil.error(Config.NET_MESSAGE.NO_LOGIN);
            EventBus.getDefault().post(new EventMessage(Event.goToLogin));
            return;
        }
        if (index == mViewPager.getCurrentItem()) {
            view.setSelected(true);
            return;
        } else {
            // 主动调用生命周期函数，会发生未知错误
//            if (index == 2) {
//                Fragment fragment = mFragments.get(2);
//                fragment.onResume();
//            }
            mViewPager.setCurrentItem(index, false);
        }
        for (View tab : mTabs) {
            tab.setSelected(tab == view);
        }
        mCurrentTab = mTabNames.get(index);

    }

    private DialogInterface.OnKeyListener onKeyListener1 = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if ((i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_DOWN) || i == 4) {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();

                    alertDialog = null;
                    mViewPager.setCurrentItem(0, false);
                    mTabs.get(2).setSelected(false);
                } else {
                    finish();
                }

            } /*else if (i == 4) {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }*/
            return true;
        }

    };

    /*@OnClick({R.id.xysh, R.id.xyshImg})
    protected void xysh(View v) {
//        /customizYTH/#/
        if (SessionUtil.getInstance().isLogin()) {
            Intent intent = new Intent(this, WebViewJavaActivity.class);
            *//*intent.putExtra("url", "http" + (BuildConfig.DEBUG ? "" : "s") + "://" + BuildConfig.XYSH_HOST + "/customizYTH/#/?phone=" +
                    SessionUtil.getInstance().getLoginUser().phone + "&token=" + SessionUtil.getInstance().getOAuthToken()
                    + "&version=" + BuildConfig.VERSION_NAME);*//*
            intent.putExtra("url", "http" + (BuildConfig.DEBUG ? "" : "") + "://" + BuildConfig.XYSH_HOST + "/customizYTH/#/?phone=" +
                    SessionUtil.getInstance().getLoginUser().phone + "&token=" + SessionUtil.getInstance().getOAuthToken()
                    + "&version=" + BuildConfig.VERSION_NAME);
            intent.putExtra("hideToolbar", true);
            intent.putExtra("showWindow", true);
            startActivity(intent);
//            AgentFActivity.Companion.start(this, PayWebFragment.class, null);
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }*/

    @Override
    protected void onStart() {
        super.onStart();
        if (SessionUtil.getInstance().isLogin()) {
            CartManager.getAmount();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case viewHome:
                onClickTabItems(mTabs.get(0));
                break;
            case xinyong:
                onClickTabItems(mTabs.get(1));
                break;
            case viewCategory:
                getSharedPreferences(Const.CLASSIFICATION_TYPE, 0).edit().putString(Const.CLASSIFICATION_TYPE, Const.NORMAL_MODE).commit();
                AgentFActivity.Companion.start(this, CategoryFragment.class, null);
//                onClickTabItems(mTabs.get(1));
//                EventBus.getDefault().postSticky(new EventMessage(Event.changeCategory, message.getData()));
                break;
            case viewNearStore:
                onClickTabItems(mTabs.get(1));
                break;
            case viewUserCenter:
                onClickTabItems(mTabs.get(3));
                break;
            case viewCart:
                if (SessionUtil.getInstance().isLogin()) {
                    onClickTabItems(mTabs.get(1));
                } else {
                    ToastUtil.error(Config.NET_MESSAGE.NO_LOGIN);
                    EventBus.getDefault().post(new EventMessage(Event.goToLogin));
                }
                break;
            case cartAmountUpdate:
                int total = (int) message.getData();
                mCartBadgeTv.setText(total > 99 ? "99+" : String.valueOf(total));
                mCartBadgeTv.setVisibility(total > 0 ? View.VISIBLE : View.GONE);
                break;
            case logout:
                mCartBadgeTv.setText("");
                mCartBadgeTv.setVisibility(View.GONE);
                mTvLiveHomeBadge.setText("");
                mTvLiveHomeBadge.setVisibility(View.GONE);
                logout();
                break;
            case goToLogin:
                Logger.e("跳登录");
                Intent intent = new Intent(this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                onClickTabItems(mTabs.get(0));

                break;
            case goMain:
                onClickTabItems(mTabs.get(0));
                break;

            case goToLiveHome:
                onClickTabItems(mTabs.get(2));
                break;
            case liveHomeMessageUpdate:
                long count = (long) message.getData();
                mTvLiveHomeBadge.setText(count > 99 ? "99+" : String.valueOf(count));
                mTvLiveHomeBadge.setVisibility(count > 0 ? View.VISIBLE : View.GONE);
                break;

            case isReadHigou:
                alertDialog.dismiss();
                break;
            /*if (message.getEvent().equals(Event.isReadHigou)) {
                alertDialog.dismiss();
            }*/
            default:
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatusMsg(MsgStatus message) {
        switch (message.getAction()) {
            case MsgStatus.ACTION_EDIT_PHONE:
                Log.d("Seven", "去guangu");
                onClickTabItems(mTabs.get(0));
                break;
            default:
        }
    }

    private PublishDailog mDailog;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void publishInfo(MsgFViewPager msgFViewPager) {
        if (!SessionUtil.getInstance().isLogin()) {
            return;
        }
        if (mDailog == null) {
            mDailog = new PublishDailog(this);
            mDailog.setOnClickCallBack(new PublishCallBackImpl());
        }
        mDailog.show();
        mDailog.setLinkModel(msgFViewPager.getAction() == MsgFViewPager.ACTION_ADD_CLICK_LINK);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void stickEvent(EventMessage message) {
        if (message.getEvent().equals(Event.select2Fragment)) {
            onClickTabItems(mTabs.get(2));
        }
    }

    public class PublishCallBackImpl implements PublishDailog.onClickCallBack {

        @Override
        public void onTakePic(boolean linkeModel) {
            gotoSelectPic(linkeModel ? PublishPicActivity.TYPE_LINKE_IMAGE : PublishPicActivity.TYPE_IMAGE);
        }

        @Override
        public void onTakeVideo() {
            gotoSelectPic(PublishPicActivity.TYPE_VIDEO);
        }

        @Override
        public void onHistoryClick(boolean isLinkModel) {
            Intent intent;
            if (isLinkModel) {
                intent = new Intent(MainActivity.this, LinkPiblishHisActivity.class);
            } else {
                intent = new Intent(MainActivity.this, PublishHisActivity.class);
            }
            startActivity(intent);
        }

        @Override
        public void onTakeLink() {
            gotoSelectPic(PublishPicActivity.TYPE_LINKE);
        }
    }

    private void gotoSelectPic(final int requestCode) {
        RxPermissions rxPermissions = new RxPermissions(this);
        rxPermissions.request(Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (aBoolean) {
                    gotoMatisseActivity(requestCode);
                } else {
                    Toast.makeText(MainActivity.this, "权限拒绝，无法使用，请打开权限", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void gotoMatisseActivity(int requestCode) {
        Matisse.from(this)
                .choose(requestCode != PublishPicActivity.TYPE_VIDEO ? MimeType.ofImage() : MimeType.ofVideo())
                .captureStrategy(
                        new CaptureStrategy(true, "com.weiju.ddmall.fileprovider"))
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(requestCode != PublishPicActivity.TYPE_VIDEO && requestCode != PublishPicActivity.TYPE_LINKE ? 9 : 1)
                .imageEngine(new PicassoEngine())
                .forResult(requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            return;
        }
        ArrayList<Uri> uris = (ArrayList<Uri>) Matisse.obtainResult(data);
        if (StringUtil.isNullOrEmpty(uris)) {
            return;
        }
        Intent intent = new Intent(this, PublishPicActivity.class);
        intent.putParcelableArrayListExtra(Constants.KEY_EXTROS, uris);
        intent.putExtra("type", requestCode);
        startActivity(intent);
    }

    /**
     * 社区刷新
     */
    private void refreshCommunity(int index) {
        Fragment fragment = mFragments.get(index);
        if (!fragment.isAdded()) {
            return;
        }
        List<Fragment> fragments = mFragments.get(index).getChildFragmentManager().getFragments();
        if (fragments.size() > 2) {
            List<Fragment> fs = fragments.get(2).getChildFragmentManager().getFragments();
            for (Fragment f : fs) {
                try {
                    Method requestData = f.getClass().getDeclaredMethod("requestData");
                    requestData.setAccessible(true);
                    requestData.invoke(f);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (currentIndex == 2) {
            EventBus.getDefault().post(new EventMessage(Event.onKeyDown));
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

    private void startTestScrollShotWebView() {
        Intent intent = new Intent(this, WebViewJavaActivity.class);
        intent.putExtra("url", "https://github.com");
        intent.putExtra("isTestScrollShot", true);
        intent.putExtra("showWindow", true);
        startActivity(intent);
    }

    private void startTestShareRemoteImg() {
        WebViewShotShareActivity.start(this,
                "https://github.com/stevenMieMie/subsampling-scale-image-view-demo/blob/master/art/01.png?raw=true");
    }
}
