package com.weiju.ccmall.module.jkp;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.page.WebViewJavaActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class TransLevelDialog extends DialogFragment {

    @BindView(R.id.tvCheckRule)
    TextView tvCheckRule;
    @BindView(R.id.ivClose)
    ImageView ivClose;
    Unbinder unbinder;

    public static TransLevelDialog newInstance() {
        TransLevelDialog dialog = new TransLevelDialog();
        Bundle args = new Bundle();
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_trans_level, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        getDialog().setCanceledOnTouchOutside(false);
    }

    @OnClick({R.id.tvCheckRule, R.id.ivClose, R.id.ivTrans})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvCheckRule:
                Intent intent = new Intent(getContext(), WebViewJavaActivity.class);
                intent.putExtra("url", BuildConfig.WECHAT_URL + "jkp/public");
                getContext().startActivity(intent);
                break;
            case R.id.ivClose:
                dismiss();
                break;
            case R.id.ivTrans:
                dismiss();
                if (mOnTransferListener != null) {
                    mOnTransferListener.onTrans();
                }
                break;
        }
    }

    private OnTransferListener mOnTransferListener;

    public void setOnTransferListener(OnTransferListener onTransferListener) {
        mOnTransferListener = onTransferListener;
    }

    public interface OnTransferListener {
        void onTrans();
    }
}
