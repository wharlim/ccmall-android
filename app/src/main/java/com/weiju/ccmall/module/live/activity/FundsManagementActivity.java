package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.fragment.FundsManagementFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/5/12.
 */
public class FundsManagementActivity extends BaseActivity {
    private String[] mTitle = {"待结算", "货款账户"};
    public static int TYPE_SETTLE = 0;
    public static int TYPE_LOAN = 1;

    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;
    @BindView(R.id.tabLayout)
    protected TabLayout mTabLayout;
    private FragmentPagerAdapter fragmentStatePagerAdapter;
    ArrayList<BaseFragment> baseFragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_funds_management);
        mHeaderLayout.setVisibility(View.GONE);
        ButterKnife.bind(this);
        initViewPager();
    }

    private void initViewPager() {
        int type = getIntent().getIntExtra("type", TYPE_SETTLE);
        baseFragments.add(FundsManagementFragment.newInstance(TYPE_SETTLE));
        baseFragments.add(FundsManagementFragment.newInstance(TYPE_LOAN));
        fragmentStatePagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return baseFragments.get(position);
            }

            @Override
            public int getCount() {
                return baseFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mTitle[position];
            }
        };
        mTabLayout.setTabIndicatorFullWidth(false);
        mViewPager.setAdapter(fragmentStatePagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(baseFragments.size());
        mViewPager.setCurrentItem(type == TYPE_SETTLE ? TYPE_SETTLE : TYPE_LOAN);
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

    public static void start(Context context, int type) {
        Intent intent = new Intent(context, FundsManagementActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }


}
