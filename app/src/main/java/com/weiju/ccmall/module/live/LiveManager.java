package com.weiju.ccmall.module.live;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.InputType;
import android.text.TextUtils;

import com.alivc.live.pusher.AlivcBeautyLevelEnum;
import com.alivc.live.pusher.AlivcLivePushConfig;
import com.alivc.live.pusher.AlivcPreviewDisplayMode;
import com.alivc.live.pusher.AlivcPreviewOrientationEnum;
import com.orhanobut.logger.Logger;
import com.qmuiteam.qmui.widget.dialog.QMUIDialog;
import com.qmuiteam.qmui.widget.dialog.QMUIDialogAction;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.module.live.activity.LiveEndActivity;
import com.weiju.ccmall.module.live.activity.LiveNoticeActivity;
import com.weiju.ccmall.module.live.activity.LivePushActivity;
import com.weiju.ccmall.module.live.activity.PlayerSkinActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.UserToken;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ChatroomKit;
import com.weiju.ccmall.shared.util.Common;
import com.weiju.ccmall.shared.util.DataInterface;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.File;

import io.rong.imlib.RongIMClient;
import io.rong.imlib.model.UserInfo;
import rx.functions.Action1;

import static com.alivc.live.pusher.AlivcPreviewOrientationEnum.ORIENTATION_PORTRAIT;


/**
 * @author chenyanming
 * @time 2019/11/23 on 16:03
 * @desc ${TODD}
 */
public class LiveManager {
    public static AlivcLivePushConfig mAlivcLivePushConfig = null;
    public static AlivcPreviewOrientationEnum mOrientationEnum = ORIENTATION_PORTRAIT;

    public static AlivcLivePushConfig getAlivcLivePushConfig(Context context) {
        if (null == mAlivcLivePushConfig) {
            mAlivcLivePushConfig = new AlivcLivePushConfig();
            mAlivcLivePushConfig.setBeautyLevel(AlivcBeautyLevelEnum.BEAUTY_Professional);
            mAlivcLivePushConfig.setPreviewDisplayMode(AlivcPreviewDisplayMode.ALIVC_LIVE_PUSHER_PREVIEW_ASPECT_FILL);
            mAlivcLivePushConfig.setNetworkPoorPushImage(Environment.getExternalStorageDirectory().getPath() + File.separator + "ccmall_alivc_resource/poor_network.png");
            mAlivcLivePushConfig.setPausePushImage(Environment.getExternalStorageDirectory().getPath() + File.separator + "ccmall_alivc_resource/background_push.png");
            Common.copyAll(context);
        }
        return mAlivcLivePushConfig;
    }

    private static void checkPermissions(Activity activity, Runnable runnable) {
        RxPermissions rxPermissions = new RxPermissions(activity);
        rxPermissions.request(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.READ_PHONE_STATE
        ).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (!aBoolean) {
                    MyApplication.CAN_OPEN_LIVE = false;
                    ToastUtil.error("请允许 app 获取相关权限，否则将导致部分功能无法使用");
                } else {
                    MyApplication.CAN_OPEN_LIVE = true;
                    runnable.run();
                }
            }
        });
    }

    public static void checkRoomPassword(Context context, LiveRoom liveRoom) {
        if (!MyApplication.CAN_OPEN_LIVE) {
            checkPermissions((Activity) context, () -> {
                checkRoomPassword(context, liveRoom);
            });
            return;
        }
        if (liveRoom.status == 2) {
            ToastUtil.error("直播已结束!");
            return;
        }
        if (liveRoom.status != 1 && liveRoom.status != 3 && liveRoom.status != 4) {
            ToastUtil.error("状态错误");
            return;
        }

        boolean isMine = false;
        User user = null;
        if ((user = SessionUtil.getInstance().getLoginUser()) != null) {
            isMine = liveRoom.memberId.equals(user.id);
        }
        if (liveRoom.isForecast()) {
            // 预告
            LiveNoticeActivity.start(context, isMine, liveRoom.liveId);
            return;
        }

        if (TextUtils.isEmpty(liveRoom.livePassword) || isMine) {
            toLiveRoom(context, liveRoom);
        } else {
            final QMUIDialog.EditTextDialogBuilder builder = new QMUIDialog.EditTextDialogBuilder(context);
            builder.setTitle("输入房间密码")
                    .setPlaceholder("请输入密码")
                    .setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
                    .addAction("取消", new QMUIDialogAction.ActionListener() {
                        @Override
                        public void onClick(QMUIDialog dialog, int index) {
                            dialog.dismiss();
                        }
                    })
                    .addAction("确定", new QMUIDialogAction.ActionListener() {
                        @Override
                        public void onClick(QMUIDialog dialog, int index) {
                            String text = builder.getEditText().getText().toString().trim();
                            String password = StringUtil.md5(text);
                            if (liveRoom.livePassword.equals(password)) {
                                toLiveRoom(context, liveRoom);
                                dialog.dismiss();
                            } else {
                                ToastUtil.error("密码错误");
                            }
                        }
                    });
            builder.show();
        }

    }

    public static void toLiveRoom(Context context, LiveRoom liveRoom) {
//        if (UiUtils.checkUserLogin(context)) {

            switch (liveRoom.status) {
                case 1: {
                    // 直播中
                    if (isMyLive(liveRoom) && context instanceof Activity) {
                        // 自己的直播，那么重新进入开播
                        LivePushActivity.startActivity((Activity) context, LiveManager.getAlivcLivePushConfig(context),
                                liveRoom.liveId, true, false, false, LiveManager.mOrientationEnum,
                                Camera.CameraInfo.CAMERA_FACING_FRONT, false, "", "",
                                false, LiveManager.getAlivcLivePushConfig(context).isExternMainStream(), liveRoom.memberId);

                        break;
                    }
                }
                case 3: // 回放
                    //  直播进行中
                    Intent intent = new Intent(context, PlayerSkinActivity.class);
                    intent.putExtra("liveId", liveRoom.liveId);
                    intent.putExtra("memberId", liveRoom.memberId);
                    intent.putExtra("status", liveRoom.status);
                    context.startActivity(intent);
                    break;
                default:
                    ToastUtil.error("状态错误!");
                    break;
            }
//        }
    }

    public static void elementOnClick(Context context, LiveRoom liveData) {
        if (UiUtils.checkUserLogin(context)) {
//            0=未开始，1=直播中，2=直播结束，3=直播有录播
// * 0=未开始，1=直播中，2=结束，3=回放 4=预告
            if (liveData.status == 2 || liveData.status == 0) {
                if (!TextUtils.isEmpty(liveData.pageId)) {
                    String target = String.format("custom?pageId=%s", liveData.pageId);
                    EventUtil.compileEvent(context, "native", target, false);
                } else {
                    ToastUtil.error(liveData.statusStr);
                }
            } else {
                checkRoomPassword(context, liveData);
            }
        }
    }

    public static void toLiveEnd(Context context, String liveId, String headImage, String name, boolean isPlay, int followStatus) {
        EventBus.getDefault().post(new EventMessage(Event.liveRoomChange));
        Intent intent = new Intent(context, LiveEndActivity.class);
        intent.putExtra("isPlay", isPlay);
        intent.putExtra("liveId", liveId);
        intent.putExtra("headImage", headImage);
        intent.putExtra("name", name);
        intent.putExtra("followStatus", followStatus);
        context.startActivity(intent);
    }

    public static void initRongCloud(Context context, UserToken userToken, String liveId, String nikeName, String headImage, int audienceNum, boolean sendAddMessasge) {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        UserInfo userInfo;
        if (null == loginUser) {
            String nickName = "";
            String headUrl = "";
            if (!TextUtils.isEmpty(userToken.nickName)) nickName = userToken.nickName;
            if (!TextUtils.isEmpty(userToken.headUrl)) headUrl = userToken.headUrl;
            userInfo = new UserInfo(getDeviceID(), nickName, Uri.parse(headUrl));
        } else {
            userInfo = new UserInfo(loginUser.id, loginUser.nickname, Uri.parse(loginUser.avatar));
        }
        DataInterface.initUserInfo(userInfo);
        if (RongIMClient.getInstance().getCurrentConnectionStatus() != RongIMClient.ConnectionStatusListener.ConnectionStatus.CONNECTED) {
            ChatroomKit.connect(userToken.rongYunToken, new RongIMClient.ConnectCallback() {
                @Override
                public void onTokenIncorrect() {
                    ToastUtil.error("onTokenIncorrect");
                }

                @Override
                public void onSuccess(String s) {
                    ChatroomKit.setCurrentUser(userInfo);
                }

                @Override
                public void onError(RongIMClient.ErrorCode e) {
                    ToastUtil.error(e.getMessage());
                }
            });

            RongIMClient.setConnectionStatusListener(new RongIMClient.ConnectionStatusListener() {
                @Override
                public void onChanged(ConnectionStatus status) {
                    switch (status) {
                        case CONNECTED://连接成功。
//                            ToastUtil.error("连接成功");
                            if (!TextUtils.isEmpty(liveId)) {
                                joinChatRoom(context, liveId, nikeName, headImage, audienceNum, sendAddMessasge);
                            }

                            break;
                        case DISCONNECTED://断开连接。
                            ToastUtil.error("断开连接");
                            break;
                        case CONNECTING://连接中。
                            break;
                        case NETWORK_UNAVAILABLE://网络不可用。
                            ToastUtil.error("网络不可用");
                            break;
                        case KICKED_OFFLINE_BY_OTHER_CLIENT://用户账户在其他设备登录，本机会被踢掉线
                            ToastUtil.error("用户账户在其他设备登录");
                            EventBus.getDefault().post(new EventMessage(Event.logout));
                            break;
                        default:
                    }
                }
            });

        }
    }

    public static void getRongyunToken(Context context, String liveId, String nikeName, String headImage, int audienceNum, boolean sendAddMessasge) {
        IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
        User loginUser = SessionUtil.getInstance().getLoginUser();
//        if (null != loginUser) {
            APIManager.startRequest(null != loginUser ? mUserService.getToken() : mUserService.getTokenTourist(getDeviceID()), new BaseRequestListener<UserToken>() {
                @Override
                public void onSuccess(UserToken result) {
                    super.onSuccess(result);
                    initRongCloud(context, result, liveId, nikeName, headImage, audienceNum, sendAddMessasge);
                }
            }, context);
//        }
    }

    public static void joinChatRoom(Context context, String liveId, String nikeName, String headImage, int audienceNum, boolean sendAddMessasge) {
        ChatroomKit.joinChatRoom(liveId, -1, new RongIMClient.OperationCallback() {
            @Override
            public void onSuccess() {
//                ToastUtil.success("直播室聊天创建成功! ");
                if (sendAddMessasge) {
                    /*
                    ChatroomWelcomeMessage welcomeMessage = new ChatroomWelcomeMessage();
                    welcomeMessage.setHeadImage(headImage);
                    welcomeMessage.setNickName(nikeName);
                    welcomeMessage.setAudienceNum(audienceNum);
                    ChatroomKit.sendMessage(welcomeMessage,false);
                     */
                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {
                getRongyunToken(context, liveId, nikeName, headImage, audienceNum, sendAddMessasge);
//                ToastUtil.error("直播室聊天创建失败,无法聊天! ");
            }
        });
    }

    public static boolean isMyLive(LiveRoom liveRoom) {
        User loginUser = SessionUtil.getInstance().getLoginUser();
//        Log.d("Seven", "isMyLive, " + liveRoom.memberId + ", " + loginUser.id);
        if (null == loginUser) {
            return false;
        }
        return loginUser.id.equals(liveRoom.memberId);
    }

    private static String deviceID = null;

    /**
     * 获取设备唯一ID
     * @return
     */
    public static String getDeviceID() {
        if(deviceID == null){
            try {
                //一共13位  如果位数不够可以继续添加其他信息
                deviceID = "35" +
                        Build.BOARD.length() % 10 +
                        Build.BRAND.length() % 10 +
                        Build.CPU_ABI.length() % 10 +
                        Build.DEVICE.length() % 10 +
                        Build.DISPLAY.length() % 10 +
                        Build.HOST.length() % 10 +
                        Build.ID.length() % 10 +
                        Build.MANUFACTURER.length() % 10 +
                        Build.MODEL.length() % 10 +
                        Build.PRODUCT.length() % 10 +
                        Build.TAGS.length() % 10 +
                        Build.TYPE.length() % 10 +
                        Build.USER.length() % 10;
            } catch (Exception e) {
                deviceID = "";
            }
        }
        Logger.d("deviceID:" + deviceID);
        return deviceID;
    }
}
