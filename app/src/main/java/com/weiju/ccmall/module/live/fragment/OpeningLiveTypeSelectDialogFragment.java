package com.weiju.ccmall.module.live.fragment;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.activity.OpeningLiveActivity;
import com.weiju.ccmall.module.live.consts.OpeningLiveType;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class OpeningLiveTypeSelectDialogFragment extends DialogFragment {

    @BindView(R.id.llTypeLeader)
    LinearLayout llTypeLeader;
    @BindView(R.id.llTypeVip)
    LinearLayout llTypeVip;
    @BindView(R.id.llTypeAnchor)
    LinearLayout llTypeAnchor;
    Unbinder unbinder;

    public static OpeningLiveTypeSelectDialogFragment newInstance() {
        OpeningLiveTypeSelectDialogFragment fragment = new OpeningLiveTypeSelectDialogFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_opening_live_type_select, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        //getDialog().getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.llTypeLeader, R.id.llTypeVip, R.id.llTypeAnchor})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llTypeLeader:
                OpeningLiveActivity.start(getContext(), OpeningLiveType.LEADER);
                break;
            case R.id.llTypeVip:
                OpeningLiveActivity.start(getContext(), OpeningLiveType.VIP);
                break;
            case R.id.llTypeAnchor:
                OpeningLiveActivity.start(getContext(), OpeningLiveType.ANCHOR);
                break;
        }
        dismiss();
    }
}
