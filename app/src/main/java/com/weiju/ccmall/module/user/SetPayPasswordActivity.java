package com.weiju.ccmall.module.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.util.Base64;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.RegexUtils;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.auth.model.AuthModel;
import com.weiju.ccmall.module.blockchain.transferout.SimpleTextWatcher;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.component.CaptchaBtn;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICaptchaService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/6/23.
 */
public class SetPayPasswordActivity extends BaseActivity {

    @BindView(R.id.newPasswordEt)
    protected EditText mNewPasswordEt;

    @BindView(R.id.surePasswordEt)
    protected EditText mSurePasswordEt;
    @BindView(R.id.tvPhone)
    TextView mTvPhone;
    @BindView(R.id.captchaEt)
    EditText mCaptchaEt;
    @BindView(R.id.captchaBtn)
    CaptchaBtn mCaptchaBtn;
    @BindView(R.id.editBtn)
    TextView mEditBtn;

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.etIdCard)
    EditText etIdCard;
    @BindView(R.id.llIdCard)
    LinearLayout llIdCard;
    @BindView(R.id.tvBankCard)
    TextView tvBankCard;
    @BindView(R.id.llBankCard)
    LinearLayout llBankCard;
    @BindView(R.id.etBankCardId)
    EditText etBankCardId;
    @BindView(R.id.llBankCardId)
    LinearLayout llBankCardId;
    @BindView(R.id.llCaptcha)
    LinearLayout llCaptcha;

    private IUserService mUserService;
    private User mLoginUser;
    private boolean mIsFromAuth;
    private String mIdCard;
    private String mBankCardId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pay_password);
        ButterKnife.bind(this);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        initView();
        initData();
    }

    private void initView() {
        mIsFromAuth = getIntent().getBooleanExtra("isFromAuth", false);
        mIdCard = getIntent().getStringExtra("idCard");
        mBankCardId = getIntent().getStringExtra("bankCardId");
        setTitle("设置支付密码");
        setLeftBlack();
        if (mIsFromAuth) {
            etIdCard.setText(mIdCard);
            etIdCard.setEnabled(false);
            etBankCardId.setText(mBankCardId);
            etBankCardId.setEnabled(false);
        } else {
            initEditText(etIdCard);
            initEditText(etBankCardId);
        }
        initEditText(mCaptchaEt);
        initEditText(mNewPasswordEt);
        initEditText(mSurePasswordEt);
    }

    private void initData() {
        mLoginUser = SessionUtil.getInstance().getLoginUser();
        if (mLoginUser != null){
            mTvPhone.setText(ConvertUtil.maskPhone(mLoginUser.phone));
        }
        getAuth();
    }

    private void initEditText(EditText editText) {
        editText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                initConfirmView();
            }
        });
    }

    private void initConfirmView() {
        String idCard = etIdCard.getText().toString().trim();
        String bankCardId = etBankCardId.getText().toString().trim();
        String captcha = mCaptchaEt.getText().toString().trim();
        String newPass = mNewPasswordEt.getText().toString();
        String surePass = mSurePasswordEt.getText().toString();
        if (newPass.length() > 0 && surePass.length() > 0 && idCard.length() > 0 && bankCardId.length() > 0 && captcha.length() > 0) {
            mEditBtn.setEnabled(true);
        } else {
            mEditBtn.setEnabled(false);
        }
    }

    private void getAuth() {
        APIManager.startRequest(mUserService.getAuth(), new BaseRequestListener<AuthModel>(this) {
            @Override
            public void onSuccess(AuthModel model) {
                AuthModel.MemberAccountBean memberAccountBean = model.memberAccountBean;
                if (memberAccountBean != null) {
                    String bankAccount = memberAccountBean.bankAccount;
                    tvBankCard.setText(String.format("%s(%s)", memberAccountBean.bankName, bankAccount.substring(bankAccount.length() - 4)));
                    tvName.setText(memberAccountBean.bankUser);
                }
            }
        }, this);
    }

    @OnClick(R.id.captchaBtn)
    public void onViewClicked() {
        ICaptchaService service = ServiceManager.getInstance().createService(ICaptchaService.class);
        String phone = mLoginUser.phone;
        String token = StringUtil.md5(BuildConfig.TOKEN_SALT + phone);
        APIManager.startRequest(service.getCheckNumberPayPwd(token, phone), new BaseRequestListener<Object>(this) {

            @Override
            public void onSuccess(Object result) {
                mCaptchaBtn.start();
            }

        }, this);
    }

    @OnClick(R.id.editBtn)
    protected void onEdit() {
        String newPass = mNewPasswordEt.getText().toString();
        String surePass = mSurePasswordEt.getText().toString();

        if (newPass.length() < 6) {
            ToastUtil.error("支付密码必须为6位数字");
            mNewPasswordEt.requestFocus();
            return;
        }
        if (isSimplePwd(newPass)) {
            ToastUtil.error("当前密码过于简单，请重新设置");
            mNewPasswordEt.requestFocus();
            return;
        }
        if (!newPass.equals(surePass)) {
            ToastUtil.error("两次密码输入不一致");
            mSurePasswordEt.requestFocus();
            return;
        }
        String encodeNewPass = Base64.encodeToString(newPass.getBytes(), Base64.NO_WRAP);
        HashMap<String, String> params = new HashMap<>();
        params.put("payPwd", encodeNewPass);
        params.put("phone", mLoginUser.phone);
        params.put("checkNumber", mCaptchaEt.getText().toString());
        params.put("idCardNo", etIdCard.getText().toString());
        params.put("bankNo", etBankCardId.getText().toString());

        ToastUtil.showLoading(this);
        APIManager.startRequest(mUserService.putPayPwd(params), new BaseRequestListener<Object>(this) {

            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("操作成功");
                EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_USER_CHANGE));
                finish();
            }
        }, this);

    }

    private boolean isSimplePwd(String pwd) {
        if (RegexUtils.isMatch("(?:(?:0(?=1)|1(?=2)|2(?=3)|3(?=4)|4(?=5)|5(?=6)|6(?=7)|7(?=8)|8(?=9)){5}|(?:9(?=8)|8(?=7)|7(?=6)|6(?=5)|5(?=4)|4(?=3)|3(?=2)|2(?=1)|1(?=0)){5})\\d"
                , pwd)) {
            return true;
        } else return RegexUtils.isMatch("([\\d])\\1{2,}", pwd);
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, SetPayPasswordActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, boolean isFromAuth, String idCard, String bankCardId) {
        Intent intent = new Intent(context, SetPayPasswordActivity.class);
        intent.putExtra("isFromAuth", isFromAuth);
        intent.putExtra("idCard", idCard);
        intent.putExtra("bankCardId", bankCardId);
        context.startActivity(intent);
    }

}
