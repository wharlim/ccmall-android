package com.weiju.ccmall.module.category.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.jkp.JiKaoPuProductDetailActivity;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.Tag;
import com.weiju.ccmall.shared.component.TagTextView;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.category
 * @since 2017-06-18
 */
public class ProductListAdapter extends BaseAdapter<SkuInfo, ProductListAdapter.ViewHolder> {

    private int mColumns = 1;
    private boolean mShopkeeper;
    private Context mContext;
    private String mOrderType;
    private boolean mAddProduct;

    private AddLiveProductListener mAddLiveProductListener;

    public void setAddLiveProductListener(AddLiveProductListener addLiveProductListener) {
        mAddLiveProductListener = addLiveProductListener;
    }

    public void isAddProduct(boolean isAddProduct) {
        mAddProduct = isAddProduct;
    }

    public ProductListAdapter(Context context) {
        super(context);
        mContext = context;
    }

    public ProductListAdapter(Context context, String orderType) {
        super(context);
        mContext = context;
        mOrderType = orderType;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mShopkeeper = true;
        if (viewType == 2) {
            return new ViewHolder(layoutInflater.inflate(R.layout.el_product_column2_item, parent, false));
        } else if (viewType == AppTypes.PUSH.PRODUCT_TYPE) {
            return new ViewHolder(layoutInflater.inflate(R.layout.item_product_push, parent, false));
        }
        return new ViewHolder(layoutInflater.inflate(R.layout.el_product_column_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final SkuInfo skuInfo = items.get(position);
        holder.setProduct(skuInfo);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (skuInfo.isJpkProduct()) {
                    JiKaoPuProductDetailActivity.start(context, skuInfo);
                } else {
                    EventUtil.viewProductDetail(context, skuInfo.skuId, false, mOrderType);
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return mColumns;
    }

    public void setColumns(int columns) {
        this.mColumns = columns;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemSalesTv)
        protected TextView itemSalesTv;
        @BindView(R.id.itemPriceTv)
        protected TextView itemPriceTv;
        @BindView(R.id.itemThumbIv)
        protected SimpleDraweeView itemThumbIv;
        @BindView(R.id.ivBanjia)
        protected ImageView mBanjiaView;
        @BindView(R.id.tvCCM)
        protected TextView mTvCCM;

        @BindView(R.id.tvAddProduct)
        protected TextView mTvAddProduct;
        @BindView(R.id.tvSuanLv)
        protected TextView mTvSuanLv;
        @BindView(R.id.tvZhuan)
        protected TextView mTvZhuan;
        @BindView(R.id.itemPriceLive)
        protected TextView mItemPriceLive;
        @BindView(R.id.itemMarkPriceLive)
        protected TextView mItemMarkPriceLive;
        @BindView(R.id.tvLiveSale)
        protected TextView mTvLiveSale;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setTag(SpannableStringBuilder title, List<Tag> tags) {
            for (Tag tag : tags) {
                title.insert(0, tag.name);
                int start = 0;
                int end = tag.name.length();
                //稍微设置标签文字小一点
                title.setSpan(new RelativeSizeSpan(0.9f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                //设置圆角背景
                title.setSpan(new RoundBackgroundColorSpan(context.getResources().getColor(R.color.red), Color.WHITE), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        private void setProduct(final SkuInfo product) {
            mTvAddProduct.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (null != mAddLiveProductListener) {
                        mAddLiveProductListener.addLiveProdcut(product);
                    }
                }
            });

            mTvAddProduct.setVisibility(mAddProduct ? View.VISIBLE : View.GONE);


            FrescoUtil.setImageSmall(itemThumbIv, product.thumb);
            itemPriceTv.setText(MoneyUtil.centToYuan¥StrNoZero(product.retailPrice));
            itemSalesTv.setText(String.format("销量：%d", product.totalSaleCount));
//            mTvCCM.setText(String.format("奖%sCCM", MoneyUtil.coinToYuanStrNoZero(product.coin)));
//            mTvCCM.setText(String.format("奖%s%%算率", product.countRateExc));
            mTvCCM.setVisibility(mAddProduct ? View.GONE : View.VISIBLE);
            itemPriceTv.setVisibility(mAddProduct ? View.GONE : View.VISIBLE);

            mTvSuanLv.setVisibility(mAddProduct ? View.VISIBLE : View.GONE);
            mTvZhuan.setVisibility(mAddProduct ? View.VISIBLE : View.GONE);
            mItemPriceLive.setVisibility(mAddProduct ? View.VISIBLE : View.GONE);
            mItemMarkPriceLive.setVisibility(mAddProduct ? View.VISIBLE : View.GONE);
            mTvLiveSale.setVisibility(mAddProduct ? View.VISIBLE : View.GONE);

            mTvSuanLv.setText(String.format("%s%%算率", product.countRateExc));
            mTvZhuan.setText(String.format("预计赚%s元", product.getAnchorDistributionProfit()));
            mItemPriceLive.setText(MoneyUtil.centToYuan¥StrNoZero(product.retailPrice));
            mItemMarkPriceLive.setText(MoneyUtil.centToYuan¥StrNoZero(product.marketPrice));
            TextViewUtil.addThroughLine(mItemMarkPriceLive);
            mTvLiveSale.setText(String.format("热销%s件", product.sales));

            if (product.isJpkProduct()) {
                mTvCCM.setText(String.format("%s元优惠券", MoneyUtil.centToYuan¥StrNoZero(product.costPrice)));
            } else {
                mTvCCM.setText(String.format("奖%s%%算率", product.countRateExc));
            }
            TagTextView itemTitleTv;
            mBanjiaView.setVisibility(product.isBanjia() ? View.VISIBLE : View.GONE);
            switch (getItemViewType()) {
                case 2:
                    TextView tvMarkPrice = (TextView) itemView.findViewById(R.id.itemMarkPriceTv);
                    TextViewUtil.addThroughLine(tvMarkPrice);
                    tvMarkPrice.setText(MoneyUtil.centToYuan¥StrNoZero(product.marketPrice));

                    TextView tvTitle = itemView.findViewById(R.id.itemTitleTv);
                    SpannableStringBuilder ssb = new SpannableStringBuilder();
                    ssb.append(product.name);
                    setTag(ssb, product.tags);
                    tvTitle.setText(ssb);

//                    LinearLayout tagViews = itemView.findViewById(R.id.tagViews);
//                    setTag(tagViews, product.tags);
                    break;
                case AppTypes.PUSH.PRODUCT_TYPE:
                    itemSalesTv.setText(String.format("销量：%d", product.sales));
                    TextView tvGuige = (TextView) itemView.findViewById(R.id.tvGuige);
                    tvGuige.setText(product.spec);
                    TextView tvSharePrice = (TextView) itemView.findViewById(R.id.tvSharePrice);
                    tvSharePrice.setText(String.format("分享赚 %s~%s元", MoneyUtil.centToYuan(product.minPrice), MoneyUtil.centToYuan(product.maxPrice)));
                    TextView titleView = itemView.findViewById(R.id.itemTitleTv);
                    TextViewUtil.setTagTitle(titleView, product.name, product.tags);
                    itemPriceTv.setText("会员价：" + MoneyUtil.centToYuan¥StrNoZero(product.retailPrice));
                    mBanjiaView.setVisibility(View.GONE);
                    break;
                default:
                    itemTitleTv = itemView.findViewById(R.id.itemTitleTv);
                    itemTitleTv.setTags(product.tags);
                    itemTitleTv.setText(product.name);

                    TextView tvMarkPrice2 = (TextView) itemView.findViewById(R.id.itemMarkPriceTv);
                    TextViewUtil.addThroughLine(tvMarkPrice2);
                    tvMarkPrice2.setText(MoneyUtil.centToYuan¥StrNoZero(product.marketPrice));
                    break;
            }
        }
    }

    public interface AddLiveProductListener {
        void addLiveProdcut(SkuInfo skuInfo);
    }
}
