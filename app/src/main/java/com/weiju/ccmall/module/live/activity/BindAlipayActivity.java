package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.blankj.utilcode.utils.RegexUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.deal.DealFirstActivity;
import com.weiju.ccmall.module.live.entity.AlipayAccountEntity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BindAlipayActivity extends BaseActivity {

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.etAccount)
    EditText etAccount;
    @BindView(R.id.etAgain)
    EditText etAgain;

    ILiveStoreService mService;
    private AlipayAccountEntity mAlipayUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_alipay);
        ButterKnife.bind(this);
        setTitle("输入收款支付宝");
        setLeftBlack();
        initView();
        mService = ServiceManager.getInstance().createService(ILiveStoreService.class);
    }

    private void initView() {
        mAlipayUser = (AlipayAccountEntity) getIntent().getSerializableExtra("alipayUser");
        tvName.setText(mAlipayUser.alipayUser);
    }

    @OnClick(R.id.tvConfirm)
    public void onConfirm() {
        String account = etAccount.getText().toString();
        if (TextUtils.isEmpty(account)) {
            ToastUtil.error("请输入支付宝账号");
            return;
        }

        if (!RegexUtils.isMobileSimple(account) && !RegexUtils.isEmail(account)) {
            ToastUtil.error("请检查支付宝账号格式");
            return;
        }

        String again = etAgain.getText().toString();
        if (TextUtils.isEmpty(again)) {
            ToastUtil.error("请再次输入支付宝账号");
            return;
        }

        if (!account.equals(again)) {
            ToastUtil.error("两次输入的账号不一致");
            return;
        }

        APIManager.startRequest(mService.getAlipayAccount(mAlipayUser.alipayUser, account), new BaseRequestListener<AlipayAccountEntity>(this) {
            @Override
            public void onSuccess(AlipayAccountEntity result) {
                super.onSuccess(result);
                result.totalMoney = mAlipayUser.totalMoney;
                DealFirstActivity.start(BindAlipayActivity.this, result);
            }
        }, this);

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(MsgStatus status) {
        switch (status.getAction()) {
            case MsgStatus.ACTION_DEAL_SUCESS:
                finish();
                break;
            default:
                break;
        }
    }

    public static void start(Context context, AlipayAccountEntity alipayUser) {
        Intent intent = new Intent(context, BindAlipayActivity.class);
        intent.putExtra("alipayUser", alipayUser);
        context.startActivity(intent);
    }

}
