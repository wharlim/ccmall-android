package com.weiju.ccmall.module.user.adapter;

import android.text.Html;
import android.text.TextUtils;

import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SuperGroup;
import com.weiju.ccmall.shared.component.CountDownGray;
import com.weiju.ccmall.shared.util.FrescoUtil;

/**
 * @author chenyanming
 * @time 2019/8/14 on 11:25
 * @desc ${TODD}
 */
public class SuperGroupAdapter extends BaseQuickAdapter<SuperGroup, BaseViewHolder> {
    public SuperGroupAdapter() {
        super(R.layout.item_super_group);
    }

    @Override
    protected void convert(BaseViewHolder helper, SuperGroup item) {
        //status  活动上下架状态，0下架，1上架
        boolean status = item.activityBean.status == 1 && item.productBean.status == 1;
        //stock 库存 >0有库存
        boolean stock = item.productBean.stock > 0;
        //过期
        boolean overdue = !TextUtils.isEmpty(item.activityBean.endDate) && TimeUtils.getNowTimeDate().getTime() >
                TimeUtils.string2Millis(item.activityBean.endDate);
        helper.setVisible(R.id.layoutCollectIng, status && stock && !overdue);
        helper.setVisible(R.id.tvGoCollect, status && stock && !overdue);
        helper.setVisible(R.id.tvCollectEnd, !status || !stock || overdue);
        CountDownGray countDown = helper.getView(R.id.countDownView);
        FrescoUtil.setImgMask(helper.getView(R.id.ivProduct), item.productBean.thumb,
                item.activityBean.status == 0 || item.productBean.status == 0, item.productBean.stock,
                overdue);
        helper.setText(R.id.tvTitle, item.productBean.name);
        helper.setText(R.id.tvCollectEnd, item.productBean.status == 0 || item.activityBean.status == 0 || !stock ? "未在时间内完成，拼团已过期" : "活动已结束");

        if (!TextUtils.isEmpty(item.activityBean.endDate)) {
            countDown.setTimeLeft(TimeUtils.string2Millis(item.activityBean.endDate) - TimeUtils.getNowTimeMills(), null);
        }
        helper.setText(R.id.tvGroupNum, Html.fromHtml(String.format("还差<font color =\"#f51861\">%s</font>人成团", item.groupInfo.num)));
    }
}
