package com.weiju.ccmall.module.category;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.cart.CartActivity;
import com.weiju.ccmall.module.category.adapter.NewCategoryAdapter;
import com.weiju.ccmall.module.live.consts.ConstantKey;
import com.weiju.ccmall.module.search.SearchActivity;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Category;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.bean.event.MsgCategory;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICategoryService;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.category
 * @since 2017-06-16
 */
public class CategoryFragment extends BaseFragment {

    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.searchLayout)
    LinearLayout mSearchLayout;
    @BindView(R.id.ivNoData)
    ImageView mIvNoData;
    @BindView(R.id.tvNoData)
    TextView mTvNoData;
    @BindView(R.id.tvGoMain)
    TextView mTvNoDataBtn;
    @BindView(R.id.layoutNodata)
    LinearLayout mLayoutNodata;
    @BindView(R.id.leftContainer)
    FrameLayout mLeftContainer;

    @BindView(R.id.tv_finish)
    ImageView tv_finish;
    @BindView(R.id.ivCart)
    ImageView ivCart;
    @BindView(R.id.tv_search)
    TextView mTvSearch;
    @BindView(R.id.tv_title)
    TextView mTvTitle;

    //    private CategoryAdapter mCategoryAdapter;
    private NewCategoryAdapter mAdapter;
    private ICategoryService mCategoryService;
    private String mCategoryId = "";
    private boolean isLoaded = false;
    private List<Category> mDatas = new ArrayList<>();
    private static final int PAGE_SIZE = 30;

    private String classifacationType = "";
    private boolean mIsOnlySelectClassify;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_category_layout, container, false);
        ButterKnife.bind(this, rootView);
        mCategoryService = ServiceManager.getInstance().createService(ICategoryService.class);
        initNoData();
        Bundle extras = getActivity().getIntent().getExtras();

        if (getArguments() != null) {
            mIsOnlySelectClassify = getArguments().getBoolean(ConstantKey.KEY_IS_ONLY_SELECT_CLASSIFY, false);
            mTvSearch.setVisibility(mIsOnlySelectClassify ? View.GONE : View.VISIBLE);
            ivCart.setVisibility(mIsOnlySelectClassify ? View.GONE : View.VISIBLE);
            mTvTitle.setVisibility(mIsOnlySelectClassify ? View.VISIBLE : View.GONE);
        }

        classifacationType = getContext().getSharedPreferences(Const.CLASSIFICATION_TYPE, 0).getString(Const.CLASSIFICATION_TYPE, "");
        if (Const.NEWRETAIL_MODE.equals(classifacationType)) {
            tv_finish.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getView() != null && !isLoaded) {
            initViews();
        }
    }

    void initViews() {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.leftContainer, getCategoryLeftFragment());
        fragmentTransaction.commit();
        mAdapter = new NewCategoryAdapter(mDatas);
        mRecyclerView.setLayoutManager(getLayoutManager());
        mRecyclerView.setAdapter(mAdapter);
//        mAdapter.setEmptyView(mLayoutNodata);
        isLoaded = true;
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getData(false);
            }
        });
        mRecyclerView.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                Category category = mDatas.get(position);
                if (mIsOnlySelectClassify) {
                    EventBus.getDefault().postSticky(new EventMessage(Event.selectClassify, category));
                    getActivity().finish();
                    return;
                }
                Intent intent = new Intent(getActivity(), CategoryActivity.class);
                intent.putExtra("categoryId", category.id);
//                getContext().getSharedPreferences(Const.CLASSIFICATION_TYPE, 0).edit().putString(Const.CLASSIFICATION_TYPE, Const.NEWRETAIL_MODE).commit();
                /*if (!"".equals(classifacationType)) {
                    intent.putExtra(Const.CLASSIFICATION_TYPE, classifacationType);
                }*/
                getActivity().startActivity(intent);
            }
        });
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });
    }

    private GridLayoutManager getLayoutManager() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        gridLayoutManager.setAutoMeasureEnabled(true);
        return gridLayoutManager;
    }


    private void initNoData() {
        mIvNoData.setImageResource(R.mipmap.no_data_normal);
        mTvNoData.setText("这个页面去火星了");
        mTvNoDataBtn.setText("刷新看看");
        mTvNoDataBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new MsgCategory(MsgCategory.ACTION_RE_QUEST));
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && isAdded() && !isLoaded) {
            initViews();
        }
    }

    private LeftCategoryFragment getCategoryLeftFragment() {
        LeftCategoryFragment leftCategoryFragment = new LeftCategoryFragment();
        Bundle bundle = new Bundle();
        bundle.putString("categoryId", mCategoryId);
        leftCategoryFragment.setArguments(bundle);
        return leftCategoryFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    public void getData(boolean isRefresh) {
        if (mCategoryId == null || mCategoryId.isEmpty()) {
            return;
        }
        if (isRefresh) {
            mDatas.clear();
            mAdapter.notifyDataSetChanged();
        }
        int page = mDatas.size() / PAGE_SIZE + 1;
        APIManager.startRequest(
                mCategoryService.getCategory(mCategoryId, page, PAGE_SIZE),
                new BaseRequestListener<PaginationEntity<Category, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<Category, Object> result) {
                        mDatas.addAll(result.list);
                        if (result.list.size() < PAGE_SIZE) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mAdapter.loadMoreFail();

                    }
                }, getContext());
    }

    @OnClick(R.id.tv_search)
    protected void goToSearch() {
        startActivity(new Intent(getContext(), SearchActivity.class));
    }

    @OnClick(R.id.ivCart)
    protected void cart() {
        if (UiUtils.checkUserLogin(getActivity())) {
            startActivity(new Intent(getActivity(), CartActivity.class));
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void selectCategory(EventMessage message) {
        if (message.getEvent() == Event.changeCategory) {
            EventBus.getDefault().removeStickyEvent(message);
            String categoryId = String.valueOf(message.getData());
            if (!categoryId.equalsIgnoreCase(mCategoryId)) {
                mCategoryId = categoryId;
                getData(true);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMessage(MsgCategory msgCategory) {
        switch (msgCategory.getAction()) {
            case MsgCategory.ACTION_SHOW_NODATA:
                mLayoutNodata.setVisibility(View.VISIBLE);
                break;
            case MsgCategory.ACTION_GONE_NODATA:
                mLayoutNodata.setVisibility(View.GONE);
                break;
        }
    }

    @OnClick(R.id.tv_finish)
    public void finish(View view) {
        getActivity().finish();
    }


}
