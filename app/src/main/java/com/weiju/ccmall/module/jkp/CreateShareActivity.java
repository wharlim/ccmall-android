package com.weiju.ccmall.module.jkp;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.FileUtils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.JkpOriginalProduct;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.util.CCMFileUtils;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bingoogolapple.qrcode.zxing.QRCodeEncoder;

/**
 * 创建分享
 */
public class CreateShareActivity extends BaseActivity {

    @BindView(R.id.tvProfit)
    TextView tvProfit;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvShareContent)
    TextView tvShareContent;
    @BindView(R.id.tvShowProfit)
    TextView tvShowProfit;
    @BindView(R.id.tvShowInviteCode)
    TextView tvShowInviteCode;
    @BindView(R.id.tvShowGoodLink)
    TextView tvShowGoodLink;
    @BindView(R.id.tvOnlyCopyCmm)
    TextView tvOnlyCopyCmm;
    @BindView(R.id.tvOnlyCopyLink)
    TextView tvOnlyCopyLink;
    @BindView(R.id.tvOnlyCopyTitle)
    TextView tvOnlyCopyTitle;
    @BindView(R.id.tvCopyShareTxt)
    TextView tvCopyShareTxt;
    @BindView(R.id.tvShareImg)
    TextView tvShareImg;

    JkpOriginalProduct jkpOriginalProduct;
    SkuInfo skuInfo;

    boolean showProfit;
    boolean showInviteCode;
    boolean showLink;
    @BindView(R.id.tvShareImgTitle)
    TextView tvShareImgTitle;
    @BindView(R.id.ivShareImgSelectState)
    ImageView ivShareImgSelectState;
    @BindView(R.id.tvRetailPrice)
    TextView tvRetailPrice;
    @BindView(R.id.tvMarketPrice)
    TextView tvMarketPrice;
    @BindView(R.id.tvCoupon)
    TextView tvCoupon;
    @BindView(R.id.ivGoodImg)
    ImageView ivGoodImg;
    @BindView(R.id.ivQrCode)
    ImageView ivQrCode;
    @BindView(R.id.llShareContainer)
    LinearLayout llShareContainer;
    @BindView(R.id.rvImgs)
    RecyclerView rvImgs;

    boolean selectShareContainer;

    private int downLoadImgsCount;

    private ShareImgsAdater shareImgsAdater = new ShareImgsAdater();

    public static void start(Context context, JkpOriginalProduct jkpOriginalProduct, SkuInfo skuInfo) {
        Intent intent = new Intent(context, CreateShareActivity.class);
        intent.putExtra("jkpOriginalProduct", jkpOriginalProduct);
        intent.putExtra("skuInfo", skuInfo);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_share);
        ButterKnife.bind(this);
        jkpOriginalProduct = (JkpOriginalProduct) getIntent().getSerializableExtra("jkpOriginalProduct");
        skuInfo = (SkuInfo) getIntent().getSerializableExtra("skuInfo");
        initData();
        setTitle("创建分享");
        setLeftBlack();
    }

    private void initData() {
        tvProfit.setText("预估奖励收益：" + MoneyUtil.centToYuan¥StrNoZero(jkpOriginalProduct.forecastProfit));
        tvTitle.setText(jkpOriginalProduct.title);
        updateSharedContent();

        tvShareImgTitle.setText(jkpOriginalProduct.title);

        //满减价格
        int fullAmount = jkpOriginalProduct.getFullAmountFromCouponInfo();
        if (fullAmount * 100 > jkpOriginalProduct.zkFinalPrice) {
            tvRetailPrice.setText(jkpOriginalProduct.couponInfo);
        } else {
            tvRetailPrice.setText("券后价:" + MoneyUtil.centToYuan¥StrNoZero(skuInfo.retailPrice));
        }

        tvMarketPrice.setText("原价:" + MoneyUtil.centToYuan¥StrNoZero(skuInfo.marketPrice));
        tvCoupon.setText(MoneyUtil.centToYuan¥StrNoZero(skuInfo.costPrice));
        Glide.with(this)
                .load(Uri.parse(jkpOriginalProduct.pictUrl))
                .into(ivGoodImg);
        String shareUrl = BuildConfig.WECHAT_URL + "jkp/share/" + skuInfo.originId + "/" + MoneyUtil.centToYuanStrNoZero(skuInfo.costPrice) + "/" +
                MoneyUtil.centToYuanStrNoZero(skuInfo.retailPrice) + "/" + MoneyUtil.centToYuanStrNoZero(skuInfo.marketPrice) + "?tbkPwd=" + jkpOriginalProduct.tbkPwd;
        ivQrCode.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ivQrCode.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                ivQrCode.setImageBitmap(QRCodeEncoder.syncEncodeQRCode(shareUrl, ivQrCode.getWidth()));
            }
        });
        // 图片网格列表
        if (!TextUtils.isEmpty(jkpOriginalProduct.smallImages)) {
            String[] imgs = jkpOriginalProduct.smallImages.split(",");

            downLoadImgsCount = 0;
            for (int i = 0; i < imgs.length; ++i) {
                final int index = i;
                Glide.with(this)
                        .downloadOnly()
                        .load(Uri.parse(imgs[i]))
                        .into(new SimpleTarget<File>() {
                            @Override
                            public void onResourceReady(@NonNull File resource, @Nullable Transition<? super File> transition) {
                                imgs[index] = resource.getAbsolutePath();
                                downLoadImgsCount++;
//                                Log.d("Seven", imgs[index] + "");
                                if (downLoadImgsCount == imgs.length) {
//                                    Log.d("Seven", "load list");
                                    showImgs(imgs);
                                }
                            }
                        });
            }
        }
    }

    private void showImgs(String[] files) {
        shareImgsAdater.setNewData(Arrays.asList(files));
        rvImgs.setAdapter(shareImgsAdater);
    }

    private void updateSharedContent() {
        final String divider = "\n-----------------------";
        String ctn = "【原价】：" + MoneyUtil.centToYuanStrNoZero(skuInfo.marketPrice) + "元";
        ctn += "\n【券后价】：" + MoneyUtil.centToYuanStrNoZero(skuInfo.retailPrice) + "元";
        // 收益
        if (showProfit) {
            ctn += "\n【用集靠谱再省】：" + MoneyUtil.centToYuanStrNoZero(jkpOriginalProduct.forecastProfit) + "元";
            ctn += divider;
        }
        if (showInviteCode) {
            ctn += "\n【邀请码】：" + jkpOriginalProduct.inviteCode;
            ctn += divider;
        }

        ctn += "\n复制此评论" + jkpOriginalProduct.tbkPwd + "";
        ctn += "\n去【tao寳】下单";
        ctn += divider;
        if (showLink) {
            ctn += "\n【商品链接】";
            ctn += "\n" + jkpOriginalProduct.couponShortUrl;
        }
        tvShareContent.setText(ctn);
    }

    @OnClick({R.id.tvShowProfit, R.id.tvShowInviteCode, R.id.tvShowGoodLink, R.id.tvOnlyCopyCmm, R.id.tvOnlyCopyLink, R.id.tvOnlyCopyTitle})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvShowProfit:
                showProfit = !showProfit;
                setLeftDrawable(tvShowProfit, showProfit ? R.drawable.ic_share_checked : R.drawable.ic_share_uncheck);
                updateSharedContent();
                break;
            case R.id.tvShowInviteCode:
                showInviteCode = !showInviteCode;
                setLeftDrawable(tvShowInviteCode, showInviteCode ? R.drawable.ic_share_checked : R.drawable.ic_share_uncheck);
                updateSharedContent();
                break;
            case R.id.tvShowGoodLink:
                showLink = !showLink;
                setLeftDrawable(tvShowGoodLink, showLink ? R.drawable.ic_share_checked : R.drawable.ic_share_uncheck);
                updateSharedContent();
                break;
            case R.id.tvOnlyCopyCmm:
                copy(jkpOriginalProduct.tbkPwd, "已复制口令：");
                break;
            case R.id.tvOnlyCopyLink:
                copy(jkpOriginalProduct.couponShortUrl, "已复制商品链接：");
                break;
            case R.id.tvOnlyCopyTitle:
                copy(jkpOriginalProduct.title, "已复制标题：");
                break;
        }
    }

    private void setLeftDrawable(TextView tv, int leftDrawableRes) {
        Drawable drawable = getResources().getDrawable(leftDrawableRes);
        drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
        tv.setCompoundDrawables(drawable, null, null, null);
    }

    private void copy(String cpContent, String title) {
        ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData mClipData = ClipData.newPlainText("Label", cpContent);
        cm.setPrimaryClip(mClipData);
        WJDialog dialog = new WJDialog(this);
        dialog.show();
        dialog.setTitle(title);
        dialog.setContentText(cpContent);
        dialog.hideCancelBtn();
        dialog.setConfirmText("确定");
        dialog.setOnConfirmListener(v -> {
            dialog.dismiss();
        });
    }

    @OnClick({R.id.tvCopyShareTxt, R.id.tvShareImg})
    public void onShare(View view) {
        switch (view.getId()) {
            case R.id.tvCopyShareTxt:
                String cpContent = jkpOriginalProduct.title + "\n" + tvShareContent.getText().toString();
                ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData mClipData = ClipData.newPlainText("Label", cpContent);
                cm.setPrimaryClip(mClipData);
                ToastUtil.success("复制成功");
                break;
            case R.id.tvShareImg:
                shareImg();
                break;
        }
    }

    private void shareImg() {
        ArrayList<String> sharedFile = new ArrayList<>();
        String sharedDir = CCMFileUtils.getSharedFileDir();
        if (selectShareContainer) {
            int w = llShareContainer.getWidth();
            int h = llShareContainer.getHeight();
            Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            llShareContainer.setBackground(new ColorDrawable(Color.WHITE));
            ivShareImgSelectState.setVisibility(View.INVISIBLE);
            llShareContainer.draw(canvas);
            ivShareImgSelectState.setVisibility(View.VISIBLE);
            File shareFile = CCMFileUtils.saveSharedPicture(bitmap);
            sharedFile.add(shareFile.getAbsolutePath());
        }
        for (String f: shareImgsAdater.getSelected()) {
            File targetFile = new File(sharedDir, System.currentTimeMillis() + ".jpg");
            FileUtils.copyFile(new File(f), targetFile);
            sharedFile.add(targetFile.getAbsolutePath());
        }
        // sharedFile.addAll(shareImgsAdater.getSelected());
        if (!sharedFile.isEmpty()) {
            String cpCtn = jkpOriginalProduct.title + "\n" + tvShareContent.getText().toString();
            OutLinkUtils.sharedToWxCircle(this, cpCtn, sharedFile);
        } else {
            ToastUtil.error("请选择需要分享的图片!");
        }
    }

    @OnClick(R.id.llShareContainer)
    public void onSelectShareContainer() {
        selectShareContainer = !selectShareContainer;
        ivShareImgSelectState.setImageResource(
                selectShareContainer?R.drawable.ic_share_checked:R.drawable.ic_share_uncheck);
    }
}
