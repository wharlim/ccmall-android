package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.module.live.adapter.MyHistoryLivesAdapter;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.LiveShareDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import io.reactivex.Observable;

/**
 * 我的直播
 */
public class MyHistoryLivesActivity extends BaseListActivity {

    private MyHistoryLivesAdapter adapter = new MyHistoryLivesAdapter();
    private ILiveService service;
    private final int pageSize = 15;
    private int pageOffset = 1;
    private boolean isForecast; // 是否预告
    private User user;

    private LiveShareDialog mLiveShareDialog;

    {
        service = ServiceManager.getInstance().createService(ILiveService.class);
    }

    public static void start(Context context, boolean isForecast) {
        Intent intent = new Intent(context, MyHistoryLivesActivity.class);
        intent.putExtra("isForecast", isForecast);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        user = SessionUtil.getInstance().getLoginUser();
        isForecast = getIntent().getBooleanExtra("isForecast", false);
        if (user == null) {
            finish();
        }
        super.onCreate(savedInstanceState);
        mRecyclerView.setPadding(0, ConvertUtil.dip2px(10), 0, 0);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.window_background));
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    @Override
    public String getTitleStr() {
        return isForecast ? "直播预告" : "我的直播";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (view != null) {
            int id = view.getId();
            LiveRoom room = this.adapter.getItem(position);
            switch (id) {
                case R.id.tvUp:
                    updateLiveOnlineStatus(room, 1, position);
                    break;
                case R.id.tvDown:
                    updateLiveOnlineStatus(room, 0, position);
                    break;
                case R.id.tvDel:
                    delete(room, position);
                    break;
                case R.id.tvEnd:
                    stopLive(room.liveId);
                    break;
                case R.id.tvShare:
                    share(room);
                    break;
                case R.id.tvStartLive:
                    // 开始直播
                    if (room != null) {
                        startLive(room.liveId, room.memberId);
                    }
                    break;
                default:
                    if (room.isForecast()) {
                        LiveNoticeActivity.start(this, true, room.liveId);
                    } else {
                        LiveDetailActivity.start(this, room);
                    }
                    break;
            }
        }
    }

    /**
     * 发起直播
     */
    private void startLive(String liveId, String memberId) {
        LivePushActivity.startActivity(this, LiveManager.getAlivcLivePushConfig(this),
                liveId, true, false, false, LiveManager.mOrientationEnum,
                Camera.CameraInfo.CAMERA_FACING_FRONT, false, "", "",
                false, LiveManager.getAlivcLivePushConfig(this).isExternMainStream(), memberId);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // 关闭可能存在的分享进度条
        ToastUtil.hideLoading();
    }

    @Subscribe
    public void onLiveDeleted(EventMessage msg) {
        if (msg.getEvent() == Event.deleteLive) {
            getData(true);
        }
    }

    protected void share(LiveRoom room) {
        if (UiUtils.checkUserLogin(this) && null != room) {
            mLiveShareDialog = null;
            if (null == mLiveShareDialog) {
                User loginUser = SessionUtil.getInstance().getLoginUser();
                mLiveShareDialog = new LiveShareDialog(this,
                        room.title, room.title, room.liveImage, room.liveId, loginUser.invitationCode, room.nickName, room.isForecast());
            }
            mLiveShareDialog.show();
        }
    }

    public void delete(LiveRoom room, int pos) {
        // 删除预告、回放
        WJDialog dialog = new WJDialog(this);
        dialog.setLayoutRes(R.layout.dialog_confirm_live);
        dialog.show();
        dialog.setTitle(room.status == 4 ? "删除预告" : "删除回放");
        dialog.setContentText("删除后无法恢复，请确认");
        dialog.setCancelText("取消");
        dialog.setConfirmText("确定");
        dialog.setOnConfirmListener((v) -> {
            dialog.dismiss();
            ToastUtil.showLoading(this);
            APIManager.startRequest(service.deleteLiveBroadcast(room.liveId), new BaseRequestListener<Object>() {
                @Override
                public void onSuccess(Object result) {
                    ToastUtil.success("删除成功！");
                    ToastUtil.hideLoading();
                    adapter.remove(pos);
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onError(Throwable e) {
                    ToastUtil.hideLoading();
                }
            }, this);
        });

    }

    private void updateLiveOnlineStatus(LiveRoom room, int onlineStatus, int pos) {
        final WJDialog dialog = new WJDialog(this);
        dialog.setLayoutRes(R.layout.dialog_confirm_live);
        dialog.show();
        dialog.setTitle(onlineStatus == 1?"上架回放": "下架回放");
        dialog.setContentText(onlineStatus == 1?"上架后，观众可观看此回放，请确认":"下架后，观众不可观看此回放，请确认");
        dialog.setCancelText("取消");
        dialog.setConfirmText("确定");
        dialog.setOnConfirmListener((v) -> {
            dialog.dismiss();
            ToastUtil.showLoading(this);
            APIManager.startRequest(service.updateLiveBroadcast(
                    room.liveId, onlineStatus
            ), new BaseRequestListener<Object>() {
                @Override
                public void onSuccess(Object result) {
                    ToastUtil.hideLoading();
                    room.onlineStatus = onlineStatus;
                    adapter.notifyItemChanged(pos);
                    ToastUtil.success("更新成功！");
                }

                @Override
                public void onError(Throwable e) {
                    ToastUtil.hideLoading();
                }
            }, this);
        });
    }

    private void stopLive(String liveId) {
        final WJDialog dialog = new WJDialog(this);
        dialog.setLayoutRes(R.layout.dialog_confirm_live);
        dialog.show();
        dialog.setTitle("结束直播");
        dialog.setContentText("直播正在进行中，结束后无法恢复，请确认");
        dialog.setCancelText("取消");
        dialog.setConfirmText("确定");
        dialog.setOnConfirmListener((v) -> {
            dialog.dismiss();
            APIManager.startRequest(service.stopLive(liveId), new BaseRequestListener<Object>() {
                @Override
                public void onSuccess(Object result) {
                    getData(true);
                }
            }, this);
        });
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return adapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        if (isRefresh) {
            pageOffset = 1;
        }
        Observable<RequestResult<PaginationEntity<LiveRoom, Object>>> api;
        if (isForecast) {
            api = service.getLiveListWithForecast(
                    user.id, null, pageOffset, pageSize, 4
            );
        } else {
            api = service.getLiveList(
                    user.id, null, pageOffset, pageSize
            );
        }

        APIManager.startRequest(api, new BaseRequestListener<PaginationEntity<LiveRoom, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<LiveRoom, Object> result) {
                mRefreshLayout.setRefreshing(false);
                if (pageOffset == 1) {
                    adapter.setNewData(result.list);
                } else {
                    adapter.addData(result.list);
                }
                if (result.totalPage <= pageOffset) {
                    adapter.loadMoreEnd(true);
                } else {
                    pageOffset++;
                    adapter.loadMoreComplete();
                }
            }
        }, this);
    }

    @Subscribe
    public void onLiveChange(EventMessage ev) {
        if (ev.getEvent() == Event.liveRoomChange) {
            getData(true);
        }
    }
}
