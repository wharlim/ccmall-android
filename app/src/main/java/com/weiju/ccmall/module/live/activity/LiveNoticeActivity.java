package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveForecastTime;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.LiveShareDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.ColorFilterTransformation;

/**
 * 直播预告详情页面
 */
public class LiveNoticeActivity extends BaseActivity {

    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.layoutTopTitle)
    FrameLayout layoutTopTitle;
    @BindView(R.id.llRootView)
    LinearLayout llRoot;
    @BindView(R.id.shareBtn)
    ImageView shareBtn;
    @BindView(R.id.followBtn)
    TextView followBtn;
    @BindView(R.id.avatarIv)
    SimpleDraweeView avatarIv;
    @BindView(R.id.tvLiveTitle)
    TextView tvLiveTitle;
    @BindView(R.id.tvStartTime)
    TextView tvStartTime;
    @BindView(R.id.llLiveStartTime)
    LinearLayout llLiveStartTime;
    @BindView(R.id.tvLeftTime)
    TextView tvLeftTime;
    @BindView(R.id.llLiveLeftTime)
    LinearLayout llLiveLeftTime;
    @BindView(R.id.tvDelete)
    TextView tvDelete;
    @BindView(R.id.tvStart)
    TextView tvStart;
    @BindView(R.id.llBottomBtnContainer)
    LinearLayout llBottomBtnContainer;
    @BindView(R.id.llBottomTipContainer)
    LinearLayout llBottomTipContainer;

    ILiveService service = ServiceManager.getInstance().createService(ILiveService.class);

    private boolean isMine;
    private String liveId;
    private LiveRoom liveRoom;
    private Uri bgLocal;

    private LiveShareDialog mLiveShareDialog;

    private CountDownTimer mCountDownTimer;

    public int followStatus; // 0为未关注

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_notice);
        ButterKnife.bind(this);
        initStatusBar();
        isMine = getIntent().getBooleanExtra("isMine", false);
        liveId = getIntent().getStringExtra("liveId");
        bgLocal = getIntent().getParcelableExtra("bgLocal");
        llRoot.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (bgLocal != null) {
                    loadBg(Glide.with(LiveNoticeActivity.this)
                            .load(bgLocal));
                } else {
                    loadBg(Glide.with(LiveNoticeActivity.this)
                            .load(getResources().getDrawable(R.drawable.img_create_live_room_bg)));
                }
                llRoot.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        if (bgLocal != null) {
            avatarIv.setImageURI(bgLocal);
        }
        shareBtn.setVisibility(isMine ? View.VISIBLE : View.INVISIBLE);
        followBtn.setVisibility(isMine? View.INVISIBLE: View.VISIBLE);
        title.setVisibility(isMine ? View.VISIBLE : View.INVISIBLE);
        getLiveInfo(liveId);
        getLiveForecastTime(liveId);
    }

    private void initStatusBar() {
        View decorView = getWindow().getDecorView();
        int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        decorView.setSystemUiVisibility(option);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void loadBg(RequestBuilder<Drawable> rb) {
        rb.apply(RequestOptions.bitmapTransform(new MultiTransformation<>(
                new CenterCrop(),
                new ColorFilterTransformation(Color.parseColor("#BA000000"))
        )))
                .into(new SimpleTarget<Drawable>(llRoot.getMeasuredWidth(), llRoot.getMeasuredHeight()) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        llRoot.setBackground(resource);
                    }
                });
    }

    private void getLiveInfo(String liveId) {
        APIManager.startRequest(service.getLive(liveId), new BaseRequestListener<LiveRoom>() {
            @Override
            public void onSuccess(LiveRoom result) {
                initLiveInfo(result);
            }
        }, this);
    }

    private void initLiveInfo(LiveRoom liveRoom) {
        this.liveRoom = liveRoom;
        tvLiveTitle.setText(liveRoom.title);
        if (bgLocal == null) {
            FrescoUtil.setImageSmall(avatarIv, liveRoom.liveImage);
            loadBg(Glide.with(LiveNoticeActivity.this)
                    .load(Uri.parse(liveRoom.liveImage)));
        }
        if (isMine) {
            llLiveStartTime.setVisibility(View.VISIBLE);
            llBottomBtnContainer.setVisibility(View.VISIBLE);
//            tvStartTime.setText(liveRoom.startTime);
            llLiveLeftTime.setVisibility(View.GONE);
            llBottomTipContainer.setVisibility(View.GONE);
        } else {
            llLiveStartTime.setVisibility(View.GONE);
            llBottomBtnContainer.setVisibility(View.GONE);
            llLiveLeftTime.setVisibility(View.VISIBLE);
            llBottomTipContainer.setVisibility(View.VISIBLE);
        }
        getLiveUser();
    }

    private void getLiveForecastTime(String liveId) {
        APIManager.startRequest(service.getLiveStartTime(liveId), new BaseRequestListener<LiveForecastTime>() {
            @Override
            public void onSuccess(LiveForecastTime result) {
                if (isMine) {
                    tvStartTime.setText(result.forecastDate);
                } else {
                    long leftMillSeconds = result.forecastTime;

                    if (mCountDownTimer != null) {
                        mCountDownTimer.cancel();
                    }
                    mCountDownTimer = new CountDownTimer(leftMillSeconds, 1000) {
                        @Override
                        public void onTick(long millisUntilFinished) {
                            updateLeftTime(millisUntilFinished);
                        }

                        @Override
                        public void onFinish() {
                            updateLeftTime(0);
                        }
                    }.start();
                }
            }
        }, this);
    }

    private void updateLeftTime(long millSecLeft) {

        long sec = (millSecLeft / 1000);
        long min = sec / 60;
        long hour = min / 60;
        long day = hour / 24;

        String html = String.format("距离开始仅剩：<span style=\"color:#32D482\">%02d</span>天" +
                        "<span style=\"color:#32D482\">%02d</span>时" +
                        "<span style=\"color:#32D482\">%02d</span>分" +
                        "<span style=\"color:#32D482\">%02d</span>秒",
                day, hour%24, min%60, sec%60);
        tvLeftTime.setText(Html.fromHtml(html));
    }

    public static void start(Context context, boolean isMine, String liveId) {
        start(context, isMine, liveId, null);
    }

    public static void start(Context context, boolean isMine, String liveId, Uri bgLocal) {
        Intent intent = new Intent(context, LiveNoticeActivity.class);
        intent.putExtra("isMine", isMine);
        intent.putExtra("liveId", liveId);
        if (bgLocal != null) {
            intent.putExtra("bgLocal", bgLocal);
        }
        context.startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        // 关闭可能存在的分享进度条
        ToastUtil.hideLoading();
    }

    @OnClick(R.id.back)
    public void back() {
        finish();
    }

    @OnClick(R.id.shareBtn)
    public void share() {
        final LiveRoom room = liveRoom;
        if (UiUtils.checkUserLogin(this) && null != room) {
            mLiveShareDialog = null;
            if (null == mLiveShareDialog) {
                User loginUser = SessionUtil.getInstance().getLoginUser();
                mLiveShareDialog = new LiveShareDialog(this,
                        room.title, room.title, room.liveImage, room.liveId, loginUser.invitationCode, room.nickName,true);
            }
            mLiveShareDialog.show();
        }
    }

    @OnClick(R.id.followBtn)
    public void follow() {
        if (followStatus == 1) {
            return;
        }
        final LiveRoom room = liveRoom;
        if (UiUtils.checkUserLogin(this) && null != room) {
            APIManager.startRequest(service.followAnchor(room.memberId, room.liveId), new BaseRequestListener<Object>() {
                @Override
                public void onSuccess(Object result) {
                    updateFollowBtnState();
                }
            }, this);
        }
    }

    private void getLiveUser() {
        APIManager.startRequest(service.getLiveUserInfo(liveRoom.memberId), new BaseRequestListener<LiveUser>() {
            @Override
            public void onSuccess(LiveUser result) {
                super.onSuccess(result);
                followStatus = result.followStatus;
                updateFollowBtnState();
            }
        }, this);
    }

    private void updateFollowBtnState() {
        if(followStatus == 1) {
            followBtn.setText("已关注");
            followBtn.setBackgroundResource(R.drawable.bg_followed);
        } else {
            followBtn.setText("+关注");
            followBtn.setBackgroundResource(R.drawable.bg_follow);
        }
    }

    @OnClick(R.id.tvDelete)
    public void delete() {
        // 删除预告
        WJDialog dialog = new WJDialog(this);
        dialog.show();
        dialog.setTitle("提示");
        dialog.setContentText("是否删除预告?");
        dialog.setCancelText("否");
        dialog.setConfirmText("是");
        dialog.setOnConfirmListener((v) -> {
            dialog.dismiss();
            ToastUtil.showLoading(this);
            APIManager.startRequest(service.deleteLiveBroadcast(liveId), new BaseRequestListener<Object>() {
                @Override
                public void onSuccess(Object result) {
                    ToastUtil.success("删除成功！");
                    ToastUtil.hideLoading();
                    EventBus.getDefault().post(new EventMessage(Event.deleteLive));
                    finish();
                }

                @Override
                public void onError(Throwable e) {
                    ToastUtil.hideLoading();
                }
            }, this);
        });

    }
    @OnClick(R.id.tvStart)
    public void start() {
        // 开始直播
        if (liveRoom != null) {
            startLive(liveId, liveRoom.memberId);
        }
    }
    /**
     * 发起直播
     */
    private void startLive(String liveId, String memberId) {
        LivePushActivity.startActivity(this, LiveManager.getAlivcLivePushConfig(this),
                liveId, true, false, false, LiveManager.mOrientationEnum,
                Camera.CameraInfo.CAMERA_FACING_FRONT, false, "", "",
                false, LiveManager.getAlivcLivePushConfig(this).isExternMainStream(), memberId);

    }
}
