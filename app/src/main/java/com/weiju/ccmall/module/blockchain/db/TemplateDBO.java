package com.weiju.ccmall.module.blockchain.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.weiju.ccmall.module.blockchain.beans.TemplateItem;
import com.weiju.ccmall.module.blockchain.beans.TransactionItem;

import java.util.ArrayList;
import java.util.List;

public class TemplateDBO {

    public static final int LOCAL_STATE_READY = 0; // 数据更新完成,未下载
    public static final int LOCAL_STATE_DOWNLOADED = 1; // 下载解压完成
    /**
     * id : 4
     * templateId : a8241f1d354f4b08aab91a82ebe797f3
     * templateName : 全网图表
     * remark : 啊啊
     * fileUrl : https://static.create-chain.net/ccmall/c0/34/66/55661153660d43f39500075241e86484.zip
     * status : 1
     * createDate : 2019-11-14 18:22:22
     * updateDate : 2019-11-14 18:39:01
     * deleteFlag : 0
     */

    private TemplateDatabaseHelper helper;
    private SQLiteDatabase db;
    private static final String TABLE_NAME = "template";
    private boolean isClosed = false;

    public TemplateDBO(Context context) {
        helper = new TemplateDatabaseHelper(context);
        db = helper.getWritableDatabase();
    }

    public void close() {
        if (db != null && db.isOpen()) {
            db.close();
        }
        isClosed = true;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void insert(TemplateItem item) {
        ContentValues cv = new ContentValues();
        cv.put("_id", item.id);
        cv.put("templateId", item.templateId);
        cv.put("templateName", item.templateName);
        cv.put("remark", item.remark);
        cv.put("fileUrl", item.fileUrl);
        cv.put("status", item.status);
        cv.put("createDate", item.createDate);
        cv.put("updateDate", item.updateDate);
        cv.put("deleteFlag", item.deleteFlag);
        db.insert(TABLE_NAME, null, cv);
    }

    public void update(TemplateItem item) {
        ContentValues cv = new ContentValues();
//        cv.put("_id", item.id);
        cv.put("templateId", item.templateId);
        cv.put("templateName", item.templateName);
        cv.put("remark", item.remark);
        cv.put("fileUrl", item.fileUrl);
        cv.put("status", item.status);
        cv.put("createDate", item.createDate);
        cv.put("updateDate", item.updateDate);
        cv.put("deleteFlag", item.deleteFlag);
        cv.put("localState", LOCAL_STATE_READY);
        db.update(TABLE_NAME, cv, "_id=?", new String[]{item.id+""});
    }

    public void updateLocalState(int _id, int localState) {
        ContentValues cv = new ContentValues();
        cv.put("localState", localState);
        db.update(TABLE_NAME, cv, "_id=?", new String[]{_id+""});
    }

    public TemplateItem query(int id) {
        String[] col = new String[]{"_id", "templateId", "updateDate"};
        Cursor cursor = db.query(TABLE_NAME, col, "_id=?", new String[]{id+""}, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int _id = cursor.getInt(0);
            String templateId = cursor.getString(1);
            String updateDate = cursor.getString(2);
            TemplateItem item = new TemplateItem();
            item.id = _id;
            item.templateId = templateId;
            item.updateDate = updateDate;
            cursor.close();
            return item;
        }
        return null;
    }

    public List<TemplateItem> queryByLocalState(int localState) {
        String[] col = new String[]{"_id", "templateId", "updateDate", "fileUrl"};
        Cursor cursor = db.query(TABLE_NAME, col, "localState=?", new String[]{localState+""}, null, null, null);
        if (cursor != null) {
            List<TemplateItem> ret = new ArrayList<>();
            while (cursor.moveToNext()) {
                int _id = cursor.getInt(0);
                String templateId = cursor.getString(1);
                String updateDate = cursor.getString(2);
                String fileUrl = cursor.getString(3);
                TemplateItem item = new TemplateItem();
                item.id = _id;
                item.templateId = templateId;
                item.updateDate = updateDate;
                item.fileUrl = fileUrl;
                ret.add(item);
            }
            cursor.close();
            return ret;
        }
        return null;
    }
}
