package com.weiju.ccmall.module.world.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/5/8.
 */
public class OrderEntity {
    /**
     * merchantOrderNo : 2311589150400715
     * memberId : 3c49f39386f246db92eae66bbc863fe7
     * recName : 测试
     * zipCode :
     * payType : WEIXIN_XCX
     * createDate : 2020-05-08 15:30:23
     * updateDate :
     * price : 0
     * actualPrice : 1
     * totalPrice : 1
     * discountPrice : 0
     * payTransactionId :
     * phone : 15988888888
     * telephone : 15988888888
     * payTime :
     * skuId :
     * skuName :
     * orderStatus : 1
     * gnum :
     * expressWeight : 0
     * properties :
     * supplyType :
     * deliverCity :
     * provinceName : 广东省
     * cityName : 广州市
     * regionName : 黄埔区
     * detail : 测试
     * serviceFee : 0
     * taxFee : 0
     * shippingFee : 0
     * tax :
     * isProfit : 0
     * count : 0
     * expressId :
     * expressStatus :
     * expressName :
     * expressCode :
     * orderSource : 2
     * nationName :
     * statusStr : 未付款
     * orderId : ad84746b3a9a4576a09ec1d4c15c4ec2
     * mainImagesUrl :
     * refundStatus :
     * errorRemark :
     * deliveryDate :
     * receiptDate :
     * rejectStatus :
     * rejectRemark :
     * refundRemark :
     * refundStatusStr : 未退款
     * applyRefundTime :
     * rejectTime :
     * refundTime :
     */

    @SerializedName("merchantOrderNo")
    public String merchantOrderNo;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("recName")
    public String recName;
    @SerializedName("zipCode")
    public String zipCode;
    @SerializedName("payType")
    public String payType;
    @SerializedName("payTypeStr")
    public String payTypeStr;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("price")
    public int price;
    @SerializedName("actualPrice")
    public int actualPrice;
    @SerializedName("totalPrice")
    public int totalPrice;
    @SerializedName("discountPrice")
    public int discountPrice;
    @SerializedName("payTransactionId")
    public String payTransactionId;
    @SerializedName("phone")
    public String phone;
    @SerializedName("telephone")
    public String telephone;
    @SerializedName("payTime")
    public String payTime;
    @SerializedName("skuId")
    public String skuId;
    @SerializedName("skuName")
    public String skuName;
    @SerializedName("orderStatus")
    public int orderStatus;
    @SerializedName("gnum")
    public String gnum;
    @SerializedName("expressWeight")
    public String expressWeight;
    @SerializedName("properties")
    public String properties;
    @SerializedName("supplyType")
    public String supplyType;
    @SerializedName("deliverCity")
    public String deliverCity;
    @SerializedName("provinceName")
    public String provinceName;
    @SerializedName("cityName")
    public String cityName;
    @SerializedName("regionName")
    public String regionName;
    @SerializedName("detail")
    public String detail;
    @SerializedName("serviceFee")
    public int serviceFee;
    @SerializedName("taxFee")
    public long taxFee;
    @SerializedName("shippingFee")
    public int shippingFee;
    @SerializedName("tax")
    public double tax;
    @SerializedName("isProfit")
    public int isProfit;
    @SerializedName("count")
    public int count;
    @SerializedName("expressId")
    public String expressId;
    @SerializedName("expressStatus")
    public String expressStatus;
    @SerializedName("expressName")
    public String expressName;
    @SerializedName("expressCode")
    public String expressCode;
    @SerializedName("orderSource")
    public String orderSource;
    @SerializedName("nationName")
    public String nationName;
    @SerializedName("statusStr")
    public String statusStr;
    @SerializedName("orderId")
    public String orderId;
    @SerializedName("mainImagesUrl")
    public String mainImagesUrl;
    @SerializedName("refundStatus")
    public int refundStatus;
    @SerializedName("errorRemark")
    public String errorRemark;
    @SerializedName("deliveryDate")
    public String deliveryDate;
    @SerializedName("receiptDate")
    public String receiptDate;
    @SerializedName("rejectStatus")
    public String rejectStatus;
    @SerializedName("rejectRemark")
    public String rejectRemark;
    @SerializedName("refundRemark")
    public String refundRemark;
    @SerializedName("refundStatusStr")
    public String refundStatusStr;
    @SerializedName("applyRefundTime")
    public String applyRefundTime;
    @SerializedName("rejectTime")
    public String rejectTime;
    @SerializedName("refundTime")
    public String refundTime;
    @SerializedName("countRateExc")
    public double countRateExc;
    /**
     * tax : 0.0
     * refundStatus : 0
     * rejectStatus : 0
     * itemId : CC16A375A17240888
     */

    @SerializedName("itemId")
    public String itemId;

    public String getAddressDetail() {
        return provinceName + cityName + regionName + detail;
    }
}
