package com.weiju.ccmall.module.xysh.activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.page.WebViewCreditLifeActivity;
import com.weiju.ccmall.module.user.QRCodeActivity;
import com.weiju.ccmall.module.user.model.UpMemberModel;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.adapter.XinYongUserAdapter;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.bean.Translation;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.bean.XyshUser;
import com.weiju.ccmall.module.xysh.fragment.XinYongHomeFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.ProfitMoney;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.XinYongUser;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.TranslationUpgradeDialog;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.BaseUrl;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * @author chenyanming
 * @time 2019/12/10 on 11:26
 * @desc
 */
public class XinYongUserActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.ivAvatar)
    SimpleDraweeView mIvAvatar;
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.tvPhone)
    TextView mTvPhone;
    //    @BindView(R.id.ivBack)
//    ImageView mIvBack;
    @BindView(R.id.tvProfit)
    TextView tvProfit;
    @BindView(R.id.tvRunningCount)
    TextView tvRunningCount;

    @BindView(R.id.tvMemberGrade)
    TextView mTvMemberGrade;
    @BindView(R.id.tvDescribe)
    TextView mTvDescribe;
    @BindView(R.id.tvCheckRates)
    TextView mTvCheckRates;
    @BindView(R.id.tvUpgrade)
    TextView mTvUpgrade;

    XyshUser xyshUser;
    private XinYongUserAdapter mAdapter = new XinYongUserAdapter();
    private IUserService service = ServiceManager.getInstance().createService(IUserService.class);
    private XyshService xyshService = ServiceManager.getInstance().createService2(XyshService.class);
    private TranslationUpgradeDialog mUpgradeDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        xyshUser = SessionUtil.getInstance().getXyshUserInfo();
        setContentView(R.layout.activity_xinyong_user);
        ButterKnife.bind(this);

        initView();
        initData();
        getProfit();
        boolean hasRealNameAuth = getIntent().getBooleanExtra("hasRealNameAuth", false);
        if (hasRealNameAuth) {
            mAdapter.getData().get(0).desc = "已实名";
            mAdapter.notifyDataSetChanged();
            xyshUser.rlAuthFlag = "1";
            SessionUtil.getInstance().setXyshUserInfo(xyshUser);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        boolean hasRealNameAuth = intent.getBooleanExtra("hasRealNameAuth", false);
        if (hasRealNameAuth) {
            mAdapter.getData().get(0).desc = "已实名";
            mAdapter.notifyDataSetChanged();
            xyshUser.rlAuthFlag = "1";
            SessionUtil.getInstance().setXyshUserInfo(xyshUser);
        }
    }

    private void getProfit() {
        APIManager.startRequest(service.memberProfit(), new BaseRequestListener<ProfitMoney>() {
            @Override
            public void onSuccess(ProfitMoney result) {
                super.onSuccess(result);
                if (!isDestroyed()) {
                    tvProfit.setText(String.format("累计收入 %s元", result.getProfitMoney()));
                }
            }
        }, this);

//        if (xyshUser.hasRealNameAuth()) {
//            hasRealNameAuth = true;
//            mAdapter.getData().get(0).desc = "已实名";
//        } else {
//            hasRealNameAuth = false;
//            mAdapter.getData().get(0).desc = "";
//        }
//        mAdapter.notifyDataSetChanged();


        APIManager.startRequest(xyshService.cardInfoGet(0), new Observer<QueryUserBankCardResult>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(QueryUserBankCardResult ret) {
                if ("0".equals(ret.resCode)) {
                    int planRunningCount = 0;
                    for (QueryUserBankCardResult.BankInfListBean item : ret.bankInfList) {
                        if (item.isRunning()) {
                            planRunningCount++;
                        }
                    }
                    if (planRunningCount > 0) {
                        tvRunningCount.setText(String.format("执行中: %d张", planRunningCount));
                    } else {
                        tvRunningCount.setText("");
                    }

                    if (!TextUtils.isEmpty(ret.usrName)) {
                        mTvName.setText(ret.usrName);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onComplete() {
            }
        });

        APIManager.startRequest(service.getUpMember(), new BaseRequestListener<UpMemberModel>() {
            @Override
            public void onSuccess(UpMemberModel result) {
                String desc = "";
                if (result.phone != null && result.phone.length() == 11) {
                    desc = result.phone.substring(0, 3) + "****" + result.phone.substring(7);
                }
                mAdapter.getData().get(1).desc = desc;
                mAdapter.notifyDataSetChanged();
            }
        }, this);
    }

    private void initData() {
        List<XinYongUser> list = new ArrayList<>();
        list.add(new XinYongUser(R.drawable.icon_shimingrenzheng, "实名认证", xyshUser.hasRealNameAuth() ? "已实名" : "未实名"));
//        list.add(new XinYongUser(R.drawable.icon_item_update, "我要升级", "即将开放"));
//        list.add(new XinYongUser(R.drawable.icon_tuijianren, "分享人", ""));
        list.add(new XinYongUser(R.drawable.icon_tuijianren, "分享人", ""));
//        list.add(new XinYongUser(R.drawable.icon_yinhangkaguanli, "银行卡管理", ""));
        list.add(new XinYongUser(R.drawable.icon_bangzhuzhongxin, "帮助中心", ""));
        mAdapter.setNewData(list);
        getMoveTag();
    }

    private void initView() {
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            QMUIStatusBarHelper.translucent(XinYongUserActivity.this);
//            QMUIStatusBarHelper.setStatusBarLightMode(XinYongUserActivity.this);
//            QMUIStatusBarHelper.setStatusBarDarkMode(XinYongUserActivity.this);
//        }
//        int height = QMUIStatusBarHelper.getStatusbarHeight(XinYongUserActivity.this);
//        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mIvBack.getLayoutParams();
//        layoutParams.topMargin = height;
        setLeftBlack();
        setTitle("我的");

        GridLayoutManager gridLayoutManager = new GridLayoutManager(XinYongUserActivity.this, 4);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setAdapter(mAdapter);


        User loginUser = SessionUtil.getInstance().getLoginUser();
        FrescoUtil.setImage(mIvAvatar, loginUser.avatar);
        mTvName.setText(loginUser.nickname);
        mTvPhone.setText(loginUser.phone);

        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                XinYongUser item = mAdapter.getItem(position);
                if (null != item) {
                    onClick(item.title);
                }
            }
        });
        initGradeView(xyshUser.usrLvl);
    }

    /**
     * @param grade Unkonw(-1, "异常"),
     *              V0(0, "大众会员",0),
     *              V1(1, "vip会员",0),
     *              V2(2, "黄金会员",9800),
     *              V3(3, "铂金会员",39800),
     *              V4(4,"钻石会员",99800);
     */
    private void initGradeView(int grade) {
        int drawableRightId = R.drawable.ic_member_normal;
        int colorId = R.color.color_ffd482;
        int drawableBgId = R.drawable.bg_shape_color_normal;
        switch (grade) {
            case 0:
                drawableRightId = R.drawable.ic_member_normal;
                colorId = R.color.color_ffd482;
                drawableBgId = R.drawable.bg_shape_color_normal;
                break;
            case 1:
                drawableRightId = R.drawable.ic_member_vip;
                colorId = R.color.color_f9c9a3;
                drawableBgId = R.drawable.bg_shape_color_vip;
                break;
            case 2:
                drawableRightId = R.drawable.ic_member_gold;
                colorId = R.color.color_fef7bc;
                drawableBgId = R.drawable.bg_shape_color_gold;
                break;
            case 3:
                drawableRightId = R.drawable.ic_member_platinum;
                colorId = R.color.color_f6f6f6;
                drawableBgId = R.drawable.bg_shape_color_platinum;
                break;
            case 4:
                drawableRightId = R.drawable.ic_member_diamond;
                colorId = R.color.color_cadcce;
                drawableBgId = R.drawable.bg_shape_color_diamond;
                break;
        }
        if (grade != -1) {
            Drawable drawable = getResources().getDrawable(drawableRightId);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
            mTvName.setCompoundDrawables(null, null, drawable, null);
//            mTvMemberGrade.setTextColor(getResources().getColor(colorId));
            mTvMemberGrade.setText(xyshUser.usrLvlName);
//            mTvDescribe.setTextColor(getResources().getColor(colorId));
            mTvDescribe.setText(xyshUser.remark);
//            mTvCheckRates.setTextColor(getResources().getColor(colorId));
//            mTvUpgrade.setBackground(getResources().getDrawable(drawableBgId));
            mTvUpgrade.setVisibility(grade == 4 ? View.GONE : View.VISIBLE);
        }
    }

    private void getMoveTag() {
        APIManager.startRequest(service.getMoveTag(), new BaseRequestListener<Translation>() {
            @Override
            public void onSuccess(Translation result) {
                super.onSuccess(result);
                if (result.moveTag) {
                    if (mUpgradeDialog == null)
                        mUpgradeDialog = new TranslationUpgradeDialog(XinYongUserActivity.this);
                    mUpgradeDialog.show();
                    mUpgradeDialog.setCurrentLevelText(xyshUser.usrLvlName);
                    mUpgradeDialog.setLatestLevelText(result.startVipStr);
                    mUpgradeDialog.setNextLevelText(result.endVipStr);
                    mUpgradeDialog.setOnConfirmListener(v -> moveCcpayVip());
                }

            }
        }, this);
    }

    private void moveCcpayVip() {
        APIManager.startRequest(service.moveCcpayVip(""), new BaseRequestListener<Object>(this) {
            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                mUpgradeDialog.dismiss();
                ToastUtil.success("系统正在处理中，请稍后查看。");
                finish();
//                updateUserInfo();
            }
        }, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUserInfo();
    }

    private void updateUserInfo() {
        ToastUtil.showLoading(this);
        APIManager.startRequest(xyshService.userPersonCenter(SessionUtil.getInstance().getOAuthToken()), new Observer<XYSHCommonResult<XyshUser>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<XyshUser> ret) {
                ToastUtil.hideLoading();
                if (ret.success) {
                    SessionUtil.getInstance().setXyshUserInfo(ret.data);
                    xyshUser = ret.data;
                    initGradeView(xyshUser.usrLvl);
                } else {
                    ToastUtil.error(ret.msg);
                }
            }

            @Override
            public void onError(Throwable e) {
                ToastUtil.error(e.getMessage());
                ToastUtil.hideLoading();
            }

            @Override
            public void onComplete() {
                ToastUtil.hideLoading();
            }
        });
    }

    private void onClick(String title) {
        switch (title) {
            case "实名认证":
                if (xyshUser.hasRealNameAuth()) {
                    ToastUtil.success("已实名！");
                } else {
                    RealNameAuthActivity.start(this);
                }
                break;
            case "我要升级":
                break;
            case "推荐人":
                break;
            case "银行卡管理":
                if (!xyshUser.hasRealNameAuth()) {
                    ToastUtil.error("您还没有实名，先去实名认证吧");
                    return;
                }
                BankAdminActivity.start(this);
                break;
            case "帮助中心":
//                startActivity(new Intent(XinYongUserActivity.this, HelpListActivity.class));
                HelpWebActivity.start(XinYongUserActivity.this);
                break;

            default:
        }
    }

//    @OnClick(R.id.ivBack)
//    protected void back() {
//        finish();
//    }

    @OnClick(R.id.layoutReceivables)
    protected void receivable() {
        startActivity(new Intent(XinYongUserActivity.this, ReceivablesListActvity.class));
    }

    @OnClick(R.id.llBankAdmin)
    protected void bankAdmin() {
        if (!xyshUser.hasRealNameAuth()) {
            ToastUtil.error("您还没有实名，先去实名认证吧");
            return;
        }
        BankAdminActivity.start(this);
    }

    @OnClick(R.id.ll_repayment_plan)
    public void onRepaymentPlan() {
        if (!xyshUser.hasRealNameAuth()) {
            XinYongHomeFragment.showRealNameAuthDialog(this);
            return;
        }
        RepaymentListActivity.start(this, false, RepaymentListActivity.FROM_PLAN);
    }

    @OnClick({R.id.tvCheckRates, R.id.tvUpgrade, R.id.rlInvite})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlInvite:
                QRCodeActivity.start(this, QRCodeActivity.TYPE_InvitationCodeByIconCreditCard);
                break;
            case R.id.tvCheckRates:
                WebViewCreditLifeActivity.start(this, String.format("%s/#/MyRate", getHost()), false, xyshUser.usrLvl, 2, xyshUser.usrTel);
                break;
            case R.id.tvUpgrade:
                WebViewCreditLifeActivity.start(this, String.format("%s/#/UpServer", getHost()), false, xyshUser.usrLvl, 2, xyshUser.usrTel);
                break;
        }
    }

    private String getHost() {
        String host;
        if (BuildConfig.DEBUG) {
            host = "http://" + BaseUrl.getInstance().getCCPayHostPost();
        } else {
            host = "https://" + BuildConfig.XYSH_HOST + "/customizYTH";
        }
        return host;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case creditLifeUpgradeSuccess:
//                updateUserInfo();
                finish();
                break;
        }
    }
}
