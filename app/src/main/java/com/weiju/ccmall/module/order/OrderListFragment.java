package com.weiju.ccmall.module.order;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.order.adapter.OrderListAdapter;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.OrderStatus;
import com.weiju.ccmall.shared.decoration.SpacesItemDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;

import static com.weiju.ccmall.module.order.OrderListActivity.MODE_LIVE_STORE;
import static com.weiju.ccmall.module.order.OrderListActivity.MODE_MY_GIFT;
import static com.weiju.ccmall.shared.Constants.PAGE_SIZE;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.order
 * @since 2017-07-06
 */
public class OrderListFragment extends BaseFragment implements PageManager.RequestListener {

    @BindView(R.id.noDataLayout)
    NoData mNoDataLayout;
    private Page mPage;

    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    private OrderListAdapter mOrderListAdapter;
    private PageManager mPageManager;
    private IOrderService mOrderService;
    private boolean isLoaded = false;

    Observable<RequestResult<PaginationEntity<Order, Object>>>
            requestResultObservable = null;

    public static OrderListFragment newInstance(Page page) {
        Bundle args = new Bundle();
        args.putSerializable("page", page);
        OrderListFragment fragment = new OrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.base_list_layout, container, false);
        ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        mOrderService = ServiceManager.getInstance().createService(IOrderService.class);

        initView();
        return view;
    }


    private void getIntentData() {
        mPage = (Page) getArguments().get("page");
    }

    private void initView() {
        getIntentData();
        mOrderListAdapter = new OrderListAdapter(getActivity(), mPage.model);
        mRecyclerView.setAdapter(mOrderListAdapter);
        try {
            mPageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(mRecyclerView)
                    .setItemDecoration(new SpacesItemDecoration(ConvertUtil.dip2px(15), true))
                    .setRequestListener(this)
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .build(getContext());
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }
//        mPageManager.onRefresh();
    }

    public void refresh() {
        mPageManager.onRefresh();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getUserVisibleHint() && !isLoaded) {
//            mPageManager.onRefresh();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    protected boolean isNeedLogin() {
        return true;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        if (isVisibleToUser) {
            super.setUserVisibleHint(isVisibleToUser);
            mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
            getIntentData();
            initData(1);
//            mPageManager.onRefresh();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
//        mOrderListAdapter.removeAllItems();
//        mPageManager.onRefresh();
        if (mOrderListAdapter != null) {
            mOrderListAdapter.onResume();
        }
    }

    @Override
    public void nextPage(final int page) {


        initData(page);

    }

    private void initData(int page) {
        if (mPage.model == MODE_LIVE_STORE) {
            requestResultObservable = mOrderService.orderList("all".equalsIgnoreCase(mPage.id) ?
                    null : OrderStatus.getCodeByKey(mPage.id), page, PAGE_SIZE);
        } else if (mPage.model == MODE_MY_GIFT) {
            requestResultObservable = mOrderService.getGiftOrderListByStatus("all".equalsIgnoreCase(mPage.id) ?
                    null : OrderStatus.getCodeByKey(mPage.id), page, PAGE_SIZE);
        } else if ("all".equalsIgnoreCase(mPage.id)) {
            requestResultObservable = mOrderService.getAllOrderList(page, "15", "");
        } else {
            switch (mPage.id) {
                case AppTypes.ORDER.SELLER_WAIT_SHIP:
                    requestResultObservable = mOrderService.getStoreOrderList(2, page, "");
                    break;
                case AppTypes.ORDER.SELLER_HAS_SHIP:
                    requestResultObservable = mOrderService.getStoreOrderList(3, page, "");
                    break;
                case AppTypes.ORDER.SELLER_HAS_COMPLETE:
                    requestResultObservable = mOrderService.getStoreOrderList(4, page, "");
                    break;
                case AppTypes.ORDER.SELLER_HAS_CLOSE:
                    requestResultObservable = mOrderService.getStoreOrderList(0, page, "");
                    break;
                case AppTypes.ORDER.SELLER_REFUND:
                    requestResultObservable = mOrderService.getStoreOrderList(AppTypes.ORDER.STATUS_SELLER_REFUND, page, "");
                    break;
                case AppTypes.ORDER.SELLER_ORDER_REFUND:
                    break;
                //新零售--店铺订单--全部
                case AppTypes.ORDER.STORE_ORDER_ALL:
//                    mNoDataLayout.setVisibility(View.VISIBLE);
                    requestResultObservable = mOrderService.getStoreOrderList(-1, page, "onnOrder");
                    break;
                //新零售--店铺订单--待发货
                case AppTypes.ORDER.STORE_ORDER_TO_BE_DELIVERED:
//                    mNoDataLayout.setVisibility(View.VISIBLE);
                    requestResultObservable = mOrderService.getStoreOrderList(2, page, "onnOrder");
                    break;
                //新零售--店铺店铺--待处理
                case AppTypes.ORDER.STORE_ORDER_TO_BE_PROCESSED:
                    requestResultObservable = mOrderService.getStoreOrderList(1, page, "onnOrder");
                    break;
                //新零售--店铺订单--已发货
                case AppTypes.ORDER.STORE_ORDER_SHPIPPED:
                    requestResultObservable = mOrderService.getStoreOrderList(3, page, "onnOrder");
//                    mNoDataLayout.setVisibility(View.VISIBLE);
                    break;
                //新零售--店铺订单--待评价
                case AppTypes.ORDER.STORE_ORDER_PENDING_COMMENT:
                    mNoDataLayout.setVisibility(View.VISIBLE);
                    break;
                //新零售--我的销售--全部
                case AppTypes.ORDER.MY_SELL_ORDER_ALL:
//                    mNoDataLayout.setVisibility(View.VISIBLE);
                    requestResultObservable = mOrderService.getSaleOrderList("", page, "onnOrder");
                    break;
                //新零售--我的销售--待付款
                case AppTypes.ORDER.MY_SELL_ORDER_PENDING_PAYMENT:
//                    mNoDataLayout.setVisibility(View.VISIBLE);
                    requestResultObservable = mOrderService.getSaleOrderList("1", page, "onnOrder");
                    break;
                //新零售--我的销售--未解冻
                case AppTypes.ORDER.MY_SELL_ORDER_FROZEN:
//                    mNoDataLayout.setVisibility(View.VISIBLE);
                    requestResultObservable = mOrderService.getSaleOrderList("2", page, "onnOrder");
                    break;
                //新零售--我的销售--已解冻
                case AppTypes.ORDER.MY_SELL_ORDER_NOT_FROZEN:
//                    mNoDataLayout.setVisibility(View.VISIBLE);
                    requestResultObservable = mOrderService.getSaleOrderList("3", page, "onnOrder");
                    break;
                //新零售--我的--订单--全部
                case AppTypes.ORDER.MY_ORDER_ALL:
                    requestResultObservable = mOrderService.getAllOrderList(page, "15", "onnOrder");
                    break;
                //新零售--我的--订单--待付款
                case AppTypes.ORDER.MY_ORDER_PENDING_PAYMENT:
                    requestResultObservable = mOrderService.getOrderListByStatus(12, page, "onnOder");
                    break;
                //新零售--我的--订单--待确认
                case AppTypes.ORDER.MY_ORDER_TO_BE_CONFIRMED:
                    requestResultObservable = mOrderService.getOrderListByStatus(10, page, "onnOrder");
                    break;
                //新零售--我的--订单--待发货
                case AppTypes.ORDER.MY_ORDER_TO_BE_DELIVERED:
                    requestResultObservable = mOrderService.getOrderListByStatus(2, page, "onnOrder");
                    break;
                //新零售--我的--订单--待收货
                case AppTypes.ORDER.MY_ORDER_PEDNDING_RECEIPT:
                    requestResultObservable = mOrderService.getOrderListByStatus(3, page, "onnOrder");
                    break;
                default:
                    requestResultObservable = mOrderService.getOrderListByStatus(OrderStatus.getCodeByKey(mPage.id), page, "");
                    break;
            }
        }

        APIManager.startRequest(requestResultObservable, new BaseRequestListener<PaginationEntity<Order, Object>>(mRefreshLayout) {
            @Override
            public void onSuccess(PaginationEntity<Order, Object> result) {
                if (page == 1) {
                    if (mPage.model == MODE_LIVE_STORE && mPage.id.equals("paid")) {
                        ((OrderListActivity) getActivity()).upDateCount(result.total);
                    }
                    mOrderListAdapter.removeAllItems();
                }
                mOrderListAdapter.addItems((List<Order>) result.list);
                mPageManager.setTotalPage(result.totalPage);
                mPageManager.setLoading(false);

                mNoDataLayout.setVisibility(mOrderListAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onComplete() {
                super.onComplete();
                mRefreshLayout.setRefreshing(false);
                isLoaded = true;
            }
        }, getContext());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
        if (message.getEvent().equals(Event.confirmUpgrade) || message.getEvent().equals(Event.createOrderSuccess) || message.getEvent().equals(Event.newRetailPaySuccess) || message.getEvent().equals(Event.cancelOrder) || message.getEvent().equals(Event.refundOrder) || message.getEvent().equals(Event.paySuccess) || message.getEvent().equals(Event.refundOrder) || message.getEvent().equals(Event.finishOrder)) {
            if (mOrderListAdapter == null) {
                return;
            }
            mPageManager.onRefresh();
        } else if (message.getEvent().equals(Event.orderChange)) {
            refresh();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(MsgStatus message) {
        if (message.getAction() == MsgStatus.ACTION_STORE_SHIT_SUCCEED || message.getAction() == MsgStatus.ACTION_REFUND_CHANGE) {
            mPageManager.onRefresh();
        }
    }

}
