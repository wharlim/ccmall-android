package com.weiju.ccmall.module.world.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.world.IWorldService;
import com.weiju.ccmall.module.world.activity.WorldProductDetailActivity;
import com.weiju.ccmall.module.world.adapter.WorldCatgoryAdapter;
import com.weiju.ccmall.module.world.entity.CatgoryItemEntity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseObserver;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WorldCatgoryFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    public RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    public SmartRefreshLayout mRefreshLayout;
    @BindView(R.id.layoutNodata)
    LinearLayout mLayoutNodata;

    private List<CatgoryItemEntity> mData = new ArrayList<>();
    private WorldCatgoryAdapter mAdapter = new WorldCatgoryAdapter(mData);

    private int mPage;
    private String mCategoryId;
    private IWorldService mService = ServiceManager.getInstance().createService(IWorldService.class);


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        Fresco.initialize(this.getActivity());
        View rootView = inflater.inflate(R.layout.fragment_world_catgory, container, false);
        ButterKnife.bind(this, rootView);
        getIntentData();
        initView();
//        getData(true);
        mRefreshLayout.autoRefresh();
        return rootView;
    }

    public void getIntentData() {
        mCategoryId = getArguments().getString("categoryId");
    }

    public void initView() {
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.bg_gray));
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);

        int padding = SizeUtils.dp2px(7);
        mRecyclerView.setPadding(0, 0, padding, padding);

        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(this::onListItemClick);
        mRefreshLayout.setOnRefreshListener(refreshLayout -> {
            getData(true);
        });
//        mRefreshLayout.setEnableLoadMore(true);
        mRefreshLayout.setOnLoadMoreListener(refreshLayout -> {
            getData(false);
        });
//        mRefreshLayout.setEnableHeaderTranslationContent(false);
        mRefreshLayout.setEnableOverScrollBounce(false);//是否启用越界回弹
        mRefreshLayout.setEnableLoadMoreWhenContentNotFull(false);//在内容不满一页的时候，是否可以上拉加载更多
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && mRefreshLayout != null) mRefreshLayout.autoRefresh();
    }

    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        CatgoryItemEntity itemEntity = mAdapter.getData().get(position);
        LogUtils.d("onListItemClick position:" + position + "skuName:" + itemEntity.skuName);
        WorldProductDetailActivity.start(getContext(), itemEntity.itemId);
    }

    public void getData(final boolean isRefresh) {
        if (mCategoryId == null || !getUserVisibleHint()) {
            mRefreshLayout.finishRefresh(false);
            return;
        }
        if (isRefresh) {
            mPage = 1;
//            mRefreshLayout.setNoMoreData(false);
        } else {
            mPage++;
        }
        APIManager.startRequest(mService.getGoods(mCategoryId, mPage, 20),
                new BaseObserver<PaginationEntity<CatgoryItemEntity, Object>>(getContext()) {
                    @Override
                    public void onHandleSuccess(PaginationEntity<CatgoryItemEntity, Object> result) {
                        // 修复异步网络请求加载后，页面已经关闭导致的崩溃
                        if (getActivity() == null || !isAdded()) {
                            return;
                        }
                        if (isRefresh) mRefreshLayout.finishRefresh(true);
                        else mRefreshLayout.finishLoadMore(true);

                        if (mPage == 1) mData.clear();
                        mData.addAll(result.list);
                        mAdapter.notifyDataSetChanged();
                        if (result.page >= result.totalPage) {
                            mRefreshLayout.finishLoadMoreWithNoMoreData();
                        }
//
                        if (mData.size() > 0) {
                            mLayoutNodata.setVisibility(View.GONE);
                        } else {
                            mLayoutNodata.setVisibility(View.VISIBLE);
                        }
//                        if (result.page >= result.totalPage) {
//                            mAdapter.loadMoreEnd(isRefresh && mData.size() < 5);
//                        } else {
//                            mAdapter.loadMoreComplete();
//                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        // 修复异步网络请求加载后，页面已经关闭导致的崩溃
                        if (getActivity() == null || !isAdded()) {
                            return;
                        }
                        ToastUtil.error(e.getMessage());
                        if (isRefresh) mRefreshLayout.finishRefresh(false);
                        else mRefreshLayout.setNoMoreData(false);

                    }

                });
    }


    public static WorldCatgoryFragment newInstance(String categoryId) {
        Bundle args = new Bundle();
        args.putString("categoryId", categoryId);
        WorldCatgoryFragment fragment = new WorldCatgoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
