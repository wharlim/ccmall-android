package com.weiju.ccmall.module.xysh.activity.repayment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 一张卡的还款计划列表
 */
public class RepaymentDetailActivity extends BaseActivity {
    @BindView(R.id.ivAvatar)
    SimpleDraweeView ivAvatar;
    @BindView(R.id.tvBankName)
    TextView tvBankName;
    @BindView(R.id.tvCardType)
    TextView tvCardType;
    @BindView(R.id.tvRePayDate)
    TextView tvRePayDate;
    @BindView(R.id.tvCardNumber)
    TextView tvCardNumber;
    @BindView(R.id.tvCheckPlan)
    TextView tvCheckPlan;
    @BindView(R.id.tvBill)
    TextView tvBill;
    @BindView(R.id.tvRepay)
    TextView tvRepay;
    @BindView(R.id.iv_state)
    ImageView ivState;
    @BindView(R.id.tv_tab_all)
    TextView tvTabAll;
    @BindView(R.id.tv_tab_repaying)
    TextView tvTabRepaying;
    @BindView(R.id.tv_tab_repay_success)
    TextView tvTabRepaySuccess;
    @BindView(R.id.tv_tab_repay_fail)
    TextView tvTabRepayFail;
    @BindView(R.id.viewPager)
    ViewPager viewPager;

    QueryUserBankCardResult.BankInfListBean creditCard;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repayment_detail);
        ButterKnife.bind(this);
        creditCard = (QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("creditCard");
        initCreditCard();
        setTitle("还款计划");
        setLeftBlack();
        setUpViewPager();
    }

    private void initCreditCard() {
        ivAvatar.setImageResource(BankUtils.getBankIconByName(creditCard.bankName));
        tvBankName.setText(creditCard.bankName);
        tvCardNumber.setText(BankUtils.formatBankCardNo(creditCard.cardNo));
        tvCheckPlan.setVisibility(View.GONE);
        tvRePayDate.setVisibility(creditCard.hasPlan() ? View.VISIBLE : View.GONE);
        tvBill.setText(String.format("账单日  %s日", creditCard.billDate));
        tvRepay.setText(String.format("还款日  %s日", creditCard.repayDate));
        if (creditCard.isRunning()) {
            ivState.setImageResource(R.drawable.ic_plan_running);
        } else if (creditCard.isSuccess()) {
            ivState.setImageResource(R.drawable.ic_plan_success);
        } else if (creditCard.isFail()){
            ivState.setImageResource(R.drawable.ic_plan_fail);
        } else {
            ivState.setImageResource(R.drawable.ic_plan_none);
        }
    }

    private void setUpViewPager() {
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                return RepaymentPlanListFragment.newInstance(i, creditCard);
            }

            @Override
            public int getCount() {
                return 4;
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {
                tvTabAll.setSelected(false);
                tvTabRepaying.setSelected(false);
                tvTabRepaySuccess.setSelected(false);
                tvTabRepayFail.setSelected(false);
                switch (i) {
                    case 0:
                        tvTabAll.setSelected(true);
                        break;
                    case 1:
                        tvTabRepaying.setSelected(true);
                        break;
                    case 2:
                        tvTabRepaySuccess.setSelected(true);
                        break;
                    case 3:
                        tvTabRepayFail.setSelected(true);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        viewPager.setCurrentItem(0);
        tvTabAll.setSelected(true);
    }

    public static void start(Context context, QueryUserBankCardResult.BankInfListBean creditCard) {
        Intent activity = new Intent(context, RepaymentDetailActivity.class);
        activity.putExtra("creditCard", creditCard);
        context.startActivity(activity);
    }

    @OnClick({R.id.tv_tab_all, R.id.tv_tab_repaying, R.id.tv_tab_repay_success, R.id.tv_tab_repay_fail})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_tab_all:
                viewPager.setCurrentItem(0, true);
                break;
            case R.id.tv_tab_repaying:
                viewPager.setCurrentItem(1, true);
                break;
            case R.id.tv_tab_repay_success:
                viewPager.setCurrentItem(2, true);
                break;
            case R.id.tv_tab_repay_fail:
                viewPager.setCurrentItem(3, true);
                break;
        }
    }
}
