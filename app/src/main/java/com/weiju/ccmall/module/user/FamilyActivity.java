package com.weiju.ccmall.module.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blankj.utilcode.utils.ConvertUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.adapter.FamilyAdapter;
import com.weiju.ccmall.module.user.adapter.FamilyRecommendTopAdapter;
import com.weiju.ccmall.module.user.adapter.VipTypeMenuAdapter;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Family;
import com.weiju.ccmall.shared.bean.VipTypeInfo;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author zjm
 * @date 2018/1/19
 */
public class FamilyActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {


    @BindView(R.id.rvList)
    RecyclerView mRvList;
    @BindView(R.id.layoutRefresh)
    SwipeRefreshLayout mLayoutRefresh;

    private QMUIPopup mListPopup;

    private ArrayList<Family.DatasEntity> mDatas = new ArrayList<>();
    private FamilyAdapter mAdapter = new FamilyAdapter(mDatas);
    private IUserService mService;
    private List<VipTypeInfo> mMenuData = new ArrayList<>();
    ;
    private VipTypeMenuAdapter mMenuAdapter = new VipTypeMenuAdapter(mMenuData);
    private int mCurrentType = 99;

    private ViewGroup headerView;
    private View topLabel;
    private RecyclerView topRecycler;
    private FamilyRecommendTopAdapter topAdapter;
    private View moreBtn;
    private TextView customerCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);
        ButterKnife.bind(this);
        initView();
        initData();
        initHeader();
    }

    private void initData() {
        mService = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(
                mService.getVipTypeInfo(),
                new BaseRequestListener<List<VipTypeInfo>>(mLayoutRefresh) {
                    @Override
                    public void onSuccess(List<VipTypeInfo> result) {
                        super.onSuccess(result);
                        mMenuData.clear();
                        mMenuData.add(new VipTypeInfo(99, "查看全部"));
                        mMenuData.addAll(result);
                        getData(true);
                    }
                },this
        );
        loadRecommendCustomer();
    }

    private void initView() {
        setTitle("我的客户");
        setLeftBlack();
        getHeaderLayout().setRightDrawable(R.mipmap.icon_filter);
        getHeaderLayout().setOnRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenuListDialog();
            }
        });

        getHeaderLayout().setRight2Drawable(R.mipmap.icon_search2);
        getHeaderLayout().setOnRight2ClickListener(v -> {
            FamilyRecommendSearchActivity.start(this);
        });

        mRvList.setAdapter(mAdapter);
        mRvList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter.setEmptyView(new NoData(this));
        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getData(false);
            }
        });
        mAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                clickMember(position);
            }
        });
        mLayoutRefresh.setOnRefreshListener(this);
        setLayoutRefresh(mLayoutRefresh);
    }

    private void initHeader() {
        headerView = (ViewGroup) LayoutInflater.from(this).inflate(R.layout.item_family_top, mRvList, false);
        mAdapter.addHeaderView(headerView);
        topLabel = headerView.findViewById(R.id.topLabel);
        topRecycler = headerView.findViewById(R.id.topRecycler);
        moreBtn = headerView.findViewById(R.id.moreBtn);
        customerCount = headerView.findViewById(R.id.customerCount);

        topRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        topAdapter = new FamilyRecommendTopAdapter();
        List<Family.DatasEntity> datas = new ArrayList<>();
//        datas.add(new FamilyRecommend());
//        datas.add(new FamilyRecommend());
        topAdapter.setDatas(datas);
        topRecycler.setAdapter(topAdapter);

        moreBtn.setOnClickListener(v -> {
            FamilyRecommendActivity.start(this);
        });
    }

    private void clickMember(int position) {
    }

    private void showMenuListDialog() {
        if (mListPopup == null) {
            RecyclerView rvMenu = new RecyclerView(this);
            rvMenu.setLayoutManager(new LinearLayoutManager(this));
            rvMenu.setAdapter(mMenuAdapter);
            mMenuAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    selectMenuItem(position);
                }
            });
            mListPopup = new QMUIPopup(this);
            mListPopup.setContentView(rvMenu);
            mListPopup.setPositionOffsetX(ConvertUtils.dp2px(-5));
            mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_RIGHT);
            mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_NONE);
        }
        mListPopup.show(getHeaderLayout().getRightAnchor());
    }

    private void selectMenuItem(int position) {
        mMenuAdapter.setSelectPosition(position);
        mMenuAdapter.notifyDataSetChanged();
        mCurrentType = mMenuData.get(position).vipType;
        mListPopup.dismiss();
        getData(true);
    }

    private void getData(boolean isRefresh) {
        if (isRefresh) {
            mDatas.clear();
        }
        APIManager.startRequest(
                mService.getFamilyList(mDatas.size() / Constants.PAGE_SIZE + 1, Constants.PAGE_SIZE, 1, mCurrentType)
                , new BaseRequestListener<Family>(mLayoutRefresh) {
                    @Override
                    public void onSuccess(Family result) {
                        mDatas.addAll(result.datas);
                        mAdapter.notifyDataSetChanged();
                        customerCount.setText(String.format("我的客户共%d人", result.totalRecord));
                        if (result.datas.size() < Constants.PAGE_SIZE) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        mAdapter.loadMoreFail();
                    }
                },this);
    }

    @Override
    public void onRefresh() {
        getData(true);
    }

    private void loadRecommendCustomer() {
        BaseRequestListener<Family> l = new BaseRequestListener<Family>() {
            @Override
            public void onSuccess(Family result) {
                super.onSuccess(result);
                topAdapter.setDatas(result.datas);
                if (result.datas == null || result.datas.size() == 0) {
                    topLabel.setVisibility(View.GONE);
                    topRecycler.setVisibility(View.GONE);
                    moreBtn.setVisibility(View.GONE);
                }
            }
        };
        APIManager.startRequest(mService.getAwardClientList(1, 2), l, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ChangeCustomerNoteNameActivity.REQUEST_CODE && data != null) {
                String memberId = data.getStringExtra("memberId");
                String noteName = data.getStringExtra("noteName");
                for(int i = 0; i < mDatas.size(); ++i) {
                    if (Objects.equals(mDatas.get(i).memberId, memberId)) {
                        mDatas.get(i).noteName = noteName;
                    }
                }
                mAdapter.notifyDataSetChanged();
                ArrayList<Family.DatasEntity> recommend = topAdapter.getDatas();
                for (int i = 0; i < recommend.size(); ++i) {
                    if (Objects.equals(recommend.get(i).memberId, memberId)) {
                        recommend.get(i).noteName = noteName;
                    }
                }
                topAdapter.notifyDataSetChanged();
            }
        }
    }
}
