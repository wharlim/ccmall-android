package com.weiju.ccmall.module.xysh.activity.realname_auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.SelectBankActivity;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.Bank;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.component.dialog.CityPickerDialog;
import com.weiju.ccmall.shared.constant.Action;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.card.payment.CardIOActivity;
import io.card.payment.CreditCard;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class RealNameBankCardActivity extends BaseActivity {
    private static final int MY_SCAN_REQUEST_CODE = 10;
    @BindView(R.id.etCardNumber)
    EditText etCardNumber;

    @BindView(R.id.etPhone)
    EditText etPhone;

    @BindView(R.id.tvSelectBank)
    TextView tvSelectBank;

    CityPickerDialog mCityPickerDialog;
    @BindView(R.id.tv_address)
    TextView tvAddress;

    String mBankName;
    String mBankNumber;

    String mProvince;
    String mCity;
    String mCounty;

    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    @BindView(R.id.ivBankIcon)
    ImageView ivBankIcon;
    @BindView(R.id.tvBankNameNo)
    TextView tvBankNameNo;
    @BindView(R.id.tvBankCardType)
    TextView tvBankCardType;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realname_bankcard);
        ButterKnife.bind(this);
        setLeftBlack();
        setTitle("实名认证");
        mCityPickerDialog = new CityPickerDialog(this);
        etCardNumber.setOnFocusChangeListener((v, hasFocus) -> {
            if (!hasFocus) {
                checkCardInfo();
            }
        });
    }

    private void checkCardInfo() {
        String cardNo = etCardNumber.getText().toString().trim().replace(" ", "");
        BankUtils.checkCard(this, cardNo, BankUtils.CARD_CHECK_ACCEPT_DEBIT, (pass, type) -> {
            if (pass) {
                if ("DC".equals(type)) {
                    tvBankCardType.setText("储蓄卡");
                } else if ("CC".equals(type)) {
                    tvBankCardType.setText("信用卡");
                }
                updateCardNo();
            }
        });
    }

    private void updateCardNo() {
        String bankName = "银联";
        if (!TextUtils.isEmpty(mBankName)) {
            bankName = mBankName;
        }
        ivBankIcon.setImageResource(BankUtils.getBankIconByName(bankName));
        String cutCardNo = "0000";
        String cardNo = etCardNumber.getText().toString().trim().replace(" ", "");
        if (!TextUtils.isEmpty(cardNo) && cardNo.length() >= 4) {
            cutCardNo = BankUtils.cutBankCardNo(cardNo);
        }
        tvBankNameNo.setText(String.format("%s(%s)", bankName, cutCardNo));
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, RealNameBankCardActivity.class);
        context.startActivity(intent);
    }

    @OnClick(R.id.tv_realname_auth)
    public void onAuth() {
//        RealNameAuthResultActivity.start(this, false);
        String cardNo = etCardNumber.getText().toString().trim().replace(" ", "");
        if (TextUtils.isEmpty(cardNo)) {
            ToastUtil.error("请输入银行卡号");
            return;
        }
        if (TextUtils.isEmpty(mBankName) || TextUtils.isEmpty(mBankNumber)) {
            ToastUtil.error("请选择银行");
            return;
        }
        String phone = etPhone.getText().toString().trim();
        if (!UiUtils.checkETPhone(etPhone)) {
            return;
        }

        if (TextUtils.isEmpty(mProvince) || TextUtils.isEmpty(mCity) || TextUtils.isEmpty(mCounty)) {
            ToastUtil.error("请选择开户地区");
            return;
        }

        User user = SessionUtil.getInstance().getLoginUser();
        if (user == null) {
            ToastUtil.error("请先登录");
            return;
        }

        BankUtils.checkCard(this, cardNo, BankUtils.CARD_CHECK_ACCEPT_DEBIT, (pass, type) -> {
            if (pass) {
                ToastUtil.showLoading(this, true);
                APIManager.startRequest(service.userBankRelauth(
                        mBankNumber,
                        mBankName,
                        user.phone,
                        cardNo,
                        phone,
                        SessionUtil.getInstance().getOAuthToken(),
                        mProvince,
                        mCity,
                        mCounty
                ), new Observer<XYSHCommonResult<Object>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(XYSHCommonResult<Object> ret) {
                        ToastUtil.hideLoading();
                        if ("0".equals(ret.resCode)) {
                            RealNameAuthResultActivity.start(RealNameBankCardActivity.this, true);
                        } else {
                            ToastUtil.error(ret.resMsg + "");
                            RealNameAuthResultActivity.start(RealNameBankCardActivity.this, false);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
            } else {
                ToastUtil.error("卡号不正确");
            }
        });
    }

    @OnClick(R.id.ivScanCard)
    public void onScanCard() {
        Intent scanIntent = new Intent(this, CardIOActivity.class)
                .putExtra(CardIOActivity.EXTRA_HIDE_CARDIO_LOGO, true)//去除水印
//                .putExtra(CardIOActivity.EXTRA_SUPPRESS_MANUAL_ENTRY, true)//去除键盘
                .putExtra(CardIOActivity.EXTRA_LANGUAGE_OR_LOCALE, "zh-Hans")//设置提示为中文
                ;
        startActivityForResult(scanIntent, MY_SCAN_REQUEST_CODE);
    }

    @OnClick(R.id.layoutSelectBank)
    public void onSelectBank() {
//        Intent intent = new Intent(this, SelectBankActivity.class);
//        startActivityForResult(intent, Action.SELECT_BANK);
        SelectBankActivity.start(this, Action.SELECT_BANK);
    }

    @OnClick(R.id.ll_select_address)
    public void onSelectAddress() {
        mCityPickerDialog.showPickerView(new CityPickerDialog.CitySelectListener() {
            @Override
            public void select(String province, String city, String county) {
                tvAddress.setText(province + " " + city + " " + county);
                mProvince = province;
                mCity = city;
                mCounty = county;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SCAN_REQUEST_CODE) {
            if (data != null && data.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)) {
                CreditCard scanResult = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT);
                etCardNumber.setText(scanResult.cardNumber);
                checkCardInfo();
            }
        } else if (requestCode == Action.SELECT_BANK) {
            if (null != data) {
                Bank bank = SelectBankActivity.obtainResult(data);
                if (null != bank) {
                    tvSelectBank.setText(bank.bankName);
                    tvSelectBank.setTextColor(getResources().getColor(R.color.text_black));
                    mBankNumber = bank.bankNo;
                    mBankName = bank.bankName;
                    updateCardNo();
                }
            }
        }
    }
}
