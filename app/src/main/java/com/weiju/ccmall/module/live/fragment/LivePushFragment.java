package com.weiju.ccmall.module.live.fragment;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alivc.live.pusher.AlivcLivePushBGMListener;
import com.alivc.live.pusher.AlivcLivePushError;
import com.alivc.live.pusher.AlivcLivePushErrorListener;
import com.alivc.live.pusher.AlivcLivePushInfoListener;
import com.alivc.live.pusher.AlivcLivePushNetworkListener;
import com.alivc.live.pusher.AlivcLivePusher;
import com.blankj.utilcode.utils.AppUtils;
import com.blankj.utilcode.utils.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.module.live.activity.LivePushActivity;
import com.weiju.ccmall.module.live.adapter.LiveChatMessageAdapter;
import com.weiju.ccmall.module.live.adapter.LiveHorizontalUserAdapter;
import com.weiju.ccmall.module.user.SetPasswordActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.AddLive;
import com.weiju.ccmall.shared.bean.HasPasswordModel;
import com.weiju.ccmall.shared.bean.LiveMessage;
import com.weiju.ccmall.shared.bean.LiveRedEnvelopes;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.LivingRoomHotDataMsg;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.MaxHeightRecyclerView;
import com.weiju.ccmall.shared.component.dialog.LiveBannedListDialog;
import com.weiju.ccmall.shared.component.dialog.LiveBlockDialog;
import com.weiju.ccmall.shared.component.dialog.LiveCouponListDialog;
import com.weiju.ccmall.shared.component.dialog.LiveMeiYanDialog;
import com.weiju.ccmall.shared.component.dialog.LiveProductDialog;
import com.weiju.ccmall.shared.component.dialog.LiveRedListDialog;
import com.weiju.ccmall.shared.component.dialog.LiveSendCouponDialog;
import com.weiju.ccmall.shared.component.dialog.LiveSendMessageDialog;
import com.weiju.ccmall.shared.component.dialog.LiveSendRedDialog;
import com.weiju.ccmall.shared.component.dialog.LiveShareDialog;
import com.weiju.ccmall.shared.component.dialog.LiveUserDialog;
import com.weiju.ccmall.shared.component.dialog.LiveUserInfoDialog;
import com.weiju.ccmall.shared.component.dialog.MusicDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PopupWindowManage;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.message.ChatroomEndMessage;
import com.weiju.ccmall.shared.message.ChatroomGoodsMessage;
import com.weiju.ccmall.shared.message.ChatroomRedMessage;
import com.weiju.ccmall.shared.message.ChatroomTextMessage;
import com.weiju.ccmall.shared.message.ChatroomUserQuit;
import com.weiju.ccmall.shared.message.ChatroomWelcomeMessage;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ChatroomKit;
import com.weiju.ccmall.shared.util.DataInterface;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.SharedPreferenceUtils;
import com.weiju.ccmall.shared.util.SoftKeyBoardListener;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.rong.imlib.RongIMClient;
import io.rong.message.CommandNotificationMessage;
import io.rong.message.TextMessage;
import xiao.free.horizontalrefreshlayout.HorizontalRefreshLayout;
import xiao.free.horizontalrefreshlayout.RefreshCallBack;
import xiao.free.horizontalrefreshlayout.refreshhead.LoadingRefreshHeader;

import static com.alivc.live.pusher.AlivcLivePushCameraTypeEnum.CAMERA_TYPE_BACK;
import static com.alivc.live.pusher.AlivcLivePushCameraTypeEnum.CAMERA_TYPE_FRONT;

/**
 * @author chenyanming
 * @time 2019/12/18 on 17:37
 * @desc
 */
public class LivePushFragment extends Fragment implements Runnable {
    public static final String TAG = "LivePushFragment";

    private static final String LIVE_ID = "live_id";
    private static final String ASYNC_KEY = "async_key";
    private static final String AUDIO_ONLY_KEY = "audio_only_key";
    private static final String VIDEO_ONLY_KEY = "video_only_key";
    private static final String QUALITY_MODE_KEY = "quality_mode_key";
    private static final String CAMERA_ID = "camera_id";
    private static final String FLASH_ON = "flash_on";
    private static final String AUTH_TIME = "auth_time";
    private static final String PRIVACY_KEY = "privacy_key";
    private static final String MIX_EXTERN = "mix_extern";
    private static final String MIX_MAIN = "mix_main";
    private final long REFRESH_INTERVAL = 2000;


    @BindView(R.id.ivAvatar)
    SimpleDraweeView mIvAvatar;
    @BindView(R.id.viewMoreLine)
    View mViewMoreLine;
    @BindView(R.id.tvLiuChang)
    TextView mTvLiuChang;
    @BindView(R.id.tvGaoQing)
    TextView mTvGaoQing;
    @BindView(R.id.tvChaoQing)
    TextView mTvChaoQing;
    @BindView(R.id.layoutLiveing)
    RelativeLayout mLayoutLiveing;
    @BindView(R.id.layoutStart)
    RelativeLayout mLayoutStart;
    @BindView(R.id.layoutStartLive)
    LinearLayout mLayoutStartLive;
    @BindView(R.id.layoutBottom)
    LinearLayout mLayoutBottom;

    @BindView(R.id.ivUser1)
    SimpleDraweeView mIvUserOne;
    @BindView(R.id.ivUser2)
    SimpleDraweeView mIvUserTwo;
    @BindView(R.id.ivUser3)
    SimpleDraweeView mIvUserThree;
    @BindView(R.id.ivUser4)
    SimpleDraweeView mIvUserFour;
    @BindView(R.id.tvLiveUser)
    TextView mTvLiveUser;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvPopularity)
    TextView mTvPopularity;
    @BindView(R.id.tvQingXi)
    TextView mTvQingXi;
    @BindView(R.id.recyclerViewMessage)
    MaxHeightRecyclerView mMessageRecyclerView;
    @BindView(R.id.ivCouponList)
    ImageView mIvCouponList;

    @BindView(R.id.rv_selections)
    RecyclerView mRecycler;
    @BindView(R.id.refresh)
    HorizontalRefreshLayout refreshLayout;
    @BindView(R.id.ivGoods)
    SimpleDraweeView ivGoods;
    @BindView(R.id.ivCloseGoods)
    ImageView ivCloseGoods;
    @BindView(R.id.tvGoodsName)
    TextView tvGoodsName;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.llGoods)
    LinearLayout llGoods;


    private AlivcLivePusher mAlivcLivePusher = null;
    private String mPushUrl = null;
    private SurfaceView mSurfaceView = null;
    private boolean mAsync = false;

    private boolean mAudio = false;
    private boolean mVideoOnly = false;
    private boolean isPushing = false;
    private Handler mHandler = new Handler();

    private LivePushActivity.PauseState mStateListener = null;
    private int mCameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
    private boolean isFlash = false;
    private boolean mMixExtern = false;
    private boolean mMixMain = false;
    private boolean flashState = true;


    private int mQualityMode = 0;


    private boolean videoThreadOn = false;
    private boolean videoThreadOn2 = false;
    private boolean videoThreadOn3 = false;
    private boolean audioThreadOn = false;

    private MusicDialog mMusicDialog = null;

    private String mAuthString = "?auth_key=%1$d-%2$d-%3$d-%4$s";
    private String mMd5String = "%1$s-%2$d-%3$d-%4$d-%5$s";
    private String mTempUrl = null;
    private String mAuthTime = "";
    private String mPrivacyKey = "";

    Vector<Integer> mDynamicals = new Vector<>();

    //直播中更多操作弹窗
    private PopupWindowManage mMoreWindowManage;
    private View mMorePopupView;
    private View mGiftPoupView;
    private LiveMeiYanDialog mLiveMeiYanDialog;


    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);
    private IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
    private String mLiveId;

    //观众弹窗
    private LiveUserDialog mLiveUserDialog;
    private String mMemberId;
    //分享弹窗
    private LiveShareDialog mLiveShareDialog;
    private LiveRoom mLiveRoom;
    private boolean mIsStartLive;

    //主播信息弹窗
    private LiveUserInfoDialog mLiveUserInfoDialog;
    private LiveUser mLiveUser;
    //商品弹窗
    private LiveProductDialog mLiveProductDialog;
    private boolean mIsStop;
    private LiveSendMessageDialog mLiveSendMessageDialog;
    private LiveChatMessageAdapter mLiveChatMessageAdapter;
    private LiveHorizontalUserAdapter mLiveHorizontalUserAdapter = new LiveHorizontalUserAdapter();

    private int mLiveUserTotal;

    private boolean mIsRefreshLayout = false;

    private RelativeLayout.LayoutParams mMessageLayoutParams;

    private Handler mRongYunHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case ChatroomKit.MESSAGE_ARRIVED:
                case ChatroomKit.MESSAGE_SENT:
                    io.rong.imlib.model.Message message = (io.rong.imlib.model.Message) msg.obj;
                    if (!message.getTargetId().equals(mLiveId)) {
                        return false;
                    }
                    String objectName = message.getObjectName();
                    switch (objectName) {
                        case "RC:TxtMsg":
//                                文本消息
                            getTextMessage(message, false);
                            break;
                        case "RC:CmdNtf":

                            //刷新观众列表
                            mUserCurrentPage = 1;
                            getLiveAudienceUserList();

                            CommandNotificationMessage cmdMsg = (CommandNotificationMessage) message.getContent();
                            if ("LivingRoomHotDataMsg".equals(cmdMsg.getName())) {
                                LivingRoomHotDataMsg livingRoomHotDataMsg = null;
                                try {
                                    livingRoomHotDataMsg = new Gson().fromJson(cmdMsg.getData(), LivingRoomHotDataMsg.class);
                                    LiveMessage liveMessage = new LiveMessage();
                                    liveMessage.memberId = message.getSenderUserId();
                                    liveMessage.name = livingRoomHotDataMsg.systemChatMsg.nickname;
                                    liveMessage.isWelcome = true;
                                    liveMessage.content = livingRoomHotDataMsg.systemChatMsg.content;
                                    mLiveChatMessageAdapter.addData(liveMessage);
                                    mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);
                                    // 人气
                                    mTvPopularity.setText(String.format("人气：%s", livingRoomHotDataMsg.popularIndex));
                                } catch (Exception e) {
                                    Log.e("Seven", "error! message: " + cmdMsg.getData() + ";e: " + e.getMessage());
                                }
                            }
                            break;
                        case "CCM:Chatroom:Text:Message":
                            ChatroomTextMessage contentText = (ChatroomTextMessage) message.getContent();
                            boolean isLoginUser = message.getSenderUserId().equals(mLoginUser.id);
                            LiveMessage liveMessageText = new LiveMessage();
                            liveMessageText.memberId = message.getSenderUserId();
                            liveMessageText.name = contentText.getNickName();
                            liveMessageText.headImage = contentText.getHeadImage();
                            liveMessageText.showAvatar = isLoginUser;
                            liveMessageText.content = contentText.getContent();
                            mLiveChatMessageAdapter.addData(liveMessageText);
                            mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);

                            break;
                        case "CCM:Chatroom:Red:Envelope":
                            //红包
                            break;
                        case "CCM:Chatroom:Welcome":
                            //进入房间
                            ChatroomWelcomeMessage content = (ChatroomWelcomeMessage) message.getContent();
                            mLiveUserTotal = content.getAudienceNum();
//                            if (mLiveUserTotal <= 5) {
//                                getLiveAudienceList();
//                        } else {
//                            mTvLiveUser.setText(String.format("观众\n%s", mLiveUserTotal));
//                        }

                            //刷新观众列表
                            mUserCurrentPage = 1;
                            getLiveAudienceUserList();

                            LiveMessage liveMessage = new LiveMessage();
                            liveMessage.memberId = message.getSenderUserId();
                            liveMessage.name = content.getNickName();
                            liveMessage.headImage = content.getHeadImage();
                            liveMessage.isWelcome = true;
                            liveMessage.content = String.format("等%s位进入房间", mLiveUserTotal);
                            mLiveChatMessageAdapter.addData(liveMessage);
                            mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);

                            break;

                        case "CCM:Chatroom:Goods:Explain":
                            getLiveSku();
                            break;
                        default:

                    }
                    break;

                case ChatroomKit.MESSAGE_SEND_ERROR:
                    break;

                default:
            }
            return false;
        }
    });
    private HasPasswordModel mHasPasswordModel;
    private User mLoginUser;
    private LiveSendRedDialog mLiveRedListDialog;
    private boolean mIsNeetReconnect;
    private boolean mIsPause;
    private int mUserCurrentPage;
    private String mSkuId;


    public static LivePushFragment newInstance(String url, boolean async, boolean mAudio, boolean mVideoOnly,
                                               int cameraId, boolean isFlash, int mode, String authTime,
                                               String privacyKey, boolean mixExtern, boolean mixMain, String memberId) {
        LivePushFragment livePushFragment = new LivePushFragment();
        Bundle bundle = new Bundle();
        bundle.putString(LIVE_ID, url);
        bundle.putBoolean(ASYNC_KEY, async);
        bundle.putBoolean(AUDIO_ONLY_KEY, mAudio);
        bundle.putBoolean(VIDEO_ONLY_KEY, mVideoOnly);
        bundle.putInt(QUALITY_MODE_KEY, mode);
        bundle.putInt(CAMERA_ID, cameraId);
        bundle.putBoolean(FLASH_ON, isFlash);
        bundle.putString(AUTH_TIME, authTime);
        bundle.putString(PRIVACY_KEY, privacyKey);
        bundle.putBoolean(MIX_EXTERN, mixExtern);
        bundle.putBoolean(MIX_MAIN, mixMain);
        bundle.putString("memberId", memberId);
        livePushFragment.setArguments(bundle);
        return livePushFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mLiveId = getArguments().getString(LIVE_ID);
            mTempUrl = mPushUrl;
            mAsync = getArguments().getBoolean(ASYNC_KEY, false);
            mAudio = getArguments().getBoolean(AUDIO_ONLY_KEY, false);
            mVideoOnly = getArguments().getBoolean(VIDEO_ONLY_KEY, false);
            mCameraId = getArguments().getInt(CAMERA_ID);
            isFlash = getArguments().getBoolean(FLASH_ON, false);
            mMixExtern = getArguments().getBoolean(MIX_EXTERN, false);
            mMixMain = getArguments().getBoolean(MIX_MAIN, false);
            mQualityMode = getArguments().getInt(QUALITY_MODE_KEY);
            mAuthTime = getArguments().getString(AUTH_TIME);
            mPrivacyKey = getArguments().getString(PRIVACY_KEY);
            mMemberId = getArguments().getString("memberId");

            flashState = isFlash;
        }
        if (mAlivcLivePusher != null) {
            mAlivcLivePusher.setLivePushInfoListener(mPushInfoListener);
            mAlivcLivePusher.setLivePushErrorListener(mPushErrorListener);
            mAlivcLivePusher.setLivePushNetworkListener(mPushNetworkListener);
            mAlivcLivePusher.setLivePushBGMListener(mPushBGMListener);
            isPushing = mAlivcLivePusher.isPushing();
        }

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.push_fragment, container, false);
        ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        EventBus.getDefault().register(this);

        initView();
        initData();
    }

    private void initData() {
        mLoginUser = SessionUtil.getInstance().getLoginUser();
        // getLiveAudienceList();
        getLiveUserInfo(false);
    }


    private void initRong() {
        ChatroomKit.addEventHandler(mRongYunHandler);
        DataInterface.setBanStatus(false);
        LiveManager.joinChatRoom(getActivity(), mLiveId, mLoginUser.nickname, mLoginUser.avatar, 0, false);
    }

    private void getLiveSku() {
        APIManager.startRequest(mILiveService.getLiveSku(mLiveId),
                new BaseRequestListener<SkuInfo>() {
                    @Override
                    public void onSuccess(SkuInfo result) {
                        super.onSuccess(result);
                        if (TextUtils.isEmpty(result.skuId)) {
                            llGoods.setVisibility(View.GONE);
                            if (mLiveProductDialog != null) mLiveProductDialog.clearSelectIndex();
                            return;
                        }
                        mSkuId = result.skuId;
                        llGoods.setVisibility(View.VISIBLE);
                        FrescoUtil.setImageSmall(ivGoods, result.thumb);
                        tvGoodsName.setText(result.name);
                        tvPrice.setText(MoneyUtil.centToYuan¥Str(result.retailPrice));
                    }

                    @Override
                    public void onError(Throwable e) {
                    }
                }, getContext());
    }

    @OnClick({R.id.ivCloseGoods, R.id.llGoods})
    public void onGoodsViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivCloseGoods:
                deleteLiveSkuId();
                break;
            case R.id.llGoods:
                EventBus.getDefault().post(new EventMessage(Event.openNewActivity));
                EventUtil.viewProductDetail(getContext(), mSkuId, false, "", mLiveId);
                break;
        }
    }

    private void deleteLiveSkuId() {
        APIManager.startRequest(mILiveService.deleteLiveSkuId(mLiveId),
                new BaseRequestListener<Object>(getActivity()) {
                    @Override
                    public void onSuccess(Object result) {
                        super.onSuccess(result);
                        ChatroomGoodsMessage message = new ChatroomGoodsMessage();
                        ChatroomKit.sendMessage(message, true);
                    }

                }, getContext());
    }

    /**
     * 获取直播详情
     */
    private void getLiveDetail() {
        APIManager.startRequest(mILiveService.getLive(mLiveId),
                new BaseRequestListener<LiveRoom>() {
                    @Override
                    public void onSuccess(LiveRoom result) {
                        mLiveRoom = result;
                    }
                }, getActivity());
    }

    private void getLiveUserInfo(boolean isShowDialog) {

        APIManager.startRequest(mILiveService.getLiveUserInfo(mMemberId),
                new BaseRequestListener<LiveUser>() {
                    @Override
                    public void onSuccess(LiveUser result) {
                        mLiveUser = result;
                        SessionUtil.getInstance().setLiveUser(result);
                        FrescoUtil.setImage(mIvAvatar, result.headImage);
                        tvName.setText(result.nickName);
//                        mTvPopularity.setText(String.format("人气：%s", result.popularity));

                        if (isShowDialog) {
                            mLiveUserInfoDialog.show();
                            mLiveUserInfoDialog.setLiveUser(result);
                        }
                    }
                }, getActivity());
    }

    private void initView() {
        mMoreWindowManage = PopupWindowManage.getInstance(getActivity());
        mMoreWindowManage.setBackgroundDrawable(R.drawable.bg_live_more_gray_10);


        mMorePopupView = View.inflate(getActivity(), R.layout.view_popup_live_more, null);
        TextView tvCamera = mMorePopupView.findViewById(R.id.tvCamera);
        tvCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeCamera();
            }
        });
        TextView tvMeiYan = mMorePopupView.findViewById(R.id.tvMeiYan);
        tvMeiYan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMoreWindowManage.dismiss();
                meiyan();
            }
        });

        TextView tvBannedList = mMorePopupView.findViewById(R.id.tvBannedList);
        tvBannedList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LiveBannedListDialog liveBannedListDialog = new LiveBannedListDialog(getActivity(), mLiveId);
                liveBannedListDialog.show();
                mMoreWindowManage.dismiss();
            }
        });

        TextView password = mMorePopupView.findViewById(R.id.tvPassword);
        password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLiveRoom.hasPassword()) {
                    ClipboardManager cm = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData mClipData = ClipData.newPlainText("Label", mLiveRoom.livePasswordDecode);
                    cm.setPrimaryClip(mClipData);
                    ToastUtil.success("复制成功");
                } else {
                    ToastUtil.error("直播间未设置密码");
                }
                mMoreWindowManage.dismiss();
            }
        });


        //礼品
        mGiftPoupView = View.inflate(getActivity(), R.layout.view_popup_live_gift, null);
        TextView tvRed = mGiftPoupView.findViewById(R.id.tvRed);
        tvRed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRedDialog();
                mMoreWindowManage.dismiss();
            }
        });

        TextView tvCouppn = mGiftPoupView.findViewById(R.id.tvCoupon);
        tvCouppn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LiveSendCouponDialog liveSendCouponDialog = new LiveSendCouponDialog(getActivity(), mLiveId);
                liveSendCouponDialog.show();
                mMoreWindowManage.dismiss();
            }
        });


        mTvGaoQing.setSelected(true);

        mMessageRecyclerView.setVisibility(View.VISIBLE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        mMessageRecyclerView.setLayoutManager(linearLayoutManager);
        mLiveChatMessageAdapter = new LiveChatMessageAdapter();
        mMessageRecyclerView.setAdapter(mLiveChatMessageAdapter);

        mMessageRecyclerView.addOnItemTouchListener(new OnItemChildClickListener() {
            @Override
            public void onSimpleItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                LiveMessage item = mLiveChatMessageAdapter.getItem(position);
                switch (view.getId()) {
                    case R.id.tvContent:
                        if (null != item && !item.isNotice && !item.showAvatar) {
                            LiveBlockDialog liveBlockDialog = new LiveBlockDialog(getActivity(), mLiveId, item);
                            liveBlockDialog.show();
                        }
                        break;
                }
            }
        });

        mMessageLayoutParams = (RelativeLayout.LayoutParams) mMessageRecyclerView.getLayoutParams();

        SoftKeyBoardListener.setListener(getActivity(), new SoftKeyBoardListener.OnSoftKeyBoardChangeListener() {
            @Override
            public void keyBoardShow(int height) {
                if (null != mLiveRedListDialog) {
                    mLiveRedListDialog.keyBoardShow(height);
                }
                mMessageLayoutParams.bottomMargin = SizeUtils.dp2px(60) + height;
                mMessageRecyclerView.setLayoutParams(mMessageLayoutParams);
            }

            @Override
            public void keyBoardHide(int height) {
                if (null != mLiveRedListDialog) {
                    mLiveRedListDialog.keyBoardHide(height);
                }
                mMessageLayoutParams.bottomMargin = SizeUtils.dp2px(100);
                mMessageRecyclerView.setLayoutParams(mMessageLayoutParams);
            }
        });
        refreshLayout.setRefreshCallback(new RefreshCallBack() {
            @Override
            public void onLeftRefreshing() {
                mUserCurrentPage = 1;
                mIsRefreshLayout = true;
                getLiveAudienceUserList();
            }

            @Override
            public void onRightRefreshing() {
                mUserCurrentPage++;
                mIsRefreshLayout = true;
                getLiveAudienceUserList();
            }
        });
        refreshLayout.setRefreshHeader(new LoadingRefreshHeader(getActivity()), HorizontalRefreshLayout.LEFT);
        refreshLayout.setRefreshHeader(new LoadingRefreshHeader(getActivity()), HorizontalRefreshLayout.RIGHT);

        LinearLayoutManager linearLayoutHorizontalManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mRecycler.setLayoutManager(linearLayoutHorizontalManager);
        mRecycler.setAdapter(mLiveHorizontalUserAdapter);


    }

    private void getLiveAudienceUserList() {
        APIManager.startRequest(mILiveService.getLiveAudienceList(mLiveId, mUserCurrentPage, 5),
                new BaseRequestListener<PaginationEntity<LiveUser, Object>>() {
                    @Override
                    public void onSuccess(PaginationEntity<LiveUser, Object> result) {
                        if (null != refreshLayout && mIsRefreshLayout) {
                            mIsRefreshLayout = false;
                            refreshLayout.onRefreshComplete();
                        }
                        if (mUserCurrentPage == 1) {
                            mLiveHorizontalUserAdapter.setNewData(result.list);
                        } else {
                            mLiveHorizontalUserAdapter.addData(result.list);
                            mRecycler.scrollToPosition(mLiveHorizontalUserAdapter.getData().size() - 1);
                        }
                        if (result.page >= result.totalPage) {
                            mLiveHorizontalUserAdapter.loadMoreEnd();
                        } else {
                            mLiveHorizontalUserAdapter.loadMoreComplete();
                        }

                    }
                }, getActivity());
    }

    private void getTextMessage(io.rong.imlib.model.Message message, boolean showAvatar) {
        APIManager.startRequest(mUserService.getMemberInfoById(message.getSenderUserId()), new BaseRequestListener<User>() {
            @Override
            public void onSuccess(User result) {
                boolean isLoginUser = result.id.equals(mLoginUser.id);
                LiveMessage liveMessage = new LiveMessage();
                liveMessage.memberId = message.getSenderUserId();
                liveMessage.name = result.nickname;
                liveMessage.headImage = result.avatar;
                liveMessage.showAvatar = showAvatar || isLoginUser;
                liveMessage.content = showAvatar && !isLoginUser ? String.format("%s 进入房间", liveMessage.name) : ((TextMessage) (message.getContent())).getContent();
                mLiveChatMessageAdapter.addData(liveMessage);
                mMessageRecyclerView.scrollToPosition(mLiveChatMessageAdapter.getData().size() - 1);
            }
        }, getActivity());
    }

    /**
     * 转换镜头
     */
    private void changeCamera() {
        if (mCameraId == CAMERA_TYPE_FRONT.getCameraId()) {
            mCameraId = CAMERA_TYPE_BACK.getCameraId();
        } else {
            mCameraId = CAMERA_TYPE_FRONT.getCameraId();
        }
        mAlivcLivePusher.switchCamera();
    }


    public void setAlivcLivePusher(AlivcLivePusher alivcLivePusher) {
        this.mAlivcLivePusher = alivcLivePusher;
    }

    public void setStateListener(LivePushActivity.PauseState listener) {
        this.mStateListener = listener;
    }

    public void setSurfaceView(SurfaceView surfaceView) {
        this.mSurfaceView = surfaceView;
    }


    AlivcLivePushInfoListener mPushInfoListener = new AlivcLivePushInfoListener() {
        @Override
        public void onPreviewStarted(AlivcLivePusher pusher) {
//            showToast(getString(R.string.start_preview));
        }

        @Override
        public void onPreviewStoped(AlivcLivePusher pusher) {
//            showToast(getString(R.string.stop_preview));
        }

        @Override
        public void onPushStarted(AlivcLivePusher pusher) {
//            showToast(getString(R.string.start_push));
        }

        @Override
        public void onFirstAVFramePushed(AlivcLivePusher pusher) {
        }

        @Override
        public void onPushPauesed(AlivcLivePusher pusher) {
//            showToast(getString(R.string.pause_push));
        }

        @Override
        public void onPushResumed(AlivcLivePusher pusher) {
//            showToast(getString(R.string.resume_push));
        }

        @Override
        public void onPushStoped(AlivcLivePusher pusher) {
//            showToast(getString(R.string.stop_push));
        }

        /**
         * 推流重启通知
         *
         * @param pusher AlivcLivePusher实例
         */
        @Override
        public void onPushRestarted(AlivcLivePusher pusher) {
//            showToast(getString(R.string.restart_success));
        }

        @Override
        public void onFirstFramePreviewed(AlivcLivePusher pusher) {
//            showToast(getString(R.string.first_frame));
        }

        @Override
        public void onDropFrame(AlivcLivePusher pusher, int countBef, int countAft) {
//            showToast(getString(R.string.drop_frame) + ", 丢帧前：" + countBef + ", 丢帧后：" + countAft);
        }

        @Override
        public void onAdjustBitRate(AlivcLivePusher pusher, int curBr, int targetBr) {
//            showToast(getString(R.string.adjust_bitrate) + ", 当前码率：" + curBr + "Kps, 目标码率：" + targetBr + "Kps");
        }

        @Override
        public void onAdjustFps(AlivcLivePusher pusher, int curFps, int targetFps) {
//            showToast(getString(R.string.adjust_fps) + ", 当前帧率：" + curFps + ", 目标帧率：" + targetFps);
        }
    };

    AlivcLivePushErrorListener mPushErrorListener = new AlivcLivePushErrorListener() {

        @Override
        public void onSystemError(AlivcLivePusher livePusher, AlivcLivePushError error) {
            showDialog(getString(R.string.system_error) + error.toString());
        }

        @Override
        public void onSDKError(AlivcLivePusher livePusher, AlivcLivePushError error) {
            if (error != null) {
                showDialog(getString(R.string.sdk_error) + error.toString());
            }
        }
    };

    AlivcLivePushNetworkListener mPushNetworkListener = new AlivcLivePushNetworkListener() {
        @Override
        public void onNetworkPoor(AlivcLivePusher pusher) {
            //  showNetWorkDialog(getString(R.string.network_poor));
        }

        @Override
        public void onNetworkRecovery(AlivcLivePusher pusher) {
            //    showToast(getString(R.string.network_recovery));
        }

        @Override
        public void onReconnectStart(AlivcLivePusher pusher) {

            //   showToastShort(getString(R.string.reconnect_start));
        }

        @Override
        public void onReconnectFail(AlivcLivePusher pusher) {

            //   showDialog(getString(R.string.reconnect_fail));
        }

        @Override
        public void onReconnectSucceed(AlivcLivePusher pusher) {
            //   showToast(getString(R.string.reconnect_success));
        }

        @Override
        public void onSendDataTimeout(AlivcLivePusher pusher) {
            //    showDialog(getString(R.string.senddata_timeout));
        }

        @Override
        public void onConnectFail(AlivcLivePusher pusher) {
            //   showDialog(getString(R.string.connect_fail));
        }

        @Override
        public void onConnectionLost(AlivcLivePusher pusher) {
            //   showToast("推流已断开");
        }

        @Override
        public String onPushURLAuthenticationOverdue(AlivcLivePusher pusher) {
            showDialog("流即将过期，请更换url");
            refreshPushUrl();
            return getAuthString(mAuthTime);
        }

        @Override
        public void onSendMessage(AlivcLivePusher pusher) {
            showToast(getString(R.string.send_message));
        }

        @Override
        public void onPacketsLost(AlivcLivePusher pusher) {
            //  showToast("推流丢包通知");
        }
    };

    private AlivcLivePushBGMListener mPushBGMListener = new AlivcLivePushBGMListener() {
        @Override
        public void onStarted() {

        }

        @Override
        public void onStoped() {

        }

        @Override
        public void onPaused() {

        }

        @Override
        public void onResumed() {

        }

        @Override
        public void onProgress(final long progress, final long duration) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mMusicDialog != null) {
                        mMusicDialog.updateProgress(progress, duration);
                    }
                }
            });
        }

        @Override
        public void onCompleted() {

        }

        @Override
        public void onDownloadTimeout() {

        }

        @Override
        public void onOpenFailed() {
            showDialog(getString(R.string.bgm_open_failed));
        }
    };

    @Override
    public void onDestroy() {
        ChatroomKit.quitChatRoom(new RongIMClient.OperationCallback() {
            @Override
            public void onSuccess() {
                ChatroomKit.removeEventHandler(mHandler);
                if (DataInterface.isLoginStatus()) {
                    Toast.makeText(getActivity(), "退出聊天室成功", Toast.LENGTH_SHORT).show();

                    ChatroomUserQuit userQuit = new ChatroomUserQuit();
                    userQuit.setId(ChatroomKit.getCurrentUser().getUserId());
                    ChatroomKit.sendMessage(userQuit, false);

                }
            }

            @Override
            public void onError(RongIMClient.ErrorCode errorCode) {
                ChatroomKit.removeEventHandler(mHandler);

            }
        });
        super.onDestroy();
    }

    private void showToast(final String text) {
        if (getActivity() == null || text == null) {
            return;
        }
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    Toast toast = Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });
    }

    private void showToastShort(final String text) {
        if (getActivity() == null || text == null) {
            return;
        }
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    Toast toast = Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
            }
        });
    }

    private void showDialog(final String message) {
        if (getActivity() == null || message == null) {
            return;
        }
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setTitle(getString(R.string.dialog_title));
                    dialog.setMessage(message);
                    dialog.setNegativeButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    dialog.show();
                }
            }
        });
    }

    private void showNetWorkDialog(final String message) {
        if (getActivity() == null || message == null) {
            return;
        }
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null) {
                    final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setTitle(getString(R.string.dialog_title));
                    dialog.setMessage(message);
                    dialog.setNegativeButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                        }
                    });
                    dialog.setNeutralButton(getString(R.string.reconnect), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            try {
                                mAlivcLivePusher.reconnectPushAsync(null);
                            } catch (IllegalStateException e) {

                            }
                        }
                    });
                    dialog.show();
                }
            }
        });
    }

    @Override
    public void run() {
        if (mAlivcLivePusher != null) {
            try {
                isPushing = mAlivcLivePusher.isNetworkPushing();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
            AlivcLivePushError error = mAlivcLivePusher.getLastError();

        }
        mHandler.postDelayed(this, REFRESH_INTERVAL);

    }

    @Override
    public void onResume() {
        super.onResume();
        mHandler.post(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mHandler.removeCallbacks(this);
    }

    private String getMD5(String string) {

        byte[] hash;

        try {
            hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }

        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) {
                hex.append("0");
            }
            hex.append(Integer.toHexString(b & 0xFF));
        }

        return hex.toString();
    }

    private String getUri(String url) {
        String result = "";
        String temp = url.substring(7);
        if (temp != null && !temp.isEmpty()) {
            result = temp.substring(temp.indexOf("/"));
        }
        return result;
    }


    private String getAuthString(String time) {
        if (!time.isEmpty() && !mPrivacyKey.isEmpty()) {
            long tempTime = (System.currentTimeMillis() + Integer.valueOf(time)) / 1000;
            String tempprivacyKey = String.format(mMd5String, getUri(mPushUrl), tempTime, 0, 0, mPrivacyKey);
            String auth = String.format(mAuthString, tempTime, 0, 0, getMD5(tempprivacyKey));
            mTempUrl = mPushUrl + auth;
        } else {
            mTempUrl = mPushUrl;
        }
        return mTempUrl;
    }

    private void startPCM(final Context context) {
        new ScheduledThreadPoolExecutor(1, new ThreadFactory() {
            private AtomicInteger atoInteger = new AtomicInteger(0);

            @Override
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setName("LivePushActivity-readPCM-Thread" + atoInteger.getAndIncrement());
                return t;
            }
        }).execute(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                audioThreadOn = true;
                byte[] pcm;
                int allSended = 0;
                int sizePerSecond = 44100 * 2;
                InputStream myInput = null;
                OutputStream myOutput = null;
                boolean reUse = false;
                long startPts = System.nanoTime() / 1000;
                try {
                    File f = new File("/sdcard/ccmall_alivc_resource/441.pcm");
                    myInput = new FileInputStream(f);
                    byte[] buffer = new byte[2048];
                    int length = myInput.read(buffer, 0, 2048);
                    while (length > 0 && audioThreadOn) {
                        long pts = System.nanoTime() / 1000;
                        mAlivcLivePusher.inputStreamAudioData(buffer, length, 44100, 1, pts);
                        allSended += length;
                        if ((allSended * 1000000L / sizePerSecond - 50000) > (pts - startPts)) {
                            try {
                                Thread.sleep(45);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        length = myInput.read(buffer);
                        if (length < 2048) {
                            myInput.close();
                            myInput = new FileInputStream(f);
                            length = myInput.read(buffer);
                        }
                        try {
                            Thread.sleep(3);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    myInput.close();
                    audioThreadOn = false;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void stopPcm() {
        audioThreadOn = false;
    }


    @OnClick(R.id.ivMore)
    protected void more() {
        mMoreWindowManage.showWindow(mViewMoreLine, mMorePopupView);
    }

    @OnClick(R.id.tvStartCamera)
    protected void camera() {
        changeCamera();
    }

    @OnClick(R.id.ivClose)
    protected void close() {
        getActivity().finish();
    }

    @OnClick(R.id.ivStartLive)
    protected void startLive() {
        APIManager.startRequest(mUserService.hasPassowrd(), new BaseRequestListener<HasPasswordModel>(getActivity()) {
            @Override
            public void onSuccess(HasPasswordModel model) {
                super.onSuccess(model);
                mHasPasswordModel = model;
                if (model.status) {
                    initRong();
                    getPushUrl();
                } else {
                    showSetPassword();
                }
            }
        }, getActivity());

    }

    private void showSetPassword() {
        WJDialog wjDialog = new WJDialog(getActivity());
        wjDialog.show();
        wjDialog.setContentText("您未设置登录密码，如果直播过程需要发红包，请先去设置密码");
        wjDialog.setCancelText("开始直播");
        wjDialog.setConfirmText("设置密码");

        wjDialog.setOnCancelListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wjDialog.dismiss();
                initRong();
                getPushUrl();
            }
        });

        wjDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                wjDialog.dismiss();
                startActivity(new Intent(getActivity(), SetPasswordActivity.class));
            }
        });
    }

    @OnClick({R.id.tvLiuChang, R.id.tvGaoQing, R.id.tvChaoQing})
    protected void qingxi(View view) {
        switch (view.getId()) {
            case R.id.tvLiuChang:
                mTvQingXi.setText("流畅");
                mTvLiuChang.setSelected(true);
                mTvGaoQing.setSelected(false);
                mTvChaoQing.setSelected(false);
                break;
            case R.id.tvGaoQing:
                mTvQingXi.setText("高清");
                mTvLiuChang.setSelected(false);
                mTvGaoQing.setSelected(true);
                mTvChaoQing.setSelected(false);
                break;
            case R.id.tvChaoQing:
                mTvQingXi.setText("超清");
                mTvLiuChang.setSelected(false);
                mTvGaoQing.setSelected(false);
                mTvChaoQing.setSelected(true);
                break;
            default:
        }
    }

    @OnClick(R.id.ivMeiYan)
    protected void meiyan() {
        if (null == mLiveMeiYanDialog) {
            mLiveMeiYanDialog = new LiveMeiYanDialog(Objects.requireNonNull(getActivity()));
            mLiveMeiYanDialog.setAlivcLivePusher(mAlivcLivePusher);
            mLiveMeiYanDialog.setOnDismissListener(onDismissListener);
        }
        mLayoutStartLive.setVisibility(View.GONE);
        mLayoutBottom.setVisibility(View.GONE);
        mLiveMeiYanDialog.show();
    }

    @OnClick(R.id.ivShare)
    protected void share() {
        if (UiUtils.checkUserLogin(getActivity()) && null != mLiveRoom) {
            mLiveShareDialog = null;
            if (null == mLiveShareDialog) {
                User loginUser = SessionUtil.getInstance().getLoginUser();
                mLiveShareDialog = new LiveShareDialog(Objects.requireNonNull(getActivity()),
                        mLiveRoom.title, mLiveRoom.title, mLiveRoom.liveImage, mLiveId, loginUser.invitationCode, mLiveRoom.nickName);
            }
            mLiveShareDialog.show();
        }
    }

    @OnClick({R.id.tvLiveUser, R.id.ivUser1, R.id.ivUser2, R.id.ivUser3})
    protected void showLiveUser() {
        LiveUserDialog mLiveUserDialog = new LiveUserDialog(Objects.requireNonNull(getActivity()), mLiveId);
        mLiveUserDialog.show();
    }

    @OnClick(R.id.ivCloseLive)
    protected void closeLive() {
        WJDialog finishBroadcastDialog = new WJDialog(getContext());
        finishBroadcastDialog.show();
        finishBroadcastDialog.setContentText("您确定结束直播吗？");
        finishBroadcastDialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopPush(finishBroadcastDialog);
            }
        });
    }

    @OnClick({R.id.layoutUserInfo, R.id.ivAvatar})
    protected void userInfo() {
        if (null == mLiveUserInfoDialog) {
            mLiveUserInfoDialog = new LiveUserInfoDialog(Objects.requireNonNull(getActivity()), null);
        }
        getLiveUserInfo(true);
    }

    @OnClick({R.id.ivProduct, R.id.tvReplaceProducts})
    protected void proudct() {
        if (null == mLiveProductDialog) {
            mLiveProductDialog = new LiveProductDialog(Objects.requireNonNull(getActivity()), mLiveId, false, mLiveUser.shopkeeper);
        }
        mLiveProductDialog.show();

    }

    @OnClick(R.id.ivGift)
    protected void gift() {
        mMoreWindowManage.showWindow(mViewMoreLine, mGiftPoupView);
    }

    private void showRedDialog() {
        if (mHasPasswordModel.status) {
            mLiveRedListDialog = new LiveSendRedDialog(Objects.requireNonNull(getActivity()), mLiveId, mSendRedListener);
            mLiveRedListDialog.show();
        } else {
            ToastUtil.error("未设置密码，无法发红包");
        }
    }


    private LiveSendRedDialog.SendRedListener mSendRedListener = new LiveSendRedDialog.SendRedListener() {
        @Override
        public void sendRedSuccess(LiveRedEnvelopes liveRedEnvelopes) {
            final ChatroomRedMessage chatroomRedMessage = new ChatroomRedMessage();
            chatroomRedMessage.setRedId(liveRedEnvelopes.redEnvelopesId);
            ChatroomKit.sendMessage(chatroomRedMessage, true);
        }
    };

    @OnClick(R.id.ivRedList)
    protected void showRedListDialog() {
        LiveRedListDialog liveRedListDialog = new LiveRedListDialog(Objects.requireNonNull(getActivity()), mLiveId, LiveRedListDialog.PUSH, mLiveUser);
        liveRedListDialog.show();
    }

    @OnClick(R.id.ivCouponList)
    protected void showCouponList() {
        LiveCouponListDialog liveCouponListDialog = new LiveCouponListDialog(getActivity(), mLiveId, mLiveUser, LiveCouponListDialog.COUPON_PUSH);
        liveCouponListDialog.show();
    }

    @OnClick(R.id.layoutMessage)
    protected void sendMessage() {
        if (null == mLiveSendMessageDialog) {
            User loginUser = SessionUtil.getInstance().getLoginUser();
            mLiveSendMessageDialog = new LiveSendMessageDialog(Objects.requireNonNull(getActivity()));
            mLiveSendMessageDialog.setSubmitListener(new LiveSendMessageDialog.OnSubmitListener() {
                @Override
                public void submit(String content) {
                    mLiveSendMessageDialog.dismiss();
                    ChatroomTextMessage chatroomTextMessage = new ChatroomTextMessage();
                    chatroomTextMessage.setContent(content);
                    chatroomTextMessage.setNickName(loginUser.nickname);
                    chatroomTextMessage.setHeadImage(loginUser.avatar);
                    ChatroomKit.sendMessage(chatroomTextMessage, true);
                }
            });
        }
        mLiveSendMessageDialog.show("发送", "");
    }

    private DialogInterface.OnDismissListener onDismissListener = new DialogInterface.OnDismissListener() {
        @Override
        public void onDismiss(DialogInterface dialog) {
            mLayoutStartLive.setVisibility(View.VISIBLE);
            mLayoutBottom.setVisibility(View.VISIBLE);
        }
    };


    private void getPushUrl() {
        ToastUtil.showLoading(getActivity());
        APIManager.startRequest(mILiveService.startLive(mLiveId), new BaseRequestListener<AddLive>() {
            @Override
            public void onSuccess(AddLive result) {
                ToastUtil.hideLoading();
                SharedPreferenceUtils.setStartLiveId(Objects.requireNonNull(getContext()), mLiveId);
                getLiveDetail();
                mIsStartLive = true;
                mLayoutStart.setVisibility(View.GONE);
                mLayoutLiveing.setVisibility(View.VISIBLE);
                mPushUrl = result.pushUrl;
                mTempUrl = mPushUrl;

                if (mAsync) {
                    mAlivcLivePusher.startPushAysnc(getAuthString(mAuthTime));
                } else {
                    mAlivcLivePusher.startPush(getAuthString(mAuthTime));
                }
                if (mMixMain) {
                    startPCM(getActivity());
                }

                startOnLineUpdateState();
                EventBus.getDefault().post(new EventMessage(Event.startLiveSuccess));
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.hideLoading();
            }
        }, getActivity());
    }

    public void refreshPushUrl() {
        if (!mIsStartLive) return;
        APIManager.startRequest(mILiveService.refreshPushUrl(mLiveId), new BaseRequestListener<AddLive>() {
            @Override
            public void onSuccess(AddLive result) {
                mPushUrl = result.pushUrl;
                mTempUrl = mPushUrl;
                mAlivcLivePusher.startPush(mPushUrl);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
            }
        }, getActivity());
    }

    public void stopPush(WJDialog wjDialog) {

        if (!mIsStartLive) {
            wjDialog.dismiss();
            getActivity().finish();
            return;
        }

        mIsStop = true;
        ToastUtil.showLoading(getActivity());
        APIManager.startRequest(mILiveService.stopLive(mLiveId), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                stopLive(wjDialog);
            }

            @Override
            public void onError(Throwable e) {
//                super.onError(e);
                stopLive(wjDialog);
            }
        }, getActivity());
    }


    private void stopLive(WJDialog wjDialog) {
        ToastUtil.hideLoading();
        SharedPreferenceUtils.setStartLiveId(Objects.requireNonNull(getContext()), "");
//        MyApplication.getInstance().mAlivcLivePusher = null;
        if (null != mAlivcLivePusher && mAlivcLivePusher.isPushing()) {
            mAlivcLivePusher.stopPush();
        }
        wjDialog.dismiss();
        LiveManager.toLiveEnd(getActivity(), mLiveId, mLiveUser.headImage, mLiveUser.nickName, false, 0);
        getActivity().finish();

        final ChatroomEndMessage chatroomEndMessage = new ChatroomEndMessage();
        chatroomEndMessage.setVersionName(AppUtils.getAppVersionName(getActivity()));
        ChatroomKit.sendMessage(chatroomEndMessage, false);
    }


    /**
     * 获取观众
     */
    private void getLiveAudienceList() {
        mIvUserOne.setVisibility(View.GONE);
        mIvUserTwo.setVisibility(View.GONE);
        mIvUserThree.setVisibility(View.GONE);
        mIvUserFour.setVisibility(View.GONE);
        mTvLiveUser.setVisibility(View.GONE);
        APIManager.startRequest(mILiveService.getLiveAudienceList(mLiveId, 1, 5), new BaseRequestListener<PaginationEntity<LiveUser, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<LiveUser, Object> result) {
                if (result.list.size() == 0) {
                    return;
                }
                switch (result.list.size()) {
                    case 1:
                        mIvUserOne.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserOne, result.list.get(0).headImage);
                        break;
                    case 2:
                        mIvUserOne.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserOne, result.list.get(0).headImage);
                        mIvUserTwo.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserTwo, result.list.get(1).headImage);
                        break;
                    case 4:
                        mIvUserOne.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserOne, result.list.get(0).headImage);
                        mIvUserTwo.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserTwo, result.list.get(1).headImage);
                        mIvUserThree.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserThree, result.list.get(2).headImage);
                        mIvUserFour.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserFour, result.list.get(3).headImage);
                        break;
                    default:
                        mIvUserOne.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserOne, result.list.get(0).headImage);
                        mIvUserTwo.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserTwo, result.list.get(1).headImage);
                        mIvUserThree.setVisibility(View.VISIBLE);
                        FrescoUtil.setImage(mIvUserThree, result.list.get(2).headImage);

                }
                mTvLiveUser.setText(String.format("观众\n%s", result.total));
                mTvLiveUser.setVisibility(result.total > 4 ? View.VISIBLE : View.GONE);
            }
        }, getActivity());
    }


//    private void stopLive() {
//        Context context = getContext();
//        if (null != context) {
//            SharedPreferenceUtils.setStartLiveId(context, "");
//            if (null != mAlivcLivePusher) {
//                mAlivcLivePusher.stopPush();
//            }
//        }
//
//        final ChatroomEndMessage chatroomEndMessage = new ChatroomEndMessage();
//        ChatroomKit.sendMessage(chatroomEndMessage, false);
//
//        stopOnLineUpdateState();
//    }


    @Override
    public void onDestroyView() {
//        if (!mIsStop) {
//            APIManager.startRequest(mILiveService.stopLive(mLiveId), new BaseRequestListener<Object>() {
//                @Override
//                public void onSuccess(Object result) {
//                    stopLive();
//                }
//
//                @Override
//                public void onError(Throwable e) {
////                    super.onError(e);
//                    stopLive();
//                }
//            }, getActivity());
//
//        }
        super.onDestroyView();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        // 避免内存泄漏
        stopOnLineUpdateState();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case liveUserRefresh:
//                getLiveAudienceList();
                break;
            case addLiveProduct:
                if (null != mLiveProductDialog) {
                    mLiveProductDialog.getLiveSkus(true);
                }
                break;
            case goToLoginRongIM:
                User loginUser = SessionUtil.getInstance().getLoginUser();
                LiveManager.joinChatRoom(getActivity(), mLiveId, loginUser.nickname, loginUser.avatar, mLiveUserTotal, true);
                break;
            case networkConnected:
                if (mIsNeetReconnect) {
                    mIsNeetReconnect = false;
                    mAlivcLivePusher.reconnectPushAsync(null);
                }
                break;
            case networkDisconnected:
                mIsNeetReconnect = true;
                break;
            case sendCouponSuccess:
                mIvCouponList.setVisibility(View.VISIBLE);
                break;
            default:
        }
    }

    private CountDownTimer mCountDownTimer;

    private void startOnLineUpdateState() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
        }
        mCountDownTimer = new CountDownTimer(Long.MAX_VALUE, 30000) {
            @Override
            public void onTick(long millisUntilFinished) {
                APIManager.startRequest(mILiveService.sendOutLine(mPushUrl), new BaseRequestListener<Object>() {
                    @Override
                    public void onSuccess(Object result) {
                        // no -op
                        Log.d("Seven", "setOutLine");
                    }
                }, getContext());
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    private void stopOnLineUpdateState() {
        if (mCountDownTimer != null) {
            mCountDownTimer.cancel();
            mCountDownTimer = null;
        }
    }
}
