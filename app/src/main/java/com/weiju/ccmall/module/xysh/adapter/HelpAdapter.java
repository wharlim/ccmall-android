package com.weiju.ccmall.module.xysh.adapter;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.HelpCourse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenyanming
 * @time 2019/12/16 on 14:16
 * @desc
 */
public class HelpAdapter extends BaseQuickAdapter<String, BaseViewHolder> {
    public HelpAdapter() {
        super(R.layout.item_help);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.setText(R.id.tvTitle, item);
        TextView title = helper.getView(R.id.tvTitle);
        Drawable drawable = mContext.getResources().getDrawable(R.drawable.icon_bangzhuzhongxin);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        title.setCompoundDrawables(null, drawable, null, null);

        RecyclerView recyclerView = helper.getView(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(linearLayoutManager);
        HelpItemAdapter itemAdapter = new HelpItemAdapter();

        List<HelpCourse> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            HelpCourse helpCourse = new HelpCourse();
            helpCourse.title = "还款原理是什么？传统常见还款有dasdah电脑卡死的";
            helpCourse.showLine = i != 2;
            list.add(helpCourse);

        }
        itemAdapter.setNewData(list);
        recyclerView.setAdapter(itemAdapter);

    }
}
