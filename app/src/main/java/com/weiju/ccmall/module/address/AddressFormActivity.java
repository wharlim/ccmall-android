package com.weiju.ccmall.module.address;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Address;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.RegionSelectDialog;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.contracts.IRegion;
import com.weiju.ccmall.shared.contracts.OnSelectRegionLister;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IAddressService;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.address
 * @since 2017-06-10
 */
public class AddressFormActivity extends BaseActivity {

    private boolean isEditAddress = false;
    private IAddressService mAddressService;
    private Address mAddress = new Address();
    private ILiveStoreService mLiveStoreService;

    @BindView(R.id.contactsEt)
    protected EditText mContactsEt;
    @BindView(R.id.phoneEt)
    protected EditText mPhoneEt;
    @BindView(R.id.regionTv)
    protected TextView mRegionTv;
    @BindView(R.id.detailEt)
    protected EditText mDetailEt;
    @BindView(R.id.defaultSwitch)
    protected Switch mDefaultSwitch;
    private boolean mIsReturnGoodsAddress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_form_layout);
        ButterKnife.bind(this);
        mAddressService = ServiceManager.getInstance().createService(IAddressService.class);
        mLiveStoreService = ServiceManager.getInstance().createService(ILiveStoreService.class);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        String addressId = "";
        if (intent == null) {
            isEditAddress = false;
        } else if (intent.getExtras() == null) {
            isEditAddress = false;
        } else {
            String action = intent.getExtras().getString("action");
            isEditAddress = action != null && action.equals(Key.EDIT_ADDRESS);
            addressId = intent.getExtras().getString("addressId");
            mIsReturnGoodsAddress = getIntent().getBooleanExtra("isReturnGoodsAddress", false);
        }
        setTitle(isEditAddress ? mIsReturnGoodsAddress ? "编辑退货地址" : "编辑收货地址" : mIsReturnGoodsAddress ? "添加退货地址" : "添加收货地址");
        initHeaderButtons();
        if (isEditAddress && addressId != null && !addressId.isEmpty()) {
            getAddressInfo(addressId);
        }
    }

    private void initHeaderButtons() {
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_black);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (isEditAddress) {
            getHeaderLayout().setRightDrawable(R.mipmap.icon_address_trash);
            getHeaderLayout().setOnRightClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    WJDialog dialog = new WJDialog(AddressFormActivity.this);
                    dialog.show();
                    dialog.setCancelable(true);
                    dialog.setContentText("确定删除该收货地址？");
                    dialog.setOnConfirmListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            deleteAddress();
                        }
                    });
                }
            });
        }
    }

    private void deleteAddress() {
        Observable<RequestResult<Object>> observable = mAddressService.deleteAddress(mAddress.addressId);
        if (mIsReturnGoodsAddress) {
            observable = mLiveStoreService.delAddr(mAddress.addressId);
        }
        APIManager.startRequest(observable, new BaseRequestListener<Object>(this) {

            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("删除成功");
                EventBus.getDefault().post(new EventMessage(Event.deleteAddress, mAddress));
                finish();
            }
        },this);
    }

    private void getAddressInfo(final String addressId) {
        Observable<RequestResult<Address>> observable = mAddressService.getAddressDetail(addressId);
        if (mIsReturnGoodsAddress) {
            observable = mLiveStoreService.getAddr(addressId);
        }
        APIManager.startRequest(observable, new BaseRequestListener<Address>(this) {
            @Override
            public void onSuccess(Address address) {
                mAddress = address;
                mContactsEt.setText(mAddress.contacts);
                mPhoneEt.setText(mAddress.phone);
                mRegionTv.setText(mAddress.getFullRegion());
                mDetailEt.setText(mAddress.detail);
                mDefaultSwitch.setChecked(mAddress.isDefault);
            }
        },this);
    }

    @OnClick(R.id.regionLayout)
    protected void showRegionSelector() {
        new RegionSelectDialog(this, new OnSelectRegionLister() {
            @Override
            public void selected(HashMap<String, IRegion> regions) {
                IRegion province = regions.get("province");
                if (province != null) {
                    mAddress.provinceId = province.getId();
                    mAddress.provinceName = province.getName();
                }
                IRegion city = regions.get("city");
                if (city != null) {
                    mAddress.cityId = city.getId();
                    mAddress.cityName = city.getName();
                }
                IRegion distinct = regions.get("distinct");
                if (distinct != null) {
                    mAddress.districtId = distinct.getId();
                    mAddress.districtName = distinct.getName();
                }
                mRegionTv.setText(mAddress.getFullRegion());
            }
        }).show();
    }

    @OnClick(R.id.saveBtn)
    protected void onSave() {
        String contacts = mContactsEt.getText().toString();
        if (contacts.isEmpty()) {
            ToastUtil.error("收件人不能为空");
            return;
        }
        String phone = mPhoneEt.getText().toString();
        if (phone.isEmpty()) {
            ToastUtil.error("联系电话不能为空");
            return;
        }
//        if (!RegexUtils.isMobileSimple(phone)) {
//            ToastUtil.error("请检查联系电话格式");
//            return;
//        }
        if (StringUtils.isEmpty(mAddress.provinceId) || StringUtils.isEmpty(mAddress.cityId) || StringUtils.isEmpty(mAddress.districtId)) {
            ToastUtil.error("请选择地区");
            return;
        }
        String detail = mDetailEt.getText().toString();
        if (detail.isEmpty()) {
            ToastUtil.error("请输入收货地址");
            return;
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("isDefault", mDefaultSwitch.isChecked() ? 1 : 0);
        params.put("contact", contacts);
        params.put("phone", phone);
        params.put("provinceName", mAddress.provinceName);
        params.put("cityName", mAddress.cityName);
        params.put("districtName", mAddress.districtName);
        params.put("provinceId", mAddress.provinceId);
        params.put("cityId", mAddress.cityId);
        params.put("districtId", mAddress.districtId);
        params.put("detail", detail);

        if (isEditAddress) {
            Observable<RequestResult<Object>> resultObservable = mAddressService.editAddress(mAddress.addressId, params);
            if (mIsReturnGoodsAddress) {
                resultObservable = mLiveStoreService.editAddr(mAddress.addressId, params);
            }
            APIManager.startRequest(resultObservable, new BaseRequestListener<Object>(this) {

                @Override
                public void onSuccess(Object result) {
                    ToastUtil.success("保存成功");
                    EventBus.getDefault().post(new EventMessage(Event.saveAddress));
                    finish();
                }
            },this);
        } else {
            Observable<RequestResult<Address>> resultObservable = mAddressService.createAddress(params);
            if (mIsReturnGoodsAddress) {
                resultObservable = mLiveStoreService.addAddr(params);
            }
            APIManager.startRequest(resultObservable, new BaseRequestListener<Address>(this) {

                @Override
                public void onSuccess(Address result) {
                    ToastUtil.success("保存成功");
                    EventBus.getDefault().post(new EventMessage(Event.saveAddress));
                    finish();
                }
            },this);
        }

    }
}
