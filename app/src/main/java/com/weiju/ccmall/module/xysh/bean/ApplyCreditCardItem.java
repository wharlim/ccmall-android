package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ApplyCreditCardItem implements Serializable {

    /**
     * name : 光大国民老公白熊卡
     * stationChannelId : 974490231652745216
     * stationBankCardChannelId : 1045868976405479425
     * sort : 1
     * description : 国民老公，又萌又暖
     * imgUrlPath : https://lion-cloud.oss-cn-hangzhou.aliyuncs.com/applyCard/924946155891064834/bank-card-4bccc069-b282-4e14-917a-55bf9ee74751.jpg
     * bankCardStyleList : [{"id":"1023859245277249536","name":"嗨购京东,满100减30","sort":1},{"id":"1023859245294026752","name":"唯品会购物满108立减18元","sort":2},{"id":"1023859245306609664","name":"微信、QQ支付交易计积分","sort":3},{"id":"1023859245323386881","name":"十元享美食、看大片","sort":4}]
     */

    @SerializedName("name")
    public String name;
    @SerializedName("stationChannelId")
    public String stationChannelId;
    @SerializedName("stationBankCardChannelId")
    public String stationBankCardChannelId;
    @SerializedName("sort")
    public int sort;
    @SerializedName("description")
    public String description;
    @SerializedName("imgUrlPath")
    public String imgUrlPath;
    @SerializedName("bankCardStyleList")
    public List<TagListItem> bankCardStyleList;
}
