package com.weiju.ccmall.module.address;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Address;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IAddressService;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.service.contract.ILotteryService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

public class AddressListActivity extends BaseActivity implements PageManager.RequestListener {

    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.noDataLayout)
    protected View mNoDataLayout;
    @BindView(R.id.tvAddAddress)
    protected TextView tvAddAddress;

    private AddressAdapter mAddressAdapter;
    private boolean isSelectAddress = false;
    private IAddressService mAddressService;
    private ILiveStoreService mLiveStoreService;
    private PageManager mPageManager;
    private boolean mIsLottery;
    private String mDrawId;
    private boolean mIsReturnGoodsAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        ButterKnife.bind(this);
        initAction();
        mAddressService = ServiceManager.getInstance().createService(IAddressService.class);
        mLiveStoreService = ServiceManager.getInstance().createService(ILiveStoreService.class);

        mAddressAdapter = new AddressAdapter(this, isSelectAddress, mIsLottery, mIsReturnGoodsAddress);
        mRecyclerView.setAdapter(mAddressAdapter);
        try {
            mPageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(mRecyclerView)
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .setRequestListener(this)
                    .build(this);
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }
    }

    private void initAction() {
        showHeader();
        Intent intent = getIntent();
        if (intent == null) {
            isSelectAddress = false;
        } else if (intent.getExtras() == null) {
            isSelectAddress = false;
        } else {
            String action = getIntent().getExtras().getString("action");
            isSelectAddress = action != null && action.equals(Key.SELECT_ADDRESS);
            mIsLottery = getIntent().getBooleanExtra("isLottery", false);
            mIsReturnGoodsAddress = getIntent().getBooleanExtra("isReturnGoodsAddress", false);
            mDrawId = getIntent().getStringExtra("drawId");
        }
        setTitle(isSelectAddress ? "选择收货地址" : (mIsReturnGoodsAddress ? "管理退货地址" : "管理收货地址"));
        if (mIsReturnGoodsAddress) {
            tvAddAddress.setText("+ 添加退货地址");
            tvAddAddress.setTextColor(getResources().getColor(R.color.white));
            tvAddAddress.setBackgroundColor(getResources().getColor(R.color.red));
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        setLeftBlack();
//        getHeaderLayout().setRightDrawable(R.drawable.icon_add);
//        getHeaderLayout().setOnRightClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(AddressListActivity.this, AddressFormActivity.class));
//            }
//        });
        mPageManager.onRefresh();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void selectAddress(EventMessage message) {
        if (message.getEvent() == Event.selectAddress) {
            Intent intent = getIntent();
            intent.putExtra("address", (Address) message.getData());
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else if (message.getEvent() == Event.deleteAddress) {
            mPageManager.onRefresh();
        } else if (message.getEvent() == Event.saveAddress) {
            mPageManager.onRefresh();
        } else if (message.getEvent() == Event.selectLotteryAddress) {
            Address address = (Address) message.getData();
            if (address != null) {
                acceptPrize(address);
            }
        }
    }

    /**
     * 领取奖品
     *
     * @param address
     */
    private void acceptPrize(final Address address) {
        ILotteryService service = ServiceManager.getInstance().createService(ILotteryService.class);
        APIManager.startRequest(
                service.acceptPrize(mDrawId, address.addressId),
                new BaseRequestListener<Object>(this) {
                    @Override
                    public void onSuccess(Object result) {
                        super.onSuccess(result);
                        EventBus.getDefault().post(new EventMessage(Event.acceptPrizeSuccess, address));
                        ToastUtil.success("领取成功");
                        finish();
                    }
                },this
        );
    }

    @Override
    public void nextPage(final int page) {
        Observable<RequestResult<PaginationEntity<Address, Object>>> addressList = mAddressService.getAddressList(page);
        if (mIsReturnGoodsAddress){
            addressList = mLiveStoreService.getList(page, 15);
        }
        APIManager.startRequest(addressList, new BaseRequestListener<PaginationEntity<Address, Object>>(mRefreshLayout) {

            @Override
            public void onSuccess(PaginationEntity<Address, Object> result) {
                if (page == 1) {
                    mAddressAdapter.removeAllItems();
                }
                mPageManager.setLoading(false);
                mPageManager.setTotalPage(result.totalPage);
                mAddressAdapter.addItems(result.list);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mPageManager.setLoading(false);
                mRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onComplete() {
                super.onComplete();
                mPageManager.setLoading(false);
                mRefreshLayout.setRefreshing(false);
            }
        },this);
    }

    @OnClick(R.id.tvAddAddress)
    public void onViewClicked() {
        Intent intent = new Intent(AddressListActivity.this, AddressFormActivity.class);
        intent.putExtra("isReturnGoodsAddress", mIsReturnGoodsAddress);
        startActivity(intent);
    }
}
