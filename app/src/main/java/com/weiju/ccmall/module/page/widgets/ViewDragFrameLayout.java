package com.weiju.ccmall.module.page.widgets;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.MotionEventCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.ViewDragHelper;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

import com.weiju.ccmall.R;

public class ViewDragFrameLayout extends FrameLayout {

    private View mBtn;

    public ViewDragFrameLayout(Context context) {
        super(context);
    }

    public ViewDragFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ViewDragFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
    ViewDragHelper mViewDragHelper;
    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        //中间参数表示灵敏度,比如滑动了多少像素才视为触发了滑动.值越大越灵敏.
        mViewDragHelper = ViewDragHelper.create(this, 1f, new ViewDragCallback());
        mBtn = findViewById(R.id.imageHome);
    }
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        //固定写法
        int action = MotionEventCompat.getActionMasked(ev);
        if (action == MotionEvent.ACTION_CANCEL
                || action == MotionEvent.ACTION_UP) {
            mViewDragHelper.cancel();
            return false;
        }
        boolean ret = mViewDragHelper.shouldInterceptTouchEvent(ev);
        return ret;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        //固定写法
        mViewDragHelper.processTouchEvent(event);
        return true;
    }

    @Override
    public void computeScroll() {
        //固定写法
        //此方法用于自动滚动,比如自动回滚到默认位置.
        if (mViewDragHelper.continueSettling(true)) {
            ViewCompat.postInvalidateOnAnimation(this);
        }
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
    }

    private class ViewDragCallback extends ViewDragHelper.Callback{

        @Override
        public boolean tryCaptureView(View child, int pointerId) {
            return mBtn == child;
        }

        @Override
        public int clampViewPositionHorizontal(View child, int left, int dx) {
            if (left < 0) {
                return 0;
            } else if (left > getMeasuredWidth() - child.getMeasuredWidth()) {
                return getMeasuredWidth() - child.getMeasuredWidth();
            }
            return left;
        }

        @Override
        public int clampViewPositionVertical(View child, int top, int dy) {
            if (top < 0) {
                return 0;
            } else if (top > getMeasuredHeight() - child.getMeasuredHeight()) {
                return getMeasuredHeight() - child.getMeasuredHeight();
            }
            return top;
        }

        @Override
        public int getViewHorizontalDragRange(@NonNull View child) {
            return child == mBtn ? getMeasuredWidth() - child.getWidth() : 0;
        }

        @Override
        public int getViewVerticalDragRange(@NonNull View child) {
            return child == mBtn ? getMeasuredHeight() - child.getHeight() : 0;
        }

        @Override
        public void onViewReleased(@NonNull View releasedChild, float xvel, float yvel) {
            FrameLayout.LayoutParams lp = (LayoutParams) releasedChild.getLayoutParams();
            lp.leftMargin = releasedChild.getLeft();
            lp.topMargin = releasedChild.getTop();
            lp.gravity = 0;
            releasedChild.setLayoutParams(lp);
        }
    }
}
