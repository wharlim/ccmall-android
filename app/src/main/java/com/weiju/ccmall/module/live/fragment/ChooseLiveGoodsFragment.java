package com.weiju.ccmall.module.live.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.activity.ChooseLiveGoodsActivity;
import com.weiju.ccmall.module.live.adapter.ChooseLiveGoodsAdapter;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICollectService;
import com.weiju.ccmall.shared.service.contract.IFootService;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class ChooseLiveGoodsFragment extends BaseListFragment {

    @BindView(R.id.cleanBtn)
    protected ImageView mCleanBtn;
    @BindView(R.id.flSearch)
    protected FrameLayout flSearch;
    @BindView(R.id.keywordEt)
    protected EditText mKeywordEt;

    public static int TYPE_LIVE = 0;
    public static int TYPE_COLLECT = 1;
    public static int TYPE_FOOT = 2;
    public static int TYPE_SEARCH = 3;

    private List<SkuInfo> mData = new ArrayList<>();
    private ChooseLiveGoodsAdapter mAdapter = new ChooseLiveGoodsAdapter(mData);
    private int mType;
    private int mCurrentPage;
    private ChooseLiveGoodsActivity mActivity;
    private String mKeyword = "";

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);
    private ICollectService mCollectService = ServiceManager.getInstance().createService(ICollectService.class);
    private IFootService mFootService = ServiceManager.getInstance().createService(IFootService.class);
    private IProductService mProductService = ServiceManager.getInstance().createService(IProductService.class);

    @Override
    public int getContentLayoutRes() {
        return R.layout.fragment_choose_live_goods;
    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mType = getArguments() != null ? getArguments().getInt("type") : 0;
    }

    @Override
    public void initView() {
        super.initView();
        mRecyclerView.setPadding(0, ConvertUtil.dip2px(10), 0, 0);
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.bg_gray));
        mActivity = (ChooseLiveGoodsActivity) getActivity();
        if (mType == TYPE_SEARCH) {
            initSearchLayout();
        }
    }

    private void initSearchLayout() {
        flSearch.setVisibility(View.VISIBLE);
        if (!mKeyword.isEmpty()) {
            mKeywordEt.setText(mKeyword);
//            mPageManager.onRefresh();
        } else {
            mKeywordEt.requestFocus();
        }

        mKeywordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mCleanBtn.setVisibility(charSequence.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mKeywordEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH || (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    search(textView.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });

    }

    protected void search(String keyword) {
        if (TextUtils.isEmpty(keyword)) {
            ToastUtil.error("请输入关键词");
            return;
        }
        mKeyword = keyword;
        mCleanBtn.setVisibility(View.VISIBLE);
        hideInput();
        getData(true);
    }

    /**
     * 隐藏键盘
     */
    protected void hideInput() {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(INPUT_METHOD_SERVICE);
        View v = mActivity.getWindow().peekDecorView();
        if (null != v) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        if (mActivity != null) {
            SkuInfo skuInfo = mAdapter.getItem(position);
            if (skuInfo == null) return;
            if (skuInfo.isSelected) {
                mActivity.removeSkuId(skuInfo.skuId);
                skuInfo.isSelected = false;
            } else {
                mActivity.addSkuId(skuInfo.skuId);
                skuInfo.isSelected = true;
            }
            mAdapter.notifyDataSetChanged();
            EventBus.getDefault().post(new EventMessage(Event.chooseLiveGoods, skuInfo.skuId));
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(final boolean isRefresh) {
        if (mType == TYPE_SEARCH && mKeyword.isEmpty()){
            if (mRefreshLayout.isRefreshing()) mRefreshLayout.setRefreshing(false);
            return;
        }
        if (isRefresh) {
            mCurrentPage = 1;
        } else {
            mCurrentPage++;
        }
        Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> observable = mService.productList(mCurrentPage, 15);
        if (mType == TYPE_COLLECT) {
            observable = mCollectService.getCollectList(mCurrentPage);
        } else if (mType == TYPE_FOOT) {
            observable = mFootService.getFootList(mCurrentPage);
        } else if (mType == TYPE_SEARCH) {
            observable = mProductService.search(mKeyword, mCurrentPage);
        }
        APIManager.startRequest(observable, new BaseRequestListener<PaginationEntity<SkuInfo, Object>>(mRefreshLayout) {
            @Override
            public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                super.onSuccess(result);
                List<String> chooseSkuIds = mActivity.getChooseSkuIds();
                if (mCurrentPage == 1 && mData.size() > 0) {
                    mActivity.removeSkus(mData);
//                    List<String> allSkuIds = mActivity.getAllSkuIds();
//                    for (String chooseSkuId : chooseSkuIds) {
//                        if (!allSkuIds.contains(chooseSkuId)) {
//                            mActivity.removeSkuId(chooseSkuId);
//                        }
//                    }
                }
                for (SkuInfo skuInfo : result.list) {
                    for (String skuId : chooseSkuIds) {
                        if (skuInfo.skuId.equals(skuId)) {
                            skuInfo.isSelected = true;
                            break;
                        }
                    }
                }
                if (mCurrentPage == 1) {
                    mData.clear();
                }
                mData.addAll(result.list);
                mActivity.addSkus(mData);
                mAdapter.notifyDataSetChanged();
                if (result.page >= result.totalPage) {
                    mAdapter.loadMoreEnd();
                } else {
                    mAdapter.loadMoreComplete();
                }
            }
        }, getContext());

    }

    @OnClick(R.id.cleanBtn)
    protected void cleanKeyword() {
        mKeywordEt.requestFocus();
        mKeywordEt.setText("");
        mCleanBtn.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(EventMessage message) {
        if (message.getEvent().equals(Event.chooseLiveGoods)) {
            if (!getUserVisibleHint()) {
                String skuId = (String) message.getData();
                for (SkuInfo skuInfo : mData) {
                    if (skuInfo.skuId.equals(skuId)) {
                        skuInfo.isSelected = !skuInfo.isSelected;
                        break;
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
        }
    }

    public static ChooseLiveGoodsFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt("type", type);
        ChooseLiveGoodsFragment fragment = new ChooseLiveGoodsFragment();
        fragment.setArguments(args);
        return fragment;
    }

}
