package com.weiju.ccmall.module.product.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Presents;
import com.weiju.ccmall.shared.bean.ProductActivityModel;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.util.PicassoUtils;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.photopicker.widget.BGANinePhotoLayout;

public class ProductActivityAdapter extends BaseMultiItemQuickAdapter<ProductActivityModel, BaseViewHolder> {

    private boolean mSingLine;

    public ProductActivityAdapter(List<ProductActivityModel> data, boolean isShowImg) {
        super(data);
        addItemType(ProductActivityModel.TEXT, R.layout.item_product_activity_tag_text);
        addItemType(ProductActivityModel.IMG, R.layout.item_product_activity_present);
    }

    @Override
    protected void convert(BaseViewHolder helper, ProductActivityModel item) {
        switch (item.getItemType()) {
            case ProductActivityModel.TEXT:
                SkuInfo.ActivityTagEntity tagEntity = item.getTagEntity();
                ImageView ivTag = helper.getView(R.id.ivTag);
                if (StringUtils.isEmpty(item.getTagEntity().iconUrl)) {
                    ivTag.setVisibility(View.GONE);
                } else {
                    PicassoUtils.loadImage(ivTag, item.getTagEntity().iconUrl, 0, SizeUtils.dp2px(18));
                }

                TextView tvText = helper.getView(R.id.tvText);
                tvText.setSingleLine(mSingLine);
                tvText.setText(tagEntity.getActivityText(mSingLine));
                break;
            case ProductActivityModel.IMG:
                List<Presents> presents = item.getPresents();
//                BGANinePhotoLayout layoutImages = helper.getView(R.id.layoutImages);
//                setImagePhone(layoutImages, presents);
                break;
            default:
        }
    }

    private void setImagePhone(BGANinePhotoLayout layoutImages, List<Presents> presents) {
        ArrayList<String> images = new ArrayList<>();
        for (Presents present : presents) {
            images.add(present.thumbUrl);
        }
        layoutImages.setData(images);
    }

    public void setSingLine(boolean singLine) {
        mSingLine = singLine;
    }
}
