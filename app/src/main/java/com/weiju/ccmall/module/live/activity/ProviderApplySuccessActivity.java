package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;

import com.blankj.utilcode.utils.ToastUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.entity.DepositEntity;
import com.weiju.ccmall.module.pay.PayMsg;
import com.weiju.ccmall.module.pay.PayOrderActivity;
import com.weiju.ccmall.newRetail.bean.ActiveValueRule;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProviderApplySuccessActivity extends BaseActivity {


    @BindView(R.id.tvContent)
    TextView mTvContent;

    ILiveStoreService mService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_apply_success);
        ButterKnife.bind(this);
        setTitle("入驻申请");
        setLeftBlack();
        mService = ServiceManager.getInstance().createService(ILiveStoreService.class);
        APIManager.startRequest(mService.getSuccessInfo(), new BaseRequestListener<ActiveValueRule>(this) {
            @Override
            public void onSuccess(ActiveValueRule result) {
                super.onSuccess(result);
                if (!isDestroyed()) {
                    mTvContent.setText(Html.fromHtml(result.content));
                }
            }
        }, this);
    }

    @OnClick(R.id.tvPay)
    public void onViewClicked() {
        APIManager.startRequest(mService.payLiveStoreDeposit(), new BaseRequestListener<DepositEntity>(this) {
            @Override
            public void onSuccess(DepositEntity result) {
                super.onSuccess(result);
//                OrderService.viewPayActivity(ProviderApplySuccessActivity.this, result.orderCode, -1);
                Intent intent = new Intent(ProviderApplySuccessActivity.this, PayOrderActivity.class);
                intent.putExtra("orderCode", result.orderCode);
                intent.putExtra("goodsType", PayOrderActivity.GOODSTYPE_LIVE_STORE);
                ProviderApplySuccessActivity.this.startActivity(intent);
            }
        }, this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(PayMsg msgStatus) {
        switch (msgStatus.getAction()) {
            case PayMsg.ACTION_ALIPAY_SUCCEED:
            case PayMsg.ACTION_WEBPAY_SUCCEED:
            case PayMsg.ACTION_WXPAY_SUCCEED:
            case PayMsg.ACTION_BALANCE_SUCCEED:
//                EventBus.getDefault().post(new EventMessage(Event.paySuccess));
                EventBus.getDefault().post(new EventMessage(Event.ApplyPaySuccess));
                ToastUtil.success("支付成功");
                finish();
                break;
            case PayMsg.ACTION_WXPAY_FAIL:
                ToastUtils.showShortToast(msgStatus.message);
                break;
            case PayMsg.ACTION_ALIPAY_FAIL:
                ToastUtils.showShortToast(msgStatus.message);
                break;
            case PayMsg.ACTION_WEBPAY_FAIL:
                finish();
//                EventUtil.viewHome(PayOrderActivity.this);
                break;
            default:
        }
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, ProviderApplySuccessActivity.class);
        context.startActivity(intent);
    }

}
