package com.weiju.ccmall.module.user;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StyleRes;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.util.FrescoUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/1/10 on 16:17
 * @desc 确认邀请人弹窗
 */
public class InvitaionUserDialog extends Dialog {


    @BindView(R.id.ivStoreAvatar)
    SimpleDraweeView mIvStoreAvatar;
    @BindView(R.id.tvUserName)
    TextView mTvUserName;
    @BindView(R.id.tvUserPhone)
    TextView mTvUserPhone;
    private View.OnClickListener mOnClickListener;

    public void setUser(User mUser) {
        mTvUserName.setText(mUser.nickname);
        mTvUserPhone.setText(mUser.phone);
        FrescoUtil.setImageSmall(mIvStoreAvatar, mUser.avatar);
    }

    private User mUser;

    public InvitaionUserDialog(@NonNull Context context, View.OnClickListener onClickListener) {
        this(context, 0);
        this.mOnClickListener = onClickListener;
    }

    public InvitaionUserDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_invitaion_user_layout);

        ButterKnife.bind(this);
        initWindow();

    }


    private void initWindow() {
        Window window = getWindow();
        if (window != null) {
            window.setGravity(Gravity.CENTER);
            window.setWindowAnimations(R.style.BottomDialogStyle);
            window.getDecorView().setPadding(0, 0, 0, 0);
            //获得window窗口的属性
            WindowManager.LayoutParams lp = window.getAttributes();
            //设置窗口宽度为充满全屏
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            //设置窗口高度为包裹内容
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            //将设置好的属性set回去
            window.setAttributes(lp);
        }
    }

    @OnClick(R.id.cancelBtn)
    protected void onCancel() {
        dismiss();
    }

    @OnClick(R.id.okBtn)
    protected void onOk(View view) {
        mOnClickListener.onClick(view);
    }

}
