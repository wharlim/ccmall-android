package com.weiju.ccmall.module.user.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Page;

import java.util.List;

/**
 * @author chenyanmin
 * @date 2019/6/23
 * @desc
 */
public class MyContributionSortTagAdapter extends BaseQuickAdapter<Page, BaseViewHolder> {
    private int mSelectPosition = 0;

    public MyContributionSortTagAdapter() {
        super(R.layout.item_contribution_sort_tag);
    }

    public void setSelectPosition(int position) {
        if (position == mSelectPosition) {
            return;
        } else {
            mSelectPosition = position;
            notifyDataSetChanged();
        }

    }

    @Override
    protected void convert(BaseViewHolder helper, Page item) {
        helper.setVisible(R.id.tagIv, mSelectPosition == helper.getPosition());
        helper.setText(R.id.tagTv, item.name);
        helper.setTextColor(R.id.tagTv, mContext.getResources().getColor(
                mSelectPosition == helper.getPosition() ? R.color.contribution_sort_tag_select_text : R.color.white));
    }
}
