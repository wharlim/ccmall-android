package com.weiju.ccmall.module.jkp.newjkp;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Ben
 * @date 2020/4/10.
 */
public class GoodsListByCategory {

    /**
     * min_id : 16529956
     * data : [{"activity_type":"普通活动","activityid":"c7444b536f2f4a5d9eae835508e733ec","coupon_condition":"80.0","couponexplain":"单笔满80元可用","couponurl":"https://uland.taobao.com/quan/detail?sellerId=2206763495837&activityId=c7444b536f2f4a5d9eae835508e733ec","deposit":"0.0","deposit_deduct":"0.0","discount":"0.55","general_index":"0","guide_article":"","is_brand":"0","is_explosion":"0","is_live":"0","is_shipping":"0","isquality":"0","itemdesc":"专柜品质！【纯棉】放心好棉，亲肤柔软，不刺激，春夏热卖，孕期+哺乳一套搞定！俏皮少女风，舒适坐月子~朋友圈文案：1套抵3套，专业版型设计【赠运费险】","itemendprice":"49.0","itemid":"613213047429","itempic":"https://img.alicdn.com/imgextra/i2/2206763495837/O1CN01b9JXn81szOQ9tLNQV_!!2206763495837.jpg","itempic_copy":"1_613213047429_1586411997.jpg","itemprice":"89.0","itemsale":"328","itemsale2":"0","itemshorttitle":"纯棉春秋月子服产后哺乳衣","itemtitle":"月子服春秋季4月份5纯棉产后哺乳衣喂奶孕妇睡衣产妇怀孕期夏薄款","me":"","online_users":"0","original_article":"","original_img":"","planlink":"","report_status":"0","seller_id":"0","seller_name":"淘起来","sellernick":"艾莱季旗舰店","shopid":"0","shopname":"艾莱季旗舰店","shoptype":"B","son_category":"50012772","start_time":"1586411977","starttime":"1586411892","taobao_image":"","tkmoney":"9.81","tkrates":"20.02","tktype":"3","todaycouponreceive":"0","todaysale":"0","userid":"2206763495837","videoid":"0","ysyl_tlj_face":"-1.0","couponreceive":"0","fqcat":"3","end_time":"1586966399","down_type":"0","cuntao":"0","couponsurplus":"0","couponstarttime":"1586361600","couponreceive2":"0","couponnum":"10000","couponmoney":"40","couponendtime":"1586966399","product_id":"16530565"}]
     */

    @SerializedName("min_id")
    public int minId;
    @SerializedName("data")
    public List<DataBean> data;

    public static class DataBean {
        /**
         * activity_type : 普通活动
         * activityid : c7444b536f2f4a5d9eae835508e733ec
         * coupon_condition : 80.0
         * couponexplain : 单笔满80元可用
         * couponurl : https://uland.taobao.com/quan/detail?sellerId=2206763495837&activityId=c7444b536f2f4a5d9eae835508e733ec
         * deposit : 0.0
         * deposit_deduct : 0.0
         * discount : 0.55
         * general_index : 0
         * guide_article :
         * is_brand : 0
         * is_explosion : 0
         * is_live : 0
         * is_shipping : 0
         * isquality : 0
         * itemdesc : 专柜品质！【纯棉】放心好棉，亲肤柔软，不刺激，春夏热卖，孕期+哺乳一套搞定！俏皮少女风，舒适坐月子~朋友圈文案：1套抵3套，专业版型设计【赠运费险】
         * itemendprice : 49.0
         * itemid : 613213047429
         * itempic : https://img.alicdn.com/imgextra/i2/2206763495837/O1CN01b9JXn81szOQ9tLNQV_!!2206763495837.jpg
         * itempic_copy : 1_613213047429_1586411997.jpg
         * itemprice : 89.0
         * itemsale : 328
         * itemsale2 : 0
         * itemshorttitle : 纯棉春秋月子服产后哺乳衣
         * itemtitle : 月子服春秋季4月份5纯棉产后哺乳衣喂奶孕妇睡衣产妇怀孕期夏薄款
         * me :
         * online_users : 0
         * original_article :
         * original_img :
         * planlink :
         * report_status : 0
         * seller_id : 0
         * seller_name : 淘起来
         * sellernick : 艾莱季旗舰店
         * shopid : 0
         * shopname : 艾莱季旗舰店
         * shoptype : B
         * son_category : 50012772
         * start_time : 1586411977
         * starttime : 1586411892
         * taobao_image :
         * tkmoney : 9.81
         * tkrates : 20.02
         * tktype : 3
         * todaycouponreceive : 0
         * todaysale : 0
         * userid : 2206763495837
         * videoid : 0
         * ysyl_tlj_face : -1.0
         * couponreceive : 0
         * fqcat : 3
         * end_time : 1586966399
         * down_type : 0
         * cuntao : 0
         * couponsurplus : 0
         * couponstarttime : 1586361600
         * couponreceive2 : 0
         * couponnum : 10000
         * couponmoney : 40
         * couponendtime : 1586966399
         * product_id : 16530565
         */

        @SerializedName("activity_type")
        public String activityType;
        @SerializedName("activityid")
        public String activityid;
        @SerializedName("coupon_condition")
        public String couponCondition;
        @SerializedName("couponexplain")
        public String couponexplain;
        @SerializedName("couponurl")
        public String couponurl;
        @SerializedName("deposit")
        public String deposit;
        @SerializedName("deposit_deduct")
        public String depositDeduct;
        @SerializedName("discount")
        public String discount;
        @SerializedName("general_index")
        public String generalIndex;
        @SerializedName("guide_article")
        public String guideArticle;
        @SerializedName("is_brand")
        public String isBrand;
        @SerializedName("is_explosion")
        public String isExplosion;
        @SerializedName("is_live")
        public String isLive;
        @SerializedName("is_shipping")
        public String isShipping;
        @SerializedName("isquality")
        public String isquality;
        @SerializedName("itemdesc")
        public String itemdesc;
        @SerializedName("itemendprice")
        public String itemendprice;
        @SerializedName("itemid")
        public String itemid;
        @SerializedName("itempic")
        public String itempic;
        @SerializedName("itempic_copy")
        public String itempicCopy;
        @SerializedName("itemprice")
        public String itemprice;
        @SerializedName("itemsale")
        public String itemsale;
        @SerializedName("itemsale2")
        public String itemsale2;
        @SerializedName("itemshorttitle")
        public String itemshorttitle;
        @SerializedName("itemtitle")
        public String itemtitle;
        @SerializedName("me")
        public String me;
        @SerializedName("online_users")
        public String onlineUsers;
        @SerializedName("original_article")
        public String originalArticle;
        @SerializedName("original_img")
        public String originalImg;
        @SerializedName("planlink")
        public String planlink;
        @SerializedName("report_status")
        public String reportStatus;
        @SerializedName("seller_id")
        public String sellerId;
        @SerializedName("seller_name")
        public String sellerName;
        @SerializedName("sellernick")
        public String sellernick;
        @SerializedName("shopid")
        public String shopid;
        @SerializedName("shopname")
        public String shopname;
        @SerializedName("shoptype")
        public String shoptype;
        @SerializedName("son_category")
        public String sonCategory;
        @SerializedName("start_time")
        public String startTime;
        @SerializedName("starttime")
        public String starttime;
        @SerializedName("taobao_image")
        public String taobaoImage;
        @SerializedName("tkmoney")
        public String tkmoney;
        @SerializedName("tkrates")
        public String tkrates;
        @SerializedName("tktype")
        public String tktype;
        @SerializedName("todaycouponreceive")
        public String todaycouponreceive;
        @SerializedName("todaysale")
        public String todaysale;
        @SerializedName("userid")
        public String userid;
        @SerializedName("videoid")
        public String videoid;
        @SerializedName("ysyl_tlj_face")
        public String ysylTljFace;
        @SerializedName("couponreceive")
        public String couponreceive;
        @SerializedName("fqcat")
        public String fqcat;
        @SerializedName("end_time")
        public String endTime;
        @SerializedName("down_type")
        public String downType;
        @SerializedName("cuntao")
        public String cuntao;
        @SerializedName("couponsurplus")
        public String couponsurplus;
        @SerializedName("couponstarttime")
        public String couponstarttime;
        @SerializedName("couponreceive2")
        public String couponreceive2;
        @SerializedName("couponnum")
        public String couponnum;
        @SerializedName("couponmoney")
        public String couponmoney;
        @SerializedName("couponendtime")
        public String couponendtime;
        @SerializedName("product_id")
        public String productId;
    }
}
