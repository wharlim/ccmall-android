package com.weiju.ccmall.module.auth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.blankj.utilcode.utils.ToastUtils;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.auth.model.CardDetailModel;
import com.weiju.ccmall.module.auth.model.CardItemModel;
import com.weiju.ccmall.module.auth.model.body.AddCardBody;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.CardUrl;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICaptchaService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.CountDownRxUtils;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;


import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;


public class AuthPhoneActivity2 extends BaseActivity {

    @BindView(R.id.etCheckNumber)
    EditText mEtCheckNumber;
    @BindView(R.id.etPassword)
    EditText mEtPassword;
    @BindView(R.id.tvGetCheckNumber)
    TextView mTvGetCheckNumber;
    private final int TIME_COUNT = 60;
    @BindView(R.id.ivImg)
    ImageView mIvImg;
    @BindView(R.id.tvTag)
    TextView mTvTag;
    @BindView(R.id.tvSubmit)
    TextView mTvSubmit;
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.etIdCard)
    EditText mEtIdCard;
    @BindView(R.id.etCardNumber)
    EditText mEtCardNumber;
    @BindView(R.id.etPhoneNumber)
    EditText mEtPhoneNumber;
    @BindView(R.id.spSelectYH)
    Spinner mSpSelectYH;

    @BindView(R.id.activity_auth_phone)
    LinearLayout activityAuthPhone;
    @BindView(R.id.spSelectYHTip)
    TextView mSpSelectYHTip;
    private CardItemModel mCardItemModel;

    private ICaptchaService mCaptchaService;
    private User mUser;
    private IUserService mUserService;
    private int mType;
    private boolean mIsEdit;
    private String mUrl = "auth/sign";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth_phone2);
        ButterKnife.bind(this);
        getIntentData();
        setLeftBlack();
        initView();
        initData();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void getIntentData() {
        Intent intent = getIntent();
        mType = intent.getIntExtra(Config.INTENT_KEY_TYPE_NAME, Config.ERROR_CODE);
        mIsEdit = intent.getBooleanExtra("isEdit", false);
    }

    private void initView() {
        setTitle("绑定银行卡");
        mIvImg.setBackground(getResources().getDrawable(R.drawable.bg_auth_card_gongcat));
    }


    private void initData() {
        if (Config.IS_DEBUG) {
            mEtCheckNumber.setText("15500000000");
            mEtPassword.setText("123456");
            mEtIdCard.setText("420101196809257490");
            mEtCardNumber.setText("6222026354434538402");
        }
        mUser = SessionUtil.getInstance().getLoginUser();

        mTvName.setText(mUser.userName);
        mCaptchaService = ServiceManager.getInstance().createService(ICaptchaService.class);
        mUserService = ServiceManager.getInstance().createService(IUserService.class);

        APIManager.startRequest(mUserService.getBankList(), new BaseRequestListener<List<CardItemModel>>(this) {
            @Override
            public void onSuccess(List<CardItemModel> cardItemModels) {
                initSprinner(cardItemModels);
            }
        },this);

        if (mIsEdit) {
          //  mUrl = "account/edit";
            Observable<RequestResult<CardDetailModel>> gongCatCard = MyApplication.isGongCat?
                    mUserService.getGongCatCard(SessionUtil.getInstance().getOAuthToken()):
                    mUserService.getCard();
            APIManager.startRequest(gongCatCard, new BaseRequestListener<CardDetailModel>(this) {
                @Override
                public void onSuccess(CardDetailModel model) {
                    mTvName.setText(model.name);
                    mEtCardNumber.setText(model.bankNum);
                    mCardItemModel = new CardItemModel(model.bankId, model.bankName);
                    mEtPhoneNumber.setText(model.reserveMobile);
                    mEtCheckNumber.setText(model.mobile);
                    mEtIdCard.setText(model.idNumber);
                    mSpSelectYHTip.setVisibility(View.GONE);
                    mSpSelectYH.setVisibility(View.VISIBLE);
                }
            },this);
        }

    }

    private void initSprinner(final List<CardItemModel> cardItemModels) {
        ArrayList<String> names = new ArrayList<>();
        for (CardItemModel cardItemModel : cardItemModels) {
            names.add(cardItemModel.bankName);
        }
        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, names);
        mSpSelectYH.setAdapter(adapter);
        mSpSelectYH.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mCardItemModel = cardItemModels.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getCheckNumber() {
        if (UiUtils.checkETPhone(mEtPhoneNumber)) {
            APIManager.startRequest(mCaptchaService.getMemberAuthMsgByReservedPhone(mEtPhoneNumber.getText().toString()), new BaseRequestListener<Object>(this) {
                @Override
                public void onSuccess(Object result) {
                    CountDownRxUtils.textViewCountDown(mTvGetCheckNumber, TIME_COUNT, "获取验证码");
                }
            },this);
        }

    }


    private void addCard() {
        if (!checkInfo()) {
            return;
        }

        AddCardBody addCardBody = new AddCardBody(
                mTvName.getText().toString().trim(),
                mCardItemModel.bankName,
                mEtCardNumber.getText().toString().trim(),
                mEtPhoneNumber.getText().toString(),
                mEtCheckNumber.getText().toString(),
                mEtIdCard.getText().toString()
        );
        APIManager.startRequest(mUserService.addCard(mUrl, addCardBody.toMap()), new BaseRequestListener<CardUrl>(this) {
            @Override
            public void onSuccess(CardUrl result) {
                super.onSuccess(result);
                goSubmitSucceedActivity(result.url);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.error(e.getMessage());
            }
        },this);
    }

    private boolean checkInfo() {
        if (mTvName.getText().toString().length() < 2) {
            ToastUtil.error("请输入持卡人姓名");
            return false;
        }
        if (mEtIdCard.getText().toString().length() < 14) {
            ToastUtil.error("请输入您的身份证");
            return false;
        }

        if (mCardItemModel == null) {
            ToastUtil.error("请选择开户银行");
            return false;
        }
        if (mEtCardNumber.getText().toString().length() == 0) {
            ToastUtil.error("请输入银行卡号");
            return false;
        }
//        if (mEtPhoneNumber.getText().toString().length() < 10) {
//            ToastUtil.error("请输入预留手机号");
//            return false;
//        }

        if (mEtCheckNumber.getText().toString().length() < 10) {
            ToastUtil.error("请输入手机号");
            return false;
        }
        return true;
    }

    private void goSubmitSucceedActivity(String url) {
        ToastUtils.showShortToast("提交银行卡信息成功");
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
        EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_CARD_CHANGE));
        finish();
    }

    @OnClick({R.id.tvGetCheckNumber, R.id.tvSubmit, R.id.spSelectYHTip})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvGetCheckNumber:
                getCheckNumber();
                break;
            case R.id.tvSubmit:
                addCard();
                break;
            case R.id.spSelectYHTip:
                mSpSelectYHTip.setVisibility(View.GONE);
                mSpSelectYH.setVisibility(View.VISIBLE);
                mSpSelectYH.performClick();
                break;
            default:
                break;
        }
    }

}
