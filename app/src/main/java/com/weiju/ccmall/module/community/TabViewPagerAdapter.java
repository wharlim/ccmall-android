package com.weiju.ccmall.module.community;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.weiju.ccmall.module.community.models.CoursePlate;

import java.util.List;


/**
 * @author Stone
 * @time 2017/12/26  10:25
 * @desc 通用的TabViewPagerAdapter
 */

public class TabViewPagerAdapter extends FragmentPagerAdapter {
    private String[] titleArr;
    private final FragmentFactory mDataResource;
    public TabViewPagerFragment.TabViewPageAdapterTag mTag;
    private List<CoursePlate.CourseCatsBean> mCourseCats;

    public TabViewPagerAdapter(Context context, FragmentManager fm, TabViewPagerFragment.TabViewPageAdapterTag tag) {
        super(fm);
        mTag = tag;
        mDataResource = createDataResource(tag, context);
        assert mDataResource != null;
        titleArr = mDataResource.getTabTitle();
    }

    public TabViewPagerAdapter(Context context, FragmentManager fm, TabViewPagerFragment.TabViewPageAdapterTag tag, List<CoursePlate.CourseCatsBean> courseCats) {
        super(fm);
        mCourseCats = courseCats;
        mTag = tag;
        mDataResource = createDataResource(tag, context);
        assert mDataResource != null;
        titleArr = mDataResource.getTabTitle();
    }

    @Override
    public Fragment getItem(int position) {
        return mDataResource.createFragment(position);
    }

    @Override
    public int getCount() {
        return titleArr.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleArr[position];
    }

    private FragmentFactory createDataResource(TabViewPagerFragment.TabViewPageAdapterTag tag, Context context) {
        if (tag == TabViewPagerFragment.TabViewPageAdapterTag.COMMUNITY) {
            return new CommunityFragmentFactory(context);
        } else if (tag == TabViewPagerFragment.TabViewPageAdapterTag.SCHOOL) {
            return new SchoolFragmentFactory(context, mCourseCats);
        }
        return null;
    }

    public int getBottomLineWidth() {
        return mDataResource.getTabBottomLineWidth();
    }

    public boolean getVisiableAdd(int position) {
        return mDataResource.getVisiableAdd(position);
    }

    public int getAddClickAction(int position) {
        return mDataResource.getAddClickAction(position);
    }

    public boolean getNeedLogin(int position) {
        return mDataResource.getNeedLogin(position);
    }

    public int getTextPadding() {
        return mDataResource.getTextPadding();
    }

    public Class getSkipPage(int position) {
        return mDataResource.getScrollSkipPage(position);
    }

    public boolean isDistribute() {
        return mDataResource.isDistribute();
    }

    public boolean showLine() {
        return mDataResource.showLine();
    }
}
