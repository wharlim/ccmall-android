package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.adapter.SupportBanksAdapter;
import com.weiju.ccmall.module.xysh.bean.SupportBanksItem;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SupportBanksActivity extends BaseListActivity {

    private SupportBanksAdapter mAdapter = new SupportBanksAdapter();
    private String channelShortName;
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    @Override
    public String getTitleStr() {
        return "查看支持银行";
    }

    @Override
    public void initView() {
        super.initView();
        channelShortName = getIntent().getStringExtra("channelShortName");
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(service.supportBank(channelShortName), new Observer<XYSHCommonResult<List<SupportBanksItem>>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<List<SupportBanksItem>> listXYSHCommonResult) {
                mAdapter.setNewData(listXYSHCommonResult.data);
                mRefreshLayout.setRefreshing(false);
                mAdapter.loadMoreEnd(true);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_support_banks;
    }

    public static void start(Context context, String channelShortName) {
        Intent intent = new Intent(context, SupportBanksActivity.class);
        intent.putExtra("channelShortName", channelShortName);
        context.startActivity(intent);
    }
}
