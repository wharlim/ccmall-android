package com.weiju.ccmall.module.user.profit.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.module.balance.BalanceAdapter;
import com.weiju.ccmall.module.balance.BalanceDetailActivity;
import com.weiju.ccmall.module.order.OrderListFragment;
import com.weiju.ccmall.module.user.profit.adapter.ProfitAdapter;
import com.weiju.ccmall.shared.basic.BaseListFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Balance;
import com.weiju.ccmall.shared.bean.CommonExtra;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.enums.AccountType;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBalanceService;

/**
 * @author chenyanmin
 * @date 2019/6/22
 */
public class ProfitListFragment extends BaseListFragment {
    private ProfitAdapter mAdapter = new ProfitAdapter();
    private Page mPage;
    private int mType;
    private int mCurrentPage;

    private IBalanceService mBalanceService = ServiceManager.getInstance().createService(IBalanceService.class);

    public static OrderListFragment newInstance(Page page) {
        Bundle args = new Bundle();
        args.putSerializable("page", page);
        OrderListFragment fragment = new OrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void initView() {
        super.initView();
        switch (mPage.id) {
            //platForm=1 商城，=2信用生活， =3集靠谱
            case "mall":
                mType = 1;
                break;
            case "xysh":
                mType = 2;
                break;
            case "jkp":
                mType = 3;
                break;
            default:
                mType = 1;
        }

        mRecyclerView.addItemDecoration(new ListDividerDecoration(getContext()));

    }

    @Override
    public void getIntentData() {
        super.getIntentData();
        mPage = (Page) getArguments().get("page");
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {

    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
        } else {
            mCurrentPage++;
        }
        APIManager.startRequest(mBalanceService.getProfitListByPlatForm(mType, mCurrentPage, 10),
                new BaseRequestListener<PaginationEntity<Balance, CommonExtra>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<Balance, CommonExtra> result) {
                        if (isRefresh) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh);
                        } else {
                            mAdapter.loadMoreComplete();
                        }

                    }
                },getContext());
    }
}
