package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.orhanobut.logger.Logger;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.fragment.ChooseLiveGoodsFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.body.AddLiveBody;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/5/12.
 */
public class ChooseLiveGoodsActivity extends BaseActivity {
    @BindView(R.id.tvConfirm)
    TextView tvConfirm;
    private List<String> mSkuIds = new ArrayList<>();
    private List<String> mTitles = new ArrayList<>();
    private List<SkuInfo> mSkus = new ArrayList<>();

    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;
    @BindView(R.id.tabLayout)
    protected TabLayout mTabLayout;
    private FragmentPagerAdapter mFragmentPagerAdapter;
    private List<BaseFragment> baseFragments = new ArrayList<>();
    private String mLiveId;
    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);
    private boolean mIsCreate;  //是否是创建直播

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_live_goods);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        setLeftBlack();
        setTitle("选择直播商品");
        initViewPager();
        initConfirmBtn();
    }

    private void initViewPager() {
        mLiveId = getIntent().getStringExtra("liveId");
        boolean shopkeeper = getIntent().getBooleanExtra("shopkeeper", false);
        mIsCreate = getIntent().getBooleanExtra("isCreate", false);
        if (mIsCreate) {
            ArrayList<String> skuIds = getIntent().getStringArrayListExtra("skuIds");
            mSkuIds.addAll(skuIds);
        }
        if (shopkeeper) {
            mTitles.add("直播小店");
            baseFragments.add(ChooseLiveGoodsFragment.newInstance(ChooseLiveGoodsFragment.TYPE_LIVE));
        }
        mTitles.add("收藏夹");
        mTitles.add("我的足迹");
        mTitles.add("搜索");
        baseFragments.add(ChooseLiveGoodsFragment.newInstance(ChooseLiveGoodsFragment.TYPE_COLLECT));
        baseFragments.add(ChooseLiveGoodsFragment.newInstance(ChooseLiveGoodsFragment.TYPE_FOOT));
        baseFragments.add(ChooseLiveGoodsFragment.newInstance(ChooseLiveGoodsFragment.TYPE_SEARCH));
        mFragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return baseFragments.get(position);
            }

            @Override
            public int getCount() {
                return baseFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mTitles.get(position);
            }
        };
        mTabLayout.setTabIndicatorFullWidth(false);
        mViewPager.setAdapter(mFragmentPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(baseFragments.size());
        mViewPager.setCurrentItem(0);
    }


    protected void initConfirmBtn() {
        int selectedQuantity = mSkuIds.size();
        if (selectedQuantity > 0) {
            tvConfirm.setEnabled(true);
            tvConfirm.setText("确定(" + selectedQuantity + ")");
        } else {
            tvConfirm.setEnabled(false);
            tvConfirm.setText("确定");
        }
    }

    public List<String> getChooseSkuIds() {
        return mSkuIds;
    }

    public void addSkuId(String skuId) {
        mSkuIds.add(skuId);
        initConfirmBtn();
    }

    public void removeSkuId(String skuId) {
        mSkuIds.remove(skuId);
        initConfirmBtn();
    }

    public List<SkuInfo> getChooseSkus() {
        List<SkuInfo> chooseSkus = new ArrayList<>();
        for (String skuId : mSkuIds) {
            for (SkuInfo skus : mSkus) {
                if (skuId.equals(skus.skuId)) {
                    chooseSkus.add(skus);
                    break;
                }
            }
        }
        return chooseSkus;
    }

    public List<String> getAllSkuIds() {
        List<String> allSkuIds = new ArrayList<>();
        for (SkuInfo skus : mSkus) {
            allSkuIds.add(skus.skuId);
        }
        return allSkuIds;
    }

    public void addSkus(List<SkuInfo> skus) {
        mSkus.addAll(skus);
    }

    public void removeSkus(List<SkuInfo> skus) {
        mSkus.removeAll(skus);
    }

    @OnClick(R.id.tvConfirm)
    public void onViewClicked() {
        Logger.d("mSkuIds:" + mSkuIds.toString());
        if (mIsCreate) {
            EventBus.getDefault().post(new EventMessage(Event.addLiveProduct, getChooseSkus()));
            finish();
            return;
        }
        APIManager.startRequest(mService.addLiveSkuRelations(new AddLiveBody(mLiveId, null, null, null, null, null, mSkuIds)),
                new BaseRequestListener<Object>(this) {
                    @Override
                    public void onSuccess(Object result) {
                        ToastUtil.success("添加成功");
                        EventBus.getDefault().post(new EventMessage(Event.addLiveProduct));
                        finish();
                    }
                }, this);
    }

    public static void start(Context context, String liveId, boolean shopkeeper) {
        start(context, liveId, shopkeeper, false, new ArrayList<String>());
    }

    public static void start(Context context, String liveId, boolean shopkeeper, boolean isCreate, ArrayList<String> skuIds) {
        Intent intent = new Intent(context, ChooseLiveGoodsActivity.class);
        intent.putExtra("liveId", liveId);
        intent.putExtra("shopkeeper", shopkeeper);
        intent.putExtra("isCreate", isCreate);
        intent.putExtra("skuIds", skuIds);
        context.startActivity(intent);
    }

}
