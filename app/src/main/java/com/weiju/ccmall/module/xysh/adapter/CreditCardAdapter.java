package com.weiju.ccmall.module.xysh.adapter;


import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.ApplyCreditCardItem;
import com.weiju.ccmall.shared.util.FrescoUtil;


/**
 * @author chenyanming
 * @time 2019/12/17 on 17:12
 * @desc
 */
public class CreditCardAdapter extends BaseQuickAdapter<ApplyCreditCardItem, BaseViewHolder> {
    public CreditCardAdapter() {
        super(R.layout.item_credit_card);
    }

    @Override
    protected void convert(BaseViewHolder helper, ApplyCreditCardItem item) {
        helper.setText(R.id.tvTitle, item.name);
        helper.setText(R.id.tvDesc, item.description);
        SimpleDraweeView iv = helper.getView(R.id.ivAvatar);
        FrescoUtil.setImage(iv,  item.imgUrlPath);
    }
}
