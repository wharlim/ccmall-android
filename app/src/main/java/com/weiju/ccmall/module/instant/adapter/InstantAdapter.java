package com.weiju.ccmall.module.instant.adapter;

import android.support.annotation.Nullable;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.InstantData;
import com.weiju.ccmall.shared.component.InstantProgressBar;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import java.util.List;

public class InstantAdapter extends BaseQuickAdapter<InstantData.Product, BaseViewHolder> {

    public InstantAdapter(@Nullable List<InstantData.Product> data) {
        super(R.layout.item_instant_product, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, InstantData.Product item) {
        SimpleDraweeView ivImage = helper.getView(R.id.ivImg);
        FrescoUtil.setImgMask(ivImage, item.thumbUrl, item.status, item.stock);

        helper.setText(R.id.tvTitle, item.skuName);
        helper.setText(R.id.tvRetailPrice, ConvertUtil.centToCurrency(helper.getConvertView().getContext(), item.retailPrice));
        helper.setText(R.id.tvMarketPrice, ConvertUtil.centToCurrency(helper.getConvertView().getContext(), item.marketPrice));
        helper.setText(R.id.tvCCM, String.format("奖%s%%算率", item.countRateExc));

        TextView tvMarketPrice = helper.getView(R.id.tvMarketPrice);
        TextViewUtil.addThroughLine(tvMarketPrice);

        long startTime = 0;
        if (!StringUtils.isEmpty(item.sellBegin)) {
            startTime = TimeUtils.string2Millis(item.sellBegin);
        }
        long endTime = 0;
        if (!StringUtils.isEmpty(item.sellEnd)) {
            endTime = TimeUtils.string2Millis(item.sellEnd);
        }
        long currentTime = System.currentTimeMillis();
        TextView tvGo = helper.getView(R.id.tvGo);
        if (currentTime < startTime) {
            tvGo.setText("即将开抢");
            tvGo.setSelected(false);
        } else if (currentTime > endTime) {
            tvGo.setText("已结束");
            tvGo.setSelected(true);
        } else {
            tvGo.setSelected(false);
            tvGo.setText("马上抢");
            helper.addOnClickListener(R.id.tvGo);
        }

        InstantProgressBar progressBar = helper.getView(R.id.progressBar);
        if (item.quantity == 0) {
            progressBar.setProgress(0);
        } else if (item.saleCount >= item.quantity || item.stock == 0) {
            LogUtils.e("总数" + item.quantity + "  当前销量" + item.saleCount);
            progressBar.setProgress(100);
            if (!tvGo.isSelected()) {
                tvGo.setText("抢光了");
                tvGo.setSelected(true);
            }
        } else {
            progressBar.setProgress(100 * item.saleCount / item.quantity);
        }

    }
}
