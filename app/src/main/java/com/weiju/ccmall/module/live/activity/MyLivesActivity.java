package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.entity.VerifyStatusEntity;
import com.weiju.ccmall.module.live.fragment.OpeningLiveTypeSelectDialogFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.glide.transformations.ColorFilterTransformation;

/**
 * 我的直播间
 */
public class MyLivesActivity extends BaseActivity {
    @BindView(R.id.ivAvatarBg)
    ImageView ivAvatarBg;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.layoutTopTitle)
    FrameLayout layoutTopTitle;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvFollow)
    TextView tvFollow;
    @BindView(R.id.tvFans)
    TextView tvFans;
    @BindView(R.id.tvLikeNum)
    TextView tvLikeNum;
    @BindView(R.id.avatarIv)
    SimpleDraweeView avatarIv;
    @BindView(R.id.tvLiveLevel)
    TextView tvLiveLevel;
    @BindView(R.id.ivProvider)
    ImageView mIvProvider;
    @BindView(R.id.ivSwitchToShop)
    ImageView mIvSwitchToShop;

    private LiveUser liveUser;
    private User loginUser;

    ILiveStoreService iLiveStoreService;

    public static void start(Context context, LiveUser liveUser, boolean singleTask) {
        Intent intent = new Intent(context, MyLivesActivity.class);
        intent.putExtra("liveUser", liveUser);
        if (singleTask) {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_lives);
        ButterKnife.bind(this);
        initStatusBar();
        liveUser = (LiveUser) getIntent().getSerializableExtra("liveUser");
        loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser == null) {
            finish();
            return;
        }
        initLiveUser(liveUser, loginUser);
        iLiveStoreService = ServiceManager.getInstance().createService(ILiveStoreService.class);
    }

    private void initLiveUser(LiveUser liveUser, User loginUser) {
        ivAvatarBg.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                loadBg(Glide.with(MyLivesActivity.this)
                        .load(Uri.parse(liveUser.headImage))
                );
                ivAvatarBg.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });
        FrescoUtil.setImageSmall(avatarIv, liveUser.headImage);
        tvName.setText(loginUser.nickname);
        tvFollow.setText(liveUser.followNum + "");
        tvFans.setText(liveUser.fansNum + "");
        tvLikeNum.setText(liveUser.likeNum + "");
        if (TextUtils.isEmpty(liveUser.liveLevelStr)) {
            tvLiveLevel.setVisibility(View.GONE);
        } else {
            tvLiveLevel.setText(liveUser.liveLevelStr);
        }

        if (liveUser.shopkeeper){
            mIvProvider.setVisibility(View.GONE);
            mIvSwitchToShop.setVisibility(View.VISIBLE);
        }else {
            mIvProvider.setVisibility(View.VISIBLE);
            mIvSwitchToShop.setVisibility(View.GONE);
        }
    }

    private void initStatusBar() {
        View decorView = getWindow().getDecorView();
        int option = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
        decorView.setSystemUiVisibility(option);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void loadBg(RequestBuilder<Drawable> rb) {
        rb.apply(RequestOptions.placeholderOf(R.drawable.img_create_live_room_bg))
                .apply(RequestOptions.bitmapTransform(new MultiTransformation<>(
                        new CenterCrop(),
                        new ColorFilterTransformation(Color.parseColor("#BA000000"))
                )))
                .into(new SimpleTarget<Drawable>(ivAvatarBg.getMeasuredWidth(), ivAvatarBg.getMeasuredHeight()) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        ivAvatarBg.setImageDrawable(resource);
                    }

                    @Override
                    public void onLoadStarted(@Nullable Drawable placeholder) {
                        ivAvatarBg.setImageDrawable(placeholder);
                    }
                });
    }

    @OnClick({R.id.back, R.id.llCreateLive, R.id.llLiveNotice, R.id.llMyLive, R.id.llOpeningLive,R.id.ivProvider, R.id.ivSwitchToShop})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.llCreateLive:
                CreatLiveRoomActivity.start(this, liveUser.shopkeeper);
                break;
            case R.id.llLiveNotice:
                MyHistoryLivesActivity.start(this, true);
                break;
            case R.id.llMyLive:
                MyHistoryLivesActivity.start(this, false);
                break;
            case R.id.llOpeningLive:
                OpeningLiveTypeSelectDialogFragment.newInstance().show(getSupportFragmentManager(),
                        "OpeningLiveTypeSelectDialogFragment");
                break;
            case R.id.ivProvider:
                APIManager.startRequest(iLiveStoreService.getVerifyStatus(), new BaseRequestListener<VerifyStatusEntity>(this) {
                    @Override
                    public void onSuccess(VerifyStatusEntity result) {
                        super.onSuccess(result);
                        switch (result.liveStoreBean.applyForType) {
                            case VerifyStatusEntity.STATUS_NOT:
                                ProviderApplyProtocolActivity.start(MyLivesActivity.this);
                                break;
                            case VerifyStatusEntity.STATUS_WAIT:
                                ProviderApplyStatusActivity.start(MyLivesActivity.this);
                                break;
                            case VerifyStatusEntity.STATUS_FAIL:
                                ProviderApplyStatusActivity.start(MyLivesActivity.this, result.liveStoreBean.mchInId, result.liveStoreBean.reason);
                                break;
                            case VerifyStatusEntity.STATUS_SUCCESS:
                                ProviderApplySuccessActivity.start(MyLivesActivity.this);
                                break;
                        }

                    }
                }, this);
                break;
            case R.id.ivSwitchToShop:
                ProviderShopActivity.start(this);
                finish();
                break;
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void transferHandler(EventMessage message) {
        switch (message.getEvent()) {
            case ApplyPaySuccess:
                ProviderShopActivity.start(this);
                finish();
                break;
        }
    }

}
