package com.weiju.ccmall.module.xysh.activity.realname_auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.activity.XinYongUserActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RealNameAuthResultActivity extends BaseActivity {
    @BindView(R.id.iv_auth_result_icon)
    ImageView ivAuthResultIcon;
    @BindView(R.id.tv_auth_result_txt)
    TextView tvAuthResultTxt;
    @BindView(R.id.tv_complete)
    TextView tvComplete;

    private boolean mRealNameAuthResult;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realname_auth_result);
        ButterKnife.bind(this);
        setLeftBlack();
        setTitle("结果页");

        boolean result = getIntent().getBooleanExtra("result", false);
        if (result) {
            ivAuthResultIcon.setImageResource(R.drawable.ic_xinyong_add_success);
            tvAuthResultTxt.setText("认证成功");
            tvComplete.setText("完成");
        } else {
            ivAuthResultIcon.setImageResource(R.drawable.ic_xinyong_add_fail);
            tvAuthResultTxt.setText("认证失败");
            tvComplete.setText("重新认证");
        }
        mRealNameAuthResult = result;
    }

    public static void start(Context context, boolean success) {
        Intent intent = new Intent(context, RealNameAuthResultActivity.class);
        intent.putExtra("result", success);
        context.startActivity(intent);
    }

    @OnClick(R.id.tv_complete)
    public void onComplete() {
        if (mRealNameAuthResult) {
            Intent intent = new Intent(this, XinYongUserActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("hasRealNameAuth", mRealNameAuthResult);
            startActivity(intent);
        } else {
            finish();
        }
    }
}
