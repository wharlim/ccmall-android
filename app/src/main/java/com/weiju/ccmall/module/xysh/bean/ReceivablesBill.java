package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ReceivablesBill implements Serializable {

    /**
     * tranId : 2019051423373618200612
     * tmSmp : 1559274909000
     * payTime : 2019-05-31 11:55:09
     * cnlNo : ZzlLocal
     * channelName : 积分通道Y
     * tranSts : 3
     * txnAmt : 200
     * txnRate : 0.5
     * feeAmt : 3
     * txnCardNo : 4392250041436782
     * clrCardNo : 6217921003904060
     * retMsg : 回调成功！(订单失败)
     */

    @SerializedName("tranId")
    public String tranId;
    @SerializedName("tmSmp")
    public double tmSmp;
    @SerializedName("payTime")
    public String payTime;
    @SerializedName("cnlNo")
    public String cnlNo;
    @SerializedName("channelName")
    public String channelName;
    @SerializedName("tranSts")
    public String tranSts;
    public String getStateText() {
        if ("1".equals(tranSts)) {
            return "执行中";
        } else if ("2".equals(tranSts)) {
            return "交易成功";
        } else if ("0".equals(tranSts)){
            return "交易失败";
        } else {
            return "交易失败";
        }
    }
    @SerializedName("txnAmt")
    public float txnAmt;
    @SerializedName("txnRate")
    public float txnRate;
    @SerializedName("feeAmt")
    public float feeAmt;
    @SerializedName("txnCardNo")
    public String txnCardNo;
    @SerializedName("clrCardNo")
    public String clrCardNo;
    @SerializedName("clrCardName")
    public String clrCardName;
    @SerializedName("retMsg")
    public String retMsg;
}
