package com.weiju.ccmall.module.live.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * @author Ben
 * @date 2020/4/18.
 */
public class UploadProductEntity implements Serializable {

    /**
     * productName : 男装
     * categoryId : b4059a00ccd3425db80f686aa4e85839
     * isShippingFree : 1
     * freeQuantity : 5
     * comRatio : 20
     * skuProValOne : 黄色;白色
     * skuProValTwo : 大码;小码
     * bannerImage : ["https://static.create-chain.net/ccmall/b6/33/eb/9be3c19e1ead436b85c28224c1edd94b.jpg","https://static.create-chain.net/ccmall/72/d5/b7/7244f6f581eb4ab8888952f0b1824c01.jpg","https://static.create-chain.net/ccmall/87/f5/f5/a9c09f494f724f599c22b0d9f191e8e7.jpg","https://static.create-chain.net/ccmall/23/89/2e/55fd4b51021c4a99ab27afa99e9d2d61.jpg"]
     * detailImage : ["https://static.create-chain.net/ccmall/b6/33/eb/9be3c19e1ead436b85c28224c1edd94b.jpg","https://static.create-chain.net/ccmall/72/d5/b7/7244f6f581eb4ab8888952f0b1824c01.jpg","https://static.create-chain.net/ccmall/87/f5/f5/a9c09f494f724f599c22b0d9f191e8e7.jpg","https://static.create-chain.net/ccmall/23/89/2e/55fd4b51021c4a99ab27afa99e9d2d61.jpg"]
     * skuBeans : [{"retailPrice":4150,"marketPrice":5500,"weight":5,"stock":45},{"retailPrice":7840,"marketPrice":9900,"weight":8,"stock":99},{"retailPrice":3443,"marketPrice":3434,"weight":2,"stock":34},{"retailPrice":4324,"marketPrice":1212,"weight":84,"stock":32}]
     */

    @SerializedName("productName")
    public String productName;
    @SerializedName("categoryId")
    public String categoryId;
    @SerializedName("isShippingFree")
    public int isShippingFree;  //0：不包邮；1：包邮
    @SerializedName("freeQuantity")
    public String freeQuantity;
    @SerializedName("comRatio")
    public String comRatio;

    @SerializedName("info")
    public String info;
    @SerializedName("platformComRatio")
    public int platformComRatio;
//    @SerializedName("skuProValOne")
//    public String skuProValOne;
//    @SerializedName("skuProValTwo")
//    public String skuProValTwo;
    @SerializedName("bannerImage")
    public List<String> bannerImage;
    @SerializedName("detailImage")
    public List<String> detailImage;
    @SerializedName("skuBeans")
    public List<SpecEntity> skuBeans;
    /**
     * specs : 自然;美白
     * productId : ac1d4659b72944c3b6a3d150d4dbaf6d
     * categoryName : 身体洗护
     */

    @SerializedName("specs")
    public String specs;
    @SerializedName("productId")
    public String productId;
    @SerializedName("categoryName")
    public String categoryName;

    @SerializedName("color")
    public String color;
    @SerializedName("size")
    public String size;



}
