package com.weiju.ccmall.module.pickUp.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

/**
 * @author chenyanming
 * @time 2019/8/2 on 16:54
 * @desc ${TODD}
 */
public class PickUpProductAdapter extends BaseQuickAdapter<SkuInfo, BaseViewHolder> {
    public PickUpProductAdapter() {
        super(R.layout.item_collect);
    }

    @Override
    protected void convert(BaseViewHolder helper, SkuInfo item) {
        helper.setVisible(R.id.itemTrashBtn, false);
        helper.setText(R.id.itemTitleTv, item.name);
        helper.setText(R.id.itemMoneyTv, MoneyUtil.centToYuan¥StrNoZero(item.retailPrice));
        helper.setText(R.id.itemMoneyTv2, MoneyUtil.centToYuan¥StrNoZero(item.marketPrice));
        TextViewUtil.addThroughLine(helper.getView(R.id.itemMoneyTv2));
        helper.setText(R.id.tvCount, String.format("销量：%s", item.totalSaleCount));
        helper.setVisible(R.id.ivBanjia, item.isBanjia());
        FrescoUtil.setSkuImgSmallMask(helper.getView(R.id.itemThumbIv), item);
        helper.setText(R.id.tvCCM, String.format("奖%s%%算率", item.countRateExc));
    }
}
