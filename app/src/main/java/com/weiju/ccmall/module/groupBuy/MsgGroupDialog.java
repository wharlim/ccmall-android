package com.weiju.ccmall.module.groupBuy;

import com.weiju.ccmall.shared.bean.Product;
import com.weiju.ccmall.shared.bean.SkuInfo;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/11/2.
 */
public class MsgGroupDialog {
    private SkuInfo mSkuInfo;
    private Product mProduct;

    public MsgGroupDialog(SkuInfo skuInfo, Product product) {
        mSkuInfo = skuInfo;
        mProduct = product;
    }

    public SkuInfo getSkuInfo() {
        return mSkuInfo;
    }

    public void setSkuInfo(SkuInfo skuInfo) {
        mSkuInfo = skuInfo;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }
}
