package com.weiju.ccmall.module.blockchain.beans;

import com.google.gson.annotations.SerializedName;

public class TransferableCCM {

    /**
     * coin : 2912.1632
     */

    @SerializedName("coin")
    public String coin;
}
