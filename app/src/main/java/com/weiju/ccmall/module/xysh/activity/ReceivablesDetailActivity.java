package com.weiju.ccmall.module.xysh.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.ReceivablesBill;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author chenyanming
 * @time 2019/12/16 on 18:06
 * @desc
 */
public class ReceivablesDetailActivity extends BaseActivity {

    @BindView(R.id.tvTransferAmt)
    TextView tvTransferAmt;
    @BindView(R.id.tvReceiveAmt)
    TextView tvReceiveAmt;
    @BindView(R.id.tvServiceCharge)
    TextView tvServiceCharge;
    @BindView(R.id.tvReceiveCard)
    TextView tvReceiveCard;
    @BindView(R.id.tvPayCard)
    TextView tvPayCard;
    @BindView(R.id.tvChannel)
    TextView tvChannel;
    @BindView(R.id.tvOrderNo)
    TextView tvOrderNo;
    @BindView(R.id.tvTransferTime)
    TextView tvTransferTime;
    @BindView(R.id.tvTransferState)
    TextView tvTransferState;
    private ReceivablesBill mReceivablesBill;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receivable_detail);
        ButterKnife.bind(this);
        setTitle("收款账单详情");
        setLeftBlack();
        mReceivablesBill = (ReceivablesBill) getIntent().getSerializableExtra("ReceivablesBill");
        initView();
    }

    private void initView() {
        tvTransferAmt.setText(String.format("¥%.2f", mReceivablesBill.txnAmt));
        tvReceiveAmt.setText(String.format("¥%.2f", mReceivablesBill.txnAmt - mReceivablesBill.feeAmt));
        tvServiceCharge.setText(String.format("%.2f", mReceivablesBill.feeAmt));
        tvReceiveCard.setText(BankUtils.formatBankCardNo(mReceivablesBill.clrCardNo));
        tvPayCard.setText(BankUtils.formatBankCardNo(mReceivablesBill.txnCardNo));
        tvChannel.setText(mReceivablesBill.channelName);
        tvOrderNo.setText(mReceivablesBill.tranId);
        tvTransferTime.setText(mReceivablesBill.payTime);
        tvTransferState.setText(mReceivablesBill.getStateText());
    }
}
