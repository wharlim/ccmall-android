package com.weiju.ccmall.module.live.fragment;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveData;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class LiveDetailOverflowMenuFragment extends BottomSheetDialogFragment {

    Unbinder unbinder;
    @BindView(R.id.ivUpDown)
    ImageView ivUpDown;
    @BindView(R.id.tvUpDown)
    TextView tvUpDown;
    @BindView(R.id.llUpDown)
    LinearLayout llUpDown;
    @BindView(R.id.llCpPwd)
    LinearLayout llCpPwd;
    @BindView(R.id.llClPwd)
    LinearLayout llClPwd;

    private ILiveService service = ServiceManager.getInstance().createService(ILiveService.class);

    private LiveRoom liveRoom;
    private LiveData liveData;

    private Callback mCallBack;

    public static LiveDetailOverflowMenuFragment newInstance(LiveRoom liveRoom, LiveData liveData) {
        Bundle args = new Bundle();
        args.putSerializable("liveRoom", liveRoom);
        args.putSerializable("liveData", liveData);
        LiveDetailOverflowMenuFragment fragment = new LiveDetailOverflowMenuFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        liveRoom = (LiveRoom) getArguments().getSerializable("liveRoom");
        liveData = (LiveData) getArguments().getSerializable("liveData");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_live_detail_over_menu, container, false);
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getDialog().findViewById(R.id.design_bottom_sheet).setBackgroundColor(Color.TRANSPARENT);
    }

    private void initView() {
        if (liveRoom.onlineStatus == 1) {
            tvUpDown.setText("下架");
            ivUpDown.setImageResource(R.drawable.ic_live_detail_down);
        } else {
            tvUpDown.setText("上架");
            ivUpDown.setImageResource(R.drawable.ic_live_detail_up);
        }
        llCpPwd.setVisibility(liveData.hasLivePasswordDecode?View.VISIBLE:View.GONE);
        llClPwd.setVisibility(liveRoom.hasPassword()?View.VISIBLE:View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.llUpDown, R.id.llCpPwd, R.id.llClPwd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llUpDown:
                dismiss();
                updateLiveOnlineStatus(liveRoom, Math.abs(liveRoom.onlineStatus - 1));
                break;
            case R.id.llCpPwd:
                dismiss();
                cpPwd(liveData.livePasswordDecode);
                break;
            case R.id.llClPwd:
                dismiss();
                clearPwd();
                break;
        }
    }

    private void updateLiveOnlineStatus(LiveRoom room, int onlineStatus) {
        final WJDialog dialog = new WJDialog(getContext());
        dialog.show();
        dialog.setTitle(onlineStatus == 1?"上架回放": "下架回放");
        dialog.setContentText(onlineStatus == 1?"上架后，观众可观看此回放，请确认":"下架后，观众不可观看此回放，请确认");
        dialog.setCancelText("取消");
        dialog.setConfirmText("确定");
        dialog.setOnConfirmListener((v) -> {
            dialog.dismiss();
            ToastUtil.showLoading(getContext());
            APIManager.startRequest(service.updateLiveBroadcast(
                    room.liveId, onlineStatus
            ), new BaseRequestListener<Object>() {
                @Override
                public void onSuccess(Object result) {
                    ToastUtil.hideLoading();
                    room.onlineStatus = onlineStatus;
                    if (onlineStatus == 1) {
                        ToastUtil.success("上架成功！");
                    } else {
                        ToastUtil.success("下架成功！");
                    }
                    EventBus.getDefault().post(new EventMessage(Event.liveRoomChange, liveRoom));
                }

                @Override
                public void onError(Throwable e) {
                    ToastUtil.hideLoading();
                }
            }, getContext());
        });
    }

    private void cpPwd(String pwd) {
        ClipboardManager clipboardManager = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
        clipboardManager.setPrimaryClip(ClipData.newPlainText("", pwd));
        ToastUtil.success(String.format("直播间密码:%s，已复制到剪贴板", pwd));
    }

    private void clearPwd() {
        final WJDialog dialog = new WJDialog(getContext());
        dialog.show();
        dialog.setTitle("清除密码");
        dialog.setContentText("清除后，观众观看此回放不再需要密码");
        dialog.setCancelText("取消");
        dialog.setConfirmText("确定");
        dialog.setOnConfirmListener((v) -> {
            dialog.dismiss();
            if (mCallBack != null) {
                mCallBack.onClearPwd();
            }
        });
    }

    public interface Callback {
        void onClearPwd();
    }

    public void setCallback(Callback cb) {
        mCallBack = cb;
    }
}
