package com.weiju.ccmall.module.xysh.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.HelpCourse;

/**
 * @author chenyanming
 * @time 2019/12/16 on 14:32
 * @desc
 */
public class HelpItemAdapter extends BaseQuickAdapter<HelpCourse, BaseViewHolder> {
    public HelpItemAdapter() {
        super(R.layout.item_help_item);
    }

    @Override
    protected void convert(BaseViewHolder helper, HelpCourse item) {
        helper.setText(R.id.tvTitle, item.title);
        helper.setVisible(R.id.line, item.showLine);
    }
}
