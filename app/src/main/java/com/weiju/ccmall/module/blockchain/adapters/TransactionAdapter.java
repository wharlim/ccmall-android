package com.weiju.ccmall.module.blockchain.adapters;

import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.beans.TransactionItem;
import com.weiju.ccmall.module.blockchain.utils.BlockChainUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.VH> {

    ArrayList<TransactionItem> data;
    // 传递数据
    public TransactionAdapter(ArrayList<TransactionItem> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return VH.newInstance(LayoutInflater.from(viewGroup.getContext()), viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull VH vh, int i) {
        vh.bindView(data.get(i));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class VH extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_transaction_type)
        TextView tvTransactionType;
        @BindView(R.id.tv_transaction_time)
        TextView tvTransactionTime;
        @BindView(R.id.tv_value)
        TextView tvValue;
        @BindView(R.id.iv_up_down)
        ImageView ivUpDown;

        Resources resources;

        VH(View itemView) {
            super(itemView);
            resources = itemView.getResources();
            ButterKnife.bind(this, itemView);
        }

        void bindView(TransactionItem transactionItem) {
            tvTransactionType.setText("类型: "+transactionItem.typeStr);
            tvTransactionTime.setText("时间: " + transactionItem.createTime);
            tvValue.setText(transactionItem.getAmount());
            if (transactionItem.isAmountNegative()) {
                ivUpDown.setImageResource(R.mipmap.icon_detail_down);
                tvValue.setTextColor(resources.getColor(R.color.text_down));
            } else {
                ivUpDown.setImageResource(R.mipmap.icon_detail_up);
                tvValue.setTextColor(resources.getColor(R.color.text_up));
            }
        }

        public static VH newInstance(LayoutInflater inflater, ViewGroup parent) {
            View view = inflater.inflate(R.layout.item_transaction_detail, parent, false);
            return new VH(view);
        }
    }
}
