package com.weiju.ccmall.module.xysh.fragment

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.TextView
import com.bumptech.glide.Glide
import com.qmuiteam.qmui.util.QMUIStatusBarHelper
import com.tbruyelle.rxpermissions.RxPermissions
import com.weiju.ccmall.R
import com.weiju.ccmall.module.xysh.XyshService
import com.weiju.ccmall.module.xysh.helper.UploadHelper
import com.weiju.ccmall.shared.basic.AgentFActivity
import com.weiju.ccmall.shared.basic.AgentFragment
import com.weiju.ccmall.shared.util.PermissionsUtils
import com.weiju.ccmall.shared.util.SessionUtil
import com.weiju.ccmall.shared.util.ToastUtil
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.PicassoEngine
import com.zhihu.matisse.internal.entity.CaptureStrategy
import kotlinx.android.synthetic.main.fragment_picture.*
import top.zibin.luban.Luban
import top.zibin.luban.OnCompressListener
import java.io.File

/**
 * Created by 林先生_lcy.
 * User: lcy
 * Date: 2019/4/2
 * Time: 下午6:50
 */
class PictureFragment : AgentFragment() {

    var picture: String? = null

    override fun layoutId(): Int {
        return R.layout.fragment_picture
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        QMUIStatusBarHelper.setStatusBarDarkMode(activity)
        QMUIStatusBarHelper.translucent(activity, resources.getColor(R.color.red))
        val toolbar = activity?.findViewById<Toolbar>(R.id.toolbar)
        toolbar?.setBackgroundColor(resources.getColor(R.color.red))
        toolbar?.setNavigationIcon(R.drawable.ioco)
        toolbar?.findViewById<TextView>(R.id.toolbarTitle)?.setTextColor(-1)
        super.onViewCreated(view, savedInstanceState)
        activity?.title = "上传照片"
        picture = SessionUtil.getInstance().getString("livingbody")
        Glide.with(this).load(picture).into(ID_face)
        ID_face.setOnClickListener {
            showDiagram()
        }
        next.setOnClickListener {
            if (picture == null)
                ToastUtil.error("请选择本人照片")
            else {
                val util = SessionUtil.getInstance()
                util.putString("livingbody", picture)
//                request(::upSuccess, service2(XyshService::class.java).userPersonRelauth(
//                        SessionUtil.getInstance().loginUser!!.phone,
//                        util.getString("usrName"),
//                        util.getString("idNo"),
//                        SessionUtil.getInstance().oAuthToken,
//                        UploadHelper.getPart("idhandlepic", picture!!),
//                        UploadHelper.getPart("livingbody", picture!!),
//                        UploadHelper.getPart("idfrontpic", util.getString("idfrontpic")),
//                        UploadHelper.getPart("idbackpic", util.getString("idbackpic"))))
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == 1015) {
            activity?.setResult(resultCode)
            activity?.finish()
        } else if (data != null) {
            val array = data.getSerializableExtra("extra_result_selection_path") as? ArrayList<String>?
            if (array != null && array.isNotEmpty()) {
                if (requestCode == 1100) {
                    scale(array[0])
                    Glide.with(this).load(array[0]).into(ID_face)
                }
            }
        }
    }

    private fun selectImg(requestCode: Int) {
        val rxPermissions = RxPermissions(activity!!)
        rxPermissions.request(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        ).subscribe { aBoolean ->
            if (aBoolean!!) {
                Matisse.from(this)
                        .choose(MimeType.ofImage())
                        .countable(true)
                        .maxSelectable(1)
                        .capture(true)
                        .captureStrategy(CaptureStrategy(true, "com.weiju.ccmall.fileProvider"))
                        .imageEngine(PicassoEngine())
                        .forResult(requestCode)
            } else {
                PermissionsUtils.goPermissionsSetting(activity)
                ToastUtil.error("无法获得必要的权限")
            }
        }
    }

    /**
     * 压缩
     */
    private fun scale(path: String) {
        Luban.with(context?.applicationContext)
                .load(File(path))                              // 忽略不压缩图片的大小
                .setCompressListener(object : OnCompressListener {
                    override fun onSuccess(file: File?) {
                        picture = file?.absolutePath
                        hideLoading()
                    }

                    override fun onError(e: Throwable?) {
                        hideLoading()
                    }

                    override fun onStart() {
                        showLoading()
                    } //设置回调

                }).launch();    //启动压缩
    }

    private fun upSuccess(postCode: Int, data: Any?) {
        AgentFActivity.start2(this, BankCardFragment::class.java)
    }

    private fun showHideDiagram(isVisibility: Int) {
        diagram_bg.visibility = isVisibility
        diagram.visibility = isVisibility
        diagram_text.visibility = isVisibility
    }

    private fun showDiagram() {
        showHideDiagram(View.VISIBLE)
        diagram_bg.setOnClickListener { }
        diagram_text.setOnClickListener {
            showHideDiagram(View.GONE)
            selectImg(1100)
        }
    }

}
