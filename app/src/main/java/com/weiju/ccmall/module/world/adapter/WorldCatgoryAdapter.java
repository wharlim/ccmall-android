package com.weiju.ccmall.module.world.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.world.entity.CatgoryItemEntity;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

import java.util.List;

/**
 * @author Ben
 * @date 2020/4/10.
 */
public class WorldCatgoryAdapter extends BaseQuickAdapter<CatgoryItemEntity, BaseViewHolder> {
    public WorldCatgoryAdapter(@Nullable List<CatgoryItemEntity> data) {
        super(R.layout.item_world_catgory, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, CatgoryItemEntity product) {
        FrescoUtil.setImageSmall(helper.getView(R.id.sdvItemThumb), product.mainImagesUrl);
        helper.setText(R.id.tvItemTitle, product.skuName)
                .setText(R.id.tvItemPrice, MoneyUtil.centToYuan¥Str(product.price))
                .setText(R.id.tvCCM, String.format("奖%s%%算率", product.countRateExc));
    }

}
