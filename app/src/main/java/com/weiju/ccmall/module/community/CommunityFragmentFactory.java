package com.weiju.ccmall.module.community;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.blankj.utilcode.utils.SizeUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.newRetail.activity.CommunityActivity;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.MsgFViewPager;
import com.weiju.ccmall.shared.util.SessionUtil;


/**
 * @author Stone
 * @time 2018/3/11  18:12
 * @desc 社区的创建fragment的factory
 */

public class CommunityFragmentFactory extends FragmentFactory {
    private Boolean[] visiable = new Boolean[]{true, true, false};
    private Boolean[] needLogin = new Boolean[]{false, false, true};
    private int[] clickAction = new int[]{MsgFViewPager.ACTION_ADD_CLICK_LINK, MsgFViewPager.ACTION_ADD_CLICK_VIDEO, -1};

    public CommunityFragmentFactory(Context context) {
        super(context);
    }

    @Override
    public String[] getTabTitle() {
        String[] tabs = new String[CommunityActivity.plates.size()];
        for (int i = 0; i < CommunityActivity.plates.size(); i++) {
            tabs[i] = CommunityActivity.plates.get(i).title;
        }
//        return mContext.getResources().getStringArray(R.array.Message_Title_Arr);
        return tabs;
    }

    @Override
    public Fragment getFragment(int position, Fragment fragment) {
        fragment = CommunityCourseFragment.newInstance(position);
        return fragment;
    }

    @Override
    public boolean getVisiableAdd(int position) {
//        if (position == 0) {
//            User loginUser = SessionUtil.getInstance().getLoginUser();
//            if (loginUser != null && loginUser.isPower == 1) {
//                return true;
//            } else {
//                return false;
//            }
//        }
//        return visiable[position];
        return false;
    }

    @Override
    public boolean getNeedLogin(int position) {
        return false;
    }


    @Override
    public int getTabBottomLineWidth() {
        return SizeUtils.dp2px(100);
    }

    @Override
    public int getAddClickAction(int position) {
        return clickAction[position];
    }
}
