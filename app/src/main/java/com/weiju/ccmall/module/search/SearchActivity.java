package com.weiju.ccmall.module.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.category.adapter.ProductListAdapter;
import com.weiju.ccmall.module.jkp.IJkpProductService;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.JkpSearchResult;
import com.weiju.ccmall.shared.bean.Keyword;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.page.PageType;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

public class SearchActivity extends BaseActivity implements PageManager.RequestListener {

    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;

    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.keywordsLayout)
    protected ScrollView mKeywordsLayout;

    @BindView(R.id.hotKeywordsLayout)
    protected FlexboxLayout mHotKeywordsLayout;

    @BindView(R.id.cancelBtn)
    protected TextView mCancelBtn;
    @BindView(R.id.cleanBtn)
    protected ImageView mCleanBtn;
    @BindView(R.id.keywordEt)
    protected EditText mKeywordEt;
    @BindView(R.id.noDataLayout)
    View noDataLayout;
    @BindView(R.id.noDataLabel)
    TextView noDataLabel;

    private PageManager mPageManager;
    private ProductListAdapter mAdapter;
    private IProductService mProductService;
    private IJkpProductService mJkpProductService;
    private String mKeyword = "";
    private int mType;
    private String classifacationType = "";

    private ILiveService mLiveService;
    private boolean mAddProduct;
    private String mLiveId;

    private PageType pageType;

    {
        pageType = PageType.HOME;
        mJkpProductService = ServiceManager.getInstance().createService(IJkpProductService.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getParam();
        ButterKnife.bind(this);
        mProductService = ServiceManager.getInstance().createService(IProductService.class);
        mLiveService = ServiceManager.getInstance().createService(ILiveService.class);
        classifacationType = getIntent().getStringExtra(Const.CLASSIFICATION_TYPE);
        loadHotKeywords();
        mAdapter = new ProductListAdapter(this);
        mAdapter.isAddProduct(mAddProduct);
        mAdapter.setAddLiveProductListener(mAddLiveProductListener);
        mAdapter.setColumns(mType);
        mRecyclerView.setAdapter(mAdapter);
        try {
            mPageManager = PageManager.getInstance()
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .setRecyclerView(mRecyclerView)
                    .setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false))
                    .setRequestListener(this)
                    .build(this);
        } catch (PageManager.PageManagerException e) {
            e.printStackTrace();
        }
        initLayout();
    }

    private ProductListAdapter.AddLiveProductListener mAddLiveProductListener = new ProductListAdapter.AddLiveProductListener() {
        @Override
        public void addLiveProdcut(SkuInfo skuInfo) {
            APIManager.startRequest(mLiveService.addLiveSkuRelation(mLiveId, skuInfo.skuId),
                    new BaseRequestListener<Object>(SearchActivity.this) {
                        @Override
                        public void onSuccess(Object result) {
                            ToastUtil.success("添加成功");
                            EventBus.getDefault().post(new EventMessage(Event.addLiveProduct));
                        }
                    }, SearchActivity.this);

        }
    };

    void initLayout() {
        if (!mKeyword.isEmpty()) {
            mKeywordsLayout.setVisibility(View.GONE);
            mRefreshLayout.setVisibility(View.VISIBLE);
            mKeywordEt.setText(mKeyword);
            mPageManager.onRefresh();
        } else {
            mKeywordsLayout.setVisibility(View.VISIBLE);
            mRefreshLayout.setVisibility(View.GONE);
            mKeywordEt.requestFocus();
        }

        mKeywordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                mCleanBtn.setVisibility(charSequence.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mKeywordEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH || (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    search(textView.getText().toString().trim());
                    return true;
                }
                return false;
            }
        });

        mKeywordEt.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    mInputMethodManager.showSoftInput(mKeywordEt, InputMethodManager.SHOW_IMPLICIT);
                } else {
                    mInputMethodManager.hideSoftInputFromWindow(mKeywordEt.getWindowToken(), 0);
                }
            }
        });
        noDataLabel.setText("这里空空的，什么都没有~");
    }

    private void loadHotKeywords() {
        APIManager.startRequest(mProductService.getHotKeywords(), new BaseRequestListener<List<Keyword>>(this) {
            @Override
            public void onSuccess(List<Keyword> keywords) {
                mHotKeywordsLayout.removeAllViews();
                FlexboxLayout.LayoutParams layoutParams = new FlexboxLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ConvertUtil.dip2px(30));
                int margin = ConvertUtil.dip2px(5);
                layoutParams.setMargins(margin, margin, margin, margin);
                int padding = ConvertUtil.dip2px(15);
                for (final Keyword keyword : keywords) {
                    TextView textView = new TextView(SearchActivity.this);
                    textView.setLayoutParams(layoutParams);
                    textView.setTextSize(12);
                    textView.setTextColor(getResources().getColor(R.color.default_text_color));
                    textView.setBackgroundResource(R.drawable.bg_keyword);
                    textView.setText(keyword.name);
                    textView.setPadding(padding, 0, padding, 0);
                    textView.setGravity(Gravity.CENTER_VERTICAL);
                    mHotKeywordsLayout.addView(textView);
                    textView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            search(keyword.name);
                        }
                    });
                }
            }
        }, this);
    }

    void getParam() {
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            mKeyword = getIntent().getExtras().getString("keyword");
            mKeyword = mKeyword == null ? "" : mKeyword;
            mAddProduct = intent.getBooleanExtra("addProduct", false);
            mLiveId = intent.getStringExtra("liveId");
        }
        mType = intent.getIntExtra(Config.INTENT_KEY_TYPE_NAME, 0);
        if (intent.hasExtra("pageType")) {
            pageType = PageType.valueOf(intent.getStringExtra("pageType"));
        }
    }

    @OnClick(R.id.cancelBtn)
    protected void cancelSearch() {
        finish();
//        mKeywordEt.setText("");
//        mKeywordEt.requestFocus();
//        mKeywordsLayout.setVisibility(View.VISIBLE);
//        mRefreshLayout.setVisibility(View.GONE);
//        mCancelBtn.setVisibility(View.GONE);
//        mCleanBtn.setVisibility(View.GONE);
    }

    @OnClick(R.id.cleanBtn)
    protected void cleanKeyword() {
        mKeywordEt.requestFocus();
        mKeywordEt.setText("");
        mCleanBtn.setVisibility(View.GONE);
    }

    @OnClick(R.id.backBtn)
    protected void onBack() {
        finish();
    }

    protected void search(String keyword) {
        if (TextUtils.isEmpty(keyword)) {
            ToastUtil.error("请输入关键词");
            return;
        }
        mKeyword = keyword;
        mKeywordEt.setText(mKeyword);
        mPageManager.onRefresh();
        mKeywordsLayout.setVisibility(View.GONE);
        mRefreshLayout.setVisibility(View.VISIBLE);
        mRefreshLayout.setRefreshing(true);
        mCleanBtn.setVisibility(View.VISIBLE);
        mCancelBtn.setVisibility(View.VISIBLE);
        mKeywordEt.clearFocus();
    }

    @Override
    public void nextPage(final int page) {
        if (pageType == PageType.JKP) {
            APIManager.startRequest(mJkpProductService.search(mKeyword, page, 15, ""), new BaseRequestListener<JkpSearchResult>() {
                @Override
                public void onSuccess(JkpSearchResult searchResult) {
                    mRefreshLayout.setRefreshing(false);
                    if (searchResult == null) {
                        return;
                    } else if ("2".equals(searchResult.searchType)) {
                        if (searchResult.data == null) {
                            return;
                        } else {
                            searchResult.data.parseCouponAmountFromCouponInfo();
                            searchResult.resultList = new ArrayList<>();
                            searchResult.resultList.add(searchResult.data);
                        }
                    } else if ("1".equals(searchResult.searchType)) {
                        if (searchResult.resultList == null) {
                            return;
                        }
                    }
                    if (page == 1) {
                        mAdapter.getItems().clear();
                        noDataLayout.setVisibility(searchResult.resultList.size() > 0 ? View.GONE : View.VISIBLE);
                    }
                    mPageManager.setLoading(false);
                    if (searchResult.resultList.size() >= 15) {
                        mPageManager.setTotalPage(page + 1);
                    } else {
                        mPageManager.setTotalPage(page);
                    }
                    mAdapter.addItems(searchResult.getSkuProducts());
                }

                @Override
                public void onError(Throwable e) {
                    super.onError(e);
                    mRefreshLayout.setRefreshing(false);
                }
            }, this);
        } else {
            Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> observable;
            if (mType == AppTypes.PUSH.PRODUCT_TYPE) {
                observable = mProductService.searchPush(mKeyword, page);
            } else {
                if (Const.NEWRETAIL_MODE.equals(classifacationType))
                    observable = mProductService.search(mKeyword, page, "onnOrder");
                else
                    observable = mProductService.search(mKeyword, page);
            }
            APIManager.startRequest(observable, new BaseRequestListener<PaginationEntity<SkuInfo, Object>>(this) {
                @Override
                public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                    if (page == 1) {
                        mAdapter.getItems().clear();
                        noDataLayout.setVisibility(result.list.size() > 0 ? View.GONE : View.VISIBLE);
                    }
                    mPageManager.setLoading(false);
                    mPageManager.setTotalPage(result.totalPage);
                    mAdapter.addItems(result.list);
                }

                @Override
                public void onComplete() {
                    super.onComplete();
                    mRefreshLayout.setRefreshing(false);
                }

                @Override
                public void onError(Throwable e) {
                    super.onError(e);
                    mRefreshLayout.setRefreshing(false);
                }
            }, this);
        }
    }
}
