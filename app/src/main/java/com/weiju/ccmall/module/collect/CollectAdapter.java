package com.weiju.ccmall.module.collect;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICollectService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Chan on 2017/6/15.
 */

public class CollectAdapter extends BaseAdapter<SkuInfo, CollectAdapter.ViewHolder> {

    private ICollectService mService;

    public CollectAdapter(Context context) {
        super(context);
    }

    @Override
    public CollectAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(layoutInflater.inflate(R.layout.item_collect, parent, false));
    }

    @Override
    public void onBindViewHolder(final CollectAdapter.ViewHolder holder, final int position) {
        final SkuInfo skuInfo = items.get(position);
        holder.setCollect(skuInfo);

        holder.itemView.findViewById(R.id.itemTrashBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WJDialog wjDialog = new WJDialog(context);
                wjDialog.show();
                wjDialog.setContentText("确定取消该收藏？");
                wjDialog.setCancelText("取消");
                wjDialog.setConfirmText("确定");
                wjDialog.setOnCancelListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        wjDialog.dismiss();
                    }
                });
                wjDialog.setOnConfirmListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        wjDialog.dismiss();
                        delCollect(holder.itemView.getContext(), items.get(position).skuId, position);
                    }
                });

            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), NewProductDetailActivity.class);
                intent.putExtra(Key.SKU_ID, skuInfo.skuId);
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }

    private void delCollect(Context context, String skuId, final int position) {
        if (mService == null) {
            mService = ServiceManager.getInstance().createService(ICollectService.class);
        }
        APIManager.startRequest(mService.changeCollect("collect/delCollect", skuId), new BaseRequestListener<PaginationEntity<SkuInfo, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                items.remove(position);
                notifyDataSetChanged();
            }
        },context);
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemThumbIv)
        protected SimpleDraweeView mItemThumbIv;
        @BindView(R.id.itemTitleTv)
        protected TextView mItemTitleTv;
        @BindView(R.id.itemMoneyTv)
        protected TextView mItemMoneyTv;
        @BindView(R.id.itemMoneyTv2)
        protected TextView mItemMoneyTv2;
        @BindView(R.id.tvCount)
        protected TextView mtvCount;
        @BindView(R.id.ivBanjia)
        protected ImageView mIvBanjia;
        @BindView(R.id.tvCCM)
        protected TextView mTvCCM;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        protected void setCollect(final SkuInfo skuInfo) {
            mItemTitleTv.setText(skuInfo.name);
            mItemMoneyTv.setText(MoneyUtil.centToYuan¥StrNoZero(skuInfo.retailPrice));
            mItemMoneyTv2.setText(MoneyUtil.centToYuan¥StrNoZero(skuInfo.marketPrice));
            TextViewUtil.addThroughLine(mItemMoneyTv2);
            //     mItemMoneyTv2.setVisibility(View.VISIBLE);
            mtvCount.setText("销量：" + skuInfo.totalSaleCount);
            mTvCCM.setText(String.format("奖%s%%算率", skuInfo.countRateExc));
            FrescoUtil.setSkuImgSmallMask(mItemThumbIv, skuInfo);
            mIvBanjia.setVisibility(skuInfo.isBanjia() ? View.VISIBLE : View.GONE);

        }
    }
}
