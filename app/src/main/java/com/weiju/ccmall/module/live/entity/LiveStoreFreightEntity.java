package com.weiju.ccmall.module.live.entity;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.bean.StoreFreight;

import java.util.List;

/**
 * @author Ben
 * @date 2020/4/21.
 */
public class LiveStoreFreightEntity {

    /**
     * templated : true
     * freightList : []
     */

    @SerializedName("templated")
    public boolean templated;
    @SerializedName("templatedTips")
    public String templatedTips;
    @SerializedName("freightList")
    public List<StoreFreight> freightList;
}
