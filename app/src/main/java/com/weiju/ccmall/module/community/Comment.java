package com.weiju.ccmall.module.community;


import android.text.TextUtils;

import java.io.Serializable;

public class Comment implements Serializable {
    public String commentId;
    public String topicId;
    public String memberId;
    public String nickName;
    public String headImage;
    public String content;
    public String reply;
    public String replyDate;
    public String createDate;
    public String updateDate;
    public int status;
    public String courseId;

    public boolean hasReply() {
        return !TextUtils.isEmpty(reply);
    }

    public String getPostTime() {
        return DateUtils.getDateString(this.createDate);
    }

    public String getReplyTime() {
        if (TextUtils.isEmpty(replyDate)) {
            return "";
        }

        return DateUtils.getDateString(replyDate);
    }
}
