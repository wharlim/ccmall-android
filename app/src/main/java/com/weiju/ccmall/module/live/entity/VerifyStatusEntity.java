package com.weiju.ccmall.module.live.entity;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/4/16.
 */
public class VerifyStatusEntity {
    //审核状态（0：未申请；1：待审核；2：审核失败；3：审核成功；）
    public static final int STATUS_NOT = 0;
    public static final int STATUS_WAIT = 1;
    public static final int STATUS_FAIL = 2;
    public static final int STATUS_SUCCESS = 3;

    /**
     * reason : 店铺名非法
     * result : 暂无申请信息
     * liveStoreBean : {"memberId":"","applyForType":-1,"mchInId":""}
     */

    @SerializedName("reason")
    public String reason;   //失败原因（失败才有该字段）
    @SerializedName("result")
    public String result;   //暂无申请信息时才有该字段
    @SerializedName("liveStoreBean")
    public LiveStoreBeanBean liveStoreBean;
    @SerializedName("liveStoreApplyBean")
    public ApplyInfoEntity liveStoreApplyBean;

    public static class LiveStoreBeanBean {
        /**
         * memberId :
         * applyForType : -1
         * mchInId :
         */

        @SerializedName("memberId")
        public String memberId;
        @SerializedName("applyForType")
        public int applyForType;    //审核状态（0：未申请；1：待审核；2：审核失败；3：审核成功；）
        @SerializedName("mchInId")
        public String mchInId;

        /**
         * reason : 店铺名非法
         * payBond : 0
         * bondMoney : 0
         */

        @SerializedName("reason")
        public String reason;
        @SerializedName("payBond")
        public String payBond;
        @SerializedName("bondMoney")
        public String bondMoney;
    }

}
