package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.bean.SNCode;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class BindITSNActivity extends BaseActivity {

    @BindView(R.id.ll_pos)
    LinearLayout llPos;
    @BindView(R.id.tv_bind_count)
    TextView tvBindCount;
    @BindView(R.id.etSN)
    EditText etSN;

    private int availableCount;
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind_itsn);
        ButterKnife.bind(this);
        setLeftBlack();
        setTitle("绑定智能终端SN码");
        loadBindSN();

//        addPost("SN60030334444");
//        addPost("SN60030334444");
//        addPost("SN60030334444");
//        addPost("SN60030334444");
        updateBindAvailableCount(0);
    }

    private void updateBindAvailableCount(int count) {
        availableCount = count;
        tvBindCount.setText(Html.fromHtml("您有绑码机会 <span style=\"color:#EBB95B\">" + count + "</span> 次"));
    }

    private void loadBindSN() {
        APIManager.startRequest(service.getBindData(), new Observer<XYSHCommonResult<SNCode.SNCodeResult>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<SNCode.SNCodeResult> ret) {
                if (ret.success || ret.code == 0) {
                    updateBindAvailableCount(ret.data.count);
                    for (SNCode code :
                            ret.data.list) {
                        addPost(code.snCode);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void bindSN(String sn) {
        if (availableCount <= 0) {
            ToastUtil.error("绑码次数不足");
            return;
        }
        ToastUtil.showLoading(this, true);
        APIManager.startRequest(service.bindSnCode(sn), new Observer<XYSHCommonResult<Object>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<Object> ret) {
                ToastUtil.hideLoading();
                if (ret.code == 0) {
                    // 添加成功
                    addPost(sn);
                    updateBindAvailableCount(availableCount-1);
                } else {
                    ToastUtil.error(ret.message);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private void addPost(String posNo) {
        View itemView = LayoutInflater.from(this).inflate(R.layout.item_bind_itsn, llPos, false);
        TextView posNoTv = itemView.findViewById(R.id.tv_post_no);
        posNoTv.setText(posNo);
        llPos.addView(itemView);
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, BindITSNActivity.class);
        context.startActivity(intent);
    }

    @OnClick(R.id.tvAdd)
    public void addSN() {
        String sn = etSN.getText().toString().trim();
        if (TextUtils.isEmpty(sn)) {
            ToastUtil.error("请输入正确的SN码");
            return;
        }
        bindSN("SN" + sn);
    }
}
