package com.weiju.ccmall.module.live.entity;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Ben
 * @date 2020/4/15.
 */
public class ApplyInfoEntity implements Serializable {

    /**
     * mchInId : c9b67b1b671b4e5880b16f4d836f70e8
     * phone : 15070981234
     * mail : 1234@qq.com
     * supplierPrincipal : af8d785b384ad9281286275095141f15
     * categoryId : 62d29248b1da4e409bec5d8aba864eaf
     * storeName : 某某
     * storeBusinessProvince : 上海市
     * storeBusinessCity : 上海市
     * storeBusinessArea : 浦东区
     * storeBusinessAddr : 哈哈哈哈
     * createDate : 2020-04-16 19:42:31.0
     * updateDate : 2020-04-16 19:42:31.0
     * deleteFlag : 0
     */

    @SerializedName("mchInId")
    public String mchInId;
    @SerializedName("phone")
    public String phone;
    @SerializedName("mail")
    public String mail;
    @SerializedName("supplierPrincipal")
    public String supplierPrincipal;
    @SerializedName("categoryId")
    public String categoryId;
    @SerializedName("storeName")
    public String storeName;
    @SerializedName("storeBusinessProvince")
    public String storeBusinessProvince;
    @SerializedName("storeBusinessCity")
    public String storeBusinessCity;
    @SerializedName("storeBusinessArea")
    public String storeBusinessArea;
    @SerializedName("storeBusinessAddr")
    public String storeBusinessAddr;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public String deleteFlag;

    @SerializedName("existFlag")
    public int existFlag;   //是否存在店铺信息,0存在


}
