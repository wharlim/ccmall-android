package com.weiju.ccmall.module.live.adapter;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.StoreFreight;
import com.weiju.ccmall.shared.component.DecimalEditText;
import com.weiju.ccmall.shared.util.MoneyUtil;

import java.util.List;

public class StoreFreightAdapter extends BaseQuickAdapter<StoreFreight, BaseViewHolder> {

    private boolean mIsEdit;

    public StoreFreightAdapter(@Nullable List<StoreFreight> data, boolean isEdit) {
        super(R.layout.item_sore_freight, data);
        mIsEdit = isEdit;
    }

    @Override
    protected void convert(BaseViewHolder helper, StoreFreight item) {
        helper.setText(R.id.tvName, item.provinceName);

        DecimalEditText etInitFreight = helper.getView(R.id.etInitFreight);
        DecimalEditText etAddFreight = helper.getView(R.id.etAddFreight);
        etInitFreight.setText(MoneyUtil.centToYuanStrNoZero(item.initFreight));
        etAddFreight.setText(MoneyUtil.centToYuanStrNoZero(item.addFreight));
        if (!mIsEdit){
            banEditText(etInitFreight);
            banEditText(etAddFreight);
        }else {
            useEditText(etInitFreight);
            useEditText(etAddFreight);
            etInitFreight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String s1 = s.toString();
                    if (TextUtils.isEmpty(s1)) {
                        item.initFreight = 0;
                    } else {
                        item.initFreight = (int) MoneyUtil.yuanStrToCentLong(s1);
                    }

                }
            });
            etAddFreight.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    String s1 = s.toString();
                    if (TextUtils.isEmpty(s1)) {
                        item.addFreight = 0;
                    } else {
                        item.addFreight = (int) MoneyUtil.yuanStrToCentLong(s1);
                    }

                }
            });
        }

        helper.setIsRecyclable(false);// 禁止复用
    }

    private void banEditText(EditText editText){
        editText.setFocusable(false);
        editText.setFocusableInTouchMode(false);
        editText.setBackgroundColor(Color.parseColor("#FFFFFF"));

    }

    private void useEditText(EditText editText){
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.setBackgroundColor(Color.parseColor("#EFEFEF"));

    }


}
