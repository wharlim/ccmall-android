package com.weiju.ccmall.module.blockchain.computingpower;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.beans.AllCCm;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class BlockFragment extends BaseFragment implements PageManager.RequestListener {

    @BindView(R.id.noDataLayout)
    NoData noDataLayout;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    Unbinder unbinder;

    BlockAdapter blockAdapter;
    PageManager pageManager;

    AllCCm.AccountBlockBean accountBlock;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.base_list_layout, container, false);
        accountBlock = (AllCCm.AccountBlockBean) getArguments().getSerializable("accountBlock");
        unbinder = ButterKnife.bind(this, view);
        initView();
        return view;
    }

    protected void initView() {
        blockAdapter = new BlockAdapter(accountBlock.block());
        recyclerView.setAdapter(blockAdapter);
        try {
            pageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(recyclerView)
                    .setRequestListener(this)
                    .setSwipeRefreshLayout(refreshLayout)
                    .setNoDataLayout(noDataLayout)
                    .build(getContext());
            pageManager.onRefresh(); // 初始化数据
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }
        refreshLayout.setEnabled(false);
    }

    public static BlockFragment newInstance(AllCCm.AccountBlockBean accountBlock) {
        Bundle args = new Bundle();
        args.putSerializable("accountBlock", accountBlock);
        BlockFragment fragment = new BlockFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void nextPage(int page) {

    }
}
