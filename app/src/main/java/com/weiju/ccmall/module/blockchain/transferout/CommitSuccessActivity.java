package com.weiju.ccmall.module.blockchain.transferout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommitSuccessActivity extends BaseActivity {
    @BindView(R.id.tv_receiver_addr)
    TextView tvReceiverAddr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commit_success);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        tvReceiverAddr.setText(intent.getStringExtra("addr"));
        setTitle("确认信息");
        setLeftBlack();
    }

    @OnClick(R.id.tv_back)
    public void onViewClicked() {
        finish();
    }

    public static void start(Context context, String addr) {
        Intent intent = new Intent(context, CommitSuccessActivity.class);
        intent.putExtra("addr", addr);
        context.startActivity(intent);
    }
}
