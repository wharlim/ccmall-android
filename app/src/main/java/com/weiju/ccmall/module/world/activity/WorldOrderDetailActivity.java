package com.weiju.ccmall.module.world.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.world.IWorldService;
import com.weiju.ccmall.module.world.entity.OrderEntity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.OrderService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.MessageFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorldOrderDetailActivity extends BaseActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvOrderState)
    TextView tvOrderState;
    @BindView(R.id.tvOrderNo)
    TextView tvOrderNo;
    @BindView(R.id.tvCopyOrderNo)
    TextView tvCopyOrderNo;
    @BindView(R.id.tvLogisticsNo)
    TextView tvLogisticsNo;
    @BindView(R.id.tvReceiver)
    TextView tvReceiver;
    @BindView(R.id.tvReceiverPhone)
    TextView tvReceiverPhone;
    @BindView(R.id.tvAddress)
    TextView tvAddress;
    @BindView(R.id.sdvGoodsIcon)
    SimpleDraweeView sdvGoodsIcon;
    @BindView(R.id.tvGoodsName)
    TextView tvGoodsName;
    @BindView(R.id.tvGoodsDesc)
    TextView tvGoodsDesc;
    @BindView(R.id.tvGoodsPrice)
    TextView tvGoodsPrice;
    @BindView(R.id.tvGoodsCount)
    TextView tvGoodsCount;
    @BindView(R.id.tvOrderTime)
    TextView tvOrderTime;
    @BindView(R.id.tvPayType)
    TextView tvPayType;
    @BindView(R.id.tvGoodsTotalPrice)
    TextView tvGoodsTotalPrice;
    @BindView(R.id.tvFreight)
    TextView tvFreight;
    @BindView(R.id.tvActualPay)
    TextView tvActualPay;
    @BindView(R.id.llOrderExpress)
    LinearLayout llOrderExpress;
    @BindView(R.id.llAddressDetail)
    LinearLayout llAddressDetail;
    @BindView(R.id.tvCheckLogistics)
    TextView tvCheckLogistics;
    @BindView(R.id.tvConfirmReceipt)
    TextView tvConfirmReceipt;
    @BindView(R.id.tvBuyAgain)
    TextView tvBuyAgain;
    @BindView(R.id.llClick)
    LinearLayout llClick;
    @BindView(R.id.tvRefundStatus)
    TextView tvRefundStatus;
    @BindView(R.id.tvRate)
    TextView tvRate;
    @BindView(R.id.llRate)
    LinearLayout llRate;
    @BindView(R.id.tvPayTag)
    TextView tvPayTag;
    @BindView(R.id.tvCCM)
    TextView tvCCM;


    private IWorldService mService = ServiceManager.getInstance().createService(IWorldService.class);
    private String mOrderCode;
    private OrderEntity mOrderEntity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_order_detail);
        ButterKnife.bind(this);
        QMUIStatusBarHelper.translucent(this);
        initData();
    }

    private void initData() {
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            mOrderCode = intent.getExtras().getString("orderCode");
        }
        if (mOrderCode == null || mOrderCode.isEmpty()) {
            ToastUtil.error("参数错误");
            finish();
            return;
        }
        getOrderDetail();
    }

    private void getOrderDetail() {
        APIManager.startRequest(mService.getOrderDetail(mOrderCode), new BaseRequestListener<OrderEntity>(this) {
            @Override
            public void onSuccess(OrderEntity result) {
                super.onSuccess(result);
                mOrderEntity = result;
                initView();

            }
        }, this);
    }

    private void initView() {
        tvOrderState.setText(mOrderEntity.statusStr);
        tvOrderNo.setText(mOrderEntity.merchantOrderNo);
        if (!TextUtils.isEmpty(mOrderEntity.expressCode)) {
            llOrderExpress.setVisibility(View.VISIBLE);
            tvLogisticsNo.setText(mOrderEntity.expressCode);
        }
        tvReceiver.setText(String.format("收货人：%s", mOrderEntity.recName));
        tvReceiverPhone.setText(mOrderEntity.phone);
        tvAddress.setText(String.format("收货地址：%s", mOrderEntity.getAddressDetail()));
        llAddressDetail.setOnLongClickListener(v -> {
            copy(mOrderEntity.recName + " " + mOrderEntity.phone + " " + mOrderEntity.getAddressDetail());
            return true;
        });
        FrescoUtil.setImageSmall(sdvGoodsIcon, mOrderEntity.mainImagesUrl);
        tvGoodsName.setText(mOrderEntity.skuName);
        tvGoodsDesc.setText(mOrderEntity.properties);
        tvGoodsPrice.setText(MoneyUtil.centToYuan¥Str(mOrderEntity.price));
        tvGoodsCount.setText(String.format("x%s", mOrderEntity.count));
        tvOrderTime.setText(mOrderEntity.createDate);
        tvPayType.setText(mOrderEntity.payTypeStr);
        tvGoodsTotalPrice.setText(MoneyUtil.centToYuan¥Str(mOrderEntity.totalPrice));
        tvFreight.setText(mOrderEntity.shippingFee > 0 ? MoneyUtil.centToYuan¥Str(mOrderEntity.shippingFee) : "包邮");
        if (mOrderEntity.tax > 0) {
            double tax = MoneyUtil.keepTwoDecimals(mOrderEntity.tax * 100);
            tvRate.setText(MessageFormat.format("{0}%（估：{1}）", tax, MoneyUtil.centToYuan¥Str(mOrderEntity.taxFee)));
            llRate.setVisibility(View.VISIBLE);
        } else {
            llRate.setVisibility(View.GONE);
        }
        if (mOrderEntity.orderStatus == 0 || mOrderEntity.orderStatus == 1) {
            tvPayTag.setText("待付款：");
        }
        tvActualPay.setText(MoneyUtil.centToYuan¥Str(mOrderEntity.actualPrice));
        tvCCM.setText(Html.fromHtml(String.format("预计可返<font color=#8B57F4>%s%%</font>算率", mOrderEntity.countRateExc)));
        if (mOrderEntity.orderStatus == 0 || mOrderEntity.orderStatus == 4) {
            llClick.setVisibility(View.VISIBLE);
            if (mOrderEntity.orderStatus == 0) {
                tvCheckLogistics.setVisibility(View.GONE);
                tvConfirmReceipt.setVisibility(View.GONE);
                tvBuyAgain.setVisibility(View.VISIBLE);
            } else {
                tvCheckLogistics.setVisibility(View.VISIBLE);
                tvConfirmReceipt.setVisibility(View.VISIBLE);
                tvBuyAgain.setVisibility(View.GONE);
            }
        } else {
            llClick.setVisibility(View.GONE);
        }
        if (mOrderEntity.refundStatus != 0) {
            tvRefundStatus.setVisibility(View.VISIBLE);
            tvRefundStatus.setText(mOrderEntity.refundStatusStr);
        }
    }

    @OnClick(R.id.ivBack)
    public void onViewBackClicked() {
        finish();
    }

    public static void start(Context context, String orderCode) {
        Intent intent = new Intent(context, WorldOrderDetailActivity.class);
        intent.putExtra("orderCode", orderCode);
        context.startActivity(intent);
    }

    @OnClick(R.id.tvCopyOrderNo)
    public void onCopyOrderNoClicked() {
        copy(mOrderEntity.merchantOrderNo);
    }

    private void copy(String content) {
        ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData mClipData = ClipData.newPlainText("Label", content);
        cm.setPrimaryClip(mClipData);
        ToastUtil.success("复制成功");
    }

    @OnClick({R.id.tvCheckLogistics, R.id.tvConfirmReceipt, R.id.tvBuyAgain})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvCheckLogistics:
                OrderService.checkExpress(this, mOrderEntity.expressCode, mOrderEntity.expressId);
                break;
            case R.id.tvConfirmReceipt:
                OrderService.finishWorldOrder(this, mOrderCode);
                break;
            case R.id.tvBuyAgain:
                WorldProductDetailActivity.start(this, mOrderEntity.itemId);
                break;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
        if (message.getEvent().equals(Event.finishOrder)) {
            getOrderDetail();
        }
    }
}
