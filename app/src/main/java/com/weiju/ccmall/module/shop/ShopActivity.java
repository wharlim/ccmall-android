package com.weiju.ccmall.module.shop;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.shop.bean.ShopItem;
import com.weiju.ccmall.module.shop.behaviors.TransferHeaderBehavior;
import com.weiju.ccmall.module.shop.views.ShopTitleView;
import com.weiju.ccmall.module.shop.views.Utils;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Category;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ShareUtils;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 店铺主页
 */
public class ShopActivity extends BaseActivity {

    private String storeId;
    private ShopItem shop;
    private IProductService iProductService;

    private Toolbar toolbar;
    private ShopTitleView scrollShopTitle;
    private ViewPager viewPager;
    private MagicIndicator mMagicIndicator;
    private FragmentStatePagerAdapter fragmentStatePagerAdapter;
    private boolean mIsLiveStore;
    private SimpleDraweeView icShop;
    private TextView tvTitle;
    private TextView tvFans;
    private TextView tvGoodsCount;
    private User mUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        ButterKnife.bind(this);
        Log.d("Seven", "dd " + Utils.dpToPx(96));
        storeId = getIntent().getStringExtra("storeId");
        mIsLiveStore = getIntent().getBooleanExtra("isLiveStore", false);
        if (TextUtils.isEmpty(storeId)) {
            ToastUtil.error("店铺不存在!");
            finish();
            return;
        }
        mUser = SessionUtil.getInstance().getLoginUser();
        iProductService = ServiceManager.getInstance().createService(IProductService.class);
        toolbar = findViewById(R.id.toolBar);
        toolbar.setNavigationOnClickListener(v -> {
            finish();
        });

//        ImageView iv = findViewById(R.id.iv);
//        FrameLayout flHeader = findViewById(R.id.flHeader);
//        flHeader.setBackgroundColor(getResources().getColor(R.color.red));
//        iv.setVisibility(View.GONE);

        scrollShopTitle = findViewById(R.id.scroll_shop_title);
        viewPager = findViewById(R.id.view_pager);
        mMagicIndicator = findViewById(R.id.magicIndicator);

        FrameLayout flHeader = findViewById(R.id.flHeader);
        LinearLayout llLive = findViewById(R.id.llLive);
        icShop = findViewById(R.id.icShop);
        tvTitle = findViewById(R.id.tvTitle);
        tvFans = findViewById(R.id.tvFans);
        tvGoodsCount = findViewById(R.id.tvGoodsCount);

        if (mIsLiveStore) {
            flHeader.setVisibility(View.GONE);
            llLive.setVisibility(View.VISIBLE);
        }
        getStoreInfo();
        getStoreParentCategory();
    }

    private void getStoreInfo() {
        APIManager.startRequest(mIsLiveStore ? iProductService.getLiveStore(storeId) : iProductService.getStore(storeId), new BaseRequestListener<ShopItem>() {
            @Override
            public void onSuccess(ShopItem result) {
                super.onSuccess(result);
                if (isDestroyed()) {
                    return;
                }
//                Log.d("Seven", "shop: " + result.toString());
                shop = result;
                if (mIsLiveStore) {
                    FrescoUtil.setImageSmall(icShop, result.thumbUrl);
                    tvTitle.setText(result.storeName);
                    tvFans.setText(String.format("主播粉丝数：%d", result.fansNum));
                    tvGoodsCount.setText(String.format("产品数：%d", result.productNum));
                } else {
                    scrollShopTitle.setShopItem(result);
                }

                CoordinatorLayout.LayoutParams lp = (CoordinatorLayout.LayoutParams) findViewById(R.id.transferHeaderBehaviorView).getLayoutParams();
                TransferHeaderBehavior behavior = (TransferHeaderBehavior) lp.getBehavior();
                behavior.setTitle(shop.storeName);
            }
        }, this);
    }

    private void initViewPager(List<Category> categories) {
        fragmentStatePagerAdapter = new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                Category category = categories.get(position);
                return mIsLiveStore ? CategoryFragment.newInstance(storeId, category.id, true) : CategoryFragment.newInstance(storeId, category.id);
            }

            @Override
            public int getCount() {
                return categories.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return categories.get(position).name;
            }
        };

        viewPager.setAdapter(fragmentStatePagerAdapter);
        viewPager.setOffscreenPageLimit(categories.size());
        viewPager.setCurrentItem(0);
    }

    private void initIndicator(List<Category> categories) {
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setLeftPadding(ConvertUtil.dip2px(20));
        commonNavigator.setRightPadding(ConvertUtil.dip2px(20));
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return categories.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(categories.get(index).name);
                titleView.setNormalColor(getResources().getColor(R.color.default_text_color));
                titleView.setSelectedColor(getResources().getColor(R.color.red));
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(14);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getResources().getColor(R.color.red));
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                return indicator;
            }
        });
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, viewPager);
    }

    // 获取商品分类
    private void getStoreParentCategory() {
        APIManager.startRequest(mIsLiveStore ? iProductService.getCategoryByStore(storeId) : iProductService.getStoreParentCategory(storeId, 1, 100),
                new BaseRequestListener<PaginationEntity<Category, Object>>() {
                    @Override
                    public void onSuccess(PaginationEntity<Category, Object> result) {
                        super.onSuccess(result);
                        if (isDestroyed()) {
                            return;
                        }

                        Category all = new Category();
                        all.id = "ALL";
                        all.name = "全部";
                        result.list.add(0, all);

                        initViewPager(result.list);
                        initIndicator(result.list);
                    }
                }, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        ToastUtil.hideLoading();
    }

    @OnClick(R.id.rlShare)
    public void onViewClicked() {
        if (UiUtils.checkUserLogin(this)) {
            String url = mIsLiveStore ? "lt/" : "st/";
            String thumbUrl = shop.thumbUrl;
            ShareUtils.showShareDialog(this, shop.storeName, "", thumbUrl,
                    BuildConfig.WECHAT_URL + url + storeId + "/" + mUser.invitationCode);
        }
    }

    public static void start(Context context, String storeId) {
        Intent activity = new Intent(context, ShopActivity.class);
        activity.putExtra("storeId", storeId);
        context.startActivity(activity);
    }

    public static void start(Context context, String storeId, boolean isLiveStore) {
        Intent activity = new Intent(context, ShopActivity.class);
        activity.putExtra("storeId", storeId);
        activity.putExtra("isLiveStore", isLiveStore);
        context.startActivity(activity);
    }

}
