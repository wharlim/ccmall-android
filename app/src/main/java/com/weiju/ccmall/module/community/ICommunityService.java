package com.weiju.ccmall.module.community;

import com.weiju.ccmall.module.community.models.CoursePlate;
import com.weiju.ccmall.module.community.publish.HistoryExtra;
import com.weiju.ccmall.module.community.publish.PublishHisModule;
import com.weiju.ccmall.module.community.publish.PublishInfoBody;
import com.weiju.ccmall.shared.bean.AddTopliBody;
import com.weiju.ccmall.shared.bean.Image;
import com.weiju.ccmall.shared.bean.TopicLibraryModel;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * @author Stone
 * @time 2018/6/11  14:17
 * @desc ${TODD}
 */
public interface ICommunityService {

    @FormUrlEncoded
    @POST("group/addGroupTopicComment")
    Observable<RequestResult<MaterialVideoModule.CommentModule>> addGroupTopicComment(
            @Field("topicId") String topicId,
            @Field("content") String content
    );


    @FormUrlEncoded
    @POST("group/cancelGroupTopicLike")
    Observable<RequestResult<Object>> cancelGroupTopicLike(
            @Field("topicId") String topicId,
            @Field("likeId") String likeId
    );

    @FormUrlEncoded
    @POST("group/addGroupTopicLike")
    Observable<RequestResult<Like>> addGroupTopicLike(
            @Field("topicId") String topicId
    );

    @GET("materialLibrary/getMaterialLibraryCategoryList")
    Observable<RequestResult<List<GroupCategoryModel>>> getMaterialLibraryCategoryList(@Query("type") String type);


    @GET("group/getGroupTopicList")
    Observable<RequestResult<PaginationEntity<MaterialVideoModule, Object>>> getGroupTopicListNew(
            @Query("pageSize") int authorId,
            @Query("pageOffset") int page
    );

    @GET("materialLibrary/getMaterialLibraryList")
    Observable<RequestResult<PaginationEntity<MaterialVideoModule, Object>>> getMaterialLibraryList(@Query("categoryId") String categoryId, @Query("keys") String keys, @Query("pageOffset") int pageOffset, @Query("pageSize") int pageSize);


    @GET("course/getCourseList")
    Observable<RequestResult<PaginationEntity<CourseModule, Object>>> getCourseList(@Query("courseType") int type, @Query("pageOffset") int pageOffset, @Query("pageSize") int pageSize, @Query("plateId") String plateId);

    @GET
    Observable<RequestResult<PaginationEntity<Course, Object>>> getCourseListByCategoryId(@Url String url, @Query("keys") String keys, @Query("categoryId") String categoryId, @Query("courseType") int type, @Query("pageOffset") int pageOffset, @Query("pageSize") int pageSize);


    @GET("group/getGroupTopicCommentList")
    Observable<RequestResult<PaginationEntity<MaterialVideoModule.CommentModule, Object>>> getGroupTopicCommentList(@Query("pageOffset") int pageOffset, @Query("pageSize") int pageSize, @Query("topicId") String topicId);


    @GET("course/getCourseBannerList")
    Observable<RequestResult<ArrayList<Course>>> getCourseBannerList(@Query("plateId") String plateId);

    @GET("course/getCourseCommentList")
    Observable<RequestResult<PaginationEntity<Comment, Object>>> getCourseCommentList(
            @Query("courseId") String courseId,
            @Query("pageOffset") int page
    );

    @FormUrlEncoded
    @POST("course/addCourseComment")
    Observable<RequestResult<Comment>> addCourseComment(
            @Field("courseId") String courseId,
            @Field("content") String content
    );

    @GET("course/getCourse")
    Observable<RequestResult<Course>> courseDetail(
            @Query("courseId") String courseId
    );


    @GET("materialLibrary/getMyMaterialLibraryList")
    Observable<RequestResult<PaginationEntity<PublishHisModule, HistoryExtra>>> getPublishHistory(@Query("pageOffset") int pageOffset, @Query("pageSize") int i);


    @GET("materialLibrary/getMyMaterialLibraryDetail")
    Observable<RequestResult<PublishHisModule>> getOldPublishData(@Query("libraryId") String libraryId);


    @Multipart
    @POST("upload/uploadImage")
    Observable<RequestResult<Image>> uploadImageNew(
            @Part("version") RequestBody version,
            @Part MultipartBody.Part body
    );

    String TYPE_JSON = "Content-Type:application/json";

    @Headers(TYPE_JSON)
    @POST
    Observable<RequestResult<Object>> publishInfo(@Url String url, @Body PublishInfoBody body);


    @Headers(TYPE_JSON)
    @POST
    Observable<RequestResult<Object>> AddTopliBody(
            @Url String url,
            @Body AddTopliBody body
    );

    @GET("groupTopicAction/getGroupTopicLibraryList")
    Observable<RequestResult<PaginationEntity<TopicLibraryModel, HistoryExtra>>> getMyMaterialLibraryList(
            @Query("pageOffset") int pageOffset,
            @Query("pageSize") int pageSize
    );

    @GET("course/getPlates")
    Observable<RequestResult<List<CoursePlate>>> getPlates();

    @FormUrlEncoded
    @POST("course/addCourseReword")
    Observable<RequestResult<Object>> addCourseReword(@Field("courseId") String courseId);

    @FormUrlEncoded
    @POST("course/addBrowseLog")
    Observable<RequestResult<Object>> addBrowseLog(@Field("courseId") String courseId);
}

