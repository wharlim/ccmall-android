package com.weiju.ccmall.module.blockchain.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.adapters.TransactionAdapter;
import com.weiju.ccmall.module.blockchain.beans.TransactionItem;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBlockChain;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class DetailFragment extends BaseFragment implements PageManager.RequestListener {

    @BindView(R.id.noDataLayout)
    NoData mNoDataLayout;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.srl_layout)
    SwipeRefreshLayout srlLayout;
    Unbinder unbinder;

    TransactionAdapter transactionAdapter;
    PageManager pageManager;
    IBlockChain blockChain;
    ArrayList<TransactionItem> transactionItems = new ArrayList<>();
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        blockChain = ServiceManager.getInstance().createService(IBlockChain.class);
        unbinder = ButterKnife.bind(this, view);
        initView();
//        pageManager.onRefresh();
        return view;
    }

    private void initView() {
        transactionAdapter = new TransactionAdapter(transactionItems);
        recycler.setAdapter(transactionAdapter);
        try {
            pageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(recycler)
                    .setRequestListener(this)
                    .setSwipeRefreshLayout(srlLayout)
                    .setNoDataLayout(mNoDataLayout)
                    .build(getContext());
//            pageManager.onRefresh(); // 初始化数据
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }
        mNoDataLayout.setTextView("暂无记录");
    }

    private void getData(int page) {
        APIManager.startRequest(blockChain.getDayLog(page), new BaseRequestListener<PaginationEntity<TransactionItem, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<TransactionItem, Object> result) {
                super.onSuccess(result);
                if (!isAdded()) {
                    return;
                }
                if (page == 1) {
                    transactionItems.clear();
                }
                if (result.list != null) {
                    transactionItems.addAll(result.list);
                }
                transactionAdapter.notifyDataSetChanged();
                pageManager.setTotalPage(result.totalPage);
            }

            @Override
            public void onComplete() {
                super.onComplete();
                if (!isAdded() || getView() == null || srlLayout == null) {
                    return;
                }
                pageManager.setLoading(false);
                srlLayout.setRefreshing(false);
            }
        }, getContext());
    }

    public static DetailFragment newInstance() {
        Bundle args = new Bundle();
        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        pageManager.onRefresh();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void nextPage(int page) {
        // 加载一页数据
        getData(page);
    }
}
