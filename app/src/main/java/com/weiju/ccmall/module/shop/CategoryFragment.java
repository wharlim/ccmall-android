package com.weiju.ccmall.module.shop;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.category.adapter.ProductListAdapter;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;

import io.reactivex.Observable;

public class CategoryFragment extends BaseFragment implements PageManager.RequestListener{

    private SwipeRefreshLayout refreshLayout;
    private RecyclerView recyclerView;
    private String storeId;
    private String categoryId;

    private PageManager mPageManager;
    private GridLayoutManager mDoubleColumnLayoutManager;
    private ProductListAdapter mAdapter;

    private IProductService iProductService;
    private boolean mIsLiveStore;

    public static CategoryFragment newInstance(String storeId, String categoryId) {
        CategoryFragment categoryFragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString("storeId", storeId);
        args.putString("categoryId", categoryId);
        categoryFragment.setArguments(args);
        return categoryFragment;
    }
    public static CategoryFragment newInstance(String storeId, String categoryId, boolean isLiveStore) {
        CategoryFragment categoryFragment = new CategoryFragment();
        Bundle args = new Bundle();
        args.putString("storeId", storeId);
        args.putString("categoryId", categoryId);
        args.putBoolean("isLiveStore", isLiveStore);
        categoryFragment.setArguments(args);
        return categoryFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        Bundle args = getArguments();
        iProductService = ServiceManager.getInstance().createService(IProductService.class);
        storeId = args.getString("storeId");
        categoryId = args.getString("categoryId");
        mIsLiveStore = args.getBoolean("isLiveStore");
        recyclerView = view.findViewById(R.id.recycler);
        refreshLayout = view.findViewById(R.id.refreshLayout);
        mDoubleColumnLayoutManager = new GridLayoutManager(getContext(), 2);
        mAdapter = new ProductListAdapter(getContext());
        mAdapter.setColumns(2);
        recyclerView.setAdapter(mAdapter);
        try {
            mPageManager = PageManager.getInstance()
                    .setSwipeRefreshLayout(refreshLayout)
                    .setRecyclerView(recyclerView)
                    .setLayoutManager(mDoubleColumnLayoutManager)
                    .setRequestListener(this)
                    .build(getContext());
        } catch (PageManager.PageManagerException e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPageManager.onRefresh();
    }

    @Override
    public void nextPage(int page) {
        Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> observable = iProductService.getSpuListByCategoryAndStore(storeId, categoryId, page, 16);
        if (mIsLiveStore) observable = iProductService.getLiveStoreSpuListByCategory(storeId, categoryId, page, 16);
        APIManager.startRequest(observable,
                new BaseRequestListener<PaginationEntity<SkuInfo, Object>>() {
                    @Override
                    public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                        super.onSuccess(result);
                        if (isAdded()) {
                            if (page == 1) {
                                mAdapter.getItems().clear();
                            }
                            mPageManager.setLoading(false);
                            mPageManager.setTotalPage(result.totalPage);
                            mAdapter.addItems(result.list);
                        }
                    }
                    @Override
                    public void onComplete() {
                        super.onComplete();
                        refreshLayout.setRefreshing(false);
                    }
                }, getContext());
    }
}
