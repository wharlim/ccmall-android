package com.weiju.ccmall.module.pickUp.activity;

import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.pickUp.adapter.PickUpAdapter;
import com.weiju.ccmall.module.point.PointListActivity;
import com.weiju.ccmall.module.transfer.StepFirstActivity;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.PickUp;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.enums.AccountType;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IPickUpService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * @author chenyanming
 * @time 2019/8/1 on 16:33
 * @desc ${TODD}
 */
public class PickUpListActivity extends BaseListActivity {
    private PickUpAdapter mAdapter = new PickUpAdapter();

    private IPickUpService mService = ServiceManager.getInstance().createService(IPickUpService.class);

    @Override
    public void initView() {
        super.initView();
        EventBus.getDefault().register(this);
        getHeaderLayout().setRightText("使用记录");
        getHeaderLayout().setOnRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PickUpListActivity.this, PickUpUsedListActivity.class));
            }
        });
        findViewById(R.id.tv_giving).setOnClickListener(v -> {
            if (mAdapter.getData().size() <= 0) {
                ToastUtil.error("没有可赠送的提货券！");
                return ;
            }
            Intent intent = new Intent(this, StepFirstActivity.class);
            intent.putExtra(Config.INTENT_KEY_TYPE_NAME, AppTypes.TRANSFER.PICKUP_COUPON);
            intent.putExtra("AccountType", AccountType.PickUpCoupon);
            startActivity(intent);
        });
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_pickup_list;
    }

    @Override
    public String getTitleStr() {
        return "提货券";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        PickUp item = mAdapter.getItem(position);
        if (null == item) {
            return;
        }
        switch (view.getId()) {
            case R.id.tvUse:
                Intent intent = new Intent(PickUpListActivity.this, PickUpProductListActivity.class);
                intent.putExtra("vouchersId", item.vouchersId);
                startActivity(intent);
                break;
            default:
                Intent intent1 = new Intent(PickUpListActivity.this, PickUpDetailListActivity.class);
                intent1.putExtra("vouchersId", item.vouchersId);
                intent1.putExtra("type", PickUpDetailListActivity.UN_USE);
                startActivity(intent1);
                break;
        }

    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mService.getSumList(mCurrentPage, 15),
                new BaseRequestListener<PaginationEntity<PickUp, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<PickUp, Object> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && result.list.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void transferHandler(EventMessage message) {
        switch (message.getEvent()) {
            case transferSuccess:
            case exchSuccess:
            case createOrderSuccess:
                mCurrentPage = 1;
                getData(true);
                break;
            default:
        }
    }
}
