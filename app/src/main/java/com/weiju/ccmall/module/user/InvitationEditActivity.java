package com.weiju.ccmall.module.user;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.ConfirmInvitationUserDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/5/9 on 15:51
 * @desc 填写分享人
 */
public class InvitationEditActivity extends BaseActivity {

    @BindView(R.id.etPhone)
    EditText mEtPhone;
    @BindView(R.id.tvNext)
    TextView mTvNext;
    @BindView(R.id.layoutNext)
    RelativeLayout mLayoutNext;
    IUserService mService = ServiceManager.getInstance().createService(IUserService.class);

    private boolean mIsRegister ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation_edit);
        ButterKnife.bind(this);
        mIsRegister =  getIntent().getBooleanExtra("isRegister",false);
        initView();
    }

    private void initView() {
        setTitle("我的邀请人");
        setLeftBlack();
        mLayoutNext.setVisibility(mIsRegister? View.VISIBLE:View.GONE);
    }

    @OnClick(R.id.tvNext)
    protected void next(){
        EventBus.getDefault().post(new EventMessage(Event.goMain));
        finish();
    }
    
    @OnClick(R.id.tvSure)
    protected void sure(){
        String phone = mEtPhone.getText().toString();
        if (phone.length()==0){
            ToastUtil.error("请输入邀请人手机号");
            return;
        }
        User user = SessionUtil.getInstance().getLoginUser();
        if (user!=null && user.phone.equals(phone)){
            ToastUtil.error("邀请人不能填写本人");
            return;
        }
        APIManager.startRequest(mService.getUserInfoByPhone(phone), new BaseRequestListener<User>(this) {
            @Override
            public void onSuccess(final User user) {
                ConfirmInvitationUserDialog confirmUserDialog = new ConfirmInvitationUserDialog(InvitationEditActivity.this, user);
                confirmUserDialog.setOnConfirmListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setInvitationCode(user.invitationCode);
                    }
                });
                confirmUserDialog.show();
            }
        },this);


    }

    private void setInvitationCode(String invitationCode){
        APIManager.startRequest(mService.setInvitationCode(invitationCode), new BaseRequestListener<Object>(this) {
            @Override
            public void onSuccess(Object result) {
                EventBus.getDefault().post(new EventMessage(Event.goMain));
                finish();
            }
        },this);
    }







}
