package com.weiju.ccmall.module.blockchain.beans;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AllCCm {

    /**
     * allCcm : 73.24466288
     * contribution : 7.324466288E9
     * allRate : 1.1375
     * superNode : 3
     * node : 5
     * accountBlock : {"block":[{"blockID":"00000000001aea0e1abc39b13d3b9896bc6a3a4fc6f8a61fc8bd4eb3c6ce6071","block_header":{"raw_data":{"number":1763854,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea0d48ad45e1e9407dc8202ed543d801495259d4d9e6e8f7ccf0","version":"8","timestamp":"1573825386000"},"witness_signature":"cab77aeb88a3734769b351f6fd7831e6efb7b57157bd0c08369e81c2b32404fd25095271f460c3336800625d379625fb106470db1ecd82ea50a1861dc23d098400"}},{"blockID":"00000000001aea0fcf9bb528825c9fadbfc2fa895aa54644472277e836a8dbf8","block_header":{"raw_data":{"number":1763855,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe63dbd848c3b23ac93b00b52b98cdf229fb4b40","parentHash":"00000000001aea0e1abc39b13d3b9896bc6a3a4fc6f8a61fc8bd4eb3c6ce6071","version":"8","timestamp":"1573825389000"},"witness_signature":"d0bec8f33e5f9a0d50fbb6677ec1fb3edbc4ddcf8efb8f09695f43706cfd7b3f547edc566de6bd51cd1e4cf00fe475ff586e310ac086011e3a97ca51a302d24700"}},{"blockID":"00000000001aea102e3a4c8a28b5e3f295ca5ba0865927b8c4560d3549935282","block_header":{"raw_data":{"number":1763856,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea0fcf9bb528825c9fadbfc2fa895aa54644472277e836a8dbf8","version":"8","timestamp":"1573825392000"},"witness_signature":"96e47a123f02472a721c55c868ce96ffa02d12aedd671e3a76b032a42280c04568827f5f217866550d436699cb7e75e00aef495a8f5658c477287a811a62fb9501"}},{"blockID":"00000000001aea1187967c6dfee0fb59172fd255e9cd4f4bb9970372be28528d","block_header":{"raw_data":{"number":1763857,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe63dbd848c3b23ac93b00b52b98cdf229fb4b40","parentHash":"00000000001aea102e3a4c8a28b5e3f295ca5ba0865927b8c4560d3549935282","version":"8","timestamp":"1573825395000"},"witness_signature":"2ffa71b0887fb3cc05070f7b6be7befd04a2b0cd3683c2511586b11520b2cc566f5b952c76c9dd63bf7492f5978f41ac8067f713ef51db6d3a5b651eddf2820700"}},{"blockID":"00000000001aea129dd8bf7766fe1d08c58134db303de2c4576ace580e04d498","block_header":{"raw_data":{"number":1763858,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea1187967c6dfee0fb59172fd255e9cd4f4bb9970372be28528d","version":"8","timestamp":"1573825398000"},"witness_signature":"1529016762f73f1ec17451c374b35d9356517619da1de556d877ff71a432657119284d0e2b9700b012c2711d3b1087f74e274baa7fae390204fa39b96258c2d600"}},{"blockID":"00000000001aea13b6be242b10de7504e5bff3bef98810b15d9cbf0211f6eef1","block_header":{"raw_data":{"number":1763859,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe63dbd848c3b23ac93b00b52b98cdf229fb4b40","parentHash":"00000000001aea129dd8bf7766fe1d08c58134db303de2c4576ace580e04d498","version":"8","timestamp":"1573825401000"},"witness_signature":"b4d7fd628e0507cbcc0b10ee9f19526b2d25c7e8b0ef7178dc5d357e6711fdb032d99e9f4a0b350823188cbfc40f4f7d474539504b1ee30b115d07f6b2c385a901"}},{"blockID":"00000000001aea145c33228b033a262acc77a2da55fbfa649ba9b75832be6c95","block_header":{"raw_data":{"number":1763860,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea13b6be242b10de7504e5bff3bef98810b15d9cbf0211f6eef1","version":"8","timestamp":"1573825404000"},"witness_signature":"289bb61410feb476605b1055153dfea2fb1d3c6ecdb7be685bc7a24793d7235575ce717137c3a1f7c3bc6c39f97732015fb6a0344f20c973edc31f7a18e55d3601"}},{"blockID":"00000000001aea15496401f8f50f4c50e7d60056244f296abf0c6d61e910a756","block_header":{"raw_data":{"number":1763861,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe63dbd848c3b23ac93b00b52b98cdf229fb4b40","parentHash":"00000000001aea145c33228b033a262acc77a2da55fbfa649ba9b75832be6c95","version":"8","timestamp":"1573825407000"},"witness_signature":"deb95899a8a22e86d22c2d806419d268c347d91506e1482e4266cd42835454d556c7846cce28fbac0610ca5037de6f506d10884779e9ba16d05d8420536e1cd200"}},{"blockID":"00000000001aea169c4a0f317a7f6bc6a294faaf3276efe9e433fafcc25c6e39","block_header":{"raw_data":{"number":1763862,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea15496401f8f50f4c50e7d60056244f296abf0c6d61e910a756","version":"8","timestamp":"1573825410000"},"witness_signature":"c37c2a77a508b4fa4ad9943071ffdd7492166a521cc7a488cfc49ff8e3e8b27454087e5f0a162db7980a3e6a88aa4fda78da43ad44feda8371167c5dffad7dcc01"}},{"blockID":"00000000001aea17986fe1702528f2727b53400fbaff69457aa818ea359de7b9","block_header":{"raw_data":{"number":1763863,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe63dbd848c3b23ac93b00b52b98cdf229fb4b40","parentHash":"00000000001aea169c4a0f317a7f6bc6a294faaf3276efe9e433fafcc25c6e39","version":"8","timestamp":"1573825413000"},"witness_signature":"cecb67a4600cb543d22374c89fb71b528f6c162888d340cd7bf0c946454bd90016d77a5f561626191c6509f1cd7f6dd6b6e06121030cd9716998ba99e7b7a8a200"}},{"blockID":"00000000001aea181a8efe85c4905c5ae2cd54dcda4893ceafaa8ca760214fcd","block_header":{"raw_data":{"number":1763864,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea17986fe1702528f2727b53400fbaff69457aa818ea359de7b9","version":"8","timestamp":"1573825416000"},"witness_signature":"ee4b72f998666709222e2a905cb2c1329fafc9e26401e26a0a4bdf581eaf479d189188d25d27ff74422f6e7e9f731fdbb9c039df2659be32d02714c7f5d2b4ba01"}},{"blockID":"00000000001aea19bf81986fe69f463618457b6b13ca929c45346fb255c045e0","block_header":{"raw_data":{"number":1763865,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe63dbd848c3b23ac93b00b52b98cdf229fb4b40","parentHash":"00000000001aea181a8efe85c4905c5ae2cd54dcda4893ceafaa8ca760214fcd","version":"8","timestamp":"1573825419000"},"witness_signature":"f57eed045ebb6e665c63783bd1d5b4ce665453dd5c4c51d56302ab605526dc7804300459278351d0b196682bd3ad8937e392b8ecea5edfe6447f5db2f9facba401"}},{"blockID":"00000000001aea1aab6269850e9e9b22f8d7868ab77f42b02114e26152f390e5","block_header":{"raw_data":{"number":1763866,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea19bf81986fe69f463618457b6b13ca929c45346fb255c045e0","version":"8","timestamp":"1573825422000"},"witness_signature":"1320c836d75c5d08fa2ef479b155c59f0265dacd1d5e1d6a8a7247a139bc202d7976fba0a7d76556c5edfc42b3cb45a70b13e6e20029f108656b4d6e43083b9300"}},{"blockID":"00000000001aea1bbb282a991f89fa62426e85b4bcbc2f9640946517cd44ac59","block_header":{"raw_data":{"number":1763867,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe63dbd848c3b23ac93b00b52b98cdf229fb4b40","parentHash":"00000000001aea1aab6269850e9e9b22f8d7868ab77f42b02114e26152f390e5","version":"8","timestamp":"1573825425000"},"witness_signature":"4ebb94749ed9c8b8798cfc5faf1ae9cc2b7407edfa47c48b772f390383dd17ff0bc16f27903b55fab8bb716c4a6938334e4c34c805c1cd1beb4fcc5925d5b93e00"}},{"blockID":"00000000001aea1c85d1b2a37b9657b8c8b853b6735bc3d3044218abbb33f2c8","block_header":{"raw_data":{"number":1763868,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea1bbb282a991f89fa62426e85b4bcbc2f9640946517cd44ac59","version":"8","timestamp":"1573825428000"},"witness_signature":"5eee14a0e48b94447cbd065d030857ab04ecd7000259d978adb7a53d5af003e304aef5656b8568f535beded4a0ae0c42a57ea983bae6c0cfbae9fb2c855ed58a01"}},{"blockID":"00000000001aea1d38d8e538c2743c99efe19cf5f013a11928b79433df89c606","block_header":{"raw_data":{"number":1763869,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe63dbd848c3b23ac93b00b52b98cdf229fb4b40","parentHash":"00000000001aea1c85d1b2a37b9657b8c8b853b6735bc3d3044218abbb33f2c8","version":"8","timestamp":"1573825431000"},"witness_signature":"df83d5d6168a2756705877404d588f7096c8eb50c4b88ed6cbbd16a860fb254d30c5196a7ce5fea9411cf35aa83e390b41d12e44299b5395d13e93d027b4d55701"}},{"blockID":"00000000001aea1e327d774bc7818255b7155b538c6e5443b75acee5e3eb954e","block_header":{"raw_data":{"number":1763870,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea1d38d8e538c2743c99efe19cf5f013a11928b79433df89c606","version":"8","timestamp":"1573825434000"},"witness_signature":"c7a76afe17490f7ab2d834d8595ac4e6866453404055e8ae3577a0bce62969177ecfdf140b0bf1c60bc5e3ab115b0dd1a917ad8879d04b56e90748b432beabf700"}},{"blockID":"00000000001aea1fb51bff5e1caac9f974d4b9cd04e8923aa440523bf1f1990d","block_header":{"raw_data":{"number":1763871,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe63dbd848c3b23ac93b00b52b98cdf229fb4b40","parentHash":"00000000001aea1e327d774bc7818255b7155b538c6e5443b75acee5e3eb954e","version":"8","timestamp":"1573825437000"},"witness_signature":"ad9008d633d30d00bc6a3e7e479247ae3502d6d40816577f55bf6a249bca376d4773b7ec770dc8abdb2147b113614542ff691a103519ab5c6987551293e3492e01"}},{"blockID":"00000000001aea20f563c21bbb1101dede9cba54958e50d012e28d81321b62a0","block_header":{"raw_data":{"number":1763872,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea1fb51bff5e1caac9f974d4b9cd04e8923aa440523bf1f1990d","version":"8","timestamp":"1573825440000"},"witness_signature":"ddec3e59c2e47e065fcb02f814440e3579a5989c0fee43bfc0be55540ea372581573afe9a6ca361efba04f567d679cf1c2021c2863aaae5638060f50c17d74de00"}},{"blockID":"00000000001aea21072439dedf8c74492a638b8861c56e346ac60bc0b552d3bd","block_header":{"raw_data":{"number":1763873,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe63dbd848c3b23ac93b00b52b98cdf229fb4b40","parentHash":"00000000001aea20f563c21bbb1101dede9cba54958e50d012e28d81321b62a0","version":"8","timestamp":"1573825443000"},"witness_signature":"9fba30a5d3b46043a674e3bdce38f377383209f809c0f07480b05c0fa4a27f527fc47f2d36f4854c9515c1ab816ad16d6313d58e4cefc0c51ef64d4818fea5f900"}}]}
     * accountTransction : {"response_code":"00","transaction_count":"478913","response_msg":"success","content":[{"result":"SUCCESS","contract_result":"","amount":"0.01120000","tx_timestamp":"1573824675000","fee":"0","tx_hash":"516d2009595b3a2b2c49c20ae4368aa592fd002716c77a8ef243272f1dc36617","from":"CUbo1uag6b6tW5LYzpuMnxinrRp4Gqdz8B","to":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","asset":"TRX","type":"TransferContract","height":"1763617","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"1.99000000","tx_timestamp":"1573824474000","fee":"0","tx_hash":"136cf4c8fec0dbe5d15a92e27ddab1373aa91af2675ccac4184366099eb1c5dd","from":"CYomVx2mMGPzf4wVznV3Hnq9dDN1e4rpMD","to":"CetEbdhVvLuGRT4K6UW4P9HaApi2NtR7wA","asset":"TRX","type":"TransferContract","height":"1763550","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.01000000","tx_timestamp":"1573821786000","fee":"0","tx_hash":"d2e61c0c0dd1c2a45856f7cab076d7e8da65e5403ce4c8e08a03d6c8dd50d771","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762654","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.01000000","tx_timestamp":"1573821777000","fee":"0","tx_hash":"6115398362881ff973bcb37b4cbd4c228ad2fd2352ac9d6d2177beeb07360e92","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762651","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00100000","tx_timestamp":"1573821555000","fee":"0","tx_hash":"f7e05aaae48e7d87eca8df426821f4c08eabd91f98610b9df8c1d27df8113f7e","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762577","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00100000","tx_timestamp":"1573821435000","fee":"0","tx_hash":"266ba33023221e33f9e2a5aa2cc2fb133bbad22314fda695049a7880dcb36bd1","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762537","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00100000","tx_timestamp":"1573821312000","fee":"0","tx_hash":"a69dd436a3b03b4c81ca70eb7cbb573d1776f8b27fb5f9b873908995712a8729","from":"CUbo1uag6b6tW5LYzpuMnxinrRp4Gqdz8B","to":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","asset":"TRX","type":"TransferContract","height":"1762496","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.12000000","tx_timestamp":"1573821237000","fee":"0","tx_hash":"56a32795edd04c78ca8d73a3b3e0921ab4973ee9b4a31ae4bbe90c01a31d516d","from":"CUbo1uag6b6tW5LYzpuMnxinrRp4Gqdz8B","to":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","asset":"TRX","type":"TransferContract","height":"1762471","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.01000000","tx_timestamp":"1573821177000","fee":"0","tx_hash":"7518b251f6b5030a3488cd60fe04a538bd40b337c24c149a6dc7a34b69210bfb","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762451","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00100000","tx_timestamp":"1573821009000","fee":"0","tx_hash":"84da2ac02bc39445e592e568e1a54e9274eb18b8c77223d02ec3d1dfa8bee03f","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SL52aKgU","asset":"TRX","type":"TransferContract","height":"1762395","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.01000000","tx_timestamp":"1573820139000","fee":"0","tx_hash":"9135cbfc420d30923d05767052d293011fade7ccca044cffd9c2710ec13c5dbc","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762105","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.01000000","tx_timestamp":"1573819869000","fee":"0","tx_hash":"3cdf72cea79183af41d6f9d32620cc51f15f120b9944c0a28d017104796db743","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762015","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.11000000","tx_timestamp":"1573819752000","fee":"0","tx_hash":"410b157f2765aa668b301244c6b8af9a5fd33fec5c2e5ca9cc2edc0f9b2da08c","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1761976","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"1.21000000","tx_timestamp":"1573804332000","fee":"0","tx_hash":"85534f89e5316c4e1c6f7094bb4b8649e0d0963942ece3134aba691360fa5ff4","from":"CUbo1uag6b6tW5LYzpuMnxinrRp4Gqdz8B","to":"CYomVx2mMGPzf4wVznV3Hnq9dDN1e4rpMD","asset":"TRX","type":"TransferContract","height":"1756838","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00800000","tx_timestamp":"1573797759000","fee":"0","tx_hash":"b52791192ea250121f69841ef42a4bed8a82ba241b853ecb56e6f53c0959d328","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1754647","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00500000","tx_timestamp":"1573787490000","fee":"0","tx_hash":"243d415b444b2de502c4ed6632ee34debb4a746b6cbf2834c7e5700cd9d5d5ee","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1751226","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00500000","tx_timestamp":"1573787454000","fee":"0","tx_hash":"f3e872ea79adf9e9c260421efd4df683ebfbd89eba6b7655b354137c144b6a98","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1751214","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00010000","tx_timestamp":"1573787427000","fee":"0","tx_hash":"7fd183980df7222c5cc12c08a6f87472f36a652abe27d4212e37ca9fbaae27f5","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1751205","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00100000","tx_timestamp":"1573787163000","fee":"0","tx_hash":"6cc8aff104c88c631dbb82ca32e9a18b7853ed0de509110a29ce80ea34f6e1b8","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1751117","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00500000","tx_timestamp":"1573782138000","fee":"0","tx_hash":"bf0f159cb438d0ad17a59b057630d7c938ee71b936d4c8acf22a780e9780fe93","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1749442","status":"CONFIRMED"}],"accountBlock":""}
     * values : [{"ccm":657.36,"day":"2019-10-16"},{"ccm":0,"day":"2019-10-17"},{"ccm":918.72,"day":"2019-10-19"},{"ccm":0,"day":"2019-11-10"},{"ccm":139.8491,"day":"2019-11-12"},{"ccm":139.8491,"day":"2019-11-13"},{"ccm":417.3446,"day":"2019-11-14"}]
     * templateId : a8241f1d354f4b08aab91a82ebe797f3
     */

    @SerializedName("allCcm")
    public double allCcm;
    @SerializedName("contribution")
    public double contribution;
    @SerializedName("allRate")
    public double allRate;
    @SerializedName("superNode")
    public int superNode;
    @SerializedName("node")
    public int node;
    @SerializedName("accountBlock")
    public AccountBlockBean accountBlock;
    @SerializedName("accountTransction")
    public AccountTransctionBean accountTransction;
    @SerializedName("templateId")
    public String templateId;
    @SerializedName("values")
    public List<ValuesBean> values;

    public static class AccountBlockBean implements Serializable {
        @SerializedName("block")
        public Object block;
//        public List<BlockBean> block;
        public List<BlockBean> block() {
            try {
                String res = new Gson().toJson(block);
                return new Gson().fromJson(res, new TypeToken<List<BlockBean>>(){}.getType());
            } catch (Exception e) {
                return new ArrayList<>();
            }
        }

        public static class BlockBean implements Serializable{
            /**
             * blockID : 00000000001aea0e1abc39b13d3b9896bc6a3a4fc6f8a61fc8bd4eb3c6ce6071
             * block_header : {"raw_data":{"number":1763854,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea0d48ad45e1e9407dc8202ed543d801495259d4d9e6e8f7ccf0","version":"8","timestamp":"1573825386000"},"witness_signature":"cab77aeb88a3734769b351f6fd7831e6efb7b57157bd0c08369e81c2b32404fd25095271f460c3336800625d379625fb106470db1ecd82ea50a1861dc23d098400"}
             */

            @SerializedName("blockID")
            public String blockID;
            @SerializedName("block_header")
            public BlockHeaderBean blockHeader;

            public static class BlockHeaderBean implements Serializable {
                /**
                 * raw_data : {"number":1763854,"txTrieRoot":"0000000000000000000000000000000000000000000000000000000000000000","witness_address":"1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6","parentHash":"00000000001aea0d48ad45e1e9407dc8202ed543d801495259d4d9e6e8f7ccf0","version":"8","timestamp":"1573825386000"}
                 * witness_signature : cab77aeb88a3734769b351f6fd7831e6efb7b57157bd0c08369e81c2b32404fd25095271f460c3336800625d379625fb106470db1ecd82ea50a1861dc23d098400
                 */

                @SerializedName("raw_data")
                public RawDataBean rawData;
                @SerializedName("witness_signature")
                public String witnessSignature;

                public static class RawDataBean implements Serializable {
                    /**
                     * number : 1763854
                     * txTrieRoot : 0000000000000000000000000000000000000000000000000000000000000000
                     * witness_address : 1cfe34749ad1a967bb7e7df891a52bb5c67ec1a0d6
                     * parentHash : 00000000001aea0d48ad45e1e9407dc8202ed543d801495259d4d9e6e8f7ccf0
                     * version : 8
                     * timestamp : 1573825386000
                     */

                    @SerializedName("number")
                    public int number;
//                    @SerializedName("txTrieRoot")
//                    public String txTrieRoot;
                    @SerializedName("witness_address")
                    public String witnessAddress;
//                    @SerializedName("parentHash")
//                    public String parentHash;
                    @SerializedName("version")
                    public String version;
                    @SerializedName("timestamp")
                    public String timestamp;
                }
            }
        }
    }

    public static class AccountTransctionBean implements Serializable {
        /**
         * response_code : 00
         * transaction_count : 478913
         * response_msg : success
         * content : [{"result":"SUCCESS","contract_result":"","amount":"0.01120000","tx_timestamp":"1573824675000","fee":"0","tx_hash":"516d2009595b3a2b2c49c20ae4368aa592fd002716c77a8ef243272f1dc36617","from":"CUbo1uag6b6tW5LYzpuMnxinrRp4Gqdz8B","to":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","asset":"TRX","type":"TransferContract","height":"1763617","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"1.99000000","tx_timestamp":"1573824474000","fee":"0","tx_hash":"136cf4c8fec0dbe5d15a92e27ddab1373aa91af2675ccac4184366099eb1c5dd","from":"CYomVx2mMGPzf4wVznV3Hnq9dDN1e4rpMD","to":"CetEbdhVvLuGRT4K6UW4P9HaApi2NtR7wA","asset":"TRX","type":"TransferContract","height":"1763550","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.01000000","tx_timestamp":"1573821786000","fee":"0","tx_hash":"d2e61c0c0dd1c2a45856f7cab076d7e8da65e5403ce4c8e08a03d6c8dd50d771","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762654","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.01000000","tx_timestamp":"1573821777000","fee":"0","tx_hash":"6115398362881ff973bcb37b4cbd4c228ad2fd2352ac9d6d2177beeb07360e92","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762651","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00100000","tx_timestamp":"1573821555000","fee":"0","tx_hash":"f7e05aaae48e7d87eca8df426821f4c08eabd91f98610b9df8c1d27df8113f7e","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762577","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00100000","tx_timestamp":"1573821435000","fee":"0","tx_hash":"266ba33023221e33f9e2a5aa2cc2fb133bbad22314fda695049a7880dcb36bd1","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762537","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00100000","tx_timestamp":"1573821312000","fee":"0","tx_hash":"a69dd436a3b03b4c81ca70eb7cbb573d1776f8b27fb5f9b873908995712a8729","from":"CUbo1uag6b6tW5LYzpuMnxinrRp4Gqdz8B","to":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","asset":"TRX","type":"TransferContract","height":"1762496","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.12000000","tx_timestamp":"1573821237000","fee":"0","tx_hash":"56a32795edd04c78ca8d73a3b3e0921ab4973ee9b4a31ae4bbe90c01a31d516d","from":"CUbo1uag6b6tW5LYzpuMnxinrRp4Gqdz8B","to":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","asset":"TRX","type":"TransferContract","height":"1762471","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.01000000","tx_timestamp":"1573821177000","fee":"0","tx_hash":"7518b251f6b5030a3488cd60fe04a538bd40b337c24c149a6dc7a34b69210bfb","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762451","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00100000","tx_timestamp":"1573821009000","fee":"0","tx_hash":"84da2ac02bc39445e592e568e1a54e9274eb18b8c77223d02ec3d1dfa8bee03f","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SL52aKgU","asset":"TRX","type":"TransferContract","height":"1762395","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.01000000","tx_timestamp":"1573820139000","fee":"0","tx_hash":"9135cbfc420d30923d05767052d293011fade7ccca044cffd9c2710ec13c5dbc","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762105","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.01000000","tx_timestamp":"1573819869000","fee":"0","tx_hash":"3cdf72cea79183af41d6f9d32620cc51f15f120b9944c0a28d017104796db743","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1762015","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.11000000","tx_timestamp":"1573819752000","fee":"0","tx_hash":"410b157f2765aa668b301244c6b8af9a5fd33fec5c2e5ca9cc2edc0f9b2da08c","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CKubEkgw733JjQqUX9RmFvdyLqptxa2snw","asset":"TRX","type":"TransferContract","height":"1761976","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"1.21000000","tx_timestamp":"1573804332000","fee":"0","tx_hash":"85534f89e5316c4e1c6f7094bb4b8649e0d0963942ece3134aba691360fa5ff4","from":"CUbo1uag6b6tW5LYzpuMnxinrRp4Gqdz8B","to":"CYomVx2mMGPzf4wVznV3Hnq9dDN1e4rpMD","asset":"TRX","type":"TransferContract","height":"1756838","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00800000","tx_timestamp":"1573797759000","fee":"0","tx_hash":"b52791192ea250121f69841ef42a4bed8a82ba241b853ecb56e6f53c0959d328","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1754647","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00500000","tx_timestamp":"1573787490000","fee":"0","tx_hash":"243d415b444b2de502c4ed6632ee34debb4a746b6cbf2834c7e5700cd9d5d5ee","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1751226","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00500000","tx_timestamp":"1573787454000","fee":"0","tx_hash":"f3e872ea79adf9e9c260421efd4df683ebfbd89eba6b7655b354137c144b6a98","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1751214","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00010000","tx_timestamp":"1573787427000","fee":"0","tx_hash":"7fd183980df7222c5cc12c08a6f87472f36a652abe27d4212e37ca9fbaae27f5","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1751205","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00100000","tx_timestamp":"1573787163000","fee":"0","tx_hash":"6cc8aff104c88c631dbb82ca32e9a18b7853ed0de509110a29ce80ea34f6e1b8","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1751117","status":"CONFIRMED"},{"result":"SUCCESS","contract_result":"","amount":"0.00500000","tx_timestamp":"1573782138000","fee":"0","tx_hash":"bf0f159cb438d0ad17a59b057630d7c938ee71b936d4c8acf22a780e9780fe93","from":"CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh","to":"CN9YKJCyLpddY2hjdaRtqCjQj7SJtT7M3K","asset":"TRX","type":"TransferContract","height":"1749442","status":"CONFIRMED"}]
         * accountBlock :
         */

//        @SerializedName("response_code")
//        public String responseCode;
        @SerializedName("transaction_count")
        public String transactionCount;
//        @SerializedName("response_msg")
//        public String responseMsg;
//        @SerializedName("accountBlock")
//        public String accountBlock;
        @SerializedName("content")
        public Object content;
//        public List<ContentBean> content;
        public List<ContentBean> content() {
            try {
                String res = new Gson().toJson(content);
                return new Gson().fromJson(res, new TypeToken<List<ContentBean>>(){}.getType());
            } catch (Exception e) {
                return new ArrayList<>();
            }
        }

        public static class ContentBean implements Serializable {
            /**
             * result : SUCCESS
             * contract_result :
             * amount : 0.01120000
             * tx_timestamp : 1573824675000
             * fee : 0
             * tx_hash : 516d2009595b3a2b2c49c20ae4368aa592fd002716c77a8ef243272f1dc36617
             * from : CUbo1uag6b6tW5LYzpuMnxinrRp4Gqdz8B
             * to : CfTXa9sMgHMKMvQshzAvY9gzhw5yAfqcmh
             * asset : TRX
             * type : TransferContract
             * height : 1763617
             * status : CONFIRMED
             */

            @SerializedName("result")
            public String result;
//            @SerializedName("contract_result")
//            public String contractResult;
            @SerializedName("amount")
            public String amount;
            @SerializedName("tx_timestamp")
            public String txTimestamp;
//            @SerializedName("fee")
//            public String fee;
            @SerializedName("tx_hash")
            public String txHash;
            @SerializedName("from")
            public String from;
            @SerializedName("to")
            public String to;
//            @SerializedName("asset")
//            public String asset;
            @SerializedName("type")
            public String type;
            @SerializedName("height")
            public String height;
            @SerializedName("status")
            public String status;
        }
    }

    public static class ValuesBean {
        /**
         * ccm : 657.36
         * day : 2019-10-16
         */

        @SerializedName("ccm")
        public double ccm;
        @SerializedName("day")
        public String day;
    }
}
