package com.weiju.ccmall.module.live.adapter;

import android.net.Uri;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.OpeningLiveRecItem;

public class OpeningLiveAdapter extends BaseQuickAdapter<OpeningLiveRecItem, BaseViewHolder> {
    public OpeningLiveAdapter() {
        super(R.layout.item_opening_live);
    }

    @Override
    protected void convert(BaseViewHolder helper, OpeningLiveRecItem item) {
        ImageView ic = helper.getView(R.id.civIcon);
        Glide.with(ic)
                .load(Uri.parse(item.openingMember.headImage))
                .apply(RequestOptions.placeholderOf(R.mipmap.default_avatar))
                .into(ic);
        helper.setText(R.id.tvName, item.openingMember.nickName);
        helper.setText(R.id.tvPhone, item.openingMember.phone);
        helper.setText(R.id.tvTime, item.authorizeDate);
    }
}
