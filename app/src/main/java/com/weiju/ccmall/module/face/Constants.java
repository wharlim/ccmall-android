package com.weiju.ccmall.module.face;

public interface Constants {

    int PREVIEW_WIDTH = 640;
    int PREVIEW_HEIGHT = 480;
    String BLINK = "BLINK";
    String NOD = "NOD";
    String MOUTH = "MOUTH";
    String YAW = "YAW";
    String RESULT = "result";
    String OUTTYPE = "outType";
    int LIVENESS_TRACKING_MISSED = 0x86243332;
    int LIVENESS_SUCCESS = 0x86243331;
    int LIVENESS_TIME_OUT = 0x86243333;
    int DETECT_BEGIN_WAIT = 5000;
    int DETECT_END_WAIT = 5001;
    String ERROR_ACTION_TIME_OUT = "动作超时";
    String ERROR_ACTION_GET_FAIL = "采集失败，再试一次吧";
    String ERROR_DETECT_FAIL = "活体检测失败, 请保持前台运行,点击确定重试";
}
