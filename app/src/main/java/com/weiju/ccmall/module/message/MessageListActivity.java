package com.weiju.ccmall.module.message;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.message.adapter.MessageAdapter;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Message;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IMessageService;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.message
 * @since 2017-07-05
 */
public class MessageListActivity extends BaseActivity implements PageManager.RequestListener {

    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.noDataLayout)
    protected NoData mNoDataLayout;
    private MessageAdapter mMessageAdapter;
    private PageManager mPageManager;
    private IMessageService mMessageService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_list_layout);
        ButterKnife.bind(this);
        showHeader();
        setTitle("消息");
        setLeftBlack();
        mMessageService = ServiceManager.getInstance().createService(IMessageService.class);
        mMessageAdapter = new MessageAdapter(this);
        mRecyclerView.setAdapter(mMessageAdapter);
        try {
            mPageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(mRecyclerView)
                    .setRequestListener(this)
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .build(this);
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        mPageManager.onRefresh();
    }

    @Override
    public void nextPage(final int page) {
        APIManager.startRequest(mMessageService.getMessageList(page), new BaseRequestListener<PaginationEntity<Message, Object>>(mRefreshLayout) {

            @Override
            public void onSuccess(PaginationEntity<Message, Object> result) {
                if (page == 1) {
                    mMessageAdapter.removeAllItems();
                }
                mPageManager.setLoading(false);
                mPageManager.setTotalPage(result.totalPage);
                mMessageAdapter.addItems(result.list);
                mNoDataLayout.setVisibility(result.total > 0 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mPageManager.setLoading(false);
                mRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onComplete() {
                super.onComplete();
                mPageManager.setLoading(false);
                mRefreshLayout.setRefreshing(false);
            }
        },this);
    }
}
