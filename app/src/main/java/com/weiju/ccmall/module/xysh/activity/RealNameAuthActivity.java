package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.realname_auth.RealNameBankCardActivity;
import com.weiju.ccmall.module.xysh.bean.XYSHCommonResult;
import com.weiju.ccmall.module.xysh.helper.UploadHelper;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.manager.UploadManager;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.zhihu.matisse.Matisse;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

public class RealNameAuthActivity extends BaseActivity {
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etIdCard)
    EditText etIdCard;
    @BindView(R.id.iv_idcard_1)
    SimpleDraweeView ivIdcard1;
    @BindView(R.id.iv_idcard_2)
    SimpleDraweeView ivIdcard2;
    @BindView(R.id.iv_idcard_3)
    SimpleDraweeView ivIdcard3;
    @BindView(R.id.tv_next)
    TextView tvNext;

    private String idCard1FilePath;
    private String idCard2FilePath;
    private String idCard3FilePath;

    private static final int REQ_SELECT_ID_CARD_1 = 1;
    private static final int REQ_SELECT_ID_CARD_2 = 2;
    private static final int REQ_SELECT_ID_CARD_3 = 3;

    XyshService service = ServiceManager.getInstance().createService2(XyshService.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realname_auth);
        ButterKnife.bind(this);
        setLeftBlack();
        setTitle("实名认证");
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, RealNameAuthActivity.class);
        context.startActivity(intent);
    }

    @OnClick(R.id.tv_next)
    public void onNext() {
        User user = SessionUtil.getInstance().getLoginUser();
        if (user == null) {
            ToastUtil.error("请先登录");
            return;
        }
//        RealNameBankCardActivity.start(this);
        String name = etName.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            ToastUtil.error("请填写姓名");
            return;
        }
        String idCard = etIdCard.getText().toString().trim();
        if (TextUtils.isEmpty(idCard)) {
            ToastUtil.error("请填写身份证");
            return;
        }
        if (TextUtils.isEmpty(idCard1FilePath)) {
            ToastUtil.error("请上传身份证正面图片");
            return;
        }
        if (TextUtils.isEmpty(idCard2FilePath)) {
            ToastUtil.error("请上传身份证背面图片");
            return;
        }
        if (TextUtils.isEmpty(idCard3FilePath)) {
            ToastUtil.error("请上传手持身份证图片");
            return;
        }
        ToastUtil.showLoading(this, true);
        APIManager.startRequest(service.userPersonRelauth(
                user.phone,
                name,
                idCard,
                SessionUtil.getInstance().getOAuthToken(),
                getPart("idhandlepic", idCard3FilePath),
                getPart("livingbody", idCard3FilePath),
                getPart("idfrontpic", idCard1FilePath),
                getPart("idbackpic", idCard2FilePath)
        ), new Observer<XYSHCommonResult<Object>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(XYSHCommonResult<Object> ret) {
                ToastUtil.hideLoading();
                if ("01".equals(ret.resCode)) {
                    ToastUtil.error(ret.resMsg);
                } else {
                    RealNameBankCardActivity.start(RealNameAuthActivity.this);
                }
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    private MultipartBody.Part getPart(String key, String filePath) {
        RequestBody body = RequestBody.create(MediaType.parse("image/*"), new File(filePath));
        return MultipartBody.Part.createFormData(key, System.currentTimeMillis() + ".jpg", body);
    }

    @OnClick({R.id.iv_idcard_1, R.id.iv_idcard_2, R.id.iv_idcard_3, R.id.tv_next})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_idcard_1:
                UploadManager.selectImage(this, REQ_SELECT_ID_CARD_1, 1);
                break;
            case R.id.iv_idcard_2:
                UploadManager.selectImage(this, REQ_SELECT_ID_CARD_2, 1);
                break;
            case R.id.iv_idcard_3:
                UploadManager.selectImage(this, REQ_SELECT_ID_CARD_3, 1);
                break;
            case R.id.tv_next:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case REQ_SELECT_ID_CARD_1:
                case REQ_SELECT_ID_CARD_2:
                case REQ_SELECT_ID_CARD_3:
                    List<String> paths = Matisse.obtainPathResult(data);
                    Log.d("Seven", "select path -> " + paths.get(0));
//                    File file = UploadManager.uri2File(uris.get(0), this);
//                    Log.d("Seven", "select file -> " + file.getAbsolutePath());
                    // 压缩
                    Luban.with(getApplicationContext())
                            .load(new File(paths.get(0)))
                            .setCompressListener(new OnCompressListener() {
                                @Override
                                public void onStart() {

                                }

                                @Override
                                public void onSuccess(File file) {
                                    Log.d("Seven", "select compress file -> " + file.getAbsolutePath() + " size -> " + (file.length() / 1024) + "Kb");
                                    if (requestCode == REQ_SELECT_ID_CARD_1) {
                                        ivIdcard1.setImageURI(Uri.fromFile(file));
                                        idCard1FilePath = file.getAbsolutePath();
                                    } else if (requestCode == REQ_SELECT_ID_CARD_2) {
                                        ivIdcard2.setImageURI(Uri.fromFile(file));
                                        idCard2FilePath = file.getAbsolutePath();
                                    } else if (requestCode == REQ_SELECT_ID_CARD_3) {
                                        ivIdcard3.setImageURI(Uri.fromFile(file));
                                        idCard3FilePath = file.getAbsolutePath();
                                    }
                                }

                                @Override
                                public void onError(Throwable e) {

                                }
                            })
                            .launch();
            }
        }
    }
}
