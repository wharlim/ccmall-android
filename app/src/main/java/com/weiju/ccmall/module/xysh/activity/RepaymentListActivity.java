package com.weiju.ccmall.module.xysh.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.XyshService;
import com.weiju.ccmall.module.xysh.activity.repayment.RepaymentDetailActivity;
import com.weiju.ccmall.module.xysh.adapter.RepaymentAdapter;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class RepaymentListActivity extends BaseListActivity {


    private RepaymentAdapter mAdapter;
    private XyshService service = ServiceManager.getInstance().createService2(XyshService.class);
    private boolean forceHidePlanBtn;

    private String from;
    public static final String FROM_PLAN = "plan"; // 从还款计划进入
    public static final String FROM_REPAY = "repay"; // 从智能还款入口进入

    {
        from = FROM_PLAN;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mAdapter = new RepaymentAdapter();
        forceHidePlanBtn = getIntent().getBooleanExtra("forceHidePlanBtn", false);
        if (getIntent().hasExtra("from")) {
            from = getIntent().getStringExtra("from");
        }
        mAdapter.setFrom(from);
        super.onCreate(savedInstanceState);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public String getTitleStr() {
        return "设置还款计划";
    }

    private boolean isAllZero(String s) { // 匹配 "0", "00", "000" 等
        return Pattern.matches("^0+$", s);
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {

        QueryUserBankCardResult.BankInfListBean item = mAdapter.getData().get(position);
        if (TextUtils.isEmpty(item.repayDate) || TextUtils.isEmpty(item.billDate) ||
            isAllZero(item.repayDate) || isAllZero(item.billDate)) {
            WJDialog wjDialog = new WJDialog(this);
            wjDialog.show();
            wjDialog.setTitle("提示");
            wjDialog.setContentText("需要设置账单日/还款日,是否去设置？");
            wjDialog.setOnConfirmListener(v -> {
                wjDialog.dismiss();
                Intent intent = new Intent(this, AddEditXinYongActivity.class);
                intent.putExtra("isEdit", true);
                intent.putExtra("bankInfListBean", item);
                startActivity(intent);
            });
            return;
        }

        if (view != null && view.getId() == R.id.tvCheckPlan) {
            RepaymentDetailActivity.start(this, item);
        } else if (view != null && view.getId() == R.id.tvCreatePlan) {
            IntelligenceRepaymentActivity.start(this, item);
        } else {
            if (FROM_PLAN.equals(from) || item.isRunning()) {
                RepaymentDetailActivity.start(this, item);
            } else if (FROM_REPAY.equals(from)) {
                IntelligenceRepaymentActivity.start(this, item);
            }
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public int getContentLayoutRes() {
        return R.layout.activity_repayment_list;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(service.cardInfoGet(0), new Observer<QueryUserBankCardResult>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(QueryUserBankCardResult queryUserBankCardResult) {
                mAdapter.setNewData(queryUserBankCardResult.bankInfList);
                mRefreshLayout.setRefreshing(false);
                mAdapter.loadMoreEnd(true);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @OnClick(R.id.layoutAdd)
    public void AddCard() {
        startActivity(new Intent(this, AddEditXinYongActivity.class));
    }

    @Subscribe
    public void bankChange(EventMessage ev) {
        if (ev.getEvent() == Event.bankChange) {
            getData(true);
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getData(true);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    public static void start(Context context, boolean forceHidePlanBtn, String from) {
        Intent activity = new Intent(context, RepaymentListActivity.class);
        activity.putExtra("forceHidePlanBtn", forceHidePlanBtn);
        activity.putExtra("from", from);
        context.startActivity(activity);
    }
}
