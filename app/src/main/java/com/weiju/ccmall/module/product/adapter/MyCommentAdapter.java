package com.weiju.ccmall.module.product.adapter;

import android.support.annotation.Nullable;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.MyCommentModel;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import java.util.List;

public class MyCommentAdapter extends BaseQuickAdapter<MyCommentModel, BaseViewHolder> {

    boolean mIsNewRetail = false;

    public MyCommentAdapter(@Nullable List<MyCommentModel> data) {
        super(R.layout.item_my_comment, data);
    }

    public MyCommentAdapter(@Nullable List<MyCommentModel> data, boolean isNewRetail) {
        super(R.layout.item_my_comment, data);
        mIsNewRetail = isNewRetail;
    }


    @Override
    protected void convert(BaseViewHolder helper, MyCommentModel item) {
        helper.addOnClickListener(R.id.tvComment);
        helper.addOnClickListener(R.id.tvReadComment);
        helper.addOnClickListener(R.id.tvAddComment);
        FrescoUtil.loadRvItemImg(helper, R.id.ivImage, item.productImage);
        helper.setVisible(R.id.tvComment, item.commentStatus == 0);
        helper.setVisible(R.id.tvReadComment, item.commentStatus != 0);
        helper.setVisible(R.id.tvAddComment, item.commentStatus == 1);
        if (item.deleteStatus == 1) {
            helper.setVisible(R.id.tvAddComment, false);
        } else {
            if (item.commentStatus == 1) {
                helper.setVisible(R.id.tvAddComment, true);
            }
        }


        helper.setText(R.id.tvTitle, item.skuName)
                .setText(R.id.tvPrice, MoneyUtil.centToYuan¥Str(item.realPrice))
                .setText(R.id.tvMarketPrice, MoneyUtil.centToYuan¥Str(item.marketPrice))
                .setText(R.id.tvType, item.properties)
                .setText(R.id.tvCount, "x" + item.quantity);

        if (mIsNewRetail) {

        }

        TextViewUtil.addThroughLine((TextView) helper.getView(R.id.tvMarketPrice));
    }
}
