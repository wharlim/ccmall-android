package com.weiju.ccmall.module.xysh.adapter;

import android.graphics.Color;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.helper.BankUtils;
import static com.weiju.ccmall.module.xysh.activity.RepaymentListActivity.FROM_PLAN;
import static com.weiju.ccmall.module.xysh.activity.RepaymentListActivity.FROM_REPAY;

public class RepaymentAdapter extends BaseQuickAdapter<QueryUserBankCardResult.BankInfListBean, BaseViewHolder> {
    private String from;
    public RepaymentAdapter() {
        super(R.layout.item_bank_repayment);
    }

    @Override
    protected void convert(BaseViewHolder helper, QueryUserBankCardResult.BankInfListBean item) {
        helper.setImageResource(R.id.ivAvatar, BankUtils.getBankIconByName(item.bankName));
        helper.setText(R.id.tvBankName, item.bankName);
        helper.setText(R.id.tvCardNumber, BankUtils.formatBankCardNo(item.cardNo));
        helper.setText(R.id.tvBill, String.format("账单日  %s日", item.billDate));
        helper.setText(R.id.tvRepay, String.format("还款日  %s日", item.repayDate));
        if (item.isRunning()) {
            helper.setImageResource(R.id.iv_state, R.drawable.ic_plan_running);
        } else if (item.isSuccess()) {
            helper.setImageResource(R.id.iv_state, R.drawable.ic_plan_success);
        } else if (item.isFail()){
            helper.setImageResource(R.id.iv_state, R.drawable.ic_plan_fail);
        } else {
            helper.setImageResource(R.id.iv_state, R.drawable.ic_plan_none);
        }
        helper.addOnClickListener(R.id.tvCreatePlan);
        if (FROM_PLAN.equals(from)) {
            helper.setVisible(R.id.tvCreatePlan, !item.isRunning());
            helper.setVisible(R.id.tvCheckPlan, true);
        } else if (FROM_REPAY.equals(from)) {
            helper.setVisible(R.id.tvCreatePlan, false);
            helper.setText(R.id.tvCheckPlan, item.isRunning() ? "查看计划": "点击创建计划");
        }
    }

    public void setFrom(String from) {
        this.from = from;
    }
}
