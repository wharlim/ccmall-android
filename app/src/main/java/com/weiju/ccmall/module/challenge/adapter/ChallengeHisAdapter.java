package com.weiju.ccmall.module.challenge.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Challenge;

import java.util.List;

/**
 * @author chenyanming
 * @time 2019/1/28 on 17:25
 * @desc ${TODD}
 */
public class ChallengeHisAdapter extends BaseQuickAdapter<Challenge, BaseViewHolder> {
    public ChallengeHisAdapter(@Nullable List<Challenge> data) {
        super(R.layout.item_challeng_his_layout, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, Challenge item) {

        if (item.type == 0) {
            String format = String.format("挑战开始后%s小时内成功销售 " +
                    " %s 份大礼包,可获得 %s 幸运积分", item.lifeCycle, item.target, item.score);

            helper.setText(R.id.tvTitle, format);
        } else if (item.type == 1) {
            String format = String.format("%s小时内成功升级至 " +
                    " %s ,可获得 %s 幸运积分", item.lifeCycle, item.target, item.score);
            helper.setText(R.id.tvTitle, format);
        } else {
            helper.setText(R.id.tvTitle, mContext.getResources().getString(R.string.challeng_item_pk_tip, item.lifeCycle, item.getTypeString(), "奖励"));
        }

        if (item.type>1){
            helper.setImageResource(R.id.ivAvatar, item.activityStatus == 2 ? R.drawable.ic_challeng_pk_success : R.drawable.ic_challeng_pk_error);
        }else {
            helper.setImageResource(R.id.ivAvatar, item.activityStatus == 2 ? R.drawable.ic_challeng_success : R.drawable.ic_challeng_error);
        }
        helper.setText(R.id.tvDate, String.format("%s 至 %s", item.startDate, item.endDate));
    }


}
