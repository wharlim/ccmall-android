package com.weiju.ccmall.module.user.adapter;


import android.graphics.drawable.Drawable;
import android.os.Build;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.ContributionSort;
import com.weiju.ccmall.shared.util.FrescoUtil;

import java.util.Locale;


/**
 * @author chenyanming
 * @time 2019/6/24 on 15:01
 * @desc ${TODD}
 */
public class MyContributionSortAdapter extends BaseQuickAdapter<ContributionSort, BaseViewHolder> {
    public MyContributionSortAdapter() {
        super(R.layout.item_my_contribution_sort);
    }

    @Override
    protected void convert(BaseViewHolder helper, ContributionSort item) {
        helper.setVisible(R.id.ivIndexTop, item.sortIndex < 4);
        helper.setVisible(R.id.ivIndexRight, item.sortIndex < 4);
        helper.setVisible(R.id.tvIndexRight, item.sortIndex > 3 && item.sortIndex < 11);
        helper.setVisible(R.id.tvIndex, item.sortIndex > 10);

        setIndexIcon("ic_sort_top_%d", item.sortIndex, R.drawable.ic_sort_top_1, R.id.ivIndexTop, helper);
        setIndexIcon("ic_sort_right_%d", item.sortIndex, R.drawable.ic_sort_right_1, R.id.ivIndexRight, helper);

        helper.setText(R.id.tvIndexRight, String.valueOf(item.sortIndex));
        helper.setText(R.id.tvIndex, String.format("No.%s", item.sortIndex));

        FrescoUtil.setImageSmall(helper.getView(R.id.ivAvatar), item.headImage);
        helper.setText(R.id.tvName, item.nickName);
        helper.setText(R.id.tvPhone, item.phone);
    }

    private void setIndexIcon(String format, int index, int defaule, int imageViewId, BaseViewHolder helper) {
        String drawableName = String.format(Locale.CHINA, format, index);
        int resId = MyApplication.getInstance().getResources().getIdentifier(drawableName, "drawable", MyApplication.getInstance().getPackageName());
        if (resId != 0) {
            Drawable drawable;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawable = MyApplication.getInstance().getResources().getDrawable(resId, MyApplication.getInstance().getTheme());
            } else {
                drawable = MyApplication.getInstance().getResources().getDrawable(resId);
            }
            if (drawable == null) {
                drawable = mContext.getResources().getDrawable(defaule);
            }
            helper.setImageDrawable(imageViewId, drawable);
        }

    }

}
