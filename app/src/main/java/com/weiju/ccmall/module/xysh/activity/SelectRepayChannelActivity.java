package com.weiju.ccmall.module.xysh.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.xysh.bean.ChannelItem;
import com.weiju.ccmall.module.xysh.bean.QueryUserBankCardResult;
import com.weiju.ccmall.module.xysh.fragment.AuthChannelDialogFragment;
import com.weiju.ccmall.module.xysh.fragment.PayChannelFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;

/**
 * 选择还款通道
 */
public class SelectRepayChannelActivity extends BaseActivity implements PayChannelFragment.OnChannelClickListener {

    private QueryUserBankCardResult.BankInfListBean creditCard;
    private int channelType;
    private PayChannelFragment mPayChannelFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_repay_channel);
        creditCard = (QueryUserBankCardResult.BankInfListBean) getIntent().getSerializableExtra("creditCard");
        channelType = getIntent().getIntExtra("channelType", 0);
        setTitle("选择还款通道");
        setLeftBlack();
        initPayChannels();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);
        if (fragment instanceof PayChannelFragment) {
            mPayChannelFragment = (PayChannelFragment)fragment;
            mPayChannelFragment.setOnChannelClickListener(this);
        }
    }

    private void initPayChannels() {

        getSupportFragmentManager().beginTransaction()
                .add(R.id.fl_pay_channels, PayChannelFragment.newInstance(creditCard, channelType))
                .commit();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (mPayChannelFragment !=null) {
            mPayChannelFragment.getData(true);
        }
    }

    @Override
    public void onChannelClick(int position, ChannelItem channel) {
        Intent data = new Intent();
        data.putExtra("channel", channel);
        setResult(RESULT_OK, data);
        finish();
    }

    public static void startForResult(Activity activity, QueryUserBankCardResult.BankInfListBean creditCard, int channelType, int reqCode) {
        Intent intent = new Intent(activity, SelectRepayChannelActivity.class);
        intent.putExtra("creditCard", creditCard);
        intent.putExtra("channelType", channelType);
        activity.startActivityForResult(intent, reqCode);
    }
}
