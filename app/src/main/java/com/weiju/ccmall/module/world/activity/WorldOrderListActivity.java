package com.weiju.ccmall.module.world.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.world.fragment.WorldOrderListFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.util.ConvertUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Ben
 * @date 2020/5/9.
 */
public class WorldOrderListActivity extends BaseActivity {

    @BindView(R.id.magicIndicator)
    protected MagicIndicator mMagicIndicator;
    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;

    private CommonNavigator mCommonNavigator;
    protected List<BaseFragment> mFragments = new ArrayList<>();
    protected List<Page> mPages = new ArrayList<>();
    private String mType;

    public static void start(Context context, String type) {
        Intent intent = new Intent(context, WorldOrderListActivity.class);
        intent.putExtra("type", type);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_world_order_list);
        ButterKnife.bind(this);
        QMUIStatusBarHelper.translucent(this);
        initData();
        initView();
        selectTabItem(mType);
    }

    private void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mType = bundle.getString("type");
        }
        setData();
    }

    private void setData() {
        mPages.add(new Page("", "全部"));
        mPages.add(new Page("3","待发货" ));
        mPages.add(new Page("4", "已发货"));
        mPages.add(new Page("5", "已完成"));
        for (Page page : mPages) {
            mFragments.add(WorldOrderListFragment.newInstance(page));
        }
    }

    private void initView() {
        setTitle("我的订单");
        mHeaderLayout.setVisibility(View.GONE);
        initViewPager();
        initIndicator();
    }

    private void initViewPager() {
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mPages.get(position).name;
            }
        });
        mViewPager.setOffscreenPageLimit(mFragments.size());
    }

    private void initIndicator() {
        mCommonNavigator = new CommonNavigator(this);
        mCommonNavigator.setAdjustMode(true);
        mCommonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mPages.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(mPages.get(index).name);
                titleView.setNormalColor(getResources().getColor(R.color.text_gray));
                titleView.setSelectedColor(getResources().getColor(R.color.color_8B57F4));
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(16);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getResources().getColor(R.color.color_8B57F4));
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                return indicator;
            }
        });
        mMagicIndicator.setNavigator(mCommonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

    public void selectTabItem(String type) {
        int index = 0;
        for (Page page : mPages) {
            if (page.id.equalsIgnoreCase(type)) {
                index = mPages.indexOf(page);
                break;
            }
        }
        mViewPager.setCurrentItem(index);
    }
}
