package com.weiju.ccmall.module.community.publish;

import android.support.v4.content.ContextCompat;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.TopicLibraryModel;

import java.util.ArrayList;

/**
 * @author Stone
 * @time 2018/4/16  11:36
 * @desc ${TODD}
 */

public class LinkHisQuickAdapter extends BaseQuickAdapter<TopicLibraryModel, BaseViewHolder> {

    public LinkHisQuickAdapter() {
        super(R.layout.item_link_history_publish);
    }


    @Override
    protected void convert(BaseViewHolder helper, TopicLibraryModel item) {
        TextView itemStatusTv = helper.getView(R.id.item_order_status_tv);
        BlockView gridView = helper.getView(R.id.item_image_gv);
        itemStatusTv.setText(item.statusStr);
        if (item.status == 0) {
            gridView.setRetryCommit(false);
            itemStatusTv.setTextColor(ContextCompat.getColor(mContext, R.color.color_66));
        } else if (item.status == 1) {
            gridView.setRetryCommit(false);
            itemStatusTv.setTextColor(ContextCompat.getColor(mContext, R.color.color_ac87_90));
        } else {
            gridView.setRetryCommit(true);
            itemStatusTv.setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }

        TextView tvContent = helper.getView(R.id.item_content_iv);
        tvContent.setText(item.content);
        int type = item.type;
        helper.setVisible(R.id.tvLink, type == 3);
        helper.setText(R.id.tvLink, item.linkTitle);
        if (type == 3) {
            tvContent.setMaxLines(1);
        } else {
            tvContent.setMaxLines(2);
        }

        ArrayList<String> mediaImgs = new ArrayList<>();
        if (item.imageUrls.size() > 4) {
            mediaImgs.addAll(item.imageUrls.subList(0, 4));
        } else {
            mediaImgs.addAll(item.imageUrls);
        }
        gridView.setIsVideo(false);
        gridView.setImages(mediaImgs);

        helper.setText(R.id.item_hour_num_tv, item.createDate + "  共" + item.imageUrls.size() + "张");
    }


}
