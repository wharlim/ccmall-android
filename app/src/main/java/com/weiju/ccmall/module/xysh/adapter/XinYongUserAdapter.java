package com.weiju.ccmall.module.xysh.adapter;

import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.XinYongUser;

/**
 * @author chenyanming
 * @time 2019/12/11 on 16:38
 * @desc
 */
public class XinYongUserAdapter extends BaseQuickAdapter<XinYongUser, BaseViewHolder> {
    public XinYongUserAdapter() {
        super(R.layout.item_xinyong_user);
    }

    @Override
    protected void convert(BaseViewHolder helper, XinYongUser item) {
        helper.setText(R.id.tvTitle, item.title);
        helper.setImageResource(R.id.ivIcon, item.drawableTop);
        helper.setText(R.id.tvDesc, item.desc);
        View view = helper.getView(R.id.tvDesc);
        view.setVisibility(TextUtils.isEmpty(item.desc) ? View.INVISIBLE : View.VISIBLE);
    }
}
