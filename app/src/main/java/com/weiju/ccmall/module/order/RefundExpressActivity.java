package com.weiju.ccmall.module.order;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbruyelle.rxpermissions.RxPermissions;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.qrcode.ScanActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.RefundsOrder;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.PermissionsUtils;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.functions.Action1;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.module.order
 * @since 2017-07-19
 */
public class RefundExpressActivity extends BaseActivity {

    @BindView(R.id.expressNameEt)
    protected EditText mExpressNameEt;
    @BindView(R.id.expressCodeEt)
    protected EditText mExpressCodeEt;
    @BindView(R.id.phoneTv)
    TextView mPhoneTv;
    @BindView(R.id.contactsTv)
    TextView mContactsTv;
    @BindView(R.id.addressDetailTv)
    TextView mAddressDetailTv;
    @BindView(R.id.rlUser)
    RelativeLayout mRlUser;

    private String mRefundId;
    private IOrderService mOrderService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund_express);
        ButterKnife.bind(this);
        showHeader();
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            mRefundId = intent.getExtras().getString("refundId");
        }
        if (mRefundId == null) {
            ToastUtil.error("参数错误");
            finish();
            return;
        }
        Serializable storeBean = getIntent().getSerializableExtra("storeBean");
        if (storeBean instanceof RefundsOrder.StoreBeanEntity) {
            RefundsOrder.StoreBeanEntity bean = (RefundsOrder.StoreBeanEntity) storeBean;
            mRlUser.setVisibility(View.VISIBLE);
            mContactsTv.setText(String.format("收货人：%s", bean.contact));
            mPhoneTv.setText(bean.phone);
            mAddressDetailTv.setText(String.format("收货地址：%s%s%s%s", bean.province, bean.city, bean.district, bean.address));
        }
        setTitle("填写物流信息");
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_black);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        initData();

        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void initData() {
        mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
        APIManager.startRequest(mOrderService.getRefundDetail(mRefundId,null), new BaseRequestListener<RefundsOrder>(this) {

            @Override
            public void onSuccess(RefundsOrder result) {
                super.onSuccess(result);
                if (result.apiRefundOrderBean.refundStatus == 2) {
                    String expressName = result.apiRefundOrderBean.refundGoodsExpressName;
                    final String expressCode = result.apiRefundOrderBean.refundGoodsExpressCode;
                    mExpressCodeEt.setText(expressCode);
                    mExpressNameEt.setText(expressName);
                }
            }
        },this);
    }

    @OnClick(R.id.ivScan)
    public void onViewClicked() {
        new RxPermissions(this).request(Manifest.permission.CAMERA).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (aBoolean) {
                    startActivity(new Intent(RefundExpressActivity.this, ScanActivity.class));
                } else {
                    PermissionsUtils.goPermissionsSetting(RefundExpressActivity.this);
                    ToastUtil.error("无法获得相机权限");
                }
            }
        });
    }

    @OnClick(R.id.submitBtn)
    protected void onSubmit() {
        String expressName = mExpressNameEt.getText().toString();
        if (expressName.isEmpty()) {
            ToastUtil.error("请输入物流公司");
            return;
        }
        String expressCode = mExpressCodeEt.getText().toString();
        if (expressCode.isEmpty()) {
            ToastUtil.error("请输入快递单号");
            return;
        }

        APIManager.startRequest(mOrderService.refundExpress(mRefundId, expressName, expressCode), new BaseRequestListener<Object>(this) {
            @Override
            public void onSuccess(Object result) {
                EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_REFUND_CHANGE));
                ToastUtil.success("提交成功");
                finish();
            }
        },this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(MsgStatus status) {
        switch (status.getAction()) {
            case MsgStatus.ACTION_SCAN_SUCCEED:
                mExpressCodeEt.setText(status.getCode());
                break;
            default:
        }
    }
}
