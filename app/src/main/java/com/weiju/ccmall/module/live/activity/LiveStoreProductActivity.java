package com.weiju.ccmall.module.live.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.blankj.utilcode.utils.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.adapter.LiveStoreProductAdapter;
import com.weiju.ccmall.shared.basic.BaseListActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * @author Ben
 * @date 2020/4/16.
 */
public class LiveStoreProductActivity extends BaseListActivity {

    private LiveStoreProductAdapter mAdapter = new LiveStoreProductAdapter(null);
    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);
    private String mLiveId;

    @Override
    public void getIntentData() {
        super.getIntentData();
        mLiveId = getIntent().getStringExtra("liveId");
    }

    @Override
    public void initView() {
        super.initView();
        mRecyclerView.setBackgroundColor(getResources().getColor(R.color.bg_gray));
        mRecyclerView.setPadding(0, SizeUtils.dp2px(5),0,0);
    }

    @Override
    public boolean isShowListDivider() {
        return true;
    }

    @Override
    public String getTitleStr() {
        return "直播小店产品";
    }

    @Override
    public void onListItemClick(BaseQuickAdapter adapter, View view, int position) {
        List<SkuInfo> data = mAdapter.getData();
        SkuInfo skuInfo = data.get(position);
        APIManager.startRequest(mService.addLiveSkuRelation(mLiveId, skuInfo.skuId),
                new BaseRequestListener<Object>(LiveStoreProductActivity.this) {
                    @Override
                    public void onSuccess(Object result) {
                        ToastUtil.success("添加成功");
                        EventBus.getDefault().post(new EventMessage(Event.addLiveProduct));
                        finish();
                    }
                }, LiveStoreProductActivity.this);
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        return mAdapter;
    }

    @Override
    public void getData(boolean isRefresh) {
        APIManager.startRequest(mService.productList(mCurrentPage, 10),
                new BaseRequestListener<PaginationEntity<SkuInfo, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                        super.onSuccess(result);
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd(isRefresh && result.list.size() < 5);
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, this);
    }

    public static void start(Context context, String liveId) {
        Intent intent = new Intent(context, LiveStoreProductActivity.class);
        intent.putExtra("liveId", liveId);
        context.startActivity(intent);
    }

}
