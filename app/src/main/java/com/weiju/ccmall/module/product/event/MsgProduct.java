package com.weiju.ccmall.module.product.event;

import com.weiju.ccmall.module.auth.event.BaseMsg;
import com.weiju.ccmall.shared.bean.SkuInfo;

public class MsgProduct extends BaseMsg {

    public static final int DEL_VIEW_HOSTORY = 1 << 0;
    private SkuInfo mSkuInfo;

    public MsgProduct(int action) {
        super(action);
    }


    public void setSkuInfo(SkuInfo skuInfo) {
        mSkuInfo = skuInfo;
    }

    public SkuInfo getSkuInfo() {
        return mSkuInfo;
    }
}
