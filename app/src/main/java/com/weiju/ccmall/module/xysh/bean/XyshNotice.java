package com.weiju.ccmall.module.xysh.bean;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

public class XyshNotice {

    /**
     * displayStatus : 1
     * content : {"bulletinId":"3a04d4cd78204036a4fe0496cc0c7113","bulletinName":"信用生活公告","status":1,"content":"公告内容: 版本更新","moduleType":1,"startDate":"2020-02-05 20:11:03","endDate":"2020-02-06 20:11:03","createDate":"2020-02-05 20:11:03","updateDate":"2020-02-06 13:04:35","deleteFlag":0}
     */

    @SerializedName("displayStatus")
    public int displayStatus;
    @SerializedName("content")
    public Object content;

    public ContentBean getContent() {
        Gson gson = new Gson();
        String json = gson.toJson(content);
        try {
            return gson.fromJson(json, ContentBean.class);
        } catch (Exception e) {
            return new ContentBean();
        }
    }

    public static class ContentBean {
        /**
         * bulletinId : 3a04d4cd78204036a4fe0496cc0c7113
         * bulletinName : 信用生活公告
         * status : 1
         * content : 公告内容: 版本更新
         * moduleType : 1
         * startDate : 2020-02-05 20:11:03
         * endDate : 2020-02-06 20:11:03
         * createDate : 2020-02-05 20:11:03
         * updateDate : 2020-02-06 13:04:35
         * deleteFlag : 0
         */

        @SerializedName("bulletinId")
        public String bulletinId;
        @SerializedName("bulletinName")
        public String bulletinName;
        @SerializedName("status")
        public int status;
        @SerializedName("content")
        public String content;
        @SerializedName("moduleType")
        public int moduleType;
        @SerializedName("startDate")
        public String startDate;
        @SerializedName("endDate")
        public String endDate;
        @SerializedName("createDate")
        public String createDate;
        @SerializedName("updateDate")
        public String updateDate;
        @SerializedName("deleteFlag")
        public int deleteFlag;
    }
}
