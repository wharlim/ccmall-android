package com.weiju.ccmall.module.user;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMFriendshipManager;
import com.tencent.imsdk.TIMManager;
import com.tencent.imsdk.TIMUserProfile;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.umeng.socialize.utils.Log;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.UserToken;
import com.weiju.ccmall.shared.bean.WeChatLoginModel;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PushManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.UserService;
import com.weiju.ccmall.shared.service.contract.ICaptchaService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.CountDownRxUtils;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class LoginActivity extends BaseActivity {


    @BindView(R.id.etPhone)
    EditText mEtPhone;
    @BindView(R.id.etMsgCode)
    EditText mEtMsgCode;
    @BindView(R.id.tvGetMsg)
    TextView mTvGetMsg;
    @BindView(R.id.layoutMsgCode)
    RelativeLayout mLayoutMsgCode;
    @BindView(R.id.etPassword)
    EditText mEtPassword;
    @BindView(R.id.layoutPassword)
    RelativeLayout mLayoutPassword;
    @BindView(R.id.tvFindPassword)
    TextView mTvFindPassword;
    @BindView(R.id.tvLogin)
    TextView mTvLogin;
    @BindView(R.id.ivShowPassword)
    ImageView mIvShowPassword;
    @BindView(R.id.tvRegister)
    TextView mTvRegister;

    public final static int TYPE_MSG_CODE = 1 << 0;
    public final static int TYPE_PASSWORD = 1 << 1;
    @BindView(R.id.ivLogo)
    ImageView mIvLogo;
    @BindView(R.id.layoutLoginOther)
    LinearLayout mLayoutLoginOther;

    private IUserService mUserService;
    private int mType;
    private ICaptchaService mCaptchaService;
    private boolean isUnregister = false;
    private String mPhone;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getIntentData();
        initView();
        initData();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mEtPhone.requestFocus();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mType == TYPE_PASSWORD) {
            isUnregister = false;
        }
        ToastUtil.hideLoading();
    }

    private void initData() {
        mUserService = ServiceManager.getInstance().createService(IUserService.class);
        mCaptchaService = ServiceManager.getInstance().createService(ICaptchaService.class);
    }

    private void initView() {
        setTitleNoLine();
        QMUIStatusBarHelper.translucent(this);
        QMUIStatusBarHelper.setStatusBarLightMode(this);
        setBarPadingHeight(QMUIStatusBarHelper.getStatusbarHeight(this));
        setBarPadingColor(Color.WHITE);

        switch (mType) {
            case TYPE_MSG_CODE:
                setTitle("手机登录");
                setLeftBlack();
                mLayoutMsgCode.setVisibility(View.VISIBLE);
                mLayoutPassword.setVisibility(View.GONE);
                mLayoutLoginOther.setVisibility(View.GONE);
                mEtPhone.setText(mPhone);
                break;
            case TYPE_PASSWORD:
                setTitle("登录");
                setLeftBlack();
                mLayoutMsgCode.setVisibility(View.GONE);
                mLayoutPassword.setVisibility(View.VISIBLE);
                mLayoutLoginOther.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    private void getIntentData() {
        mType = getIntent().getIntExtra(Config.INTENT_KEY_TYPE_NAME, TYPE_PASSWORD);
        mPhone = getIntent().getStringExtra("phone");
    }

    private void loginByPassword(String phone, String password, int type) {
        if (StringUtils.isEmpty(phone)) {
            phone = mEtPhone.getText().toString();
        }
        if (StringUtils.isEmpty(password)) {
            password = mEtPassword.getText().toString();
        } else {
            password = StringUtil.md5(password);
        }
        if (StringUtils.isEmpty(password)) {
            ToastUtil.error("请输入密码");
            return;
        }
        APIManager.startRequest(
                mUserService.login(phone, password, 1, type)
                        .flatMap(new FunctionGetUserInfo()), new BaseRequestListener<User>(this) {
                    @Override
                    public void onSuccess(User user) {
                        super.onSuccess(user);
                        loginSucceed(user);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                    }
                }, this);
    }

    public static void updateIMUserProfile() {
        if (SessionUtil.getInstance().getLoginUser() == null) { // 未登录，无法更新im昵称
            return;
        }
        HashMap<String, Object> profileMap = new HashMap<>();
        profileMap.put(TIMUserProfile.TIM_PROFILE_TYPE_KEY_NICK, TextUtils.isEmpty(SessionUtil.getInstance().getLoginUser().storeName) ? SessionUtil.getInstance().getLoginUser().nickname : SessionUtil.getInstance().getLoginUser().storeName);
        profileMap.put(TIMUserProfile.TIM_PROFILE_TYPE_KEY_FACEURL, TextUtils.isEmpty(SessionUtil.getInstance().getLoginUser().avatar) ? "" : SessionUtil.getInstance().getLoginUser().avatar);
        TIMFriendshipManager.getInstance().modifySelfProfile(profileMap, new TIMCallBack() {
            @Override
            public void onError(int code, String desc) {
                Log.e("ljm", "更新昵称失败");
                android.util.Log.d("TIMFriendshipManager", "更新昵称失败" + code + " " + desc);
//                                Log.e(tag, "modifySelfProfile failed: " + code + " desc" + desc);
            }

            @Override
            public void onSuccess() {
                android.util.Log.d("TIMFriendshipManager", "更新昵称成功");
                Log.e("ljm", "更新昵称成功");
            }
        });
    }

    private void loginSucceed(User user) {
        UserService.login(user);

        APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
        }, this);

        LiveManager.getRongyunToken(LoginActivity.this, null, null, null, 0,false);

        PushManager.setJPushInfo(this, user);
        EventBus.getDefault().post(new EventMessage(Event.loginSuccess));
    }

    private void loginByMsgCode() {
        String msgCode = mEtMsgCode.getText().toString();
        if (StringUtils.isEmpty(msgCode)) {
            ToastUtil.error("请输入验证码");
            return;
        }
        APIManager.startRequest(mUserService.login(mEtPhone.getText().toString(), msgCode, 1, 1)
                .flatMap(new FunctionGetUserInfo()), new BaseRequestListener<User>(this) {
            @Override
            public void onSuccess(User user) {
                super.onSuccess(user);
                loginSucceed(user);
            }
        }, this);
    }

    public static void loginTXchat() {//登录聊天模块

        new Thread() {
            @Override
            public void run() {
//                Looper.prepare();
                TIMManager.getInstance().login(userToken.memberId, userToken.tencentIMUserSig, new TIMCallBack() {
                    @Override
                    public void onError(int code, String s) {
//                        ToastUtil.error("报错");
//                        Looper.loop();
                        android.util.Log.d("TIMFriendshipManager", "IM登录失败" + code + " " + s);
                    }

                    @Override
                    public void onSuccess() {
//                        ToastUtil.success("聊天登录成功");
                        android.util.Log.d("TIMFriendshipManager", "IM登录成功");
                        updateIMUserProfile();
//                        Looper.loop();
                    }
                });
            }
        }.start();


    }

    private void getAccessToken(String code) {
        APIManager.startRequest(mUserService.getAccessToken(code), new BaseRequestListener<WeChatLoginModel>(this) {
            @Override
            public void onSuccess(WeChatLoginModel result) {
                super.onSuccess(result);
                if (result.registerStatus == 1) {
                    // 直接登录
                    loginByPassword(result.unionid, result.unionid, 2);
                } else {
                    // 跳到信息完善
                    goWxRegister(result);
                    finish();
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                LogUtils.e("微信请求 getAccessToken" + e.getMessage());
            }

            private void goWxRegister(WeChatLoginModel weChatLoginModel) {
                Intent intent = new Intent(LoginActivity.this, WxRegisterActivity.class);
                intent.putExtra("data", weChatLoginModel);
                startActivity(intent);
            }
        }, this);
    }

    @OnClick(R.id.ivShowPassword)
    public void onMIvShowPasswordClicked() {
        mIvShowPassword.setSelected(!mIvShowPassword.isSelected());
        if (mIvShowPassword.isSelected()) {
            mEtPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        } else {
            mEtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        }
        mEtPassword.setSelection(mEtPassword.length());
    }

    @OnClick(R.id.tvMsgCodeLogin)
    public void onMTvPasswordClicked() {
        isUnregister = true;

        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra(Config.INTENT_KEY_TYPE_NAME, TYPE_MSG_CODE);
        intent.putExtra("phone", mEtPhone.getText().toString());
        startActivity(intent);
    }

    @OnClick(R.id.tvFindPassword)
    public void onMTvFindPasswordClicked() {
        startActivity(new Intent(this, FindPasswordActivity.class));
    }

    @OnClick(R.id.tvGetMsg)
    public void onViewClicked() {
        if (UiUtils.checkETPhone(mEtPhone)) {
            APIManager.startRequest(mCaptchaService.getCaptchaForRegister(mEtPhone.getText().toString()), new BaseRequestListener<Object>(this) {

                @Override
                public void onSuccess(Object result) {
                    CountDownRxUtils.textViewCountDown(mTvGetMsg, 60, "获取验证码");
                }
            }, this);
        }
    }

    @OnClick(R.id.tvRegister)
    public void onGoRegister() {
        startActivity(new Intent(this, NewRegisterActivity.class));
    }

    @OnClick(R.id.tvLogin)
    public void onMTvLoginClicked() {
        if (UiUtils.checkETPhone(mEtPhone)) {
            switch (mType) {
                case TYPE_MSG_CODE:
                    loginByMsgCode();
                    break;
                case TYPE_PASSWORD:
                    loginByPassword(null, null, 0);
                    break;
                default:
                    break;
            }
        }
    }

    @OnClick(R.id.tvWxLogin)
    public void onViewClickedLogin() {
        ToastUtil.showLoading(this);
        final SendAuth.Req req = new SendAuth.Req();
        req.scope = "snsapi_userinfo";
//        req.state = TimeUtils.getNowTimeString();
        IWXAPI api = WXAPIFactory.createWXAPI(this, BuildConfig.WX_APP_ID.trim());
        api.sendReq(req);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(EventMessage message) {
        if (message.getEvent().equals(Event.loginSuccess)) {
            finish();
        } else if (message.getEvent().equals(Event.wxLoginSuccess) && !isUnregister) {
            LogUtils.e("微信请求 getAccessToken" + (String) message.getData());
            getAccessToken((String) message.getData());
        } else if (message.getEvent().equals(Event.wxLoginCancel)) {
            ToastUtil.hideLoading();
        }
    }

    private static UserToken userToken;

    public static class FunctionGetUserInfo implements Function<RequestResult<UserToken>, ObservableSource<RequestResult<User>>> {
        private IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);

        @Override
        public ObservableSource<RequestResult<User>> apply(RequestResult<UserToken> userTokenRequestResult) {
            if (userTokenRequestResult.isFail()) {
                throw new RuntimeException(userTokenRequestResult.message);
            }

            userToken = userTokenRequestResult.data;
            /*Looper.prepare();

            Looper.loop();*/
            SessionUtil.getInstance().setOAuthToken(userTokenRequestResult.data.token);
            SessionUtil.getInstance().setTIMInfo(userToken.tencentIMUserSig, userToken.memberId);
            return mUserService.getUserInfo();
        }
    }
}
