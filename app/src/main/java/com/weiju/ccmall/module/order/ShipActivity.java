package com.weiju.ccmall.module.order;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tbruyelle.rxpermissions.RxPermissions;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.qrcode.ScanActivity;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.PermissionsUtils;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import rx.functions.Action1;


public class ShipActivity extends BaseActivity {

    @BindView(R.id.etName)
    EditText mEtName;
    @BindView(R.id.etCode)
    EditText mEtCode;
    @BindView(R.id.ivScan)
    ImageView mIvScan;
    @BindView(R.id.etRemark)
    EditText mEtRemark;
    @BindView(R.id.tvSubmit)
    TextView mTvSubmit;
    @BindView(R.id.phoneTv)
    TextView mPhoneTv;
    @BindView(R.id.contactsTv)
    TextView mContactsTv;
    @BindView(R.id.addressDetailTv)
    TextView mAddressDetailTv;
    @BindView(R.id.rlUser)
    RelativeLayout mRlUser;
    @BindView(R.id.tvSellerMessage)
    TextView mTvSellerMessage;
    private IOrderService mService;
    private String mOrderCode;
    private Order.OrderMain mOrderMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ship);
        ButterKnife.bind(this);
        mOrderCode = getIntent().getStringExtra("orderCode");
        mRlUser.setVisibility(View.GONE);
        setTitle("扫描快递单号");
        if (TextUtils.isEmpty(mOrderCode)) {
            setTitle("发货");
            mOrderMain = (Order.OrderMain) getIntent().getSerializableExtra("orderMain");
            mOrderCode = mOrderMain.orderCode;
            mRlUser.setVisibility(View.VISIBLE);
            mContactsTv.setText(String.format("收货人：%s", mOrderMain.contact));
            mPhoneTv.setText(mOrderMain.phone);
            mAddressDetailTv.setText(String.format("收货地址：%s%s%s%s", mOrderMain.province, mOrderMain.city, mOrderMain.district, mOrderMain.detail));
            mTvSellerMessage.setText(Html.fromHtml("卖家留言<font color=\"#999999\">(选填，买家可见）</font>"));
            mTvSubmit.setText("发货");
        }
        setLeftBlack();
        EventBus.getDefault().register(this);
        mService = ServiceManager.getInstance().createService(IOrderService.class);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.ivScan)
    public void onViewClicked() {
        new RxPermissions(this).request(Manifest.permission.CAMERA).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (aBoolean) {
                    startActivity(new Intent(ShipActivity.this, ScanActivity.class));
                } else {
                    PermissionsUtils.goPermissionsSetting(ShipActivity.this);
                    ToastUtil.error("无法获得相机权限");
                }
            }
        });
    }

    @OnClick(R.id.tvSubmit)
    public void onSubmit() {
        HashMap<String, Object> params = new HashMap<>();
        params.put("orderCode", mOrderCode);
        params.put("expressName", mEtName.getText().toString());
        params.put("expressCode", mEtCode.getText().toString());
        params.put("sellerRemark", mEtRemark.getText().toString());
        Observable<RequestResult<Object>> observable = mService.shipOrder(APIManager.buildJsonBody(params));
        if (mOrderMain != null) observable = mService.ship(params);
        APIManager.startRequest(
                observable,
                new BaseRequestListener<Object>(this) {
                    @Override
                    public void onSuccess(Object result) {
                        ToastUtil.success("发货成功");
                        finish();
                        EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_STORE_SHIT_SUCCEED));
                    }
                }, this
        );
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getMsg(MsgStatus status) {
        switch (status.getAction()) {
            case MsgStatus.ACTION_SCAN_SUCCEED:
                mEtCode.setText(status.getCode());
                break;
        }
    }

    public static void start(Context context, Order.OrderMain orderMain) {
        Intent intent = new Intent(context, ShipActivity.class);
        intent.putExtra("orderMain", orderMain);
        context.startActivity(intent);
    }

}
