package com.weiju.ccmall.module.xysh.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UpdateDescActivity extends BaseActivity {
    @BindView(R.id.tvStartDesc)
    TextView tvStartDesc;
    @BindView(R.id.tvRepayDesc)
    TextView tvRepayDesc;
    @BindView(R.id.tvConfirm)
    TextView tvConfirm;

    public static void start(Context context) {
        Intent intent = new Intent(context, UpdateDescActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_desc);
        ButterKnife.bind(this);
        setTitle("机制更新");
        setLeftBlack();
        tvStartDesc.setText(Html.fromHtml("<span style=\"color:#D5BA80\">启动资金&nbsp;&nbsp;&nbsp;&nbsp;</span>" +
                "为了您的用卡良好，建议启动资金适当低于卡内剩余可用额度，避免空卡。"));
        tvRepayDesc.setText(Html.fromHtml("<span style=\"color:#D5BA80\">手动还款&nbsp;&nbsp;&nbsp;&nbsp;</span>" +
                "为保证计划顺利执行，请将当日的消费金额自行还入信用卡。"));
    }

    @OnClick(R.id.tvConfirm)
    public void onViewClicked() {
        finish();
    }
}
