package com.weiju.ccmall.module.blockchain.computingpower;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.webkit.WebView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.blockchain.GradientColorTextView;
import com.weiju.ccmall.module.blockchain.beans.AllCCm;
import com.weiju.ccmall.module.blockchain.utils.BlockChainUtil;
import com.weiju.ccmall.module.blockchain.utils.TemplateWebUtil;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBlockChain;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ComputingPowerActivity extends BaseActivity {
    @BindView(R.id.tv_all_computing_power)
    GradientColorTextView tvAllComputingPower;
//    @BindView(R.id.tv_computing_rate)
//    GradientColorTextView tvComputingRate;
//    @BindView(R.id.tv_active_val)
//    GradientColorTextView tvActiveVal;
    @BindView(R.id.tv_block_height)
    GradientColorTextView tvBlockHeight;
    @BindView(R.id.tv_transaction_times)
    GradientColorTextView tvTransactionTimes;
    @BindView(R.id.tv_super_node)
    GradientColorTextView tvSuperNode;
    @BindView(R.id.tv_all_node)
    GradientColorTextView tvAllNode;
    @BindView(R.id.tv_tab_block)
    TextView tvTabBlock;
    @BindView(R.id.tv_tab_transaction)
    TextView tvTabTransaction;
    @BindView(R.id.viewpager)
    ViewPager viewpager;

    IBlockChain blockChain;
    @BindView(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_computing_power);
        blockChain = ServiceManager.getInstance().createService(IBlockChain.class);
        ButterKnife.bind(this);
        setUpWebView();
        setTitle("CCM");
        setLeftBlack();
        loadData();
        onViewClicked(tvTabBlock);
    }

    public void setUpWebView() {
        // 设置背景色
        webView.setBackgroundColor(0);
        // 设置填充透明度
//        webView.getBackground().setAlpha(0);
    }

    private void initView(AllCCm res) {
        viewpager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                switch (i) {
                    case 0:
                        return BlockFragment.newInstance(res.accountBlock);
                    case 1:
                        return TransactionFragment.newInstance(res.accountTransction);
                }
                throw new IllegalStateException("无效item "+i);
            }

            @Override
            public int getCount() {
                return 2;
            }
        });

        TemplateWebUtil.loadChart(webView, res.templateId, new Gson().toJson(res.values), "算力");
    }

    private void loadData() {
        ToastUtil.showLoading(this, true);
        APIManager.startRequest(blockChain.getAllCcm(), new BaseRequestListener<AllCCm>() {
            @Override
            public void onSuccess(AllCCm result) {
                if (isFinishing()) {
                    return;
                }
                tvAllComputingPower.setText(BlockChainUtil.formatCCMCoin(result.allCcm));
//                tvComputingRate.setText(result.allRate + "%");
//                tvActiveVal.setText(BlockChainUtil.formatCCMCoin(result.contribution));
                tvSuperNode.setText(String.valueOf(result.superNode));
                tvAllNode.setText(String.valueOf(result.node));
                tvTransactionTimes.setText(result.accountTransction.transactionCount);

                List<AllCCm.AccountBlockBean.BlockBean> block = result.accountBlock.block();
                if (block != null && !block.isEmpty()) {
                    tvBlockHeight.setText(String.valueOf(block.get(block.size()-1).blockHeader.rawData.number));
                } else {
                    tvBlockHeight.setText("0");
                }
                initView(result);
            }

            @Override
            public void onComplete() {
                ToastUtil.hideLoading();
            }
        }, this);
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, ComputingPowerActivity.class);
        context.startActivity(intent);
    }

    @OnClick({R.id.tv_tab_block, R.id.tv_tab_transaction})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_tab_block:
                tvTabBlock.setBackgroundResource(R.mipmap.bg_tab_computing_power_selected);
                tvTabBlock.setTextColor(getResources().getColor(R.color.white));
                tvTabTransaction.setBackgroundResource(R.mipmap.bg_tab_computing_power_unselect);
                tvTabTransaction.setTextColor(getResources().getColor(R.color.text_gray));
                viewpager.setCurrentItem(0, true);
                break;
            case R.id.tv_tab_transaction:
                tvTabTransaction.setBackgroundResource(R.mipmap.bg_tab_computing_power_selected);
                tvTabTransaction.setTextColor(getResources().getColor(R.color.white));
                tvTabBlock.setBackgroundResource(R.mipmap.bg_tab_computing_power_unselect);
                tvTabBlock.setTextColor(getResources().getColor(R.color.text_gray));
                viewpager.setCurrentItem(1, true);
                break;
        }
    }
}
