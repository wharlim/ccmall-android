package com.weiju.ccmall.newRetail.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.TimeUtils;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.groupBuy.JoinGroupActivity;
import com.weiju.ccmall.module.order.OrderListActivity;
import com.weiju.ccmall.module.order.RefundExpressActivity;
import com.weiju.ccmall.module.order.adapter.OrderItemAdapter;
import com.weiju.ccmall.module.store.ReceiveRefundGoodsActivity;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.OrderService;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.CSUtils;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyStoreOrderDetailActivity extends BaseActivity {

    private IOrderService mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
    ;

    private int mMode;
    private String mOrderCode;

    @BindView(R.id.tvScore)
    TextView mTvScore;
    @BindView(R.id.layoutScore)
    LinearLayout mLayoutScore;
    @BindView(R.id.tvRatio)
    TextView mTvRatio;
    @BindView(R.id.tvRatioPrice)
    TextView mTvRatioPrice;
    @BindView(R.id.layoutRatio)
    LinearLayout mLayoutRatio;
    @BindView(R.id.itemShit)
    TextView mItemShit;
    @BindView(R.id.tvAgressRefundGoods)
    TextView mTvAgressRefundGoods;
    @BindView(R.id.tvAgressRefundMoney)
    TextView mTvAgressRefundMoney;
    @BindView(R.id.tvReceiveRefundGodds)
    TextView mTvReceiveRefundGodds;
    @BindView(R.id.layoutConpon)
    LinearLayout mLayoutConpon;
    @BindView(R.id.itemCancelRefundMoney)
    TextView mItemCancelRefundMoney;
    @BindView(R.id.itemCancelRefundGoods)
    TextView mItemCancelRefundGoods;
    @BindView(R.id.itemCheckGroupBuy)
    TextView mItemCheckGroupBuy;
    @BindView(R.id.itemGoGroupBuy)
    TextView mItemGoGroupBuy;
    @BindView(R.id.layoutMoney)
    LinearLayout mLayoutMoney;
    @BindView(R.id.tvSellerBuyerPayMoney)
    TextView mTvSellerBuyerPayMoney;
    @BindView(R.id.rvSellerProduct1)
    RecyclerView mRvSellerProduct1;
    @BindView(R.id.tvSellerFeight1)
    TextView mTvSellerFeight1;
    @BindView(R.id.tvSellerRecevieMoney)
    TextView mTvSellerRecevieMoney;
    @BindView(R.id.rvSellerProduct2)
    RecyclerView mRvSellerProduct2;
    @BindView(R.id.tvSellerFeight2)
    TextView mTvSellerFeight2;
    @BindView(R.id.layoutSeller)
    LinearLayout mLayoutSeller;
    @BindView(R.id.tvMoneyTag)
    TextView mTvMoneyTag;
    @BindView(R.id.tvOrderExpressCode)
    TextView mTvOrderExpressCode;
    @BindView(R.id.layoutOrderExpressCode)
    LinearLayout mLayoutOrderExpressCode;
    @BindView(R.id.tvCS)
    TextView mTvCS;
    @BindView(R.id.tvCticket)
    TextView mTvCticket;
    @BindView(R.id.tvCC)
    TextView mTvCC;
    private Order mOrder;
    @BindView(R.id.statusTv)
    protected TextView mStatusTv;
    @BindView(R.id.refundGoodTipsTv)
    protected TextView mRefundGoodTipsTv;
    @BindView(R.id.orderCodeTv)
    protected TextView mOrderCodeTv;

    @BindView(R.id.addRefundGoodExpressBtn)
    protected TextView mAddRefundGoodInfoBtn;

    @BindView(R.id.refundLayout)
    protected LinearLayout mRefundLayout;
    @BindView(R.id.refundReasonLabelTv)
    protected TextView mRefundReasonLabelTv;
    @BindView(R.id.refundReasonValueTv)
    protected TextView refundReasonValueTv;

    @BindView(R.id.refundApplyMoneyLabelTv)
    protected TextView refundApplyMoneyLabelTv;
    @BindView(R.id.refundApplyMoneyValueTv)
    protected TextView refundApplyMoneyValueTv;
    @BindView(R.id.refundMoneyLayout)
    protected LinearLayout mRefundMoneyLayout;
    @BindView(R.id.refundMoneyLabelTv)
    protected TextView refundMoneyLabelTv;
    @BindView(R.id.refundMoneyValueTv)
    protected TextView refundMoneyValueTv;

    @BindView(R.id.refundRemarkLayout)
    protected LinearLayout refundRemarkLayout;
    @BindView(R.id.refundRemarkLabelTv)
    protected TextView refundRemarkLabelTv;
    @BindView(R.id.refundRemarkValueTv)
    protected TextView refundRemarkValueTv;
    @BindView(R.id.imageRecyclerView)
    protected RecyclerView mImageRecyclerView;
    @BindView(R.id.refundExpressCompanyLayout)
    protected LinearLayout refundExpressCompanyLayout;
    @BindView(R.id.refundExpressCompanyValueTv)
    protected TextView refundExpressCompanyValueTv;
    @BindView(R.id.refundExpressCodeLayout)
    protected LinearLayout refundExpressCodeLayout;
    @BindView(R.id.refundExpressCodeValueTv)
    protected TextView refundExpressCodeValueTv;

    @BindView(R.id.phoneTv)
    protected TextView mPhoneTv;
    @BindView(R.id.contactsTv)
    protected TextView mContactsTv;
    @BindView(R.id.addressDetailTv)
    protected TextView mAddressDetailTv;
    @BindView(R.id.storeNameTv)
    protected TextView mStoreNameTv;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.buyerRemarkLayout)
    protected LinearLayout mBuyerRemarkLayout;
    @BindView(R.id.buyerRemarkTv)
    protected TextView mBuyerRemarkTv;
    @BindView(R.id.sellerRemarkLayout)
    protected LinearLayout mSellerRemarkLayout;
    @BindView(R.id.sellerRemarkTv)
    protected TextView mSellerRemarkTv;

    @BindView(R.id.createDateTv)
    protected TextView mCreateDateTv;
    @BindView(R.id.payWayTv)
    protected TextView mPayWayTv;
    @BindView(R.id.productTotalTv)
    protected TextView mProductTotalTv;
    @BindView(R.id.freightTv)
    protected TextView mFreightTv;
    @BindView(R.id.couponTv)
    protected TextView mCouponTv;
    @BindView(R.id.payMoneyTv)
    protected TextView mPayMoneyTv;

    @BindView(R.id.orderBottomLayout)
    protected LinearLayout mOrderBottomLayout;

    @BindView(R.id.itemCancelBtn)
    protected TextView mCancelBtn;
    @BindView(R.id.itemPayBtn)
    protected TextView mPayBtn;
    @BindView(R.id.itemApplyRefundMoneyBtn)
    protected TextView mApplyRefundMoneyBtn;
    @BindView(R.id.itemApplyRefundGoodsBtn)
    protected TextView mApplyRefundGoodsBtn;
    @BindView(R.id.itemViewExpressBtn)
    protected TextView mViewExpressBtn;
    @BindView(R.id.orderFinishBtn)
    protected TextView mOrderFinishBtn;
    @BindView(R.id.goldenMemberInfoLayout)
    LinearLayout mGoldenMemberInfoLayout;
    @BindView(R.id.tvGoldenMemberInfo)
    TextView mTvGoldenMemberInfo;
    @BindView(R.id.tvGoldenNum)
    TextView mTvGoldenNum;
    @BindView(R.id.tvGoldenTicket)
    TextView mTvGoldenTicket;
    @BindView(R.id.itemGoSuperGroupBuy)
    TextView mItemGoSuperGroupBuy;
    @BindView(R.id.viewWeight)
    View mViewWeight;

    @BindView(R.id.ll_my_sell)
    LinearLayout ll_my_sell;

    @BindView(R.id.tv_client_pay)
    TextView tv_client_pay;

    @BindView(R.id.tv_client_pay_number)
    TextView tv_client_pay_number;

    @BindView(R.id.tv_me_pay)
    TextView tv_me_pay;

    @BindView(R.id.tv_me_pay_number)
    TextView tv_me_pay_number;

    @BindView(R.id.tvCC1)
    TextView tvCC1;

    @BindView(R.id.tv_timer)
    TextView tv_timer;

    @BindView(R.id.rl_up_user)
    RelativeLayout rl_up_user;

    @BindView(R.id.tv_up_name)
    TextView tv_up_name;

    @BindView(R.id.tv_confirmUpgrade)
    TextView tv_confirmUpgrade;

    @BindView(R.id.tv_thaw)
    TextView tv_thaw;

    @BindView(R.id.iv_im_icon)
    ImageView iv_im_icon;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_store_detail_layout);

        ButterKnife.bind(this);

        showHeader();
        setTitle("订单详情");
        getHeaderLayout().setLeftDrawable(R.mipmap.icon_back_black);
        getHeaderLayout().setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
        Intent intent = getIntent();
        if (!(intent == null || intent.getExtras() == null)) {
            mOrderCode = intent.getExtras().getString("orderCode");
        }
        if (mOrderCode == null || mOrderCode.isEmpty()) {
            ToastUtil.error("参数错误");
            finish();
            return;
        }
        mMode = intent.getIntExtra("mode", 0);

        mLayoutMoney.setVisibility(View.GONE);
        ll_my_sell.setVisibility(View.VISIBLE);

        iv_im_icon.setVisibility(View.VISIBLE);
        loadOrderDetail(mOrderCode);
    }

    private void loadOrderDetail(String orderCode) {
        APIManager.startRequest(mOrderService.getOrderByCode(orderCode, "0"), new BaseRequestListener<Order>(this) {
            @Override
            public void onSuccess(Order result) {
                mOrder = result;
                String pay199 = getSharedPreferences(Const.PAY_199, 0).getString(Const.PAY_199, "");
                if (Const.PAY_199.equals(pay199)) {
                    mMode = 2;
                    getSharedPreferences(Const.PAY_199, 0).edit().putString(Const.PAY_199, "").commit();
                }
                initOrderProducts();
                initOrderBase();

                switch (result.orderMain.status) {
                    case AppTypes.ORDER.STATUS_BUYER_WAIT_PAY://待付款
//                        mTvMoneyTag.setText("待客户支付：");
//                        mPayMoneyTv.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this,result.orderMain.totalMoney));
                        tv_client_pay.setText("待客户支付：");
                        tv_client_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.orderMain.totalMoney));
                        tv_me_pay.setText("可结算：");
                        tv_me_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.memberProfitBean.profitMoney));

                        break;
                    case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP://待发货
                        tv_client_pay.setText("客户已支付：");
                        tv_client_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.orderMain.totalMoney));
                        tv_me_pay.setText("已结算至余额：");
                        tv_me_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.memberProfitBean.profitMoney));
                        if (result.memberProfitBean.status == 0) {
                            tv_thaw.setText("(未解冻)");
                        } else if (result.memberProfitBean.status == 1) {
                            tv_thaw.setText("(已解冻)");
                        }
                        break;
                    case AppTypes.ORDER.STATUS_BUYER_RETURN_MONEYING://退款中

                        tv_client_pay.setText("客户已支付：");
                        tv_client_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.orderMain.totalMoney));
                        tv_me_pay.setText("已结算至余额：");
                        tv_me_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.memberProfitBean.profitMoney));
                        if (result.memberProfitBean.status == 0) {
                            tv_thaw.setText("(未解冻)");
                        } else if (result.memberProfitBean.status == 1) {
                            tv_thaw.setText("(已解冻)");
                        }
                        break;
                    case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP://已发货

                        tv_client_pay.setText("客户已支付：");
                        tv_client_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.orderMain.totalMoney));
                        tv_me_pay.setText("已结算至余额：");
                        tv_me_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.memberProfitBean.profitMoney));
                        if (result.memberProfitBean.status == 0) {
                            tv_thaw.setText("(未解冻)");
                        } else if (result.memberProfitBean.status == 1) {
                            tv_thaw.setText("(已解冻)");
                        }

                        break;
                    case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED://交易成功

                        tv_client_pay.setText("客户已支付：");
                        tv_client_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.orderMain.totalMoney));
                        tv_me_pay.setText("可结算：");
                        tv_me_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.memberProfitBean.profitMoney));
                        /*if (result.memberProfitBean.status == 0) {
                            tv_thaw.setText("(未解冻)");
                        } else if (result.memberProfitBean.status == 1) {
                            tv_thaw.setText("(已解冻)");
                        }*/

                        break;
                    case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE://已关闭

                        tv_client_pay.setText("客户已支付：");
                        tv_client_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.orderMain.totalMoney));
                        tv_me_pay.setText("已结算至余额：");
                        tv_me_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.memberProfitBean.profitMoney));
                        if (result.memberProfitBean.status == 0) {
                            tv_thaw.setText("(未解冻)");
                        } else if (result.memberProfitBean.status == 1) {
                            tv_thaw.setText("(已解冻)");
                        }

                        break;

                    case AppTypes.ORDER.STATUS_BUYER_RETURN_MONECLOSE://退款成功/退货成功
                    case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODSCLOSE:

                        tv_client_pay.setText("客户已支付：");
                        tv_client_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.orderMain.totalMoney));
                        tv_me_pay.setText("已结算至余额：");
                        tv_me_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.memberProfitBean.profitMoney));
                        if (result.memberProfitBean.status == 0) {
                            tv_thaw.setText("(未解冻)");
                        } else if (result.memberProfitBean.status == 1) {
                            tv_thaw.setText("(已解冻)");
                        }

                        break;

                    case AppTypes.ORDER.STATUS_BUYER_RETURN_GOODING://退货中

                        tv_client_pay.setText("客户已支付：");
                        tv_client_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.orderMain.totalMoney));
                        tv_me_pay.setText("已结算至余额：");
                        tv_me_pay_number.setText(ConvertUtil.centToCurrency(MyStoreOrderDetailActivity.this, result.memberProfitBean.profitMoney));
                        if (result.memberProfitBean.status == 0) {
                            tv_thaw.setText("(未解冻)");
                        } else if (result.memberProfitBean.status == 1) {
                            tv_thaw.setText("(已解冻)");
                        }

                        break;
                    default:
                        break;
                }
            }
        }, this);
    }


    private void initOrderBase() {
        mStatusTv.setText(mOrder.orderMain.orderStatusStr);
        if (mOrder.isShowGroupOrderStatus()) {
            Order.GroupInfoEntity groupInfo = mOrder.groupInfo;
            if (!TextUtils.isEmpty(groupInfo.expiresDate)) {
                mRefundGoodTipsTv.setVisibility(View.VISIBLE);
                String date = TimeUtils.date2String(TimeUtils.string2Date(groupInfo.expiresDate), "MM月dd日 HH:mm:ss");
                int min = groupInfo.joinMemberNum - groupInfo.payOrderNum;
                mRefundGoodTipsTv.setText(String.format("还差%d人成团，%s截止", min > 0 ? min : 0, date));
            }
        }
        mOrderCodeTv.setText(mOrder.orderMain.orderCode);

        if (mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_SHIP || mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED) {
            mLayoutOrderExpressCode.setVisibility(View.VISIBLE);
            mTvOrderExpressCode.setText(mOrder.orderMain.expressCode.replace(",", "\n"));
        } else {
            mLayoutOrderExpressCode.setVisibility(View.GONE);
        }

        mPhoneTv.setText(mOrder.orderMain.phone);
        mContactsTv.setText("收货人：" + mOrder.orderMain.contact);
        mAddressDetailTv.setText("收货地址：" + mOrder.orderMain.getFullAddress());
        if (mMode == OrderListActivity.MODE_SELLER || mMode == 2) {
//            mStoreNameTv.setText("买家：" + mOrder.nickName);
        } else {
            String storeName = getResources().getString(R.string.appName);
            if (mOrder.orderMain.status != 1 && !mOrder.products.isEmpty()) {
                storeName = mOrder.products.get(0).storeName;
            }
//            mStoreNameTv.setText(storeName);
        }

        mStoreNameTv.setText(mOrder.orderMain.nickName);

        if (!(mOrder.orderMain.buyerRemark == null || mOrder.orderMain.buyerRemark.isEmpty())) {
            mBuyerRemarkLayout.setVisibility(View.VISIBLE);
            mBuyerRemarkTv.setText(mOrder.orderMain.buyerRemark);
        }
        if (!(mOrder.orderMain.sellerRemark == null || mOrder.orderMain.sellerRemark.isEmpty())) {
            mSellerRemarkLayout.setVisibility(View.VISIBLE);
            mSellerRemarkTv.setText(mOrder.orderMain.sellerRemark);
        }
        mCreateDateTv.setText(mOrder.orderMain.createDate);
        mPayWayTv.setText(mOrder.orderMain.payTypeStr);
        mProductTotalTv.setText(ConvertUtil.centToCurrency(this, mOrder.orderMain.totalProductMoney));
        mFreightTv.setText(mOrder.orderMain.freight == 0 ? "¥0" : ConvertUtil.centToCurrency(this, mOrder.orderMain.freight));

        String ccmTipStr = "预计可返";
        mGoldenMemberInfoLayout.setVisibility(View.VISIBLE);
        if (mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_WAIT_PAY || (mOrder.orderMain.status == AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE && mOrder.orderMain.payMoney < mOrder.orderMain.totalMoney)) {
            mTvMoneyTag.setText("待付款：");
            ccmTipStr = "预计可返";
            mGoldenMemberInfoLayout.setVisibility(View.GONE);
        }
        if (mOrder.orderMain.orderType == 5 || mOrder.orderMain.orderType == 6 || mOrder.orderMain.orderType == 7) {
            mGoldenMemberInfoLayout.setVisibility(View.GONE);
        }
        mTvGoldenMemberInfo.setText(mOrder.orderMain.goldenMemberInfo);
        mTvGoldenTicket.setText(mOrder.orderMain.useGoldenTicket == 0 ? "-¥0" : String.format("-%s", MoneyUtil.centToYuan¥StrNoZero(mOrder.orderMain.useGoldenTicket)));
        mTvGoldenNum.setText(mOrder.orderMain.goldenTicket == 0 ? "-¥0" : String.format("-%s", MoneyUtil.centToYuan¥StrNoZero(mOrder.orderMain.goldenTicket)));

        mPayMoneyTv.setText(ConvertUtil.centToCurrency(this, mOrder.orderMain.totalMoney));

        //优惠券
        if (mOrder.orderMain.discountCoupon > 0) {
            mLayoutConpon.setVisibility(View.VISIBLE);
            mCouponTv.setText(ConvertUtil.centToCurrency(this, -mOrder.orderMain.discountCoupon));
        }

        //积分
        long score = mOrder.orderMain.score;
        if (score > 0 && Config.IS_DISCOUNT) {
            mLayoutScore.setVisibility(View.VISIBLE);
            mTvScore.setText(String.format("%d积分，抵¥%.2f", score, score * 1.0f / 10));
        }

        mLayoutRatio.setVisibility(View.VISIBLE);
        mTvRatio.setText("折扣");
        if (mOrder.orderMain.discountMoney == 0) {
            mTvRatioPrice.setText("-¥0");
        } else {
            mTvRatioPrice.setText(MoneyUtil.cleanZero(String.format("-¥%.2f", mOrder.orderMain.discountMoney * 1.0f / 100)));
        }
//        }

        if (mOrder.orderMain.ticket == 0) {
            mTvCticket.setText("-¥0");
        } else {
            mTvCticket.setText(String.format("-%s", MoneyUtil.centToYuan¥StrNoZero(mOrder.orderMain.ticket)));
        }
        tvCC1.setText("*扣除款为技术服务费");
    }

    private void initOrderProducts() {
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.addItemDecoration(new ListDividerDecoration(this));
        OrderItemAdapter adapter = new OrderItemAdapter(this, mOrder.products);
        adapter.setDetailModel(true);
        adapter.setSellerModel(mMode == OrderListActivity.MODE_SELLER);
        mRecyclerView.setAdapter(adapter);
        adapter.setMemberId(mOrder.orderMain.memberId);
    }

    @OnClick(R.id.itemApplyRefundMoneyBtn)
    public void applyRefundMoney() {
        OrderService.viewApplyRefundMoneyActivity(this, mOrder);
    }

    @OnClick(R.id.itemApplyRefundGoodsBtn)
    public void applyRefundGoods() {
        OrderService.viewApplyRefundGoodsActivity(this, mOrder);
    }

    @OnClick({R.id.itemViewExpressBtn, R.id.tvOrderExpressCode})
    public void viewExpress() {
        OrderService.viewExpress(this, mOrder);
    }

    @OnClick(R.id.orderFinishBtn)
    public void finishOrder() {
        OrderService.finishOrder(this, mOrder);
    }

    @OnClick(R.id.tvAgressRefundGoods)
    public void onMTvAgressRefundGoodsClicked() {
//        OrderService.showRefundGoodsDialog(this, mOrder);
    }

    @OnClick(R.id.tvAgressRefundMoney)
    public void onMTvAgressRefundMoneyClicked() {
//        OrderService.showRefundMoneyDialog(this, mOrder);
    }

    @OnClick(R.id.tvReceiveRefundGodds)
    public void onMTvReceiveRefundGoddsClicked() {
        // 确认退货
        Intent intent = new Intent(this, ReceiveRefundGoodsActivity.class);
        intent.putExtra("orderCode", mOrder.orderMain.orderCode);
        intent.putExtra("maxPrice", mOrder.canRefundMoney(1));
        startActivity(intent);
    }

    @OnClick(R.id.addRefundGoodExpressBtn)
    public void addRefundGoodExpress() {
        Intent intent = new Intent(this, RefundExpressActivity.class);
        intent.putExtra("refundId", mOrder.refundOrder.refundId);
        startActivity(intent);
    }

    @OnClick(R.id.itemShit)
    public void onViewClicked() {
        OrderService.ship(this, mOrder);
    }

    @OnClick({R.id.itemCancelRefundMoney, R.id.itemCancelRefundGoods})
    public void onViewClicked(View view) {
        OrderService.showCancelRefund(this, mOrder);
    }

    @OnClick({R.id.itemGoGroupBuy, R.id.itemCheckGroupBuy})
    public void itemGoGroupBuy() {
        OrderService.goGroupBuy(this, mOrder);
    }

    @OnClick(R.id.itemGoSuperGroupBuy)
    public void itemGoSuperGroupBuy() {
        Intent intent = new Intent(MyStoreOrderDetailActivity.this, JoinGroupActivity.class);
        intent.putExtra(Config.INTENT_KEY_TYPE_NAME, JoinGroupActivity.TYPE_HOST);
        intent.putExtra(Config.INTENT_KEY_ID, mOrder.groupInfo.groupCode);
        intent.putExtra("isSuperGroup", true);
        startActivity(intent);
    }


    @OnClick(R.id.tvCS)
    public void onGoCS() {
        if (mMode == 4) {

            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (loginUser != null) {
                TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                    @Override
                    public void onError(int i, String s) {
                    }

                    @Override
                    public void onSuccess() {
                        IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                        APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                        }, MyStoreOrderDetailActivity.this);
                        ChatInfo chatInfo = new ChatInfo();
                        chatInfo.setType(TIMConversationType.C2C);
                        chatInfo.setId(mOrder.orderMain.memberId);
                        chatInfo.setChatName(mOrder.orderMain.nickName);
                        Intent intent = new Intent(MyStoreOrderDetailActivity.this, ChatActivity.class);
                        intent.putExtra("chatInfo", chatInfo);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                });
            }
        } else {
            CSUtils.start(this);
        }
    }

    @OnClick(R.id.tvCopy)
    public void copyOrderCode() {
        ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData mClipData = ClipData.newPlainText("Label", mOrder.orderMain.orderCode);
        cm.setPrimaryClip(mClipData);
        ToastUtil.success("复制成功");
    }

    @OnClick(R.id.storeNameTv)
    public void storeNameTv() {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, MyStoreOrderDetailActivity.this);
                    ChatInfo chatInfo = new ChatInfo();
                    chatInfo.setType(TIMConversationType.C2C);
                    chatInfo.setId(mOrder.orderMain.memberId);
                    chatInfo.setChatName(mOrder.orderMain.nickName);
                    Intent intent = new Intent(MyStoreOrderDetailActivity.this, ChatActivity.class);
                    intent.putExtra("chatInfo", chatInfo);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });
        }
    }
}
