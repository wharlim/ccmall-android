package com.weiju.ccmall.newRetail.bean;

public class GiftClientBean {

    public String nickName;
    public String userLevel;

    public GiftClientBean(String nickName, String userLevel) {
        this.nickName = nickName;
        this.userLevel = userLevel;
    }
}
