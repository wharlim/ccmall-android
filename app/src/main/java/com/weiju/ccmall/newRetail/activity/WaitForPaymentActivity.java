package com.weiju.ccmall.newRetail.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.TitleView;
import com.weiju.ccmall.newRetail.utils.TimerControl;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WaitForPaymentActivity extends BaseActivity {

    @BindView(R.id.titleView)
    TitleView titleView;

    @BindView(R.id.tv_money)
    TextView tv_money;

    @BindView(R.id.tv_status)
    TextView tv_status;

    @BindView(R.id.tv_timer)
    TextView tv_timer;

    @BindView(R.id.tv_payee)
    TextView tv_payee;

    @BindView(R.id.tv_phonenumber)
    TextView tv_phonenumber;

    @BindView(R.id.avatarIv)
    com.facebook.drawee.view.SimpleDraweeView mAvatarIv;

    @BindView(R.id.rl_pay)
    RelativeLayout rl_pay;

    @BindView(R.id.tv_alipay_account_copy)
    TextView tv_alipay_account_copy;


    private CountDownTimer mTimer;

    Order order;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_wait_for_payment);
        ButterKnife.bind(this);
        titleView.setTitle("转账支付");
        EventBus.getDefault().register(this);
        initData();
    }


    private void initData() {
        String orderCode = getIntent().getStringExtra("orderCode");
        IOrderService orderService = ServiceManager.getInstance().createService(IOrderService.class);
        APIManager.startRequest(orderService.getOrderByCode(orderCode, ""), new BaseRequestListener<Order>(this) {
            @Override
            public void onSuccess(Order result) {
                order = result;
                tv_money.setText(ConvertUtil.centToCurrency(WaitForPaymentActivity.this, result.orderMain.totalMoney));
                try {
                    SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
                    //剩余时间
                    long remaining = result.orderMain.lessTime;
                    if (order.orderMain.orderType == 5 || order.orderMain.orderType == 7) {
                        remaining = remaining - 900000;
                    }
                    if (remaining > 0) {
                        if (mTimer == null) {
                            mTimer = new CountDownTimer(remaining, 1000) {
                                @Override
                                public void onTick(long l) {
                                    String ms = formatter.format(l);
                                    String str = "请在" + ms + "内联系您的邀请人，向他支付并请他登录商城处理你的订单";
                                    SpannableString spannableString = new SpannableString(str);
                                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
                                    spannableString.setSpan(foregroundColorSpan, str.indexOf("在") + 1, str.indexOf("内联系"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                    tv_timer.setText(spannableString);
                                }

                                @Override
                                public void onFinish() {
                                    tv_timer.setText("邀请人已超时未处理你的订单，请您直接向店主支付");
                                    rl_pay.setVisibility(View.VISIBLE);
                                }
                            }.start();
                            TimerControl.getInstance().add(mTimer);
                        }
                    } else {
                        tv_timer.setText("邀请人已超时未处理你的订单，请您直接向店主支付");
//                        tv_timer.setText("联系不上推荐人，我选择直接向店主支付");
                        rl_pay.setVisibility(View.VISIBLE);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (!TextUtils.isEmpty(result.upMember.headImage))
                    FrescoUtil.setImage(mAvatarIv, String.valueOf(result.upMember.headImage));
                tv_payee.setText(result.upMember.nickName);
                tv_phonenumber.setText(result.upMember.phone);

            }
        }, this);
    }

    @OnClick({R.id.tv_alipay_account_copy, R.id.rl_pay})
    public void allClick(View view) {
        switch (view.getId()) {
            case R.id.tv_alipay_account_copy:
                ClipboardManager cm2 = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData mClipData2 = ClipData.newPlainText("Label", order.storeUser.phone);
                cm2.setPrimaryClip(mClipData2);
                ToastUtil.success("复制成功");
                break;
            case R.id.rl_pay:
                Intent intent = new Intent(this, PayUpgradeActivity.class);
//                ActivityControl.getInstance().add(PayActivity.this);
                intent.putExtra("orderCode", order.orderMain.orderCode);
                startActivity(intent);
//                ActivityControl.getInstance().add(PayActivity.this);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TimerControl.getInstance().closeAll();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(EventMessage message) {
        switch (message.getEvent()) {
            case newRetailPaySuccess:
                finish();
                break;
            default:
        }
    }
}
