package com.weiju.ccmall.newRetail.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.bean.ShoppingGuidelinesBean;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.INotesService;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HiGouDescriptionActivity extends BaseActivity {

    @BindView(R.id.agreeIv)
    ImageView agreeIv;

    @BindView(R.id.tv_go_higou)
    TextView tv_go_higou;

    @BindView(R.id.tv_descript)
    TextView tv_descript;
    @BindView(R.id.agreement)
    RelativeLayout agreement;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_higou_description_layout);
        ButterKnife.bind(this);
        agreeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (agreeIv.isSelected()) {
                    agreeIv.setSelected(false);
                } else {
                    agreeIv.setSelected(true);
                }
            }
        });

        boolean showAgreement = getIntent().getBooleanExtra("showAgreement", true);
        agreement.setVisibility(showAgreement ? View.VISIBLE : View.GONE);

        INotesService notesService = ServiceManager.getInstance().createService(INotesService.class);
        APIManager.startRequest(notesService.getShoppingGuidelines(), new BaseRequestListener<ShoppingGuidelinesBean>() {
            @Override
            public void onSuccess(ShoppingGuidelinesBean result) {
                tv_descript.setText(Html.fromHtml(result.content));
            }
        }, this);
    }


    @OnClick({R.id.tv_go_higou})
    public void allViewClick() {
        if (agreeIv.isSelected()) {
            getSharedPreferences(Const.READ_HIGOU_NEED_TO_KNOW, 0).edit().putBoolean(Const.READ_HIGOU_NEED_TO_KNOW, true).commit();
            EventBus.getDefault().post(new EventMessage(Event.isReadHigou));
            finish();
        } else {
            ToastUtil.error("请勾选<我已阅读并了解上述所有内容>");
        }
    }

    public static void start(Context context, boolean showAgreement) {
        Intent intent = new Intent(context, HiGouDescriptionActivity.class);
        intent.putExtra("showAgreement", showAgreement);
        context.startActivity(intent);
    }
}
