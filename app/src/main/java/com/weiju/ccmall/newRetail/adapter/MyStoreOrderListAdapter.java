package com.weiju.ccmall.newRetail.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderDetailActivity;
import com.weiju.ccmall.module.order.OrderListActivity;
import com.weiju.ccmall.module.order.adapter.OrderItemAdapter;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.newRetail.activity.MyStoreOrderDetailActivity;
import com.weiju.ccmall.shared.basic.BaseAdapter;
import com.weiju.ccmall.shared.basic.BaseCallback;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.OrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyStoreOrderListAdapter extends BaseAdapter<Order, MyStoreOrderListAdapter.ViewHolder> {

    Context mContext;

    private int mModel;

    public MyStoreOrderListAdapter(Context context) {

        super(context);
        mContext = context;
    }

    public MyStoreOrderListAdapter(Context context, int model) {

        super(context);
        mContext = context;
        mModel = model;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(layoutInflater.inflate(R.layout.adapter_my_store_order_list_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final Order order = items.get(i);
        viewHolder.setOrder(order);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemStoreNameTv)
        protected TextView mStoreNameTv;
        @BindView(R.id.itemStatusTv)
        protected TextView mStatusTv;
        @BindView(R.id.itemRecyclerView)
        protected RecyclerView mRecyclerView;
        @BindView(R.id.itemDescTv)
        protected TextView mDescTv;
        @BindView(R.id.itemPriceTv)
        protected TextView mPriceTv;
        @BindView(R.id.bottomLayout)
        protected LinearLayout mBottomLayout;
        @BindView(R.id.itemCancelBtn)
        protected TextView mCancelBtn;
        @BindView(R.id.itemPayBtn)
        protected TextView mPayBtn;
        @BindView(R.id.itemApplyRefundMoneyBtn)
        protected TextView mApplyRefundMoneyBtn;
        @BindView(R.id.orderFinishBtn)
        protected TextView orderFinishBtn;
        @BindView(R.id.tv_confirmUpgrade)
        protected TextView tv_confirmUpgrade;
        @BindView(R.id.itemDetailBtn)
        protected TextView mDetailBtn;
        @BindView(R.id.itemViewExpressBtn)
        protected TextView mViewExpressBtn;
        @BindView(R.id.itemCsBtn)
        protected TextView mItemCsBtn;
        @BindView(R.id.itemShit)
        protected TextView mItemShit;
        @BindView(R.id.itemRefundMony)
        protected TextView itemRefundMony;
        @BindView(R.id.itemRefundGoods)
        protected TextView itemRefundGoods;
        @BindView(R.id.itemPayMoney)
        protected TextView itemPayMoney;
        @BindView(R.id.itemCancelRefundGoods)
        protected TextView itemCancelRefundGoods;
        @BindView(R.id.itemCancelRefundMoney)
        protected TextView itemCancelRefundMoney;
        @BindView(R.id.itemGoGroupBuy)
        protected TextView mItemGoGroupBuy;
        @BindView(R.id.itemCheckGroupBuy)
        protected TextView mItemCheckGroupBuy;
        @BindView(R.id.itemComment)
        protected TextView mItemComment;
        @BindView(R.id.itemEditRefund)
        protected TextView mItemEditRefund;
        @BindView(R.id.itemCancelRefund)
        protected TextView mItemCancelRefund;
        @BindView(R.id.tvPriceTag1)
        protected TextView mTvPriceTag1;
        @BindView(R.id.tvPriceTag2)
        protected TextView mTvPriceTag2;
        @BindView(R.id.tvCS)
        protected TextView mTvCS;
        @BindView(R.id.tv_time_out)
        protected TextView tv_time_out;
        @BindView(R.id.iv_im_icon)
        protected ImageView iv_im_icon;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        private void contactCustomer(Context context, String memberId, String nickName) {
            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (loginUser != null) {
                TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                    @Override
                    public void onError(int i, String s) {
                    }

                    @Override
                    public void onSuccess() {
                        IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                        APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                        }, context);
                        ChatInfo chatInfo = new ChatInfo();
                        chatInfo.setType(TIMConversationType.C2C);
                        chatInfo.setId(memberId);
                        chatInfo.setChatName(nickName);
                        Intent intent = new Intent(context, ChatActivity.class);
                        intent.putExtra("chatInfo", chatInfo);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                });
            }
        }


        public void setOrder(Order order) {
            LinearLayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            layoutManager.setAutoMeasureEnabled(true);
            mRecyclerView.setEnabled(false);
            mRecyclerView.addItemDecoration(new ListDividerDecoration(context));
            mRecyclerView.setLayoutManager(layoutManager);
            OrderItemAdapter orderItemAdapter = new OrderItemAdapter(context, order.products);
            orderItemAdapter.setSellerModel(false);
//            orderItemAdapter.setSelectModel(true);
            orderItemAdapter.setDetailModel(true);
            orderItemAdapter.setCallback(new BaseCallback<Object>() {
                @Override
                public void callback(Object data) {
                    if (mModel == OrderListActivity.MODE_LIVE_STORE) {
                        //订单礼包
                        Intent intent = new Intent(context, OrderDetailActivity.class);
                        intent.putExtra("orderCode", order.orderMain.orderCode);
                        intent.putExtra("mode", mModel);
                        mContext.startActivity(intent);
                    } else{
                        Intent intent = new Intent(context, MyStoreOrderDetailActivity.class);
                        intent.putExtra("orderCode", order.orderMain.orderCode);
//                    intent.putExtra("mode", mModel);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mContext.startActivity(intent);
                    }
                }
            });

            orderItemAdapter.setMemberId(order.orderMain.memberId);
            mRecyclerView.setAdapter(orderItemAdapter);
            orderItemAdapter.setDetailModel(true);
            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.setClickable(false);
            mDescTv.setText(String.format("共 %d 件商品 合计：", order.products.size()));

            iv_im_icon.setVisibility(View.VISIBLE);
            iv_im_icon.setOnClickListener(v -> {
                contactCustomer(mRecyclerView.getContext(), order.orderMain.memberId, order.orderMain.nickName);
            });

            mStoreNameTv.setText(order.orderMain.nickName);

            mStoreNameTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    User loginUser = SessionUtil.getInstance().getLoginUser();
                    if (loginUser != null) {
                        TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                            @Override
                            public void onError(int i, String s) {
                            }

                            @Override
                            public void onSuccess() {
                                IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                                APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                                }, mContext);
                                ChatInfo chatInfo = new ChatInfo();
                                chatInfo.setType(TIMConversationType.C2C);
                                chatInfo.setId(order.orderMain.memberId);
                                chatInfo.setChatName(order.orderMain.nickName);
                                Intent intent = new Intent(context, ChatActivity.class);
                                intent.putExtra("chatInfo", chatInfo);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                            }
                        });
                    }
                }
            });


            for (int i = 0; i < mBottomLayout.getChildCount(); i++) {
                mBottomLayout.getChildAt(i).setVisibility(View.GONE);
            }

            mStatusTv.setText(order.orderMain.orderStatusStr);

            switch (order.orderMain.status) {
                case AppTypes.ORDER.STATUS_BUYER_WAIT_PAY://待付款
                    mTvPriceTag1.setText("待客户支付：");
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                    mTvPriceTag2.setText("可结算：");
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.memberProfitBean.profitMoney));
                    break;
                case AppTypes.ORDER.STATUS_BUYER_WAIT_SHIP://待发货
                    mTvPriceTag1.setText("客户已支付：");
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                    mTvPriceTag2.setText("已结算：");
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.memberProfitBean.profitMoney));
                    break;

                case AppTypes.ORDER.STATUS_BUYER_RETURN_MONEYING://退款中
                    mTvPriceTag1.setText("客户已支付：");
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                    mTvPriceTag2.setText("已结算：");
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.memberProfitBean.profitMoney));
                    break;

                case AppTypes.ORDER.STATUS_BUYER_HAS_SHIP://待收货
                    mTvPriceTag1.setText("客户已支付：");
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                    mTvPriceTag2.setText("已结算：");
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.memberProfitBean.profitMoney));
                    break;

                case AppTypes.ORDER.STATUS_BUYER_HAS_RECEIVED://交易成功
                    mTvPriceTag1.setText("客户已支付：");
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                    mTvPriceTag2.setText("已结算：");
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.memberProfitBean.profitMoney));
                    break;

                case AppTypes.ORDER.STATUS_BUYER_HAS_CLOSE://交易关闭
                    mTvPriceTag1.setText("客户已支付：");
                    mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.totalMoney));
                    mTvPriceTag2.setText("已结算：");
                    itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.memberProfitBean.profitMoney));
                    break;
                default:
                    break;
            }

            if (mModel == OrderListActivity.MODE_LIVE_STORE) {//卖家
                iv_im_icon.setVisibility(View.GONE);
                mStoreNameTv.setText(order.orderMain.nickName);
                mStoreNameTv.setTextColor(mContext.getResources().getColor(R.color.red));
                Drawable drawable= mContext.getResources().getDrawable(R.drawable.ic_order_message);
                drawable.setBounds(0, 0, SizeUtils.dp2px(17), SizeUtils.dp2px(17));
                mStoreNameTv.setCompoundDrawablePadding(SizeUtils.dp2px(10));
                mStoreNameTv.setCompoundDrawables(drawable,null,null,null);
                mTvPriceTag1.setText("买家实付款：");
                mTvPriceTag2.setText("你实收款：");
                mPriceTv.setText(ConvertUtil.centToCurrency(context, order.orderMain.payMoney));
                itemPayMoney.setText(ConvertUtil.centToCurrency(context, order.profitMoney));

                switch (order.orderMain.status) {
                    case AppTypes.ORDER.STATUS_SELLER_WAIT_SHIP:
                        mItemShit.setVisibility(View.VISIBLE);
                        mItemShit.setOnClickListener(v -> OrderService.shipLive(context, order));
                        break;
                    case AppTypes.ORDER.STATUS_SELLER_HAS_SHIP:
                        mViewExpressBtn.setVisibility(View.VISIBLE);
                        mViewExpressBtn.setOnClickListener(v -> OrderService.viewExpress(context, order));
                        break;
                    case AppTypes.ORDER.STATUS_SELLER_HAS_COMPLETE:
                        break;
                    case AppTypes.ORDER.STATUS_SELLER_HAS_CLOSE:
                    case 8:
                    case 7:
                        break;
                    default:
                        break;
                }
            }
        }

    }

}
