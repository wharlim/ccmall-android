package com.weiju.ccmall.newRetail.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.tencent.qcloud.tim.uikit.modules.chat.ChatLayout;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.tencent.qcloud.tim.uikit.modules.chat.layout.message.MessageLayout;
import com.tencent.qcloud.tim.uikit.modules.message.MessageInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.util.ToastUtil;

public class ChatActivity extends BaseActivity {

    ChatLayout chatLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_layout);
        ChatInfo chatInfo = (ChatInfo) getIntent().getSerializableExtra("chatInfo");

        if (chatInfo.getId().equals(chatInfo.getChatName())) {
            chatInfo.setChatName("嗨购用户");
        }

        chatLayout = findViewById(R.id.chat_layout);
        chatLayout.initDefault();
        chatLayout.setChatInfo(chatInfo);

        chatLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ToastUtil.error("该用户还未创建聊天功能");
                return true;
            }
        });

        chatLayout.getMessageLayout().setOnItemClickListener(new MessageLayout.OnItemClickListener() {
            @Override
            public void onMessageLongClick(View view, int i, MessageInfo messageInfo) {

            }

            @Override
            public void onUserIconClick(View view, int i, MessageInfo messageInfo) {

            }
        });
    }
}
