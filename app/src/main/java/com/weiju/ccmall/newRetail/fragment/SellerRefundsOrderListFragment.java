package com.weiju.ccmall.newRetail.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.auth.event.MsgStatus;
import com.weiju.ccmall.module.live.widgets.ListPopupMenu;
import com.weiju.ccmall.module.order.SellerRefundDetailActivity;
import com.weiju.ccmall.module.order.adapter.RefundsOrderListAdapter;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.newRetail.activity.StoreRefundListActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.RefundType;
import com.weiju.ccmall.shared.bean.RefundsOrder;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.OrderService;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.SessionUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

/**
 * 店铺经营-退款售后
 */
public class SellerRefundsOrderListFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.layoutCategory)
    LinearLayout layoutCategory;
    @BindView(R.id.tvCategory)
    TextView tvCategory;

    private List<RefundsOrder> mDatas = new ArrayList<>();
    private List<RefundType> mTypes = new ArrayList<>();
    private RefundsOrderListAdapter mAdapter;
    private IOrderService mOrderService;
    private String mType;
    private int mProductType;
    private String mRefundStatus;
    private ListPopupMenu mListPopupMenu;
    private String mAllType;


    /**
     * 店主查看售后记录列表
     *
     * @param type (0.待处理，1待寄回，2，待收货，3待退款全部不传)
     * @return
     */

    public static SellerRefundsOrderListFragment newInstance(String type) {
        Bundle args = new Bundle();
        args.putString(Config.INTENT_KEY_TYPE_NAME, type);
        SellerRefundsOrderListFragment fragment = new SellerRefundsOrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * @param type        "2", "退款"   "1", "退货"
     * @param productType 28 直播小店产品
     * @return
     */
    public static SellerRefundsOrderListFragment newInstance(String type, int productType) {
        Bundle args = new Bundle();
        args.putString(Config.INTENT_KEY_TYPE_NAME, type);
        args.putInt("productType", productType);
        SellerRefundsOrderListFragment fragment = new SellerRefundsOrderListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_seller_refund_order_list, container, false);
        ButterKnife.bind(this, inflate);
        return inflate;
    }

    @Override
    protected boolean isNeedLogin() {
        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getIntentData();
        initView();
        getData(true);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void getData(final boolean isRefresh) {
        if (isRefresh) {
            mDatas.clear();
            mAdapter.notifyDataSetChanged();
        }
        int page = mDatas.size() / Constants.PAGE_SIZE + 1;
        Observable<RequestResult<List<RefundsOrder>>> observable = null;

        Observable<RequestResult<PaginationEntity<RefundsOrder, Object>>> order = mOrderService.getStoreOrderRefundList(page, Constants.PAGE_SIZE, "onnOrder", mType);
        if (mProductType == StoreRefundListActivity.TYPE_LIVE_SHOP) {
            order = mOrderService.getStoreRefundList(page, Constants.PAGE_SIZE, mProductType, mType, mRefundStatus);
        }
        observable = order
                .flatMap(new Function<RequestResult<PaginationEntity<RefundsOrder, Object>>, ObservableSource<RequestResult<List<RefundsOrder>>>>() {
                    @Override
                    public ObservableSource<RequestResult<List<RefundsOrder>>> apply(RequestResult<PaginationEntity<RefundsOrder, Object>> result) throws Exception {
                        RequestResult<List<RefundsOrder>> listRequestResult = new RequestResult<>();
                        listRequestResult.code = 0;
                        listRequestResult.data = result.data.list;
                        return Observable.just(listRequestResult);
                    }
                });
        APIManager.startRequest(observable, new BaseRequestListener<List<RefundsOrder>>(mRefreshLayout) {
            @Override
            public void onSuccess(List<RefundsOrder> datas) {
                super.onSuccess(datas);
                mDatas.addAll(datas);
                mAdapter.notifyDataSetChanged();
                if (datas.size() < Constants.PAGE_SIZE) {
                    mAdapter.loadMoreEnd();
                } else {
                    mAdapter.loadMoreComplete();
                }
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                mAdapter.loadMoreFail();
            }
        }, getContext());
    }

    private void initView() {
        mOrderService = ServiceManager.getInstance().createService(IOrderService.class);
        if (mProductType == StoreRefundListActivity.TYPE_LIVE_SHOP) {
            layoutCategory.setVisibility(View.VISIBLE);
            mAllType = mType.equals("2") ? "全部退款单" : "全部退货单";
            tvCategory.setText(mAllType);
            RefundType refundtype = new RefundType();
            refundtype.key = mRefundStatus;
            refundtype.values = mAllType;
            mTypes.add(refundtype);
            mListPopupMenu = new ListPopupMenu(getContext(), new ListPopupMenu.OnMenuClickListener() {
                @Override
                public void onMenuClick(String menuIndex, String title) {
                    initRefundType(menuIndex, title);
                    tvCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_down, 0);
                }
            });
            mListPopupMenu.setData(mTypes);
            getRefundType();
        }
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setNestedScrollingEnabled(false);
        mAdapter = new RefundsOrderListAdapter(getContext(), mDatas);
        if (mProductType == StoreRefundListActivity.TYPE_LIVE_SHOP) {
            mAdapter.setProductType(mProductType);
        }
        mAdapter.setSellerModel(true);
        mAdapter.setRefundType(mType);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setEmptyView(new NoData(getContext()));

        mRecyclerView.addOnItemTouchListener(new RefundsItemClicklListener());
        if (mProductType == StoreRefundListActivity.TYPE_LIVE_SHOP) {
            mAdapter.setOnItemChildClickListener((adapter, view, position) -> {
                RefundsOrder item = mAdapter.getItem(position);
                if (item == null) return;
                switch (view.getId()) {
                    case R.id.tvRefundsCode:    //联系买家
                        goToChat(item.apiRefundOrderBean);
                        break;
                    case R.id.tvItemStoreRefuse: //2"拒绝退款" : 1"拒绝退货"
                        if (mType.equals("2")) {
                            OrderService.showRefuseRefundMoneyDialog(getActivity(), item);
                        } else {
                            OrderService.showRefuseRefundGoodsDialog(getActivity(), item);
                        }
                        break;
                    case R.id.tvItemStoreAgree: //2"同意退款" : 1"同意退货"
                        if (mType.equals("2")) {
                            OrderService.showRefundMoneyDialog(getActivity(), item);
                        } else {
                            OrderService.showAgreeRefundGoodsDialog(getActivity(), item.apiRefundOrderBean.orderCode);
                        }
                        break;
                    case R.id.tvItemStoreFinish: //确认收货
                        OrderService.finishOrder(getActivity(), item);
                        break;
                }
            });
        }

        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                getData(false);
            }
        });
    }


    private void getIntentData() {
        mType = getArguments().getString(Config.INTENT_KEY_TYPE_NAME);
        mProductType = getArguments().getInt("productType");
    }

    private void getRefundType() {
        APIManager.startRequest(mOrderService.getRefundType(mType), new BaseRequestListener<List<RefundType>>(getActivity()) {
            @Override
            public void onSuccess(List<RefundType> result) {
                super.onSuccess(result);
                mTypes.addAll(result);
                mListPopupMenu.setData(mTypes);
            }
        }, getContext());
    }

    private void initRefundType(String key, String values) {
        tvCategory.setText(values);
        mRefundStatus = key;
        getData(true);
    }

    private void goToChat(RefundsOrder.ApiRefundOrderBeanEntity apiRefundOrderBean) {
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser != null) {
            TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                @Override
                public void onError(int i, String s) {
                }

                @Override
                public void onSuccess() {
                    IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                    APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                    }, getContext());
                    ChatInfo chatInfo = new ChatInfo();
                    chatInfo.setType(TIMConversationType.C2C);
                    chatInfo.setId(apiRefundOrderBean.memberId);
                    chatInfo.setChatName(apiRefundOrderBean.nickName);
                    Intent intent = new Intent(getContext(), ChatActivity.class);
                    intent.putExtra("chatInfo", chatInfo);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                }
            });
        }
    }

    @OnClick({R.id.tvCategory})
    public void onViewClicked(View view) {
        tvCategory.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_up, 0);
        mListPopupMenu.showAsDropDown(tvCategory);
    }

    class RefundsItemClicklListener extends OnItemClickListener {
        @Override
        public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
            Intent intent = new Intent(getContext(), SellerRefundDetailActivity.class);
            intent.putExtra(SellerRefundDetailActivity.KEY_IS_STORE_SALE_ORDER, mProductType != StoreRefundListActivity.TYPE_LIVE_SHOP);
            intent.putExtra("isLiveProduct", mProductType == StoreRefundListActivity.TYPE_LIVE_SHOP);
            intent.putExtra(Config.INTENT_KEY_ID, mDatas.get(position).apiRefundOrderBean.refundId);
            intent.putExtra("memberId", mDatas.get(position).apiRefundOrderBean.memberId);
            startActivity(intent);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getStatus(MsgStatus msgStatus) {
        switch (msgStatus.getAction()) {
            case MsgStatus.ACTION_REFUND_CHANGE:
                getData(true);
                break;
        }
    }

}
