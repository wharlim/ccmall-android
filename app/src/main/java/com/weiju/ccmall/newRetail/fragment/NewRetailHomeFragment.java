package com.weiju.ccmall.newRetail.fragment;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderDetailActivity;
import com.weiju.ccmall.module.page.CustomPageActivity;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.activity.BusinessDataActivity;
import com.weiju.ccmall.newRetail.activity.GiftClientActivity;
import com.weiju.ccmall.newRetail.activity.MyStoreActivity;
import com.weiju.ccmall.newRetail.activity.MyStoreOrderListActivity;
import com.weiju.ccmall.newRetail.activity.NewRetailOrderListActivity;
import com.weiju.ccmall.newRetail.activity.StoreRefundListActivity;
import com.weiju.ccmall.newRetail.activity.StoreScoreActivity;
import com.weiju.ccmall.newRetail.activity.UpStoreMainPageActivity;
import com.weiju.ccmall.newRetail.bean.HappyBuyStore;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.GetOrderStatusCount;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.Store;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.ItemWithIcon;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.factory.PageElementFactory;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IPageService;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewRetailHomeFragment extends BaseFragment {

    @BindView(R.id.progress)
    SeekBar progressView;

    IOrderService orderService;

    IUserService userServicel;

    @BindView(R.id.to_be_processed)
    ItemWithIcon to_be_processed;

    @BindView(R.id.gift_client)
    ItemWithIcon gift_client;

    @BindView(R.id.avatarIv)
    SimpleDraweeView avatarIv;

    @BindView(R.id.tv_nickname)
    TextView tv_nickname;

    @BindView(R.id.tv_phonenumber)
    TextView tv_phonenumber;

    @BindView(R.id.tv_vip1)
    TextView tv_vip1;

    @BindView(R.id.tv_vip2)
    TextView tv_vip2;

    @BindView(R.id.tv_already_recommend)
    TextView tv_already_recommend;

    @BindView(R.id.rl_my_store_order)
    RelativeLayout rl_my_store_order;

    @BindView(R.id.cartBadgeTv)
    TextView cartBadgeTv;

    @BindView(R.id.levelTv)
    TextView levelTv;

    IProductService productService;

    User user;

    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.viewMoreOrderLayout)
    RelativeLayout viewMoreOrderLayout;

    @BindView(R.id.layoutProduct)
    LinearLayout layoutProduct;

    @BindView(R.id.itemPriceTv)
    TextView itemPriceTv;

    SkuInfo skuInfo;

    @BindView(R.id.itemTitleTv)
    TextView itemTitleTv;

    @BindView(R.id.itemPriceTvShow)
    TextView itemPriceTvShow;

    @BindView(R.id.itemThumbIv)
    SimpleDraweeView itemThumbIv;

    @BindView(R.id.tv_time)
    TextView tv_time;

    @BindView(R.id.tv_money)
    TextView tv_money;
    @BindView(R.id.bannerContainer)
    FrameLayout bannerContainer;

    AlertDialog alertDialog;

    //    AlertDialog alertDialog;
    IPageService iPageService;
    @BindView(R.id.mystoreIcon)
    SimpleDraweeView mystoreIcon;
    @BindView(R.id.mystoreName)
    TextView mystoreName;
    @BindView(R.id.mystoreGoodsCount)
    TextView mystoreGoodsCount;
    @BindView(R.id.mystoreGood0)
    SimpleDraweeView mystoreGood0;
    @BindView(R.id.mystoreGood1)
    SimpleDraweeView mystoreGood1;
    @BindView(R.id.mystoreGood2)
    SimpleDraweeView mystoreGood2;
    @BindView(R.id.mystoreGood3)
    SimpleDraweeView mystoreGood3;
    @BindView(R.id.mystoreGood4)
    SimpleDraweeView mystoreGood4;
    @BindView(R.id.mystoreLeftTime)
    TextView mystoreLeftTime;
    @BindView(R.id.mystoreContainer)
    CardView mystoreContainer;
    @BindView(R.id.ll_my_business_data)
    LinearLayout myBusinessData;
    @BindView(R.id.mystoreGoodMore)
    TextView mystoreGoodMore;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_newretail_home, container, false);
        ButterKnife.bind(this, view);
        progressView.setEnabled(false);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initDatas();
                refreshLayout.setRefreshing(false);
            }
        });

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser)
            initDatas();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        initDatas();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))//加上判断
            EventBus.getDefault().unregister(this);
    }

    public void initDatas() {


        productService = ServiceManager.getInstance().createService(IProductService.class);
        orderService = ServiceManager.getInstance().createService(IOrderService.class);
        userServicel = ServiceManager.getInstance().createService(IUserService.class);
        iPageService = ServiceManager.getInstance().createService(IPageService.class);


        APIManager.startRequest(userServicel.getNewGiftUserInfo(), new BaseRequestListener<User>() {
            @Override
            public void onSuccess(User result) {
                user = result;
                tv_nickname.setText(result.nickname);
                FrescoUtil.setImage(avatarIv, result.avatar);
                tv_phonenumber.setText(result.phone);
                tv_vip1.setText("C" + result.vipType);
                tv_vip2.setText("C" + (result.vipType + 1));
                levelTv.setText("C" + result.vipType + "用户");
                if (result.vipType == 0) {
                    tv_already_recommend.setVisibility(View.GONE);
                    progressView.setProgress(new Double(result.percent * 100).intValue());
                }
                if (result.vipType <= 4 && result.vipType != 0) {
                    if (result.vipType == 4) {
//                        int progress = result.conditionCount - 9;
                        tv_already_recommend.setText("已推荐" + result.conditionCount + "个C1");
                        progressView.setProgress(new Double(result.percent * 100).intValue());
                    } else {
//                        int progress = 3 - result.needCount;
                        tv_already_recommend.setText("已推荐" + result.conditionCount + "个C1");
                        progressView.setProgress(new Double(result.percent * 100).intValue());

                    }

                } else if (result.vipType > 4) {
//                    int progress = 5 - result.needCount;
                    tv_already_recommend.setText("已推荐" + result.conditionCount + "个C" + result.vipType);
                    progressView.setProgress(new Double(result.percent * 100).intValue());
                }
                if (result.isShopkeeper == 1) {
                    rl_my_store_order.setVisibility(View.VISIBLE);
                    APIManager.startRequest(orderService.getStoreGrade(), new BaseRequestListener<Store>() {
                        @Override
                        public void onSuccess(Store result) {
                            if (result.negativeComment >= result.warningCount) {
                                cartBadgeTv.setVisibility(View.VISIBLE);
                            }
                        }
                    }, getContext());
                } else {
                    rl_my_store_order.setVisibility(View.GONE);
                }
            }
        }, getContext());

        APIManager.startRequest(orderService.getOrderStatusCount("onnOrder"), new BaseRequestListener<GetOrderStatusCount>() {
            @Override
            public void onSuccess(GetOrderStatusCount result) {
//                if (rl_my_store_order.getVisibility() == View.VISIBLE)
//                    tv_pending_payment_number.setText(result.waitDel == 0 ? "" : result.waitDel + "");
//                    ((TextView) getActivity().findViewById(R.id.tv_pending_payment_number)).setText(result.waitDel);
//                gift_pending_payment.setBadge(result.waitDel);
//                if (rl_my_store_order.getVisibility() == View.VISIBLE)
//                    tv_to_be_delivered.setText(result.waitShip == 0 ? "" : result.waitShip + "");
//                    ((TextView) getActivity().findViewById(R.id.tv_to_be_delivered)).setText(result.waitDel);
//                    tv_to_be_delivered.setText(result.waitShip);
//                gift_to_be_delivered.setBadge(result.waitShip);
//                if (rl_my_store_order.getVisibility() == View.VISIBLE)
//                    tv_ending_receipt.setText(result.hasShip == 0 ? "" : result.hasShip + "");
//                    ((TextView) getActivity().findViewById(R.id.tv_ending_receipt)).setText(result.waitDel);
//                    tv_ending_receipt.setText(result.hasShip);
//                gift_ending_receipt.setBadge(result.hasShip);
//                to_be_processed.setBadge(result.saleCount);

            }
        }, getContext());

        getUpProduct();
        loadBanner();
    }

    private void loadBanner() {
        APIManager.startRequest(iPageService.getHibuyCarouselBanner(), new BaseRequestListener<Element>() {
            @Override
            public void onSuccess(Element result) {
                super.onSuccess(result);
                if (isAdded()) {
//                    Log.d("Seven", "res -> " + result);
                    float h = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 81, getResources().getDisplayMetrics());
                    result.height = (int) h;
                    View v = PageElementFactory.make(getContext(), result);
                    bannerContainer.removeAllViews();
                    bannerContainer.addView(v);
                }
            }
        }, getContext());
    }


    private DialogInterface.OnKeyListener onKeyListener1 = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }

            }
            return false;
        }
    };


    @OnClick({R.id.gift_after_sale, R.id.gift_pending_payment, R.id.gift_to_be_delivered, R.id.gift_ending_receipt, R.id.ll_my_order_shop_order, R.id.ll_my_business_data
            , R.id.ll_my_order_shop_rating, R.id.ll_my_order_store_products, R.id.tv_my_store_all_order, R.id.itemPayBtn,
            R.id.iv_arrow, R.id.to_be_processed, R.id.gift_client, R.id.bt_upgrade, R.id.iv_back, R.id.viewMoreOrderLayout})
    public void allViewClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.gift_after_sale: // 店铺经营-退款售后
                StoreRefundListActivity.start(getContext());
                break;
            case R.id.gift_pending_payment://店铺订单待发货
                intent = new Intent(getActivity(), MyStoreOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 2);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 1);
                startActivity(intent);
                break;

            case R.id.gift_to_be_delivered://店铺订单待收货
                intent = new Intent(getContext(), MyStoreOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 3);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 1);
                startActivity(intent);

                break;

            case R.id.gift_ending_receipt://店铺订单
                intent = new Intent(getContext(), MyStoreOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 3);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 1);
                startActivity(intent);
                break;
            case R.id.ll_my_business_data:
                BusinessDataActivity.start(getContext());
                break;

            case R.id.ll_my_order_shop_order://店铺订单--全部
                intent = new Intent(getContext(), MyStoreOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 0);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 1);
                startActivity(intent);
                break;

            //店铺评分
            case R.id.ll_my_order_shop_rating:
                startActivity(new Intent(getContext(), StoreScoreActivity.class));
                break;
            //店铺产品
            case R.id.ll_my_order_store_products:
                startActivity(new Intent(getContext(), MyStoreActivity.class));
                break;

            case R.id.tv_my_store_all_order://我的销售订单列表
            case R.id.iv_arrow:
                intent = new Intent(getContext(), NewRetailOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 0);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 2);
                startActivity(intent);
                break;

            case R.id.gift_client://嗨购客户
                startActivity(new Intent(getContext(), GiftClientActivity.class));
                break;

            case R.id.to_be_processed://我的销售订单--全部
                intent = new Intent(getContext(), NewRetailOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 0);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 2);
                startActivity(intent);
                break;

            case R.id.itemPayBtn://去买升级订单
                if (skuInfo != null) {
                    if (TextUtils.isEmpty(skuInfo.orderCode)) {
//                        getContext().getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, Const.ORDER_TYPE_OBLIGED).commit();
                        getContext().getSharedPreferences("pushId", 0).edit().putInt("pushId", skuInfo.pushId).commit();
                        intent = new Intent(getContext(), NewProductDetailActivity.class);
                        intent.putExtra(Key.SKU_ID, skuInfo.skuId);
                        intent.putExtra(Const.ORDER_TYPE, Const.ORDER_TYPE_OBLIGED);
                        startActivity(intent);
                    } else {
                        intent = new Intent(getContext(), OrderDetailActivity.class);
                        intent.putExtra("orderCode", skuInfo.orderCode);
                        intent.putExtra("mode", 4);
                        getContext().startActivity(intent);
                    }

                }
                break;


            case R.id.bt_upgrade://升级
                if (user != null) {
                    if (user.vipType == 5) {
                        if (user.isSeed == 1) {
                            if (user.seedAwardStatus == 0) {
                                showGetUpOrderDialog();
                                return;
                            }
                        }
                    }
                    LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View vPopupWindow = inflater.inflate(R.layout.popupwindow_level_layout, null, false);//引入弹窗布局
                    PopupWindow popupWindow = new PopupWindow(vPopupWindow, ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT, true);
                    popupWindow.showAtLocation(view, Gravity.BOTTOM | Gravity.RIGHT, 0, 0);
                    ((TextView) vPopupWindow.findViewById(R.id.tv_my_level)).setText("C" + user.vipType + "升级秘籍");

                    vPopupWindow.findViewById(R.id.iv_close).setOnClickListener(new View.OnClickListener() {//关闭升级弹窗
                        @Override
                        public void onClick(View view) {
                            popupWindow.dismiss();
                        }
                    });

                    RelativeLayout rl_3 = vPopupWindow.findViewById(R.id.rl_3);
                    RelativeLayout rl_4 = vPopupWindow.findViewById(R.id.rl_4);


                    TextView tv_upgrade_1 = vPopupWindow.findViewById(R.id.tv_upgrade_1);
                    TextView tv_upgrade_description_1 = vPopupWindow.findViewById(R.id.tv_upgrade_description_1);
                    ImageView iv_carry_1 = vPopupWindow.findViewById(R.id.iv_carry_1);
                    TextView tv_go_carry_1 = vPopupWindow.findViewById(R.id.tv_go_carry_1);

                    TextView tv_upgrade_2 = vPopupWindow.findViewById(R.id.tv_upgrade_2);
                    TextView tv_upgrade_description_2 = vPopupWindow.findViewById(R.id.tv_upgrade_description_2);
                    ImageView iv_carry_2 = vPopupWindow.findViewById(R.id.iv_carry_2);
                    TextView tv_go_carry_2 = vPopupWindow.findViewById(R.id.tv_go_carry_2);

                    TextView tv_upgrade_3 = vPopupWindow.findViewById(R.id.tv_upgrade_3);
                    TextView tv_upgrade_description_3 = vPopupWindow.findViewById(R.id.tv_upgrade_description_3);
                    ImageView iv_carry_3 = vPopupWindow.findViewById(R.id.iv_carry_3);
                    TextView tv_go_carry_3 = vPopupWindow.findViewById(R.id.tv_go_carry_3);

                    TextView tv_go_carry_4 = vPopupWindow.findViewById(R.id.tv_go_carry_4);

                    TextView tv_up_c5 = vPopupWindow.findViewById(R.id.tv_up_c5);
                    if (user.vipType == 0) {//C0
                        rl_3.setVisibility(View.GONE);
                        tv_upgrade_1.setText("自主选购礼包");
                        String str = "自主选购1个礼包并<br><font color='#f51861'>支付成功</font>";
                        tv_upgrade_description_1.setText(Html.fromHtml(str));
                        tv_upgrade_2.setText("完成升级订单");
                        String str1 = "系统随机匹配1个礼包并<br><font color='#f51861'>支付成功</font>";
                        tv_upgrade_description_2.setText(Html.fromHtml(str1));
                        if (user.isUpgrade == 0) {
                            tv_go_carry_1.setVisibility(View.VISIBLE);
                            tv_go_carry_1.setText("去选购");
                        } else {
                            iv_carry_1.setVisibility(View.VISIBLE);
                        }
                        tv_go_carry_2.setVisibility(View.VISIBLE);
                        tv_go_carry_2.setText("去下单");
                        tv_go_carry_4.setVisibility(View.VISIBLE);
                    } else if (user.vipType == 1 || user.vipType == 2 || user.vipType == 3) {//C1，C2,C3
                        rl_3.setVisibility(View.GONE);
                        int needCount = 3;
                        tv_upgrade_1.setText("开发" + (user.vipType * needCount) + "个C1客户");
                        String str = "我的客户C1用户需达到" + (user.vipType * needCount) + "人";
                        tv_upgrade_description_1.setText(Html.fromHtml(str));
                        tv_go_carry_1.setText("去开发");

                        tv_upgrade_2.setText("完成升级订单");

                        String str1 = "系统随机匹配1个礼包并<br><font color='#f51861'>支付成功</font>";
                        tv_upgrade_description_2.setText(Html.fromHtml(str1));
                        tv_go_carry_2.setVisibility(View.VISIBLE);
                        tv_go_carry_2.setText("去下单");

                        tv_go_carry_4.setVisibility(View.VISIBLE);

                        if (user.needCount > 0) {
                            tv_go_carry_1.setVisibility(View.VISIBLE);
                        } else {
                            iv_carry_1.setVisibility(View.VISIBLE);
                        }
                    } else if (user.vipType == 4) {//C4
                        int needCount = 3;
                        tv_upgrade_1.setText("开发" + (user.vipType * needCount) + "个C1客户");
                        String str = "我的客户C1用户需达到" + (user.vipType * needCount) + "人，且他们的嗨购订单<font color='#f51861'>已收货/已快速升级</font>";
//                        String str = "以<font color='#f51861'>已确认快速升级或已收货</font>的<br>销售订单为准";
                        String str1 = "系统随机匹配1个礼包<font color='#f51861'>并已确认<br>快速升级或已收货</font>";
                        tv_upgrade_description_1.setText(Html.fromHtml(str));
                        tv_upgrade_description_2.setText(Html.fromHtml(str1));
                        if (user.vipTypeCondition == 1) {//已达到商城等级
                            tv_upgrade_3.setText("VIP级别达到VIP用户");
                            tv_upgrade_description_3.setText("我的VIP级别达到[VIP用户]");
                            iv_carry_3.setVisibility(View.VISIBLE);
                        } else {//未达到
                            tv_upgrade_3.setText("VIP级别达到VIP用户");
                            tv_upgrade_description_3.setText("我的VIP级别达到[VIP用户]");
                            tv_go_carry_3.setVisibility(View.VISIBLE);
                            tv_go_carry_3.setText("去完成");
                        }
                        if (user.needCount > 0) {
                            tv_go_carry_1.setVisibility(View.VISIBLE);
                            tv_go_carry_2.setText("去下单");
                            tv_go_carry_2.setVisibility(View.VISIBLE);
                        } else {

                            if (!user.selfOrderStatus) {
                                tv_go_carry_2.setText("去下单");
                                tv_go_carry_2.setVisibility(View.VISIBLE);
                            } else {
                                if (user.oldVipType >= 3) {
                                    iv_carry_2.setVisibility(View.VISIBLE);
                                } else {
                                    tv_go_carry_2.setText("去下单");
                                    tv_go_carry_2.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                        if (user.needCount > 0) {
                            tv_go_carry_1.setVisibility(View.VISIBLE);
                            tv_go_carry_1.setText("去开发");
                        } else {
                            iv_carry_1.setVisibility(View.VISIBLE);
                        }

                        tv_go_carry_2.setVisibility(View.VISIBLE);
                        tv_go_carry_2.setText("去下单");
                        iv_carry_2.setVisibility(View.GONE);
                        tv_go_carry_4.setVisibility(View.VISIBLE);
                    } else if (user.vipType == 5) {//C5
                        rl_4.setVisibility(View.GONE);
                        tv_up_c5.setVisibility(View.GONE);
                        tv_upgrade_1.setText("开发15个C1或5个C5客户");
                        tv_upgrade_description_1.setText("开发15个C1客户或者开发5个C5客户。升为旗舰店后,只需开发8个C1便可升级");

                        if (user.needCount > 0 || user.needLowGradeCount > 0) {
                            tv_go_carry_1.setVisibility(View.VISIBLE);
                            tv_go_carry_1.setText("去开发");
                        } else {
                            iv_carry_1.setVisibility(View.VISIBLE);
                        }

                        tv_upgrade_2.setText("完成升级订单");
                        String str1 = "系统随机匹配1个礼包并<br><font color='#f51861'>支付成功</font>";
                        tv_upgrade_description_2.setText(Html.fromHtml(str1));
                        tv_go_carry_2.setVisibility(View.VISIBLE);
                        tv_go_carry_2.setText("去下单");
                        rl_3.setVisibility(View.GONE);

                    } else if (user.vipType == 6) {//C6
                        rl_4.setVisibility(View.GONE);
                        tv_up_c5.setVisibility(View.GONE);

                        tv_upgrade_1.setText("开发5个C" + user.vipType + "级别客户");
                        tv_upgrade_description_1.setText("我的客户C" + user.vipType + "用户需达到5人");

                        tv_upgrade_2.setText("完成升级订单");
                        String str1 = "系统随机匹配1个礼包并<br><font color='#f51861'>支付成功</font>";
                        tv_upgrade_description_2.setText(Html.fromHtml(str1));
                        tv_go_carry_2.setVisibility(View.VISIBLE);
                        tv_go_carry_2.setText("去下单");

                        if (user.needCount > 0) {
                            tv_go_carry_1.setVisibility(View.VISIBLE);
                            tv_go_carry_1.setText("去开发");
                        } else {
                            iv_carry_1.setVisibility(View.VISIBLE);
                        }

                        rl_3.setVisibility(View.GONE);
                        tv_go_carry_2.setText("去下单");
                        tv_go_carry_2.setVisibility(View.VISIBLE);
                    } else if (user.vipType >= 7) {//C7以上

                        tv_upgrade_1.setText("开发5个C" + user.vipType + "级别客户");
                        tv_upgrade_description_1.setText("我的客户C" + user.vipType + "用户需达到5人");

                        tv_upgrade_2.setText("完成升级订单");
                        String str1 = "系统随机匹配1个礼包并<br><font color='#f51861'>支付成功</font>";
                        tv_upgrade_description_2.setText(Html.fromHtml(str1));
                        tv_go_carry_2.setVisibility(View.VISIBLE);
                        tv_go_carry_2.setText("去下单");


                        if (user.needCount > 0) {
                            tv_go_carry_1.setVisibility(View.VISIBLE);
                            tv_go_carry_1.setText("去开发");
                        } else {
                            iv_carry_1.setVisibility(View.VISIBLE);
                        }

                        tv_upgrade_3.setText("VIP级别达到旗舰店");
                        tv_upgrade_description_3.setText("我的VIP级别达到[旗舰店]");
                        if (user.vipTypeCondition == 1) {//已达到商城等级
                            iv_carry_3.setVisibility(View.VISIBLE);
                        } else {//未达到
                            tv_go_carry_3.setVisibility(View.VISIBLE);
                            tv_go_carry_3.setText("去完成");
                        }

                        rl_4.setVisibility(View.GONE);
                        tv_up_c5.setVisibility(View.GONE);
                    }


                    tv_go_carry_4.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getContext(), CustomPageActivity.class);
                            intent.putExtra("pageId", user.pageId);
                            startActivity(intent);
                        }
                    });

                    tv_go_carry_3.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(getContext(), CustomPageActivity.class);
                            intent.putExtra("pageId", user.pageId);
                            startActivity(intent);
                        }
                    });


                    tv_go_carry_1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (user.vipType == 1 || user.vipType == 2 || user.vipType == 3 || user.vipType == 4) {
                                startActivity(new Intent(getContext(), GiftClientActivity.class));
                            } else if (user.vipType >= 5) {
                                startActivity(new Intent(getContext(), GiftClientActivity.class));
                            } else if (user.vipType == 0) {
                                if (TextUtils.isEmpty(user.orderCode)) {//
                                    popupWindow.dismiss();
                                    EventBus.getDefault().postSticky(new EventMessage(Event.select2Fragment));
                                } else {
                                    Intent intent = new Intent(getContext(), OrderDetailActivity.class);
                                    intent.putExtra("orderCode", user.orderCode);
                                    intent.putExtra("mode", 4);
                                    getContext().startActivity(intent);
                                }
                            }
                        }
                    });


                    tv_go_carry_2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (user.needLowGradeCount > 0 && user.vipType == 5) {
                                ToastUtil.error("您开发的客户数量尚未达标哦~");
                            } else if (user.needCount > 0) {
                                if (user.vipType == 0) {
                                    ToastUtil.error("请先自主选购1个嗨购礼包");
                                } else if (user.vipType == 1 || user.vipType == 2 || user.vipType == 3) {
                                    ToastUtil.error("您的销售目标尚未达标哦~");
                                } else if (user.vipType == 4) {
                                    ToastUtil.error("客户" + user.tipMobile + "的自选嗨购订单或系统升级订单尚未确认收货/快速升级");
                                } else if (user.vipType >= 5) {
                                    ToastUtil.error("您开发的客户数量尚未达标哦~");
                                }
                            } else {
                                if (user.isUpgrade == 1) {
                                    if (user.vipType == 4) {
                                        if (!user.selfOrderStatus) {
                                            ToastUtil.error("你还有自己的嗨购订单未快速升级/未确认收货哦！");
                                        } else {
                                            if (user.vipTypeCondition == 1) {
                                                View view1 = LayoutInflater.from(getContext()).inflate(R.layout.push_order_dialog, null);
                                                TextView tv_confirmed = view1.findViewById(R.id.tv_confirmed);
                                                TextView tv_back = view1.findViewById(R.id.tv_back);
                                                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                                AlertDialog alertDialog = builder.create();
                                                alertDialog.setView(view1, 0, 0, 0, 0);
                                                alertDialog.show();

                                                tv_confirmed.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        getUp();
                                                        alertDialog.dismiss();
                                                        popupWindow.dismiss();
                                                    }
                                                });

                                                tv_back.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View view) {
                                                        alertDialog.dismiss();
                                                    }
                                                });
                                            } else {
                                                ToastUtil.error("您的商城会员级别尚未到达VIP用户");
                                            }
                                        }
                                    } else if (user.vipType == 5) {
                                        View view1 = LayoutInflater.from(getContext()).inflate(R.layout.push_order_dialog, null);
                                        TextView tv_confirmed = view1.findViewById(R.id.tv_confirmed);
                                        TextView tv_back = view1.findViewById(R.id.tv_back);
                                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                        AlertDialog alertDialog = builder.create();
                                        alertDialog.setView(view1, 0, 0, 0, 0);
                                        alertDialog.show();

                                        tv_confirmed.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                getUp();
                                                alertDialog.dismiss();
                                                popupWindow.dismiss();
                                            }
                                        });

                                        tv_back.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                alertDialog.dismiss();
                                            }
                                        });
                                    } else if (user.vipType == 7) {
                                        if (user.vipTypeCondition == 1) {
                                            View view1 = LayoutInflater.from(getContext()).inflate(R.layout.push_order_dialog, null);
                                            TextView tv_confirmed = view1.findViewById(R.id.tv_confirmed);
                                            TextView tv_back = view1.findViewById(R.id.tv_back);
                                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                            AlertDialog alertDialog = builder.create();
                                            alertDialog.setView(view1, 0, 0, 0, 0);
                                            alertDialog.show();

                                            tv_confirmed.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    getUp();
                                                    alertDialog.dismiss();
                                                    popupWindow.dismiss();
                                                }
                                            });

                                            tv_back.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    alertDialog.dismiss();
                                                }
                                            });
                                        } else {
                                            ToastUtil.error("您的商城尚未达到旗舰店");
                                        }
                                    } else {
                                        View view1 = LayoutInflater.from(getContext()).inflate(R.layout.push_order_dialog, null);
                                        TextView tv_confirmed = view1.findViewById(R.id.tv_confirmed);
                                        TextView tv_back = view1.findViewById(R.id.tv_back);
                                        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                                        AlertDialog alertDialog = builder.create();
                                        alertDialog.setView(view1, 0, 0, 0, 0);
                                        alertDialog.show();

                                        tv_confirmed.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                getUp();
                                                alertDialog.dismiss();
                                                popupWindow.dismiss();
                                            }
                                        });

                                        tv_back.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                alertDialog.dismiss();
                                            }
                                        });
                                    }
                                } else {
                                    ToastUtil.error("你还有自己的嗨购订单未快速升级/未确认收货哦！");

                                }
                            }
                        }
                    });
                }


                break;

            case R.id.iv_back:
                getActivity().finish();
                break;

            //全部
            case R.id.viewMoreOrderLayout:

                intent = new Intent(getActivity(), NewRetailOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 0);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 3);
                startActivity(intent);
                break;

            default:
                break;

        }
    }

    private CountDownTimer timer;

    private void startTimer(long millisInFuture) {
        if (timer != null) {
            timer.cancel();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
        timer = new CountDownTimer(millisInFuture, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mystoreLeftTime.setText(formatter.format(millisUntilFinished));
            }

            @Override
            public void onFinish() {
                mystoreLeftTime.setText(formatter.format(0L));
                mystoreContainer.setVisibility(View.GONE);
                timer = null;
            }
        }.start();
    }

    private HappyBuyStore store;

    /**
     * 获取升级订单
     */
    private void getUpProduct() {
        BaseRequestListener<HappyBuyStore> listener = new BaseRequestListener<HappyBuyStore>() {
            @Override
            public void onSuccess(HappyBuyStore store) {
                super.onSuccess(store);
//                String json = "{\"pushId\":10124,\"storeImage\":\"\",\"proList\":[{\"skuId\":\"\",\"productId\":\"002d47732d904279a846651a9e086fd2\",\"skuName\":\"\",\"intro\":\"\",\"weight\":0,\"stock\":0,\"retailPrice\":0,\"marketPrice\":0,\"shippingPrice\":0,\"totalSaleCount\":0,\"saleCount\":0,\"hasPresent\":0,\"quantity\":0,\"status\":0,\"discountStatus\":1,\"buyScore\":0,\"ticket\":0,\"coin\":0,\"distributionProfit\":0,\"goldenTicket\":0,\"serviceFee\":0,\"transactionFee\":0,\"storeName\":\"礼包店铺6\",\"thumbUrl\":\"https://static.create-chain.net/ccmall/50/b3/7c/95b24b2a2cc848bf84eca6bdc69bc11f.png\",\"storeImage\":\"\"}],\"storeName\":\"礼包店铺6\",\"orderCode\":\"\",\"lessTime\":1198991,\"totalCount\":1}";
//                Gson gson = new Gson();
//                store = gson.fromJson(json, HappyBuyStore.class);
                if (isAdded() && getActivity() != null) {
//                    Log.d("Seven", store.toString());
                    if (TextUtils.isEmpty(store.storeName)) {
                        mystoreContainer.setVisibility(View.GONE);
                    } else {
                        NewRetailHomeFragment.this.store = store;
                        mystoreContainer.setVisibility(View.VISIBLE);
                        FrescoUtil.setImageSmall(mystoreIcon, store.storeImage);
                        mystoreName.setText(store.storeName);
                        mystoreGoodsCount.setText("产品数:" + store.totalCount);
                        SimpleDraweeView[] goods = new SimpleDraweeView[]{mystoreGood0, mystoreGood1, mystoreGood2, mystoreGood3, mystoreGood4};
                        int count = store.proList == null ? 0 : Math.min(goods.length, store.proList.size());
                        for (int i = 0; i < count; ++i) {
                            goods[i].setVisibility(View.VISIBLE);
                            FrescoUtil.setImageSmall(goods[i], store.proList.get(i).thumb);
                        }
                        for (int i = count; i < goods.length; ++i) {
                            goods[i].setVisibility(View.GONE);
                        }
                        mystoreGoodMore.setVisibility(store.proList.size() > goods.length ? View.VISIBLE : View.GONE);
                        startTimer(store.lessTime);
                    }
                }
            }
        };
        APIManager.startRequest(productService.getUpProductHappyBuy(), listener, getContext());
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                APIManager.startRequest(productService.getUpProduct(), new BaseRequestListener<PaginationEntity<SkuInfo, Object>>() {
//                    @Override
//                    public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
//                        if (result.list != null && result.list.size() > 0) {
//
//                            skuInfo = result.list.get(0);
//                            if (!TextUtils.isEmpty(skuInfo.name)) {
//
//                                layoutProduct.setVisibility(View.VISIBLE);
//                                itemTitleTv.setText(skuInfo.name);
//                                itemPriceTv.setText(ConvertUtil.centToCurrency(getContext(), skuInfo.retailPrice));
//                                TextViewUtil.addThroughLine(itemPriceTvShow);
//                                itemPriceTvShow.setText(ConvertUtil.centToCurrency(getContext(), skuInfo.marketPrice));
//                                FrescoUtil.setSkuImgMask(itemThumbIv, skuInfo);
//                                tv_money.setText(ConvertUtil.centToCurrency(getContext(), skuInfo.retailPrice));
//                                SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
//                                long remaining = skuInfo.lessTime;
//                                if (remaining > 0) {
//                                    TimerControl.getInstance().closeAll();
//                                    CountDownTimer timer = new CountDownTimer(remaining, 1000) {
//                                        @Override
//                                        public void onTick(long l) {
//                                            String ms = formatter.format(l);
//                                            String str = "请在" + ms + "内支付,";
//                                            SpannableString spannableString = new SpannableString(str);
//                                            ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
//                                            spannableString.setSpan(foregroundColorSpan, str.indexOf("在") + 1, str.indexOf("内"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
//                                            tv_time.setText(spannableString);
//                                        }
//
//                                        @Override
//                                        public void onFinish() {
////                                    initData();
//                                            layoutProduct.setVisibility(View.GONE);
//                                        }
//                                    }.start();
//                                    TimerControl.getInstance().add(timer);
//                                }
//                            } else {
//                                layoutProduct.setVisibility(View.GONE);
//                            }
//                        } else {
//                            layoutProduct.setVisibility(View.GONE);
//                        }
//                    }
//                }, getContext());
//            }
//        }).start();
    }

    public void getUp() {
        APIManager.startRequest(productService.getUp(), new BaseRequestListener<Object>() {

            @Override
            public void onSuccess(Object result) {
                super.onSuccess(result);
                ToastUtil.success("匹配成功，请尽快支付");
                getUpProduct();
            }
        }, getContext());
    }

    private void showDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(msg);
        //点击对话框以外的区域是否让对话框消失
        builder.setCancelable(false);
        builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBus(EventMessage message) {
        if (message.getEvent().equals(Event.isReadHigou)) {
            alertDialog.dismiss();
        }
        if (message.getEvent().equals(Event.newRetailPaySuccess) || message.getEvent().equals(Event.createOrderSuccess)) {
            initDatas();
        }
    }


    private void showGetUpOrderDialog() {
        View view1 = LayoutInflater.from(getContext()).inflate(R.layout.push_order_dialog, null);
        TextView tv_confirmed = view1.findViewById(R.id.tv_confirmed);
        TextView tv_back = view1.findViewById(R.id.tv_back);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        AlertDialog alertDialog = builder.create();
        alertDialog.setView(view1, 0, 0, 0, 0);
        alertDialog.show();

        tv_confirmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getUp();
                alertDialog.dismiss();
            }
        });

        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    @OnClick({R.id.mystoreGoodMore, R.id.mystoreGoBuy, R.id.mystoreContainer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.mystoreGoodMore:
                break;
            case R.id.mystoreContainer:
            case R.id.mystoreGoBuy:
                if (!TextUtils.isEmpty(store.orderCode)) {
                    goOrderDetail(store.orderCode);
                } else {
                    UpStoreMainPageActivity.start(getContext());
                }
                break;
        }
    }

    private void goOrderDetail(String orderCode) {
        if (!isAdded()) {
            return;
        }
        Intent intent = new Intent(getContext(), OrderDetailActivity.class);
        intent.putExtra("orderCode", orderCode);
        intent.putExtra("mode", 0);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getContext().startActivity(intent);
    }
}
