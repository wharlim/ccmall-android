package com.weiju.ccmall.newRetail.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ClientBeanList {

    @SerializedName("datas")
    public List<ClientBean> datas;

    @SerializedName("totalRecord")
    public int totalRecord;

    @SerializedName("totalPage")
    public int totalPage;


    public static class ClientBean implements Serializable {

        /*"memberId": "dac68adea0f44385ae2b",
                "headImage": "",
                "nickName": "",
                "phone": "",
                "vipType": 5,
                "vipTypeStr": "c5用户",
                "conditionCount": 0,
                "orderCode": "",
                "isUpgrade": 0,
                "needCount": "",
                "nextVipType": ""*/

        @SerializedName("memberId")
        public String memberId;

        @SerializedName("headImage")
        public String headImage;

        @SerializedName("nickName")
        public String nickName;

        @SerializedName("phone")
        public String phone;

        @SerializedName("vipType")
        public int vipType;

        @SerializedName("vipTypeStr")
        public String vipTypeStr;

        @SerializedName("conditionCount")
        public int conditionCount;

        @SerializedName("orderCode")
        public String orderCode;
        @SerializedName("noteName")
        public String noteName;
    }


}
