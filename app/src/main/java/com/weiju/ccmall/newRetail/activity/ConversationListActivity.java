package com.weiju.ccmall.newRetail.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMFriendshipManager;
import com.tencent.imsdk.TIMManager;
import com.tencent.imsdk.TIMUserProfile;
import com.tencent.imsdk.TIMValueCallBack;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.tencent.qcloud.tim.uikit.modules.conversation.ConversationLayout;
import com.tencent.qcloud.tim.uikit.modules.conversation.ConversationListLayout;
import com.tencent.qcloud.tim.uikit.modules.conversation.base.ConversationInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

public class ConversationListActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_test_im);
        showHeader();
        setLeftBlack();
        setTitle("聊天消息");


        // 从布局文件中获取会话列表面板
        ConversationLayout conversationLayout = findViewById(R.id.conversation_layout);
// 会话列表面板的默认 UI 和交互初始化
        conversationLayout.initDefault();

        conversationLayout.getTitleBar().setVisibility(View.GONE);

        conversationLayout.getConversationList();


        ConversationListLayout conversationList = conversationLayout.getConversationList();

        List<String> users = new ArrayList<String>();
        for (int i = 0; conversationList.getAdapter().getItemCount() > i; i++) {
            users.add(conversationList.getAdapter().getItem(i).getId());
        }


//获取用户资料
        TIMFriendshipManager.getInstance().getUsersProfile(users, true, new TIMValueCallBack<List<TIMUserProfile>>() {
            @Override
            public void onError(int i, String s) {
                String a = s;
            }

            @Override
            public void onSuccess(List<TIMUserProfile> timUserProfiles) {
                for (int i = 0; conversationList.getAdapter().getItemCount() > i; i++) {
                    TIMUserProfile timUserProfile = timUserProfiles.get(i);
                    conversationList.getAdapter().getItem(i).setTitle(TextUtils.isEmpty(timUserProfile.getNickName()) ? "嗨购用户" : timUserProfile.getNickName());
                }
                conversationList.getAdapter().notifyDataSetChanged();
            }
        });


        conversationList.setOnItemClickListener(new ConversationListLayout.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int i, ConversationInfo conversationInfo) {

                User loginUser = SessionUtil.getInstance().getLoginUser();
                if (loginUser != null) {
                    TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                        @Override
                        public void onError(int i, String s) {
                            ToastUtil.error(s);
                        }

                        @Override
                        public void onSuccess() {
                            List<String> users = new ArrayList<String>();
                            users.add(conversationInfo.getId());
                            IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                            APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                            }, ConversationListActivity.this);
                            ChatInfo chatInfo = new ChatInfo();
                            chatInfo.setType(TIMConversationType.C2C);
                            chatInfo.setId(conversationInfo.getId());
                            chatInfo.setChatName(conversationInfo.getTitle());
                            Intent intent = new Intent(ConversationListActivity.this, ChatActivity.class);
                            intent.putExtra("chatInfo", chatInfo);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    });
                }
            }
        });

    }

    public void setLeftBlack() {
        mHeaderLayout.setLeftDrawable(R.mipmap.icon_back_black);
        mHeaderLayout.setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
