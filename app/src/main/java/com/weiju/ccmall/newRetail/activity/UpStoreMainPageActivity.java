package com.weiju.ccmall.newRetail.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.category.adapter.ProductListAdapter;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.bean.HappyBuyStore;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.FrescoUtil;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpStoreMainPageActivity extends BaseActivity implements PageManager.RequestListener  {

    IProductService productService;
    ProductListAdapter mAdapter;
    @BindView(R.id.mystoreIcon)
    SimpleDraweeView mystoreIcon;
    @BindView(R.id.mystoreName)
    TextView mystoreName;
    @BindView(R.id.mystoreGoodsCount)
    TextView mystoreGoodsCount;
    @BindView(R.id.mystoreLeftTime)
    TextView mystoreLeftTime;
    @BindView(R.id.leftTimeContainer)
    LinearLayout leftTimeContainer;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    LinearLayoutManager mSingleColumnLayoutManager;
    int pushId = 0;
    PageManager mPageManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_up_store_mainpage);
        ButterKnife.bind(this);
        productService = ServiceManager.getInstance().createService(IProductService.class);
        mAdapter = new ProductListAdapter(this, Const.ORDER_TYPE_OBLIGED);
        recycler.setAdapter(mAdapter);
        mSingleColumnLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
//        recycler.setLayoutManager(mSingleColumnLayoutManager);
        try {
            mPageManager = PageManager.getInstance()
//                    .setSwipeRefreshLayout(mRefreshLayout)
                    .setRecyclerView(recycler)
                    .setLayoutManager(mSingleColumnLayoutManager)
                    .setRequestListener(this)
                    .build(this);
        } catch (PageManager.PageManagerException e) {
            e.printStackTrace();
        }

        setLeftBlack();
        getHeaderLayout().setTitle("店铺主页");
        loadData();

//        mPageManager.onRefresh();
    }

    private void loadData() {
        BaseRequestListener<HappyBuyStore> listener = new BaseRequestListener<HappyBuyStore>() {
            @Override
            public void onSuccess(HappyBuyStore result) {
                super.onSuccess(result);
                if (!TextUtils.isEmpty(result.storeName)) {
//                    mAdapter.setItems(result.proList);
                    startTimer(result.lessTime);
                    FrescoUtil.setImageSmall(mystoreIcon, result.storeImage);
                    mystoreName.setText(result.storeName);
                    mystoreGoodsCount.setText("产品数:" + result.totalCount);
                    pushId = result.pushId;
                    // 支付页面对应参数
                    getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, Const.ORDER_TYPE_OBLIGED).commit();
                    getSharedPreferences("pushId", 0).edit().putInt("pushId", pushId).commit();
                    loadProduct(1);
                }
            }
        };
        APIManager.startRequest(productService.getUpProductHappyBuy(), listener, this);

    }

    private void loadProduct(int page) {
        BaseRequestListener<PaginationEntity<SkuInfo, Object>> listener1 = new BaseRequestListener<PaginationEntity<SkuInfo, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                super.onSuccess(result);
                if (page == 1) {
                    mAdapter.getItems().clear();
                }
                mAdapter.addItems(result.list);
                mPageManager.setLoading(false);
                mPageManager.setTotalPage(result.totalPage);
            }
        };
        APIManager.startRequest(productService.getProList(page, 10, pushId), listener1, this);
    }

    public void setLeftBlack() {
        mHeaderLayout.setLeftDrawable(R.mipmap.icon_back_black);
        mHeaderLayout.setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private CountDownTimer timer;
    private void startTimer(long millisInFuture) {
        if (timer != null) {
            timer.cancel();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
        timer = new CountDownTimer(millisInFuture, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                mystoreLeftTime.setText(formatter.format(millisUntilFinished));
            }

            @Override
            public void onFinish() {
                mystoreLeftTime.setText(formatter.format(0L));
                finish();
                timer = null;
            }
        }.start();
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, UpStoreMainPageActivity.class);
        context.startActivity(intent);
    }

    @Override
    public void nextPage(int page) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, Const.NORMAL_MODE).commit();
        getSharedPreferences("pushId", 0).edit().putInt("pushId", 0).commit();
    }
}
