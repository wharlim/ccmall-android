package com.weiju.ccmall.newRetail.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.weiju.ccmall.R;
import com.weiju.ccmall.newRetail.fragment.SellerRefundsOrderListFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.util.ConvertUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreRefundListActivity extends BaseActivity {

    @BindView(R.id.magicIndicator)
    MagicIndicator mMagicIndicator;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    private int mProductType;

    public static int TYPE_LIVE_SHOP = 28;
    protected List<BaseFragment> mFragments = new ArrayList<>();
    protected List<Page> mPages = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_store_order_list_layout);
        ButterKnife.bind(this);
        mProductType = getIntent().getIntExtra("productType",0);
        initTab();
        initViewPager();
        initIndicator();
    }

    private void initTab() {
        setLeftBlack();
        if (mProductType == TYPE_LIVE_SHOP){    //直播小店传28
            setTitle("店铺售后单");
            mPages.add(new Page("2", "退款"));
            mPages.add(new Page("1", "退货"));
            for (Page page : mPages) {
                mFragments.add(SellerRefundsOrderListFragment.newInstance(page.id, TYPE_LIVE_SHOP));
            }
        } else {
            setTitle("退款售后");
            mPages.add(new Page("0", "待处理", 2));
            mPages.add(new Page("1", "待寄回", 2));
            mPages.add(new Page("2", "待收货", 2));
            mPages.add(new Page("3", "待退款", 2));
            mPages.add(new Page("", "全部", 2));
            for (Page page : mPages) {
                mFragments.add(SellerRefundsOrderListFragment.newInstance(page.id));
            }
        }
    }

    private void initIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mPages.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(mPages.get(index).name);
                titleView.setNormalColor(getResources().getColor(R.color.text_black));
                titleView.setSelectedColor(getResources().getColor(R.color.red));
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(16);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getResources().getColor(R.color.red));
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                return indicator;
            }
        });
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    private void initViewPager() {
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mPages.get(position).name;
            }
        });
        mViewPager.setOffscreenPageLimit(mFragments.size());
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, StoreRefundListActivity.class);
        context.startActivity(intent);
    }

    public static void start(Context context, int productType) {
        Intent intent = new Intent(context, StoreRefundListActivity.class);
        intent.putExtra("productType", productType);
        context.startActivity(intent);
    }
}
