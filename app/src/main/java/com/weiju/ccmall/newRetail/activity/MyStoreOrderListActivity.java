package com.weiju.ccmall.newRetail.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.weiju.ccmall.R;
import com.weiju.ccmall.newRetail.fragment.MyStoreOrderListFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.util.ConvertUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyStoreOrderListActivity extends BaseActivity {


    @BindView(R.id.magicIndicator)
    protected MagicIndicator mMagicIndicator;
    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;


    protected List<BaseFragment> mFragments = new ArrayList<>();
    protected List<Page> mPages = new ArrayList<>();

    public static final String ORDER_SELECTED = "ORDER_SELECTED";


    int selected = -1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_store_order_list_layout);
        ButterKnife.bind(this);
        setLeftBlack();
        initType();

        initViewPager();
        initIndicator();

    }

    private void initType() {
        setTitle("店铺订单");

        selected = getIntent().getIntExtra(ORDER_SELECTED, -1);

        mHeaderLayout.setRightDrawable(R.mipmap.icon_search);
        mHeaderLayout.setOnRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {//去搜索订单页
                Intent intent = new Intent(MyStoreOrderListActivity.this, StoreOrderSearchActivity.class);
                int currentItem = mViewPager.getCurrentItem();
                int orderStatus = 0;
                if (currentItem == 0) {
                    orderStatus = -1;
                } else if (currentItem == 1) {
                    orderStatus = 1;
                } else if (currentItem == 2) {
                    orderStatus = 2;
                } else if (currentItem == 3) {
                    orderStatus = 3;
                }
                intent.putExtra("orderStatus", orderStatus);
                startActivity(intent);
            }
        });
        mPages.add(new Page(AppTypes.ORDER.STORE_ORDER_ALL, "全部", 2));
        mPages.add(new Page(AppTypes.ORDER.STORE_ORDER_TO_BE_PROCESSED, "待付款", 2));
        mPages.add(new Page(AppTypes.ORDER.STORE_ORDER_TO_BE_DELIVERED, "待发货", 2));
        mPages.add(new Page(AppTypes.ORDER.STORE_ORDER_SHPIPPED, "待收货", 2));
        for (Page page : mPages) {
            mFragments.add(MyStoreOrderListFragment.newInstance(page));
        }

    }

    private void initIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mPages.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(mPages.get(index).name);
                titleView.setNormalColor(getResources().getColor(R.color.text_black));
                titleView.setSelectedColor(getResources().getColor(R.color.red));
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(16);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getResources().getColor(R.color.red));
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                return indicator;
            }
        });
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);

        if (-1 != selected) {
            mViewPager.setCurrentItem(selected, false);
        }
    }

    private void initViewPager() {
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mPages.get(position).name;
            }
        });
        mViewPager.setOffscreenPageLimit(mFragments.size());
    }

}
