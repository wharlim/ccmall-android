package com.weiju.ccmall.newRetail.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.NewRefundsOrderListActivity;
import com.weiju.ccmall.module.order.OrderDetailActivity;
import com.weiju.ccmall.module.product.MyCommentListActivity;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.activity.NewRetailOrderListActivity;
import com.weiju.ccmall.newRetail.utils.TimerControl;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.OrderCount;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.ItemWithIcon;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewRetailMeFragment extends BaseFragment {


    @BindView(R.id.orderUnpayCmp)
    ItemWithIcon orderUnpayCmp;

    @BindView(R.id.nicknameTv)
    TextView nicknameTv;

    @BindView(R.id.avatarIv)
    SimpleDraweeView avatarIv;

    @BindView(R.id.levelTv)
    TextView levelTv;

    @BindView(R.id.orderDispatchedCmp)
    ItemWithIcon mOrderDispatchedCmp;


    @BindView(R.id.orderPaidCmp)
    ItemWithIcon mOrderPaidCmp;

    @BindView(R.id.orderCommentCmp)
    ItemWithIcon orderCommentCmp;

    @BindView(R.id.orderServiceCmp)
    ItemWithIcon orderServiceCmp;

    IProductService productService;

    @BindView(R.id.itemTitleTv)
    TextView itemTitleTv;

    @BindView(R.id.tv_time)
    TextView tv_time;

    @BindView(R.id.itemPriceTv)
    TextView itemPriceTv;

    @BindView(R.id.itemPriceTvShow)
    TextView itemPriceTvShow;

    @BindView(R.id.itemAmountTv)
    TextView itemAmountTv;

    @BindView(R.id.ll_upgrade_product)
    LinearLayout ll_upgrade_product;

    @BindView(R.id.itemThumbIv)
    SimpleDraweeView itemThumbIv;

    @BindView(R.id.tv_money_number)
    TextView tv_money_number;

    @BindView(R.id.tv_money)
    TextView tv_money;

    SkuInfo skuInfo;

    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;

    protected boolean isVisible;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_newtail_me, container, false);
        ButterKnife.bind(this, rootView);
        orderUnpayCmp.setBadge(8);

//        EventBus.getDefault().register(this);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initData();
                refreshLayout.setRefreshing(false);
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            initData();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (EventBus.getDefault().isRegistered(this))//加上判断
            EventBus.getDefault().unregister(this);
    }


    private void initData() {
        IUserService userServicel = ServiceManager.getInstance().createService(IUserService.class);
        IOrderService orderService = ServiceManager.getInstance().createService(IOrderService.class);
        productService = ServiceManager.getInstance().createService(IProductService.class);
        APIManager.startRequest(userServicel.getNewGiftUserInfo(), new BaseRequestListener<User>() {
            @Override
            public void onSuccess(User result) {
                nicknameTv.setText(result.nickname);
                FrescoUtil.setImage(avatarIv, result.avatar);
                levelTv.setText("C" + result.vipType);
            }
        }, getContext());

        APIManager.startRequest(orderService.getOrderCount("onnOrder"), new BaseRequestListener<OrderCount>() {
            @Override
            public void onSuccess(OrderCount result) {
                orderUnpayCmp.setBadge(result.waitPay);
                mOrderDispatchedCmp.setBadge(result.hasShip);
                mOrderPaidCmp.setBadge(result.waitShip);
                orderCommentCmp.setBadge(result.waitComment);
                orderServiceCmp.setBadge(result.afterSales);


            }
        }, getContext());

        APIManager.startRequest(productService.getUpProduct(), new BaseRequestListener<PaginationEntity<SkuInfo, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                if (result.list != null) {
                    skuInfo = result.list.get(0);
                    if (!TextUtils.isEmpty(skuInfo.name)) {
                        ll_upgrade_product.setVisibility(View.VISIBLE);
                        itemTitleTv.setText(skuInfo.name);
                        itemPriceTv.setText(ConvertUtil.centToCurrency(getContext(), skuInfo.retailPrice));
                        TextViewUtil.addThroughLine(itemPriceTvShow);
                        itemPriceTvShow.setText(ConvertUtil.centToCurrency(getContext(), skuInfo.marketPrice));
                        FrescoUtil.setSkuImgMask(itemThumbIv, skuInfo);
                        tv_money.setText(ConvertUtil.centToCurrency(getContext(), skuInfo.retailPrice));
                        SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
                        long remaining = skuInfo.lessTime;
                        if (remaining > 0) {
                            TimerControl.getInstance().closeAll();
                            CountDownTimer timer = new CountDownTimer(remaining, 1000) {
                                @Override
                                public void onTick(long l) {
                                    String ms = formatter.format(l);
                                    String str = "请在" + ms + "内支付,";
                                    SpannableString spannableString = new SpannableString(str);
                                    ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
                                    spannableString.setSpan(foregroundColorSpan, str.indexOf("在") + 1, str.indexOf("内"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                    tv_time.setText(spannableString);
                                }

                                @Override
                                public void onFinish() {
                                    initData();
                                }
                            }.start();
                            TimerControl.getInstance().add(timer);
                        }
                    } else {
                        ll_upgrade_product.setVisibility(View.GONE);
                    }
                } else {
                    ll_upgrade_product.setVisibility(View.GONE);
                }
            }
        }, getContext());

    }


    @OnClick({R.id.viewMoreOrderLayout, R.id.orderUnpayCmp, R.id.orderPaidCmp, R.id.orderDispatchedCmp, R.id.orderCommentCmp, R.id.orderServiceCmp, R.id.itemPayBtn, R.id.iv_back})
    public void allViewClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            //全部
            case R.id.viewMoreOrderLayout:
                intent = new Intent(getActivity(), NewRetailOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 0);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 3);
                startActivity(intent);
                break;
            //待付款
            case R.id.orderUnpayCmp:
                intent = new Intent(getActivity(), NewRetailOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 1);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 3);
                startActivity(intent);
                break;
            //待发货
            case R.id.orderPaidCmp:
                intent = new Intent(getActivity(), NewRetailOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 3);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 3);
                startActivity(intent);
                break;
            //已发货
            case R.id.orderDispatchedCmp:
                intent = new Intent(getActivity(), NewRetailOrderListActivity.class);
                intent.putExtra(NewRetailOrderListActivity.ORDER_SELECTED, 4);
                intent.putExtra(NewRetailOrderListActivity.ORDER_TYPE, 3);
                startActivity(intent);
                break;
            //待评价
            case R.id.orderCommentCmp:
                intent = new Intent(getContext(), MyCommentListActivity.class);
                intent.putExtra(Const.CLASSIFICATION_TYPE, Const.NEWRETAIL_MODE);
                startActivity(intent);
                break;
            //售后
            case R.id.orderServiceCmp:
                Intent intent1 = new Intent(getContext(), NewRefundsOrderListActivity.class);
                intent1.putExtra("newRetail", "newRetail");

                startActivity(intent1);
                break;
            case R.id.itemPayBtn:
                if (skuInfo != null) {
                    if (TextUtils.isEmpty(skuInfo.orderCode)) {
//                        getContext().getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, Const.ORDER_TYPE_OBLIGED).commit();
                        getContext().getSharedPreferences("pushId", 0).edit().putInt("pushId", skuInfo.pushId).commit();
                        intent = new Intent(getContext(), NewProductDetailActivity.class);
                        intent.putExtra(Const.ORDER_TYPE, Const.ORDER_TYPE_OBLIGED);
                        intent.putExtra(Key.SKU_ID, skuInfo.skuId);
                        startActivity(intent);
                    } else {
                        intent = new Intent(getContext(), OrderDetailActivity.class);
                        intent.putExtra("orderCode", skuInfo.orderCode);
                        intent.putExtra("mode", 4);
                        getContext().startActivity(intent);
                    }

                }
                break;
            case R.id.iv_back:
                getActivity().finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        TimerControl.getInstance().closeAll();

//        initData();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void listHandler(EventMessage message) {
        if (message.getEvent().equals(Event.newRetailPaySuccess) || message.getEvent().equals(Event.createOrderSuccess)) {
            initData();
        }
    }
}
