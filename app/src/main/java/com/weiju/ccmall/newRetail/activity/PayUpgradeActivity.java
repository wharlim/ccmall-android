package com.weiju.ccmall.newRetail.activity;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.CountDownTimer;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.FormatException;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.BasicActivity;
import com.weiju.ccmall.module.order.OrderDetailActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.ActivityControl;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;

import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

public class PayUpgradeActivity extends BasicActivity {

    @BindView(R.id.tv_money)
    TextView tv_money;

    @BindView(R.id.tv_status)
    TextView tv_status;

    @BindView(R.id.tv_time)
    TextView tv_time;

    @BindView(R.id.tv_store_name)
    TextView tv_store_name;

    @BindView(R.id.tv_alipay_account)
    TextView tv_alipay_account;

    @BindView(R.id.iv_alipay_qrcode)
    ImageView iv_alipay_qrcode;

    @BindView(R.id.tv_order_number)
    TextView tv_order_number;

    @BindView(R.id.tv_seller_nickname)
    TextView tv_seller_nickname;

    @BindView(R.id.rl_alipay_account)
    RelativeLayout rl_alipay_account;


    private CountDownTimer mTimer;

    Order order;


    private static final String INTENT_URL_FORMAT = "intent://platformapi/startapp?saId=10000007&" +
            "clientVersion=3.7.0.0718&qrcode=https%3A%2F%2Fqr.alipay.com%2F{urlCode}%3F_s" +
            "%3Dweb-other&_t=1472443966571#Intent;" +
            "scheme=alipayqr;package=com.eg.android.AlipayGphone;end";

    @Override
    protected int getLayoutResId() {
        return R.layout.pay_upgrade_layout;
    }

    @Override
    protected void initDataNew() {
        String orderCode = getIntent().getStringExtra("orderCode");
        IOrderService orderService = ServiceManager.getInstance().createService(IOrderService.class);
        APIManager.startRequest(orderService.getOrderByCode(orderCode,""), new BaseRequestListener<Order>() {
            @Override
            public void onSuccess(Order result) {
                order = result;
                long remaining = result.orderMain.lessTime;
                if (0 >= remaining) {
                    ToastUtil.error("该订单已超时");
                    PayUpgradeActivity.this.finish();
                }
                long remaining1 = remaining - 900000;
                if ((order.orderMain.orderType == 5 && remaining1 <= 0) || order.orderMain.orderType == 6)
                    tv_money.setText(ConvertUtil.centToCurrency(PayUpgradeActivity.this, result.orderMain.totalMoney));
                else
                    tv_money.setText(ConvertUtil.centToCurrency(PayUpgradeActivity.this, result.orderMain.transactionFee));
//                tv_status.setText(result.orderMain.payTypeStr);
                tv_store_name.setText(result.storeUser.alipayReceiver);
                tv_alipay_account.setText(result.storeUser.alipayAccount);
                tv_order_number.setText(result.orderMain.orderCode);
                tv_seller_nickname.setText(result.storeUser.nickName);
                if (result.storeUser.displayStatus == 1) {
                    rl_alipay_account.setVisibility(View.GONE);
                }
                try {
                    if (!TextUtils.isEmpty(result.storeUser.alipayImageCode))
                        iv_alipay_qrcode.setImageBitmap(createQRCode(result.storeUser.alipayImageCode, 145));
                    SimpleDateFormat formatter = new SimpleDateFormat("mm:ss");
                    if (remaining > 0) {
                        if (mTimer == null) {
                            if (order.orderMain.orderType == 5 && remaining1 > 0) {
                                mTimer = new CountDownTimer(remaining1, 1000) {
                                    @Override
                                    public void onTick(long l) {
                                        String ms = formatter.format(l);
                                        String str = "请在" + ms + "内支付";
                                        SpannableString spannableString = new SpannableString(str);
                                        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
                                        spannableString.setSpan(foregroundColorSpan, str.indexOf("在") + 1, str.indexOf("内"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                        if (tv_time != null)
                                            tv_time.setText(spannableString);
                                    }

                                    @Override
                                    public void onFinish() {
                                        mTimer.cancel();
                                        PayUpgradeActivity.this.finish();
                                    }
                                }.start();
                            } else {
                                mTimer = new CountDownTimer(remaining, 1000) {
                                    @Override
                                    public void onTick(long l) {
                                        String ms = formatter.format(l);
                                        String str = "请在" + ms + "内支付";
                                        SpannableString spannableString = new SpannableString(str);
                                        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(Color.RED);
                                        spannableString.setSpan(foregroundColorSpan, str.indexOf("在") + 1, str.indexOf("内"), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                                        if (tv_time != null)
                                            tv_time.setText(spannableString);
                                    }

                                    @Override
                                    public void onFinish() {
                                        mTimer.cancel();
                                        PayUpgradeActivity.this.finish();
                                    }
                                }.start();
                            }
                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, this);

    }

    @OnClick({R.id.tv_no_pay, R.id.tv_paid, R.id.tv_pay, R.id.tv_name_copy, R.id.tv_alipay_account_copy, R.id.tv_order_number_copy})
    public void allViewClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.tv_no_pay:
                finish();
                break;
            case R.id.tv_paid://确认已支付
                View view1 = LayoutInflater.from(this).inflate(R.layout.confirm_payment_dialog, null);
                ImageView agreeIv = view1.findViewById(R.id.agreeIv);
                TextView tv_confirmed = view1.findViewById(R.id.tv_confirmed);
                TextView tv_back = view1.findViewById(R.id.tv_back);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                AlertDialog alertDialog = builder.create();
                alertDialog.setView(view1, 0, 0, 0, 0);
                alertDialog.show();

                tv_confirmed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (agreeIv.isSelected()) {
                            IOrderService orderService = ServiceManager.getInstance().createService(IOrderService.class);
                            APIManager.startRequest(orderService.comfirmPay(order.orderMain.orderId, order.orderMain.memberId), new BaseRequestListener<Object>() {
                                @Override
                                public void onSuccess(Object result) {
                                    EventBus.getDefault().post(new EventMessage(Event.newRetailPaySuccess));
                                    Intent intent = new Intent(PayUpgradeActivity.this, OrderDetailActivity.class);
                                    intent.putExtra("orderCode", order.orderMain.orderCode);
                                    intent.putExtra("mode", 4);
                                    startActivity(intent);
                                    ActivityControl.getInstance().closeAll();
                                    alertDialog.dismiss();
                                    finish();
                                }
                            }, PayUpgradeActivity.this);
                        } else {
                            ToastUtil.error("请先转账给对方");
                        }
                    }
                });

                agreeIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (agreeIv.isSelected()) {
                            agreeIv.setSelected(false);
                        } else {
                            agreeIv.setSelected(true);
                        }
                    }
                });

                tv_back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });


                break;
            case R.id.tv_pay:
                Map<DecodeHintType, String> hints = new HashMap<DecodeHintType, String>();
                hints.put(DecodeHintType.CHARACTER_SET, "utf-8");
                // 获得待解析的图片
                Bitmap bitmap = ((BitmapDrawable) iv_alipay_qrcode.getDrawable()).getBitmap();
                int[] data = new int[bitmap.getWidth() * bitmap.getHeight()];
                bitmap.getPixels(data, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
                RGBLuminanceSource source = new RGBLuminanceSource(bitmap.getWidth(), bitmap.getHeight(), data);
                BinaryBitmap bitmap1 = new BinaryBitmap(new HybridBinarizer(source));
                QRCodeReader reader = new QRCodeReader();
                Result result;
                try {
                    result = reader.decode(bitmap1, hints);
                    // 得到解析后的文字
                    String url = result.getText();
                    if (TextUtils.isEmpty(url)) {
                        ToastUtil.error("支付宝二维码有误");
                    } else {
                        if (checkAliPayInstalled(this)) {
                            String intentFullUrl = url.substring(url.lastIndexOf("/") + 1, url.length());
                            Intent intent1 = Intent.parseUri(
                                    INTENT_URL_FORMAT.replace("{urlCode}", intentFullUrl),
                                    Intent.URI_INTENT_SCHEME);
                            startActivity(intent1);
                        } else {
                            Uri uri = Uri.parse("http://m.alipay.com");
                            intent = new Intent(Intent.ACTION_VIEW, uri);
                            startActivity(intent);
                        }
                    }
                } catch (NotFoundException e) {
                    e.printStackTrace();
                    ToastUtil.error("支付宝二维码有误");
                } catch (ChecksumException e) {
                    e.printStackTrace();
                } catch (FormatException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                    ToastUtil.error("支付宝二维码有误");
                }
                break;
            case R.id.tv_name_copy:
                ClipboardManager cm = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData mClipData = ClipData.newPlainText("Label", order.nickName);
                cm.setPrimaryClip(mClipData);
                ToastUtil.success("复制成功");
                break;
            case R.id.tv_alipay_account_copy:
                ClipboardManager cm1 = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData mClipData1 = ClipData.newPlainText("Label", order.storeUser.alipayAccount);
                cm1.setPrimaryClip(mClipData1);
                ToastUtil.success("复制成功");
                break;
            case R.id.tv_order_number_copy:
                ClipboardManager cm2 = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData mClipData2 = ClipData.newPlainText("Label", order.orderMain.orderCode);
                cm2.setPrimaryClip(mClipData2);
                ToastUtil.success("复制成功");
                break;
            default:
                break;
        }
    }

    /**
     * 创建二维码
     *
     * @param str
     * @param widthAndHeight
     * @return
     * @throws WriterException
     */
    public static Bitmap createQRCode(String str, int widthAndHeight) throws WriterException {
        Hashtable<EncodeHintType, String> hints = new Hashtable<EncodeHintType, String>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        BitMatrix matrix = new MultiFormatWriter().encode(str,
                BarcodeFormat.QR_CODE, widthAndHeight, widthAndHeight);
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        int[] pixels = new int[width * height];

        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (matrix.get(x, y)) {
                    pixels[y * width + x] = BLACK;
                } else {
                    pixels[y * width + x] = WHITE;
                }
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(width, height,
                Bitmap.Config.RGB_565);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mTimer != null)
            mTimer.cancel();
    }

    /**
     * 判断是否有装支付宝
     *
     * @param context
     * @return
     */
    public static boolean checkAliPayInstalled(Context context) {

        Uri uri = Uri.parse("alipays://platformapi/startApp");
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        ComponentName componentName = intent.resolveActivity(context.getPackageManager());
        return componentName != null;
    }
}
