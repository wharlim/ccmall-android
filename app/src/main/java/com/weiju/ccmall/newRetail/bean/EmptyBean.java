package com.weiju.ccmall.newRetail.bean;

import com.google.gson.annotations.SerializedName;

public class EmptyBean  {
    @SerializedName("code")
    public int code;
    @SerializedName("message")
    public String message;
    @SerializedName("data")
    public String data;
}
