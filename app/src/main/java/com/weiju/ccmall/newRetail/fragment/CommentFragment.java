package com.weiju.ccmall.newRetail.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.adapter.ProductCommentAdapter;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.bean.ProductComment;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.decoration.SpacesItemDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentFragment extends BaseFragment implements PageManager.RequestListener {

    private ArrayList<ProductComment> mDatas = new ArrayList<>();

    private ProductCommentAdapter mAdapter = new ProductCommentAdapter(mDatas, true);

    private PageManager mPageManager;

    IProductService productService;

    @BindView(R.id.noDataLayout)
    NoData mNoDataLayout;
    private Page mPage;

    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;

    public static final int PRAISE = 1;

    public static final int AVERAGE = 2;

    public static final int BAD_REVIEW = 3;


    public static CommentFragment newInstance(Page page) {
        Bundle args = new Bundle();
        args.putSerializable("page", page);
        CommentFragment fragment = new CommentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.base_list_layout, container, false);
        ButterKnife.bind(this, view);
//        EventBus.getDefault().register(this);
        productService = ServiceManager.getInstance().createService(IProductService.class);
        getIntentData();
        initView();
        return view;
    }

    private void initView() {
        mRecyclerView.setAdapter(mAdapter);
        try {
            mPageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(mRecyclerView)
                    .setItemDecoration(new SpacesItemDecoration(ConvertUtil.dip2px(15), true))
                    .setRequestListener(this)
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .build(getContext());
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }
        mPageManager.onRefresh();
    }


    private void getIntentData() {
        mPage = (Page) getArguments().get("page");
    }

    @Override
    public void nextPage(int page) {
        if (page == 1) {
            mDatas.clear();
        }
        switch (mPage.id) {
            case PRAISE + "":
                APIManager.startRequest(productService.getNewGiftStoreComment(page, 10, PRAISE), new BaseRequestListener<PaginationEntity<ProductComment, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<ProductComment, Object> result) {

                        mDatas.addAll(result.list);
                        mAdapter.notifyDataSetChanged();
                        if (result.list.size() < Constants.PAGE_SIZE) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, getContext());
                break;
            case AVERAGE + "":
                APIManager.startRequest(productService.getNewGiftStoreComment(page, 10, AVERAGE), new BaseRequestListener<PaginationEntity<ProductComment, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<ProductComment, Object> result) {
                        mDatas.addAll(result.list);
                        mAdapter.notifyDataSetChanged();
                        if (result.list.size() < Constants.PAGE_SIZE) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, getContext());
                break;
            case BAD_REVIEW + "":
                APIManager.startRequest(productService.getNewGiftStoreComment(page, 10, BAD_REVIEW), new BaseRequestListener<PaginationEntity<ProductComment, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<ProductComment, Object> result) {
                        mDatas.addAll(result.list);
                        mAdapter.notifyDataSetChanged();
                        if (result.list.size() < Constants.PAGE_SIZE) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, getContext());
                break;
            default:
                break;
        }
    }
}
