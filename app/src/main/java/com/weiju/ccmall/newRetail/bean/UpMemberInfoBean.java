package com.weiju.ccmall.newRetail.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UpMemberInfoBean {

    @SerializedName("isBind")
    public int isBind;

    @SerializedName("upMember")
    public UpMember upMember;


    public static class UpMember implements Serializable {
        @SerializedName("memberId")
        public String memberId;
        @SerializedName("headImage")
        public String headImage;
        @SerializedName("nickName")
        public String nickName;
        @SerializedName("phone")
        public String phone;
        @SerializedName("vipType")
        public int vipType;
    }

}
