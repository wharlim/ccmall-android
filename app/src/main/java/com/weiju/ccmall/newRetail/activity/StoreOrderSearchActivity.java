package com.weiju.ccmall.newRetail.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderListActivity;
import com.weiju.ccmall.newRetail.adapter.MyStoreOrderListAdapter;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

public class StoreOrderSearchActivity extends Activity implements PageManager.RequestListener {

    @BindView(R.id.keywordEt)
    EditText keywordEt;

    private PageManager mPageManager;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;

    private MyStoreOrderListAdapter mOrderListAdapter;

    IOrderService orderService;

    @BindView(R.id.cancelBtn)
    TextView cancelBtn;

    @BindView(R.id.tv_no_data)
    TextView tv_no_data;

    int orderStatus = -1;

    private int mMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_srore_order_search);
        ButterKnife.bind(this);

        keywordEt.setHint("搜索订单编号");
        keywordEt.requestFocus();

        mMode = getIntent().getIntExtra("mode",-1);
//        orderStatus = getIntent().getIntExtra("orderStatus", -1);

        orderService = ServiceManager.getInstance().createService(IOrderService.class);
        mOrderListAdapter = new MyStoreOrderListAdapter(this, mMode);

        recyclerView.setAdapter(mOrderListAdapter);


        try {
            mPageManager = PageManager.getInstance()
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .setRecyclerView(recyclerView)
                    .setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false))
                    .setRequestListener(this)
                    .build(this);
        } catch (PageManager.PageManagerException e) {
            e.printStackTrace();
        }

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mRefreshLayout.setRefreshing(false);
            }
        });


        keywordEt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                mOrderListAdapter.cleanItems();
                ToastUtil.showLoading(StoreOrderSearchActivity.this);
                if (i == EditorInfo.IME_ACTION_SEARCH || (keyEvent != null && keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {

                    Observable<RequestResult<PaginationEntity<Order, Object>>> onnOrder = orderService.getStoreOrderList(orderStatus, 1, "onnOrder", keywordEt.getText().toString());
                    if (mMode == OrderListActivity.MODE_LIVE_STORE) onnOrder = orderService.orderSearch(1, keywordEt.getText().toString());

                    APIManager.startRequest(onnOrder, new BaseRequestListener<PaginationEntity<Order, Object>>(StoreOrderSearchActivity.this) {
                        @Override
                        public void onSuccess(PaginationEntity<Order, Object> result) {
                            super.onSuccess(result);
                            if (result.list.size() <= 0) {
                                tv_no_data.setVisibility(View.VISIBLE);
                                mRefreshLayout.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.GONE);
                                return;
                            } else {
                                tv_no_data.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                mRefreshLayout.setVisibility(View.VISIBLE);
                            }
                            mOrderListAdapter.addItems(result.list);
                            mOrderListAdapter.notifyDataSetChanged();
                        }
                    }, StoreOrderSearchActivity.this);
                    return true;
                }
                return false;
            }
        });

        keywordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                cancelBtn.setVisibility(charSequence.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void nextPage(int page) {
        if (page == 1) {
            mOrderListAdapter.removeAllItems();
        }
        Observable<RequestResult<PaginationEntity<Order, Object>>> onnOrder = orderService.getStoreOrderList(orderStatus, page, "onnOrder", keywordEt.getText().toString());
        if (mMode == OrderListActivity.MODE_LIVE_STORE) onnOrder = orderService.orderSearch(page, keywordEt.getText().toString());

        APIManager.startRequest(onnOrder, new BaseRequestListener<PaginationEntity<Order, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<Order, Object> result) {
                mOrderListAdapter.addItems(result.list);
                mOrderListAdapter.notifyDataSetChanged();
            }
        }, StoreOrderSearchActivity.this);
    }

    @OnClick({R.id.backBtn, R.id.cancelBtn})
    public void allViewClick(View view) {
        switch (view.getId()) {
            case R.id.backBtn:
                finish();
                break;
            case R.id.cancelBtn:
                keywordEt.requestFocus();
                keywordEt.setText("");
                cancelBtn.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }
}
