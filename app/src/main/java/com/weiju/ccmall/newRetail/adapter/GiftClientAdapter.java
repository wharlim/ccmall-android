package com.weiju.ccmall.newRetail.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.tencent.imsdk.TIMCallBack;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.imsdk.TIMManager;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderDetailActivity;
import com.weiju.ccmall.module.order.OrderSaleDetailActivity;
import com.weiju.ccmall.module.user.ChangeCustomerNoteNameActivity;
import com.weiju.ccmall.newRetail.activity.ChatActivity;
import com.weiju.ccmall.newRetail.bean.ClientBeanList;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import java.util.ArrayList;

public class GiftClientAdapter extends BaseQuickAdapter<ClientBeanList.ClientBean, BaseViewHolder> {


    public GiftClientAdapter(@Nullable ArrayList<ClientBeanList.ClientBean> data) {
        super(R.layout.item_gift_client_layout, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ClientBeanList.ClientBean item) {

//        helper.setText(R.id.tv_nickname, item.nickName).setText(R.id.tv_level, item.vipTypeStr)
//                .setText(R.id.tv_phone, item.phone);
//        SimpleDraweeView view = helper.getView(R.id.avatarIv);
//        FrescoUtil.setImage(view, item.headImage);
//        if (TextUtils.isEmpty(item.orderCode)) {
//            helper.setVisible(R.id.tv_sell, false);
//        } else {
//            helper.getView(R.id.tv_sell).setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent(mContext, OrderDetailActivity.class);
//                    intent.putExtra("orderCode", item.orderCode);
//                    intent.putExtra("mode", 3);
//                    mContext.startActivity(intent);
//                }
//            });
//        }
        SimpleDraweeView simpleDraweeView = helper.getView(R.id.avatarIv);
        FrescoUtil.setImageSmall(simpleDraweeView, item.headImage);
        helper.setText(R.id.customerLevel, item.vipTypeStr);
        helper.setText(R.id.noteName, TextUtils.isEmpty(item.noteName) ? item.nickName : item.noteName);
        helper.setText(R.id.phone, item.phone);
        if (!TextUtils.isEmpty(item.noteName)) {
            helper.setText(R.id.nickName, "昵称:"+item.nickName);
        } else {
            helper.setText(R.id.nickName, "");
        }
        helper.getView(R.id.setNoteBtn).setOnClickListener(v -> {
            ChangeCustomerNoteNameActivity.start((Activity) helper.itemView.getContext(),
                    item.memberId, item.noteName);
        });
        helper.getView(R.id.checkOrderStateBtn).setVisibility(TextUtils.isEmpty(item.orderCode) ? View.INVISIBLE : View.VISIBLE);
        helper.getView(R.id.checkOrderStateBtn).setOnClickListener(v -> {
            OrderSaleDetailActivity.start(helper.itemView.getContext(), item.orderCode);
        });
        helper.getView(R.id.chatBtn).setOnClickListener(v -> {
            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (loginUser != null) {
                TIMManager.getInstance().autoLogin(loginUser.id, new TIMCallBack() {
                    @Override
                    public void onError(int i, String s) {
                    }

                    @Override
                    public void onSuccess() {
                        IUserService mUserService = ServiceManager.getInstance().createService(IUserService.class);
                        APIManager.startRequest(mUserService.tencentIMLogin(), new BaseRequestListener<Object>() {
                        }, mContext);
                        ChatInfo chatInfo = new ChatInfo();
                        chatInfo.setType(TIMConversationType.C2C);
                        chatInfo.setId(item.memberId);
                        chatInfo.setChatName(TextUtils.isEmpty(item.noteName) ? item.nickName : item.noteName);
                        Context context = mContext;
                        Intent intent = new Intent(context, ChatActivity.class);
                        intent.putExtra("chatInfo", chatInfo);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                });
            }
        });
    }
}
