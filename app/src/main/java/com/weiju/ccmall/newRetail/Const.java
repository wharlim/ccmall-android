package com.weiju.ccmall.newRetail;

public class Const {

    public static final String CLASSIFICATION_TYPE = "CLASSIFICATION_TYPE";
    public static final String NORMAL_MODE = "NORMAL_MODE";
    public static final String NEWRETAIL_MODE = "NEWRETAIL_MODE";


    public static final String ORDER_TYPE = "ORDER_TYPE";
    public static final String ORDER_TYPE_FREE_ORDER = "ORDER_TYPE_FREE_ORDER";
    public static final String ORDER_TYPE_OBLIGED = "ORDER_TYPE_OBLIGED";

    public static final String PAY_199 = "PAY_199";

    public static final String READ_HIGOU_NEED_TO_KNOW = "READ_HIGOU_NEED_TO_KNOW";
}
