package com.weiju.ccmall.newRetail.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CheckTimeOutBean implements Serializable {

    @SerializedName("status")
    public boolean status;

    @SerializedName("code")
    public String code;

    @SerializedName("msg")
    public String msg;
}
