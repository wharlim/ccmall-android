package com.weiju.ccmall.newRetail.activity;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.ICommunityService;
import com.weiju.ccmall.module.community.models.CoursePlate;
import com.weiju.ccmall.module.shop.views.Utils;
import com.weiju.ccmall.newRetail.utils.StatusBarUtil;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.util.FrescoUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CommunityActivity extends AppCompatActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private CommunityItemAdapter mCommunityItemAdapter = new CommunityItemAdapter();
    private GridLayoutManager mGridLayoutManager;
    private ICommunityService service = ServiceManager.getInstance().createService(ICommunityService.class);
    public static List<CoursePlate> plates = new ArrayList<>();
//
//    ArrayList<Fragment> mFragments = new ArrayList<>();
//
//    @BindView(R.id.view_pager)
//    ViewPager view_pager;
//
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_community_layout);
//        ButterKnife.bind(this);
//
//        mFragments.add(TabViewPagerFragment.newInstance(TabViewPagerFragment.TabViewPageAdapterTag.COMMUNITY));
//        view_pager.setOffscreenPageLimit(0);
//        view_pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
//            @Override
//            public Fragment getItem(int i) {
//                return mFragments.get(i);
//            }
//
//            @Override
//            public int getCount() {
//                return mFragments.size();
//            }
//        });
//    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community_main);
        ButterKnife.bind(this);
        StatusBarUtil.setTransparent(this);
        initRecycler();
        getCoursePlates();
    }

    private void initRecycler() {
        mGridLayoutManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mGridLayoutManager);
        recyclerView.setAdapter(mCommunityItemAdapter);
        recyclerView.addItemDecoration(new CommunityItemDecoration(this, 2, Utils.dpToPx(132)));
    }

    private void getCoursePlates() {
        APIManager.startRequest(service.getPlates(), new BaseRequestListener<List<CoursePlate>>() {
            @Override
            public void onSuccess(List<CoursePlate> result) {
                if (!isDestroyed() && result != null) {
                    plates.clear();
                    plates.addAll(result);
                    mCommunityItemAdapter.notifyDataSetChanged();
                }
            }
        }, this);
    }


    @OnClick(R.id.headerLeftIv)
    public void onViewClicked() {
        finish();
    }

    private class CommunityItemAdapter extends RecyclerView.Adapter<CommunityItemHolder> {

        @NonNull
        @Override
        public CommunityItemHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            return CommunityItemHolder.newInstance(viewGroup);
        }

        @Override
        public void onBindViewHolder(@NonNull CommunityItemHolder viewHolder, int i) {
            viewHolder.itemView.setOnClickListener(v -> {
                CommunityTabActivity.start(CommunityActivity.this, i);
            });
            viewHolder.bindView(plates.get(i));
        }

        @Override
        public int getItemCount() {
            return plates.size();
        }
    }

    public static class CommunityItemHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.ivAvatar)
        SimpleDraweeView ivAvatar;
        @BindView(R.id.tvTitle)
        TextView tvTitle;

        public CommunityItemHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public static CommunityItemHolder newInstance(ViewGroup viewGroup) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_community_main, viewGroup, false);
            return new CommunityItemHolder(itemView);
        }

        public void bindView(CoursePlate coursePlate) {
            FrescoUtil.setImage(ivAvatar, coursePlate.iconUrl);
            tvTitle.setText(coursePlate.title);
        }
    }

    private static class CommunityItemDecoration extends RecyclerView.ItemDecoration {

        private int horizontalMargin;
        private int verticalMargin;
        private int spanCount = 2;
        private int itemWidth;

        CommunityItemDecoration(Context context, int spanCount, int itemWidth) {
            horizontalMargin = Utils.dpToPx(context, 40);
            verticalMargin = Utils.dpToPx(context, 25);
            this.spanCount = spanCount;
            this.itemWidth = itemWidth;
        }

        @Override
        public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
            int pos = ((RecyclerView.LayoutParams) view.getLayoutParams()).getViewLayoutPosition();
            int itemTotalWidth = parent.getMeasuredWidth() / spanCount;
            int itemActualWidth = itemWidth;
            int left = 0;
            int right = 0;
            int top = 0;
            int bottom = 0;
            if (pos % 2 == 0) {
                left = itemTotalWidth - itemActualWidth - horizontalMargin / 2;
            } else {
                left = horizontalMargin / 2;
            }
            if (pos > 1) {
                top = verticalMargin;
            }
            outRect.set(left, top, right, bottom);
        }
    }
}
