package com.weiju.ccmall.newRetail.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.tencent.imsdk.TIMConversation;
import com.tencent.imsdk.TIMManager;
import com.tencent.imsdk.TIMMessage;
import com.tencent.imsdk.TIMMessageListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.newRetail.fragment.NewRetailGiftFragment;
import com.weiju.ccmall.newRetail.fragment.NewRetailHomeFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HighGoActivity extends BaseActivity {
    List<Fragment> mFragments = new ArrayList<>();

    List<String> items = new ArrayList<>();
    @BindView(R.id.tv_message_number)
    TextView tvMessageNumber;

    private int selected = 0;

    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;

    @BindView(R.id.tv_quity)
    TextView tv_quity;

    @BindView(R.id.tv_geatured)
    TextView tv_geatured;

    @BindView(R.id.view_1)
    View view_1;

    @BindView(R.id.view_2)
    View view_2;

    @BindView(R.id.barPading)
    View mBarPading;

    NewRetailHomeFragment newRetailHomeFragment;

    AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_high_go);
        ButterKnife.bind(this);
        items.add("嗨购权益");
        items.add("嗨购精选");
        TIMManager.getInstance().addMessageListener(messageListener);
    }
    private void upldateTotalUnreadMessage() {
        long totalUnreadMessage = 0;
        List<TIMConversation> conversations = TIMManager.getInstance().getConversationList();
        if (conversations != null) {
            for (TIMConversation conversation : conversations) {
                totalUnreadMessage += conversation.getUnreadMessageNum();
            }
        }

        if (totalUnreadMessage > 0) {
            tvMessageNumber.setVisibility(View.VISIBLE);
            if (totalUnreadMessage > 99) {
                totalUnreadMessage = 99;
            }
            tvMessageNumber.setText(String.valueOf(totalUnreadMessage));
        } else {
            tvMessageNumber.setVisibility(View.INVISIBLE);
        }
    }

    TIMMessageListener messageListener = new TIMMessageListener() {//消息监听器
        @Override
        public boolean onNewMessages(List<TIMMessage> msgs) {//收到新消息
            upldateTotalUnreadMessage();
            //消息的内容解析请参考消息收发文档中的消息解析说明
            return false; //返回true将终止回调链，不再调用下一个新消息监听器
        }
    };

    @OnClick(R.id.back)
    protected void back() {
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this))//加上判断
            EventBus.getDefault().unregister(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        upldateTotalUnreadMessage();
        initViewPager();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        TIMManager.getInstance().removeMessageListener(messageListener);
        if (EventBus.getDefault().isRegistered(this))//加上判断
            EventBus.getDefault().unregister(this);
    }

    private void initViewPager() {
        if (mFragments.size() > 0) {
            return ;
        }
        newRetailHomeFragment = new NewRetailHomeFragment();

        mFragments.add(newRetailHomeFragment);
        mFragments.add(new NewRetailGiftFragment());

        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return items.get(position);
            }
        });

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    onPageChanged();
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
        } else {
            mViewPager.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                    onPageChanged();
                }
            });
        }
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setCurrentItem(0);
    }

    private void onPageChanged() {
        int currentItem = mViewPager.getCurrentItem();
        if (currentItem == 0) {
            if (selected == 0) {
                return;
            } else {
                tv_quity.setTextColor(getResources().getColor(R.color.red));
                view_1.setVisibility(View.VISIBLE);
                tv_geatured.setTextColor(getResources().getColor(R.color.black));
                view_2.setVisibility(View.INVISIBLE);
                selected = 0;
            }
        } else if (currentItem == 1) {
            if (selected == 1) {
                return;
            } else {
                tv_geatured.setTextColor(getResources().getColor(R.color.red));
                view_2.setVisibility(View.VISIBLE);
                tv_quity.setTextColor(getResources().getColor(R.color.black));
                view_1.setVisibility(View.INVISIBLE);
                selected = 1;
            }
        }
    }


    @OnClick({R.id.rl_haigou_quity, R.id.rl_featured, R.id.tv_message})
    public void allClick(View view) {
        switch (view.getId()) {
            case R.id.rl_haigou_quity:
                if (selected == 0) {
                    return;
                } else {
                    tv_quity.setTextColor(getResources().getColor(R.color.red));
                    view_1.setVisibility(View.VISIBLE);
                    tv_geatured.setTextColor(getResources().getColor(R.color.black));
                    view_2.setVisibility(View.INVISIBLE);
                    selected = 0;
                }
                mViewPager.setCurrentItem(0, false);
                break;
            case R.id.rl_featured:
                if (selected == 1) {
                    return;
                } else {
                    tv_geatured.setTextColor(getResources().getColor(R.color.red));
                    view_2.setVisibility(View.VISIBLE);
                    tv_quity.setTextColor(getResources().getColor(R.color.black));
                    view_1.setVisibility(View.INVISIBLE);
                    selected = 1;
                }
                mViewPager.setCurrentItem(1, false);
                break;
            case R.id.tv_message:
                startActivity(new Intent(this, ConversationListActivity.class));

                break;
            default:
                break;
        }
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBus(EventMessage message) {
        if (message.getEvent().equals(Event.isReadHigou)) {
            alertDialog.dismiss();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void stickEvent(EventMessage message) {
        if (message.getEvent().equals(Event.select2Fragment)) {
            try {
                EventBus.getDefault().removeStickyEvent(message);
                mViewPager.setCurrentItem(1);
            } catch (Exception e) {
            }
        }
    }

}
