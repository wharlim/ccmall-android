package com.weiju.ccmall.newRetail.bean;

import com.google.gson.annotations.SerializedName;

public class ShoppingGuidelinesBean {

     /*"contentId": "38f1b4cbb4794b168bd0051769937d65",
             "content": "hahahaahhahaha",
             "contentType": 0*/

     @SerializedName("contentId")
     public String contentId;

     @SerializedName("content")
     public String content;

     @SerializedName("contentType")
     public int contentType;
}
