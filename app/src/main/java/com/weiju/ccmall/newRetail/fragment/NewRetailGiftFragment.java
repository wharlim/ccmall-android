package com.weiju.ccmall.newRetail.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.category.CategoryFragment;
import com.weiju.ccmall.module.search.SearchActivity;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.shared.basic.AgentFActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.page.CustomPageFragment;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewRetailGiftFragment extends BaseFragment{

    private FragmentManager fm;
    private CustomPageFragment customPageFragment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_newtail_gift, container, false);
        ButterKnife.bind(this, rootView);
        fm = getChildFragmentManager();
        showChildFragment();
        return rootView;
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
        customPageFragment = (CustomPageFragment) childFragment;
    }

    private void showChildFragment() {
        FragmentTransaction transaction = fm.beginTransaction();
        if (customPageFragment == null) {
            CustomPageFragment newFrag = CustomPageFragment.newInstance(Key.HAPPY_BUY_HOME);
            newFrag.setUserVisibleHint(true);
            transaction.add(R.id.happyBuyHomeContainer, newFrag);
        } else {
            transaction.show(customPageFragment);
        }
        transaction.commit();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showChildFragment();
        }
    }

    @OnClick({R.id.searchLayout, R.id.iv_classification, R.id.iv_share})
    public void allViewClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.searchLayout://搜索
                intent = new Intent(getContext(), SearchActivity.class);
                getContext().getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, Const.ORDER_TYPE_FREE_ORDER).commit();
                intent.putExtra(Const.CLASSIFICATION_TYPE, Const.NEWRETAIL_MODE);
                startActivity(intent);
                break;
            case R.id.iv_classification://商品分类
                getContext().getSharedPreferences(Const.ORDER_TYPE, 0).edit().putString(Const.ORDER_TYPE, Const.ORDER_TYPE_FREE_ORDER).commit();
                getContext().getSharedPreferences(Const.CLASSIFICATION_TYPE, 0).edit().putString(Const.CLASSIFICATION_TYPE, Const.NEWRETAIL_MODE).commit();
                AgentFActivity.Companion.start(this, CategoryFragment.class, null);
                break;
            default:
                break;
        }
    }
}
