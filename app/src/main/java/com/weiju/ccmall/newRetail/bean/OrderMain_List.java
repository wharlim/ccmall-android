package com.weiju.ccmall.newRetail.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderMain_List<Order, R> implements Serializable {

    @SerializedName("pageOffset")
    public int page;
    @SerializedName("pageSize")
    public int pageSize;
    @SerializedName("totalRecord")
    public int total;
    @SerializedName("totalPage")
    public int totalPage;
    @SerializedName("datas")
    public ArrayList<Order> list;
    @SerializedName("ex")
    public R ex;
}
