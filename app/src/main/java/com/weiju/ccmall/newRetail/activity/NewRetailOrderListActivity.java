package com.weiju.ccmall.newRetail.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.order.OrderListFragment;
import com.weiju.ccmall.newRetail.utils.TimerControl;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ShareUtils;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NewRetailOrderListActivity extends BaseActivity {

    @BindView(R.id.magicIndicator)
    protected MagicIndicator mMagicIndicator;
    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;

    protected List<BaseFragment> mFragments = new ArrayList<>();
    protected List<Page> mPages = new ArrayList<>();

    public static final String ORDER_TYPE = "ORDER_TYPE";
    public static final String ORDER_SELECTED = "ORDER_SELECTED";


    //店铺订单
    public static final int ORDER_TYPE_STORE = 1;
    //我的销售--订单
    public static final int ORDER_TYPE_MY_SELL = 2;
    //我的--订单
    public static final int ORDER_TYPE_MY_ORDER = 3;

    int orderType = -1;

    int selected = -1;

    IOrderService orderService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_retail_orderlist_layout);
        ButterKnife.bind(this);
        setLeftBlack();


        initType();
        initViewPager();
        initIndicator();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mFragments.clear();
        mPages.clear();
        initType();
    }

    private void initType() {
        orderType = getIntent().getIntExtra(ORDER_TYPE, -1);
        selected = getIntent().getIntExtra(ORDER_SELECTED, -1);
        if (ORDER_TYPE_STORE == orderType) {
            setTitle("店铺订单");
            mHeaderLayout.setRightDrawable(R.mipmap.icon_search);
            mHeaderLayout.setOnRightClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {//去搜索订单页
                    Intent intent = new Intent(NewRetailOrderListActivity.this, StoreOrderSearchActivity.class);
                    int currentItem = mViewPager.getCurrentItem();
                    int orderStatus = 0;
                    if (currentItem == 0) {
                        orderStatus = -1;
                    } else if (currentItem == 1) {
                        orderStatus = 1;
                    } else if (currentItem == 2) {
                        orderStatus = 2;
                    } else if (currentItem == 3) {
                        orderStatus = 3;
                    }
                    intent.putExtra("orderStatus", orderStatus);
                    startActivity(intent);
                }
            });
            mPages.add(new Page(AppTypes.ORDER.STORE_ORDER_ALL, "全部", 2));
            mPages.add(new Page(AppTypes.ORDER.STORE_ORDER_TO_BE_PROCESSED, "待处理", 2));
            mPages.add(new Page(AppTypes.ORDER.STORE_ORDER_TO_BE_DELIVERED, "待发货", 2));
            mPages.add(new Page(AppTypes.ORDER.STORE_ORDER_SHPIPPED, "待收货", 2));
            for (Page page : mPages) {
                mFragments.add(OrderListFragment.newInstance(page));
            }
        } else if (ORDER_TYPE_MY_SELL == orderType) {
            setTitle("我的销售订单");
            getHeaderLayout().setRightText("去分享商品");
            getHeaderLayout().setOnRightClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventBus.getDefault().postSticky(new EventMessage(Event.select2Fragment));
                    finish();
//                    User loginUser = SessionUtil.getInstance().getLoginUser();
//                    ShareUtils.showShareDialog(NewRetailOrderListActivity.this, "嗨购礼包", "", "", BuildConfig.WECHAT_URL + "pl/" + loginUser.invitationCode);
                }
            });
            mPages.add(new Page(AppTypes.ORDER.MY_SELL_ORDER_ALL, "全部", 3));
            mPages.add(new Page(AppTypes.ORDER.MY_SELL_ORDER_PENDING_PAYMENT, "未付款", 3));
            mPages.add(new Page(AppTypes.ORDER.MY_SELL_ORDER_FROZEN, "未解冻", 3));
            mPages.add(new Page(AppTypes.ORDER.MY_SELL_ORDER_NOT_FROZEN, "已解冻", 3));

            for (Page page : mPages) {
                mFragments.add(OrderListFragment.newInstance(page));
            }
        } else if (orderType == ORDER_TYPE_MY_ORDER) {
            setTitle("我的订单");
            mPages.add(new Page(AppTypes.ORDER.MY_ORDER_ALL, "全部", 4));
            mPages.add(new Page(AppTypes.ORDER.MY_ORDER_PENDING_PAYMENT, "待付款", 4));
//            mPages.add(new Page(AppTypes.ORDER.MY_ORDER_TO_BE_CONFIRMED, "待确认", 4));
            mPages.add(new Page(AppTypes.ORDER.MY_ORDER_TO_BE_DELIVERED, "待发货", 4));
            mPages.add(new Page(AppTypes.ORDER.MY_ORDER_PEDNDING_RECEIPT, "待收货", 4));

            for (Page page : mPages) {
                mFragments.add(OrderListFragment.newInstance(page));
            }
        }

    }


    private void initIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mPages.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(mPages.get(index).name);
                titleView.setNormalColor(getResources().getColor(R.color.text_black));
                titleView.setSelectedColor(getResources().getColor(R.color.red));
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(16);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getResources().getColor(R.color.red));
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                return indicator;
            }
        });
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);

        if (-1 != selected) {
            mViewPager.setCurrentItem(selected, false);
        }
    }


    private void initViewPager() {
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mPages.get(position).name;
            }
        });
        mViewPager.setOffscreenPageLimit(mFragments.size());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        TimerControl.getInstance().closeAll();
    }
}
