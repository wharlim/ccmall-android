package com.weiju.ccmall.newRetail.activity;


import android.os.Bundle;
import android.support.annotation.Nullable;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;

import net.lucode.hackware.magicindicator.MagicIndicator;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NewRetailAfterSaleNRefundsActivity extends BaseActivity {

    @BindView(R.id.magicIndicator)
    MagicIndicator magicIndicator;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newretail_after_sale);
        ButterKnife.bind(this);
    }
}
