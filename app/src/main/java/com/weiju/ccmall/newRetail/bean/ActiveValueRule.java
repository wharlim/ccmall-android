package com.weiju.ccmall.newRetail.bean;

import com.google.gson.annotations.SerializedName;

public class ActiveValueRule {

    /**
     * contentId : 38f1b4cbb4794b168bd0051769937d6
     * content : html
     * contentType : 0
     * type : 2
     */

    @SerializedName("contentId")
    public String contentId;
    @SerializedName("content")
    public String content;
    @SerializedName("contentType")
    public int contentType;
    @SerializedName("type")
    public int type;
}
