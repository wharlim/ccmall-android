package com.weiju.ccmall.newRetail.bean;

import com.google.gson.annotations.SerializedName;

public class ActiveValue {

    /**
     * incId : 1
     * memberId : af8d785b384ad9281286275095141f15
     * activeValue : 50
     * updateDate : 2019-09-19 09:46:20
     * createDate : 2019-09-19 09:46:20
     */

    @SerializedName("incId")
    public int incId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("activeValue")
    public int activeValue;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("formatted")
    public String formatted;
}
