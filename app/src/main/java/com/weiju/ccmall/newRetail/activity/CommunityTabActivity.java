package com.weiju.ccmall.newRetail.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.TabViewPagerFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommunityTabActivity extends AppCompatActivity {

    ArrayList<Fragment> mFragments = new ArrayList<>();

    @BindView(R.id.view_pager)
    ViewPager view_pager;

    private int selectTab;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community_layout);
        ButterKnife.bind(this);
        selectTab = getIntent().getIntExtra("selectTab", 0);

        mFragments.add(TabViewPagerFragment.newInstance(TabViewPagerFragment.TabViewPageAdapterTag.COMMUNITY, selectTab));
        view_pager.setOffscreenPageLimit(0);
        view_pager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                return mFragments.get(i);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }
        });
    }

    public static void start(Context context, int selectTab) {
        Intent intent = new Intent(context, CommunityTabActivity.class);
        intent.putExtra("selectTab", selectTab);
        context.startActivity(intent);
    }
}
