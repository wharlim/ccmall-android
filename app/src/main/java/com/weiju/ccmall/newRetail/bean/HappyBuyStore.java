package com.weiju.ccmall.newRetail.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.bean.SkuInfo;

import java.util.List;

/**
 * hi购权益，我的店铺
 */
public class HappyBuyStore {

    /**
     * pushId : 10124
     * storeImage :
     * proList : [{"skuId":"","productId":"002d47732d904279a846651a9e086fd2","skuName":"","intro":"","weight":0,"stock":0,"retailPrice":0,"marketPrice":0,"shippingPrice":0,"totalSaleCount":0,"saleCount":0,"hasPresent":0,"quantity":0,"status":0,"discountStatus":1,"buyScore":0,"ticket":0,"coin":0,"distributionProfit":0,"goldenTicket":0,"serviceFee":0,"transactionFee":0,"storeName":"礼包店铺6","thumbUrl":"https://static.create-chain.net/ccmall/50/b3/7c/95b24b2a2cc848bf84eca6bdc69bc11f.png","storeImage":""}]
     * storeName : 礼包店铺6
     * orderCode :
     * lessTime : 1198991
     * totalCount : 1
     */

    @SerializedName("pushId")
    public int pushId;
    @SerializedName("storeImage")
    public String storeImage;
    @SerializedName("storeName")
    public String storeName;
    @SerializedName("orderCode")
    public String orderCode;
    @SerializedName("lessTime")
    public long lessTime;
    @SerializedName("totalCount")
    public int totalCount;
    @SerializedName("proList")
    public List<SkuInfo> proList;
}

