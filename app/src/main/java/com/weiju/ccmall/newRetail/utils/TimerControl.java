package com.weiju.ccmall.newRetail.utils;

import android.app.Activity;
import android.os.CountDownTimer;

import com.weiju.ccmall.shared.util.ActivityControl;

import java.util.ArrayList;
import java.util.List;

public class TimerControl {


    private static TimerControl sInstance = null;
    private final List<CountDownTimer> mList = new ArrayList<>();

    public static TimerControl getInstance() {
        if (sInstance == null) {
            synchronized (ActivityControl.class) {
                if (sInstance == null) {
                    sInstance = new TimerControl();
                }
            }
        }
        return sInstance;
    }

    public void add(CountDownTimer timer) {
        mList.add(timer);
    }


    public void close(Activity activity) {
        if (mList.contains(activity)) {
            mList.remove(activity);
            activity.finish();
        }
    }


    public void closeAll() {
        try {
            for (int i = 0; i < mList.size(); i++) {
                CountDownTimer timer = mList.get(i);
                if (timer != null) {
                    mList.remove(timer);
                    timer.cancel();
                    i--; //因为位置发生改变，所以必须修改i的位置,否则会有ConcurrentModificationException
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
