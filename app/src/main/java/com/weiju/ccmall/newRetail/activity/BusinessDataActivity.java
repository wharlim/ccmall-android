package com.weiju.ccmall.newRetail.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.newRetail.bean.ActiveValue;
import com.weiju.ccmall.newRetail.bean.ActiveValueRule;
import com.weiju.ccmall.newRetail.bean.OrderRates;
import com.weiju.ccmall.newRetail.widgets.CircleProgressBar;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.INotesService;
import com.weiju.ccmall.shared.service.contract.IProductService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusinessDataActivity extends BaseActivity {

    INotesService iNotesService;
    IProductService iProductService;

    @BindView(R.id.circleProgressBar)
    CircleProgressBar circleProgressBar;
    @BindView(R.id.businessTip)
    TextView businessTip;
    @BindView(R.id.activeValueRule)
    TextView activeValueRule;
    @BindView(R.id.activeValue)
    TextView activeValue;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business_data);
        ButterKnife.bind(this);
        iNotesService = ServiceManager.getInstance().createService(INotesService.class);
        iProductService = ServiceManager.getInstance().createService(IProductService.class);
        setUpTitleBar();
//        circleProgressBar.setProgress(80, true);
        String businessTipHtml = "<span style=\"color:#333333\">经营建议：</span><span style=\"color:#888888\">匹配单转化率表示系统匹配到您的店铺订单中，用户最终选择下单的比例。若转化率过低，一定程度反映您的选品与市场喜好不匹配，建议店主优化产品介绍或重新选品。</span>";
        businessTip.setText(Html.fromHtml(businessTipHtml));
        loadData();
    }

    private void loadData() {
        BaseRequestListener<ActiveValueRule> listener = new BaseRequestListener<ActiveValueRule>() {
            @Override
            public void onSuccess(ActiveValueRule result) {
                super.onSuccess(result);
                if (!isDestroyed()) {
                    activeValueRule.setText(Html.fromHtml(result.content));
                }
            }
        };
        APIManager.startRequest(iNotesService.getActiveValueRule(), listener, this);
        BaseRequestListener<ActiveValue> listener1 = new BaseRequestListener<ActiveValue>() {
            @Override
            public void onSuccess(ActiveValue result) {
                super.onSuccess(result);
                if (!isDestroyed()) {
                    activeValue.setText(result.formatted);
                }
            }
        };
        APIManager.startRequest(iNotesService.activevalue(), listener1, this);
        BaseRequestListener<OrderRates> listener2 = new BaseRequestListener<OrderRates>() {
            @Override
            public void onSuccess(OrderRates result) {
                super.onSuccess(result);
                circleProgressBar.setProgress(result.rate, true);
            }
        };
        APIManager.startRequest(iProductService.getOrderRates(), listener2, this);
    }

    public void setUpTitleBar() {
        mHeaderLayout.setTitle("经营数据");
        mHeaderLayout.setLeftDrawable(R.mipmap.icon_back_black);
        mHeaderLayout.setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, BusinessDataActivity.class);
        context.startActivity(intent);
    }
}
