package com.weiju.ccmall.newRetail.activity;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;

import com.orhanobut.logger.Logger;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.LoginActivity;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.fragment.NewRetailGiftFragment;
import com.weiju.ccmall.newRetail.fragment.NewRetailHomeFragment;
import com.weiju.ccmall.newRetail.fragment.NewRetailMeFragment;
import com.weiju.ccmall.newRetail.utils.TimerControl;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.NoScrollViewPager;
import com.weiju.ccmall.shared.constant.Event;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NewRetailActivity extends BaseActivity {


    protected ArrayList<Fragment> mFragments = new ArrayList<>();


    @BindView(R.id.viewPager)
    protected NoScrollViewPager mViewPager;

    @BindViews({R.id.tabHomeLayout, R.id.tabNewRetailLayout, R.id.tabMeLayout})
    protected List<View> mTabs;

    AlertDialog alertDialog;

    NewRetailHomeFragment newRetailHomeFragment = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_retail_layout);
        ButterKnife.bind(this);

        newRetailHomeFragment = new NewRetailHomeFragment();

        mFragments.add(newRetailHomeFragment);
        mFragments.add(new NewRetailGiftFragment());
        mFragments.add(new NewRetailMeFragment());

        mTabs.get(0).setSelected(true);

        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int i) {
                return mFragments.get(i);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }
        });


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
//                onClickTabItems(mTabs.get(position));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Window window = NewRetailActivity.this.getWindow();
                    if (position == mFragments.size() - 1) {
//                        window.setStatusBarColor(Color.parseColor("#F41757"));
                        window.setStatusBarColor(Color.parseColor("#f51861"));
                        QMUIStatusBarHelper.setStatusBarDarkMode(NewRetailActivity.this);
                    } else {
                        window.setStatusBarColor(Color.WHITE);
                        QMUIStatusBarHelper.setStatusBarLightMode(NewRetailActivity.this);
                    }
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        boolean isRead = getSharedPreferences(Const.READ_HIGOU_NEED_TO_KNOW, 0).getBoolean(Const.READ_HIGOU_NEED_TO_KNOW, false);
        if (!isRead) {

            View view1 = LayoutInflater.from(this).inflate(R.layout.check_higou_need_to_know_dialog, null);
            RelativeLayout rl_to_go_read = view1.findViewById(R.id.rl_to_go_read);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            alertDialog = builder.create();
            alertDialog.setView(view1, 0, 0, 0, 0);
            alertDialog.setCancelable(false);
            alertDialog.show();
            alertDialog.setOnKeyListener(onKeyListener1);

            rl_to_go_read.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(NewRetailActivity.this, HiGouDescriptionActivity.class));
                }
            });


        }
    }

    /*private View.OnKeyListener onKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
            if (keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_DOWN) {
            }
            return false;
        }
    };*/

    private DialogInterface.OnKeyListener onKeyListener1 = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if (i == KeyEvent.KEYCODE_BACK && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                if (alertDialog.isShowing()) {
                    finish();
                }

            }
            return false;
        }

    };

    /*@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if (alertDialog.isShowing()) {
                    finish();
                }
                break;
        }
        return super.onKeyDown(keyCode, event);
    }*/

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {//加上判断
            EventBus.getDefault().register(this);
        }
    }

    @OnClick({R.id.tabHomeLayout, R.id.tabNewRetailLayout, R.id.tabMeLayout})
    public void onClickTabItems(View view) {
        int index = mTabs.indexOf(view);
        if (index == mViewPager.getCurrentItem()) {
            view.setSelected(true);
            return;
        } else {
            if (index == 2) {
                Fragment fragment = mFragments.get(2);
                fragment.onResume();
            }
            mViewPager.setCurrentItem(index, false);
        }
        for (View tab : mTabs) {
            tab.setSelected(tab == view);
        }
    }

    public void selectedFragmentMe() {
        mViewPager.setCurrentItem(2);
        View view = mTabs.get(2);
        view.setSelected(true);
        View view1 = mTabs.get(0);
        view1.setSelected(false);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        TimerControl.getInstance().closeAll();

        if (EventBus.getDefault().isRegistered(this))//加上判断
            EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void eventBus(EventMessage message) {
        if (message.getEvent().equals(Event.isReadHigou)) {
            alertDialog.dismiss();
        } else if (message.getEvent().equals(Event.goToLogin)) {//去登录
            Logger.e("跳登录");
            ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
            ActivityManager.RunningTaskInfo info = manager.getRunningTasks(1).get(0);

            String topclassName = info.topActivity.getClassName();
            try {
                boolean isExist = false;
                if (topclassName.indexOf("LoginActivity") != -1) {
                    isExist = true;
//                        }
                }
                if (!isExist)
                    startActivity(new Intent(this, LoginActivity.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (message.getEvent().equals(Event.loginSuccess)) {//登录成功，刷新界面
            if (newRetailHomeFragment != null) {
                newRetailHomeFragment.initDatas();
            }
        }
    }
}
