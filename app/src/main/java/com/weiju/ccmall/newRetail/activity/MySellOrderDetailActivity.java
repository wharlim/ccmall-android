package com.weiju.ccmall.newRetail.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.OrderProfit;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MySellOrderDetailActivity extends BaseActivity {

    @BindView(R.id.tv_upgrad)
    TextView tv_upgrad;


    String orderCode;

    IOrderService orderService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_sell_order_detail_layout);
        ButterKnife.bind(this);
        orderCode = getIntent().getStringExtra("orderCode");

        orderService = ServiceManager.getInstance().createService(IOrderService.class);

        APIManager.startRequest(orderService.getOrderProfit(orderCode), new BaseRequestListener<OrderProfit>() {
            @Override
            public void onSuccess(OrderProfit result) {
                super.onSuccess(result);
            }
        },this);

    }


    @OnClick({R.id.iv_question_mark})
    public void allClick(View view) {
        switch (view.getId()) {
            case R.id.iv_question_mark:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage("您需升级成C1用户，并且客户在未收货时提前「快速升级」或已「确认收货」，提成才能解冻");
                //点击对话框以外的区域是否让对话框消失
                builder.setCancelable(false);
                builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.show();

                break;
            default:
                break;
        }
    }


}
