package com.weiju.ccmall.newRetail.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.community.TitleView;
import com.weiju.ccmall.newRetail.fragment.CommentFragment;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.bean.Store;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IOrderService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoreScoreActivity extends BaseActivity {

    @BindView(R.id.magicIndicator)
    protected MagicIndicator mMagicIndicator;
    @BindView(R.id.viewPager)
    protected ViewPager mViewPager;

    List<Page> mPages = new ArrayList<>();

    public int selected = 0;

    @BindView(R.id.avatarIv)
    SimpleDraweeView avatarIv;

    @BindView(R.id.tv_nickname)
    TextView tv_nickname;

    @BindView(R.id.ll_caveat)
    LinearLayout ll_caveat;

    @BindView(R.id.tv_description_match)
    TextView tv_description_match;

    @BindView(R.id.tv_service_attitude)
    TextView tv_service_attitude;

    @BindView(R.id.tv_caveat)
    TextView tv_caveat;

    @BindView(R.id.tv_logistics_services)
    TextView tv_logistics_services;

    IOrderService orderService;

    @BindView(R.id.titleView)
    TitleView titleView;



    protected List<BaseFragment> mFragments = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_score);

        ButterKnife.bind(this);

        initData();

        initIndicator();

        initViewPager();

        titleView.setTitle("店铺评分");
    }

    private void initData() {
        mPages.add(new Page(1 + "", "好评", 0));
        mPages.add(new Page(2 + "", "中评", 0));
        mPages.add(new Page(3 + "", "差评", 0));
        for (Page page : mPages) {
            mFragments.add(CommentFragment.newInstance(page));
        }

        orderService = ServiceManager.getInstance().createService(IOrderService.class);
        APIManager.startRequest(orderService.getStoreGrade(), new BaseRequestListener<Store>() {
            @Override
            public void onSuccess(Store result) {
                tv_nickname.setText(result.storeName);
                tv_description_match.setText(result.scoreBean.descScore + "分");
                tv_service_attitude.setText(result.scoreBean.serveScore + "分");
                tv_logistics_services.setText(result.scoreBean.expressScore + "分");
                FrescoUtil.setImage(avatarIv, result.thumbUrl);
                if (result.negativeComment >= 6 && result.negativeComment < 10) {
                    ll_caveat.setVisibility(View.VISIBLE);
                } else if (result.negativeComment >= 10) {
                    ll_caveat.setVisibility(View.VISIBLE);
                    tv_caveat.setText("礼包差评数达到10条，产品已被暂停接收随机订单。请积极处理售后，与买家友好协商删除差评，即可恢复正常。");
                }
                if (result.negativeComment > 0) {
                    Page page = mPages.get(2);
                    page.name = "差评（" + result.negativeComment + ")";
                    initIndicator();
                }

            }
        }, this);
    }

    private void initIndicator() {
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setAdjustMode(true);
        commonNavigator.setAdapter(new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return mPages.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(mPages.get(index).name);
                titleView.setNormalColor(getResources().getColor(R.color.text_black));
                titleView.setSelectedColor(getResources().getColor(R.color.red));
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(16);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getResources().getColor(R.color.red));
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                return indicator;
            }
        });
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);

        if (-1 != selected) {
            mViewPager.setCurrentItem(selected, false);
        }
    }

    private void initViewPager() {
        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mFragments.get(position);
            }

            @Override
            public int getCount() {
                return mFragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return mPages.get(position).name;
            }
        });
        mViewPager.setOffscreenPageLimit(mFragments.size());
    }


}
