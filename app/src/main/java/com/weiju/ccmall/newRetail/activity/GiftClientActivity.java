package com.weiju.ccmall.newRetail.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.blankj.utilcode.utils.ConvertUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.qmuiteam.qmui.widget.popup.QMUIPopup;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.user.ChangeCustomerNoteNameActivity;
import com.weiju.ccmall.module.user.adapter.VipTypeMenuAdapter;
import com.weiju.ccmall.newRetail.adapter.GiftClientAdapter;
import com.weiju.ccmall.newRetail.bean.ClientBeanList;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.VipTypeInfo;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GiftClientActivity extends BaseActivity implements PageManager.RequestListener {


    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.noDataLayout)
    NoData noDataLayout;

    IUserService userService;

    private PageManager mPageManager;

    private ArrayList<ClientBeanList.ClientBean> mDatas = new ArrayList<>();

    private GiftClientAdapter mAdapter = new GiftClientAdapter(mDatas);
//    View view;

    private String vipType = "99";

    private QMUIPopup mListPopup;

    private VipTypeMenuAdapter mMenuAdapter = null;

    private List<VipTypeInfo> mMenuData = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gift_client_layout);
        ButterKnife.bind(this);

        mMenuData.add(new VipTypeInfo(99, "查看全部"));
        mMenuData.add(new VipTypeInfo(1, "C1会员"));
        mMenuData.add(new VipTypeInfo(2, "C2会员"));
        mMenuData.add(new VipTypeInfo(3, "C3会员"));
        mMenuData.add(new VipTypeInfo(4, "C4会员"));
        mMenuData.add(new VipTypeInfo(5, "C5会员"));
        mMenuData.add(new VipTypeInfo(6, "C6会员"));
        mMenuData.add(new VipTypeInfo(7, "C7会员"));
        mMenuData.add(new VipTypeInfo(8, "C8会员"));
        mMenuData.add(new VipTypeInfo(9, "C9会员"));
        mMenuAdapter = new VipTypeMenuAdapter(mMenuData);
        userService = ServiceManager.getInstance().createService(IUserService.class);
        setLeftBlack();
        setTitle("嗨购客户");
        getHeaderLayout().setRightDrawable(R.drawable.ic_screen);

        getHeaderLayout().setOnRightClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showMenuListDialog();

            }
        });

        mRecyclerView.setAdapter(mAdapter);
        try {
            mPageManager = PageManager.getInstance()
                    .setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false))
                    .setRecyclerView(mRecyclerView)
//                    .setItemDecoration(new SpacesItemDecoration(ConvertUtil.dip2px(15), true))
                    .setRequestListener(this)
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .build(this);
        } catch (PageManager.PageManagerException e) {
            ToastUtil.error("PageManager 初始化失败");
        }
        mPageManager.onRefresh();
    }



    @Override
    public void nextPage(int page) {

        if (1 == page) {
            mDatas.clear();
        }

        APIManager.startRequest(userService.getClientList(String.valueOf(page), "15", vipType), new BaseRequestListener<ClientBeanList>() {
            @Override
            public void onSuccess(ClientBeanList result) {
                if (1 == page && result.datas.size() == 0) {
                    noDataLayout.setVisibility(View.VISIBLE);
                    return;
                }
                mPageManager.setTotalPage(result.totalRecord);
                noDataLayout.setVisibility(View.GONE);
                mDatas.addAll(result.datas);
                mAdapter.notifyDataSetChanged();
                if (result.datas.size() < Constants.PAGE_SIZE) {
                    mAdapter.loadMoreEnd();
                } else {
                    mAdapter.loadMoreComplete();
                }
                mPageManager.setLoading(false);
                setTitle("嗨购客户(" + result.totalRecord + ")");
            }

            @Override
            public void onComplete() {
                super.onComplete();
                mPageManager.setLoading(false);
                mRefreshLayout.setRefreshing(false);
            }

            @Override
            protected void onE(Throwable e) {
                super.onE(e);
                mPageManager.setLoading(false);
                mRefreshLayout.setRefreshing(false);
            }
        }, this);
    }

    private void showMenuListDialog() {
        if (mListPopup == null) {
            RecyclerView rvMenu = new RecyclerView(this);
            rvMenu.setLayoutManager(new LinearLayoutManager(this));
            rvMenu.setAdapter(mMenuAdapter);
            mMenuAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    VipTypeInfo vipTypeInfo = mMenuData.get(position);
                    vipType = vipTypeInfo.vipType + "";
                    mPageManager.onRefresh();
                    mListPopup.dismiss();
//                    selectMenuItem(position);
                }
            });
            mListPopup = new QMUIPopup(this);
            mListPopup.setContentView(rvMenu);
            mListPopup.setPositionOffsetX(ConvertUtils.dp2px(-5));
            mListPopup.setAnimStyle(QMUIPopup.ANIM_GROW_FROM_RIGHT);
            mListPopup.setPreferredDirection(QMUIPopup.DIRECTION_NONE);
        }
        mListPopup.show(getHeaderLayout().getRightAnchor());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == ChangeCustomerNoteNameActivity.REQUEST_CODE && data != null) {
                String memberId = data.getStringExtra("memberId");
                String noteName = data.getStringExtra("noteName");
                for(int i = 0; i < mDatas.size(); ++i) {
                    if (Objects.equals(mDatas.get(i).memberId, memberId)) {
                        mDatas.get(i).noteName = noteName;
                        mAdapter.notifyItemChanged(i);
                    }
                }
            }
        }
    }
}
