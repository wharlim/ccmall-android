package com.weiju.ccmall.newRetail.widgets;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

/**
 * 圆环进度
 */
public class CircleProgressBar extends View implements ValueAnimator.AnimatorUpdateListener {

    private DisplayMetrics displayMetrics;
    private int progress; // 0-100
    private Paint lightPaint;
    private Paint textPaint;
    private ValueAnimator animator;

    public CircleProgressBar(Context context) {
        super(context);
        init(context);
    }

    public CircleProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CircleProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        Resources r = context.getResources();
        displayMetrics = r.getDisplayMetrics();
        lightPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        lightPaint.setColor(Color.WHITE);
        lightPaint.setStyle(Paint.Style.STROKE);
        lightPaint.setStrokeWidth(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 3, displayMetrics));
        lightPaint.setStrokeCap(Paint.Cap.ROUND);
        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 18, displayMetrics));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
//        Log.d("Seven", width + "," + widthMode + "," + height + "," + heightMode);
        if (widthMode != MeasureSpec.EXACTLY) {
            width = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, displayMetrics);
        }
        if (heightMode != MeasureSpec.EXACTLY) {
            height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, displayMetrics);
        }
        setMeasuredDimension(width, height);
//        Log.d("Seven", width + "," + widthMode + "," + height + "," + heightMode);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawText(canvas);
        drawProgress(canvas);
    }

    public void setProgress(int progress, boolean animate) {
        if (animator != null && animator.isRunning()) {
            animator.removeAllUpdateListeners();
            animator.cancel();
        }
        if (!animate) {
            this.progress = progress;
        } else {
            animator = ValueAnimator.ofInt(0, progress);
            animator.addUpdateListener(this);
//            animator.setInterpolator(new AccelerateDecelerateInterpolator());
            animator.setDuration(5000L);
            animator.start();
        }
        invalidate();
    }

    private void drawText(Canvas canvas) {
//        Log.d("Seven", "draw " + progress+"%");
        String text = progress+"%";
        Rect bounds = new Rect();
        textPaint.getTextBounds(text, 0 , text.length(), bounds);
        canvas.drawText(text, getMeasuredWidth()/2 - bounds.width()/2,
                getMeasuredHeight()/2 + bounds.height()/2, textPaint);
    }

    private void drawProgress(Canvas canvas) {
        float halfLineWidth = lightPaint.getStrokeWidth() / 2;
        RectF oval = new RectF(halfLineWidth, halfLineWidth, getMeasuredWidth() - lightPaint.getStrokeWidth(), getMeasuredHeight() - lightPaint.getStrokeWidth());
        lightPaint.setColor(Color.parseColor("#D9C199"));
        canvas.drawArc(oval, 0, 360, false, lightPaint);
        lightPaint.setColor(Color.WHITE);
        canvas.drawArc(oval, -90, 360*progress/100, false, lightPaint);
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        int value = (int)animation.getAnimatedValue();
        this.progress = value;
        invalidate();
    }
}
