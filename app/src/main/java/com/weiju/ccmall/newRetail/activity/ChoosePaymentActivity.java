package com.weiju.ccmall.newRetail.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseActivity;

import butterknife.ButterKnife;

public class ChoosePaymentActivity extends BaseActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.choose_payment_layout);
        ButterKnife.bind(this);
    }
}
