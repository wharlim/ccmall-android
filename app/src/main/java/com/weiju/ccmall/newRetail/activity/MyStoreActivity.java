package com.weiju.ccmall.newRetail.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.category.adapter.ProductListAdapter;
import com.weiju.ccmall.module.community.TitleView;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.bean.OrderRates;
import com.weiju.ccmall.shared.basic.BaseActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.component.NoData;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PageManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyStoreActivity extends BaseActivity implements PageManager.RequestListener {


    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.refreshLayout)
    protected SwipeRefreshLayout mRefreshLayout;

    @BindView(R.id.titleView)
    TitleView titleView;


    private ProductListAdapter mAdapter;
    private IProductService mProductService;
    private LinearLayoutManager mSingleColumnLayoutManager;
    private PageManager mPageManager;

    @BindView(R.id.noDataLayout)
    NoData noDataLayout;

    IProductService productService = ServiceManager.getInstance().createService(IProductService.class);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_store);
        ButterKnife.bind(this);
        titleView.setTitle("店铺产品");
        initData();
    }

    private void initData() {
        mAdapter = new ProductListAdapter(this, Const.ORDER_TYPE_FREE_ORDER);
        mProductService = ServiceManager.getInstance().createService(IProductService.class);
        mSingleColumnLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setAdapter(mAdapter);
        try {
            mPageManager = PageManager.getInstance()
                    .setSwipeRefreshLayout(mRefreshLayout)
                    .setRecyclerView(mRecyclerView)
                    .setLayoutManager(mSingleColumnLayoutManager)
                    .setRequestListener(this)
                    .build(this);
        } catch (PageManager.PageManagerException e) {
            e.printStackTrace();
        }


        mPageManager.onRefresh();
    }

    @Override
    public void nextPage(int page) {
        APIManager.startRequest(mProductService.getNewRetailSkuList(1, 20, "1"), new BaseRequestListener<PaginationEntity<SkuInfo, Object>>() {
            @Override
            public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                if (page == 1) {
                    mAdapter.getItems().clear();
                }

                if (1 == page && result.list.size() == 0) {
                    noDataLayout.setVisibility(View.VISIBLE);
                    return;
                }
                noDataLayout.setVisibility(View.GONE);

                mPageManager.setLoading(false);
                mPageManager.setTotalPage(result.totalPage);
                mAdapter.addItems(result.list);
            }

            @Override
            public void onComplete() {
                super.onComplete();
                mRefreshLayout.setRefreshing(false);
            }
        }, this);
    }
}
