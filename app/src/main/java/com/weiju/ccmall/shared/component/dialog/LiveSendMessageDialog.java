package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.sobot.chat.widget.kpswitch.util.KeyboardUtil;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.util.CommonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 留言的弹窗
 *
 *
 * <p>
 * Created by zjm on 2017/4/25 下午10:12.
 */
public class LiveSendMessageDialog extends Dialog {

    @BindView(R.id.etContent)
    EditText mEtContent;
    @BindView(R.id.tvSubmit)
    TextView mTvSubmit;
    private Context mContext;
    private OnSubmitListener mListener;

    public LiveSendMessageDialog(@NonNull Context context) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_send_message);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.dimAmount = 0.0f;
        getWindow().setAttributes(layoutParams);
    }

    @OnClick(R.id.tvSubmit)
    public void onViewClicked() {
        if (mListener != null) {
            mListener.submit(mEtContent.getText().toString());
        }
    }

    private void setButtonText(String text) {
        mTvSubmit.setText(text);
    }

    private void setContent(String content) {
        mEtContent.setText(content);
        mEtContent.setSelection(content.length());
        showSoftKeyboard(mEtContent, mContext);
    }

    public LiveSendMessageDialog setSubmitListener(OnSubmitListener listener) {
        mListener = listener;
        return this;
    }

    public interface OnSubmitListener {
        void submit(String content);
    }

    public void showSoftKeyboard(View view, Context mContext) {
        if (view.requestFocus()) {
            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    @Override
    public void show() {
        super.show();
        showSoftKeyboard(mEtContent, mContext);
    }

    public void show(String btnStr, String etStr) {
        show();
        setButtonText(btnStr);
        setContent(etStr);
    }
}
