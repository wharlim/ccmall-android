package com.weiju.ccmall.shared.manager;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.orhanobut.logger.Logger;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.module.jkp.IJkpProductService;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.factory.GsonFactory;
import com.weiju.ccmall.shared.util.BaseUrl;
import com.weiju.ccmall.shared.util.LollipopBitmapMemoryCacheParamsSupplier;
import com.weiju.ccmall.shared.util.SessionUtil;

import java.io.IOException;
import java.net.Proxy;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceManager {

    @SuppressLint("StaticFieldLeak")
    private static ServiceManager mInstance;
    public static Retrofit mRetrofit;
    private static Retrofit mRetrofit2;
    private Context mContext;
    private static HashMap<Class, Object> services = new HashMap<>();
    private static final String GLOBAL_HEADER_TOKEN = "X-Access-Token";
    private OkHttpClient okHttpClient;
    public com.weiju.ccmall.shared.BaseUrlManagerInterceptor baseUrlManagerInterceptor;

    private ServiceManager() {
//        Gson gson = new GsonBuilder()
//                .registerTypeHierarchyAdapter(RequestResult.class, new ResultJsonDeser())
//                .create();

        mContext = MyApplication.getInstance().getApplicationContext();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl.getInstance().getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(GsonFactory.make()))
//                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getHttpClient())
                .build();
    }

    public void reSetBaseUrl() {
        mContext = MyApplication.getInstance().getApplicationContext();
        mRetrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl.getInstance().getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(GsonFactory.make()))
//                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(getHttpClient())
                .build();
    }

    public static ServiceManager getInstance() {
        if (mInstance == null) {
            synchronized (ServiceManager.class) {
                if (mInstance == null) {
                    mInstance = new ServiceManager();
                }
            }
        }
        return mInstance;
    }

    private void initRetrofit2() {
        if (mRetrofit2 == null) {
            String host = BuildConfig.XYSH_HOST;
            if (BuildConfig.DEBUG) {
                host = BaseUrl.getInstance().getCCPayHostPost();
            }
            synchronized (ServiceManager.class) {
                if (mRetrofit2 == null)
                    mRetrofit2 = new Retrofit.Builder()
                            .baseUrl((BuildConfig.DEBUG ? "http" : "https") + "://" + host + "/")
//                            .baseUrl("https://credit.ccmallv2.create-chain.net/api/")
                            .addConverterFactory(GsonConverterFactory.create(GsonFactory.make()))
                            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                            .client(getHttpClient(false))
                            .build();
            }
        }
    }

    // 切换环境时调用
    public void reInitRetrofit2() {
        mRetrofit2 = null;
        services.clear();
        initRetrofit2();
    }

    private HttpLoggingInterceptor getLoggingInterceptor() {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                if (message.contains("\"code\":") || message.contains("<-- ") && !message.contains("<-- END HTTP")) {
                    Logger.e("OkHttp: " + message);
                }
            }
        });
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return loggingInterceptor;
    }

    private OkHttpClient getHttpClient() {
        return getHttpClient(true);
    }

    private OkHttpClient getHttpClient(boolean withVersion) {
        baseUrlManagerInterceptor = new com.weiju.ccmall.shared.BaseUrlManagerInterceptor();
        baseUrlManagerInterceptor.setOriginalBaseUrl(BaseUrl.getInstance().getBaseUrl());
        okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(baseUrlManagerInterceptor)
                .addInterceptor(getLoggingInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();
                        HttpUrl originalHttpUrl = originalRequest.url();
                        // 增加 API 版本号
                        Request.Builder builder = originalRequest.newBuilder();
                        builder.addHeader(GLOBAL_HEADER_TOKEN, SessionUtil.getInstance().getOAuthToken());
                        builder.addHeader("app-version", BuildConfig.VERSION_NAME);
                        builder.addHeader("app-platform", "android");
                        if (withVersion) {
                            HttpUrl httpUrl = originalHttpUrl.newBuilder()
                                    .addQueryParameter(Key.VERSION, Constants.API_VERSION)
                                    .build();
                            builder = builder.url(httpUrl);
                        }

                        // 重新构建请求
                        return chain.proceed(builder.build());
                    }
                })
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        Response response = chain.proceed(request);
                        try {
                            String url = request.url().toString();
                            //Log.d("Seven", "url -> " + url);
                            if (url.contains("/tkPage/") ||
                                url.contains("/tkPageConfig/")) {
                                String body = response.body().string()
                                        .replace("\"event\":\"native\"","\"event\":\""+ IJkpProductService.event +"\"");
                                ResponseBody newBody = ResponseBody.create(response.body().contentType(), body);
                                response = response.newBuilder().body(newBody).build();
                                Log.d("Seven", "body -> " + body);
                            }
                        } catch (Exception e) {

                        }
                        return response;
                    }
                })
                .cookieJar(new CookieJar() {
                    @Override
                    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
                        if (cookies.size() > 0) {
                            for (Cookie cookie : cookies) {
                                if (cookie.name().equalsIgnoreCase(Key.OAUTH)) {
                                    SessionUtil.getInstance().setOAuthToken(cookie.value());
                                    break;
                                }
                            }
                        }
                    }

                    @Override
                    public List<Cookie> loadForRequest(HttpUrl url) {
                        List<Cookie> cookies = new ArrayList<>();
                        String oAuthToken = SessionUtil.getInstance().getOAuthToken();
                        if (!oAuthToken.isEmpty()) {
                            Cookie cookie = new Cookie.Builder()
                                    .name(Key.OAUTH).value(oAuthToken)
                                    .path(Constants.COOKIE_PATH)
                                    .hostOnlyDomain(BuildConfig.COOKIE_DOMAIN)
                                    .build();
                            cookies.add(cookie);
                        }
                        return cookies;
                    }
                })
                .connectTimeout(Constants.REQUEST_TIMEOUT, TimeUnit.SECONDS).sslSocketFactory(initSSLContextAndVerifier().getSocketFactory()).hostnameVerifier(new HostnameVerifier() {
                    @Override
                    public boolean verify(String s, SSLSession sslSession) {
                        return true;
                    }
                }).readTimeout(20,TimeUnit.SECONDS)
                .proxy(BuildConfig.DEBUG ? null : Proxy.NO_PROXY)
                .build();
        return okHttpClient;

    }

    /**
     * 数字证书校验，方便用charles抓包
     *
     * @return
     */
    private SSLContext initSSLContextAndVerifier() {
        SSLContext context = null;
        try {
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }

                @Override
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }};

            context = SSLContext.getInstance("TLS");
            // trustAllCerts信任所有的证书
            context.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        } catch (Exception e) {

        }

        return context;
    }


    public <T> T createService(Class<T> clazz) {
        if (services.containsKey(clazz)) {
            return (T) services.get(clazz);
        }
        T t = mRetrofit.create(clazz);
        services.put(clazz, t);
        return t;
    }

    public <T> T createService2(Class<T> clazz) {
        if (services.containsKey(clazz)) {
            return (T) services.get(clazz);
        }
        if (mRetrofit2 == null)
            initRetrofit2();
        T t = mRetrofit2.create(clazz);
        services.put(clazz, t);
        return t;
    }

    public void initFresco() {
        ImagePipelineConfig config = OkHttpImagePipelineConfigFactory
                .newBuilder(mContext, getHttpClient())
                .setDownsampleEnabled(true)
                .setBitmapMemoryCacheParamsSupplier(new LollipopBitmapMemoryCacheParamsSupplier((ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE)))
                .setBitmapsConfig(Bitmap.Config.RGB_565)
                .build();
        Fresco.initialize(mContext, config);
    }


}
