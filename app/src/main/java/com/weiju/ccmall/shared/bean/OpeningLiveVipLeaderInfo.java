package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class OpeningLiveVipLeaderInfo {

    /**
     * memberId : d5175e47c2404d2099ebc4480af7758b
     * vipNumTotal : 2
     * surplusVipNum : 2
     * supervisorNumTotal : 2
     * surplusSupervisorNum : 2
     * createDate : 2020-03-30 20:52:05
     * updateDate : 2020-03-30 20:52:16
     * deleteFlag : 0
     */

    @SerializedName("memberId")
    public String memberId;
    @SerializedName("vipNumTotal")
    public int vipNumTotal;
    @SerializedName("surplusVipNum")
    public int surplusVipNum;
    @SerializedName("supervisorNumTotal")
    public int supervisorNumTotal;
    @SerializedName("surplusSupervisorNum")
    public int surplusSupervisorNum;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
}
