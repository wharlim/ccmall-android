package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.ShareObject;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.ShareUtils;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.component
 * @since 2017-07-05
 */
public class ShareDialog extends Dialog {

    @BindView(R.id.tvShareText)
    TextView tvShareText;
    @BindView(R.id.llShareTextContainer)
    LinearLayout llShareTextContainer;
    private ShareObject mShareObject;
    private Activity mActivity;
    private UMWeb mWeb;
    private UMShareListener mUmShareListener;
    private File mFile;
    private String mImageurl;
    private String mTitle;
    private String mPureContent;

    private boolean onlyShareText;
    private boolean isPureContent;

    public ShareDialog(Activity activity, String title, String desc, String logoUrl, String linke, UMShareListener umShareListener) {
        this(activity);
        mActivity = activity;
        mWeb = new UMWeb(linke);
        mUmShareListener = umShareListener;
        mWeb.setTitle(title);
        mWeb.setDescription(desc);
        mWeb.setThumb(new UMImage(activity, logoUrl));
    }

    public ShareDialog(Activity activity, File imgFile, UMShareListener umShareListener) {
        this(activity);
        mActivity = activity;
        mFile = imgFile;
        mUmShareListener = umShareListener;
    }

    public ShareDialog(Activity activity, String title, String imageurl, UMShareListener umShareListener) {
        this(activity);
        mActivity = activity;
        mTitle = title;
        mImageurl = imageurl;
        mUmShareListener = umShareListener;
    }

    public ShareDialog(Activity activity, String title, UMShareListener umShareListener) {
        this(activity);
        mActivity = activity;
        mTitle = title;
        mUmShareListener = umShareListener;
        onlyShareText = true;
    }

    public ShareDialog(Activity activity, String content, boolean isPureContent, UMShareListener umShareListener) {
        this(activity);
        mActivity = activity;
        mPureContent = content;
        mUmShareListener = umShareListener;
        this.isPureContent = isPureContent;
    }

    private ShareDialog(Context context) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_share);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        setCanceledOnTouchOutside(true);

        tvShareText.setText(mTitle);
        llShareTextContainer.setVisibility(TextUtils.isEmpty(mTitle) ? View.GONE:View.VISIBLE);
    }

    @OnClick(R.id.shareCircleBtn)
    protected void shareToCircle() {
        if (!isWeixinAvilible()){
            ToastUtil.error("检查到您手机没有安装微信，请安装后使用该功能");
            return;
        }
        if (onlyShareText) {
            new ShareAction(mActivity)
                    .setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE)
                    .withText(mTitle)
                    .setCallback(mUmShareListener)
                    .share();
        } else if (mFile != null) {
            ShareUtils.shareImg(mActivity, mFile, SHARE_MEDIA.WEIXIN_CIRCLE, mUmShareListener);
        } else if (!TextUtils.isEmpty(mImageurl)) {
            UMImage image = new UMImage(mActivity, mImageurl);//网络图片
            new ShareAction(mActivity)
                    .setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE)
                    .withText(mTitle)
                    .withMedia(image)
                    .setCallback(mUmShareListener)
                    .share();
        } else if (isPureContent) {
            new ShareAction(mActivity)
                    .setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE)
                    .withText(mPureContent)
                    .setCallback(mUmShareListener)
                    .share();
        } else {
            new ShareAction(mActivity)
                    .setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE)
                    .withMedia(mWeb)
                    .setCallback(mUmShareListener)
                    .share();
        }
        dismiss();
    }

    @OnClick(R.id.shareFriendBtn)
    protected void shareToFriend() {
        if (!isWeixinAvilible()){
            ToastUtil.error("检查到您手机没有安装微信，请安装后使用该功能");
            return;
        }
        if (onlyShareText) {
            new ShareAction(mActivity)
                    .setPlatform(SHARE_MEDIA.WEIXIN)
                    .withText(mTitle)
                    .setCallback(mUmShareListener)
                    .share();
        } else if (mFile != null) {
            ShareUtils.shareImg(mActivity, mFile, SHARE_MEDIA.WEIXIN, mUmShareListener);
        } else if (!TextUtils.isEmpty(mImageurl)) {
            UMImage image = new UMImage(mActivity, mImageurl);//网络图片
            new ShareAction(mActivity)
                    .setPlatform(SHARE_MEDIA.WEIXIN)
                    .withText(mTitle)
                    .withMedia(image)
                    .setCallback(mUmShareListener)
                    .share();
        } else if (isPureContent) {
            new ShareAction(mActivity)
                    .setPlatform(SHARE_MEDIA.WEIXIN)
                    .withText(mPureContent)
                    .setCallback(mUmShareListener)
                    .share();
        } else {
            new ShareAction(mActivity)
                    .setPlatform(SHARE_MEDIA.WEIXIN)
                    .withMedia(mWeb)
                    .setCallback(mUmShareListener)
                    .share();
        }
        dismiss();
    }

    //判断是否安装了微信
    public boolean isWeixinAvilible() {
        final PackageManager packageManager = mActivity.getPackageManager();// 获取packagemanager
        List<PackageInfo> pinfo = packageManager.getInstalledPackages(0);// 获取所有已安装程序的包信息
        if (pinfo != null) {
            for (int i = 0; i < pinfo.size(); i++) {
                String pn = pinfo.get(i).packageName;
                if (pn.equals("com.tencent.mm")) {
                    return true;
                }
            }
        }
        return false;
    }

    @OnClick(R.id.cancelBtn)
    protected void onClose() {
        dismiss();
    }

}
