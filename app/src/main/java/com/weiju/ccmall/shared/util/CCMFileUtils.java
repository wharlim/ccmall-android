package com.weiju.ccmall.shared.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import com.blankj.utilcode.utils.AppUtils;
import com.blankj.utilcode.utils.FileUtils;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.module.product.ProductQrcodeShowActivity;

import java.io.File;

public class CCMFileUtils {

    public static String getSharedFileDir() {
        String dirPath;
        try {
            dirPath = MyApplication.getContext().getExternalCacheDir().getAbsolutePath() + "/shared";
        } catch (Exception e) {
            dirPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM).getPath()
                    + "/" + AppUtils.getAppName(MyApplication.getContext());
        }
        FileUtils.createOrExistsDir(dirPath);
        return dirPath;
    }

    public static File getSharedSaveFile() {
        String dirPath = getSharedFileDir();
        File file = new File(dirPath, System.currentTimeMillis() + ".jpg");
        FileUtils.createOrExistsDir(dirPath);
        return file;
    }

    public static File saveSharedPicture(Bitmap bitmap) {

        final File savedImageFile = getSharedSaveFile();
        ProductQrcodeShowActivity.saveBitmapFile(bitmap, savedImageFile, false);

        return savedImageFile;
    }
}
