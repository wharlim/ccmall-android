package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.FrameLayout;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.MainActivity;
import com.weiju.ccmall.module.auth.event.MsgStatus;

import org.greenrobot.eventbus.EventBus;

public class CartNoDataView extends FrameLayout {
    public CartNoDataView(@NonNull Context context) {
        super(context);
        inflate(getContext(), R.layout.layout_no_data_cart, this);
        findViewById(R.id.tvGoMain).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EventBus.getDefault().post(new MsgStatus(MsgStatus.ACTION_EDIT_PHONE));
                Intent mainActivity = new Intent(context, MainActivity.class);
                mainActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(mainActivity);
            }
        });
    }
}
