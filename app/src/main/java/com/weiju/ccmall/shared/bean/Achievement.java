package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/11/15 on 17:30
 * @desc ${TODD}
 */
public class Achievement {
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("money")
    public long money;
}
