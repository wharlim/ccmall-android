package com.weiju.ccmall.shared.component.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.LiveRedEnvelopes;
import com.weiju.ccmall.shared.component.dialog.LiveRedListDialog;

/**
 * @author chenyanming
 * @time 2020/1/7 on 16:58
 * @desc
 */
public class LiveRedListAdapter extends BaseQuickAdapter<LiveRedEnvelopes, BaseViewHolder> {

    private int mType;

    public void setType(int type) {
        mType = type;
    }

    public LiveRedListAdapter() {
        super(R.layout.item_live_red);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveRedEnvelopes item) {
        helper.setVisible(R.id.ivOpen, item.receiveStatus == 0 && item.status == 0 && mType == LiveRedListDialog.PLAY);
        helper.setVisible(R.id.ivCheck, !(item.receiveStatus == 0 && item.status == 0 && mType == LiveRedListDialog.PLAY));
        helper.setText(R.id.tvRedTitle, item.title);
        helper.setText(R.id.tvDate, item.createDate);

    }
}
