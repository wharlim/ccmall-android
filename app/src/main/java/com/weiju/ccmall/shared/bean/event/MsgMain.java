package com.weiju.ccmall.shared.bean.event;

import com.weiju.ccmall.module.auth.event.BaseMsg;

public class MsgMain extends BaseMsg {

    public final static int SELECT_HOME = 1 << 0;

    public MsgMain(int action) {
        super(action);
    }
}
