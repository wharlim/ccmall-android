package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.Message;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service.contract
 * @since 2017-07-05
 */
public interface IMessageService {

    @GET("message/list")
    Observable<RequestResult<PaginationEntity<Message, Object>>> getMessageList(@Query("pageOffset") int page);
}
