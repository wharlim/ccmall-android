package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.BindPhoneMsgModel;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service.contract
 * @since 2017-03-10
 */
public interface ICaptchaService {

    @GET("captcha/getRegisterMsg")
    Observable<RequestResult<Object>> getCaptchaForRegister(@Query("token") String token, @Query("phone") String phone);

    // 需要登录
    @GET("captcha/getMemberAuthMsg")
    Observable<RequestResult<Object>> getCaptchaForCheck(@Query("token") String token, @Query("phone") String phone);

    @GET("captcha/getMemberInfoChangeMsg")
    Observable<RequestResult<Object>> getCaptchaForUpdate(@Query("token") String token, @Query("phone") String phone);

    @GET("captcha/getCheckNumberPayPwd")
    Observable<RequestResult<Object>> getCheckNumberPayPwd(@Query("token") String token, @Query("phone") String phone);

    @GET("captcha/getUpdatePhoneMsg?")
    Observable<RequestResult<Object>> getCaptchaForUpdatePhone(@Query("token") String token, @Query("phone") String phone);

    @GET("captcha/getMemberAuthMsgByReservedPhone")
    Observable<RequestResult<Object>> getMemberAuthMsgByReservedPhone(
            @Query("phone") String phone
    );

    @GET("user/sendLoginPhoneCode")
    Observable<RequestResult<Object>> getCaptchaForRegister(
            @Query("phone") String phone
    );

    @GET("captcha/getFirstPhoneMsg")
    Observable<RequestResult<BindPhoneMsgModel>> getFirstPhoneMsg(@Query("token") String token, @Query("phone") String phone);
}
