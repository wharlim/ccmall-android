package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/8/18.
 */
public class GetOrderStatusCount {

    /**
     * waitShip : 0
     * hasShip : 0
     * hasComplete : 0
     * hasClose : 0
     * afterSales : 0
     * comment : 0
     */
    //待发货
    @SerializedName("waitShip")
    public int waitShip;
    //已发货
    @SerializedName("hasShip")
    public int hasShip;
    //已完成
    @SerializedName("hasComplete")
    public int hasComplete;
    //已关闭
    @SerializedName("hasClose")
    public int hasClose;
    //售后
    @SerializedName("afterSales")
    public int afterSales;
    //评论
    @SerializedName("comment")
    public int comment;
    //待付款
    @SerializedName("waitDel")
    public int waitDel;

    @SerializedName("saleCount")
    public int saleCount;
}
