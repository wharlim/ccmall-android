package com.weiju.ccmall.shared.basic

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import android.view.ViewGroup
import android.widget.TextView
import com.qmuiteam.qmui.util.QMUIStatusBarHelper
import com.weiju.ccmall.R

class AgentFActivity : AppCompatActivity() {

    companion object {

        val REQUEST_CODE = 712

        @JvmStatic
        fun start(a: Activity, c: Class<out Fragment>, b: Bundle? = null) {
            a.startActivityForResult(intent(a, c, b), REQUEST_CODE)
        }

        @JvmStatic
        fun start2(a: Activity, c: Class<out Fragment>, b: Bundle? = null) {
            val intent = intent(a, c, b, true)
            a.startActivityForResult(intent, REQUEST_CODE)
        }

        @JvmStatic
        fun start(f: Fragment, c: Class<out Fragment>, b: Bundle? = null) {
            f.startActivityForResult(intent(f.context!!, c, b), REQUEST_CODE)
        }

        @JvmStatic
        fun start2(f: Fragment, c: Class<out Fragment>, b: Bundle? = null) {
            val intent = intent(f.context!!, c, b, true)
            f.startActivityForResult(intent, REQUEST_CODE)
        }

        @JvmStatic
        fun start(mContext: Context, c: Class<out Fragment>, b: Bundle? = null) {
            val i = intent(mContext, c, b)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            mContext.startActivity(i)
        }

        @JvmStatic
        fun intent(mContext: Context, c: Class<out Fragment>, b: Bundle?, isInsertToolBar: Boolean = false): Intent {
            val i = Intent(mContext, AgentFActivity::class.java)
            i.putExtra("isInsertToolBar", isInsertToolBar)
            i.putExtra("bundle", b)
            i.putExtra("fragment", c.canonicalName)
            return i
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        super.onCreate(savedInstanceState)
        insertToolBar(intent.getBooleanExtra("isInsertToolBar", false))
        QMUIStatusBarHelper.setStatusBarLightMode(this)
        val fStr = intent.getStringExtra("fragment")
        val f = Class.forName(fStr).newInstance() as? Fragment?
        if (f != null) {
            val b = intent.getBundleExtra("bundle")
            f.arguments = b
            supportFragmentManager.beginTransaction()
                    .add(android.R.id.content, f, "f").commit()
        }
    }

    override fun setTitle(title: CharSequence?) {
        val textView = findViewById<TextView>(R.id.toolbarTitle)
        if (textView != null)
            textView.text = title
        else
            super.setTitle(title)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            onBackPressed()
        return super.onOptionsItemSelected(item)
    }

    private fun insertToolBar(isInsertToolBar: Boolean) {
        if (isInsertToolBar) {
            val parent = (window.decorView as ViewGroup).getChildAt(0) as? ViewGroup?
            parent?.fitsSystemWindows = true
            val toolbar = layoutInflater.inflate(R.layout.base_toolbar, parent, false) as? Toolbar
            parent?.addView(toolbar, 0, ViewGroup.LayoutParams(-1, -2))
            title = title
            setSupportActionBar(toolbar)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }
    }

}
