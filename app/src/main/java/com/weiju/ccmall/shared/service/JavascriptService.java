package com.weiju.ccmall.shared.service;

import android.support.annotation.NonNull;
import android.webkit.JavascriptInterface;

import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;

import org.greenrobot.eventbus.EventBus;

public class JavascriptService {

    @JavascriptInterface
    public void alert() {
        this.alert("");
    }

    @JavascriptInterface
    public void alert(@NonNull String message) {
        EventBus.getDefault().post(new EventMessage(Event.showAlert, message));
    }
}
