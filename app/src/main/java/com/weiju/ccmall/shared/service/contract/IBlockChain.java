package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.module.blockchain.beans.AllCCm;
import com.weiju.ccmall.module.blockchain.beans.BlockChainInfo;
import com.weiju.ccmall.module.blockchain.beans.BlockChainSwitch;
import com.weiju.ccmall.module.blockchain.beans.ChartDataItem;
import com.weiju.ccmall.module.blockchain.beans.Excess;
import com.weiju.ccmall.module.blockchain.beans.TemplateItem;
import com.weiju.ccmall.module.blockchain.beans.TransactionItem;
import com.weiju.ccmall.module.blockchain.beans.TransferableCCM;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface IBlockChain {

    @GET("pageConfig/reviewForPageConfig")
    Observable<RequestResult<BlockChainSwitch>> reviewForPageConfig();

    @POST("account/ccmTransferChainActivate")
    Observable<RequestResult<Object>> ccmTransferChainActivate();

    @GET("account/getAccountInfo")
    Observable<RequestResult<BlockChainInfo>> getAccountInfo();

    @GET("account/getDayLog")
    Observable<RequestResult<PaginationEntity<TransactionItem, Object>>> getDayLog(@Query("pageOffset") int page);

    @GET("account/getDayCcm")
    Observable<RequestResult<List<ChartDataItem>>> getDayCcm();

    @POST("account/getAllCcm")
    Observable<RequestResult<AllCCm>> getAllCcm();

    @GET("page/getAppTemplateConfigList")
    Observable<RequestResult<List<TemplateItem>>> getAppTemplateConfigList();

    @Streaming
    @GET
    Call<ResponseBody> download(@Url String url);

    @GET("account/sendTransCode.do")
    Observable<RequestResult<Object>> sendTransCode(@Query("phone") String phone);

    @FormUrlEncoded
    @POST("account/transferAccounts")
    Observable<RequestResult<Object>> transferAccounts(@Field("toAddress") String toAddress,
                                                       @Field("amount") String amount,
                                                       @Field("checkNumber") String checkNumber,
                                                       @Field("password") String password);

    @GET("account/getTransferableCcmCount")
    Observable<RequestResult<TransferableCCM>> getTransferableCcmCount();

    @POST("account/transferableCcm")
    Observable<RequestResult<Object>> transferableCcm();

    @GET("account/checkCcmExcess")
    Observable<RequestResult<Excess>> checkCcmExcess(@Query("amount") String amount);

    @GET("account/getTransInfo")
    Observable<RequestResult<Excess>> getTransInfo();

    @GET("account/checkCcmRollOut")
    Observable<RequestResult<Object>> checkCcmRollOut();
}
