package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.AddLive;
import com.weiju.ccmall.shared.bean.BannedUser;
import com.weiju.ccmall.shared.bean.BanningMessage;
import com.weiju.ccmall.shared.bean.CouponReceive;
import com.weiju.ccmall.shared.bean.CouponReceiveEx;
import com.weiju.ccmall.shared.bean.LiveCoupon;
import com.weiju.ccmall.shared.bean.LiveData;
import com.weiju.ccmall.shared.bean.LiveForecastTime;
import com.weiju.ccmall.shared.bean.LiveRedEnvelopes;
import com.weiju.ccmall.shared.bean.LiveRedEnvelopesEx;
import com.weiju.ccmall.shared.bean.LiveReportImageUploadItem;
import com.weiju.ccmall.shared.bean.LiveReportTypesItem;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.Message;
import com.weiju.ccmall.shared.bean.OpeningLiveAuthInfo;
import com.weiju.ccmall.shared.bean.OpeningLiveOrder;
import com.weiju.ccmall.shared.bean.OpeningLiveRecItem;
import com.weiju.ccmall.shared.bean.OpeningLiveSearchResult;
import com.weiju.ccmall.shared.bean.OpeningLiveVipLeaderInfo;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.body.AddLiveBody;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * @author chenyanming
 * @time 2019/11/22 on 11:46
 * @desc ${TODD}
 */
public interface ILiveService {
    String TYPE_JSON = "Content-Type:application/json";

    @Headers(TYPE_JSON)
    @POST()
    Observable<RequestResult<AddLive>> addLive(@Url String url, @Body AddLiveBody body);


    @GET("live/getLiveRecommend")
    Observable<RequestResult<List<LiveRoom>>> getLiveRecommend();

    @GET("live/getLiveList")
    Observable<RequestResult<PaginationEntity<LiveRoom, Object>>> getLiveList(@Query("memberId") String memberId,
                                                                              @Query("title") String title,
                                                                              @Query("pageOffset") int pageOffset,
                                                                              @Query("pageSize") int pageSize);
    @GET("live/searchLiveList")
    Observable<RequestResult<PaginationEntity<LiveRoom, Object>>> searchLiveList(
            @Query("liveListSearchType") int liveListSearchType,
            @Query("content") String content,
            @Query("pageOffset") int pageOffset,
            @Query("pageSize") int pageSize
    );
    @GET("live/getLiveList")
    Observable<RequestResult<PaginationEntity<LiveRoom, Object>>> getLiveListWithForecast(
            @Query("memberId") String memberId,
            @Query("title") String title,
            @Query("pageOffset") int pageOffset,
            @Query("pageSize") int pageSize,
            @Query("status") int status);

    @GET("live/getLiveIndexPage")
    Observable<RequestResult<PaginationEntity<LiveRoom, Object>>> getLiveListWithTab(
            @Query("memberId") String memberId,
            @Query("title") String title,
            @Query("pageOffset") int pageOffset,
            @Query("pageSize") int pageSize,
            @Query("tab") int tab); // 1 = 推荐，2= 关注

    @FormUrlEncoded
    @POST("live/startLive")
    Observable<RequestResult<AddLive>> startLive(@Field("liveId") String liveId);

    @GET("live/refreshPushUrl")
    Observable<RequestResult<AddLive>> refreshPushUrl(@Query("liveId") String liveId);

    @FormUrlEncoded
    @POST("live/stopLive")
    Observable<RequestResult<Object>> stopLive(@Field("liveId") String liveId);

    @GET("live/getLive")
    Observable<RequestResult<LiveRoom>> getLive(@Query("liveId") String liveId);


    @GET("live/outLive")
    Observable<RequestResult<Object>> outLive(@Query("liveId") String liveId);


    @GET("live/joinLive")
    Observable<RequestResult<LiveUser>> joinLive(@Query("liveId") String liveId, @Query("touristId") String touristId);

    @GET("live/getLiveAudienceList")
    Observable<RequestResult<PaginationEntity<LiveUser, Object>>> getLiveAudienceList(@Query("liveId") String liveId,
                                                                                      @Query("pageOffset") int pageOffset,
                                                                                      @Query("pageSize") int pageSize);

    @GET("live/getLiveUserInfo")
    Observable<RequestResult<LiveUser>> getLiveUserInfo(@Query("memberId") String memberId);

    @FormUrlEncoded
    @POST("live/followAnchor")
    Observable<RequestResult<Object>> followAnchor(@Field("anchorMemberId") String anchorMemberId, @Field("liveId") String liveId);

    @FormUrlEncoded
    @POST("live/cancelFollowAnchor")
    Observable<RequestResult<Object>> cancelFollowAnchor(@Field("anchorMemberId") String anchorMemberId);

    @FormUrlEncoded
    @POST("live/likeLive")
    Observable<RequestResult<Object>> likeLive(@Field("liveId") String liveId);


    @GET("live/getLiveSkus")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getLiveSkus(@Query("liveId") String liveId,
                                                                             @Query("pageOffset") int pageOffset,
                                                                             @Query("pageSize") int pageSize);

    @FormUrlEncoded
    @POST("live/addLiveSkuRelation")
    Observable<RequestResult<Object>> addLiveSkuRelation(@Field("liveId") String liveId,
                                                         @Field("skuId") String skuId);

    @POST("live/addLiveSkuRelations")
    Observable<RequestResult<Object>> addLiveSkuRelations(@Body AddLiveBody Body);

    @FormUrlEncoded
    @POST("live//removeLiveSkuRelation")
    Observable<RequestResult<Object>> removeLiveSkuRelation(@Field("liveId") String liveId,
                                                            @Field("skuId") String skuId);


    @GET("live/getLiveRedEnvelopesList")
    Observable<RequestResult<PaginationEntity<LiveRedEnvelopes, Object>>> getLiveRedEnvelopesList(@Query("liveId") String liveId,
                                                                                                  @Query("pageOffset") int pageOffset,
                                                                                                  @Query("pageSize") int pageSize);

    @FormUrlEncoded
    @POST("live/sendRedEnvelopes")
    Observable<RequestResult<LiveRedEnvelopes>> sendRedEnvelopes(@Field("liveId") String liveId,
                                                                 @Field("sendNum") int sendNum,
                                                                 @Field("sendMoney") long sendMoney,
                                                                 @Field("password") String password);

    @GET("live/getLiveRedEnvelopesDetailList")
    Observable<RequestResult<PaginationEntity<LiveRedEnvelopes, LiveRedEnvelopesEx>>> getLiveRedEnvelopesDetailList(@Query("liveId") String liveId,
                                                                                                                    @Query("redEnvelopesId") String redEnvelopesId,
                                                                                                                    @Query("pageOffset") int pageOffset,
                                                                                                                    @Query("pageSize") int pageSize);

    @GET("live/receiveRedEnvelopes")
    Observable<RequestResult<LiveRedEnvelopes>> receiveRedEnvelopes(@Query("liveId") String liveId,
                                                                    @Query("redEnvelopesId") String redEnvelopesId);

    @GET("report/getAllReportTitle")
    Observable<RequestResult<List<LiveReportTypesItem>>> getAllReportTitle();

    @POST("upload/uploadImages")
    Observable<RequestResult<List<LiveReportImageUploadItem>>> uploadImages(@Body RequestBody Body);

    @POST("report/record")
    Observable<RequestResult<Object>> addReport(@Body RequestBody Body);

    @GET("live/deleteLiveBroadcast")
    Observable<RequestResult<Object>> deleteLiveBroadcast(@Query("liveId") String liveId);

    @GET("live/updateLiveBroadcast")
    Observable<RequestResult<Object>> updateLiveBroadcast(
            @Query("liveId") String liveId,
            @Query("onlineStatus") int onlineStatus);

    @GET("live/getCount")
    Observable<RequestResult<LiveData>> getCount(
            @Query("liveId") String liveId);

    @GET("live/clearLivePassword")
    Observable<RequestResult<Object>> clearLivePassword(@Query("liveId") String liveId);

    @GET("live/getLiveStartTime")
    Observable<RequestResult<LiveForecastTime>> getLiveStartTime(
            @Query("liveId") String liveId);

    @FormUrlEncoded
    @POST("live/banningMessage")
    Observable<RequestResult<Object>> banningMessage(@Field("liveId") String liveId,
                                                     @Field("forbiddenMemberId") String forbiddenMemberId);

    @GET("live/getBanningMessageStatus")
    Observable<RequestResult<BanningMessage>> getBanningMessageStatus(@Query("liveId") String liveId);

    @GET("live/getBanningMessageList")
    Observable<RequestResult<PaginationEntity<BannedUser, Object>>> getBanningMessageList(@Query("liveId") String liveId,
                                                                                          @Query("pageOffset") int pageOffset,
                                                                                          @Query("pageSize") int pageSize);

    @FormUrlEncoded
    @POST("live/unbanningMessage")
    Observable<RequestResult<Object>> unbanningMessage(@Field("liveId") String liveId,
                                                       @Field("forbiddenMemberId") String forbiddenMemberId);

    @GET("live/getLiveCouponList")
    Observable<RequestResult<PaginationEntity<LiveCoupon, Object>>> getLiveCouponList(@Query("liveId") String liveId,
                                                                                      @Query("pageOffset") int pageOffset,
                                                                                      @Query("pageSize") int pageSize);


    @GET("live/getLiveCouponDetailList")
    Observable<RequestResult<PaginationEntity<CouponReceive, CouponReceiveEx>>> getLiveCouponDetailList(@Query("liveId") String liveId,
                                                                                                        @Query("sendId") String sendId,
                                                                                                        @Query("couponId") String couponId,
                                                                                                        @Query("pageOffset") int pageOffset,
                                                                                                        @Query("pageSize") int pageSize);


    @GET("live/receiveLiveCoupon")
    Observable<RequestResult<CouponReceive>> receiveLiveCoupon(@Query("sendId") String sendId, @Query("liveId") String liveId);

    @GET("live/getAllLiveCoupon")
    Observable<RequestResult<List<LiveCoupon>>> getAllLiveCoupon();

    @GET("live/getAllLiveCouponByLiveId")
    Observable<RequestResult<List<LiveCoupon>>> getAllLiveCouponByLiveId(@Query("liveId") String liveId);


    @FormUrlEncoded
    @POST("live/sendLiveCoupon")
    Observable<RequestResult<LiveCoupon>> sendLiveCoupon(@Field("liveId") String liveId,
                                                         @Field("sendNum") int sendNum,
                                                         @Field("couponId") String couponId,
                                                         @Field("remark") String remark);

    @GET("live/getLiveCouponById")
    Observable<RequestResult<LiveCoupon>> getLiveCouponById(@Query("couponId") String couponId);

    @FormUrlEncoded
    @POST("live/sendOutLine")
    Observable<RequestResult<Object>> sendOutLine(@Field("liveId") String liveId);

    @FormUrlEncoded
    @POST("live/clickShareOrGoods")
    Observable<RequestResult<Object>> clickShareOrGoods(
            @Field("liveId") String liveId,
            @Field("type") int type, // 1是分享，2是点击商品
            @Field("productId") String productId);

    // 获取剩余开通主播授权次数
    @GET("live/getAuthorizeUserInfo")
    Observable<RequestResult<OpeningLiveAuthInfo>> getAuthorizeUserInfo();
    @GET("live/getUserAuthorizeLevelInfo")
    // 获取用户开通vip/督导权限次数
    Observable<RequestResult<OpeningLiveVipLeaderInfo>> getUserAuthorizeLevelInfo();

    // 获取授权历史
    @GET("live/getLiveAuthorizeList")
    Observable<RequestResult<PaginationEntity<OpeningLiveRecItem, Object>>> getLiveAuthorizeList(
            @Query("pageOffset") int pageOffset,
            @Query("pageSize") int pageSize,
            @Query("type") String type);

    // 授权页面搜索用户
    @GET("live/searchMember")
    Observable<RequestResult<OpeningLiveSearchResult>> searchMember(@Query("phone") String phone, @Query("type") String type);

    // 开通
    @GET("live/openingLevel")
    Observable<RequestResult<OpeningLiveSearchResult>> opening(@Query("memberId") String memberId,@Query("type") String type);

    // 生成39.8元授权直播订单
    @GET("live/addAuthorizeOrder")
    Observable<RequestResult<OpeningLiveOrder>> addAuthorizeOrder(@Query("memberId") String memberId);

    @GET("liveStore/checkLiveStore")
    Observable<RequestResult<Object>> checkLiveStore();

    @GET("liveStore/productList")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> productList(@Query("pageOffset") int pageOffset, @Query("pageSize") int pageSize);

    @GET("live/getSysNote")
    Observable<RequestResult<Message>> getSysNote();

    @FormUrlEncoded
    @POST("live/addLiveSkuId")
    Observable<RequestResult<Object>> addLiveSkuId(@Field("liveId") String liveId, @Field("skuId") String skuId);

    @FormUrlEncoded
    @POST("live/addLiveSkuId")
    Observable<RequestResult<Object>> deleteLiveSkuId(@Field("liveId") String liveId);

    @FormUrlEncoded
    @POST("live/getLiveSku")
    Observable<RequestResult<SkuInfo>> getLiveSku(@Field("liveId") String liveId);

}
