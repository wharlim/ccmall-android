package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LiveData implements Serializable {

    /**
     * liveId : 139b7f9c4bc94da7b8cb71b84c94c333
     * shareCount : 0
     * interactionCount : 0
     * goodClickCount : 0
     * createDate :
     * updateDate :
     * popularity : 0
     * saleCount : 0
     * saleMoney : 0
     * predictProfitMoney : 0
     * profitCount : 0
     * joinliveMember : 1
     * liveImage :
     * title :
     * time : 0
     * startTime : 2020-01-14 17:13:50
     * endTime : 2020-01-14 17:14:32
     */

    @SerializedName("liveId")
    public String liveId;
    @SerializedName("shareCount")
    public int shareCount;
    @SerializedName("interactionCount")
    public int interactionCount;
    @SerializedName("goodClickCount")
    public int goodClickCount;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("popularity")
    public int popularity;
    @SerializedName("saleCount")
    public int saleCount;
    @SerializedName("saleMoney")
    public int saleMoney;
    @SerializedName("predictProfitMoney")
    public int predictProfitMoney;
    @SerializedName("profitCount")
    public int profitCount;
    @SerializedName("joinliveMember")
    public int joinliveMember;
    @SerializedName("liveImage")
    public String liveImage;
    @SerializedName("title")
    public String title;
    @SerializedName("time")
    public int time;
    @SerializedName("startTime")
    public String startTime;
    @SerializedName("endTime")
    public String endTime;
    @SerializedName("livePasswordDecode")
    public String livePasswordDecode; // 原始密码
    @SerializedName("hasLivePasswordDecode")
    public boolean hasLivePasswordDecode; // 是否有原始密码
}
