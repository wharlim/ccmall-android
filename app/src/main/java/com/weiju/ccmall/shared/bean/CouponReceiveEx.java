package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
public class CouponReceiveEx {


    /**
     * liveCouponSend : {"sendId":"0158824bf9bc42c5b1a971dd52dbc47a","couponId":"7dad1585889d496bb64a7addddebf36c","liveId":"dae42bcc1c0e463faec0adefe3ef2fc3","memberId":"af8d785b384ad9281286275095141f15","title":"直播间优惠券","sendNum":10,"remark":"","status":0,"receiveStatus":0,"balanceCode":"0211582435875511","receiveNum":1,"createDate":"2020-02-20 22:21:39","updateDate":"2020-02-20 22:31:08","deleteFlag":0}
     * receiveDetail : {"receiveId":"bd799aac14d445a5aed96355b1bab386","couponId":"7dad1585889d496bb64a7addddebf36c","memberId":"dac68adea0f44385ae2b84e03609a285","headImage":"http://thirdwx.qlogo.cn/mmopen/vi_32/ngNm6nYYe7N0944HXQArIIuY9TVyn2VSHLCY1vb4FHiaVibicOE7p2WsnEX9IkxznCuLJhJiaHIlIKdvUElEh3Pc2Q/132","nickName":"少龙","liveId":"dae42bcc1c0e463faec0adefe3ef2fc3","status":0,"balanceCode":"1331582436427887","createDate":"2020-02-20 22:31:12","updateDate":"2020-02-20 22:31:12","deleteFlag":0}
     */

    @SerializedName("liveCouponSend")
    public LiveCouponSend liveCouponSend;
    @SerializedName("receiveDetail")
    public ReceiveDetail receiveDetail;

}
