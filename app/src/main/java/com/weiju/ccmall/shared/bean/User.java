package com.weiju.ccmall.shared.bean;

import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * 用户Bean
 * Created by JayChan on 2016/12/13.
 */
public class User implements Serializable {
    @SerializedName("isShopkeeper")
    public int isShopkeeper;

    @SerializedName("memberId")
    public String id;
    @SerializedName("inviteCode")
    public String invitationCode;
    @SerializedName("headImage")
    public String avatar;
    @SerializedName("phone")
    public String phone;
    /**
     * 超级团购角色（0团员 1团长）
     */
    @SerializedName("role")
    public int role;
    @SerializedName("nickName")
    public String nickname;
    @SerializedName("wechat")
    public String wechat;
    @SerializedName("wechatOpenId")
    public String openId;
    @SerializedName("wechatUnionId")
    public String unionId;
    @SerializedName("userName")
    public String userName = "新用户";
    @SerializedName("identityCard")
    public String identityCard;
    /**
     * 订货管家里面的等级 没卵用的
     */
    @SerializedName("memberType")
    public int memberType;
    @SerializedName("memberTypeStr")
    public String memberTypeStr;
    @SerializedName("authStatus")
    public int authStatus;
    @SerializedName("authStatusStr")
    public String authStatusStr;
    @SerializedName("signinStatus")
    public int isSignIn;
    /**
     * 店铺等级
     */
    @SerializedName("storeType")
    public int storeType;

    @SerializedName("storeTypeStr")
    public String storeTypeStr;
    /**
     * 等级
     */
    @SerializedName("vipType")
    public int vipType;
    @SerializedName("vipTypeStr")
    public String vipTypeStr;
    @SerializedName("isStore")
    public int isStore;
    /**
     * 有没有发布官方消息的权限
     */
    @SerializedName("isPower")
    public int isPower;

    @SerializedName("referrerMemberId")
    public String referrerMemberId;

    @SerializedName("autoAuthStatus")
    public int autoAuthStatus;

    @SerializedName("conditionCount")
    public int conditionCount;

    @SerializedName("isUpgrade")
    public int isUpgrade;//0，不满足升级,1，所有条件通过，已满足升级

    @SerializedName("needCount")
    public int needCount;
    @SerializedName("needLowGradeCount")
    public int needLowGradeCount;
    @SerializedName("vipTypeCondition")
    public int vipTypeCondition; // 商城等级是否满足，只C4，C7用，0不满足，1满足

    @SerializedName("nextVipType")
    public int nextVipType;

    @SerializedName("oldVipType")
    public int oldVipType;

    @SerializedName("percent")
    public double percent;

    @SerializedName("pageId")
    public String pageId;

    @SerializedName("orderCode")
    public String orderCode;

    @SerializedName("selfOrderStatus")
    public boolean selfOrderStatus;

    @SerializedName("storeName")
    public String storeName;

    @SerializedName("tipMobile")
    public String tipMobile;

    @SerializedName("isSeed")
    public int isSeed;//isSeed ：是否是种子用户，0：不是种子用户，1：是种子用户

    @SerializedName("seedAwardStatus")
    public int seedAwardStatus;//seedAwardStatus：CCM强制订单状态；0，没有下CCM奖励的强制单；1，已经下了CCM奖励的强制单

    /*@SerializedName("tencentIMUserSig")
    public String tencentIMUserSig;

    @SerializedName("tencentIMSDKAppId")
    public String tencentIMSDKAppId;*/

    /*@SerializedName("orderPayStatus")
    public boolean orderPayStatus;*/
    @SerializedName("ccmTransferChainActivate")
    public int ccmTransferChainActivate; // 1已激活

    public boolean isCCMActivate() {
        return ccmTransferChainActivate == 1;
    }


    public static User fromJson(@Nullable String json) {
        if (json == null || json.isEmpty()) {
            return null;
        }
        return new Gson().fromJson(json, User.class);
    }

    /**
     * @return 是否显示会员价
     */
    public boolean isShopkeeper() {
        return true;
    }


    public int getRadioType() {
        if (memberType == 0) {
            return vipType;
        } else if (memberType == 1) {
            if (vipType > 3) {
                return vipType;
            } else {
                return 3;
            }
        } else if (memberType >= 2) {
            return 4;
        }
        return vipType;
    }
}
