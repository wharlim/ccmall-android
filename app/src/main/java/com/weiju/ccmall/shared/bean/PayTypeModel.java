package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class PayTypeModel {
    /**
     * appType : 1
     * payType : 1
     * payTypeStr : 微信支付
     * iconUrl : rBIC71pUZmSAd-KjAAANlm8Mcus576.png
     */

    @SerializedName("appType")
    public int appType;
    @SerializedName("payType")
    public int payType;
    @SerializedName("payTypeStr")
    public String payTypeStr;
    @SerializedName("iconUrl")
    public String iconUrl;

    public PayTypeModel(int appType, int payType, String payTypeStr, String iconUrl) {
        this.appType = appType;
        this.payType = payType;
        this.payTypeStr = payTypeStr;
        this.iconUrl = iconUrl;
    }
}
