package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.InvitationCode;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service.contract
 * @since 2017-08-03
 */
public interface IInviteService {

    @GET("code/getInvitationCode")
    Observable<RequestResult<InvitationCode>> getInvitationCode();

    @GET("code/getInvitationCodeByIcon")
    Observable<RequestResult<InvitationCode>> getInvitationCodeByIcon();

    @GET("code/getInvitationCodeByIconCreditCard")
    Observable<RequestResult<InvitationCode>> getInvitationCodeByIconCreditCard();
}
