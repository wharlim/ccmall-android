package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/6/26 on 17:31
 * @desc ${TODD}
 */
public class PkDetailEntity {

    /**
     * challengeId : 610452284e8844d2858386a27d7c32a7
     * pkScore : 20
     * memberId : 67e7258cc6cb411e8978f92b7c41f05c
     * userName :
     * nickName : 68
     * headImage : http://testimg.beautysecret.cn/G1/M00/00/03/cjcLYVmULpaANAOKAANdia7CY2E731.jpg
     * idType : 1
     * pkChallengeId : 94c25e293e5c435584b77f416e2b382c
     * pkMemberId : 31ca6e58263d468caad89e3457f823ca
     * pkUserName : 4068
     * pkNickName : 一个忧伤的胖子
     * pkHeadImage : http://wx.qlogo.cn/mmopen/vi_32/r7l5z7myibviatYzOjq58OeGgk5Zxt6vMYuibO4bj1IIHpUwZ0bOh0EjyAXMuL40wAneicK0gYjGrYVgicBLBs22sbw/0
     * pkStartDate : 2019-06-25 11:19:33
     * startPoint : 0
     * endPoint : 0
     * pkStartPoint : 0
     * pkEndPoint : 0
     * winMemberId :
     * createDate : 2019-06-25 11:19:36
     * updateDate : 2019-06-25 11:19:36
     * deleteFlag : 0
     */

    @SerializedName("challengeId")
    public String challengeId;
    @SerializedName("pkScore")
    public int pkScore;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("userName")
    public String userName;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("idType")
    public int idType;
    @SerializedName("pkChallengeId")
    public String pkChallengeId;
    @SerializedName("pkMemberId")
    public String pkMemberId;
    @SerializedName("pkUserName")
    public String pkUserName;
    @SerializedName("pkNickName")
    public String pkNickName;
    @SerializedName("pkHeadImage")
    public String pkHeadImage;
    @SerializedName("pkStartDate")
    public String pkStartDate;
    @SerializedName("startPoint")
    public int startPoint;
    @SerializedName("endPoint")
    public int endPoint;
    @SerializedName("pkStartPoint")
    public int pkStartPoint;
    @SerializedName("pkEndPoint")
    public int pkEndPoint;
    @SerializedName("winMemberId")
    public String winMemberId;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
}
