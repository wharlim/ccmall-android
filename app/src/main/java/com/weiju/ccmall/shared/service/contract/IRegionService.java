package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.RegionCity;
import com.weiju.ccmall.shared.bean.RegionDistinct;
import com.weiju.ccmall.shared.bean.RegionProvince;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service.contract
 * @since 2017-07-06
 */
public interface IRegionService {

    @GET("address/province")
    Observable<RequestResult<List<RegionProvince>>> getProvinceList();

    @GET("address/city")
    Observable<RequestResult<List<RegionCity>>> getCityList(@Query("provinceId") String provinceId);

    @GET("address/district")
    Observable<RequestResult<List<RegionDistinct>>> getDistinctList(@Query("cityId") String cityId);
}
