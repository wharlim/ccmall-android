package com.weiju.ccmall.shared.component;

import com.chad.library.adapter.base.loadmore.LoadMoreView;
import com.weiju.ccmall.R;

/**
 * @author chenyanming
 * @time 2019/11/22 on 18:13
 * @desc ${TODD}
 */
public class NullLoadMoreView extends LoadMoreView {

    @Override
    public int getLayoutId() {
        return R.layout.null_view_load_more;
    }

    @Override
    protected int getLoadingViewId() {
        return R.id.load_more_loading_view;
    }

    @Override
    protected int getLoadFailViewId() {
        return R.id.load_more_load_fail_view;
    }

    @Override
    protected int getLoadEndViewId() {
        return R.id.load_more_load_end_view;
    }
}
