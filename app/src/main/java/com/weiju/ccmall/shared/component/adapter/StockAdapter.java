package com.weiju.ccmall.shared.component.adapter;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.PropertyValue;

/**
 * @author chenyanming
 * @time 2019/11/7 on 10:46
 * @desc ${TODD}
 */
public class StockAdapter extends BaseQuickAdapter<PropertyValue, BaseViewHolder> {
    public StockAdapter() {
        super(R.layout.item_stock);
    }

    @Override
    protected void convert(BaseViewHolder helper, PropertyValue item) {
        helper.setText(R.id.tvStockTitle, item.value);
        EditText editText = helper.getView(R.id.etStock);
        editText.setText(item.stock);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                item.stock = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
