package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.activity.LiveHomeActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveMessage;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @date 2020/2/20.
 */
public class LiveBlockDialog extends Dialog {

    @BindView(R.id.ivAvatar)
    SimpleDraweeView mSimpleDraweeView;
    @BindView(R.id.tvName)
    TextView mTvName;
    private final LiveMessage mLiverUser;
    private final String mLiveId;

    Activity mActivity;

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);

    public LiveBlockDialog(Activity context, String liveId, LiveMessage liveUser) {
        super(context, R.style.Theme_Light_Dialog);
        mLiverUser = liveUser;
        mLiveId = liveId;
        mActivity = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_block);
        ButterKnife.bind(this);

        FrescoUtil.setImage(mSimpleDraweeView, mLiverUser.headImage);
        mTvName.setText(mLiverUser.name);
    }

    @OnClick({R.id.tvCanle})
    protected void close() {
        dismiss();
    }

    @OnClick({R.id.tvSure})
    protected void block() {
        APIManager.startRequest(mService.banningMessage(mLiveId, mLiverUser.memberId), new BaseRequestListener<Object>(mActivity) {
            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("禁言成功");
                dismiss();
            }
        }, getContext());
    }

}

