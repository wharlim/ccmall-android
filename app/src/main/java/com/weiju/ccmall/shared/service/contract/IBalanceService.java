package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.Balance;
import com.weiju.ccmall.shared.bean.CommonExtra;
import com.weiju.ccmall.shared.bean.DayProfit;
import com.weiju.ccmall.shared.bean.DealDetail;
import com.weiju.ccmall.shared.bean.ExchGoldTicket;
import com.weiju.ccmall.shared.bean.ExchRatioTrend;
import com.weiju.ccmall.shared.bean.PayDetail;
import com.weiju.ccmall.shared.bean.ProfitData;
import com.weiju.ccmall.shared.bean.Transfer;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface IBalanceService {

    @GET("profit/get")
    Observable<RequestResult<ProfitData>> get();

    @GET("profit/getProfitList")
    Observable<RequestResult<PaginationEntity<Balance, CommonExtra>>> getProfitList(@Query("pageOffset") int page);

    @GET("profit/getProfitListByPlatForm")
    Observable<RequestResult<PaginationEntity<Balance, CommonExtra>>> getProfitListByPlatForm(
            @Query("platForm") int platForm, @Query("pageOffset") int page, @Query("pageSize") int pageSize);

    @GET("profit/getBalanceList")
    Observable<RequestResult<PaginationEntity<Balance, CommonExtra>>> getBalanceList(@Query("pageOffset") int page);


    @GET("profit/getDayProfitStatus")
    Observable<RequestResult<DayProfit>> getDayProfitStatus();

    @GET("profit/getDayProfit")
    Observable<RequestResult<DayProfit>> getDayProfit();

    @GET("coin/coinlist")
    Observable<RequestResult<PaginationEntity<Balance, CommonExtra>>> getCoinList(@Query("pageOffset") int page);

    @GET("coin/ticketlist")
    Observable<RequestResult<PaginationEntity<Balance, CommonExtra>>> getTicketlist(@Query("pageOffset") int page);

    @GET("profit/getGoldList")
    Observable<RequestResult<PaginationEntity<Balance, CommonExtra>>> getGoldList(@Query("pageOffset") int page);

    @GET("coin/goldenTicketList")
    Observable<RequestResult<PaginationEntity<Balance, CommonExtra>>> getGoldTicketList(@Query("pageOffset") int page);

    @GET("stat/getContributionList")
    Observable<RequestResult<PaginationEntity<Balance, CommonExtra>>> getContributionList(@Query("pageOffset") int page,@Query("type") String type);

    @GET("transfer/getTransferDetail")
    Observable<RequestResult<Transfer>> getTransferDetail(@Query("transferId") String transferId);

    @GET("transfer/getPayDetail")
    Observable<RequestResult<PayDetail>> getPayDetail(@Query("payId") String payId);

    @GET("deal/get/{id}")
    Observable<RequestResult<DealDetail>> getDealDetail(@Path("id") String dealId);


    @FormUrlEncoded
    @POST("withdraw/do")
    Observable<RequestResult<Object>> addDealGongCat(
            @Field("token") String token,
            @Field("amount") long amount,
            @Field("remark") String remark
    );

    @POST("deal/add")
    @FormUrlEncoded
    Observable<RequestResult<Object>> addDeal(
            @Field("applyMoney") long applyMoney,
            @Field("applyAccountId") String applyAccountId
    );

    @GET("coin/exchRatio")
    Observable<RequestResult<ExchGoldTicket>> exchRatio();

    @GET("coin/exchRatioTrend")
    Observable<RequestResult<List<ExchRatioTrend>>> exchRatioTrend();


    @POST("coin/exchGoldenTicket")
    @FormUrlEncoded
    Observable<RequestResult<Object>> exchGoldenTicket(
            @Field("coin") long coin,
            @Field("password") String password,
            @Field("checkNumber") String checkNumber
    );
}
