package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.ActivityCheckResult;
import com.weiju.ccmall.shared.bean.GetSecondKillProductListModel;
import com.weiju.ccmall.shared.bean.InstantData;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service.contract
 * @since 2017-08-01
 */
public interface IInstantService {

    @GET("secondKill/getSecondKillList")
    Observable<RequestResult<List<InstantData.SecondKill>>> getInstantList();

    @GET("secondKill/getSecondKillProductList")
    Observable<RequestResult<GetSecondKillProductListModel>> getSecondKillProductList(
            @Query("pageOffset") int page,
            @Query("pageSize") int size,
            @Query("secondKillId") String id
    );
    @FormUrlEncoded
    @POST("secondKill/seckillExchangeTicket")
    Observable<RequestResult<HashMap>> seckillExchangeTicket(@Field("realName") String name,
                                                             @Field("phone") String phone,
                                                             @Field("address") String address);

    @GET("secondKill/checkSeckillOrderRelation")
    Observable<RequestResult<ActivityCheckResult>> checkSeckillOrderRelation();
}
