package com.weiju.ccmall.shared.bean;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class ProfitMoney {

    /**
     * profitMoney : 2884
     */

    @SerializedName("profitMoney")
    public float profitMoney;
    public String getProfitMoney() {
        return String.format("%.2f", profitMoney);
    }
}
