package com.weiju.ccmall.shared.page.element;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.util.ConvertUtil;

public class SpacerElement extends LinearLayout {

    public SpacerElement(Context context, Element element) {
        super(context);
        View view = inflate(getContext(), R.layout.el_spacer_layout, this);
        SimpleDraweeView simpleDraweeView = view.findViewById(R.id.eleBackgroundIv);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT, ConvertUtil.convertHeight(getContext(), 750, element.height));
        simpleDraweeView.setLayoutParams(layoutParams);
        LayoutParams layoutParams1 = new LayoutParams(LayoutParams.MATCH_PARENT, ConvertUtil.convertHeight(getContext(), 750, element.height));
        view.setLayoutParams(layoutParams1);
        element.setBackgroundInto(view);
    }
}
