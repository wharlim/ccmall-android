package com.weiju.ccmall.shared.component.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.ViewGroup;
import android.view.Window;

import com.weiju.ccmall.R;
import com.zyyoona7.wheel.WheelView;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/13 on 17:33
 * @desc
 */
public class SelectAddressDialog extends BottomSheetDialog {


    @BindView(R.id.wvProvince)
    WheelView mWvProvince;
    @BindView(R.id.wvCity)
    WheelView mWvCity;
    @BindView(R.id.wvArea)
    WheelView mWvArea;
    private OnConfirmListener mOnConfirmListener;


    private String[] Province = new String[]{"北京市", "天津市", "河北省"};

    private String[] Beijingcity = new String[]{"北京市"};

    private String[] tianjincity = new String[]{"天津市"};
    private String[] hebeicity = new String[]{"石家庄市", "唐山市", "秦皇岛市", "邯郸市", "邢台市", "保定市", "张家口市", "承德市", "沧州市"};


    private String[] beijingarea = new String[]{"东城区", "西城区", "崇文区", "宣武区", "朝阳区", "丰台区", "石景山区", "海淀区", "门头沟区"};

    private String[] tianjinarea = new String[]{"和平区", "河东区", "河西区", "南开区", "河北区", "红桥区", "塘沽区", "汉沽区", "大港区"};


    private String[] shijiaarea = new String[]{"长安区", "桥东区", "桥西区", "裕华区", "朝阳区", "井陉矿区", "井陉县", "正定县", "栾城区"};
    private String[] tangshanarea = new String[]{"路南区", "路北区", "古冶区", "开平区", "丰南区", "丰润区", "滦县", "海淀区", "门头沟区"};

    private int ProcinceIndex;

    public SelectAddressDialog(@NonNull Context context, OnConfirmListener onConfirmListener) {
        super(context, R.style.Theme_Light_Dialog);
        mOnConfirmListener = onConfirmListener;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_address);
        ButterKnife.bind(this);

        Window window = getWindow();
        if (null != window) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        initView();
        initData();
    }

    private void initView() {
        mWvProvince.setOnItemSelectedListener(new WheelView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelView wheelView, Object data, int position) {
                ProcinceIndex = position;
                switch (position) {
                    case 0:
                        mWvCity.setData(Arrays.asList(Beijingcity));
                        setAreaData(Arrays.asList(beijingarea));
                        break;
                    case 1:
                        mWvCity.setData(Arrays.asList(tianjincity));
                        setAreaData(Arrays.asList(tianjinarea));
                        break;
                    case 2:
                        mWvCity.setData(Arrays.asList(hebeicity));
                        setAreaData(Arrays.asList(shijiaarea));
                        break;
                    default:

                }
                mWvCity.setSelectedItemPosition(0);

            }
        });

        mWvCity.setOnItemSelectedListener(new WheelView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(WheelView wheelView, Object data, int position) {
                switch (ProcinceIndex) {
                    case 0:
                        mWvArea.setData(Arrays.asList(beijingarea));
                        break;
                    case 1:
                        mWvArea.setData(Arrays.asList(tianjinarea));
                        break;
                    case 2:
                        mWvArea.setData(Arrays.asList(position == 0 ? shijiaarea : tangshanarea));
                        break;

                    default:
                }

                mWvArea.setSelectedItemPosition(0);

            }
        });
    }

    private void setAreaData(List<String> list) {
        mWvArea.setData(list);
    }

    private void initData() {
        mWvProvince.setData(Arrays.asList(Province));
        mWvCity.setData(Arrays.asList(Beijingcity));
        mWvArea.setData(Arrays.asList(beijingarea));

        mWvProvince.setSelectedItemPosition(0);
        mWvCity.setSelectedItemPosition(0);
        mWvArea.setSelectedItemPosition(0);
    }

    @OnClick(R.id.cancel)
    protected void cancelSelect() {
        dismiss();
    }

    @OnClick(R.id.confirm)
    protected void confirm() {
        String province = (String) mWvProvince.getSelectedItemData();
        String city = (String) mWvCity.getSelectedItemData();
        String area = (String) mWvArea.getSelectedItemData();

        mOnConfirmListener.confirm(String.format("%s%s%s", province, city, area));
        dismiss();
    }

    public interface OnConfirmListener {
        void confirm(String date);
    }
}
