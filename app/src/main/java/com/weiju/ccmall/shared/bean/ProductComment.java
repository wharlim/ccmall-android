package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * <p>
 * Created by zjm on 2017/8/21.
 */
public class ProductComment {

    /**
     * headImage : cjc2GFmMXByAJQt-AAFk_SYxw6U982.jpg
     * nickName : 这**称
     * properties : 瓶:250ml/瓶
     * content : 这是我购买的产品，我是要追加评价的
     * images : ["22"]
     * reply :
     * payDate : 2018-08-24 20:02:09
     * commentDate : 2018-08-24 20:02:09
     * day : 0
     * addToComment : {"headImage":"","nickName":"这**称","properties":"瓶:250ml/瓶 ","content":"我来追加评价了，哈哈","images":["图片1","图片2","图片3"],"reply":"","payDate":"2018-08-27 10:33:02","commentDate":"2018-08-27 10:33:02","day":3,"addToComment":"","descScore":0,"expressScore":0,"serveScore":0}
     * descScore : 50
     * expressScore : 50
     * serveScore : 50
     */

    @SerializedName("headImage")
    public String headImage;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("properties")
    public String properties;
    @SerializedName("content")
    public String content;
    @SerializedName("reply")
    public String reply;
    @SerializedName("payDate")
    public String payDate;
    @SerializedName("commentDate")
    public String commentDate;
    @SerializedName("day")
    public int day;
    @SerializedName("addToComment")
    public AddToCommentEntity addToComment;
    @SerializedName("descScore")
    public int descScore;
    @SerializedName("expressScore")
    public int expressScore;
    @SerializedName("serveScore")
    public int serveScore;
    @SerializedName("images")
    public List<String> images;

    @SerializedName("phone")
    public String phone;

    @SerializedName("orderCode")
    public String orderCode;

    @SerializedName("deleteStatus")
    public int deleteStatus;



    public static class AddToCommentEntity {
        /**
         * headImage :
         * nickName : 这**称
         * properties : 瓶:250ml/瓶
         * content : 我来追加评价了，哈哈
         * images : ["图片1","图片2","图片3"]
         * reply :
         * payDate : 2018-08-27 10:33:02
         * commentDate : 2018-08-27 10:33:02
         * day : 3
         * addToComment :
         * descScore : 0
         * expressScore : 0
         * serveScore : 0
         */

        @SerializedName("headImage")
        public String headImage;
        @SerializedName("nickName")
        public String nickName;
        @SerializedName("properties")
        public String properties;
        @SerializedName("content")
        public String content;
        @SerializedName("reply")
        public String reply;
        @SerializedName("payDate")
        public String payDate;
        @SerializedName("commentDate")
        public String commentDate;
        @SerializedName("day")
        public int day;
        @SerializedName("descScore")
        public int descScore;
        @SerializedName("expressScore")
        public int expressScore;
        @SerializedName("serveScore")
        public int serveScore;
        @SerializedName("images")
        public List<String> images;



    }
}
