package com.weiju.ccmall.shared.page.bean;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.weiju.ccmall.shared.basic.BaseModel;
import com.weiju.ccmall.shared.bean.SkuInfo;

import java.lang.reflect.Type;
import java.util.List;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/9/13.
 */
public class InstantSwiperData extends BaseModel {

    @SerializedName("secondKill")
    public SecondKillEntity secondKill;
    @SerializedName("secondKillProducts")
    public List<SkuInfo> secondKillProducts;

    public static class SecondKillEntity {
        /**
         * secondKillId : e51e805fadbe4acca9644123dcc68c69
         * title : 这是秒杀活动已开始
         * status : 1
         * event : none
         * target :
         * image : http://39.108.50.110/G1/M00/00/2B/rBIdXFqzN2WAKtZ2AANcK_SkdpM388.jpg
         * startDate : 2018-09-12 14:20:00
         * endDate : 2018-09-19 14:20:59
         * createDate : 2018-03-14 11:58:49
         * statusStr : 抢购中
         */

        @SerializedName("secondKillId")
        public String secondKillId;
        @SerializedName("title")
        public String title;
        @SerializedName("status")
        public int status;
        @SerializedName("event")
        public String event;
        @SerializedName("target")
        public String target;
        @SerializedName("image")
        public String image;
        @SerializedName("startDate")
        public String startDate;
        @SerializedName("endDate")
        public String endDate;
        @SerializedName("createDate")
        public String createDate;
        @SerializedName("statusStr")
        public String statusStr;
    }

    public static InstantSwiperData json2Model(JsonElement json) {
        Type type = new TypeToken<InstantSwiperData>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }
}
