package com.weiju.ccmall.shared.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.module.xysh.bean.XyshUser;
import com.weiju.ccmall.module.xysh.fragment.XinYongHomeFragment;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;

import org.greenrobot.eventbus.EventBus;

public class SessionUtil {

    private static SessionUtil instance;
    private static Context mContext;
    private static SharedPreferences sharedPreferences;

    private SessionUtil() {
        mContext = MyApplication.getInstance().getApplicationContext();
        sharedPreferences = mContext.getSharedPreferences("Settings.sp", Context.MODE_PRIVATE);
    }

    public static SessionUtil getInstance() {
        if (instance == null || mContext == null) {
            SessionUtil.destroy();
            instance = new SessionUtil();
        }
        return instance;
    }

    private static void destroy() {
        if (instance != null) {
            mContext = null;
            sharedPreferences = null;
            instance = null;
        }
    }

    public void setTIMInfo(String tencentIMUserSig, String memberId) {
        sharedPreferences.edit().putString(Key.KEY_tencentIMUserSig, tencentIMUserSig).apply();
        sharedPreferences.edit().putString(Key.KEY_memberId, memberId).apply();
    }

    // TIM签名， 有效期3个月
    public String getTencentIMUserSig() {
        return sharedPreferences.getString(Key.KEY_tencentIMUserSig, null);
    }

    // TIM登录用户的id
    public String getMemberId() {
        return sharedPreferences.getString(Key.KEY_memberId, null);
    }

    public boolean setLoginUser(User user) {
        return sharedPreferences.edit().putString(Key.USER, new Gson().toJson(user)).commit();
    }

    /**
     * 获取登录用户信息
     *
     * @return
     */
    public User getLoginUser() {
        String json = sharedPreferences.getString(Key.USER, "");
        return User.fromJson(json);
    }

    public LiveUser getLiveUser() {
        String json = sharedPreferences.getString(Key.LIVE_USER, "");
        return LiveUser.fromJson(json);
    }

    public boolean setLiveUser(LiveUser liveUser) {
        return sharedPreferences.edit().putString(Key.LIVE_USER, new Gson().toJson(liveUser)).commit();
    }

    public boolean setXyshUserInfo(XyshUser result) {
        return sharedPreferences.edit().putString(Key.XYSH_USER, new Gson().toJson(result)).commit();
    }

    public XyshUser getXyshUserInfo() {
        String json = sharedPreferences.getString(Key.XYSH_USER, "");
        return XyshUser.fromJson(json);
    }

    public boolean setOAuthToken(String token) {
        boolean isSuccess = sharedPreferences.edit().putString(Key.OAUTH, token).commit();
        if (isSuccess) {
            EventBus.getDefault().post(new EventMessage(Event.setOAuthTokenSuccess));
        }
        return isSuccess;
    }

    public String getOAuthToken() {
        return sharedPreferences.getString(Key.OAUTH, "");
    }

    public boolean isLogin() {
        return getLoginUser() != null;
    }

    public void logout() {
        sharedPreferences.edit().remove(Key.USER).remove(Key.OAUTH).apply();
        sharedPreferences.edit().remove(Key.KEY_tencentIMUserSig).remove(Key.KEY_memberId).apply();
        sharedPreferences.edit().remove(Key.XYSH_USER).apply();
        XinYongHomeFragment.shouldGetNotice = true;
    }

    public void putString(String key, String value) {
        key += getLoginUser().phone;
        sharedPreferences.edit().putString(key, value).apply();
    }

    public String getString(String key) {
        key += getLoginUser().phone;
        return sharedPreferences.getString(key, null);
    }
}
