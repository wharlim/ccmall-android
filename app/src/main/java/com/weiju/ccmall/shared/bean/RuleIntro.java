package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class RuleIntro {
    @SerializedName("imageUrl")
    public String imageUrl;
    @SerializedName("width")
    public int width;
    @SerializedName("height")
    public int height;
}
