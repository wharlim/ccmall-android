package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class MainAdModel {

    @SerializedName("backUrl")
    public String backUrl;
    @SerializedName("target")
    public String target;
    @SerializedName("event")
    public String event;
}
