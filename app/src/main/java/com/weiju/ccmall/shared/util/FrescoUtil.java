package com.weiju.ccmall.shared.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Animatable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.blankj.utilcode.utils.ConvertUtils;
import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.ScreenUtils;
import com.blankj.utilcode.utils.SizeUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.common.logging.FLog;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.controller.AbstractDraweeController;
import com.facebook.drawee.controller.BaseControllerListener;
import com.facebook.drawee.controller.ControllerListener;
import com.facebook.drawee.drawable.ScalingUtils;
import com.facebook.drawee.generic.GenericDraweeHierarchy;
import com.facebook.drawee.generic.GenericDraweeHierarchyBuilder;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.image.ImageInfo;
import com.facebook.imagepipeline.image.QualityInfo;
import com.facebook.imagepipeline.request.BasePostprocessor;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.facebook.imagepipeline.request.Postprocessor;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;


/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.util
 * @since 2017-06-09
 */
public class FrescoUtil {
    private static final int SIZE_WIDTH = ScreenUtils.getScreenWidth();
    private static final int SIZE_HEIGHT = ScreenUtils.getScreenHeight();

    /**
     * 下架商品加水印
     */
    private static Postprocessor status0Postprocessor = new BasePostprocessor() {
        @Override
        public String getName() {
            return "status0Postprocessor";
        }

        @Override
        public void process(Bitmap bitmap) {
            BitmapUtil.waterMaskBitmap(bitmap, R.drawable.ic_postprecessor_status0);
        }
    };

    /**
     * 售罄商品加水印
     */
    private static Postprocessor stock0Postprocessor = new BasePostprocessor() {
        @Override
        public String getName() {
            return "ic_postprecessor_stock0";
        }

        @Override
        public void process(Bitmap bitmap) {
            BitmapUtil.waterMaskBitmap(bitmap, R.drawable.ic_postprecessor_stock0);
        }
    };

    /**
     * 过期商品加水印
     */
    private static Postprocessor overduePostprocessor = new BasePostprocessor() {
        @Override
        public String getName() {
            return "status0Postprocessor";
        }

        @Override
        public void process(Bitmap bitmap) {
            BitmapUtil.waterMaskBitmap(bitmap, R.drawable.ic_postprecessor_overdue);
        }
    };

    /**
     * 默认的加载图片，开启渐进式加载，智能调节压缩比例
     * <p>
     * 尽量不要在 Adapter里面调用，会重复计算宽高的 造成性能问题
     *
     * @param simpleDraweeView
     * @param imageUrl
     */
    public static void setImage(
            final SimpleDraweeView simpleDraweeView,
            final String imageUrl,
            Postprocessor postprocessor
    ) {
        ViewGroup.LayoutParams layoutParams = simpleDraweeView.getLayoutParams();
        if (layoutParams == null || layoutParams.width < 1 || layoutParams.height < 1) {
            simpleDraweeView
                    .getViewTreeObserver()
                    .addOnGlobalLayoutListener(new MyGlobalLayoutListene(simpleDraweeView, imageUrl, postprocessor));
        } else {
            setImage(simpleDraweeView, imageUrl, layoutParams.width, layoutParams.height, postprocessor);
        }
    }

    /**
     * 默认的加载图片，开启渐进式加载，智能调节压缩比例
     * <p>
     * 尽量不要在 Adapter里面调用，会重复计算宽高的 造成性能问题
     *
     * @param simpleDraweeView
     * @param imageUrl
     */
    public static void setImage(final SimpleDraweeView simpleDraweeView, final String imageUrl) {
        setImage(simpleDraweeView, imageUrl, null);
    }

    /**
     * 加载小个的图片 用户加载头像之类的
     * adapter 里建议使用这个
     *
     * @param simpleDraweeView
     * @param imageUrl
     */
    public static void setImageSmall(SimpleDraweeView simpleDraweeView, String imageUrl) {
        setImageSmall(simpleDraweeView, imageUrl, null);
    }

    /**
     * 加载小个的图片 用户加载头像之类的
     * adapter 里建议使用这个
     *
     * @param simpleDraweeView
     * @param imageUrl
     */
    public static void setImageSmall(SimpleDraweeView simpleDraweeView, String imageUrl, Postprocessor postprocessor) {
        ViewGroup.LayoutParams layoutParams = simpleDraweeView.getLayoutParams();
        if (layoutParams == null || layoutParams.width < 1 || layoutParams.height < 1) {
            setImage(simpleDraweeView, imageUrl, ConvertUtils.dp2px(80), ConvertUtils.dp2px(80), postprocessor);
        } else {
            setImage(simpleDraweeView, imageUrl, layoutParams.width, layoutParams.height, postprocessor);
        }
    }

    /**
     * 会自动给图片加上已下架 已抢光水印的方法
     *
     * @param simpleDraweeView
     * @param skuInfo
     */
    public static void setSkuImgMask(SimpleDraweeView simpleDraweeView, SkuInfo skuInfo) {
        String thumb = skuInfo.thumb;
        if (skuInfo.status == 0) {
            setImage(simpleDraweeView, thumb, status0Postprocessor);
        } else if (skuInfo.stock <= 0) {
            setImage(simpleDraweeView, thumb, stock0Postprocessor);
        } else {
            setImage(simpleDraweeView, thumb);
        }
    }

    /**
     * 会自动给图片加上已下架 已抢光水印的方法
     * Adapter中使用
     *
     * @param simpleDraweeView
     * @param skuInfo
     */
    public static void setSkuImgMaskSmall(SimpleDraweeView simpleDraweeView, SkuInfo skuInfo) {
        String thumb = skuInfo.thumb;
        if (skuInfo.status == 0) {
            setImageSmall(simpleDraweeView, thumb, status0Postprocessor);
        } else if (skuInfo.stock <= 0) {
            setImageSmall(simpleDraweeView, thumb, stock0Postprocessor);
        } else {
            setImageSmall(simpleDraweeView, thumb);
        }
    }

    /**
     * 会自动给图片加上已下架 已抢光水印的方法
     *
     * @param simpleDraweeView
     * @param url
     * @param status           状态 如果
     * @param stock            库存
     */
    public static void setImgMask(SimpleDraweeView simpleDraweeView, String url, int status, int stock) {
        if (status == 0) {
            setImage(simpleDraweeView, url, status0Postprocessor);
        } else if (stock <= 0) {
            setImage(simpleDraweeView, url, stock0Postprocessor);
        } else {
            setImage(simpleDraweeView, url);
        }
    }

    /**
     * 会自动给图片加上已下架 已抢光水印的方法
     *
     * @param simpleDraweeView
     * @param skuInfo
     */
    public static void setSkuImgSmallMask(SimpleDraweeView simpleDraweeView, SkuInfo skuInfo) {
        String thumb = skuInfo.thumb;
        if (skuInfo.status == 0) {
            setImageSmall(simpleDraweeView, thumb, status0Postprocessor);
        } else if (skuInfo.stock <= 0) {
            setImageSmall(simpleDraweeView, thumb, stock0Postprocessor);
        } else {
            setImageSmall(simpleDraweeView, thumb);
        }
    }


    /**
     * 会自动给图片加上已下架 已抢光水印的方法
     *
     * @param simpleDraweeView
     * @param thumb
     * @param status           状态 如果
     * @param stock            库存
     */
    public static void setImgMask(SimpleDraweeView simpleDraweeView, String thumb, boolean status, int stock, boolean overdue) {
        if (status) {
            //下架
            setImageSmall(simpleDraweeView, thumb, status0Postprocessor);
        } else if (stock <= 0) {
            setImageSmall(simpleDraweeView, thumb, stock0Postprocessor);
        } else if (overdue) {
            setImageSmall(simpleDraweeView, thumb, overduePostprocessor);
        } else {
            setImageSmall(simpleDraweeView, thumb);
        }
    }

    public static void setImageFixedSize(final SimpleDraweeView simpleDraweeView, final String imageUrl, int widthDp, int heightDp) {
        setImage(simpleDraweeView, imageUrl, ConvertUtils.dp2px(widthDp), ConvertUtils.dp2px(heightDp));
    }

    private static class MyGlobalLayoutListene implements ViewTreeObserver.OnGlobalLayoutListener {

        private SimpleDraweeView mSimpleDraweeView;
        private String mImageUrl;
        private Postprocessor mPostprocessor;

        public MyGlobalLayoutListene(SimpleDraweeView simpleDraweeView, String imageUrl, Postprocessor postprocessor) {
            mSimpleDraweeView = simpleDraweeView;
            mImageUrl = imageUrl;
            mPostprocessor = postprocessor;
        }

        @Override
        public void onGlobalLayout() {
            int imageWidth = mSimpleDraweeView.getWidth();
            int imageHeight = mSimpleDraweeView.getHeight();
            if (imageWidth < 5 || imageHeight < 5) {
                setImage(mSimpleDraweeView, mImageUrl, SIZE_WIDTH, SIZE_HEIGHT, mPostprocessor);
            } else {
                setImage(mSimpleDraweeView, mImageUrl, imageWidth, imageHeight, mPostprocessor);
            }
            mSimpleDraweeView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
        }
    }


    public static void setImage(
            final SimpleDraweeView simpleDraweeView,
            final String imageUrl,
            final int imgWidth,
            final int height) {
        setImage(simpleDraweeView, imageUrl, imgWidth, height, null);
    }

    private static void setImage(
            final SimpleDraweeView simpleDraweeView,
            String imageUrl,
            int imgWidth,
            int imgHeight,
            Postprocessor postprocessor) {
        if (StringUtils.isEmpty(imageUrl)) {
            imageUrl = "https://static.create-chain.net/ccmall/70/c8/99/97eeef15e4d7408a8057e5ba9f68daf2.jpg";
        }

        if (imgWidth > SizeUtils.dp2px(30) || imgHeight > SizeUtils.dp2px(30)) {
            imgWidth = imgWidth * 2 / 3;
            imgHeight = imgHeight * 2 / 3;
        }
        if (StringUtils.isEmpty(imageUrl)) {
            simpleDraweeView.setImageResource(R.drawable.default_image);
            return;
        }
        if (imageUrl.endsWith(".gif")) {
            AbstractDraweeController controller = Fresco
                    .newDraweeControllerBuilder()
                    .setUri(imageUrl)
                    .setAutoPlayAnimations(true)
                    .build();
            simpleDraweeView.setController(controller);
        } else {
            final String finalImageUrl = getOssImageUrl(imageUrl, imgWidth, imgHeight);
//            Timber.INSTANCE.e("新的图片url  " + finalImageUrl);
            ControllerListener controllerListener = new BaseControllerListener<ImageInfo>() {
                @Override
                public void onFinalImageSet(
                        String id,
                        @Nullable ImageInfo imageInfo,
                        @Nullable Animatable anim) {
                    if (imageInfo == null) {
                        return;
                    }
                    QualityInfo qualityInfo = imageInfo.getQualityInfo();
                    FLog.d("Final image received! " +
                                    "Size %d x %d",
                            "Quality level %d, good enough: %s, full quality: %s",
                            imageInfo.getWidth(),
                            imageInfo.getHeight(),
                            qualityInfo.getQuality(),
                            qualityInfo.isOfGoodEnoughQuality(),
                            qualityInfo.isOfFullQuality());
                }

                @Override
                public void onIntermediateImageSet(String id, @Nullable ImageInfo imageInfo) {
                    LogUtils.d("Intermediate image received");
                }

                @Override
                public void onFailure(String id, Throwable throwable) {
                    FLog.e(getClass(), throwable, "Error loading %s", id);
                    LogUtils.e("加载失败" + finalImageUrl);
                }
            };
            try {
                ResizeOptions resizeOptions = getReSizeOptions(imgWidth, imgHeight);
                ImageRequest request = ImageRequestBuilder.newBuilderWithSource(Uri.parse(finalImageUrl))
                        .setProgressiveRenderingEnabled(true)
                        .setResizeOptions(resizeOptions)
                        .setAutoRotateEnabled(true)
                        .setLowestPermittedRequestLevel(ImageRequest.RequestLevel.FULL_FETCH)
                        .setPostprocessor(postprocessor)
                        .build();
                DraweeController controller = Fresco.newDraweeControllerBuilder()
                        .setImageRequest(request)
                        .setOldController(simpleDraweeView.getController())
                        .setRetainImageOnFailure(true)
                        .setTapToRetryEnabled(true)
                        .setControllerListener(controllerListener)
                        .build();
                simpleDraweeView.setController(controller);
            } catch (Exception e) {

            }
        }
    }

    private static ResizeOptions getReSizeOptions(int imgWidth, int imgHeight) {
        if (imgWidth == 0 || imgHeight == 0) {
            return null;
        }
        return new ResizeOptions(imgWidth, imgHeight);
    }

    private static String getOssImageUrl(String imageUrl, int imgWidth, int height) {
        if (!imageUrl.contains("http://static")) {
            return imageUrl;
        } else {
            return String.format("%s?x-oss-process=image/resize,m_fill,w_%d,h_%d/quality,Q_85", imageUrl, imgWidth, height);
        }
    }


    public static GenericDraweeHierarchy getGenericDraweeHierarchy(Context context) {
        return new GenericDraweeHierarchyBuilder(context.getResources())
                .setPlaceholderImage(R.drawable.default_image)
                .setPlaceholderImageScaleType(ScalingUtils.ScaleType.FIT_XY)
                .setActualImageScaleType(ScalingUtils.ScaleType.FIT_XY)
                .build();
    }

    public static void loadRvItemImg(BaseViewHolder helper, int imgViewID, String imgUrl) {
        SimpleDraweeView simpleDraweeView = helper.getView(imgViewID);
        setImageSmall(simpleDraweeView, imgUrl);
    }

    public static void loadRvItemImgMask(BaseViewHolder helper, int imgViewID, SkuInfo skuInfo) {
        SimpleDraweeView simpleDraweeView = helper.getView(imgViewID);
        setSkuImgSmallMask(simpleDraweeView, skuInfo);
    }
}
