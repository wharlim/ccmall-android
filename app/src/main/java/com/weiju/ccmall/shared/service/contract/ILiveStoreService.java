package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.module.live.entity.AlipayAccountEntity;
import com.weiju.ccmall.module.live.entity.ApplyHeadEntity;
import com.weiju.ccmall.module.live.entity.ApplyInfoEntity;
import com.weiju.ccmall.module.live.entity.DepositEntity;
import com.weiju.ccmall.module.live.entity.LiveStoreFreightEntity;
import com.weiju.ccmall.module.live.entity.ProductEntity;
import com.weiju.ccmall.module.live.entity.SettleAccountEntity;
import com.weiju.ccmall.module.live.entity.StoreHomeEntity;
import com.weiju.ccmall.module.live.entity.UploadProductEntity;
import com.weiju.ccmall.module.live.entity.VerifyStatusEntity;
import com.weiju.ccmall.newRetail.bean.ActiveValueRule;
import com.weiju.ccmall.shared.bean.Address;
import com.weiju.ccmall.shared.bean.DealDetail;
import com.weiju.ccmall.shared.bean.StoreFreight;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * @author Ben
 * @date 2020/4/16.
 */
public interface ILiveStoreService {

    @GET("liveStore/getVerifyStatus")
    Observable<RequestResult<VerifyStatusEntity>> getVerifyStatus();

    @GET("liveStore/getTreaty")
    Observable<RequestResult<ActiveValueRule>> getTreaty();

    @GET("liveStore/checkApplyField")
    Observable<RequestResult<Object>> checkApplyField(@Query("storeName") String storeName, @Query("provinceName") String provinceName,
                                                      @Query("cityName") String cityName, @Query("districtName") String districtName,
                                                      @Query("addr") String addr, @Query("phone") String phone, @Query("mail") String mail);

    @FormUrlEncoded
    @POST("liveStore/signApply")
    Observable<RequestResult<ApplyInfoEntity>> signApply(@FieldMap HashMap<String, Object> params);

    @GET("liveStore/payLiveStoreDeposit")
    Observable<RequestResult<DepositEntity>> payLiveStoreDeposit();

    @GET("liveStore/getApplyInfo")
    Observable<RequestResult<ApplyHeadEntity>> getApplyInfo(@Query("mchInId") String mchInId);

    @GET("liveStore/checkStoreExist")
    Observable<RequestResult<ApplyInfoEntity>> checkStoreExist();

    @GET("liveStore/getSuccessInfo")
    Observable<RequestResult<ActiveValueRule>> getSuccessInfo();


    @GET("liveStore/getStoreFreight")
    Observable<RequestResult<LiveStoreFreightEntity>> getStoreFreight();

    @POST("liveStore/saveStoreFreightBatch")
    Observable<RequestResult<Object>> saveStoreFreightBatch(@Body List<StoreFreight> body);

    @GET("liveStore/homePage")
    Observable<RequestResult<StoreHomeEntity>> homePage();

    @POST("liveStorePro/saveProduct")
    Observable<RequestResult<Object>> saveProduct(@Body UploadProductEntity entity);

    @GET("liveStorePro/getProList")
    Observable<RequestResult<PaginationEntity<ProductEntity, Object>>> getProList(@Query("status") int status,
                                                                                  @Query("pageOffset") int page, @Query("pageSize") int pageSize);
    @FormUrlEncoded
    @POST("liveStorePro/delProduct")
    Observable<RequestResult<Object>> delProduct(@Field("productId") String productId);

    @FormUrlEncoded
    @POST("liveStorePro/upProductStatus")
    Observable<RequestResult<Object>> upProductStatus(@Field("pushStatus") int pushStatus, @Field("productId") String productId);

    @GET("liveStorePro/getProductInfo")
    Observable<RequestResult<UploadProductEntity>> getProductInfo(@Query("productId") String productId);

    @POST("liveStorePro/updateProduct")
    Observable<RequestResult<Object>> updateProduct(@Body UploadProductEntity entity);

    @GET("liveStorePro/getComRatioSectionInfo")
    Observable<RequestResult<UploadProductEntity>> getComRatioSectionInfo();

    @GET("liveStore/getSettleAccounts")
    Observable<RequestResult<PaginationEntity<SettleAccountEntity, StoreHomeEntity>>> getSettleAccounts(@Query("pageOffset") int page,
                                                                                                        @Query("pageSize") int pageSize);
    @GET("liveStore/getBalanceStream")
    Observable<RequestResult<PaginationEntity<SettleAccountEntity, StoreHomeEntity>>> getBalanceStream(@Query("type") int type, @Query("pageOffset") int page,
                                                                                                       @Query("pageSize") int pageSize);

    @GET("deal/checkAlipayAccount")
    Observable<RequestResult<AlipayAccountEntity>> checkAlipayAccount();

    @GET("deal/getAlipayAccount")
    Observable<RequestResult<AlipayAccountEntity>> getAlipayAccount(@Query("alipayUser") String alipayUser, @Query("alipayAccount") String alipayAccount);

    @FormUrlEncoded
    @POST("deal/addLiveStore")
    Observable<RequestResult<Object>> addLiveStore(@Field("applyAccountId") String applyAccountId, @Field("applyMoney") long applyMoney,
                                                   @Field("password") String password, @Field("checkNumber") String checkNumber);

    @GET("liveStore/getCashOutDetail")
    Observable<RequestResult<DealDetail>> getCashOutDetail(@Query("dealId") String dealId);

    @GET("liveStoreAddr/getDefault")
    Observable<RequestResult<Address>> getDefault();

    @GET("liveStoreAddr/getList")
    Observable<RequestResult<PaginationEntity<Address, Object>>> getList(@Query("pageOffset") int page,
                                                                         @Query("pageSize") int pageSize);

    @DELETE("liveStoreAddr/delAddr/{id}")
    Observable<RequestResult<Object>> delAddr(@Path("id") String addressId);

    @GET("liveStoreAddr/getAddr/{id}")
    Observable<RequestResult<Address>> getAddr(@Path("id") String addressId);

    @FormUrlEncoded
    @POST("liveStoreAddr/editAddr/{id}")
    Observable<RequestResult<Object>> editAddr(@Path("id") String addressId, @FieldMap HashMap<String, Object> params);

    @FormUrlEncoded
    @POST("liveStoreAddr/addAddr")
    Observable<RequestResult<Address>> addAddr(@FieldMap HashMap<String, Object> params);

}
