package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Tag;
import com.weiju.ccmall.shared.util.ConvertUtil;

import java.util.List;

public class TagViews extends FlowLayout {
    public TagViews(Context context) {
        super(context);
    }

    public TagViews(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setData(List<Tag> tags) {
        int padding = ConvertUtil.dip2px(3);
        int padingBottom = 0;
        padingBottom = tags != null && tags.size() > 0 ? SizeUtils.dp2px(10) : 0;
        int dividers = SizeUtils.dp2px(5);
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, dividers, 0);

        clearAllViews();
        if (tags != null) {
            for (Tag tag : tags) {
                TextView textView = new TextView(getContext());
                textView.setLayoutParams(layoutParams);
                textView.setTextSize(12);
                textView.setTextColor(getResources().getColor(R.color.white));
                textView.setBackgroundResource(R.drawable.btn_bg_red);
                textView.setText(tag.name);
                textView.setPadding(padding, 0, padding, 0);
                addView(textView);
            }
        }
        setPadding(0, 0, 0, padingBottom);
    }
}
