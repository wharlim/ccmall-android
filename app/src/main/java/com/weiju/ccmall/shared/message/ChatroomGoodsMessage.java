package com.weiju.ccmall.shared.message;

import android.annotation.SuppressLint;
import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

@SuppressLint("ParcelCreator")
@MessageTag(value = "CCM:Chatroom:Goods:Explain", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class ChatroomGoodsMessage extends MessageContent {

    public ChatroomGoodsMessage() {
    }

    private String skuId;

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("skuId", skuId);
        } catch (JSONException e) {
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ChatroomGoodsMessage(byte[] data) {
        String jsonStr = null;

        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e1) {

        }

        try {
            JSONObject jsonObj = new JSONObject(jsonStr);

            if (jsonObj.has("redId")) {
                skuId = jsonObj.optString("redId");
                setSkuIdId(skuId);
            }
        } catch (JSONException e) {
        }

    }

    public void setSkuIdId(String skuId) {
        this.skuId = skuId;
    }

    public String getSkuId() {
        return skuId;
    }

    //给消息赋值。
    public ChatroomGoodsMessage(Parcel in) {
        skuId = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        //这里可继续增加你消息的属性
//        setRedId(redId);
    }

    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<ChatroomGoodsMessage> CREATOR = new Creator<ChatroomGoodsMessage>() {

        @Override
        public ChatroomGoodsMessage createFromParcel(Parcel source) {
            return new ChatroomGoodsMessage(source);
        }

        @Override
        public ChatroomGoodsMessage[] newArray(int size) {
            return new ChatroomGoodsMessage[size];
        }
    };

    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     *
     * @return 一个标志位，表明Parcelable对象特殊对象类型集合的排列。
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * 将类的数据写入外部提供的 Parcel 中。
     *
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, skuId);//该类为工具类，对消息中属性进行序列化
        //这里可继续增加你消息的属性
    }
}
