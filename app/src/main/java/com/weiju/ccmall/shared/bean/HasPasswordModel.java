package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/12/13.
 */
public class HasPasswordModel {
    /**
     * status : true
     * code : 1
     * msg : 有密码
     * data :
     */

    @SerializedName("status")
    public boolean status;
    @SerializedName("code")
    public String code;
    @SerializedName("msg")
    public String msg;
    @SerializedName("data")
    public String data;
}
