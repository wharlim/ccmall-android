package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ExpressContentView extends LinearLayout {

    public ExpressContentView(Context context) {
        super(context);
        setOrientation(LinearLayout.VERTICAL);
    }

    public void setData(String[] strs) {
        for (String str : strs) {
            TextView textView = new TextView(getContext());
            textView.setText(str);
            addView(textView);
        }
    }
}
