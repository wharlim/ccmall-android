package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * @author chenyanming
 * @time 2019/4/29 on 14:25
 * @desc ${TODD}
 */
public class ExchRatioTrend {
    /**
     * day : 20190410
     * dayRatio : 102
     */

    @SerializedName("day")
    public String day;
    @SerializedName("dayRatio")
    public long dayRatio;
}
