package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
public class LiveCouponListNoDataDialog extends Dialog {

    public LiveCouponListNoDataDialog(@NonNull Context context) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_coupon_list_nodata);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.ivClose, R.id.vClose})
    protected void close() {
        dismiss();
    }

    @Override
    public void dismiss() {
        EventBus.getDefault().post(new EventMessage(Event.couponDialogDismiss));
        super.dismiss();
    }
}
