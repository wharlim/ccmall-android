package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenyanming
 * @time 2019/8/5 on 14:27
 * @desc ${TODD}
 */
public class OrderVouchers {
    @SerializedName("list")
    public ArrayList<PickUp> list;
    @SerializedName("totalBean")
    public PickUpEx totalBean;
}
