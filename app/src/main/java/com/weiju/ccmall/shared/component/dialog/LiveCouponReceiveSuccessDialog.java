package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.blankj.utilcode.utils.ToastUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.shop.ShopActivity;
import com.weiju.ccmall.shared.bean.CouponReceive;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

import org.greenrobot.eventbus.EventBus;

import java.text.MessageFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
public class LiveCouponReceiveSuccessDialog extends Dialog {


    @BindView(R.id.tvMoney)
    TextView mTvMoney;
    @BindView(R.id.tvTitle)
    TextView mTvTitle;
    @BindView(R.id.tvDate)
    TextView mTvDate;
    @BindView(R.id.tvAuto)
    TextView tvAuto;

    private CouponReceive mCouponReceive;
    private Context mContext;

    public LiveCouponReceiveSuccessDialog(@NonNull Context context, CouponReceive couponReceive) {
        super(context, R.style.Theme_Light_Dialog);
        mCouponReceive = couponReceive;
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_coupon_receive_success);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        if (null != mCouponReceive) {
            mTvTitle.setText(mCouponReceive.couponTitle);
            mTvMoney.setText(MoneyUtil.centToYuanStrNoZero(mCouponReceive.cost));
            mTvDate.setText(String.format("有效期：%s", mCouponReceive.limitEndDate));
            Countdown();
        }
    }

    private int time = 3;

    private void Countdown() {
        tvAuto.postDelayed(() -> {
            time -= 1;
            if (time == 0) {
                viewCouponReceiveDetail();
            } else {
                tvAuto.setText(MessageFormat.format("已存入会员中心-优惠券，{0}秒后自动跳转购买", time));
                Countdown();
            }
        }, 1000);
    }

    @OnClick({R.id.ivClose})
    protected void close() {
        dismiss();
    }

    @Override
    public void dismiss() {
        EventBus.getDefault().post(new EventMessage(Event.couponDialogDismiss));
        super.dismiss();
    }

    @OnClick(R.id.ivImmediateUse)
    public void onViewClicked() {
        viewCouponReceiveDetail();
    }

    private void viewCouponReceiveDetail() {
        Date now = new Date();
        if (mCouponReceive == null || mCouponReceive.limitStartDate == null) {
            // 优惠券数据异常
        } else if (now.before(mCouponReceive.limitStartDate)) {
            ToastUtils.showShortToast("该优惠券还未到使用时间");
        } else if (mCouponReceive.couponType == 1) { // 店铺优惠券
            EventBus.getDefault().post(new EventMessage(Event.openNewActivity));
            ShopActivity.start(mContext, mCouponReceive.storeId, true);
        } else if (mCouponReceive.couponType == 0) { // 产品类型优惠券
            EventBus.getDefault().post(new EventMessage(Event.openNewActivity));
            EventUtil.viewProductDetail(mContext, mCouponReceive.skuId, false, "", mCouponReceive.liveId);
        }
        dismiss();
    }
}
