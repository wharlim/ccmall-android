package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Chan on 2017/6/16.
 */

public class CommonExtra implements Serializable {

    @SerializedName("profitSumMoney")
    public long profitSumMoney;
    @SerializedName("freezeSumMoney")
    public long freezeSumMoney;
    @SerializedName("availableMoney")
    public long availableMoney;
    @SerializedName("goldenAvlTicket")
    public long goldenAvlTicket;


    /**
     * memberId : 34a561b32c664348bb88e0fb04647b25
     * profitSumCoin : 0
     * freezeSumCoin : 0
     * unfreezeSumCoin : 0
     * availableCoin : 799900
     * indexNumber : 0
     * sumTicket : 800000
     * todayUnfreezeCoin : 0
     */

    @SerializedName("memberId")
    public String memberId;
    @SerializedName("profitSumCoin")
    public long profitSumCoin;
    @SerializedName("freezeSumCoin")
    public long freezeSumCoin;
    @SerializedName("unfreezeSumCoin")
    public long unfreezeSumCoin;
    @SerializedName("availableCoin")
    public long availableCoin;
    @SerializedName("indexNumber")
    public long indexNumber;
    @SerializedName("sumTicket")
    public long sumTicket;
    @SerializedName("todayUnfreezeCoin")
    public long todayUnfreezeCoin;


    /**
     * profitSumGold : 0
     * freezeSumGold : 0
     * unfreezeSumGold : 0
     * availableGold : 99980000
     */

    @SerializedName("profitSumGold")
    public long profitSumGold;
    @SerializedName("freezeSumGold")
    public long freezeSumGold;
    @SerializedName("unfreezeSumGold")
    public long unfreezeSumGold;
    @SerializedName("availableGold")
    public long availableGold;

    /**
     *  psp,tsp,pcp
     */
    @SerializedName("totalPoint")
    public long totalPoint;
}
