package com.weiju.ccmall.shared.util;

import com.bigkoo.convenientbanner.ConvenientBanner;
import com.bigkoo.convenientbanner.holder.CBViewHolderCreator;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.common.CarouselItemViewHolder;

import java.util.List;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.util
 * @since 2017-06-09
 */
public class CarouselUtil {

    public static <T> void initCarousel(ConvenientBanner convenientBanner, List<T> data,int page_indicator_focused) {
        convenientBanner.setPages(new CBViewHolderCreator<CarouselItemViewHolder<T>>() {
            @Override
            public CarouselItemViewHolder<T> createHolder() {
                return new CarouselItemViewHolder<>();
            }
        }, data);

        convenientBanner.setPageIndicator(new int[]{R.drawable.icon_page_indicator,page_indicator_focused})
                .setPageIndicatorAlign(ConvenientBanner.PageIndicatorAlign.CENTER_HORIZONTAL);
        convenientBanner.startTurning(5000);
    }
}
