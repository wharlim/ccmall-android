package com.weiju.ccmall.shared.component.adapter;

import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.CouponReceive;
import com.weiju.ccmall.shared.util.FrescoUtil;

import java.util.List;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
public class LiveCouponReceiveDetailAdapter extends BaseQuickAdapter<CouponReceive, BaseViewHolder> {



    public LiveCouponReceiveDetailAdapter() {
        super(R.layout.item_live_coupon_receive);
    }

    @Override
    protected void convert(BaseViewHolder helper, CouponReceive item) {
        SimpleDraweeView view = helper.getView(R.id.ivAvatar);
        FrescoUtil.setImage(view, item.headImage);
        helper.setText(R.id.tvName, item.nickName);
    }
}
