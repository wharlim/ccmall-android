package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.entity.SpecEntity;
import com.weiju.ccmall.shared.component.DecimalEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanmin
 * @date 2019/6/22
 * @desc
 */
public class SettingSpecDialog extends Dialog {

    protected OnConfirmListener mConfirmListener;
    @BindView(R.id.etSellingPrice)
    DecimalEditText mEtSellingPrice;
    @BindView(R.id.etMarketPrice)
    DecimalEditText mEtMarketPrice;
    @BindView(R.id.etStock)
    EditText mEtStock;
    @BindView(R.id.etWeight)
    DecimalEditText mEtWeight;
    @BindView(R.id.cancelBtn)
    TextView mCancelBtn;
    @BindView(R.id.confirmBtn)
    TextView mConfirmBtn;

    public SettingSpecDialog(Context context) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_setting_spec);
        ButterKnife.bind(this);
    }

    public void setOnConfirmListener(@NonNull OnConfirmListener listener) {
        mConfirmListener = listener;
    }

    @OnClick(R.id.cancelBtn)
    protected void onClose(View view) {
        dismiss();
    }

    @OnClick(R.id.confirmBtn)
    protected void onConfirm(View view) {
        if (mConfirmListener != null) {
            SpecEntity entity = new SpecEntity();
            String sellingPrice = mEtSellingPrice.getText().toString();
            String marketPrice = mEtMarketPrice.getText().toString();
            String stock = mEtStock.getText().toString();
            String weight = mEtWeight.getText().toString();
            entity.setRetailPrice(sellingPrice);
            entity.setMarketPrice(marketPrice);
            entity.setStock(stock);
            entity.setWeight(weight);

            mConfirmListener.confirm(entity);
        }
        dismiss();
    }

    public interface OnConfirmListener {
        void confirm(SpecEntity entity);
    }
}
