package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.common.AdvancedCountdownTimer;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CountDownGray extends LinearLayout {

    @BindView(R.id.tvDays)
    TextView tvDays;
    @BindView(R.id.tvHours)
    TextView mTvHours;
    @BindView(R.id.tvMinutes)
    TextView mTvMinutes;
    @BindView(R.id.tvSeconds)
    TextView mTvSeconds;
    @BindView(R.id.tvDayText)
    TextView mTvDayText;
    private AdvancedCountdownTimer mCountdownTimer;
    private boolean isFrame = false;

    public CountDownGray(Context context) {
        super(context);
        initViews();
    }

    public CountDownGray(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CountDownGray);
        isFrame = typedArray.getBoolean(R.styleable.CountDownGray_cd_gray_frame, false);
        typedArray.recycle();
        initViews();
    }

    private void initViews() {
        View view;
        if (isFrame) {
            view = inflate(getContext(), R.layout.cmp_count_down_red_layout, this);
        } else {
            view = inflate(getContext(), R.layout.cmp_count_down_gray_layout, this);
        }

        ButterKnife.bind(this, view);
    }

    public void setTimeLeft(long time, @Nullable final OnFinishListener listener) {
        mCountdownTimer = new AdvancedCountdownTimer(time, 1000) {

            @Override
            public void onTick(long millisUntilFinished, int percent) {
                millisUntilFinished /= 1000;
                long days = millisUntilFinished / 3600 / 24;
                long hours = millisUntilFinished / 3600 % 24;
                long minutes = millisUntilFinished / 60 % 60;
                long seconds = millisUntilFinished % 60;
//                if (days > 0) {
//                    hours += days * 24;
//                }
                tvDays.setVisibility(isFrame && days == 0 ? GONE : VISIBLE);
                mTvDayText.setVisibility(isFrame && days == 0 ? GONE : VISIBLE);
                tvDays.setText(String.format(Locale.getDefault(), "%02d", days));
                mTvHours.setText(String.format(Locale.getDefault(), "%02d", hours));
                mTvMinutes.setText(String.format(Locale.getDefault(), "%02d", minutes));
                mTvSeconds.setText(String.format(Locale.getDefault(), "%02d", seconds));
            }

            @Override
            public void onFinish() {
                if (null != listener) {
                    listener.onFinish();
                }
            }
        };

        mCountdownTimer.start();
    }

    @Override
    protected void onDetachedFromWindow() {
        if (mCountdownTimer != null) {
            mCountdownTimer.cancel();
            mCountdownTimer = null;
        }
        super.onDetachedFromWindow();
    }

    public interface OnFinishListener {
        void onFinish();
    }

}
