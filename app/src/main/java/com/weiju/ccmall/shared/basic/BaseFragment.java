package com.weiju.ccmall.shared.basic;


import android.content.Intent;
import android.support.v4.app.Fragment;

import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.user.LoginActivity;
import com.weiju.ccmall.shared.bean.event.MsgMain;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

public class BaseFragment extends Fragment {

    protected CharSequence title;

    public void setTitle(CharSequence title) {
        this.title = title;
    }

    public void setTitle(int titleId) {
        setTitle(getResources().getString(titleId));
    }

    public CharSequence getTitle() {
        return this.title;
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onResume() {
        super.onResume();


    }

    protected boolean isNeedLogin() {
        return false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (isNeedLogin() && !SessionUtil.getInstance().isLogin()) {
                EventBus.getDefault().post(new MsgMain(MsgMain.SELECT_HOME));

                startActivity(new Intent(MyApplication.getContext(), LoginActivity.class));
                ToastUtil.error(Config.NET_MESSAGE.NO_LOGIN);
            }
        }
    }
}
