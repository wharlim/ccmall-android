package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveRedEnvelopes;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.LiveRedLoadMoreView;
import com.weiju.ccmall.shared.component.adapter.LiveRedListAdapter;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2020/1/7 on 14:42
 * @desc
 */
public class LiveRedListDialog extends Dialog {

    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;

    private String mLiveId;
    private Context mContext;
    private LiveRedListAdapter mLiveRedListAdapter = new LiveRedListAdapter();
    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);

    public final static int PUSH = 0;
    public final static int PLAY = 1;
    private int mType;


    private boolean mDialogDismiss = true;
    private LiveUser mLiveUser;

    public LiveRedListDialog(Context context, String liveId, int type, LiveUser liveUser) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mLiveId = liveId;
        mType = type;
        mLiveUser = liveUser;
    }


    private int mCurrentPage = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_red_list);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    private void initData() {
        getLiveRedEnvelopesList();
    }

    private void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mLiveRedListAdapter);
        mRecyclerView.addItemDecoration(new ListDividerDecoration(mContext));

        View inflate = View.inflate(mContext, R.layout.cmp_no_data, null);
        inflate.findViewById(R.id.noDataLabel).setVisibility(View.GONE);
        ImageView viewById = inflate.findViewById(R.id.noDataIcon);
        viewById.setImageResource(R.drawable.img_no_red);
        mLiveRedListAdapter.setEmptyView(inflate);
        mLiveRedListAdapter.setType(mType);

        mLiveRedListAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentPage++;
                getLiveRedEnvelopesList();
            }
        }, mRecyclerView);

        mLiveRedListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                LiveRedEnvelopes item = mLiveRedListAdapter.getItem(position);
                if (null == item) {
                    return;
                }

                if (mType == PUSH) {
                    LiveRedDetailDialog liveRedDetailDialog = new LiveRedDetailDialog(mContext, mLiveId, item.redEnvelopesId, mType,mLiveUser);
                    liveRedDetailDialog.show();
                    dismiss();
                } else {
                    mDialogDismiss = false;
                    if (item.receiveStatus == 0 && item.status == 0) {
                        //抢红包
                        LiveRedOpenDialog liveRedOpenDialog = new LiveRedOpenDialog(mContext, mLiveId, item.redEnvelopesId,mLiveUser);
                        liveRedOpenDialog.show();
                    } else if (item.receiveStatus == 1 && item.receiveMoney > 0) {
                        //查看详情
                        LiveRedMoneyDetailDialog liveRedMoneyDetailDialog = new LiveRedMoneyDetailDialog(mContext, mLiveId, item.redEnvelopesId,mLiveUser);
                        liveRedMoneyDetailDialog.show();
                    } else {
                        LiveRedDetailDialog liveRedMoneyDetailDialog = new LiveRedDetailDialog(mContext, mLiveId, item.redEnvelopesId, mType,mLiveUser);
                        liveRedMoneyDetailDialog.show();
                    }
                    dismiss();
                }
            }
        });

        mLiveRedListAdapter.setHeaderAndEmpty(true);
        mLiveRedListAdapter.setFooterViewAsFlow(true);
        mLiveRedListAdapter. setLoadMoreView(new LiveRedLoadMoreView());

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCurrentPage = 1;
                getLiveRedEnvelopesList();
            }
        });
    }

    private void getLiveRedEnvelopesList() {
        APIManager.startRequest(mILiveService.getLiveRedEnvelopesList(mLiveId, mCurrentPage, 20),
                new BaseRequestListener<PaginationEntity<LiveRedEnvelopes, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<LiveRedEnvelopes, Object> result) {
                        if (mCurrentPage == 1) {
                            mLiveRedListAdapter.setNewData(getLiveRedEnvelopes(result.list));
                        } else {
                            mLiveRedListAdapter.addData(getLiveRedEnvelopes(result.list));
                        }
                        if (result.page >= result.totalPage) {
                            mLiveRedListAdapter.loadMoreEnd();
                        } else {
                            mLiveRedListAdapter.loadMoreComplete();
                        }

                    }
                }, mContext);
    }

    private List<LiveRedEnvelopes> getLiveRedEnvelopes(List<LiveRedEnvelopes> liveRedEnvelopes) {
        for (LiveRedEnvelopes liveRedEnvelope : liveRedEnvelopes) {
            liveRedEnvelope.createDate = TimeUtils.date2String(TimeUtils.string2Date(liveRedEnvelope.createDate), "HH:mm");
        }
        return liveRedEnvelopes;
    }


    @Override
    public void dismiss() {
        if (mDialogDismiss) {
            EventBus.getDefault().post(new EventMessage(Event.dialogDismiss));
        }
        super.dismiss();
    }

    @OnClick(R.id.ivClose)
    protected void onClose(View view) {
        dismiss();
    }

}
