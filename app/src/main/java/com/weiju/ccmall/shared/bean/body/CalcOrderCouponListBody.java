package com.weiju.ccmall.shared.bean.body;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.bean.SkuAmount;

import java.io.Serializable;
import java.util.List;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/12/5.
 */
public class CalcOrderCouponListBody implements Serializable {
    /**
     * products : [{"skuId":"3d5727dc23624c8b85aa42b91d1ccc25","quantity":100},{"skuId":"a61107919b07431db677151301ed3441","quantity":1}]
     */
    @SerializedName("products")
    public List<SkuAmount> products;
}
