package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class OpeningLiveSearchResult {

    /**
     * memberId : 2a09557ef20509458320774cb073efe1
     * inviteCode : 369250265
     * headImage : https://static.create-chain.net/ccmall/4a/8a/e3/e58db07f226b478ca110edad337173c1.jpg
     * phone : 15625435846
     * nickName : dddddd
     * wechat :
     * inviteMemberType : 0
     * inviteMemberTypeStr :
     * xwsWechatOpenId :
     * wechatUnionId : olO1T53P-VZrBjmtmiNBkDwsnnYI
     * isStore : 0
     * userName : 黄华
     * qq :
     * identityCard : 452502199303285231
     * memberType : 0
     * memberTypeStr :
     * authStatus : 2
     * authStatusStr : 认证成功
     * signinStatus : 0
     * storeMemberId :
     * storeType : 0
     * storeTypeStr :
     * vipType : 9
     * vipTypeStr : 旗舰店
     * vipMemberXwsStatus : 0
     * xdsWechatOpenId : ozJM56PWCEb3Yif2aB6r41tasMkM
     * hasDeposit : 0
     * hidePrestoredStatus : 0
     * zhAuthStatus : 1
     * zhTeamCount : 0
     * autoAuthStatus : 0
     * countryCode : +86
     * authorizeCcmDeduction : 1
     * ccmTransferChainActivate : 0
     * password : $2a$10$LOifuViAnPdytUc/SU1TDOTOchAFLruPjW/hAdj/E7Cn6P8avQwT2
     * salt :
     * freezeType : 0
     * freezeDate :
     * freezeReason :
     * referrerMemberId : af8d785b384ad9281286275095141f15
     * createDate : 2018-10-26 10:15:19
     * deleteFlag : 0
     * proxyMemberId :
     * hasDepositStr :
     * levelStr :
     * source : 0
     * noteName :
     */

    @SerializedName("memberId")
    public String memberId;
    @SerializedName("inviteCode")
    public int inviteCode;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("phone")
    public String phone;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("wechat")
    public String wechat;
    @SerializedName("inviteMemberType")
    public int inviteMemberType;
    @SerializedName("inviteMemberTypeStr")
    public String inviteMemberTypeStr;
    @SerializedName("xwsWechatOpenId")
    public String xwsWechatOpenId;
    @SerializedName("wechatUnionId")
    public String wechatUnionId;
    @SerializedName("isStore")
    public int isStore;
    @SerializedName("userName")
    public String userName;
    @SerializedName("qq")
    public String qq;
    @SerializedName("identityCard")
    public String identityCard;
    @SerializedName("memberType")
    public int memberType;
    @SerializedName("memberTypeStr")
    public String memberTypeStr;
    @SerializedName("authStatus")
    public int authStatus;
    @SerializedName("authStatusStr")
    public String authStatusStr;
    @SerializedName("signinStatus")
    public int signinStatus;
    @SerializedName("storeMemberId")
    public String storeMemberId;
    @SerializedName("storeType")
    public int storeType;
    @SerializedName("storeTypeStr")
    public String storeTypeStr;
    @SerializedName("vipType")
    public int vipType;
    @SerializedName("vipTypeStr")
    public String vipTypeStr;
    @SerializedName("vipMemberXwsStatus")
    public int vipMemberXwsStatus;
    @SerializedName("xdsWechatOpenId")
    public String xdsWechatOpenId;
    @SerializedName("hasDeposit")
    public int hasDeposit;
    @SerializedName("hidePrestoredStatus")
    public int hidePrestoredStatus;
    @SerializedName("zhAuthStatus")
    public int zhAuthStatus;
    @SerializedName("zhTeamCount")
    public int zhTeamCount;
    @SerializedName("autoAuthStatus")
    public int autoAuthStatus;
    @SerializedName("countryCode")
    public String countryCode;
    @SerializedName("authorizeCcmDeduction")
    public int authorizeCcmDeduction;
    @SerializedName("ccmTransferChainActivate")
    public int ccmTransferChainActivate;
    @SerializedName("password")
    public String password;
    @SerializedName("salt")
    public String salt;
    @SerializedName("freezeType")
    public int freezeType;
    @SerializedName("freezeDate")
    public String freezeDate;
    @SerializedName("freezeReason")
    public String freezeReason;
    @SerializedName("referrerMemberId")
    public String referrerMemberId;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
    @SerializedName("proxyMemberId")
    public String proxyMemberId;
    @SerializedName("hasDepositStr")
    public String hasDepositStr;
    @SerializedName("levelStr")
    public String levelStr;
    @SerializedName("source")
    public int source;
    @SerializedName("noteName")
    public String noteName;
}
