package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveRedEnvelopes;
import com.weiju.ccmall.shared.bean.LiveRedEnvelopesEx;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.LiveRedLoadMoreView;
import com.weiju.ccmall.shared.component.adapter.LiveRedDetailAdapter;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.SessionUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2020/1/7 on 14:42
 * @desc
 */
public class LiveRedDetailDialog extends Dialog {

    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.ivAvatar)
    SimpleDraweeView mIvAvatar;
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.tvRedNum)
    TextView mTvRedNum;

    private String mLiveId;
    private Context mContext;
    private LiveRedDetailAdapter mLiveRedListAdapter = new LiveRedDetailAdapter();
    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);
    private String mRedEnvelopesId;
    private int mType;

    private LiveUser mLiverUser;

    public LiveRedDetailDialog(Context context, String liveId, String redEnvelopesId, int type, LiveUser liveUser) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mLiveId = liveId;
        mRedEnvelopesId = redEnvelopesId;
        mType = type;
        mLiverUser = liveUser;
    }


    private int mCurrentPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_red_list_detail);
        ButterKnife.bind(this);
        initView();
        initData();
    }

    private void initData() {
        FrescoUtil.setImage(mIvAvatar,mLiverUser.headImage);
        mTvName.setText(mLiverUser.nickName);
        getLiveRedEnvelopesList();
    }

    private void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mLiveRedListAdapter);
        mRecyclerView.addItemDecoration(new ListDividerDecoration(mContext));


        mLiveRedListAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentPage++;
                getLiveRedEnvelopesList();
            }
        }, mRecyclerView);

        mLiveRedListAdapter.setHeaderAndEmpty(true);
        mLiveRedListAdapter.setFooterViewAsFlow(true);
        mLiveRedListAdapter.setLoadMoreView(new LiveRedLoadMoreView());

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCurrentPage = 1;
                getLiveRedEnvelopesList();
            }
        });
    }

    private void getLiveRedEnvelopesList() {
        APIManager.startRequest(mILiveService.getLiveRedEnvelopesDetailList(mLiveId, mRedEnvelopesId, mCurrentPage, 20),
                new BaseRequestListener<PaginationEntity<LiveRedEnvelopes, LiveRedEnvelopesEx>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<LiveRedEnvelopes, LiveRedEnvelopesEx> result) {
                        if (mCurrentPage == 1) {
                            mLiveRedListAdapter.setNewData(getLiveRedEnvelopes(result.list));
                        } else {
                            mLiveRedListAdapter.addData(getLiveRedEnvelopes(result.list));
                        }
                        if (result.page >= result.totalPage) {
                            mLiveRedListAdapter.loadMoreEnd();
                        } else {
                            mLiveRedListAdapter.loadMoreComplete();
                        }
                        if (null == result.ex.redEnvelopesSend) {
                            mTvRedNum.setText("已领取0/0个，共0.00/0元");
                            return;
                        }

                        if (mLiveRedListAdapter.getData().size() == 0) {
                            mTvRedNum.setText(String.format("已领取0/%s个，共0.00/%s元",
                                    result.ex.redEnvelopesSend.sendNum,
                                    MoneyUtil.centToYuanStrNoZero(result.ex.redEnvelopesSend.sendMoney)));

                            return;
                        }
                        if (result.ex.redEnvelopesSend.sendNum == result.ex.redEnvelopesSend.receiveNum) {
                            if (mType == LiveRedListDialog.PUSH) {
                                mTvRedNum.setText(String.format("%s个红包共%s元，已被抢完", result.ex.redEnvelopesSend.receiveNum,
                                        MoneyUtil.centToYuanStrNoZero(result.ex.redEnvelopesSend.sendMoney)));
                            } else {
                                mTvRedNum.setText(String.format("%s个红包已被抢完", result.ex.redEnvelopesSend.receiveNum));
                            }
                        } else {
                            mTvRedNum.setText(String.format("已领取%s/%s个，共%s/%s元", result.ex.redEnvelopesSend.receiveNum,
                                    result.ex.redEnvelopesSend.sendNum, MoneyUtil.centToYuanStrNoZero(result.ex.redEnvelopesSend.receiveMoney),
                                    MoneyUtil.centToYuanStrNoZero(result.ex.redEnvelopesSend.sendMoney)));
                        }
                    }
                }, mContext);
    }


    private List<LiveRedEnvelopes> getLiveRedEnvelopes(List<LiveRedEnvelopes> liveRedEnvelopes) {
        for (LiveRedEnvelopes liveRedEnvelope : liveRedEnvelopes) {
            liveRedEnvelope.createDate = TimeUtils.date2String(TimeUtils.string2Date(liveRedEnvelope.createDate), "HH:mm");
        }
        return liveRedEnvelopes;
    }


    @Override
    public void dismiss() {
        EventBus.getDefault().post(new EventMessage(Event.dialogDismiss));
        super.dismiss();
    }

    @OnClick(R.id.ivClose)
    protected void onClose(View view) {
        dismiss();
    }


}
