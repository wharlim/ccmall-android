package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddTopliBody {

    /**
     * type : 1
     * content : 发布CCMALL素材，有权限的用户
     * images : ["http://39.108.50.110/G1/M00/00/29/rBIdXFqnrmOAdmPSAAHg3EJpZII297.jpg","http://39.108.50.110/G1/M00/00/29/rBIdXFqnrmCAFjYBAAJEE_4-fGA003.jpg"]
     * linkTitle :
     * linkUrl :
     */

    @SerializedName("type")
    public int type;
    @SerializedName("content")
    public String content;
    @SerializedName("linkTitle")
    public String linkTitle;
    @SerializedName("linkUrl")
    public String linkUrl;
    @SerializedName("images")
    public List<String> images;
    @SerializedName("topicId")
    public String topicId;
}
