package com.weiju.ccmall.shared.bean.event;

import com.weiju.ccmall.module.auth.event.BaseMsg;

public class MsgCategory extends BaseMsg {
    public final static int ACTION_SHOW_NODATA = 1 << 0;
    public final static int ACTION_GONE_NODATA = 1 << 1;
    public final static int ACTION_RE_QUEST = 1 << 2;

    public MsgCategory(int action) {
        super(action);
    }

}
