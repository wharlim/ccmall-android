package com.weiju.ccmall.shared.bean;

/**
 * @author chenyanming
 * @time 2019/11/15 on 14:26
 * @desc ${TODD}
 */
public class JoinProduct {
    /**
     * 规格Id
     */
    public String skuId;
    /**
     * 上下架状态
     */
    public String status;
    /**
     * 库存
     */
    public String stock;
}
