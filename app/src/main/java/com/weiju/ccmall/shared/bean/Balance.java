package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.enums.AccountType;

/**
 * Created by Chan on 2017/6/17.
 *
 * @author Chan
 * @package com.weiju.ccmall.shared.bean
 * @since 2017/6/17 下午3:03
 */

public class Balance {
    @SerializedName("did")
    public String did;
    @SerializedName("code")
    public String code;
    @SerializedName("typeId")
    public long typeId;
    @SerializedName("money")
    public long money;
    @SerializedName("typeName")
    public String typeName;
    @SerializedName("statusStr")
    public String statusStr;
    @SerializedName("createDate")
    public String createDate;


    /**
     * ticketId : d70ec656c9c84833bea4dc6208c33a13
     * memberId : 34a561b32c664348bb88e0fb04647b25
     * sortIndex : 0
     * typeStr :
     * ticket : 1000000
     * orderId :
     * orderCode :
     */

    @SerializedName("ticketId")
    public String ticketId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("sortIndex")
    public int sortIndex;
    @SerializedName("typeStr")
    public String typeStr;
    @SerializedName("ticket")
    public long ticket;
    @SerializedName("orderId")
    public String orderId;
    @SerializedName("orderCode")
    public String orderCode;


    /**
     * coinFreezeId : 6a1fbeede77542c6ab961aad93888d2a
     * coin : -100000
     * orderMemberId :
     * profitTime : 2019-01-03 14:25:59
     */

    @SerializedName("coinFreezeId")
    public String coinFreezeId;
    @SerializedName("coin")
    public long coin;
    @SerializedName("orderMemberId")
    public String orderMemberId;
    @SerializedName("profitTime")
    public String profitTime;


    @SerializedName("freezeId")
    public String freezeId;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("freezeProfitMoney")
    public long freezeProfitMoney;
    @SerializedName("freezeSumMoney")
    public long freezeSumMoney;
    @SerializedName("orderMoney")
    public long orderMoney;


    /**
     * goldProfitId : 8ea30dad58c247e783742304e19e7f7f
     * type : 2
     * orderMoney : 20000
     * unfreezeSumGold : 0
     * availableGold : 99980000
     * status : 1
     * level : 0
     * gold : -20000
     */

    @SerializedName("goldProfitId")
    public String goldProfitId;
    @SerializedName("type")
    public int type;
    @SerializedName("unfreezeSumGold")
    public int unfreezeSumGold;
    @SerializedName("availableGold")
    public int availableGold;
    @SerializedName("status")
    public int status;
    @SerializedName("level")
    public int level;
    @SerializedName("gold")
    public int gold;
    /**
     * goldenTicket : 100000
     */

    @SerializedName("goldenTicket")
    public long goldenTicket;

    /**
     * psp,tsp,pcp
     */
    @SerializedName("point")
    public long point;
    @SerializedName("formatPoint")
    public double formatPoint;


    public String getTypeName(AccountType accountType) {
        return accountType == AccountType.Balance ? typeName : typeStr;
    }

    public String getCreateDate(AccountType accountType) {
        return accountType == AccountType.CCM ? profitTime : createDate;
    }

    public long getMoney(AccountType accountType) {
        switch (accountType) {
            case CCM:
                return coin;
            case CB:
                return ticket;
            case Balance:
                return money;
            case Profit:
                return freezeProfitMoney;
            case ShopMoney:
                return gold;
            case GoldenTicket:
                return goldenTicket;
            case TSP:
            case PSP:
                return point;
            case PCP:
                return point / 10;

            default:
                return money;
        }
    }

}
