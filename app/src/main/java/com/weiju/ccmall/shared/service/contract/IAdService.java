package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.GuideModel;
import com.weiju.ccmall.shared.bean.MainAdModel;
import com.weiju.ccmall.shared.bean.RedDialog;
import com.weiju.ccmall.shared.bean.RedPacketEx;
import com.weiju.ccmall.shared.bean.RedPacketModel;
import com.weiju.ccmall.shared.bean.Splash;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service.contract
 * @since 2017-08-03
 */
public interface IAdService {

    @GET("splashScreen/getSplashScreen")
    Observable<RequestResult<Splash>> getSplashAd();

    @GET("popupWindows/get")
    Observable<RequestResult<MainAdModel>> getMainAd();


    @GET("redEnvelopeActivity/getMyRedEnvelope")
    Observable<RequestResult<RedDialog>> getRedDialog();

    @GET("redEnvelope/getRedEnvelopeList")
    Observable<RequestResult<PaginationEntity<RedPacketModel, RedPacketEx>>> getRedPacketList(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize
    );

    @POST("redEnvelope/receiveRedEnvelope")
    @FormUrlEncoded
    Observable<RequestResult<RedPacketModel>> openRedPacket(
            @Field("redEnvelopeId") String redEnvelopeId
    );


    @GET("guidePage/getGuidePage")
    Observable<RequestResult<GuideModel>> getGuide();

}
