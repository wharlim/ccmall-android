package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.tencent.qcloud.tim.uikit.component.CircleImageView;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.ImageUtil;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/24 on 10:32
 * @desc
 */
public class LiveShareDialog extends Dialog {
    private final Activity mContext;
    private final UMWeb mWeb;

    @BindView(R.id.shareView)
    protected View shareView;

    String mLink;
    @BindView(R.id.flLivebg)
    ImageView flLivebg;
    @BindView(R.id.userIcon)
    CircleImageView userIcon;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.tvLiveTitle)
    TextView tvLiveTitle;
    @BindView(R.id.ivQrCode)
    ImageView ivQrCode;

    private String mLiveId;
    private String liveTitle;
    private String logoUrl;
    private String nickName;
    private ILiveService liveService = ServiceManager.getInstance().createService(ILiveService.class);

    public LiveShareDialog(@NonNull Activity context, String title, String desc, String logoUrl,
                           String liveId, String invitationCode, String nickName, boolean isForecast) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        String url = "%slw/%s/%s";
        if (isForecast) {
            url += "/4";
        }
        mLiveId = liveId;
        liveTitle = title;
        this.logoUrl = logoUrl;
        this.nickName = nickName;
        mLink = String.format(url, BuildConfig.WECHAT_URL, liveId, invitationCode);
        mWeb = new UMWeb(mLink);
        mWeb.setTitle(title);
        mWeb.setDescription("我在CCMALL直播中");

        if (TextUtils.isEmpty(logoUrl)) {
            mWeb.setThumb(new UMImage(context, R.drawable.logo));
        } else {
            mWeb.setThumb(new UMImage(context, logoUrl));
        }
    }

    public LiveShareDialog(@NonNull Activity context, String title, String desc, String logoUrl,
                           String liveId, String invitationCode, String nickName) {
        this(context, title, desc, logoUrl, liveId, invitationCode, nickName, false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_share);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.dimAmount = 0.0f;
        getWindow().setAttributes(layoutParams);

        LiveUser liveUser = SessionUtil.getInstance().getLiveUser();
        if (TextUtils.isEmpty(nickName) && liveUser != null) {
            nickName = liveUser.userName;
        }
        initShareView(logoUrl, liveUser != null ? liveUser.headImage : logoUrl, nickName, liveTitle);
    }

    public void initShareView(String liveBg, String userIcon, String userName, String liveTitle) {
        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.img_create_live_room_bg)
                .transform(new MultiTransformation<>(new RoundedCorners(10), new CenterCrop()));
        Glide.with(getContext())
                .load(Uri.parse(liveBg))
                .apply(options)
                .into(flLivebg);
        RequestOptions options1 = new RequestOptions()
                .placeholder(R.mipmap.default_avatar);
        Glide.with(getContext())
                .load(Uri.parse(userIcon))
                .apply(options1)
                .into(this.userIcon);


        tvName.setText(userName);
        tvLiveTitle.setText(liveTitle);

        Bitmap logoBitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.logo);
        Bitmap qrCode = ImageUtil.createQRCodeBitmap(mContext, mLink, 1000, 1000, "UTF-8",
                "H", "2", Color.BLACK, Color.WHITE, logoBitmap, 0.25F);
        ivQrCode.setImageBitmap(qrCode);
    }

    @OnClick({R.id.tvFriend, R.id.tvCircle})
    protected void share(View view) {
        EventBus.getDefault().post(new EventMessage(Event.openNewActivity));
        ShareAction shareAction = new ShareAction(mContext)
                .setPlatform(SHARE_MEDIA.WEIXIN)
                .withMedia(mWeb)
                .setCallback(mUmShareListener);
        switch (view.getId()) {
            case R.id.tvFriend:
                shareAction.setPlatform(SHARE_MEDIA.WEIXIN);
                break;
            case R.id.tvCircle:
                shareAction.setPlatform(SHARE_MEDIA.WEIXIN_CIRCLE);
                break;
            default:
        }
        shareAction.share();
        dismiss();
    }

    @OnClick(R.id.llLiveShareRoot)
    protected void llLiveShareRoot() {
        dismiss();
    }

    @OnClick(R.id.tvCode)
    protected void creatBitmap() {
        dismiss();
        ToastUtil.showLoading(mContext);
        // ImageUtil.saveImageToGallery(mContext, qrCode);

        Bitmap shareBitmap = Bitmap.createBitmap(shareView.getWidth(), shareView.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(shareBitmap);
        shareView.setVisibility(View.VISIBLE);
        shareView.draw(canvas);
        shareView.setVisibility(View.INVISIBLE);
        ImageUtil.saveImageToGallery(mContext, shareBitmap);
    }


    private UMShareListener mUmShareListener = new UMShareListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {

            ToastUtil.showLoading(mContext);
            APIManager.startRequest(liveService.clickShareOrGoods(mLiveId, 1, null), new BaseRequestListener<Object>() {
                @Override
                public void onSuccess(Object result) {
                    // no op
                }
            }, mContext);
        }

        @Override
        public void onResult(SHARE_MEDIA share_media) {
            ToastUtil.hideLoading();
        }

        @Override
        public void onError(SHARE_MEDIA share_media, Throwable throwable) {
            ToastUtil.hideLoading();
        }

        @Override
        public void onCancel(SHARE_MEDIA share_media) {
            ToastUtil.hideLoading();
        }
    };
}
