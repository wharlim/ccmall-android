package com.weiju.ccmall.shared.component.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.LiveRedEnvelopes;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

/**
 * @author chenyanming
 * @time 2020/1/8 on 10:29
 * @desc
 */
public class LiveRedDetailAdapter extends BaseQuickAdapter<LiveRedEnvelopes, BaseViewHolder> {
    public LiveRedDetailAdapter() {
        super(R.layout.item_live_red_detail);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveRedEnvelopes item) {
        FrescoUtil.setImageSmall(helper.getView(R.id.ivAvatar), item.headImage);
        helper.setText(R.id.tvName, item.nickName);
        helper.setText(R.id.tvDate, item.createDate);
        helper.setText(R.id.tvMoney, String.format("%s元", MoneyUtil.centToYuanStrNoZero(item.receiveMoney)));
    }
}
