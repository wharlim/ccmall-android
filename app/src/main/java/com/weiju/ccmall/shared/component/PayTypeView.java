package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.PayTypeModel;
import com.weiju.ccmall.shared.bean.ProfitData;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PayTypeView extends FrameLayout {

    @BindView(R.id.ivLogo)
    SimpleDraweeView mIvLogo;
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.ivSelect)
    ImageView mIvSelect;
    @BindView(R.id.tvGoldLuck)
    TextView mTvGoldLuck;

    private boolean mIsSelect;
    private PayTypeModel mModel;
    private ItemClickListener mItemClickListener;

    public PayTypeView(@NonNull Context context) {
        super(context);
        View view = inflate(context, R.layout.view_pay_type, this);
        ButterKnife.bind(this, view);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemClickListener != null) {
                    mItemClickListener.onSelectItem(getType());
                }
            }
        });
    }

    public void setData(PayTypeModel model, ProfitData profitData, long totalCost, int productType) {
        mModel = model;
        FrescoUtil.setImage(mIvLogo, model.iconUrl);
        if (model.payType == AppTypes.PAY_TYPE.BALANCE) {
            mTvName.setText(String.format("%s（可用¥%s）", model.payTypeStr, MoneyUtil.centToYuanStrNoZero(profitData.availableMoney)));
        } else if (model.payType == AppTypes.PAY_TYPE.BALANCE_GOLD) {
            mTvName.setText(String.format("%s（可用¥%s）", model.payTypeStr, MoneyUtil.centToYuanStrNoZero(profitData.availableGold)));
            mTvGoldLuck.setText(Html.fromHtml(String.format("使用购物券支付，预计可返<font color =\"#f51861\">%s</font>幸运积分",
                    totalCost > 0 ? 0 : (int) (profitData.availableGoldLuck / 100))));
            mTvGoldLuck.setVisibility(VISIBLE);
            if (productType == 26) {
                mTvGoldLuck.setVisibility(GONE);
            }
        } else {
            mTvName.setText(model.payTypeStr);
        }
    }

    public int getType() {
        return mModel.payType;
    }

    public void select(boolean isSelect) {
        mIsSelect = isSelect;
        mIvSelect.setSelected(mIsSelect);
    }

    public boolean isSelect() {
        return mIsSelect;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onSelectItem(int payType);
    }
}
