package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class LivingRoomHotDataMsg {
    @SerializedName("popularIndex")
    public String popularIndex;
    public static class SystemChatMsg {
        @SerializedName("nickname")
        public String nickname;
        @SerializedName("content")
        public String content;
    }

    @SerializedName("systemChatMsg")
    public SystemChatMsg systemChatMsg;
}
