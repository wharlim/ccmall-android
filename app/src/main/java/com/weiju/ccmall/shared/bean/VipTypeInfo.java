package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class VipTypeInfo {

    /**
     * vipType : 0
     * vipTypeStr : 普通会员
     */

    @SerializedName("vipType")
    public int vipType;
    @SerializedName("vipTypeStr")
    public String vipTypeStr;

    public VipTypeInfo(int vipType, String vipTypeStr) {
        this.vipType = vipType;
        this.vipTypeStr = vipTypeStr;
    }
}
