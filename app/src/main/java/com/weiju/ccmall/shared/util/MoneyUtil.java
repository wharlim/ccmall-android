package com.weiju.ccmall.shared.util;

import android.animation.ValueAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/5/7.
 * ConvertUtil 价格计算方法太多 太乱了 无法维护
 * 现在用这个新的 util 来重新计算价格
 * <p>
 * -----名词定义------
 */
public class MoneyUtil {

    /**
     * 将分转成元
     * 用 String 输出，并在前面加上金钱符号
     *
     * @param cent
     * @return 109 ->  ¥1.09
     */
    public static String centToYuan¥Str(long cent) {
        return cleanZero(String.format("¥%.2f", MoneyUtil.centToYuan(cent)));
    }

    /**
     * 将分转成元
     * 用 String 输出
     *
     * @param cent
     * @return 110 ->  1.10
     */
    public static String centToYuanStr(long cent) {
        if (cent == 0) {
            return "0";
        }
        return cleanZero(String.format("%.2f", MoneyUtil.centToYuan(cent)));
    }

    /**
     * 将分转换成元，如果是.00结尾，则不显示.00
     *
     * @param cent
     * @return 2100 ->  21 ||  2110 ->  21.10
     */
    public static String centToYuanStrNo00End(long cent) {
        String centStr = String.valueOf(cent);
        if (cent==0){
            return   centStr;
        }
        if (centStr.endsWith("00")) {
            return cleanZero(String.valueOf(cent / 100));
        } else {
            return cleanZero(String.format("%.2f", centToYuan(cent)));
        }
    }

    /**
     * 将分转成元
     * 用 String 输出，并在前面加上金钱符号,如果.00结尾，则不显示
     *
     * @param cent
     * @return 1900 ->  ¥19
     */
    public static String centToYuan¥StrNo00End(long cent) {
        if (cent == 0) {
            return "0";
        }
        String str = centToYuan¥Str(cent);
        if (str.endsWith(".00")) {
            return str.substring(0, str.length() - 3);
        } else {
            return cleanZero(str);
        }
    }

    /**
     * 将分转成元,去掉所有0
     * 用 String 输出，并在前面加上金钱符号
     *
     * @param cent
     * @return 1900 ->  ¥19 | 2520 -> ¥25.2
     */
    public static String centToYuan¥StrNoZero(long cent) {

        if (cent==0){
            return  "¥0" ;
        }
        return cleanZero("¥" + centToYuanStrNoZero(cent));
    }

    /**
     * 将分转成元,去掉所有0
     * 用 String 输出
     *
     * @param cent
     * @return 1900 ->  19 | 2520 -> 25.2
     */
    public static String centToYuanStrNoZero(long cent) {
        String centStr = String.valueOf(cent);
        if (cent==0){
             return   centStr;
        }
        if (centStr.endsWith("00")) {
            return cleanZero(String.valueOf(cent / 100));
        } else if (centStr.endsWith("0")) {
            return cleanZero(centToYuanStr(cent));
        } else {
            return cleanZero(String.format("%.2f", centToYuan(cent)));
        }
    }

    /**
     * 将CCM转成元（除以8个0）,去掉所有0
     * 用 String 输出
     *
     * @param cent
     * @return
     */
    public static String coinToYuanStrNoZero(long cent) {
        String centStr = String.valueOf(cent);
        if (cent==0){
            return   centStr;
        }
        centStr = cent<0?centStr.substring(1,centStr.length()):centStr;

        if(centStr.length()<=8){
            StringBuilder startCent = new StringBuilder("0.");
            for (int i = 0;i<(8-centStr.length());i++){
                startCent.append("0");
            }
            centStr = startCent+centStr;
        }else {
            centStr = centStr.substring(0,centStr.length()-8)+"." + centStr.substring(centStr.length()-8,centStr.length());
        }
        int length = centStr.length();
        for (int i = 0 ;i<length;i++){
            if (centStr.endsWith("0")) {
                centStr = centStr.substring(0,centStr.length()-1);
            }else if (centStr.endsWith(".")){
                centStr = centStr.substring(0,centStr.length()-1);
                return cent<0?"-"+centStr:centStr;
            }else {
                return cent<0?"-"+centStr:centStr;
            }
        }
          return cent<0?"-"+centStr:centStr;
    }


    /**
     * 将分转成元，用小数输出
     *
     * @param cent
     * @return 109 -> 1.09
     */
    public static double centToYuan(long cent) {
        BigDecimal bigDecimal = BigDecimal.valueOf(cent);
        return bigDecimal.divide(BigDecimal.valueOf(100)).doubleValue();
    }

    /**
     * 将 String 的元转化成long 的分输出出去
     *
     * @param yuanStr "1.09"
     * @return 1.09 -> 1.09
     */
    public static long yuanStrToCentLong(String yuanStr) {
        if (StringUtils.isEmpty(yuanStr)) {
            return 0;
        }
        double moneyDouble = Double.parseDouble(yuanStr);
        moneyDouble = moneyDouble > 0 ? moneyDouble + 0.005 : 0;
        long money2 = new Double(moneyDouble * 100).longValue();
        return money2;
    }

    /**
     * 将 String 的元转化成Long的分输出出去
     *
     * @param yuanStr "1.09"
     * @return 1.09 -> 1.09
     */
    public static Long yuanToCentLong(String yuanStr) {
        if (StringUtils.isEmpty(yuanStr)) {
            return null;
        }
        double moneyDouble = Double.parseDouble(yuanStr);
        moneyDouble = moneyDouble > 0 ? moneyDouble + 0.005 : 0;
        long money2 = new Double(moneyDouble * 100).longValue();
        return money2;
    }


    /**
     * 给 textview 设置一个金额，并且加上数值变化的动画
     *
     * @param tvMoney 需要设置钱的 textview
     * @param money   分
     */
    public static void setMoneyDataAnimator(final TextView tvMoney, long money) {
        double yuan = ConvertUtil.cent2yuan(money);
        ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, (float) yuan);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                tvMoney.setText(String.format("%.2f", animation.getAnimatedValue()));
            }
        });
        valueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        valueAnimator.setDuration((long) (0.65 * 1000));
        valueAnimator.start();
    }

    public static String cleanZero(String money){
        if (money.equals("0")){
            return   money;
        }
        if(money.endsWith(".0")){
            return money.replace(".0", "");
        }else if (money.endsWith(".00")){
            return money.replace(".00", "");
        }else if(money.contains(".") && money.endsWith("0")){
            return money.substring(0,money.length()-1);
        }
        return money;
    }

    public static double keepTwoDecimals(double d) {
        DecimalFormat df = new DecimalFormat("######0.00");
        df.format(d);
        return d;
    }

    public static Long yuanToCentLong(double moneyDouble) {
        moneyDouble = moneyDouble > 0 ? moneyDouble + 0.005 : 0;
        long money2 = new Double(moneyDouble * 100).longValue();
        return money2;
    }

}
