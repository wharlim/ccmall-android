package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author chenyanming
 * @time 2019/2/22 on 17:52
 * @desc
 */
public class ContributionType {
    /**
     * value : 0
     * minFillValue : 3
     */

    @SerializedName("value")
    public String value;
    @SerializedName("minFillValue")
    public float minFillValue;
    @SerializedName("list")
    public List<ContributionData> list;
}
