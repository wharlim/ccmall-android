package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class MemberRatio {

    /**
     * memberType : 3
     * joinPrice : 99900
     * ratio : 70
     */

    @SerializedName("memberType")
    public int memberType;
    @SerializedName("joinPrice")
    public long joinPrice;
    @SerializedName("ratio")
    public int ratio;
}
