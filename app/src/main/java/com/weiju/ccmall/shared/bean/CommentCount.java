package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/8/27.
 */
public class CommentCount {
    /**
     * waitCount : 0
     * laterCount : 6
     */

    @SerializedName("waitCount")
    public int waitCount;
    @SerializedName("laterCount")
    public int laterCount;
}
