package com.weiju.ccmall.shared;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * 常量
 * Created by JayChan on 2016/12/13.
 */
public class Constants {

    public static final String API_VERSION = "1.0";
    public static final String COOKIE_PATH = "/";
    public static final int REQUEST_TIMEOUT = 30;
    public static final int NOT_LOGIN_CODE = 1;
    public static final int SUCCESS_CODE = 0;
    public static final int ERROR_CODE = 2;

    public static final String KEY_TYPE = "type";

    public static final String API_SALT = "a70c34cc321f407d990c7a2aa7900729";

    public static final SimpleDateFormat FORMAT_DATE_FULL = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINA);
    public static final SimpleDateFormat FORMAT_DATE_SIMPLE = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
    public static final int SHARE_THUMB_SIZE = 150;

    public static final int PAGE_SIZE = 10;


    public static final String KEY_EXTROS = "key";
    // todo slogan
    public static final String share_tile_text = "无忧生活 无忧品质";
    public static final String KEY_IS_EDIT = "isEdit";
    public static final String KEY_CATEGORY_ID = "category_id";
    public static final String KEY_TITLE = "key_title";
    public static final String KEY_IS_VIDEO = "key_video";
    public static final String KEY_LIBRARY_ID = "key_library";
    public static final String KEY_MEDIAURL = "key_mdiaurl";

    public static class Extras {
        public static final String PUSH_MESSAGE = "PUSH_MESSAGE";
        public static final String PRODUCT = "DATA_PRODUCT";
        public static final String DATA_HANDLER = "DATA_DATAHANDLER";
        public static final String STRING = "DATA_STRING";
        public static final String NICKNAME = "DATA_NICKNAME";
        public static final String PHONE = "DATA_PHONE";
        public static final String PASSWORD = "DATA_PASSWORD";
        public static final String ADDRESS = "DATA_ADDRESS";
        public static final String CARTS = "DATA_CARTS";
        public static final String ORDER_STATUS = "ORDER_STATUS";
        public static final String ORDER_CODE = "ORDER_CODE";
        public static final String ORDER_TRANSFER = "ORDER_TRANSFER";
        public static final String USER = "DATA_USER";
        public static final String FEEDBACK = "FEEDBACK";
        public static final String ORDER_ID = "ORDER_ID";
        public static final String QUESTION_ID = "QUESTION_ID";
        public static final String CATEGORY_ID = "CATEGORY_ID";
        public static final String PRODUCT_CODE = "PRODUCT_CODE";
        public static final String SHOPPING = "SHOPPING";
        public static final String COURSE = "COURSE";
        public static final String MOMENT = "MOMENT";
        public static final String AUTHOR = "AUTHOR";
        public static final String TOPICID = "TOPICID";
        public static final String CART_IDS = "CART_IDS";

        public static final String KEY_EXTRAS = "key_data";
        public static final String KET_TYPE = "KEY_TYPE";
        public static final String KEY_BANK_APPLYID = "KEY_ACCOUND_ID";
        public static String SEARCH_ORDER = "search_order";
    }

}
