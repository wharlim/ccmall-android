package com.weiju.ccmall.shared.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;

import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.Constants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.util
 * @since 2017-07-06
 */
public class BitmapUtil {

    public static Bitmap scaleBitmap(String imgUrl, int width, int height) {
        URL url;
        Bitmap scaledBitmap = null;
        HttpURLConnection connection = null;
        try {
            url = new URL(imgUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap;
            if (input == null) {
                bitmap = BitmapFactory.decodeResource(MyApplication.getInstance().getResources(), R.mipmap.ic_launcher);
            } else {
                bitmap = BitmapFactory.decodeStream(input);
            }
            scaledBitmap = Bitmap.createScaledBitmap(bitmap, width, height, true);
            bitmap.recycle();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return scaledBitmap;
    }

    public static Bitmap scaleBitmapForShare(String filePath) {
        return scaleBitmap(filePath, Constants.SHARE_THUMB_SIZE, Constants.SHARE_THUMB_SIZE);
    }

    public static byte[] bitmapToBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        bitmap.recycle();
        return stream.toByteArray();
    }

    public static Bitmap waterMaskBitmap(Bitmap srcBitmap, int watermarkRes) {
        Context context = MyApplication.getInstance().getApplicationContext();
        BitmapDrawable bitmapDrawable = (BitmapDrawable) context.getResources().getDrawable(watermarkRes);

        Bitmap watermarkBitmap = bitmapDrawable.getBitmap();
        int width = srcBitmap.getWidth();
        int height = srcBitmap.getHeight();
        watermarkBitmap = zoomImage(watermarkBitmap, width, height);
        int x = (width - watermarkBitmap.getWidth()) / 2;
        int y = (height - watermarkBitmap.getHeight()) / 2;

        //将该图片作为画布
        Canvas canvas = new Canvas(srcBitmap);
        //在画布 0，0坐标上开始绘制原始图片
        canvas.drawBitmap(srcBitmap, 0, 0, null);
        //在画布上绘制水印图片
        canvas.drawBitmap(watermarkBitmap, x, y, null);
        // 保存 Canvas.ALL_SAVE_FLAG
        canvas.save();
        // 存储
        canvas.restore();
        return srcBitmap;
    }

    /**
     * 将素材图缩放到和原图匹配的大小（该方法仅限用于商品售罄和下架水印）
     *
     * @param bgimage
     * @param srcWidth  商品图大小
     * @param srcHeight 商品图大小
     * @return
     */
    public static Bitmap zoomImage(Bitmap bgimage, float srcWidth,
                                   float srcHeight) {
        float minSize = srcHeight > srcWidth ? srcWidth : srcHeight;
        float newSize = minSize * 2 / 3;
        // 获取这个图片的宽和高
        float width = bgimage.getWidth();
        float height = bgimage.getHeight();
        // 创建操作图片用的matrix对象
        Matrix matrix = new Matrix();
        // 计算宽高缩放率
        float scaleWidth = newSize / width;
        float scaleHeight = newSize / height;
        // 缩放图片动作
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width,
                (int) height, matrix, true);
        return bitmap;
    }

}
