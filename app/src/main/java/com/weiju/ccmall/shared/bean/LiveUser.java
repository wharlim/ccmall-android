package com.weiju.ccmall.shared.bean;

import android.support.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author chenyanming
 * @time 2019/11/22 on 17:07
 * @desc ${TODD}
 */
public class LiveUser implements Serializable {

    /**
     * memberId : 0c82a2238bfc4f0281fef0cd0e365287
     * userId : 41456445
     * account : s40146298
     * isChild : 0
     * nickName :
     * password : 0c82a2238bfc4f0281fef0cd0e365287
     * email :
     * headImage : http://thirdwx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIdibYzJQ5BvmicasaziaGyDGmoEibhydicntSZ69Z61OKz3tWeHlgiazGoiccRBq9EPjrTC8LVwES6hsfhA/132
     * createDate : 2019-11-21 14:13:24
     * updateDate : 2019-11-21 14:13:24
     * deleteFlag : 0
     */

    @SerializedName("memberId")
    public String memberId;
    @SerializedName("userId")
    public String userId;
    @SerializedName("account")
    public String account;
    @SerializedName("isChild")
    public int isChild;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("password")
    public String password;
    @SerializedName("email")
    public String email;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;


    /**
     * userName : 陈子程
     * liveLimit : 0
     * followNum : 0
     * fansNum : 0
     * likeNum : 0
     * popularity : 0
     */

    @SerializedName("userName")
    public String userName;
    @SerializedName("liveLimit")
    public int liveLimit;
    @SerializedName("followNum")
    public int followNum;
    @SerializedName("fansNum")
    public int fansNum;
    @SerializedName("likeNum")
    public int likeNum;
    @SerializedName("popularity")
    public int popularity;
    @SerializedName("liveNoticeCount")
    public int liveNoticeCount;


    /**
     * followStatus : 0
     */

    @SerializedName("followStatus")
    public int followStatus;
    @SerializedName("liveLevel")
    public int liveLevel;
    @SerializedName("liveLevelStr")
    public String liveLevelStr;

    public String id;
    public String name;
    public String portrait;
    public String token;


    /**
     * audienceNum : 1
     * liveId : fbba035dbbb54d34ba487d010c8ba136
     */

    @SerializedName("audienceNum")
    public int audienceNum;
    @SerializedName("liveId")
    public String liveId;
    @SerializedName("shopkeeper")
    public boolean shopkeeper;//true为店主

    public static LiveUser fromJson(@Nullable String json) {
        if (json == null || json.isEmpty()) {
            return null;
        }
        return new Gson().fromJson(json, LiveUser.class);
    }
}
