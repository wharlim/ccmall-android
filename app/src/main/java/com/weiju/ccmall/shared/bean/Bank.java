package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author chenyanming
 * @time 2019/6/3 on 15:50
 * @desc ${TODD}
 */
public class Bank implements Serializable {

    /**
     * bank_no : 102100099996
     * bank_name : 中国工商银行
     * rank : 0
     * recommend : 1
     * pinyin : ZGGSYH
     */

    @SerializedName("bank_no")
    public String bankNo;
    @SerializedName("bank_name")
    public String bankName;
    @SerializedName("rank")
    public int rank;
    /**
     * 0不是热门，1是热门推荐
     */
    @SerializedName("recommend")
    public int recommend;
    @SerializedName("pinyin")
    public String pinyin;
}
