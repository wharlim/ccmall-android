package com.weiju.ccmall.shared.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * @author chenyanming
 * @time 2019/6/3 on 15:48
 * @desc ${TODD}
 */
public class BankItem implements MultiItemEntity {

    public static final int TITLE = 1;
    public static final int ITEM = 2;

    public int mItemType;
    public String title;
    public Bank mItem;
    public boolean isShowLine = true;

    public BankItem(int itemType) {
        mItemType = itemType;
    }


    @Override
    public int getItemType() {
        return mItemType;
    }

    public BankItem(String title) {
        mItemType = TITLE;
        this.title = title;
    }

    public BankItem(Bank item,String title) {
        mItemType = ITEM;
        mItem = item;
        this.title = title;
    }
}
