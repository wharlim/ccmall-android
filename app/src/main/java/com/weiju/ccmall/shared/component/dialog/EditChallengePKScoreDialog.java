package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.ImageView;

import com.blankj.utilcode.utils.StringUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.contracts.OnValueChangeLister;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditChallengePKScoreDialog extends Dialog {

    @BindView(R.id.minusBtn)
    protected ImageView mMinusBtn;
    @BindView(R.id.plusBtn)
    protected ImageView mPlusBtn;
    @BindView(R.id.valueEt)
    protected EditText mValueEt;

    private int mMin = 10;
    private int mMax = 200;
    private int mValue = 10;
    private OnValueChangeLister mListener;

    public EditChallengePKScoreDialog(Context context, int min, int max) {
        this(context, 0);
        mMin = min;
        mMax = max;
        mValue = min;
    }

    private EditChallengePKScoreDialog(Context context, int theme) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_challenge_pk_score);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.CENTER);
        setCanceledOnTouchOutside(true);
        initViews();
        setButtonsEnabled();
        mValueEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
//                if ((charSequence.length() > 0 && !charSequence.toString().endsWith("0")) || Integer.parseInt(charSequence.toString()) < 10) {
//                    ToastUtil.error("积分必须是10的倍数");
//                    mValueEt.setText(beforeValue);
//                    return;
//                }
                int value = 0;
                if (!StringUtils.isEmpty(mValueEt.getText().toString())) {
                    mValue = Integer.valueOf(mValueEt.getText().toString());
                }
                setButtonsEnabled();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    public void setOnChangeListener(OnValueChangeLister listener) {
        this.mListener = listener;
    }

    private void initViews() {
        mValueEt.setText(String.valueOf(this.mValue));
    }

    public void setValue(int value) {
        value = value < mMin ? mMin : value;
        this.mValue = value <= mMax ? value : mMax;

        mValueEt.setText(String.valueOf(this.mValue));
        setButtonsEnabled();
    }

    private void setButtonsEnabled() {
        this.mMinusBtn.setEnabled(mMin < this.mValue);
        this.mPlusBtn.setEnabled(mMax > this.mValue);
    }


    @OnClick(R.id.minusBtn)
    protected void onMinus() {
        setValue(this.mValue - 10);
    }

    @OnClick(R.id.plusBtn)
    protected void onPlus() {
        setValue(this.mValue + 10);
    }

    @OnClick(R.id.confirmBtn)
    protected void confirmToChange() {
        String value = mValueEt.getText().toString();
        if (StringUtils.isEmpty(value)) {
            return;
        }
        if (!value.endsWith("0") || Integer.parseInt(value.toString()) < 10) {
            ToastUtil.error("积分必须是10的倍数");
        } else {
            int score = Integer.parseInt(value);
            if (score < mMin) {
                ToastUtil.error(String.format("积分不能小于%s", mMin));
            } else if (score > mMax) {
                ToastUtil.error(String.format("积分不能大于%s", mMax));
            } else {
                if (this.mListener != null) {
                    this.mListener.changed(Integer.parseInt(value));
                }
                dismiss();
            }
        }

    }

    @OnClick(R.id.cancelBtn)
    protected void onClose() {
        dismiss();
    }

}
