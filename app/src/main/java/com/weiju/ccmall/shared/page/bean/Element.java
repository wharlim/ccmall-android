package com.weiju.ccmall.shared.page.bean;

import android.graphics.Color;
import android.view.View;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.common.base.Strings;
import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;
import com.tencent.bugly.crashreport.CrashReport;
import com.weiju.ccmall.R;

public class Element {

    @SerializedName("type")
    public String type;
    @SerializedName("height")
    public int height;
    @SerializedName("background")
    public String background;
    @SerializedName("columns")
    public int columns;
    @SerializedName("labelColor")
    public String labelColor;
    @SerializedName("data")
    public JsonElement data;
    @SerializedName("hasMore")
    public boolean hasMore;
    @SerializedName("moreLink")
    public Event moreLink;
    @SerializedName("title")
    public String title;

    public String orderType = "";


    public boolean hasBackground() {
        return !Strings.isNullOrEmpty(background);
    }

    public boolean isImageBackground() {
        return hasBackground() && (background.startsWith("http://") || background.startsWith("https://"));
    }

    public boolean isColorBackground() {
        return hasBackground() && background.startsWith("#");
    }

    public void setBackgroundInto(View view) {
        try { if (!hasBackground()) {
                return;
            }
            SimpleDraweeView imageView = (SimpleDraweeView) view.findViewById(R.id.eleBackgroundIv);
            if (imageView == null) {
                return;
            }
            if (isImageBackground()) {
                imageView.setImageURI(background);
                imageView.setVisibility(View.VISIBLE);
            } else {
                imageView.setVisibility(View.GONE);
                view.setBackgroundColor(Color.parseColor(background));
            }
        } catch (Exception e) {
            CrashReport.postCatchedException(e);
        }
    }

    @Override
    public String toString() {
        return "Element{" +
                "type='" + type + '\'' +
                ", height=" + height +
                ", background='" + background + '\'' +
                ", columns=" + columns +
                ", labelColor='" + labelColor + '\'' +
                ", data=" + data +
                ", hasMore=" + hasMore +
                ", moreLink=" + moreLink +
                ", title='" + title + '\'' +
                '}';
    }

    public class Event {
        @SerializedName("event")
        public String event;
        @SerializedName("target")
        public String target;

        @Override
        public String toString() {
            return "Event{" +
                    "event='" + event + '\'' +
                    ", target='" + target + '\'' +
                    '}';
        }
    }
}
