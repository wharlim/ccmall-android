package com.weiju.ccmall.shared.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;

import cn.bingoogolapple.photopicker.activity.BGAPhotoPreviewActivity;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.util
 * @since 2017-06-14
 */
public class ImageUtil {

//    public static void showGallerySelectorForAvatar(Activity activity, int selectAmount, final OnImageSelectListener listener) {
//        showGallerySelector(activity, selectAmount, false, listener);
//    }
//
//    public static void showGallerySelector(Activity activity, int selectAmount, boolean crop, final OnImageSelectListener listener) {
//        RxGalleryFinal galleryFinal = RxGalleryFinal.with(activity).image().imageLoader(ImageLoaderType.FRESCO);
//        if (selectAmount == 1) {
//            galleryFinal.radio().crop();
//            galleryFinal.subscribe(new RxBusResultSubscriber<ImageRadioResultEvent>() {
//                @Override
//                protected void onEvent(ImageRadioResultEvent imageRadioResultEvent) {
//                    //图片选择结果
//                    listener.onSelect(imageRadioResultEvent.getResult());
//                }
//            });
//            if (crop) {
//                galleryFinal.cropAspectRatioOptions(0, new AspectRatio(null, 1, 1));
//            }
//
//            galleryFinal.openGallery();
//            if (crop) {
//                RxGalleryFinalApi.getInstance(activity).onCrop(true).onCropImageResult(new IRadioImageCheckedListener() {
//                    @Override
//                    public void cropAfter(Object t) {
//                        listener.onCropImage((File) t);
//                    }
//
//                    @Override
//                    public boolean isActivityFinish() {
//                        return true;
//                    }
//                });
//            }
//        } else {
//            galleryFinal.multiple().maxSize(selectAmount);
//            galleryFinal.subscribe(new RxBusResultSubscriber<ImageMultipleResultEvent>() {
//                @Override
//                protected void onEvent(ImageMultipleResultEvent imageMultipleResultEvent) {
//                    //图片选择结果
//                    listener.onMultipleSelect(imageMultipleResultEvent.getResult());
//                }
//            });
//            galleryFinal.openGallery();
//        }
//    }

    public static void previewImage(Context context, ArrayList<String> images, int position, boolean isSaveFile) {
        File saveFile = null;
        if (isSaveFile) {
            saveFile = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        }
        if (images.size() == 1) {
            Intent intent = new BGAPhotoPreviewActivity.IntentBuilder(context)
                    .saveImgDir(saveFile)
                    .previewPhoto(images.get(0))
                    .build();
            context.startActivity(intent);
        } else if (images.size() > 1) {
            Intent intent = new BGAPhotoPreviewActivity.IntentBuilder(context)
                    .saveImgDir(saveFile)
                    .previewPhotos(images)
                    .currentPosition(position)
                    .build();
            context.startActivity(intent);
        }
    }

    public static Bitmap createQRCodeBitmap(Context context, String content, int width, int height, String character_set,
                                            String error_correction_level, String margin, int color_black,
                                            int color_white, Bitmap logoBitmap, float logoPercent) {
        // 字符串内容判空
        if (TextUtils.isEmpty(content)) {
            return null;
        }
        // 宽和高>=0
        if (width < 0 || height < 0) {
            return null;
        }
        //ToastUtil.showLoading(context);

        try {
            /** 1.设置二维码相关配置,生成BitMatrix(位矩阵)对象 */
            Hashtable<EncodeHintType, String> hints = new Hashtable<>();
            // 字符转码格式设置
            if (!TextUtils.isEmpty(character_set)) {
                hints.put(EncodeHintType.CHARACTER_SET, character_set);
            }
            // 容错率设置
            if (!TextUtils.isEmpty(error_correction_level)) {
                hints.put(EncodeHintType.ERROR_CORRECTION, error_correction_level);
            }
            // 空白边距设置
            if (!TextUtils.isEmpty(margin)) {
                hints.put(EncodeHintType.MARGIN, margin);
            }
            /** 2.将配置参数传入到QRCodeWriter的encode方法生成BitMatrix(位矩阵)对象 */
            BitMatrix bitMatrix = new QRCodeWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);

            /** 3.创建像素数组,并根据BitMatrix(位矩阵)对象为数组元素赋颜色值 */
            int[] pixels = new int[width * height];
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    //bitMatrix.get(x,y)方法返回true是黑色色块，false是白色色块
                    if (bitMatrix.get(x, y)) {
                        pixels[y * width + x] = color_black;//黑色色块像素设置
                    } else {
                        pixels[y * width + x] = color_white;// 白色色块像素设置
                    }
                }
            }

            /** 4.创建Bitmap对象,根据像素数组设置Bitmap每个像素点的颜色值,并返回Bitmap对象 */
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);

            /** 5.为二维码添加logo图标 */
            if (logoBitmap != null) {
                return addLogo(context, bitmap, logoBitmap, logoPercent);
            }
            return bitmap;
        } catch (WriterException e) {
            ToastUtil.hideLoading();
            e.printStackTrace();
            return null;
        }
    }

    private static Bitmap addLogo(Context context, Bitmap srcBitmap, Bitmap logoBitmap, float logoPercent) {
        if (srcBitmap == null) {
            return null;
        }
        if (logoBitmap == null) {
            return srcBitmap;
        }
        //传值不合法时使用0.2F
        if (logoPercent < 0F || logoPercent > 1F) {
            logoPercent = 0.2F;
        }

        /** 1. 获取原图片和Logo图片各自的宽、高值 */
        int srcWidth = srcBitmap.getWidth();
        int srcHeight = srcBitmap.getHeight();
        int logoWidth = logoBitmap.getWidth();
        int logoHeight = logoBitmap.getHeight();

        /** 2. 计算画布缩放的宽高比 */
        float scaleWidth = srcWidth * logoPercent / logoWidth;
        float scaleHeight = srcHeight * logoPercent / logoHeight;

        /** 3. 使用Canvas绘制,合成图片 */
        Bitmap bitmap = Bitmap.createBitmap(srcWidth, srcHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(srcBitmap, 0, 0, null);
        canvas.scale(scaleWidth, scaleHeight, srcWidth / 2, srcHeight / 2);
        canvas.drawBitmap(logoBitmap, srcWidth / 2 - logoWidth / 2, srcHeight / 2 - logoHeight / 2, null);

        return bitmap;
    }

    public static int saveImageToGallery(Context context, Bitmap bmp) {
        //生成路径
        String root = Environment.getExternalStorageDirectory().getAbsolutePath();
        String dirName = MyApplication.getInstance().getResources().getString(R.string.appName);
        File appDir = new File(root, dirName);
        if (!appDir.exists()) {
            appDir.mkdirs();
        }

        //文件名为时间
        long timeStamp = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String sd = sdf.format(new Date(timeStamp));
        String fileName = sd + ".jpg";

        //获取文件
        File file = new File(appDir, fileName);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            //通知系统相册刷新
            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                    Uri.fromFile(new File(file.getPath()))));

            ToastUtil.success("二维码保存成功，快去分享给好友吧");
            ToastUtil.hideLoading();

            return 2;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ToastUtil.hideLoading();
        return -1;
    }

}
