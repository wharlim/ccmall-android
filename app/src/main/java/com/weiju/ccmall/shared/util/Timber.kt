package com.weiju.ccmall.shared.util

import android.util.Log

import com.weiju.ccmall.BuildConfig

object Timber {

    @JvmStatic
    fun e(msg: Any) {
        if (!BuildConfig.DEBUG)
            return
        val trace = Throwable().stackTrace
        var tag = "TAG"
        if (trace != null && trace.size > 1) {
            tag = trace[1].fileName
            tag = tag.substring(0, tag.indexOf(".")) + "_" + trace[1].methodName
        }
        Log.e(tag, msg.toString())
    }

    fun d(msg: Any){
        if (!BuildConfig.DEBUG)
            return
        val trace = Throwable().stackTrace
        var tag = "TAG"
        if (trace != null && trace.size > 1) {
            tag = trace[1].fileName
            tag = tag.substring(0, tag.indexOf(".")) + "_" + trace[1].methodName
        }
        Log.d(tag, msg.toString())
    }


}
