package com.weiju.ccmall.shared.basic;

public class BaseEventMsg {
    private int action;

    public BaseEventMsg(int action) {
        this.action = action;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }
}
