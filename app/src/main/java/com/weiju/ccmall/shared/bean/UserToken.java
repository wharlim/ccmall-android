package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author chenyanming
 * @time 2019/1/11 on 19:07
 */
public class UserToken implements Serializable {
    @SerializedName("token")
    public String token = "";

    @SerializedName("tencentIMUserSig")
    public String tencentIMUserSig;

    @SerializedName("tencentIMSDKAppId")
    public String tencentIMSDKAppId;

    @SerializedName("memberId")
    public String memberId;

    @SerializedName("rongYunToken")
    public String rongYunToken;

    @SerializedName("nickName")
    public String nickName;
    @SerializedName("headUrl")
    public String headUrl;

}
