package com.weiju.ccmall.shared.manager;

import android.content.Context;

import com.blankj.utilcode.utils.LogUtils;
import com.weiju.ccmall.shared.bean.User;

import java.util.HashSet;
import java.util.Set;

import cn.jpush.android.api.JPushInterface;

public class PushManager {

    public static void registerJPushService(Context context) {
        JPushInterface.init(context);
    }


    public static void setJPushInfo(Context context, User user) {
        if (user == null) {
            JPushInterface.cleanTags(context, 1);
            LogUtils.e("清空推送 tag");
        } else {
            Set<String> set = new HashSet<String>();
            set.add(user.id);
            JPushInterface.setTags(context, 1, set);

            LogUtils.e("设置推送 tag   " + user.id);
        }
    }

}
