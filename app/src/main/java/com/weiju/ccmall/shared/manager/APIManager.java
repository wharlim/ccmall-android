package com.weiju.ccmall.shared.manager;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.contracts.RequestListener;
import com.weiju.ccmall.shared.util.SessionUtil;

import org.greenrobot.eventbus.EventBus;

import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.RequestBody;

/**
 * APIManager
 * Created by JayChan on 2016/12/13.
 */
public class APIManager {

    public static <T> void startRequest(Observable<T> observable, Observer<T> observer) {
        observable.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(observer);
    }

    public static <T> void startRequest(final Observable<RequestResult<T>> observable, final RequestListener<T> listener, Context context) {

        if (observable == null) {
            return;
        }
        listener.onStart();
        if (CacheUtil.getInstance().cache(observable, listener)) {
            return;
        }
        /*Dialog progressDialog = getProgressDialog(context);
        progressDialog.show();*/
        startRequest(observable, new Observer<RequestResult<T>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(RequestResult<T> result) {
                String message = result.message;
                if (result.isSuccess()) {
//                    listener.onSuccess(result.data);
                    CacheUtil.getInstance().onSuccess(observable, listener, result.data);
                } else if (result.isNotLogin()) {
                    /*if ("请先登录！".equals(message)) {
                        MyApplication.isShowNoLogin = true;
                    }*/
                    if (!MyApplication.isShowNoLogin /*|| "请先登录！".equals(message)*/) {
                        MyApplication.isShowNoLogin = true;
//                        listener.onError(new Exception(Config.NET_MESSAGE.NO_LOGIN));
                        CacheUtil.getInstance().onError(observable, listener, new Exception(Config.NET_MESSAGE.NO_LOGIN));
                        EventBus.getDefault().post(new EventMessage(Event.goToLogin));
                        EventBus.getDefault().post(new EventMessage(Event.loginError));
                        SessionUtil.getInstance().logout();
                    } else if (SessionUtil.getInstance().isLogin()) {
                        EventBus.getDefault().post(new EventMessage(Event.goToLogin));
                        SessionUtil.getInstance().logout();
                    }
                } else if (result.isFail()) {
//                    listener.onError(new Exception(message));
                    CacheUtil.getInstance().onError(observable, listener, new Exception(message));
                } else {
//                    listener.onError(new Exception("未知错误"));
                    CacheUtil.getInstance().onError(observable, listener, new Exception("未知错误"));
                }
            }

            @Override
            public void onError(Throwable e) {
                /*if (progressDialog.isShowing())
                    progressDialog.dismiss();*/
                Logger.e(e, "API Error.");
                if (e instanceof SocketTimeoutException) {
//                    listener.onError(new Exception("请求超时"));
                    CacheUtil.getInstance().onError(observable, listener, new Exception("请求超时"));
                } else if (e instanceof IllegalStateException) {
//                    listener.onError(new Exception("服务器数据异常"));
                    CacheUtil.getInstance().onError(observable, listener, new Exception("服务器数据异常"));
                } else if (e instanceof UnknownHostException) {
//                    listener.onError(new Exception("网络状态异常"));
                    CacheUtil.getInstance().onError(observable, listener, new Exception("网络状态异常"));
                } else {
                    if (!"Null is not a valid element".equals(e.getMessage()))
//                        listener.onError(e);
                        CacheUtil.getInstance().onError(observable, listener, e);
                }
            }

            @Override
            public void onComplete() {
//                listener.onComplete();
                CacheUtil.getInstance().onComplete(observable, listener);
                /*if (progressDialog.isShowing())
                    progressDialog.dismiss();*/
            }
        });
    }

    @NonNull
    public static RequestBody buildJsonBody(Serializable object) {
        return RequestBody.create(MediaType.parse("application/json;charset=utf-8"), new Gson().toJson(object));
    }


    public static Dialog getProgressDialog(Context context) {
        Dialog mProgressDialog = null;

        synchronized (APIManager.class) {
            if (mProgressDialog == null) {
                View view = LayoutInflater.from(context).inflate(R.layout.loading_layout, null);
                mProgressDialog = new Dialog(context, R.style.LoadingDialog);
                mProgressDialog.setContentView(view, new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.MATCH_PARENT));
                mProgressDialog.setCancelable(true);
            }
        }
        return mProgressDialog;
    }
}
