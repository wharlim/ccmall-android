package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;


public class H5WebView extends WebView {


    private H5WebChromeClient h5WebChromeClient;
    private WebViewInterceptUrlCallback webViewInterceptUrlCallback;

    public H5WebView(Context context) {
        super(context);
        initView();
    }

    public H5WebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public H5WebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public void setWebViewChangeTitleCallback(H5WebChromeClient.WebViewChangeTitleCallback callback) {
        this.h5WebChromeClient.setWebViewChangeTitleCallback(callback);
    }

    @Override
    public void setWebChromeClient(WebChromeClient client) {
        if(client instanceof H5WebChromeClient){
            h5WebChromeClient = (H5WebChromeClient) client;
        }
        super.setWebChromeClient(client);
    }

    void initView(){

        h5WebChromeClient = new H5WebChromeClient();
        WebSettings settings = getSettings();
        settings.setUseWideViewPort(true);
        settings.setJavaScriptEnabled(true);
        setWebChromeClient(h5WebChromeClient);

        settings.setPluginState(WebSettings.PluginState.ON);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setAllowFileAccess(true);
        settings.setDefaultTextEncodingName("UTF-8");
        settings.setGeolocationEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);

        settings.setAppCacheMaxSize(Long.MAX_VALUE);
//        String appCachePath = getContext().getCacheDir().getAbsolutePath();
//        settings.setAppCachePath(appCachePath);
        settings.setAppCacheEnabled(true);

        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setSupportMultipleWindows(true);
        // settings.setLoadWithOverviewMode(true);
        // settings.setDatabaseEnabled(true);
        // settings.setPageCacheCapacity(IX5WebSettings.DEFAULT_CACHE_CAPACITY);
        // settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d("H5WebView", "url: " + url);
                if (webViewInterceptUrlCallback != null) {
                    if (webViewInterceptUrlCallback.onInterceptUrl(url)) {
                        return true;
                    }
                }
                return super.shouldOverrideUrlLoading(view, url);
            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                String url = request.getUrl().toString();
                Log.d("H5WebView", "request url: " + url);
                if (webViewInterceptUrlCallback != null) {
                    if (webViewInterceptUrlCallback.onInterceptUrl(url)) {
                        return true;
                    }
                }
                return super.shouldOverrideUrlLoading(view, request);
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //适配5.0不允许http和https混合使用情况
            settings.setMixedContentMode(android.webkit.WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
    }


    public void setProgressbar(ProgressBar progressbar){
        h5WebChromeClient.setProgressbar(progressbar);
    }

    public void setWebViewInterceptUrlCallback(WebViewInterceptUrlCallback callback) {
        this.webViewInterceptUrlCallback = callback;
    }

    @Override
    public void loadUrl(String url) {
        super.loadUrl(url);
        h5WebChromeClient.setCurrentUrl(url);
    }

    public interface WebViewInterceptUrlCallback{
        boolean onInterceptUrl(String url);
    }

/*
    @Override
    protected boolean drawChild(Canvas canvas, View child, long drawingTime) {
        boolean ret = super.drawChild(canvas, child, drawingTime);
        canvas.save();
        Paint paint = new Paint();
        paint.setColor(0x7fff0000);
        paint.setTextSize(24.f);
        paint.setAntiAlias(true);
        if (getX5WebViewExtension() != null) {
            canvas.drawText(this.getContext().getPackageName() + "-pid:"
                    + android.os.Process.myPid(), 10, 50, paint);
            canvas.drawText(
                    "X5  Core:" + QbSdk.getTbsVersion(this.getContext()), 10,
                    100, paint);
        } else {
            canvas.drawText(this.getContext().getPackageName() + "-pid:"
                    + android.os.Process.myPid(), 10, 50, paint);
            canvas.drawText("Sys Core", 10, 100, paint);
        }
        canvas.drawText(Build.MANUFACTURER, 10, 150, paint);
        canvas.drawText(Build.MODEL, 10, 200, paint);
        canvas.restore();
        return ret;
    }*/
}