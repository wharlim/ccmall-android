package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.UpgradeProgress;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service.contract
 * @since 2017-08-03
 */
public interface IUpgradeService {

    @GET("stat/getUpProgress")
    Observable<RequestResult<UpgradeProgress>> getUpgradeProgress();
}
