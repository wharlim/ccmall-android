package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

import java.util.List;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/8/28.
 */
public class TopicLibraryModel extends BaseModel {


    /**
     * topicId : fd3c70a5dacf45d88899600bbe7d02c9
     * authorId : 34a561b32c664348bb88e0fb04647b25
     * status : 0
     * type : 2
     * content : 测试发布链接
     * imageUrls : ["http://39.108.50.110/G1/M00/00/82/rBIdXFuFISuAW7SrAAD_LGBcAxs330.jpg"]
     * linkTitle : 百度
     * linkUrl : https://www.baidu.com
     * createDate : 2018-08-28 17:52:40
     * statusStr : 审核中
     */

    @SerializedName("topicId")
    public String topicId;
    @SerializedName("authorId")
    public String authorId;
    /**
     * 审核状态，0审核中，1审核成功，2审核失败
     */
    @SerializedName("status")
    public int status;
    /**
     * 0空，1图文，3链接
     */
    @SerializedName("type")
    public int type;
    @SerializedName("content")
    public String content;
    @SerializedName("linkTitle")
    public String linkTitle;
    @SerializedName("linkUrl")
    public String linkUrl;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("statusStr")
    public String statusStr;
    @SerializedName("imageUrls")
    public List<String> imageUrls;
}
