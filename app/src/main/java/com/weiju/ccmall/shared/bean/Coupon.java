package com.weiju.ccmall.shared.bean;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.util.CarshReportUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.bean
 * @since 2017-06-20
 */
public class Coupon implements Serializable {
    @SerializedName("couponId")
    public String couponId;
    @SerializedName("platformCouponId")
    public String platformCouponId;

    /**
     * 获取优惠券可用列表时，应该使用的是platformCouponId，但是
     * 在领券中心，platformCouponId即是couponId
     * @return
     */
    public String getTheCouponId() {
        return TextUtils.isEmpty(platformCouponId) ? couponId : platformCouponId;
    }

    @SerializedName("couponType")
    public int couponType;
    @SerializedName("title")
    public String title;
    @SerializedName("cost")
    public long cost;
    @SerializedName("minOrderMoney")
    public long minOrderMoney;
    @SerializedName("productId")
    public String productId;
    @SerializedName("limitStartDate")
    public Date limitStartDate;
    @SerializedName("limitEndDate")
    public Date limitEndDate;
    @SerializedName("iconUrl")
    public String thumb;
    @SerializedName("storeId")
    public String storeId;
    @SerializedName("storeName")
    public String storeName;
    @SerializedName("receiveStatus")
    public int receiveStatus;
    @SerializedName("status")
    public boolean isUsed = Boolean.FALSE;
    public boolean isSelected = Boolean.FALSE;

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Coupon) {
            return ((Coupon) obj).couponId.equals(couponId);
        }
        return super.equals(obj);
    }

    public String getStatusText() {
        if (isUsed) {
            return "已使用，";
        }
        if (new Date().after(limitEndDate)) {
            return "已过期，";
        }
        return "";
    }

    public String getDateRange() {
        if (limitEndDate == null || limitStartDate == null) {
            return "";
        }
        String start = Constants.FORMAT_DATE_SIMPLE.format(limitStartDate);
        String end = Constants.FORMAT_DATE_SIMPLE.format(limitEndDate);
        return String.format("%s ~ %s", start, end);
    }

    public boolean couponCanUse() {
        if (limitEndDate == null || limitStartDate == null) {
            CarshReportUtils.post("优惠券数据异常" + couponId);
            return !isUsed;
        }
        Date now = new Date();
        return !(isUsed || now.after(limitEndDate));
    }

    public boolean userCanUse() {
        if (limitEndDate == null || limitStartDate == null) {
            CarshReportUtils.post("优惠券数据异常" + couponId);
            return false;
        }
        Date now = new Date();
        return !(isUsed || now.before(limitStartDate) || now.after(limitEndDate));
    }
}
