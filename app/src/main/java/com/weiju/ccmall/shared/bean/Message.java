package com.weiju.ccmall.shared.bean;

import android.text.TextUtils;

import com.google.common.base.Strings;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Arrays;

public class Message implements Serializable {

    @SerializedName("msgId")
    public String id;
    @SerializedName("id")
    public String memberId;
    @SerializedName("refundId")
    public String refundId;
    @SerializedName("type")
    public int type;
    @SerializedName("infoId")
    public String infoId;
    @SerializedName("thumbUrl")
    public String thumb;
    @SerializedName("title")
    public String title;
    @SerializedName("content")
    public String content;
    @SerializedName("createDate")
    public String createDate;

    public boolean isImageMessage() {
        return !TextUtils.isEmpty(thumb);
    }

    public boolean hasDetail() {
        return Arrays.asList(16, 17, 41, 44, 45, 46, 47, 48, 50, 51, 70, 71, 74, 98).contains(type);
    }

    public String getContent() {
        if (Strings.isNullOrEmpty(content)) {
            return "";
        }
        return content.replaceAll("\\n(\\s)*", "\n");
    }
}
