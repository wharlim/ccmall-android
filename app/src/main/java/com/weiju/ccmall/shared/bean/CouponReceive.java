package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
public class CouponReceive {

    /**
     * receiveId : bd799aac14d445a5aed96355b1bab386
     * couponId : 7dad1585889d496bb64a7addddebf36c
     * memberId : dac68adea0f44385ae2b84e03609a285
     * headImage : http://thirdwx.qlogo.cn/mmopen/vi_32/ngNm6nYYe7N0944HXQArIIuY9TVyn2VSHLCY1vb4FHiaVibicOE7p2WsnEX9IkxznCuLJhJiaHIlIKdvUElEh3Pc2Q/132
     * nickName : 少龙
     * liveId : dae42bcc1c0e463faec0adefe3ef2fc3
     * status : 0
     * balanceCode : 1331582436427887
     * createDate : 2020-02-20 22:31:12
     * updateDate : 2020-02-20 22:31:12
     * deleteFlag : 0
     */

    @SerializedName("receiveId")
    public String receiveId;
    @SerializedName("couponId")
    public String couponId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("liveId")
    public String liveId;
    @SerializedName("status")
    public int status;
    @SerializedName("balanceCode")
    public String balanceCode;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
    @SerializedName("title")
    public String title;
    @SerializedName("cost")
    public long cost;
    @SerializedName("limitEndDate")
    public String limitEndDate;


    /**
     * couponTitle :
     * cost :
     * limitStartDate :
     */

    @SerializedName("couponTitle")
    public String couponTitle;
    @SerializedName("limitStartDate")
    public Date limitStartDate;

    /**
     * sendId : 73b7d32919a44fca8a4a0a150bf11da3
     * cost : 3000
     * limitStartDateStr : 2020-05-28
     * limitEndDateStr : 2020-05-31
     * couponType : 1
     * skuId :
     * storeId : f86846867f644057ba13548983f7ac0d
     */

    @SerializedName("sendId")
    public String sendId;
    @SerializedName("limitStartDateStr")
    public String limitStartDateStr;
    @SerializedName("limitEndDateStr")
    public String limitEndDateStr;
    @SerializedName("couponType")
    public int couponType;
    @SerializedName("skuId")
    public String skuId;
    @SerializedName("storeId")
    public String storeId;
}
