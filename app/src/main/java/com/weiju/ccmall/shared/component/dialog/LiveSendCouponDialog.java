package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.chad.library.adapter.base.listener.OnItemLongClickListener;
import com.sobot.chat.widget.kpswitch.util.KeyboardUtil;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveCoupon;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.MaxHeightRecyclerView;
import com.weiju.ccmall.shared.component.adapter.LiveCouponAdapter;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.message.ChatroomCouponMessage;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.ChatroomKit;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;

/**
 * @author chenyanming
 * @date 2020/2/20.
 */
public class LiveSendCouponDialog extends Dialog {

    @BindView(R.id.recyclerView)
    MaxHeightRecyclerView mRecyclerView;
    @BindView(R.id.llContent)
    LinearLayout llContent;
    @BindView(R.id.tvBack)
    TextView mTvBack;
    @BindView(R.id.ivBack)
    ImageView mIvBack;
    @BindView(R.id.layoutBackgroud)
    LinearLayout mLayoutBackgroud;
    @BindView(R.id.layoutDetail)
    LinearLayout mLayoutDetail;
    @BindView(R.id.etNum)
    EditText mEtNum;
    @BindView(R.id.tvSelectTip)
    TextView mTvSelectTip;
    @BindView(R.id.tvTip)
    TextView tvTip;
    @BindView(R.id.tvTtile)
    TextView mTvTitle;
    @BindView(R.id.tvCloseTip)
    TextView mTvCloseTip;
    @BindView(R.id.tvResultTip)
    TextView mTvResultTip;
    @BindView(R.id.layoutResult)
    LinearLayout mLayoutResult;
    @BindView(R.id.tvMoney)
    TextView mTvMoney;
    @BindView(R.id.tvContent)
    TextView mTvContent;
    @BindView(R.id.tvDate)
    TextView mTvDate;
    @BindView(R.id.tvStock)
    TextView mTvStock;
    @BindView(R.id.ivResult)
    ImageView mIvResult;

    private LiveCouponAdapter mLiveCouponAdapter;
    private String mLiveId;
    private final Activity mContext;


    private boolean mCanDissmiss;
    private int mStock;
    private String mCouponId;

    public LiveSendCouponDialog(@NonNull Activity context, String liveId) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mLiveId = liveId;
    }


    private int COUPON_LIST = 0;
    private int COUPON_DETAIL = 1;
    private int COUPON_RESULT = 2;
    private int mType = COUPON_LIST;

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_send_coupon);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.dimAmount = 0.0f;
        getWindow().setAttributes(layoutParams);
        initView();
        initData();
    }

    private void initData() {
        APIManager.startRequest(mService.getAllLiveCouponByLiveId(mLiveId), new BaseRequestListener<List<LiveCoupon>>(mContext) {
            @Override
            public void onSuccess(List<LiveCoupon> result) {
                mLiveCouponAdapter.setNewData(result);
                if (result.size() == 0) {
                    mLayoutBackgroud.setBackground(mContext.getResources().getDrawable(R.drawable.img_live_send_coupon_detail));
                }
                if (mLiveCouponAdapter.getData().size() > 0 && tvTip.getVisibility() == View.GONE) {
                    tvTip.setVisibility(View.VISIBLE);
                }
            }
        }, getContext());
    }

    private void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mLiveCouponAdapter = new LiveCouponAdapter();
        mRecyclerView.setAdapter(mLiveCouponAdapter);

        View inflate = View.inflate(mContext, R.layout.cmp_coupon_no_data, null);
        mLiveCouponAdapter.setEmptyView(inflate);

        mRecyclerView.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                LiveCoupon item = mLiveCouponAdapter.getItem(position);
                if (null != item) {
                    mStock = item.stock;
                    mCouponId = item.couponId;
                    mTvMoney.setText(MoneyUtil.centToYuan¥StrNoZero(item.cost));
                    mTvContent.setText(item.title);
                    mTvDate.setText(String.format("有效期：%s", item.limitEndDate));
                    mTvStock.setText(String.format("库存%s", item.stock));
                    showCouponDetail();
                }
            }
        });

        mRecyclerView.addOnItemTouchListener(new OnItemLongClickListener() {
            @Override
            public void onSimpleItemLongClick(BaseQuickAdapter adapter, View view, int position) {
                LiveCoupon item = mLiveCouponAdapter.getItem(position);
                if (null != item) {
                    mStock = item.stock;
                    mCouponId = item.couponId;
                    mTvMoney.setText(MoneyUtil.centToYuan¥StrNoZero(item.cost));
                    mTvContent.setText(item.title);
                    mTvDate.setText(String.format("有效期：%s", item.limitEndDate));
                    mTvStock.setText(String.format("库存%s", item.stock));
                    showCouponDetail();
                }
            }
        });
    }

    private void showCouponDetail() {
        mType = COUPON_DETAIL;
        mEtNum.setText("");
        llContent.setVisibility(View.GONE);
        mTvBack.setVisibility(View.GONE);
        mIvBack.setVisibility(View.VISIBLE);
        mLayoutBackgroud.setBackground(mContext.getResources().getDrawable(R.drawable.img_live_send_coupon_detail));
        mLayoutDetail.setVisibility(View.VISIBLE);
    }

    @OnClick({R.id.tvBack, R.id.ivClose})
    protected void close() {
        dismiss();
    }

    @OnClick(R.id.ivBack)
    protected void back() {
        if (mType == COUPON_DETAIL) {
            mType = COUPON_LIST;
            KeyboardUtil.hideKeyboard(mEtNum);
            llContent.setVisibility(View.VISIBLE);
            mTvBack.setVisibility(View.VISIBLE);
            mIvBack.setVisibility(View.GONE);
            mLayoutBackgroud.setBackground(mContext.getResources().getDrawable(R.drawable.img_live_send_coupon_list));
            mLayoutDetail.setVisibility(View.GONE);
        } else if (mType == COUPON_RESULT) {
            mType = COUPON_DETAIL;
            mCanDissmiss = false;
            mLayoutResult.setVisibility(View.GONE);
            mTvTitle.setText("发放优惠券");
            mTvSelectTip.setVisibility(View.VISIBLE);
            mLayoutDetail.setVisibility(View.VISIBLE);
            mLayoutBackgroud.setBackground(mContext.getResources().getDrawable(R.drawable.img_live_send_coupon_detail));

        }
    }

    @OnClick(R.id.ivSend)
    protected void send() {
        String num = mEtNum.getText().toString();
        if (TextUtils.isEmpty(num)) {
            ToastUtil.error("请输入张数");
            return;
        }

        if (Integer.parseInt(num) > mStock) {
            ToastUtil.error("库存不足");
            return;
        }
        KeyboardUtil.hideKeyboard(mEtNum);
        //发放成功后手动扣减库存

        sendCoupon(Integer.parseInt(num));

    }

    private void sendCoupon(int sendNum) {
        APIManager.startRequest(mService.sendLiveCoupon(mLiveId, sendNum, mCouponId, null), new BaseRequestListener<LiveCoupon>(mContext) {
            @Override
            public void onSuccess(LiveCoupon result) {
                EventBus.getDefault().post(new EventMessage(Event.sendCouponSuccess));
                ChatroomCouponMessage chatroomRedMessage = new ChatroomCouponMessage();
                chatroomRedMessage.setCouponId(mCouponId);
                chatroomRedMessage.setSendId(result.sendId);
                ChatroomKit.sendMessage(chatroomRedMessage, true);

                initData();
                mStock -= sendNum;
                mTvStock.setText(String.format("库存%s", mStock));
                showResult(true);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                showResult(false);
            }
        }, getContext());
    }

    private void showResult(boolean result) {
        mType = COUPON_RESULT;
        mLayoutResult.setVisibility(View.VISIBLE);
        mLayoutDetail.setVisibility(View.GONE);
        mTvSelectTip.setVisibility(View.GONE);
        mTvTitle.setText(result ? "赠送成功" : "赠送失败");
        mTvCloseTip.setVisibility(result ? View.VISIBLE : View.INVISIBLE);
        mTvResultTip.setText(result ? "赠送成功，快点通知小伙伴领取吧~" : "赠送失败，请重新赠送优惠券");
        mLayoutBackgroud.setBackground(mContext.getResources().getDrawable(R.drawable.img_live_send_coupon_result));
        mIvResult.setImageResource(result ? R.drawable.ic_live_send_coupon_success : R.drawable.ic_live_send_coupon_error);

        if (result) {
            mCanDissmiss = true;
            Observable
                    .interval(0, 1, TimeUnit.SECONDS)
                    .take(3)
                    .observeOn(AndroidSchedulers.mainThread())
                    .map(new Function<Long, Long>() {
                        @Override
                        public Long apply(@NonNull Long aLong) throws Exception {
                            return 3 - aLong;

                        }
                    })
                    .subscribe(new Observer<Long>() {

                        @Override
                        public void onSubscribe(@NonNull Disposable d) {

                        }

                        @Override
                        public void onNext(@NonNull Long aLong) {

                        }

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }

                        @Override
                        public void onComplete() {
                            if (mCanDissmiss) {
                                dismiss();
                            }
                        }
                    });
        }
    }
}

