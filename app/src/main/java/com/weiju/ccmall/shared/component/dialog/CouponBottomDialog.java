package com.weiju.ccmall.shared.component.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseCallback;
import com.weiju.ccmall.shared.bean.Coupon;
import com.weiju.ccmall.shared.service.CouponService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/8/10.
 */
public class CouponBottomDialog extends BottomSheetDialog {


    @BindView(R.id.layoutCoupon)
    LinearLayout mLayoutCoupon;
    @BindView(R.id.tvSubmit)
    TextView mTvSubmit;

    public CouponBottomDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_coupon_bottom);
        ButterKnife.bind(this);
    }

    public void setData(List<Coupon> couponList) {
//        mCoupon = coupon;
        for (final Coupon coupon : couponList) {
            View couponView = View.inflate(getContext(), R.layout.item_coupon_center, null);
            SimpleDraweeView itemThumbIv = couponView.findViewById(R.id.itemThumbIv);
            FrescoUtil.setImage(itemThumbIv, coupon.thumb);
            TextView tvTitle = couponView.findViewById(R.id.itemTitleTv);
            tvTitle.setText(coupon.title);
            TextView tvPrice = couponView.findViewById(R.id.itemPriceTv);
            tvPrice.setText(ConvertUtil.centToCurrency(getContext(), coupon.cost));
            final TextView tvGetCoupon = couponView.findViewById(R.id.getCouponBtn);
            setBtnStatus(tvGetCoupon, coupon);
            TextView tvDesc = couponView.findViewById(R.id.itemDescTv);
            tvDesc.setText(String.format("满%s减%s", ConvertUtil.cent2yuanNoZero(coupon.minOrderMoney), ConvertUtil.cent2yuanNoZero(coupon.cost)));
            tvDesc.setVisibility(StringUtils.isEmpty(coupon.productId) ? View.VISIBLE : View.GONE);
            tvGetCoupon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getCoupon(tvGetCoupon, coupon);
                }
            });
            mLayoutCoupon.addView(couponView);
            mLayoutCoupon.addView(new View(getContext()), new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, SizeUtils.dp2px(10)));
        }
    }

    public void getCoupon(final TextView tvGetCoupon, final Coupon coupon) {
        CouponService.getCoupon(getContext(), coupon.couponId, new BaseCallback<Object>() {
            @Override
            public void callback(Object data) {
                dismiss();
                coupon.receiveStatus = 1;
                setBtnStatus(tvGetCoupon, coupon);

            }
        });
    }

    private void setBtnStatus(TextView btn, Coupon coupon) {
        if (coupon.receiveStatus == 1) {
            btn.setEnabled(false);
            btn.setText("已领取");
        } else {
            btn.setEnabled(true);
            btn.setText("立刻领取");
        }
    }

    @OnClick(R.id.tvSubmit)
    public void onMTvSubmitClicked() {
        dismiss();
    }
}
