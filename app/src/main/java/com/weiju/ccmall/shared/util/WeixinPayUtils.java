package com.weiju.ccmall.shared.util;

import android.app.Activity;
import android.os.SystemClock;

import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.pay.PayMsg;
import com.weiju.ccmall.shared.bean.WxPayModel;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IPayService;


import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

/**
 * 本地加密的原生微信支付工具类
 * 在本地预下单，然后调用唤起微信支付，最后结果将回调在WXPayEntryActivity页面，请监听 onResp 方法
 * Created by zjm on 2018/3/30.
 */
public class WeixinPayUtils {

    private static final String WX_APPID = BuildConfig.WX_APP_ID;
    private static final String WX_MCH_ID = "1527922411";
    private static final String WX_PAY_KEY = "d5304abf1cca2b486b102ad57ac982d8";

    private static final String WX_NOTIFY_URL = BaseUrl.getInstance().getBaseUrl() + "wxnotify/wxapppay";
    private static IWXAPI sWxapi;
    private static String sAppName;

    private static IWXAPI initWePay(final Activity context) {
        sWxapi = WXAPIFactory.createWXAPI(context, WX_APPID, true);
        sWxapi.registerApp(WX_APPID);
        sAppName = context.getString(R.string.appName);
        return sWxapi;
    }

    /**
     * @param context
     * @param money   金额 单位：分
     * @param orderNo
     */
    public static void pay(final Activity context, long money, String orderNo) {
        if (sWxapi == null) {
            initWePay(context);
        }
        HashMap<String, String> params = buildParamsToPay(money, orderNo, WX_NOTIFY_URL);
        IPayService payService = ServiceManager.getInstance().createService(IPayService.class);
        RequestBody requestBody = RequestBody.create(MediaType.parse("text/xml;ChartSet=utf-8"), StringUtil.hashMap2Xml(params));
        APIManager.startRequest(payService.unifiedOrder(requestBody), new Observer<ResponseBody>() {

                    @Override
                    public void onSubscribe(Disposable d) {
                        ToastUtil.showLoading(context);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        try {
                            startPay(responseBody.string());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        EventBus.getDefault().post(new PayMsg(PayMsg.ACTION_WXPAY_FAIL, "微信支付预下单接口异常"));
                    }

                    @Override
                    public void onComplete() {
                        ToastUtil.hideLoading();
                    }
                }
        );
    }

    /**
     * 调起微信支付
     *
     * @param xmlStr 微信服务器返回的 xml
     */
    private static void startPay(String xmlStr) {
        try {
            HashMap<String, String> hashMap = StringUtil.xml2HashMap(xmlStr);
            Gson gson = new Gson();
            String json = gson.toJson(hashMap);
            WxPayModel prePayResponse = gson.fromJson(json, WxPayModel.class);
            if (!prePayResponse.isSuccess()) {
                throw new Exception(prePayResponse.returnMsg);
            }

            PayReq request = new PayReq();
            request.appId = prePayResponse.appid;
            request.partnerId = prePayResponse.mchId;
            request.prepayId = prePayResponse.prepayId;
            request.packageValue = "Sign=WXPay";
            request.nonceStr = prePayResponse.nonceStr;
            request.timeStamp = String.valueOf(SystemClock.currentThreadTimeMillis() / 1000);
            request.sign = buildSign(request);
            sWxapi.sendReq(request);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
//            ToastUtils.showShortToast(e.getMessage());
        }
    }


    private static HashMap<String, String> buildParamsToPay(long money, String orderNo, String notifyUrl) {
        HashMap<String, String> params = new HashMap<>(11);
        params.put("appid", WX_APPID);
        params.put("attach", sAppName + "支付订单：" + orderNo);
        params.put("body", "订单号：" + orderNo);
        params.put("mch_id", WX_MCH_ID);
        params.put("nonce_str", StringUtil.randomString());
        params.put("notify_url", notifyUrl);
        params.put("out_trade_no", String.format("%s%s", orderNo, System.currentTimeMillis() / 1000));
        params.put("spbill_create_ip", "192.168.0.1");
        params.put("total_fee", String.valueOf(money));
        params.put("trade_type", "APP");
        params.put("sign", buildSign(params));
        return params;
    }

    private static String buildSign(PayReq request) {
        HashMap<String, String> hashMap = new HashMap<>(6);
        hashMap.put("appid", request.appId);
        hashMap.put("partnerid", request.partnerId);
        hashMap.put("prepayid", request.prepayId);
        hashMap.put("package", request.packageValue);
        hashMap.put("noncestr", request.nonceStr);
        hashMap.put("timestamp", request.timeStamp);
        return buildSign(hashMap);
    }

    private static String buildSign(HashMap<String, String> params) {
        Object[] keys = params.keySet().toArray();
        Arrays.sort(keys);
        List<String> pieces = new ArrayList<>();
        for (Object key : keys) {
            pieces.add(key + "=" + params.get(String.valueOf(key)));
        }
        pieces.add("key=" + WX_PAY_KEY);
        String str = Joiner.on("&").join(pieces);
        return StringUtil.md5(str).toUpperCase();
    }
}
