package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/4/29 on 10:43
 * @desc ${TODD}
 */
public class ExchGoldTicket  {


    /**
     * dayRatio : 101
     * coin : 248910139426
     * goldTicket : 251389
     */

    @SerializedName("dayRatio")
    public long dayRatio;
    @SerializedName("coin")
    public long coin;
    @SerializedName("goldTicket")
    public long goldTicket;
}
