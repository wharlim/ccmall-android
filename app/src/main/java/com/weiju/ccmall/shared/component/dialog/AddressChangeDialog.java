package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.adapter.AddressChangeAdapter;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Address;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveStoreService;
import com.weiju.ccmall.shared.util.CommonUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressChangeDialog extends Dialog {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout refreshLayout;
    private OnDialogClickListener mListener;


    private int mCurrentPage = 1;
    private AddressChangeAdapter mAdapter;
    private Activity mContext;
    private String mAddressId;
    private ILiveStoreService mService = ServiceManager.getInstance().createService(ILiveStoreService.class);

    public AddressChangeDialog(Activity context, String addressId) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mAddressId = addressId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_address_change);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.CENTER);
        setCanceledOnTouchOutside(false);
        initView();
        initData();
    }

    private void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(linearLayoutManager);
//        recyclerView.setHasFixedSize(false);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        refreshLayout.setOnRefreshListener(() -> getAddressList(true));

        mAdapter = new AddressChangeAdapter();
        View inflate = View.inflate(mContext, R.layout.cmp_no_data, null);
        mAdapter.setEmptyView(inflate);
        mAdapter.setOnLoadMoreListener(() -> getAddressList(false), recyclerView);
        mAdapter.setHeaderAndEmpty(true);
        mAdapter.setFooterViewAsFlow(true);

        recyclerView.addItemDecoration(new ListDividerDecoration(mContext));
        recyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener((adapter, view, position) -> {
//                EventBus.getDefault().post(new EventMessage(Event.openNewActivity));
            List<Address> data = mAdapter.getData();
            Address addressSelect = data.get(position);
            if (!addressSelect.isSelect) {
                for (int i = 0; i < data.size(); i++) {
                    Address address = data.get(i);
                    if (i == position) {
                        address.isSelect = true;
                        mAddressId = address.addressId;
                    } else {
                        address.isSelect = false;
                    }
                }
                mAdapter.notifyDataSetChanged();
            }
            if (mListener != null) {
                mListener.onSelectAddress(addressSelect);
            }
        });
    }

    private void initData() {
        getAddressList(true);
    }

    private void getAddressList(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 1;
        } else {
            mCurrentPage++;
        }
        APIManager.startRequest(mService.getList(mCurrentPage, 15),
                new BaseRequestListener<PaginationEntity<Address, Object>>(isRefresh ? refreshLayout : null) {
                    @Override
                    public void onSuccess(PaginationEntity<Address, Object> result) {
                        super.onSuccess(result);
                        for (Address address : result.list) {
                            address.isSelect = address.addressId.equals(mAddressId);
                        }
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, mContext);
    }

    @OnClick(R.id.tvManageAddress)
    public void onChangeAddress() {
        if (mListener != null) {
            mListener.onManageAddress();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void selectAddress(EventMessage message) {
        if (message.getEvent() == Event.saveAddress || message.getEvent() == Event.deleteAddress) {
            getAddressList(true);
        }
    }

    public void setOnDialogClickListener(OnDialogClickListener listener) {
        mListener = listener;
    }

    public interface OnDialogClickListener {
        void onManageAddress();

        void onSelectAddress(Address address);
    }

}
