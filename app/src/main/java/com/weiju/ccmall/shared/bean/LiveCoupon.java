package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @date 2020/2/21.
 */
public class LiveCoupon {

    /**
     * sendId : 0158824bf9bc42c5b1a971dd52dbc47a
     * couponId : 7dad1585889d496bb64a7addddebf36c
     * liveId : dae42bcc1c0e463faec0adefe3ef2fc3
     * memberId : af8d785b384ad9281286275095141f15
     * title : 直播间优惠券
     * sendNum : 10
     * remark :
     * status : 0
     * receiveStatus : 1
     * balanceCode : 0211582435875511
     * receiveNum : 1
     * createDate : 2020-02-20 22:21:39
     * updateDate : 2020-02-20 22:31:08
     * deleteFlag : 0
     */

    @SerializedName("sendId")
    public String sendId;
    @SerializedName("couponId")
    public String couponId;
    @SerializedName("liveId")
    public String liveId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("title")
    public String title;
    @SerializedName("sendNum")
    public int sendNum;
    @SerializedName("remark")
    public String remark;
    /**
     * 当前发放优惠券活动的状态0=默认，1=全部被领取
     */
    @SerializedName("status")
    public int status;
    /**
     * 当前用户此前的领取状态: 0=默认, 1=已领取过
     */
    @SerializedName("receiveStatus")
    public int receiveStatus;
    @SerializedName("balanceCode")
    public String balanceCode;
    @SerializedName("receiveNum")
    public int receiveNum;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
    @SerializedName("cost")
    public long cost;


    /**
     * couponType : 0
     * cost : 10000
     * minOrderMoney : 0
     * productId : 4d419905d3c54979b216f0ee3ab7f19d
     * limitStartDate : 2019-10-29 08:22:00
     * limitEndDate : 2019-11-29 09:22:59
     * iconUrl : https://static.create-chain.net/ccmall/79/74/c8/553179367ff34f61af4bebc16b5f342c.png
     * stock : 10
     * storeId :
     * storeName :
     * acceptStartDate : 2019-10-29 08:22:00
     * acceptEndDate : 2019-11-29 09:22:59
     */

    @SerializedName("couponType")
    public int couponType;
    @SerializedName("minOrderMoney")
    public int minOrderMoney;
    @SerializedName("productId")
    public String productId;
    @SerializedName("limitStartDate")
    public String limitStartDate;
    @SerializedName("limitEndDate")
    public String limitEndDate;
    @SerializedName("iconUrl")
    public String iconUrl;
    @SerializedName("stock")
    public int stock;
    @SerializedName("storeId")
    public String storeId;
    @SerializedName("storeName")
    public String storeName;
    @SerializedName("acceptStartDate")
    public String acceptStartDate;
    @SerializedName("acceptEndDate")
    public String acceptEndDate;

    @SerializedName("couponTitle")
    public String couponTitle;
}
