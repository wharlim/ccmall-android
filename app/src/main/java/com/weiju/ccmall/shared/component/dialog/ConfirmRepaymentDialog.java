package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.component
 * @since 2017-08-03
 */
public class ConfirmRepaymentDialog extends Dialog {

    @BindView(R.id.cancelBtn)
    protected TextView mCancelBtn;
    @BindView(R.id.confirmBtn)
    protected TextView mConfirmBtn;

    protected View.OnClickListener mConfirmListener;
    protected View.OnClickListener mCancelListener;
    @BindView(R.id.contentTv)
    TextView mContentTv;


    public ConfirmRepaymentDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_confirm_repayment);
        ButterKnife.bind(this);
        String strMsg = "在满足完美账单前提下，<font color=#F9351B>当前生成的预览计划已超过最后还款日，</font>为防止逾期，建议您适当调大启动资金";
        mContentTv.setText(Html.fromHtml(strMsg));
    }

    public void setOnConfirmListener(@NonNull View.OnClickListener listener) {
        mConfirmListener = listener;
    }
    public void setOnBtnCancelListener(@NonNull View.OnClickListener listener) {
        mCancelListener = listener;
    }

    @OnClick(R.id.cancelBtn)
    protected void onClose(View view) {
        if (mCancelListener != null) {
            mCancelListener.onClick(view);
        }
        dismiss();
    }

    @OnClick(R.id.confirmBtn)
    protected void onConfirm(View view) {
        if (mConfirmListener != null) {
            mConfirmListener.onClick(view);
        }
        dismiss();
    }
}
