package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class JkpLevel {

    /**
     * isUp : 1
     * memberTkBean : {"memberId":"2a09557ef20509458320774cb073efe1","alimmPid":"","jdPid":0,"relationId":"2120068474","inviterCode":"","accountName":"t**6","jkpLevel":0}
     */

    @SerializedName("isUp")
    public int isUp;
    @SerializedName("memberTkBean")
    public MemberTkBeanBean memberTkBean;

    public static class MemberTkBeanBean {
        /**
         * memberId : 2a09557ef20509458320774cb073efe1
         * alimmPid :
         * jdPid : 0
         * relationId : 2120068474
         * inviterCode :
         * accountName : t**6
         * jkpLevel : 0
         */

        @SerializedName("memberId")
        public String memberId;
        @SerializedName("alimmPid")
        public String alimmPid;
        @SerializedName("jdPid")
        public int jdPid;
        @SerializedName("relationId")
        public String relationId;
        @SerializedName("inviterCode")
        public String inviterCode;
        @SerializedName("accountName")
        public String accountName;
        @SerializedName("jkpLevel")
        public int jkpLevel;
    }
}
