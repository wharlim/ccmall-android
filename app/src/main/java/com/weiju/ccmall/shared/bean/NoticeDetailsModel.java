package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/7/20.
 */
public class NoticeDetailsModel {


    @SerializedName("notesId")
    public String notesId;
    @SerializedName("title")
    public String title;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("content")
    public String content;
}
