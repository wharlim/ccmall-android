package com.weiju.ccmall.shared.bean;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.R;

import java.io.Serializable;
import java.util.List;

/**
 * @author chenyanming
 * @time 2019/11/22 on 17:46
 * @desc ${TODD}
 */
public class LiveRoom implements Serializable {
    /**
     * liveId : 0b1379ca586c4de29677d446587bcaa6
     * memberId : 58a6ee26caa1451baf78abb4150ba92e
     * userId : 41474326
     * webinarId : 133611932
     * startTime : 2019-11-22 11:35:00
     * endTime : null
     * maxOnlineNum : 0
     * liveImage : https://static.mingpinvip.cn/qqmpapp/25/fc/81/25fc813087e6487bb627ca2d2e700736.jpg
     * liveDoc :
     * livePassword :
     * title : 畅所欲言
     * status : 0
     * statusStr : 未开始
     * intro :
     * topics :
     * type : 0
     * autoRecord : 1
     * isChat : 0
     * host :
     * buffer : 3
     * isAllowExtension : 1
     * recordId :
     * createDate : 2019-11-22 11:39:12
     * updateDate : 2019-11-22 11:39:12
     * deleteFlag : 0
     */

    @SerializedName("liveId")
    public String liveId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("userId")
    public String userId;
    @SerializedName("webinarId")
    public String webinarId;
    @SerializedName("startTime")
    public String startTime;
    @SerializedName("endTime")
    public String endTime;
    @SerializedName("maxOnlineNum")
    public int maxOnlineNum;
    @SerializedName("liveImage")
    public String liveImage;
    @SerializedName("liveDoc")
    public String liveDoc;
    @SerializedName("livePassword")
    public String livePassword;
    @SerializedName("livePasswordDecode")
    public String livePasswordDecode;

    public boolean hasPassword() {
        return !TextUtils.isEmpty(livePassword);
    }

    @SerializedName("title")
    public String title;
    @SerializedName("nickName")
    public String nickName;
    /**
     * 0=未开始，1=直播中，2=结束，3=回放 4=预告
     */
    @SerializedName("status")
    public int status;

    public String getStatusStr() {
        String ret = "";
        switch (status) {
            case 0:
                ret = "";
                break;
            case 1:
                ret = "直播中";
                break;
            case 2:
                ret = "结束";
                break;
            case 3:
                ret = "回放";
                break;
            case 4:
                ret = "预告";
                break;
                default:
        }
        return ret;
    }

    public int getStatusBg() {
        int bgRes = R.drawable.bg_live_state_running;
        switch (status) {
            case 1:
                bgRes = R.drawable.bg_live_state_running;
                break;
            case 3:
                bgRes = R.drawable.bg_live_state_replay;
                break;
            case 4:
                bgRes = R.drawable.bg_live_state_notice;
                break;
        }
        return bgRes;
    }

    @SerializedName("statusStr")
    public String statusStr;
    @SerializedName("intro")
    public String intro;
    @SerializedName("topics")
    public String topics;
    @SerializedName("type")
    public int type;
    @SerializedName("autoRecord")
    public int autoRecord;
    @SerializedName("isChat")
    public int isChat;
    @SerializedName("host")
    public String host;
    @SerializedName("buffer")
    public int buffer;
    @SerializedName("isAllowExtension")
    public int isAllowExtension;
    @SerializedName("recordId")
    public String recordId;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;


    @SerializedName("recommendId")
    public String recommendId;
    @SerializedName("sortIndex")
    public int sortIndex;
    @SerializedName("image")
    public String image;

    @SerializedName("posterHeight")
    public int posterHeight;
    @SerializedName("poster")
    public String poster;

    @SerializedName("pageId")
    public String pageId;

    @SerializedName("height")
    public int height;


    /**
     * realStartTime : null
     * endTime : null
     * likeNum : 0
     * likeStatus : 0
     * pullUrl : null
     * recordUrl : http://sgcoss.weijuit.com/record/qqmp/a8cc278000454c688c04105783439098/2019-12-19-15-53-14_2019-12-19-15-56-58.mp4
     * statusStr : null
     */

    @SerializedName("realStartTime")
    public Object realStartTime;
    @SerializedName("likeNum")
    public int likeNum;
    @SerializedName("likeStatus")
    public int likeStatus;
    @SerializedName("pullUrl")
    public PullUrl pullUrl;
    @SerializedName("recordUrl")
    public List<RecordUrl> recordUrl;

    @SerializedName("liveStatus")
    public int liveStatus;

    @SerializedName("onlineStatus")
    public int onlineStatus; // 上架状态，0下架，1上架
    @SerializedName("isForecast")
    public int isForecast; // 是否预告，1为预告，0不是

    public boolean isForecast() {
        return status == 4;
    }

    @SerializedName("forecastTime")
    public String forecastTime;

    @SerializedName("popularity") // 直播人气
    public String popularity;
}
