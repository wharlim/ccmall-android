package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class MemberStore {

    @SerializedName("storeId")
    public String storeId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("sobotId")
    public String sobotId;
    @SerializedName("status")
    public int status;
    @SerializedName("statusStr")
    public String statusStr;
    @SerializedName("storeName")
    public String storeName;
    @SerializedName("bannerImage")
    public String bannerImage;
    @SerializedName("thumbUrl")
    public String thumbUrl;
    @SerializedName("contact")
    public String contact;
    @SerializedName("phone")
    public String phone;
    @SerializedName("province")
    public String province;
    @SerializedName("city")
    public String city;
    @SerializedName("district")
    public String district;
    @SerializedName("address")
    public String address;
    @SerializedName("saleProductCount")
    public int saleProductCount;
    @SerializedName("saleCount")
    public int saleCount;
    @SerializedName("expressName")
    public String expressName;
    @SerializedName("shipAddress")
    public String shipAddress;
    @SerializedName("descScore")
    public int descScore;
    @SerializedName("expressScore")
    public int expressScore;
    @SerializedName("serveScore")
    public int serveScore;
    @SerializedName("wxQrCode")
    public String wxQrCode;
    @SerializedName("reason")
    public String reason;
    @SerializedName("shipType")
    public int shipType;
}
