package com.weiju.ccmall.shared.util;

import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.orhanobut.logger.Logger;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendMessageToWX;
import com.tencent.mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.mm.opensdk.modelmsg.WXWebpageObject;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.shared.basic.BaseCallback;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.PrePayResponse;
import com.weiju.ccmall.shared.bean.ShareObject;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IPayService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.util
 * @since 2017-07-06
 */
public class WechatUtil {


    public static IWXAPI newWxApi(Context context) {
        IWXAPI wxApi = WXAPIFactory.createWXAPI(context, BuildConfig.WX_APP_ID, false);
        wxApi.registerApp(BuildConfig.WX_APP_ID);
        return wxApi;
    }


    public static void compileResponse(BaseResp baseResp, @Nullable BaseCallback<BaseResp> successCallback) {
        compileResponse(baseResp, successCallback, null);
    }

    public static void compileResponse(BaseResp baseResp, @Nullable BaseCallback<BaseResp> successCallback, @Nullable BaseCallback<BaseResp> failureCallback) {
        if (baseResp.errCode == BaseResp.ErrCode.ERR_OK) {
            if (successCallback != null) {
                successCallback.callback(baseResp);
            }
        } else {
            if (failureCallback != null) {
                failureCallback.callback(baseResp);
            }
        }
    }
}
