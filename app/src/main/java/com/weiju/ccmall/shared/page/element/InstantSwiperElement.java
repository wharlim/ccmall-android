package com.weiju.ccmall.shared.page.element;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.TimeUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.component.CountDown;
import com.weiju.ccmall.shared.decoration.SpacesItemHorizontalDecoration;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.page.bean.InstantSwiperData;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ViewConstructor")
public class InstantSwiperElement extends LinearLayout {

    private CountDown mCountDown;
    private TextView mTitleTv;
    private TextView mMoreBtn;
    private TextView mTvTag;
    private RecyclerView mRecyclerView;

    public InstantSwiperElement(final Context context, final Element element) {
        super(context);
        View view = inflate(getContext(), R.layout.el_instant_swiper_layout, this);
        element.setBackgroundInto(view);
        mTitleTv = (TextView) view.findViewById(R.id.eleTitleTv);
        mCountDown = (CountDown) view.findViewById(R.id.eleCountDown);
        mTvTag = (TextView) view.findViewById(R.id.tvTag);
        mMoreBtn = view.findViewById(R.id.eleMoreTv);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        setData(context, element);
    }

    private void setData(final Context context, Element element) {
        final InstantSwiperData instantSwiperData = InstantSwiperData.json2Model(element.data);
        if(instantSwiperData.secondKill!=null){
            mTitleTv.setText(instantSwiperData.secondKill.title);
            mCountDown.setTimeLeft(getCountTime(instantSwiperData.secondKill), null);
            mMoreBtn.setVisibility(VISIBLE);
            mMoreBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    EventUtil.viewInstant(context, instantSwiperData.secondKill.secondKillId, false);
                }
            });
        }



        mRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        SwiperAdapter swiperAdapter = new SwiperAdapter();
        mRecyclerView.addItemDecoration(new SpacesItemHorizontalDecoration(ConvertUtil.dip2px(10), true));
        mRecyclerView.setAdapter(swiperAdapter);
        swiperAdapter.setItems(instantSwiperData.secondKillProducts);
        swiperAdapter.notifyDataSetChanged();
    }

    private long getCountTime(InstantSwiperData.SecondKillEntity secondKill) {
        long startDate = TimeUtils.string2Millis(secondKill.startDate);
        long endDate = TimeUtils.string2Millis(secondKill.endDate);
        long nowTimeDate = TimeUtils.getNowTimeDate().getTime();
        if (startDate > nowTimeDate) {
            mTvTag.setText("距离开始");
            return startDate - nowTimeDate;
        } else if (nowTimeDate < endDate) {
            mTvTag.setText("距离结束");
            return endDate - nowTimeDate;
        } else {
            mTvTag.setText("已结束");
            mCountDown.setVisibility(GONE);
            return 0;
        }
    }


    private class SwiperAdapter extends RecyclerView.Adapter<ViewHolder> {

        private List<SkuInfo> items = new ArrayList<>();

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.el_swiper_item, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final SkuInfo skuInfo = items.get(position);
            FrescoUtil.setImgMask(holder.mThumbIv, skuInfo.thumb, skuInfo.status, skuInfo.stock);
            holder.mTitleTv.setText(skuInfo.name);
            holder.mPriceTv.setText(ConvertUtil.centToCurrency(getContext(), skuInfo.retailPrice));
            TextViewUtil.addThroughLine(holder.mMarkPriceTv);
            holder.mMarkPriceTv.setText(ConvertUtil.centToCurrency(getContext(), skuInfo.marketPrice));
            holder.mTvCCM.setText(String.format("奖%s%%算率", skuInfo.countRateExc));
            holder.itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventUtil.viewProductDetail(getContext(), skuInfo.skuId, false);
                }
            });

            holder.mBanjiaView.setVisibility(View.GONE);
        }

        @Override
        public int getItemCount() {
            return items!=null?items.size():0;
        }

        void setItems(@NonNull List<SkuInfo> items) {
            this.items = items;
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemThumbIv)
        SimpleDraweeView mThumbIv;
        @BindView(R.id.itemTitleTv)
        TextView mTitleTv;
        @BindView(R.id.itemPriceTv)
        TextView mPriceTv;
        @BindView(R.id.itemMarkPriceTv)
        TextView mMarkPriceTv;
        @BindView(R.id.ivBanjia)
        protected ImageView mBanjiaView;
        @BindView(R.id.tvCCM)
        protected TextView mTvCCM;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
