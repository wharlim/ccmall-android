package com.weiju.ccmall.shared.util;

import android.app.Activity;
import android.util.Log;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.weiju.ccmall.shared.component.dialog.ShareDialog;

import java.io.File;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/9/8.
 */
public class ShareUtils {

    public static void showShareDialog(final Activity activity, String title, String desc, String logoUrl, String linke) {
        if (StringUtils.isEmpty(desc)) {
            desc = "     ";
        }
        new ShareDialog(activity, title, desc, logoUrl, linke, new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                LogUtils.e("onStart   ");

                ToastUtil.showLoading(activity);
            }

            @Override
            public void onResult(SHARE_MEDIA share_media) {
                LogUtils.e("onResult   ");

                ToastUtil.hideLoading();
            }

            @Override
            public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                LogUtils.e("onError   ");

                ToastUtil.hideLoading();
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media) {
                LogUtils.e("onCancel   ");

                ToastUtil.hideLoading();
            }

        }).show();
    }

    public static void showShareDialog(final Activity activity, String content) {
        new ShareDialog(activity, content, true, new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                LogUtils.e("onStart   ");
                ToastUtil.showLoading(activity);
            }

            @Override
            public void onResult(SHARE_MEDIA share_media) {
                LogUtils.e("onResult   ");

                ToastUtil.hideLoading();
            }

            @Override
            public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                LogUtils.e("onError   ");

                ToastUtil.hideLoading();
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media) {
                LogUtils.e("onCancel   ");

                ToastUtil.hideLoading();
            }
        }).show();
    }

    public static void share(Activity activity, String title, String desc, String logoUrl, String linke) {
        UMWeb web = new UMWeb(linke);
        web.setTitle(title);
        web.setDescription(desc);
        web.setThumb(new UMImage(activity, logoUrl));
//        showLoading();
        new ShareAction(activity).setPlatform(SHARE_MEDIA.WEIXIN).setCallback(new UMShareListener() {
            @Override
            public void onStart(SHARE_MEDIA share_media) {
                Log.e("err", "start");
            }

            @Override
            public void onResult(SHARE_MEDIA share_media) {
                Log.e("err", "result");
//                hideLoading();
            }

            @Override
            public void onError(SHARE_MEDIA share_media, Throwable throwable) {
                Log.e("err", "err" + throwable.getMessage());
//                hideLoading();
            }

            @Override
            public void onCancel(SHARE_MEDIA share_media) {
//                hideLoading();
            }
        }).withMedia(web).share();
    }

    public static void shareImg(Activity activity, File file, SHARE_MEDIA media, UMShareListener umShareListener) {
        UMImage image = new UMImage(activity, file);
        new ShareAction(activity).setPlatform(media).setCallback(umShareListener).withMedia(image).share();
    }
}
