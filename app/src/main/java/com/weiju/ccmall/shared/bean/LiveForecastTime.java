package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class LiveForecastTime {
    @SerializedName("forecastTime")
    public long forecastTime;
    @SerializedName("forecastDate")
    public String forecastDate;
}
