package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Property implements Serializable {
    @SerializedName("propertyId")
    public String id;
    @SerializedName("propertyName")
    public String name;
    @SerializedName("propertyValues")
    public List<PropertyValue> values;
}