package com.weiju.ccmall.shared.bean.event;

import com.weiju.ccmall.module.auth.event.BaseMsg;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/8/28.
 * 给方兆磊封装的 viewpager 设置的 eb 事件
 */
public class MsgFViewPager extends BaseMsg {
    public static final int ACTION_ADD_CLICK_LINK = 1;
    public static final int ACTION_ADD_CLICK_VIDEO = 12;

    public MsgFViewPager(int action) {
        super(action);
    }
}
