package com.weiju.ccmall.shared.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.weiju.ccmall.module.MainActivity;
import com.weiju.ccmall.shared.util.EventUtil;

import org.json.JSONException;
import org.json.JSONObject;

import cn.jpush.android.api.JPushInterface;

public class JPushReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            Bundle bundle = intent.getExtras();
            if (bundle == null) {
                return;
            }
            try {
                String json = bundle.getString(JPushInterface.EXTRA_EXTRA);
                LogUtils.e("推送通知" + json);
                JSONObject jsonObject = new JSONObject(json);
                String event = jsonObject.getString("event");
                String target = jsonObject.getString("target");
                if (!StringUtils.isEmpty(target) && !StringUtils.isEmpty(event)) {
                    EventUtil.compileEvent(context, event, target, true);
                } else {
                    startApp(context);
                }
            } catch (JSONException e) {
                startApp(context);
                e.printStackTrace();
            }
        }
    }

    private void startApp(Context context) {
        startActivity(context, MainActivity.class);
    }

    private void startActivity(Context context, Class<?> cls) {
        Intent intent = new Intent(context, cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
}
