package com.weiju.ccmall.shared.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.weiju.ccmall.module.pay.PayOrderActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.PayTypeModel;
import com.weiju.ccmall.shared.bean.ProfitData;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBalanceService;
import com.weiju.ccmall.shared.service.contract.IUserService;

import java.util.ArrayList;
import java.util.List;

public class PayTypeLayout extends LinearLayout {

    private ArrayList<PayTypeView> mTypeViews = new ArrayList<>();
    private int mPayType = -1;
    private int mSelectPayType = -1;
    private long mAvailableMoney;
    private long mAvailableGold;
    private boolean mIsAlipay = false;

    private boolean isLoading = false;
    List<PayTypeModel> mTypeModels; // 缓存数据，避免重复请求
    ProfitData mProfitData; // 缓存数据，避免重复请求
    long totalCost;
    boolean hasPickUp;
    long totalPrice;

    private int mOrderType;
    private int goodsType = -1;
    private int productType = -1;

    public void setProductType(int productType) {
        this.productType = productType;
    }

    public PayTypeLayout(Context context) {
        this(context, null);
    }

    public PayTypeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
        //  initData();
    }

    public void setOrderType(int orderType) {
        mOrderType = orderType;
    }

    public ProfitData getProfitData() {
        return mProfitData;
    }


    public void initData(long availableGoldLuck, long totalCost, boolean hasPickUp, long totalPrice) {
        this.totalCost = totalCost;
        this.hasPickUp = hasPickUp;
        this.totalPrice = totalPrice;
        if (mTypeModels != null && mProfitData != null) {
            mProfitData.availableGoldLuck = availableGoldLuck;
            setData(mTypeModels, mProfitData,totalCost,hasPickUp,totalPrice);
            return ;
        }

        if (isLoading) {
            return ;
        }
        isLoading = true;

        final IBalanceService balanceService = ServiceManager.getInstance().createService(IBalanceService.class);
        final IUserService userService = ServiceManager.getInstance().createService(IUserService.class);
//        Log.d("Seven", "" + Log.getStackTraceString(new Throwable()));
        APIManager.startRequest(balanceService.get(), new BaseRequestListener<ProfitData>() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onSuccess(final ProfitData result) {
                result.availableGoldLuck = availableGoldLuck;
                mProfitData = result;
                String goodsTypeStr = null;
                if (goodsType == PayOrderActivity.GOODSTYPE_ANCHOR || goodsType == PayOrderActivity.GOODSTYPE_CREDIT_LIFE_UPGRADE) {
                    goodsTypeStr = String.valueOf(goodsType);
                } else if (goodsType == PayOrderActivity.GOODSTYPE_LIVE_STORE){
                    goodsTypeStr = String.valueOf(2);
                } else if (productType == 26){
                    goodsTypeStr = String.valueOf(4);
                }
                APIManager.startRequest(userService.getPayTypes(goodsTypeStr), new BaseRequestListener<List<PayTypeModel>>() {
                    @Override
                    public void onSuccess(List<PayTypeModel> typeModels) {
                        mTypeModels = typeModels;
                        isLoading = false;
                        setData(typeModels, result, PayTypeLayout.this.totalCost, PayTypeLayout.this.hasPickUp, PayTypeLayout.this.totalPrice);
                        /*if (mOrderType == 5 || mOrderType == 6 || mOrderType == 7) {
                            typeModels.remove(2);
                            typeModels.remove(2);

                        } else {
                            setData(typeModels, result,totalCost,hasPickUp,totalPrice);
                        }*/
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        isLoading = false;
//                        ArrayList<PayTypeModel> payTypeModels = new ArrayList<>();
//                        payTypeModels.add(new PayTypeModel(1, AppTypes.PAY_TYPE.BALANCE, "余额支付", "http://img.beautysecret.cn/G1/M00/07/D5/rBIC8VpUZo-AJ3U3AAAObxDYVcM114.png"));
//                        setData(payTypeModels, result.availableMoney);
//>>>>>>> develop_0822_backup
                    }
                }, PayTypeLayout.super.getContext());
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                isLoading = false;
            }
        }, PayTypeLayout.super.getContext());
    }

    public void setGoodsType(int goodsType) {
        this.goodsType = goodsType;
    }


    private void initView() {
        setOrientation(VERTICAL);
    }

/*<<<<<<< HEAD
    public void setData(List<PayTypeModel> data, ProfitData profitData, long totalCost, boolean hasPickUp, long totalPrice) {
        mAvailableMoney = profitData.availableMoney;
        mAvailableGold = profitData.availableGold;
        removeAllViews();
        mTypeViews.clear();
        for (final PayTypeModel model : data) {
            if (hasPickUp && totalPrice == 0 && !(model.payType == AppTypes.PAY_TYPE.BALANCE || model.payType == AppTypes.PAY_TYPE.BALANCE_GOLD)) {
                continue;
            }
            final PayTypeView payTypeView = new PayTypeView(getContext());
            payTypeView.setData(model, profitData, totalCost);
            payTypeView.setItemClickListener(new PayItemClickListener());
            mTypeViews.add(payTypeView);
            addView(payTypeView);
        }
        mSelectPayType = -1;
        setSelectView();
    }
=======*/

            public void setData(List<PayTypeModel> data, ProfitData profitData,long totalCost,boolean hasPickUp, long totalPrice) {
                mAvailableMoney = profitData.availableMoney;
                mAvailableGold = profitData.availableGold;
                mTypeViews.clear();
                removeAllViews();
                for (final PayTypeModel model : data) {
                    if (hasPickUp && totalPrice == 0 && !(model.payType == AppTypes.PAY_TYPE.BALANCE || model.payType == AppTypes.PAY_TYPE.BALANCE_GOLD)) {
                        continue;
                    }
                    final PayTypeView payTypeView = new PayTypeView(getContext());
                    payTypeView.setData(model, profitData, totalCost, productType);
                    payTypeView.setItemClickListener(new PayItemClickListener());
                    mTypeViews.add(payTypeView);
                    addView(payTypeView);
                }
                setSelectView();

                if (mOrderType == 5 || mOrderType == 6 || mOrderType == 7) {

                } else {
                    mSelectPayType = -1;
                    setSelectView();
                }
            }

//>>>>>>> develop_0822_backup

    private void setSelectView() {
        if (mSelectPayType == -1 && mTypeViews.size() > 0) {
            mSelectPayType = mTypeViews.get(0).getType();
        }
        for (PayTypeView typeView : mTypeViews) {
            typeView.select(typeView.getType() == mSelectPayType);
        }
    }

    public int getSelectType() {
        for (PayTypeView typeView : mTypeViews) {
            if (typeView.isSelect()) {
                return typeView.getType();
            }
        }
        return mSelectPayType;
    }

    public long getBalance() {
        return mAvailableMoney;
    }

    public long getBalanceGold() {
        return mAvailableGold;
    }

    /**
     * @param selectPayType 设置预选类型，加载完成后会自动选中想要的方案
     */
    public void setSelectPayType(int selectPayType) {
        mSelectPayType = selectPayType;
        setSelectView();
    }

    private class PayItemClickListener implements PayTypeView.ItemClickListener {
        @Override
        public void onSelectItem(int payType) {
            for (PayTypeView typeView : mTypeViews) {
                typeView.select(typeView.getType() == payType);
            }
        }
    }

    public void setOnlyWeChat() {
        mTypeViews.clear();
        removeAllViews();
        final PayTypeView payTypeView = new PayTypeView(getContext());
        PayTypeModel model = new PayTypeModel(1, 6, "微信小程序支付",
                "https://static.create-chain.net/ccmall/19/60/42/de83fa17b00640ddaf81d984f42a3f3a.png");
        payTypeView.setData(model, null, 0, productType);
//        payTypeView.setItemClickListener(new PayItemClickListener());
        payTypeView.select(true);
        mTypeViews.add(payTypeView);
        addView(payTypeView);
    }
}
