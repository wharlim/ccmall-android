package com.weiju.ccmall.shared.page;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.shared.basic.BaseFragment;
import com.weiju.ccmall.shared.bean.PageConfig;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.contracts.RequestListener;
import com.weiju.ccmall.shared.factory.PageElementFactory;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.service.contract.IPageService;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * @author zjm
 * @date 2018/3/5
 */
public class CustomPageFragment extends BaseFragment {

    @BindView(R.id.layoutElement)
    LinearLayout mLayoutElement;
    @BindView(R.id.refreshLayout)
    SmartRefreshLayout mRefreshLayout;

    private IPageService mPageService = ServiceManager.getInstance().createService(IPageService.class);
    private String mPageId;
    private PageType mPageType;
    private boolean isGetIntentData;
    private List<Element> mElements = new ArrayList<>();
    private static int PAGE_SIZE = 10;
    private int mCurrentPage;
    private OnLoadPageConfigListener mOnLoadPageConfigListener;

    public static CustomPageFragment newInstance(String pageId) {
        return newInstance(pageId, PageType.HOME);
    }

    public static CustomPageFragment newInstance(String pageId, PageType pageType) {
        Bundle args = new Bundle();
        args.putString("id", pageId);
        args.putString("pageType", pageType.name());
        CustomPageFragment fragment = new CustomPageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Fresco.initialize(this.getActivity());
        View rootView = inflater.inflate(R.layout.fragment_custom_page, container, false);
        ButterKnife.bind(this, rootView);
        getIntentData();
        initView();
        LogUtils.e("自定义页面  onCreateView  " + mPageId);
        if (mVisible && mLayoutElement.getChildCount() == 0) {
            mRefreshLayout.autoRefresh();
        }
        return rootView;
    }

    boolean mVisible = false;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        mVisible = isVisibleToUser;
        if (mLayoutElement != null && mLayoutElement.getChildCount() == 0 && isGetIntentData && isVisibleToUser) {
            LogUtils.e("自定义页面自动加载  " + mPageId);
            mRefreshLayout.autoRefresh();
        }
    }

    private void getIntentData() {
        mPageId = getArguments().getString("id");
        mPageType = PageType.valueOf(
                getArguments().getString("pageType", PageType.HOME.name())
        );
        isGetIntentData = true;
    }


    private void initView() {
        mRefreshLayout.setEnableHeaderTranslationContent(false);
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(RefreshLayout refreshLayout) {
                loadPageConfig();
            }
        });
        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(RefreshLayout refreshLayout) {
                addViews(false);
            }
        });
        mRefreshLayout.setEnableAutoLoadMore(true);//是否启用列表惯性滑动到底部时自动加载更多
        mRefreshLayout.setEnableOverScrollBounce(false);//是否启用越界回弹
    }

    private void addViews(boolean isRefresh) {
        if (isRefresh) {
            mCurrentPage = 0;
            mLayoutElement.removeAllViews();
            mRefreshLayout.finishRefresh();
            mRefreshLayout.setNoMoreData(false);
        } else {
            mCurrentPage++;
            mRefreshLayout.finishLoadMore();
        }
        int start = mCurrentPage * PAGE_SIZE;
        int end = start + PAGE_SIZE;
        if (end > mElements.size()) {
            end = mElements.size();
            mRefreshLayout.finishLoadMoreWithNoMoreData();
        }
        for (int i = start; i < end; i++) {
            Element element = mElements.get(i);
            if (Key.HAPPY_BUY_HOME.equals(mPageId)) { // 还够精选下的单全部orderType=5
                element.orderType = Const.ORDER_TYPE_FREE_ORDER;
            }
            mLayoutElement.addView(PageElementFactory.make(getContext(), element));
        }
    }

    private RequestListener<List<Element>> getElementListResponseListener() {
        return new RequestListener<List<Element>>() {
            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(List<Element> elements) {
//                for (Element e : elements)
                // 修复异步网络请求加载后，页面已经关闭导致的崩溃
                if (getActivity() == null || !isAdded()) {
                    return;
                }
                mElements = elements;
                addViews(true);
            }

            @Override
            public void onError(Throwable e) {
                // 修复异步网络请求加载后，页面已经关闭导致的崩溃
                if (getActivity() == null || !isAdded()) {
                    return;
                }
                ToastUtil.error(e.getMessage());
                mRefreshLayout.finishRefresh();
                mRefreshLayout.setNoMoreData(false);
            }

            @Override
            public void onComplete() {
            }
        };
    }


    public void loadPageConfig() {
        LogUtils.e("自定义页面开始加载数据  " + mPageId);
        if (StringUtils.isEmpty(mPageId)) {
            return;
        }
        if (mOnLoadPageConfigListener != null) {
            mOnLoadPageConfigListener.onLoadPageConfig();
        }
        switch (mPageId) {
            case Key.PAGE_HOME: {
                Observable<RequestResult<List<Element>>> api = mPageService.getHomePageConfig();
                if (mPageType == PageType.JKP) {
                    api = mPageService.getTkHomePageConfig();
                }
                APIManager.startRequest(api, getElementListResponseListener(), getContext());
            }
                return;
            case Key.HAPPY_BUY_HOME:
                APIManager.startRequest(mPageService.getHappyBuyHome(), getElementListResponseListener(), getContext());
                return ;
            default:{
                Observable<RequestResult<PageConfig>> api = mPageService.getPageConfig(mPageId);
                if (mPageType == PageType.JKP) {
                    api = mPageService.getTkPageConfig(mPageId);
                }
                APIManager.startRequest(
                        api.flatMap(new Function<RequestResult<PageConfig>, Observable<RequestResult<List<Element>>>>() {
                                    @Override
                                    public Observable<RequestResult<List<Element>>> apply(RequestResult<PageConfig> pageConfigRequestResult) throws Exception {
                                        if (pageConfigRequestResult.isFail()) {
                                            throw new RuntimeException(pageConfigRequestResult.message);
                                        }
                                        RequestResult<List<Element>> listRequestResult = new RequestResult<>();
                                        listRequestResult.code = pageConfigRequestResult.code;
                                        listRequestResult.message = pageConfigRequestResult.message;
                                        listRequestResult.data = pageConfigRequestResult.data.elements;
                                        return Observable.just(listRequestResult);
                                    }
                                }),
                        getElementListResponseListener(), getContext()
                );
            }
                break;
        }
    }

    public interface OnLoadPageConfigListener {
        void onLoadPageConfig();
    }

    public void setOnLoadPageConfigListener(OnLoadPageConfigListener onLoadPageConfigListener) {
        mOnLoadPageConfigListener = onLoadPageConfigListener;
    }
}
