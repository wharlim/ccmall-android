package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.blankj.utilcode.utils.SizeUtils;

/**
 * 产品详情里面的促销的里面的 itemView
 */
public class ProductActivityTagView extends LinearLayout {

    public ProductActivityTagView(Context context) {
        super(context);
        initView();
    }

    private void initView() {
        setOrientation(LinearLayout.HORIZONTAL);

        int topBottomPading = SizeUtils.dp2px(5);
        setPadding(0, topBottomPading, 0, topBottomPading);

        ImageView imageView = new ImageView(getContext());
    }
}
