package com.weiju.ccmall.shared.manager;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.blankj.utilcode.utils.LogUtils;
import com.weiju.ccmall.module.pay.PayActivity;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.shared.basic.BaseCallback;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.CartAmount;
import com.weiju.ccmall.shared.bean.CartItem;
import com.weiju.ccmall.shared.bean.CartStore;
import com.weiju.ccmall.shared.bean.MemberRatio;
import com.weiju.ccmall.shared.bean.SkuAmount;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.service.contract.ICartService;
import com.weiju.ccmall.shared.util.ActivityControl;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.manager
 * @since 2017-06-09
 */
public class CartManager {

    public static void addToCart(final Context context, final SkuInfo skuInfo, final int amount, boolean checkOnSale) {
        addToCart(context, skuInfo, amount, checkOnSale, null);
    }

    public static void addToCart(final Context context, final SkuInfo skuInfo, final int amount, boolean checkOnSale, String liveId) {
        if (!UiUtils.checkUserLogin(context)) {
            return;
        }

        if (checkOnSale && skuInfo.status == 0) {
            ToastUtil.error("商品已下架");
            return;
        }
        getQuantity(context, skuInfo, new BaseCallback<Integer>() {
            @Override
            public void callback(Integer data) {
                if (skuInfo.stock < amount + data) {
                    ToastUtil.error("库存不足");
                    return;
                }
                ICartService service = ServiceManager.getInstance().createService(ICartService.class);
                APIManager.startRequest(service.addToCart(skuInfo.skuId, amount, liveId), new BaseRequestListener<CartAmount>() {

                    @Override
                    public void onSuccess(CartAmount result) {
                        ToastUtil.success("添加购物车成功");
                        EventBus.getDefault().post(new EventMessage(Event.cartAmountUpdate, result.amount));
                    }
                },context);
            }
        });
    }

    private static void getQuantity(Context context, final SkuInfo skuInfo, final BaseCallback<Integer> callback) {
        ICartService cartService = ServiceManager.getInstance().createService(ICartService.class);
        APIManager.startRequest(cartService.getAllList(), new BaseRequestListener<List<CartStore>>() {
            @Override
            public void onSuccess(List<CartStore> result) {
                for (CartStore cartStore : result) {
                    for (CartItem product : cartStore.products) {
                        if (product.skuId.equals(skuInfo.skuId)) {
                            int amount = Math.min(product.amount, product.stock);
                            callback.callback(amount);
                            return;
                        }
                    }
                }
                callback.callback(0);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);

            }
        },context);

    }

    public static void buyNow(Activity activity, final Context context, final SkuInfo skuInfo, final int amount, User user, String orderType) {
        buyNow(activity, context, skuInfo, amount, user, orderType, null);
    }

    public static void buyNow(Activity activity, final Context context, final SkuInfo skuInfo, final int amount, User user, String orderType, String liveId) {
        if (!UiUtils.checkUserLogin(context)) {
            return;
        }
        getQuantity(context, skuInfo, new BaseCallback<Integer>() {
            @Override
            public void callback(Integer data) {
                if (skuInfo.stock < amount + data) {
                    ToastUtil.error("库存不足");
                    return;
                }
                Intent intent = new Intent(context, PayActivity.class);
                intent.putExtra(Const.ORDER_TYPE, orderType);
                intent.putExtra("from", "buy");
                intent.putExtra("skuAmount", new SkuAmount(skuInfo.skuId, amount));
                intent.putExtra("payByMoney", skuInfo.newerConfig != null && skuInfo.newerConfig.payByMoney);
                intent.putExtra("skuInfo", skuInfo);
                if (!TextUtils.isEmpty(liveId)) {
                    intent.putExtra("liveId", liveId);
                }
                ActivityControl.getInstance().add(activity);
                if (user != null)
                    intent.putExtra("user", user);

                context.startActivity(intent);
            }
        });

    }
    public static void buyNow(Activity activity, final Context context, final SkuInfo skuInfo, final int amount, User user) {
        buyNow(activity, context,skuInfo,amount, user, "");
    }

    /**
     * 获取购物车总数量
     */
    public static void getAmount() {
        ICartService cartService = ServiceManager.getInstance().createService(ICartService.class);
        APIManager.startRequest(cartService.getTotal(), new Observer<RequestResult<CartAmount>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull RequestResult<CartAmount> cartAmountRequestResult) {
                int amount = 0;
                if (cartAmountRequestResult.isSuccess()) {
                    amount = cartAmountRequestResult.data.amount;
                }
                EventBus.getDefault().post(new EventMessage(Event.cartAmountUpdate, amount));
            }

            @Override
            public void onError(@NonNull Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    /**
     * @param list
     * @return 购物车所有商品的价格和
     */
    public static long getTotalMoney(List<CartStore> list) {
        long totalPrice = 0L;
        if (list == null) {
            return totalPrice;
        }
        for (CartStore item : list) {
            if (item.products == null) {
                continue;
            }
            for (CartItem product : item.products) {
                if (product.isSelected) {
                    totalPrice += (ConvertUtil.cent2CurrentCent(product) * product.amount);
                }
            }
        }
        return totalPrice;
    }

    public static long getOrderRadioMoney(List<CartStore> cartStores, List<MemberRatio> memberRatios) {
        return getOrderRadioMoney(cartStores, memberRatios, false, false);
    }

    /**
     * 计算选中商品一共打折了多少钱
     *
     * @param cartStores   商品列表
     * @param memberRatios 折扣列表
     * @param isAllSelect  是否计算所有商品（如果为 false，只机选选中商品）
     * @param isGroupBuy   是否是团购（团购不参与打折）
     * @return
     */
    public static long getOrderRadioMoney(List<CartStore> cartStores, List<MemberRatio> memberRatios, boolean isAllSelect, boolean isGroupBuy) {
        long radioMoney = 0;
        if (isGroupBuy) {
            return radioMoney;
        }
        for (CartStore cartStore : cartStores) {
            for (CartItem product : cartStore.products) {
                if ((isAllSelect || product.isSelected)) {
                    if (product.isHalfActivity()) {
                        radioMoney += product.retailPrice / 2 * (product.amount / 2);
                    }
                }
            }
        }
        return radioMoney;
        // 下面是旧的会员折扣了，目前不需要
//        int vipType = SessionUtil.getInstance().getLoginUser().vipType;
//        if (vipType == AppTypes.FAMILY.MEMBER_NORMAL) {
//            long updateVipMoney = getUpdateVipMoney(memberRatios);
//            long totalMoney = getTotalRetailPrice(cartStores, isAllSelect);
//            if (totalMoney < updateVipMoney) {
//                return 0;
//            }
//        }
//        for (CartStore cartStore : cartStores) {
//            for (CartItem product : cartStore.products) {
//                if ((isAllSelect || product.isSelected)) {
//                    radioMoney += product.getVipRadioMoney() * product.cost;
//                }
//            }
//        }
//        return radioMoney;
    }

    /**
     * @param memberRatios
     * @return 可以升级到尊享会员的最低价格
     */
    private static long getUpdateVipMoney(List<MemberRatio> memberRatios) {
        if (memberRatios != null) {
            for (MemberRatio memberRatio : memberRatios) {
                if (memberRatio.memberType == AppTypes.FAMILY.MEMBER_ZUNXIANG) {
                    return memberRatio.joinPrice;
                }
            }
        }
        return 100000000;
    }

    /**
     * 计算所有参与积分折扣的金额的和
     *
     * @param cartStores         需要计算的商品
     * @param updateMemberRatios
     * @return
     */
    public static long getScoreTotalMoney(List<CartStore> cartStores, ArrayList<MemberRatio> updateMemberRatios) {
        long orderRadioMoney = getOrderRadioMoney(cartStores, updateMemberRatios, true, false);
        long totalBasePrice = getTotalBasePrice(cartStores);
        long price = totalBasePrice - orderRadioMoney - getNoScorePrice(cartStores, orderRadioMoney > 0);
        return price;
    }

    /**
     * 获取不参与积分抵扣的商品价格和
     *
     * @param cartStores   商品列表
     * @param isRadioPrice 商品是否打折了
     * @return 不参与积分抵扣的商品价的和
     */
    private static long getNoScorePrice(List<CartStore> cartStores, boolean isRadioPrice) {
        int memberType = isRadioPrice ? AppTypes.FAMILY.MEMBER_ZUNXIANG : AppTypes.FAMILY.MEMBER_NORMAL;
        long price = 0;
        for (CartStore cartStore : cartStores) {
            for (CartItem product : cartStore.products) {
                if (product.buyScore != 1) {
                    price += product.getTypePrice(memberType) * product.amount;
                }
            }
        }
        return price;
    }

    private static long getTotalBasePrice(List<CartStore> cartStores) {
        long price = 0;
        for (CartStore cartStore : cartStores) {
            price += getTotalBasePrice((ArrayList<CartItem>) cartStore.products);
        }
        return price;
    }

    /**
     * 计算这些商品的折扣，[废弃]
     * 如果会员折扣低 就获取会员折扣
     * 如果买满商品的折扣低 就使用买满商品的折扣
     */
    public static OrderRadio getOrderRadio(List<CartStore> cartStores, List<MemberRatio> memberRatios) {
        return getOrderRadio(cartStores, memberRatios, false);
    }

    /**
     * 计算这些商品的折扣，
     * 如果会员折扣低 就获取会员折扣
     * 如果买满商品的折扣低 就使用买满商品的折扣
     *
     * @param cartStores   购物车的所有商品，只计算 isSelected == true
     * @param memberRatios 会员折扣列表
     * @param isAllSelect  是否所有商品都是选中的（结算下单调这个）
     * @return 最终打折率和折扣金额
     */
    public static OrderRadio getOrderRadio(List<CartStore> cartStores, List<MemberRatio> memberRatios, boolean isAllSelect) {
        return getOrderRadio(cartStores, memberRatios, isAllSelect, false, false);
    }


    /**
     * @param cartStores
     * @param memberRatios
     * @param isAllSelect
     * @param isCountNoScoreMoney 是否计算不积分商品总价
     * @param isGroupBuy          是不是团购
     * @return
     */
    public static OrderRadio getOrderRadio(List<CartStore> cartStores, List<MemberRatio> memberRatios, boolean isAllSelect, boolean isCountNoScoreMoney, boolean isGroupBuy) {
        if (cartStores == null || cartStores.size() == 0 || memberRatios == null || memberRatios.size() == 0 || isGroupBuy) {
            return new OrderRadio(1, 0, 0); //数据异常 不打折
        }

        ArrayList<CartItem> cartItems = new ArrayList<>();//所有选中的商品
        if (isAllSelect) {
            for (CartStore cartStore : cartStores) {
                for (CartItem product : cartStore.products) {
                    cartItems.add(product);
                }
            }
        } else {
            cartItems.addAll(getSelectedCartItems(cartStores));
        }
        if (cartItems.size() == 0) {
            return new OrderRadio(1, 0, 0);
        }

        //基础总价
        long totalBasePrice = getTotalBasePrice(cartItems);
        //当前会员总价
        long currentMemberPrice = getCurrentMemberPrice(cartItems);

//        Map<String, CartItem.VipTypePricesEntity> vitTypePriceMap = getVitTypePriceMap(cartItems, memberRatios);//各级会员价
//        long suitableVipTypeProce;//匹配价格
//        CartItem.VipTypePricesEntity suitableVipTypePricesEntity = getSuitableVipTypePrice(vitTypePriceMap, memberRatios);
//        suitableVipTypeProce = suitableVipTypePricesEntity == null ? Long.MAX_VALUE : suitableVipTypePricesEntity.price;
//        long finalTotalPrice = suitableVipTypeProce < currentMemberPrice ? suitableVipTypeProce : currentMemberPrice;

        //购买商品达到的最佳会员折扣价
        long suitableVipTypePrice = getSuitableVipTypePrice(cartItems, memberRatios);
        MemberRatio memberRatio = getSuitableVipTypePriceEntity(cartItems, memberRatios);
        long finalTotalPrice;
        if (memberRatio == null || suitableVipTypePrice < 0) {
            finalTotalPrice = currentMemberPrice;
        } else {
            finalTotalPrice = suitableVipTypePrice < currentMemberPrice ? suitableVipTypePrice : currentMemberPrice;
        }

        OrderRadio orderRadio = new OrderRadio(finalTotalPrice * 1.0f / totalBasePrice, totalBasePrice - finalTotalPrice, finalTotalPrice);

        if (isCountNoScoreMoney) {
//            计算不使用积分的金额
            long finalNoScoreMoney = 0;
            for (CartItem cartItem : cartItems) {
                if (cartItem.buyScore != 0) {
                    continue;
                }
                if (suitableVipTypePrice > currentMemberPrice || memberRatio == null) {
                    cartItem.scorePrice = cartItem.currentVipTypePrice * cartItem.amount;
                    finalNoScoreMoney += cartItem.scorePrice;
                } else {
                    cartItem.scorePrice = cartItem.getTypePrice(memberRatio.memberType) * cartItem.amount;
                    finalNoScoreMoney += cartItem.scorePrice;
                }
            }
            orderRadio.setFinalNoScoreMoney(finalNoScoreMoney);
        }
        LogUtils.e("折扣计算\n" +
                "\n 基础总价" + totalBasePrice +
                "\n 当前会员总价" + currentMemberPrice +
                "\n 匹配价格" + suitableVipTypePrice +
                "\n 最终价格" + finalTotalPrice +
                "\n 不参与积分的价格" + orderRadio.finalNoScoreMoney
        );
        return orderRadio;
    }


    public static class OrderRadio {
        public float radio = 1;
        public long radioMoney = 0;
        public long finalMoney = 0;
        /**
         * 不参与积分的商品总价
         */
        public long finalNoScoreMoney = 0;

        public OrderRadio() {

        }

        public OrderRadio(float radio, long radioMoney, long finalMoney) {
            this.radio = radio;
            this.radioMoney = radioMoney;
            this.finalMoney = finalMoney;
        }

        public void setFinalNoScoreMoney(long finalNoScoreMoney) {
            this.finalNoScoreMoney = finalNoScoreMoney;
        }
    }

    /**
     * 购买商品达到的最佳会员折扣价（按普通会员价匹配）
     *
     * @param cartItems    购买的商品列表
     * @param memberRatios
     * @return
     */
    private static long getSuitableVipTypePrice(ArrayList<CartItem> cartItems, List<MemberRatio> memberRatios) {
        MemberRatio finalMemberRatio = getSuitableVipTypePriceEntity(cartItems, memberRatios);
        long price = -1;
        if (finalMemberRatio == null) {
            return price;
        }
        price = 0;
        for (CartItem cartItem : cartItems) {
            price += cartItem.getTypePrice(finalMemberRatio.memberType) * cartItem.amount;
        }
        return price;
    }

    /**
     * 购买商品达到的最佳会员折扣（按普通会员价匹配）
     *
     * @param cartItems
     * @param memberRatios
     * @return
     */
    private static MemberRatio getSuitableVipTypePriceEntity(ArrayList<CartItem> cartItems, List<MemberRatio> memberRatios) {
        long currentMemberPrice = getTotalBasePrice(cartItems);
        MemberRatio finalMemberRatio = null;
        for (MemberRatio memberRatio : memberRatios) {
            if (memberRatio.joinPrice <= currentMemberPrice && (finalMemberRatio == null || finalMemberRatio.memberType < memberRatio.memberType)) {
                finalMemberRatio = memberRatio;
            }
        }
        return finalMemberRatio;
    }

    /**
     * 根据会员价格列表和会员升级价格列表，匹配出最低的折扣价格
     *
     * @param vitTypePriceMap 所有等级会员价格列表
     * @param memberRatios    会员升级价格列表
     * @return 如果一个都没匹配上，就返回 Long.MAX_VALUE，否则返回匹配的最低优惠价
     */
    private static CartItem.VipTypePricesEntity getSuitableVipTypePrice(Map<String, CartItem.VipTypePricesEntity> vitTypePriceMap, List<MemberRatio> memberRatios) {
        for (MemberRatio memberRatio : memberRatios) {
            CartItem.VipTypePricesEntity vipTypePricesEntity = vitTypePriceMap.get(memberRatio.memberType + "");
            if (vipTypePricesEntity != null && vipTypePricesEntity.price >= memberRatio.joinPrice) {
                return vipTypePricesEntity;
            }
        }
        return null;
    }


    /**
     * 获取选中的商品的 当前等级和更高级别的价格和map
     *
     * @param cartItems    商品列表
     * @param memberRatios 可升级和本等级的会员折扣列表
     * @return
     */
    private static Map<String, CartItem.VipTypePricesEntity> getVitTypePriceMap(ArrayList<CartItem> cartItems, List<MemberRatio> memberRatios) {
        HashMap<String, CartItem.VipTypePricesEntity> vipTypePricesMap = new HashMap<>();
        for (MemberRatio memberRatio : memberRatios) {
            for (CartItem cartItem : cartItems) {
                CartItem.VipTypePricesEntity vipTypePricesEntity = vipTypePricesMap.get(memberRatio.memberType + "");
                if (vipTypePricesEntity == null) {
                    CartItem.VipTypePricesEntity temp = new CartItem.VipTypePricesEntity();
                    temp.price = cartItem.getTypePrice(memberRatio.memberType) * cartItem.amount;
                    temp.vipType = memberRatio.memberType;
                    vipTypePricesMap.put(memberRatio.memberType + "", temp);
                } else {
                    vipTypePricesEntity.price += cartItem.getTypePrice(memberRatio.memberType) * cartItem.amount;
                }

            }
        }
//        for (CartItem cartItem : cartItems) {
//            if (cartItem.vipTypePrices == null || cartItem.vipTypePrices.size() != 5) {
//                for (int i = 0; i < 5; i++) {
//                    CartItem.VipTypePricesEntity vipTypePricesEntity = vipTypePricesMap.get(i + "");
//                    if (vipTypePricesEntity == null) {
//                        vipTypePricesEntity = new CartItem.VipTypePricesEntity();
//                        vipTypePricesEntity.vipType = i;
//                        vipTypePricesEntity.price = cartItem.retailPrice * cartItem.cost;
//                        vipTypePricesMap.put(i + "", vipTypePricesEntity);
//                    } else {
//                        vipTypePricesEntity.price += cartItem.retailPrice * cartItem.cost;
//                    }
//                }
//            } else {
//                for (CartItem.VipTypePricesEntity vipTypePrice : cartItem.vipTypePrices) {
//                    CartItem.VipTypePricesEntity vipTypePricesEntity = vipTypePricesMap.get(vipTypePrice.vipType + "");
//                    if (vipTypePricesEntity == null) {
//                        CartItem.VipTypePricesEntity vipTypePricesEntity1 = new CartItem.VipTypePricesEntity();
//                        vipTypePricesEntity1.vipType = vipTypePrice.vipType;
//                        vipTypePricesEntity1.price = vipTypePrice.price * cartItem.cost;
//                        vipTypePricesMap.put(vipTypePrice.vipType + "", vipTypePricesEntity1);
//                    } else {
//                        vipTypePricesEntity.price += vipTypePrice.price * cartItem.cost;
//                    }
//                }
//            }
//        }
        return vipTypePricesMap;
    }

    /**
     * 获得所有商品的当前会员价的和
     *
     * @return
     */
    public static long getCurrentMemberPrice(ArrayList<CartItem> cartItems) {
        long price = 0;
        for (CartItem cartItem : cartItems) {
            price += cartItem.currentVipTypePrice * cartItem.amount;
        }
        return price;
    }

    /**
     * 获得所有商品的普通会员价的和
     *
     * @param cartItems
     * @return
     */
    public static long getTotalBasePrice(ArrayList<CartItem> cartItems) {
        long price = 0;
        for (CartItem cartItem : cartItems) {
            price += cartItem.retailPrice * cartItem.amount;
        }
        return price;
    }

    public static long getTotalRetailPrice(List<CartStore> stores, boolean isAllSelect) {
        long price = 0;
        for (CartStore store : stores) {
            for (CartItem product : store.products) {
                if (isAllSelect || product.isSelected) {
                    price += product.retailPrice * product.amount;
                }
            }
        }
        return price;
    }

    /**
     * 获取选中的商品list
     *
     * @param list 所有商品
     * @return 选中商品列表
     */
    public static ArrayList<CartItem> getSelectedCartItems(List<CartStore> list) {
        ArrayList<CartItem> cartItems = new ArrayList<>();
        if (list == null) {
            return cartItems;
        }
        for (CartStore cartStore : list) {
            for (CartItem product : cartStore.products) {
                if (product.isSelected) {
                    cartItems.add(product);
                }
            }
        }
        return cartItems;
    }

    /**
     * 获取选中的商品 id list
     *
     * @param list 所有商品
     * @return 选中商品列表
     */
    public static ArrayList<String> getSelectedIds(List<CartStore> list) {
        ArrayList<String> ids = new ArrayList<>();
        if (list == null) {
            return ids;
        }

        for (CartStore item : list) {
            if (item.products == null) {
                continue;
            }
            for (CartItem product : item.products) {
                if (product.isSelected) {
                    ids.add(product.skuId);
                }
            }
        }
        return ids;
    }

    /**
     * 获取选中的商品是否为混合商品
     */
    public static boolean isSelectedMixture(List<CartStore> list) {
        ArrayList<CartItem> cartItems = getSelectedCartItems(list);
        boolean isLiveProduct = false;
        boolean isNormalProduct = false;
        for (CartItem item : cartItems) {
            if (!isLiveProduct) isLiveProduct = item.isLiveStoreProduct();
            if (!isNormalProduct) isNormalProduct = !item.isLiveStoreProduct();
        }
        return isLiveProduct && isNormalProduct;
    }

    public static int getSelectedQuantity(List<CartStore> list) {
        int quantity = 0;
        for (CartStore item : list) {
            if (item.products == null) {
                continue;
            }
            for (CartItem product : item.products) {
                if (product.isSelected) {
                    quantity += product.amount;
                }
            }
        }
        return quantity;
    }

    /**
     * 删除所有选中的商品
     *
     * @param list 所有商品
     */
    public static void removeSelected(List<CartStore> list) {
        ArrayList<CartStore> teamCard = new ArrayList<>();
        teamCard.addAll(list);
        for (CartStore cartStore : teamCard) {
            if (cartStore.products != null) {
                for (CartItem product : cartStore.products) {
                    if (product.isSelected) {
                        list.remove(product);
                    }
                }
            }
        }
    }

    public static void groupBuy(final Context context, final SkuInfo skuInfo, final int amount) {
        if (!UiUtils.checkUserLogin(context)) {
            return;
        }
        getQuantity(context, skuInfo, new BaseCallback<Integer>() {
            @Override
            public void callback(Integer data) {
                if (skuInfo.stock < amount + data) {
                    ToastUtil.error("库存不足");
                    return;
                }
                Intent intent = new Intent(context, PayActivity.class);
                intent.putExtra("from", "groupBuy");
                intent.putExtra("skuAmount", new SkuAmount(skuInfo.skuId, amount));
                context.startActivity(intent);
            }
        });
    }

    public static void joinGroup(final Context context, final SkuInfo skuInfo, final int amount, final String groupCode) {
        if (!UiUtils.checkUserLogin(context)) {
            return;
        }
        getQuantity(context, skuInfo, new BaseCallback<Integer>() {
            @Override
            public void callback(Integer data) {
                if (skuInfo.stock < amount + data) {
                    ToastUtil.error("库存不足");
                    return;
                }
                Intent intent = new Intent(context, PayActivity.class);
                intent.putExtra("from", "joinGroup");
                intent.putExtra("skuAmount", new SkuAmount(skuInfo.skuId, amount));
                intent.putExtra("groupCode", groupCode);
                context.startActivity(intent);
            }
        });

    }

    public static void updateCartItem(Context context, String skuId, int amount) {

        ICartService service = ServiceManager.getInstance().createService(ICartService.class);
        APIManager.startRequest(service.updateCartItem(skuId, amount), new BaseRequestListener<CartAmount>() {

            @Override
            public void onSuccess(CartAmount result) {
                EventBus.getDefault().post(new EventMessage(Event.cartAmountUpdate, result.amount));
            }
        },context);
    }
}
