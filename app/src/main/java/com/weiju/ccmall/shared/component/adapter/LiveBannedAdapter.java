package com.weiju.ccmall.shared.component.adapter;

import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.BannedUser;
import com.weiju.ccmall.shared.util.FrescoUtil;

import java.util.List;

/**
 * @author chenyanming
 * @date 2020/2/21.
 */
public class LiveBannedAdapter extends BaseQuickAdapter<BannedUser, BaseViewHolder> {
    public LiveBannedAdapter() {
        super(R.layout.item_live_banned_user);
    }

    @Override
    protected void convert(BaseViewHolder helper, BannedUser item) {
        helper.addOnClickListener(R.id.tvUnbanning);
        SimpleDraweeView simpleDraweeView = helper.getView(R.id.ivAvatar);
        FrescoUtil.setImage(simpleDraweeView, item.headImage);
        helper.setText(R.id.tvName, item.nickName);
        helper.setText(R.id.tvDate, String.format("操作时间：%s", item.bannedMessageDate));
    }
}
