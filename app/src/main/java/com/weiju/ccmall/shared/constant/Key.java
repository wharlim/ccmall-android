package com.weiju.ccmall.shared.constant;

public class Key {
    public static final String VERSION = "version";
    public static final String OAUTH = "__outh";
    public static final String SKU_ID = "skuId";
    public static final String PRODUCT_ID = "productId";
    public static final String LOAD = "load";
    public static final String INVITATION_CODE = "invitation_code";
    public static final String PAGE_TO_BE_SHOPKEEPER = "to_be_shopkeeper";
    public static final String PAGE_SECOND_KILL = "second_kill";
    public static final String PAGE_HOME = "HOME";
    public static final String SELECT_ADDRESS = "select_address";
    public static final String EDIT_ADDRESS = "edit_address";
    public static final String USER = "user";
    public static final String SELECT_COUPON = "select_coupon";
    public static final String HAPPY_BUY_HOME = "HAPPY_BUY_HOME"; // 嗨购精选

    public static final String KEY_tencentIMUserSig = "KEY_tencentIMUserSig"; // TIM 登录签名
    public static final String KEY_memberId = "KEY_memberId"; // TIM登录用户id
    public static final String XYSH_USER = "xysh_user";
    public static final String LIVE_USER = "live_user";
}
