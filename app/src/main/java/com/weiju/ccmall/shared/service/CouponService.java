package com.weiju.ccmall.shared.service;

import android.content.Context;

import com.weiju.ccmall.shared.basic.BaseCallback;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ICouponService;
import com.weiju.ccmall.shared.util.ToastUtil;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service
 * @since 2017-07-05
 */
public class CouponService {

    public static void getCoupon(Context context, String couponId) {
        ICouponService service = ServiceManager.getInstance().createService(ICouponService.class);
        APIManager.startRequest(service.receiveCouponById(couponId), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("优惠券领取成功");
            }
        },context);
    }

    public static void getCoupon(Context context, String couponId, final BaseCallback<Object> callback) {
        ICouponService service = ServiceManager.getInstance().createService(ICouponService.class);
        APIManager.startRequest(service.receiveCouponById(couponId), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("优惠券领取成功");
                callback.callback(result);
            }
        },context);
    }
    public static void getCouponWithoutToast(Context context, String couponId, final BaseCallback<Object> callback) {
        ICouponService service = ServiceManager.getInstance().createService(ICouponService.class);
        APIManager.startRequest(service.receiveCouponById(couponId), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                callback.callback(result);
            }
        },context);
    }
}
