package com.weiju.ccmall.shared.contracts;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.contract
 * @since 2017-06-08
 */
public interface OnValueChangeLister {
    void changed(int value);
}
