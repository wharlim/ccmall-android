package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/8/22.
 */
public class YiBaoPay {

    /**
     * payConfig : {"timestamp":"1543458341","sign":"quctJXPhSF/FYhclFx+v13RWM2d9QjI51Ddcm7JNKB5RD/xMNi1i7KYWA784ZmDsW7ASqrGOHQibcJ/bGdLzlqe1ABHdclSD9pEVLMnj/Tl69PNUSAFywCmjlatxZcUsxWyKP8jLc3JIfzSfPUy2CdLZhA9NerDKjlR30OBVUnOenMvUPlJruAtjdwEHkIFtNZRc9L1u2OSeGMlI3cgwXNL1JLMXlYrrpYhmvSrlI64OqFxdKI54dI7Qj5sk1z1loGZG0vYK928gus86J5xXm2HqAKhvGuw6Aya5hD2bAqitOmskuuodQLM3m6dP3UZvUAPU4+r9a5ew7a7SvHraig==","noncestr":"awfr1gooXMt7BH65NWZDU3OCm9pcOeaz","partnerid":"1501265261","packagevalue":"Sign=WXPay","prepayid":"wx29102541267423d8510322ba1866767754","appid":"wx434333000f28e296"}
     * payUrl :
     * orderId : 00615436251003241543458336
     * uniqueOrderNo : 1001201811290000000282732422
     */

    @SerializedName("payConfig")
    public PayConfigEntity payConfig;
    @SerializedName("payUrl")
    public String payUrl;
    @SerializedName("orderId")
    public String orderId;
    @SerializedName("uniqueOrderNo")
    public String uniqueOrderNo;

    public static class PayConfigEntity {
        /**
         * timestamp : 1543458341
         * sign : quctJXPhSF/FYhclFx+v13RWM2d9QjI51Ddcm7JNKB5RD/xMNi1i7KYWA784ZmDsW7ASqrGOHQibcJ/bGdLzlqe1ABHdclSD9pEVLMnj/Tl69PNUSAFywCmjlatxZcUsxWyKP8jLc3JIfzSfPUy2CdLZhA9NerDKjlR30OBVUnOenMvUPlJruAtjdwEHkIFtNZRc9L1u2OSeGMlI3cgwXNL1JLMXlYrrpYhmvSrlI64OqFxdKI54dI7Qj5sk1z1loGZG0vYK928gus86J5xXm2HqAKhvGuw6Aya5hD2bAqitOmskuuodQLM3m6dP3UZvUAPU4+r9a5ew7a7SvHraig==
         * noncestr : awfr1gooXMt7BH65NWZDU3OCm9pcOeaz
         * partnerid : 1501265261
         * packagevalue : Sign=WXPay
         * prepayid : wx29102541267423d8510322ba1866767754
         * appid : wx434333000f28e296
         */

        @SerializedName("timestamp")
        public String timestamp;
        @SerializedName("sign")
        public String sign;
        @SerializedName("noncestr")
        public String noncestr;
        @SerializedName("partnerid")
        public String partnerid;
        @SerializedName("package")
        public String packagevalue;
        @SerializedName("prepayid")
        public String prepayid;
        @SerializedName("appid")
        public String appid;
    }
}
