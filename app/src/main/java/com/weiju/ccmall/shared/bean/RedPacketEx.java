package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/8/6.
 */
public class RedPacketEx extends BaseModel {

    /**
     * totalMoney : 8800
     * totalCount : 1
     */

    @SerializedName("totalMoney")
    public long totalMoney;
    @SerializedName("totalCount")
    public int totalCount;
}
