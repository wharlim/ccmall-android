package com.weiju.ccmall.shared.message;

import android.annotation.SuppressLint;
import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

/**
 * @author chenyanming
 * @time 2020/1/7 on 15:16
 * @desc
 */
@SuppressLint("ParcelCreator")
@MessageTag(value = "CCM:Chatroom:Red:Envelope", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class ChatroomRedMessage extends MessageContent {
    public ChatroomRedMessage() {
    }

    //红包Id
    private String redId;

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("redId", redId);
        } catch (JSONException e) {
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ChatroomRedMessage(byte[] data) {
        String jsonStr = null;

        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e1) {

        }

        try {
            JSONObject jsonObj = new JSONObject(jsonStr);

            if (jsonObj.has("redId")) {
                redId = jsonObj.optString("redId");
                setRedId(redId);
            }
        } catch (JSONException e) {
        }

    }

    public void setRedId(String redId) {
        this.redId = redId;
    }

    public String getRed() {
        return redId;
    }

    //给消息赋值。
    public ChatroomRedMessage(Parcel in) {
        redId = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        //这里可继续增加你消息的属性
//        setRedId(redId);
    }

    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<ChatroomRedMessage> CREATOR = new Creator<ChatroomRedMessage>() {

        @Override
        public ChatroomRedMessage createFromParcel(Parcel source) {
            return new ChatroomRedMessage(source);
        }

        @Override
        public ChatroomRedMessage[] newArray(int size) {
            return new ChatroomRedMessage[size];
        }
    };

    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     *
     * @return 一个标志位，表明Parcelable对象特殊对象类型集合的排列。
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * 将类的数据写入外部提供的 Parcel 中。
     *
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, redId);//该类为工具类，对消息中属性进行序列化
        //这里可继续增加你消息的属性
    }
}
