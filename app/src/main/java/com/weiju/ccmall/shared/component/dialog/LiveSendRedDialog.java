package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveRedEnvelopes;
import com.weiju.ccmall.shared.bean.ProfitData;
import com.weiju.ccmall.shared.component.DecimalEditText;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IBalanceService;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/24 on 10:32
 * @desc
 */
public class LiveSendRedDialog extends Dialog {
    private final Activity mContext;
    @BindView(R.id.tvBack)
    TextView mTvBack;
    @BindView(R.id.etMoney)
    DecimalEditText mEtMoney;
    @BindView(R.id.layoutMoney)
    LinearLayout mLayoutMoney;
    @BindView(R.id.etRedNum)
    EditText mEtRedNum;
    @BindView(R.id.layoutNum)
    LinearLayout mLayoutNum;
    @BindView(R.id.etPassword)
    EditText mEtPassword;
    @BindView(R.id.tvCommit)
    TextView mTvCommit;
    @BindView(R.id.tvMoneyTip)
    TextView mTvMoneyTip;

    private long mAvailableMoney;

    private String mLiveId;

    private SendRedListener mSendRedListener;

    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);

    public LiveSendRedDialog(@NonNull Activity context, String liveId, SendRedListener sendRedListener) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mLiveId = liveId;
        mSendRedListener = sendRedListener;
    }


    private int INPUT_MONEY = 0;
    private int INPIY_PASSWORD = 1;
    private int mType = INPUT_MONEY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_send_red);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.dimAmount = 0.0f;
        getWindow().setAttributes(layoutParams);
        initView();
        initData();
    }

    private void initView() {
        mEtMoney.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0 && !s.toString().equals(".")) {
                    double moneyD = Double.parseDouble(s.toString());
                    String redNumStr = mEtRedNum.getText().toString();
                    if (TextUtils.isEmpty(redNumStr)) {
                        mTvCommit.setEnabled(false);
                        return;
                    }
                    int num = Integer.parseInt(redNumStr);
                    mTvCommit.setEnabled(moneyD / num >= 0.01);
                    mTvMoneyTip.setVisibility(moneyD / num < 0.01 ? View.VISIBLE : View.INVISIBLE);
                    mTvMoneyTip.setText("单个红包不可低于0.01元");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEtRedNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String numStr = s.toString();
                if (numStr.length() > 0 && !numStr.equals("0")) {
                    int numI = Integer.parseInt(numStr);
                    if (numI > 200) {
                        String content = s.toString().substring(0, s.length() - 1);
                        mEtRedNum.setText(content);
                        mEtRedNum.setSelection(content.length());
                        mTvMoneyTip.setText("红包个数不得大于200个");
                        mTvMoneyTip.setVisibility(View.VISIBLE);
                        return;
                    }

                    String moneyStr = mEtMoney.getText().toString();
                    if (TextUtils.isEmpty(moneyStr) || moneyStr.equals("0")) {
                        mTvCommit.setEnabled(false);
                        return;
                    }

                    double moneyD = Double.parseDouble(moneyStr);
                    mTvCommit.setEnabled(moneyD / numI >= 0.01);
                    mTvMoneyTip.setVisibility(moneyD / numI < 0.01 ? View.VISIBLE : View.INVISIBLE);
                    mTvMoneyTip.setText("单个红包不可低于0.01元");
                } else {
                    mTvCommit.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTvCommit.setEnabled(!TextUtils.isEmpty(s));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void initData() {
        IBalanceService mBalanceService = ServiceManager.getInstance().createService(IBalanceService.class);
        APIManager.startRequest(mBalanceService.get(), new BaseRequestListener<ProfitData>(mContext) {
            @Override
            public void onSuccess(ProfitData profitData) {
                mAvailableMoney = profitData.availableGold;
            }
        }, getContext());
    }


    @OnClick(R.id.tvCommit)
    protected void commit() {
        if (mType == INPUT_MONEY) {
            String moneyStr = mEtMoney.getText().toString();
            double moneyD = Double.parseDouble(moneyStr);
            if (moneyD * 100 > mAvailableMoney) {
                ToastUtil.error("您的购物金不足，请重新输入金额");
                return;
            }
            mType = INPIY_PASSWORD;
            mTvBack.setText("返回");
            mTvCommit.setText("确定");
            mTvCommit.setEnabled(false);
            mEtPassword.setText("");
            mLayoutMoney.setVisibility(View.GONE);
            mLayoutNum.setVisibility(View.GONE);
            mEtPassword.setVisibility(View.VISIBLE);
            mTvMoneyTip.setVisibility(View.GONE);
        } else {
            sendRedEnvelopes();
        }

    }


    private void sendRedEnvelopes() {
        String sendNum = mEtRedNum.getText().toString();
        int sendNumI = Integer.parseInt(sendNum);
        String sendMoney = mEtMoney.getText().toString();
        double sendMoneyD = Double.parseDouble(sendMoney);
        long sendMoneyL = (long) (sendMoneyD * 100);
        String password = mEtPassword.getText().toString();

        APIManager.startRequest(mILiveService.sendRedEnvelopes(mLiveId, sendNumI, sendMoneyL, password),
                new BaseRequestListener<LiveRedEnvelopes>(mContext) {
                    @Override
                    public void onSuccess(LiveRedEnvelopes result) {
                        if (null != mSendRedListener) {
                            mSendRedListener.sendRedSuccess(result);
                        }
                        ToastUtil.success("发送成功，快去红包列表查看吧！");
                        dismiss();
                    }
                }, mContext);
    }

    @OnClick(R.id.tvBack)
    protected void back() {
        if (mType == INPUT_MONEY) {
            dismiss();
        } else {
            mType = INPUT_MONEY;
            mTvBack.setText("取消");
            mTvCommit.setText("发红包");
            mTvCommit.setEnabled(true);
            mLayoutMoney.setVisibility(View.VISIBLE);
            mLayoutNum.setVisibility(View.VISIBLE);
            mEtPassword.setVisibility(View.GONE);
        }

    }

    private boolean getEnable() {
        String redNumStr = mEtRedNum.getText().toString();
        String moneyStr = Objects.requireNonNull(mEtMoney.getText()).toString();

        if (TextUtils.isEmpty(moneyStr) || TextUtils.isEmpty(redNumStr)) {
            return false;
        }

        double moneyD = Double.parseDouble(moneyStr);
        long moneyL = (long) (moneyD * 100);
        if (moneyL > mAvailableMoney) {
            return false;
        }
        int redNumI = Integer.parseInt(redNumStr);
        if (redNumI > 200) {
            return false;
        }

        if (redNumI == 0 || moneyL == 0) {
            return false;
        }

        if (moneyD / redNumI < 0.01) {
            return false;
        }
        return true;
    }

    public interface SendRedListener {
        void sendRedSuccess(LiveRedEnvelopes liveRedEnvelopes);
    }


    public void keyBoardShow(int height) {
        mTvCommit.setVisibility(View.GONE);
    }

    public void keyBoardHide(int height) {
        mTvCommit.setVisibility(View.VISIBLE);
    }


}
