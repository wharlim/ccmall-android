package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.world.activity.WorldPayActivity;
import com.weiju.ccmall.module.world.entity.CatgoryItemEntity;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.component.NumberField;
import com.weiju.ccmall.shared.contracts.OnValueChangeLister;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WorldSkuDialog extends Dialog {

    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.selectedTv)
    protected TextView mSelectedTv;
    @BindView(R.id.priceTv)
    protected TextView mPriceTv;
    @BindView(R.id.thumbIv)
    protected SimpleDraweeView mThumbIv;
    @BindView(R.id.stockTv)
    protected TextView mStockTv;
    @BindView(R.id.numberField)
    protected NumberField mNumberField;
    @BindView(R.id.closeBtn)
    ImageView mCloseBtn;

    protected CatgoryItemEntity product;

    private int mAmount = 1;
    private OnSelectListener mSelectListener;

    public WorldSkuDialog(Context context, CatgoryItemEntity entity) {
        this(context, 0);
        this.product = entity;
    }

    private WorldSkuDialog(Context context, int themeResId) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_world_product_sku);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        setCanceledOnTouchOutside(true);
        initViews();
    }

    private void initViews() {
        setSkuInfo();
        mNumberField.setOnChangeListener(new OnValueChangeLister() {
            @Override
            public void changed(int value) {
                mAmount = value;
            }
        });
    }

    private void setSkuInfo() {
        List<String> mainImages = product.mainImages;
        if (mainImages.size() > 0) {
            FrescoUtil.setImage(mThumbIv, mainImages.get(0));
        }
//        mStockTv.setText(String.format("库存 %s 件", product.stockQuantity));
        mPriceTv.setText(MoneyUtil.centToYuan¥StrNoZero(product.price));
        mSelectedTv.setText(product.attr);
        mNumberField.setValues(mAmount, 1, product.stockQuantity);
    }

    @OnClick(R.id.confirmBtn)
    protected void onConfirm() {
        product.quantity = mAmount;
        WorldPayActivity.start(getContext(), product);
        dismiss();
    }

    @OnClick(R.id.closeBtn)
    protected void onClose() {
        dismiss();
    }

    public void setSelectListener(OnSelectListener selectListener) {
        mSelectListener = selectListener;
    }

    public interface OnSelectListener {
        void onSelectSku(SkuInfo skuInfo);
    }

}
