package com.weiju.ccmall.shared.component.adapter;

import android.widget.ImageView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

/**
 * @author chenyanming
 * @time 2019/12/26 on 10:45
 * @desc
 */
public class LiveProductAdapter extends BaseQuickAdapter<SkuInfo, BaseViewHolder> {

    private int selectIndex = -1;
    private boolean mIsAdd;
    private boolean mIsCreate;

    public LiveProductAdapter() {
        super(R.layout.item_live_product);
    }

    public void setIsAdd(boolean isAdd) {
        mIsAdd = isAdd;
    }

    public void setIsCreate(boolean isCreate) {
        mIsCreate = isCreate;
    }

    @Override
    protected void convert(BaseViewHolder helper, SkuInfo item) {
        helper.setText(R.id.tvTitle, item.name);
        helper.setText(R.id.tvSuanLv, String.format("%s%%算率", item.countRateExc));
//        helper.setText(R.id.tvZhuan, String.format("预计赚%s元", item.getAnchorDistributionProfit()));
        helper.setText(R.id.tvPrice, MoneyUtil.centToYuanStrNoZero(item.retailPrice));
        FrescoUtil.setImageSmall(helper.getView(R.id.ivProduct), item.thumb);
        ImageView view = helper.getView(R.id.ivAddOrDelete);
        view.setSelected(mIsAdd);
        helper.addOnClickListener(R.id.ivAddOrDelete);
        helper.setText(R.id.tvIndex, String.valueOf(helper.getPosition() + 1));
//        helper.setVisible(R.id.tvZhuan,!mIsAdd);

        helper.addOnClickListener(R.id.tvExplain);
        int pos = helper.getLayoutPosition() - getHeaderLayoutCount();
        if (pos == selectIndex) {
            helper.getView(R.id.tvExplain).setSelected(true);
            helper.setText(R.id.tvExplain, "当前讲解");
        } else {
            helper.getView(R.id.tvExplain).setSelected(false);
            helper.setText(R.id.tvExplain, "开始讲解");
        }
        helper.setVisible(R.id.tvExplain,!mIsAdd && !mIsCreate);
    }


    public void setSelectIndex(int index) {
        this.selectIndex = index;
        notifyDataSetChanged();
    }

    public int getSelectedIndex() {
        return this.selectIndex;
    }
}
