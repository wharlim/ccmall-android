package com.weiju.ccmall.shared.util;

import android.app.Activity;
import android.os.Message;
import android.text.TextUtils;

import com.alipay.sdk.app.PayTask;
import com.weiju.ccmall.module.pay.PayMsg;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.PayResult;
import com.weiju.ccmall.shared.bean.YiBaoPay;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IPayService;

import org.greenrobot.eventbus.EventBus;

import java.util.Map;

/**
 * Created by zjm on 2018/3/28.
 */
public class AliPayUtils {

    private static final int SDK_PAY_FLAG = 1;

    public static void pay(final Activity activity, final Order order){
        IPayService service = ServiceManager.getInstance().createService(IPayService.class);
        APIManager.startRequest(
                service.getHiCardPayYiBo(
                        order.orderMain.orderCode,
                        "2",
                        "",
                        StringUtil.md5("a70c34cc321f407d990c7a2aa7900729" + order.orderMain.orderCode)
                ),
                new BaseRequestListener<YiBaoPay>(activity) {
                    @Override
                    public void onSuccess(YiBaoPay result) {
                        goPay(activity,result.payUrl);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        activity.finish();
                        EventUtil.viewOrderDetail(activity, order.orderMain.orderCode, false);
                    }
                },activity);
    }

    /**
     * 调用之后请在需要的页面接受 EventBus ，支付结果将以 EventBus 的方式回调出来
     * @param context
     */
    private static void goPay(final Activity context, final String orderInfo) {
        Runnable payRunnable = new Runnable() {
            @Override
            public void run() {
                PayTask alipay = new PayTask(context);
                Map<String, String> result = alipay.payV2(orderInfo, true);

                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;

                checkPayResult(result);
            }
        };
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    /**
     * 检查支付结果，用 eventBus 将支付结果发送出去
     *
     * @param result
     */
    private static void checkPayResult(Map<String, String> result) {
        PayResult payResult = new PayResult(result);
        /**
         对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
         */
        // 同步返回需要验证的信息
        String resultInfo = payResult.getResult();
        String resultStatus = payResult.getResultStatus();
        // 判断resultStatus 为9000则代表支付成功
        if (TextUtils.equals(resultStatus, "9000")) {
            // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
            EventBus.getDefault().post(new PayMsg(PayMsg.ACTION_ALIPAY_SUCCEED));
        } else {
            // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
            EventBus.getDefault().post(new PayMsg(PayMsg.ACTION_ALIPAY_FAIL));
        }
    }

}
