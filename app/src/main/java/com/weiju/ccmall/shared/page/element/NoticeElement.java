package com.weiju.ccmall.shared.page.element;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.weiju.ccmall.R;
import com.weiju.ccmall.module.notice.NoticeListActivity;
import com.weiju.ccmall.shared.bean.NoticeListModel;
import com.weiju.ccmall.shared.component.NoticeView;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.util.ConvertUtil;

import java.util.ArrayList;
import java.util.List;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/7/21.
 */
public class NoticeElement extends LinearLayout {

    private NoticeView mTvContent;
    private ProgressBar mProgressBar;

    public NoticeElement(Context context, Element element) {
        super(context);
        initView(element);
    }


    private void initView(Element element) {
        View view = inflate(getContext(), R.layout.el_notice_layout, this);
        element.setBackgroundInto(view);
        mTvContent = (NoticeView) view.findViewById(R.id.tvContent);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getContext().startActivity(new Intent(getContext(), NoticeListActivity.class));
            }
        });
        ArrayList<NoticeListModel.DatasEntity> datasEntities = ConvertUtil.json2NoticeList(element.data);
        List<String> names = new ArrayList<>();
        for (NoticeListModel.DatasEntity data : datasEntities) {
            names.add(data.title);
        }
        mTvContent.addNotice(names);
        mTvContent.startFlipping();
        mTvContent.setVisibility(VISIBLE);

    }
}
