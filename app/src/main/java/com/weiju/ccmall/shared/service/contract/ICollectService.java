package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.module.collect.bean.ShopCollectItem;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Chan on 2017/6/15.
 */

public interface ICollectService {

    @GET("collect/getMemberCollectList")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getCollectList(@Query("pageOffset") int page);

    @FormUrlEncoded
    @POST()
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> changeCollect(@Url String url, @Field("skuId") String skuId);

    @GET("collect/getStoreList")
    Observable<RequestResult<PaginationEntity<ShopCollectItem, Object>>> getStoreList(@Query("pageOffset") int page);
}
