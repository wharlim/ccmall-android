package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.MainAdModel;
import com.weiju.ccmall.shared.util.EventUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainAdView extends LinearLayout {

    @BindView(R.id.ivClose)
    ImageView mIvClose;
    @BindView(R.id.layoutMainAd)
    LinearLayout mLayoutMainAd;
    @BindView(R.id.ivAd)
    ImageView mIvAd;

    private MainAdModel mResult;
    private OnClickListener mListener;


    public MainAdView(Context context) {
        this(context, null);
    }

    public MainAdView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.view_main_ad, this);
        ButterKnife.bind(this);
    }

    public void setCloseClickListener(OnClickListener listener) {
        mListener = listener;
    }

    @OnClick(R.id.ivClose)
    public void onClose() {
        if (mListener != null) {
            mListener.onClick(mIvClose);
        }
    }

    @OnClick(R.id.ivAd)
    public void onAdClicked() {
        if (mResult != null) {
            EventUtil.compileEvent(getContext(), mResult.event, mResult.target, false);
        }
        if (mListener != null) {
            mListener.onClick(mIvClose);
        }
    }

    public void setData(MainAdModel result) {
        mResult = result;
        Picasso.with(getContext()).load(result.backUrl).into(mIvAd);
    }
}
