package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author Ben
 * @date 2020/5/11.
 */
public class VouchersTransfer {

    /**
     * status : false
     * code :
     * msg : 提货券:0211578014946634赠送成功;提货券:0161577398755768赠送成功;提货券:0211578014945757赠送成功;
     * data : {"successCount":3,"failCount":0,"successList":[{"goodsId":"dd40c8aad0744f0780591b7005a8dc53","goodsCode":"0211578014946634","vouchersId":"2a640c0b39e0405f9adc70bfbbbbfbb8","title":"提货券408","iconUrl":"https://static.create-chain.net/ccmall/59/0d/37/cde3cea0c9164b8bb4cb04a5ac16a19c.jpg","cost":40800,"status":0,"useDate":"","expireDate":"2020-02-01 00:00:00","orderCode":"","orderId":"9652f0633734450fb7bdac9de68c1522","orderSeries":1,"transferDate":"","inMemberId":"","inMemberPhone":"","outMemberId":"","outMemberPhone":"","memberId":"dac68adea0f44385ae2b84e03609a285"},{"goodsId":"9a15629032574e8ba4a463ff7771027f","goodsCode":"0161577398755768","vouchersId":"a59cbdfb86914be3ba2e121c52e3a814","title":"提货券398A","iconUrl":"https://static.create-chain.net/ccmall/da/fb/5a/a5ccc3bf56e14364b791eb87442d9202.png","cost":39800,"status":0,"useDate":"","expireDate":"2020-01-01 00:00:00","orderCode":"","orderId":"4cec3dcf2945439eb3a1188aec51ddc1","orderSeries":2,"transferDate":"","inMemberId":"","inMemberPhone":"","outMemberId":"","outMemberPhone":"","memberId":"dac68adea0f44385ae2b84e03609a285"},{"goodsId":"189c164c26814ab7a532d1e107ab9a84","goodsCode":"0211578014945757","vouchersId":"a30319e7dbb14b308204bb3f9266cd3e","title":"提货劵418","iconUrl":"https://static.create-chain.net/ccmall/43/92/2e/cb4b134d9a484ee2972e16d1b345f871.png","cost":41800,"status":0,"useDate":"","expireDate":"2020-02-01 00:00:00","orderCode":"","orderId":"e86f3fdad2a74cf499d5105d3709138a","orderSeries":1,"transferDate":"","inMemberId":"","inMemberPhone":"","outMemberId":"","outMemberPhone":"","memberId":"dac68adea0f44385ae2b84e03609a285"}],"failList":[]}
     */

    @SerializedName("status")
    public boolean status;
    @SerializedName("code")
    public String code;
    @SerializedName("msg")
    public String msg;
    @SerializedName("data")
    public DataBean data;

    public static class DataBean {
        /**
         * successCount : 3
         * failCount : 0
         * successList : [{"goodsId":"dd40c8aad0744f0780591b7005a8dc53","goodsCode":"0211578014946634","vouchersId":"2a640c0b39e0405f9adc70bfbbbbfbb8","title":"提货券408","iconUrl":"https://static.create-chain.net/ccmall/59/0d/37/cde3cea0c9164b8bb4cb04a5ac16a19c.jpg","cost":40800,"status":0,"useDate":"","expireDate":"2020-02-01 00:00:00","orderCode":"","orderId":"9652f0633734450fb7bdac9de68c1522","orderSeries":1,"transferDate":"","inMemberId":"","inMemberPhone":"","outMemberId":"","outMemberPhone":"","memberId":"dac68adea0f44385ae2b84e03609a285"},{"goodsId":"9a15629032574e8ba4a463ff7771027f","goodsCode":"0161577398755768","vouchersId":"a59cbdfb86914be3ba2e121c52e3a814","title":"提货券398A","iconUrl":"https://static.create-chain.net/ccmall/da/fb/5a/a5ccc3bf56e14364b791eb87442d9202.png","cost":39800,"status":0,"useDate":"","expireDate":"2020-01-01 00:00:00","orderCode":"","orderId":"4cec3dcf2945439eb3a1188aec51ddc1","orderSeries":2,"transferDate":"","inMemberId":"","inMemberPhone":"","outMemberId":"","outMemberPhone":"","memberId":"dac68adea0f44385ae2b84e03609a285"},{"goodsId":"189c164c26814ab7a532d1e107ab9a84","goodsCode":"0211578014945757","vouchersId":"a30319e7dbb14b308204bb3f9266cd3e","title":"提货劵418","iconUrl":"https://static.create-chain.net/ccmall/43/92/2e/cb4b134d9a484ee2972e16d1b345f871.png","cost":41800,"status":0,"useDate":"","expireDate":"2020-02-01 00:00:00","orderCode":"","orderId":"e86f3fdad2a74cf499d5105d3709138a","orderSeries":1,"transferDate":"","inMemberId":"","inMemberPhone":"","outMemberId":"","outMemberPhone":"","memberId":"dac68adea0f44385ae2b84e03609a285"}]
         * failList : []
         */

        @SerializedName("successCount")
        public int successCount;
        @SerializedName("failCount")
        public int failCount;
        @SerializedName("successList")
        public List<PickUp> successList;
        @SerializedName("failList")
        public List<PickUp> failList;

    }
}
