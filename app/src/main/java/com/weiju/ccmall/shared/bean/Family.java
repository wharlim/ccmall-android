package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Family {

    /**
     * datas : [{"headImage":"http://img.kangerys.com/G1/M00/00/00/CqxOrVehYx2Ac2QBAAA1kHVRyx0171.jpg","nickName":"Geoffrey","phone":"15626204068","currMonthSaleMoney":0},{"headImage":"http://wx.qlogo.cn/mmopen/C2rEUskXQiblFYMUl9O0G05Q6pKibg7V1WpHX6CIQaic824apriabJw4r6EWxziaSt5BATrlbx1GVzwW2qjUCqtYpDvIJLjKgP1ug/0","nickName":"JayChan","phone":"13560463108","currMonthSaleMoney":0}]
     * pageOffset : 1
     * pageSize : 2
     * totalRecord : 17
     * totalPage : 9
     * ex : {}
     */

    @SerializedName("pageOffset")
    public int pageOffset;
    @SerializedName("pageSize")
    public int pageSize;
    @SerializedName("totalRecord")
    public int totalRecord;
    @SerializedName("totalPage")
    public int totalPage;
    @SerializedName("datas")
    public List<DatasEntity> datas;

    public static class DatasEntity {

        /**
         * memberId : 205a04bac6c641febcc23650c58aa0ed
         * headImage : https://static.create-chain.net/ccmall/22/67/f6/07c101456c464e29b80e5c7c43d1cf93.jpg
         * nickName : dddddd
         * phone : 15625435846
         * memberType : 0
         * memberTypeStr : 旗舰店
         * noteName : 二狗
         * currMonthSaleMoney : 0
         */

        @SerializedName("memberId")
        public String memberId;
        @SerializedName("headImage")
        public String headImage;
        @SerializedName("nickName")
        public String nickName;
        @SerializedName("phone")
        public String phone;
        @SerializedName("memberType")
        public int memberType;
        @SerializedName("memberTypeStr")
        public String memberTypeStr;
        @SerializedName("noteName")
        public String noteName;
        @SerializedName("currMonthSaleMoney")
        public int currMonthSaleMoney;
    }
}
