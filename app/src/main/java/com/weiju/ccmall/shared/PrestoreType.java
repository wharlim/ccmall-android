package com.weiju.ccmall.shared;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

/**
 * @author chenyanming
 * @time 2019/12/17 on 11:38
 * @desc
 */
public class PrestoreType extends BaseModel {
    /**
     * name : 预存
     * value : 0
     */

    @SerializedName("name")
    public String name;
    @SerializedName("value")
    public String value;

    public PrestoreType(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
