package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.BannedUser;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.adapter.LiveBannedAdapter;
import com.weiju.ccmall.shared.component.adapter.LiveUserAdapter;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.ToastUtil;


import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author chenyanming
 * @date 2020/2/21.
 */
public class LiveBannedListDialog extends Dialog {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;

    private final String mLiveId;
    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);
    private Activity mContext;
    private int mCurrentPage = 1;
    private LiveBannedAdapter mLiveBannedAdapter;


    public LiveBannedListDialog(@NonNull Activity context, String liveId) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mLiveId = liveId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_banned_list);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.dimAmount = 0.0f;
        getWindow().setAttributes(layoutParams);
        initView();
        getBanningMessageList();
    }

    private void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.addOnItemTouchListener(new OnItemChildClickListener() {
            @Override
            public void onSimpleItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                BannedUser item = mLiveBannedAdapter.getItem(position);
                mLiveBannedAdapter.getItem(position);
                switch (view.getId()) {
                    case R.id.tvUnbanning:
                        unbanningMessage(item.memberId);
                        break;
                }
            }
        });

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCurrentPage++;
                getBanningMessageList();
            }
        });

        mLiveBannedAdapter = new LiveBannedAdapter();


        View inflate = View.inflate(mContext, R.layout.cmp_no_data, null);
        mLiveBannedAdapter.setEmptyView(inflate);

        mLiveBannedAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentPage++;
                getBanningMessageList();
            }
        }, mRecyclerView);

        mLiveBannedAdapter.setHeaderAndEmpty(true);
        mLiveBannedAdapter.setFooterViewAsFlow(true);

        mRecyclerView.setAdapter(mLiveBannedAdapter);
    }

    private void unbanningMessage(String memberId) {
        APIManager.startRequest(mILiveService.unbanningMessage(mLiveId, memberId), new BaseRequestListener<Object>(mContext) {
            @Override
            public void onSuccess(Object result) {
                ToastUtil.success("解除成功");
                mCurrentPage = 1;
                getBanningMessageList();
            }
        }, getContext());
    }

    private void getBanningMessageList() {
        APIManager.startRequest(mILiveService.getBanningMessageList(mLiveId, mCurrentPage, 20),
                new BaseRequestListener<PaginationEntity<BannedUser, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<BannedUser, Object> result) {
                        if (mCurrentPage == 1) {
                            mLiveBannedAdapter.setNewData(result.list);
                        } else {
                            mLiveBannedAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mLiveBannedAdapter.loadMoreEnd();
                        } else {
                            mLiveBannedAdapter.loadMoreComplete();
                        }
                    }
                }, mContext);
    }


}
