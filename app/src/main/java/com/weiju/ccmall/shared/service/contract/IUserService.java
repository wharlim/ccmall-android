package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.module.NearStore.model.NearStoreModel;
import com.weiju.ccmall.module.auth.model.AuthModel;
import com.weiju.ccmall.module.auth.model.CardDetailModel;
import com.weiju.ccmall.module.auth.model.CardItemModel;
import com.weiju.ccmall.module.qrcode.model.GetSubscribe;
import com.weiju.ccmall.module.user.model.SignModel;
import com.weiju.ccmall.module.user.model.UpMemberModel;
import com.weiju.ccmall.module.xysh.bean.Translation;
import com.weiju.ccmall.newRetail.bean.ClientBeanList;
import com.weiju.ccmall.newRetail.bean.UpMemberInfoBean;
import com.weiju.ccmall.shared.bean.Achievement;
import com.weiju.ccmall.shared.bean.AutoAuthStatus;
import com.weiju.ccmall.shared.bean.BindPhoneMsgModel;
import com.weiju.ccmall.shared.bean.CardUrl;
import com.weiju.ccmall.shared.bean.CheckNumber;
import com.weiju.ccmall.shared.bean.Contribution;
import com.weiju.ccmall.shared.bean.ContributionSort;
import com.weiju.ccmall.shared.bean.Family;
import com.weiju.ccmall.shared.bean.FamilyOrder;
import com.weiju.ccmall.shared.bean.GongMaoAuth;
import com.weiju.ccmall.shared.bean.HasPasswordModel;
import com.weiju.ccmall.shared.bean.MemberAchievement;
import com.weiju.ccmall.shared.bean.MemberRatio;
import com.weiju.ccmall.shared.bean.MemberStore;
import com.weiju.ccmall.shared.bean.MyStatus;
import com.weiju.ccmall.shared.bean.PayTypeModel;
import com.weiju.ccmall.shared.bean.ProfitMoney;
import com.weiju.ccmall.shared.bean.RuleIntro;
import com.weiju.ccmall.shared.bean.ScoreStat;
import com.weiju.ccmall.shared.bean.StoreFreight;
import com.weiju.ccmall.shared.bean.SuperGroup;
import com.weiju.ccmall.shared.bean.SuperMatch;
import com.weiju.ccmall.shared.bean.Unused;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.UserToken;
import com.weiju.ccmall.shared.bean.VipTypeInfo;
import com.weiju.ccmall.shared.bean.WeChatLoginModel;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * 用户接口
 * Created by JayChan on 2016/12/13.
 */
public interface IUserService {

    /**
     * @param username
     * @param password
     * @param deviceType
     * @param type       0密码登录，1验证码登录，2微信登录
     * @return
     */
    @FormUrlEncoded
    @POST("login")
    Observable<RequestResult<UserToken>> login(@Field("username") String username,
                                               @Field("password") String password,
                                               @Field("deviceType") int deviceType,
                                               @Field("type") int type);

    @POST("logout")
    Observable<RequestResult<Object>> logout();

    @FormUrlEncoded
    @POST("user/add")
    Observable<RequestResult<Object>> register(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @Headers("Content-Type:application/x-www-form-urlencoded; charset=utf-8")
    @POST("user/addWithNoPass")
    Observable<RequestResult<CheckNumber>> registerNoPassword(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("user/checkAfs")
    Observable<RequestResult<Object>> checkAfs(
            @Field("sig") String sig,
            @Field("token") String token,
            @Field("sessionId") String sessionId,
            @Field("remoteIp") String remoteIp
    );

    @FormUrlEncoded
    @Headers("Content-Type:application/x-www-form-urlencoded; charset=utf-8")
    @POST("user/xcxAdd")
    Observable<RequestResult<CheckNumber>> xcxAdd(@FieldMap HashMap<String, String> params);

    @GET("user/getUserInfo")
    Observable<RequestResult<User>> getUserInfo();

    @GET("user/getMemberInfoById")
    Observable<RequestResult<User>> getMemberInfoById( @Query("memberId") String memberId);

    @GET("user/getMemberInfoByInviteCode")
    Observable<RequestResult<User>> getUserInfoByCode(@Query("inviteCode") String invitationCode);

    @GET("user/getMemberInfoByPhone")
    Observable<RequestResult<User>> getUserInfoByPhone(@Query("phone") String phone);

    @FormUrlEncoded
    @POST("user/bindingMember")
    Observable<RequestResult<User>> bindingMember(@Field("phone") String phone);

    @FormUrlEncoded
    @POST("user/edit")
    Observable<RequestResult<Object>> editNickname(@Field("nickName") String nickname);

    @FormUrlEncoded
    @POST("user/putPassword")
    Observable<RequestResult<Object>> putPassword(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("user/putPayPwd")
    Observable<RequestResult<Object>> putPayPwd(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("user/changBindPhone")
    Observable<RequestResult<Object>> editPhone(@Field("phone") String newPhone, @Field("checkNumber") String captcha, @Field("password") String password);

    @FormUrlEncoded
    @POST("user/bingPhone")
    Observable<RequestResult<Object>> bindPhone(@Field("phone") String newPhone, @Field("checkNumber") String captcha, @Field("password") String password);

    @FormUrlEncoded
    @POST("user/chagesUserPassowrd")
    Observable<RequestResult<Object>> editPassword(@Field("password") String password, @Field("newPass") String newPassword);

    @FormUrlEncoded
    @POST("user/changeUserPayPwd")
    Observable<RequestResult<Object>> editPayPassword(@Field("payPwd") String payPwd, @Field("newPayPwd") String newPayPwd);

    @FormUrlEncoded
    @POST("user/changeHead")
    Observable<RequestResult<Object>> updateAvatar(@Field("headImage") String imageUrl);

    @FormUrlEncoded
    @POST("user/checkUserInfo")
    Observable<RequestResult<Object>> checkUserInfo(
            @Field("phone") String phone,
            @Field("checkNumber") String string,
            @Field("password") String token
    );

    @GET("accountAuth/get")
    Observable<RequestResult<AuthModel>> getAuth();

    @GET("auth/info")
    Observable<RequestResult<CardDetailModel>> getGongCatCard(@Query("token") String token);

    @GET("account/get")
    Observable<RequestResult<CardDetailModel>> getCard();

    @GET("auth/gongMallAuth")
    Observable<RequestResult<GongMaoAuth>> getGongMallAuth();

    @GET("bank/list")
    Observable<RequestResult<List<CardItemModel>>> getBankList();

    @POST
    @FormUrlEncoded
    Observable<RequestResult<CardUrl>> addCard(
            @Url String url,
            @FieldMap Map<String, String> body
    );

    @GET("accountAuth/getStatus")
    Observable<RequestResult<AutoAuthStatus>> getStatus();

    @POST
    @FormUrlEncoded
    Observable<RequestResult<Object>> submitAuth(
            @Url String url,
            @FieldMap Map<String, String> body
    );

    @GET("family/getFamilyOrderList")
    Observable<RequestResult<FamilyOrder>> getFamilyOrderList(
            @Query("pageOffset") int page,
            @Query("pageSize") int size,
            @Query("level") int level
    );

    @GET("family/getFamilyList")
    Observable<RequestResult<Family>> getFamilyList(
            @Query("pageOffset") int page,
            @Query("pageSize") int size,
            @Query("level") int level,
            @Query("memberType") int memberType
    );

    @GET("stat/myStat")
    Observable<RequestResult<MyStatus>> getMyStatus();

    @POST("transfer/doTransfer")
    @FormUrlEncoded
    Observable<RequestResult<Object>> doTransfer(
            @Field("payeePhone") String payeePhone,
            @Field("transferMoney") long transferMoney,
            @Field("trsMemo") String trsMemo,
            @Field("password") String password,
            @Field("checkNumber") String checkNumber
    );

    @POST("transfer/goldTransfer")
    @Headers("Content-Type: application/json;charset=UTF-8")
    Observable<RequestResult<Object>> transferGoldTransfer(
            @Body RequestBody body
    );


    @GET("user/getUpMember")
    Observable<RequestResult<UpMemberModel>> getUpMember();

    @FormUrlEncoded
    @POST("user/setInvitationCode")
    Observable<RequestResult<Object>> setInvitationCode(@Field("invitationCode") String invitationCode);

    @POST("score/signin")
    Observable<RequestResult<SignModel>> sign();

    @GET("user/getSubscribe")
    Observable<RequestResult<GetSubscribe>> getSubscribe();

    /**
     * @return 获取会员打折率，会员拥有积分，积分使用规则
     */
    @GET("sysConfig/getScoreStat")
    Observable<RequestResult<ScoreStat>> getScoreStat();

    /**
     * @return 获取会员打折率
     */
    @GET("sysConfig/getMemberRatio")
    Observable<RequestResult<List<MemberRatio>>> getMemberRatio();

    /**
     * @return 获取支付类型
     */
    @GET("payConfig/getPayTypeList?appType=1")
    Observable<RequestResult<List<PayTypeModel>>> getPayTypes(@Query("goodsType") String goodsType);

    /**
     * @return 获取支付类型
     */
    /*@GET("payConfig/getPayTypeList?appType=2")
    Observable<RequestResult<List<PayTypeModel>>> getPayTypes2();*/
    @POST("score/transferScore")
    @Headers("Content-Type: application/json;charset=UTF-8")
    Observable<RequestResult<Object>> transferScore(
            @Body RequestBody body
    );

    /**
     * @return 获取店铺配置
     */
    @GET("store/getMemberStore")
    Observable<RequestResult<MemberStore>> getMemberStore();

    /**
     * @return 获取店铺配置
     */
    @GET("store/getEditMemberStore")
    Observable<RequestResult<MemberStore>> getEditMemberStore();

    /**
     * @return 获取会员打折率
     */
    @GET("store/getStoreFreight")
    Observable<RequestResult<List<StoreFreight>>> getStoreFreight();

    @POST("store/addOrUpdateMemberStore")
    @Headers("Content-Type: application/json;charset=UTF-8")
    Observable<RequestResult<Object>> addOrUpdateMemberStore(
            @Body RequestBody body
    );

    /**
     * @return 获取销售额 月销售额
     */
    @GET("achievement/getMemberAchievement")
    Observable<RequestResult<MemberAchievement>> getMemberAchievement();

    @POST("store/saveStoreFreight")
    Observable<RequestResult<Object>> saveStoreFreight(
            @Body List<StoreFreight> body
    );

    @GET("store/getNearByStoreList")
    Observable<RequestResult<NearStoreModel>> getNearStoreModelList(
            @Query("lag") double longitude,
            @Query("lat") double latitude,
            @Query("province") String province,
            @Query("city") String city,
            @Query("pageOffset") int page,
            @Query("pageSize") int size
    );

    @GET("store/getNearByStoreDetail")
    Observable<RequestResult<NearStoreModel>> getNearByStoreDetail(
            @Query("memberId") String memberId
    );

    @GET("memberRuleIntro/getRuleIntro")
    Observable<RequestResult<List<RuleIntro>>> getRuleIntro();

    @GET("user/hasPassword")
    Observable<RequestResult<HasPasswordModel>> hasPassowrd();

    @GET("user/hasPayPwd")
    Observable<RequestResult<HasPasswordModel>> hasPayPwd();

    @GET("user/checkIsLogin")
    Observable<RequestResult<User>> checkIsLogin();

    @GET("user/getVipTypeInfo")
    Observable<RequestResult<List<VipTypeInfo>>> getVipTypeInfo();

    @GET("weChatOpen/getAccessToken")
    Observable<RequestResult<WeChatLoginModel>> getAccessToken(
            @Query("code") String unionid
    );

    @FormUrlEncoded
    @POST("user/firstBindPhone")
    Observable<RequestResult<BindPhoneMsgModel>> firstBindPhone(
            @Field("phone") String newPhone,
            @Field("checkNumber") String captcha,
            @Field("password") String password
    );

    @GET("stat/contribution")
    Observable<RequestResult<Contribution>> getContribution();

    @POST("transfer/moneyTransferToGold")
    @Headers("Content-Type: application/json;charset=UTF-8")
    Observable<RequestResult<Object>> moneyTransferToGold(
            @Body RequestBody body
    );

    @GET()
    Observable<RequestResult<List<ContributionSort>>> getContributionSortList(
            @Url String url,
            @Query("type") String type,
            @Query("dateType") String dateType);

    /**
     * 提货券未使用数量
     *
     * @return
     */
    @GET("vouchers/unused")
    Observable<RequestResult<Unused>> getUnused();


    @GET("superGroupOrder/getSuperGroupList")
    Observable<RequestResult<PaginationEntity<SuperGroup, Object>>> getSuperGroupList(
            @Query("pageOffset") int page,
            @Query("pageSize") int size
    );

    @GET("superGroupOrder/getSuperMatchList")
    Observable<RequestResult<PaginationEntity<SuperMatch, Object>>> getSuperMatchList(
            @Query("pageOffset") int page,
            @Query("pageSize") int size,
            @Query("groupCode") String groupCode
    );
    @GET("user/checkBindUserInfo")
    Observable<RequestResult<User>> checkBindUserInfo(@Query("phone") String mobile);

    @GET("user/getNewGiftUserInfo")
    Observable<RequestResult<User>> getNewGiftUserInfo();

    @GET("user/checkIsBind")
    Observable<RequestResult<UpMemberInfoBean>> checkIsBind();

    @GET("family/getClientList")
    Observable<RequestResult<ClientBeanList>> getClientList(@Query("pageOffset") String pageOffset, @Query("pageSize") String pageSize, @Query("vipType") String vipType);

    @GET("user/tencentIMLogin")
    Observable<RequestResult<Object>> tencentIMLogin();
    @GET("getIndexMember")
    Observable<RequestResult<UserToken>> getIndexMember(@Query("index") int index);

    @POST("family/saveMemberNoteName")
    @FormUrlEncoded
    Observable<RequestResult<Object>> saveMemberNoteName(
//            @Body RequestBody body
            @Field("memberId") String memberId, @Field("noteName") String noteName
    );
    @GET("family/searchClientList")
    Observable<RequestResult<Family>> searchClientList(
            @Query("pageOffset") int page,
            @Query("pageSize") int size,
            @Query("keyword") String keyword
//            @Query("level") int level,
//            @Query("memberType") int memberType
    );
    @GET("family/getAwardClientList")
    Observable<RequestResult<Family>> getAwardClientList(
            @Query("pageOffset") int page,
            @Query("pageSize") int size
    );


    @GET("channel-achievement")
    Observable<RequestResult<Achievement>> getAchievement();


    @GET("user/memberProfit")
    Observable<RequestResult<ProfitMoney>> memberProfit();

    @GET("rongyun/getToken")
    Observable<RequestResult<UserToken>> getToken();

    @GET("rongyun/getToken")
    Observable<RequestResult<UserToken>> getTokenTourist(@Query("touristId") String touristId);

    @GET("user/getMoveTag")
    Observable<RequestResult<Translation>> getMoveTag();

    @FormUrlEncoded
    @POST("user/moveCcpayVip")
    Observable<RequestResult<Object>> moveCcpayVip(@Field("Invalid") String Invalid);


}
