package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/11/22 on 12:02
 * @desc ${TODD}
 */
public class AddLive {

    /**
     * liveId : daaf9c5cae6a40179b649f84a1a70350
     * memberId : 58a6ee26caa1451baf78abb4150ba92e
     * userId : 41474326
     * webinarId : 246401641
     * startTime : 2019-11-25 12:00:00
     * title : 陈燕明Android直播
     * liveDoc : https://static.mingpinvip.cn/qqmpapp/33/ed/c6/33edc69f5e014ac39a83cf0dfc08735e.jpg
     * liveImage : https://static.mingpinvip.cn/qqmpapp/c5/84/72/c5847276805f40fa934b2ade16a3e1b0.jpg
     * livePassword : 123456
     * status : 0
     * statusSTr : null
     * intro :
     * topics :
     * type : 0
     * autoRecord : 1
     * isChat : 0
     * host :
     * buffer : 3
     * isAllowExtension : 1
     * createDate : 2019-11-22 12:01:13
     * updateDate : 2019-11-22 12:01:13
     * deleteFlag : 0
     */

    @SerializedName("liveId")
    public String liveId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("userId")
    public String userId;
    @SerializedName("webinarId")
    public String webinarId;
    @SerializedName("startTime")
    public String startTime;
    @SerializedName("title")
    public String title;
    @SerializedName("liveDoc")
    public String liveDoc;
    @SerializedName("liveImage")
    public String liveImage;
    @SerializedName("livePassword")
    public String livePassword;
    @SerializedName("status")
    public int status;
    @SerializedName("statusSTr")
    public Object statusSTr;
    @SerializedName("intro")
    public String intro;
    @SerializedName("topics")
    public String topics;
    @SerializedName("type")
    public int type;
    @SerializedName("autoRecord")
    public int autoRecord;
    @SerializedName("isChat")
    public int isChat;
    @SerializedName("host")
    public String host;
    @SerializedName("buffer")
    public int buffer;
    @SerializedName("isAllowExtension")
    public int isAllowExtension;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
    @SerializedName("pushUrl")
    public String pushUrl;
}
