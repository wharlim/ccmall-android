package com.weiju.ccmall.shared.bean.body;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author chenyanming
 * @time 2019/11/22 on 11:48
 * @desc ${TODD}
 */
public class AddLiveBody {

    @SerializedName("liveId")
    public String liveId;
    @SerializedName("startTime")
    public String startTime;
    @SerializedName("title")
    public String title;
    @SerializedName("liveDoc")
    public String liveDoc;
    @SerializedName("liveImage")
    public String liveImage;
    @SerializedName("livePassword")
    public String livePassword;

    @SerializedName("forecastTime")
    public String forecastTime;

    @SerializedName("livePasswordDecode")
    public String livePasswordDecode;

    @SerializedName("skuIds")
    public List<String> skuIds;

    public AddLiveBody(String liveId, String startTime, String title, String liveDoc, String liveImage, String livePassword, List<String> skuIds) {
        this.liveId = liveId;
        this.startTime = startTime;
        this.title = title;
        this.liveDoc = liveDoc;
        this.liveImage = liveImage;
        this.livePassword = livePassword;
        this.skuIds = skuIds;
    }

    public void setForecastTime(String forecastTime) {
        this.forecastTime = forecastTime;
    }

    public void setLivePasswordDecode(String livePasswordDecode) {
        this.livePasswordDecode = livePasswordDecode;
    }
}
