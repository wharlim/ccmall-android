package com.weiju.ccmall.shared.message;

import android.annotation.SuppressLint;
import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

/**
 * @author chenyanming
 * @time 2020/1/18 on 17:24
 * @desc
 */
@SuppressLint("ParcelCreator")
@MessageTag(value = "CCM:Chatroom:Text:Message", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class ChatroomTextMessage extends MessageContent {
    public ChatroomTextMessage() {
    }

    //名字
    private String nickName;
    //头像
    private String headImage;
    //观众人数
    private String content;


    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("nickName", nickName);
            jsonObj.put("headImage", headImage);
            jsonObj.put("content", content);

        } catch (JSONException e) {
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ChatroomTextMessage(byte[] data) {
        String jsonStr = null;

        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e1) {

        }

        try {
            JSONObject jsonObj = new JSONObject(jsonStr);

            if (jsonObj.has("nickName")) {
                nickName = jsonObj.optString("nickName");
                setNickName(nickName);
            }

            if (jsonObj.has("headImage")) {
                headImage = jsonObj.optString("headImage");
                setHeadImage(headImage);
            }

            if (jsonObj.has("content")) {
                content = jsonObj.optString("content");
                setContent(content);
            }
        } catch (JSONException e) {
        }

    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }


    public void setHeadImage(String headImage) {
        this.headImage = headImage;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public String getNickName() {
        return nickName;
    }

    public String getHeadImage() {
        return headImage;
    }

    //给消息赋值。
    public ChatroomTextMessage(Parcel in) {
        nickName = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        headImage = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        content = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        //这里可继续增加你消息的属性
    }

    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<ChatroomTextMessage> CREATOR = new Creator<ChatroomTextMessage>() {

        @Override
        public ChatroomTextMessage createFromParcel(Parcel source) {
            return new ChatroomTextMessage(source);
        }

        @Override
        public ChatroomTextMessage[] newArray(int size) {
            return new ChatroomTextMessage[size];
        }
    };

    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     *
     * @return 一个标志位，表明Parcelable对象特殊对象类型集合的排列。
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * 将类的数据写入外部提供的 Parcel 中。
     *
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, nickName);//该类为工具类，对消息中属性进行序列化
        ParcelUtils.writeToParcel(dest, headImage);//该类为工具类，对消息中属性进行序列化
        ParcelUtils.writeToParcel(dest, content);//该类为工具类，对消息中属性进行序列化
        //这里可继续增加你消息的属性
    }
}
