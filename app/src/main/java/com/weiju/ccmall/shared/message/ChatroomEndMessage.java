package com.weiju.ccmall.shared.message;

import android.annotation.SuppressLint;
import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

/**
 * @author chenyanming
 * @time 2020/1/14 on 11:50
 * @desc
 */
@SuppressLint("ParcelCreator")
@MessageTag(value = "CCM:Chatroom:Live:End", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class ChatroomEndMessage extends MessageContent {
    public ChatroomEndMessage() {

    }

    private String versionName;

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("versionName", versionName);
        } catch (JSONException e) {
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ChatroomEndMessage(byte[] data) {
        String jsonStr = null;

        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e1) {

        }

        try {
            JSONObject jsonObj = new JSONObject(jsonStr);

            if (jsonObj.has("versionName")) {
                versionName = jsonObj.optString("versionName");
                setVersionName(versionName);
            }
        } catch (JSONException e) {
        }

    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getVersionName() {
        return versionName;
    }

    //给消息赋值。
    public ChatroomEndMessage(Parcel in) {
        versionName = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        //这里可继续增加你消息的属性
//        setRedId(redId);
    }

    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<ChatroomEndMessage> CREATOR = new Creator<ChatroomEndMessage>() {

        @Override
        public ChatroomEndMessage createFromParcel(Parcel source) {
            return new ChatroomEndMessage(source);
        }

        @Override
        public ChatroomEndMessage[] newArray(int size) {
            return new ChatroomEndMessage[size];
        }
    };

    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     *
     * @return 一个标志位，表明Parcelable对象特殊对象类型集合的排列。
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * 将类的数据写入外部提供的 Parcel 中。
     *
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, versionName);//该类为工具类，对消息中属性进行序列化
        //这里可继续增加你消息的属性
    }
}