package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OpeningLiveAuthInfo implements Serializable {

    /**
     * memberId : af8d785b384ad9281286275095141f15
     * authorizeNumTotal : 10
     * surplusAuthorizeNum : 8
     * createDate : 2020-03-11 20:54:45
     * updateDate : 2020-03-11 20:54:45
     * deleteFlag : 0
     */

    @SerializedName("memberId")
    public String memberId;
    @SerializedName("authorizeNumTotal")
    public int authorizeNumTotal;
    @SerializedName("surplusAuthorizeNum")
    public int surplusAuthorizeNum; // 剩余次数
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
}
