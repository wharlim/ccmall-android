package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.ExpressCompany;
import com.weiju.ccmall.shared.bean.ExpressDetails;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IExpressService {

    /**
     * 查询快递详情
     *
     * @param companyCode 快递名称代码
     * @param expressCode 快递号
     * @param id          传1
     * @param time        当前时间
     * @return
     */
    @GET("https://m.kuaidi100.com/query")
    Observable<ExpressDetails> getExpressDetails(
            @Query("type") String companyCode,
            @Query("postid") String expressCode,
            @Query("id") String id,
            @Query("temp") String time
    );

    /**
     * 查询快递详情
     *
     * @param companyCode 快递名称代码
     * @param expressCode 快递号
     * @param id          传1
     * @param time        当前时间
     * @return
     */
    @GET("https://m.kuaidi100.com/query")
    Observable<List<ExpressCompany>> listExpressCompany(
            @Query("postid") String expressCode
    );
}
