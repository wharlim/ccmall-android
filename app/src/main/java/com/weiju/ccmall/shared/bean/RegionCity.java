package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.contracts.IRegion;

import java.io.Serializable;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.bean
 * @since 2017-07-06
 */
public class RegionCity implements Serializable, IRegion {

    @SerializedName("cityId")
    public String id;
    @SerializedName("city")
    public String name;
    @SerializedName("provinceId")
    public String parentId;

    @Override
    public String getType() {
        return "city";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }
}
