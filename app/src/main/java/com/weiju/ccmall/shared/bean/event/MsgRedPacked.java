package com.weiju.ccmall.shared.bean.event;

import com.weiju.ccmall.module.auth.event.BaseMsg;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/8/6.
 */
public class MsgRedPacked extends BaseMsg {

    public final static int RED_PACKED_UPDATE = 1 << 0;
    public final static int RED_PACKED_FINISH = 1 << 1;
    public final static int RED_PACKED_SCROLL = 1 << 2;
    private int mScrollY;
    private int mOffset;
    private int mPosition;

    public MsgRedPacked(int action) {
        super(action);
    }

    public void setScrollY(int scrollY) {
        mScrollY = scrollY;
    }

    public int getScrollY() {
        return mScrollY;
    }

    public void setOffset(int offset) {
        mOffset = offset;
    }

    public void setPosition(int lastPosition) {
        mPosition = lastPosition;
    }

    public int getOffset() {
        return mOffset;
    }

    public int getPosition() {
        return mPosition;
    }
}
