package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/6/24 on 15:02
 * @desc ${TODD}
 */
public class ContributionSort {

    /**
     * sortIndex : 1
     * memberId : 1049901ba4644a368e849d2c60c0fe53
     * inviteCode : 6436795
     * headImage : http://static.wyhou.com/wyhou/1544396411575.jpg
     * phone : 18800000001
     * nickName : 技术001
     * userName : 陈乔恩
     * vipType : 9
     * vipTypeStr : 旗舰店
     * point : 2035300
     */

    @SerializedName("sortIndex")
    public int sortIndex;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("inviteCode")
    public int inviteCode;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("phone")
    public String phone;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("userName")
    public String userName;
    @SerializedName("vipType")
    public int vipType;
    @SerializedName("vipTypeStr")
    public String vipTypeStr;
    @SerializedName("point")
    public long point;
}
