package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.bean
 * @since 2017-06-06
 */
public class Page implements Serializable {

    public int model;
    @SerializedName("pageId")
    public String id;
    @SerializedName("pageName")
    public String name;

    public Page() {
    }

    public Page(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Page(String id, String name, int model) {
        this.model = model;
        this.id = id;
        this.name = name;
    }
}
