package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
public class ReceiveDetail {
    /**
     * receiveId : bd799aac14d445a5aed96355b1bab386
     * couponId : 7dad1585889d496bb64a7addddebf36c
     * memberId : dac68adea0f44385ae2b84e03609a285
     * headImage : http://thirdwx.qlogo.cn/mmopen/vi_32/ngNm6nYYe7N0944HXQArIIuY9TVyn2VSHLCY1vb4FHiaVibicOE7p2WsnEX9IkxznCuLJhJiaHIlIKdvUElEh3Pc2Q/132
     * nickName : 少龙
     * liveId : dae42bcc1c0e463faec0adefe3ef2fc3
     * status : 0
     * balanceCode : 1331582436427887
     * createDate : 2020-02-20 22:31:12
     * updateDate : 2020-02-20 22:31:12
     * deleteFlag : 0
     */

    @SerializedName("receiveId")
    public String receiveId;
    @SerializedName("couponId")
    public String couponId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("liveId")
    public String liveId;
    @SerializedName("status")
    public int status;
    @SerializedName("balanceCode")
    public String balanceCode;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
}
