package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.Point;
import com.weiju.ccmall.shared.bean.PointListExtra;
import com.weiju.ccmall.shared.bean.ScoreModel;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service.contract
 * @since 2017-08-03
 */
public interface IPointService {

    @GET("score/scorelist")
    Observable<RequestResult<PaginationEntity<Point, PointListExtra>>> getPointList(@Query("pageOffset") int page);

    @GET("score/getscore")
    Observable<RequestResult<ScoreModel>> getScore();
}
