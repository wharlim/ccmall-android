package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.CouponReceive;
import com.weiju.ccmall.shared.bean.CouponReceiveEx;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.LiveRedLoadMoreView;
import com.weiju.ccmall.shared.component.adapter.LiveCouponReceiveDetailAdapter;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.MoneyUtil;


import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @date 2020/2/21.
 */
public class LiveCouponReceiveDetailDialog extends Dialog {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.tvMoney)
    TextView mTvMoney;
    @BindView(R.id.tvContent)
    TextView mTvContent;
    @BindView(R.id.tvReceiveTip)
    TextView mTvReceiveTip;

    private final Activity mContext;
    private final String mLiveId;
    private final String mCouponId;
    private final String mSendId;


    private LiveCouponReceiveDetailAdapter mAdapter = new LiveCouponReceiveDetailAdapter();
    private int mCurrentPage = 1;

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);

    public LiveCouponReceiveDetailDialog(Activity context, String liveId, String couponId,String sendId) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mLiveId = liveId;
        mCouponId = couponId;
        mSendId = sendId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_coupon_receive);
        ButterKnife.bind(this);
        initView();
        getLiveCouponDetailList();
    }

    private void initView() {
        GridLayoutManager linearLayoutManager = new GridLayoutManager(getContext(), 4);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        View inflate = View.inflate(mContext, R.layout.cmp_no_data, null);
        inflate.findViewById(R.id.noDataLabel).setVisibility(View.GONE);
        mAdapter.setEmptyView(inflate);

        mAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentPage++;
                getLiveCouponDetailList();
            }
        }, mRecyclerView);


        mAdapter.setHeaderAndEmpty(true);
        mAdapter.setFooterViewAsFlow(true);
        mAdapter.setLoadMoreView(new LiveRedLoadMoreView());

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCurrentPage = 1;
                getLiveCouponDetailList();
            }
        });

    }

    private void getLiveCouponDetailList() {

        APIManager.startRequest(mService.getLiveCouponDetailList(mLiveId, mSendId,mCouponId, mCurrentPage, 20),
                new BaseRequestListener<PaginationEntity<CouponReceive, CouponReceiveEx>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<CouponReceive, CouponReceiveEx> result) {
                        if (mCurrentPage == 1) {
                            mAdapter.setNewData(result.list);
                            mTvMoney.setText(MoneyUtil.centToYuan¥StrNoZero(result.ex.liveCouponSend.cost));
                            mTvContent.setText(result.ex.liveCouponSend.couponTitle);

                            mTvReceiveTip.setText(result.ex.liveCouponSend.sendNum == result.total ?
                                    String.format("%s张优惠券已被抢完", result.ex.liveCouponSend.sendNum) :
                                    String.format("已领取%s/%s张优惠券", result.total, result.ex.liveCouponSend.sendNum));

                        } else {
                            mAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mAdapter.loadMoreEnd();
                        } else {
                            mAdapter.loadMoreComplete();
                        }
                    }
                }, mContext);
    }


    @OnClick(R.id.ivClose)
    protected void close() {
        dismiss();
    }

    @Override
    public void dismiss() {
        EventBus.getDefault().post(new EventMessage(Event.couponDialogDismiss));
        super.dismiss();
    }
}
