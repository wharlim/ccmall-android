package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class MyStatus {

    /**
     * couponCount : 1
     * messageCount : 0
     * bindWechatStatus : 0
     * bindBankStatus : 0
     * bindWechatStatusStr : 未绑定
     * bindBankStatusStr : 待审核
     */

    @SerializedName("couponCount")
    public int couponCount;
    @SerializedName("messageCount")
    public int messageCount;
    @SerializedName("bindWechatStatus")
    public int bindWechatStatus;
    @SerializedName("bindBankStatus")
    public int bindBankStatus;
    @SerializedName("bindWechatStatusStr")
    public String bindWechatStatusStr;
    @SerializedName("bindBankStatusStr")
    public String bindBankStatusStr;
    @SerializedName("gongmallAuthStatus")
    public int gongmallAuthStatus;
    /**
     * 0为非渠道，1为渠道
     */
    @SerializedName("isChannel")
    public int isChannel;
}
