package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class ProfitData {
    @SerializedName("profitSumMoney")
    public long profitSumMoney;
    @SerializedName("availableMoney")
    public long availableMoney;
    @SerializedName("freezeSumMoney")
    public long freezeSumMoney;
    /**
     * 购物券
     */
    @SerializedName("availableGold")
    public long availableGold;
    @SerializedName("goldenAvlTicket")
    public long goldenAvlTicket;

    public long availableGoldLuck;


    /**
     * profitSumMoney : 0
     * freezeSumMoney : 0
     * availableMoney : 1499900
     * profitSumCoin : 0
     * sumTicket : 800000
     */
    @SerializedName("profitSumCoin")
    public long profitSumCoin;
    @SerializedName("availableCoin")
    public long availableCoin;
    @SerializedName("sumTicket")
    public long sumTicket;
}
