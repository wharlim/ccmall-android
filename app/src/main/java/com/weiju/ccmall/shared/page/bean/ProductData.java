package com.weiju.ccmall.shared.page.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;
import com.weiju.ccmall.shared.bean.SkuInfo;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/9/13.
 */
public class ProductData extends BaseModel {

    /**
     * skuId : effd131bfc1b49968d62def8295b4829
     */

    @SerializedName("skuId")
    public String skuId;
    @SerializedName("image")
    public String image;
    @SerializedName("sku")
    public SkuInfo sku;
}
