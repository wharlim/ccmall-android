package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 头部&导航 布局
 * Created by JayChan on 16/10/14.
 */
public class HeaderLayout extends LinearLayout {

    @BindView(R.id.headerTitleTv)
    protected TextView mHeaderTitleTv;

    @BindView(R.id.headerLeftTv)
    protected TextView mHeaderLeftTv;

    @BindView(R.id.headerRightTv)
    protected TextView mHeaderRightTv;

    @BindView(R.id.headerLeftIv)
    protected ImageView mHeaderLeftIv;

    @BindView(R.id.headerRightIv)
    protected ImageView mHeaderRightIv;

    @BindView(R.id.headerRightIv2)
    protected ImageView mHeaderRightIv2;

    @BindView(R.id.layout)
    FrameLayout mHeaderLayout;
    @BindView(R.id.rightAnchor)
    View mRightAnchor;
    @BindView(R.id.hearderImage)
    ImageView mHearderImage;

    private View mRootView;

    public HeaderLayout(Context context) {
        super(context);
        initViews();
    }

    public HeaderLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HeaderLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initViews();
    }

    private void initViews() {
        this.hide();
        mRootView = LayoutInflater.from(getContext()).inflate(R.layout.header_layout, (ViewGroup) getRootView(), true);
        ButterKnife.bind(this, mRootView);
        mHeaderTitleTv.setText("debug".equalsIgnoreCase(BuildConfig.BUILD_TYPE) ? R.string.appNameDebug : R.string.appName);
    }

    public void show() {
        this.setVisibility(VISIBLE);
    }

    public void hide() {
        this.setVisibility(GONE);
    }

    public void setLeftDrawable(Drawable drawable) {
        mHeaderLeftIv.setImageDrawable(drawable);
        mHeaderLeftIv.setVisibility(VISIBLE);
    }

    public void setLeftDrawable(@DrawableRes int resId) {
        if (mHeaderLeftIv == null) {
            return;
        }
        mHeaderLeftIv.setImageResource(resId);
        mHeaderLeftIv.setVisibility(VISIBLE);
    }

    public void setOnLeftClickListener(OnClickListener onClickListener) {
        if (mHeaderLeftIv == null) {
            return;
        }
        mHeaderLeftTv.setOnClickListener(onClickListener);
        mHeaderLeftIv.setOnClickListener(onClickListener);
    }

    public void setRightDrawable(Drawable drawable) {
        mHeaderRightIv.setImageDrawable(drawable);
        mHeaderRightIv.setVisibility(VISIBLE);
    }

    public void setRightDrawable(@DrawableRes int resId) {
        mHeaderRightIv.setImageResource(resId);
        mHeaderRightIv.setVisibility(VISIBLE);
    }

    public void setRight2Drawable(@DrawableRes int resId) {
        mHeaderRightIv2.setImageResource(resId);
        mHeaderRightIv2.setVisibility(VISIBLE);
    }

    public ImageView getRightImageView() {
        return mHeaderRightIv;
    }

    public void setRightText(CharSequence charSequence) {
        mHeaderRightTv.setText(charSequence);
        mHeaderRightTv.setVisibility(VISIBLE);
    }

    public void setRightTextColor(int color) {
        mHeaderRightTv.setTextColor(getResources().getColor(color));
    }

    public void setRightText(@StringRes int resId) {
        String string = getResources().getString(resId);
        setRightText(string);
    }

    public CharSequence getRightText() {
        if (mHeaderRightTv.getVisibility() == GONE) {
            return "";
        }
        return mHeaderRightTv.getText();
    }


    public void setOnRightClickListener(OnClickListener onClickListener) {
        mHeaderRightTv.setOnClickListener(onClickListener);
        mHeaderRightIv.setOnClickListener(onClickListener);
    }

    public void setOnRight2ClickListener(OnClickListener onClickListener) {
//        mHeaderRightTv.setOnClickListener(onClickListener);
        mHeaderRightIv2.setOnClickListener(onClickListener);
    }

    public void setTitle(@StringRes int resId) {
        this.mHeaderTitleTv.setText(resId);
        this.show();
    }

    public void setTitle(CharSequence charSequence) {
        this.mHeaderTitleTv.setText(charSequence);
        this.show();
    }

    public void setTitleColor(@ColorRes int colorRes) {
        this.mHeaderTitleTv.setTextColor(getResources().getColor(colorRes));
        this.mHeaderRightTv.setTextColor(getResources().getColor(colorRes));
        this.mHeaderLeftTv.setTextColor(getResources().getColor(colorRes));
    }

    public void setTitleImage(int drawable){
        mHearderImage.setVisibility(VISIBLE);
        mHearderImage.setImageDrawable(getResources().getDrawable(drawable));
        this.mHeaderTitleTv.setVisibility(GONE);
        this.show();
    }

    public void makeHeaderRed() {
        setTitleColor(R.color.white);
        mRootView.setSelected(true);
    }

    public void makeHeaderColor(int res) {
        setTitleColor(R.color.white);
        mHeaderLayout.setBackgroundColor(getResources().getColor(res));
    }

    public void setHeaderBackgroundColor(int color) {
//        Log.d("Seven", "setHeaderBackgroundColor -> " + color);
        mHeaderLayout.setBackgroundColor(color);
    }


    public void setNoLine() {
        mHeaderLayout.setBackgroundColor(Color.WHITE);
    }

    public View getRightAnchor() {
        return mRightAnchor;
    }

    public void setDarkMode(int darkColorRes) {
        mHeaderLayout.setBackgroundColor(getResources().getColor(darkColorRes));
        setLeftDrawable(R.mipmap.icon_back_white);
        setTitleColor(R.color.white);
    }
}
