package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2020/1/8 on 10:44
 * @desc
 */
public class LiveRedEnvelopesEx {
    @SerializedName("redEnvelopesSend")
    public LiveRedEnvelopes redEnvelopesSend;

    @SerializedName("receiveDetail")
    public LiveRedEnvelopes receiveDetail;
}
