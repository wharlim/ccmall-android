package com.weiju.ccmall.shared.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.ToastUtils;
import com.orhanobut.logger.Logger;
import com.weiju.ccmall.module.MainActivity;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.balance.BalanceListActivity;
import com.weiju.ccmall.module.cart.CartActivity;
import com.weiju.ccmall.module.category.CategoryActivity;
import com.weiju.ccmall.module.category.CategoryFragment;
import com.weiju.ccmall.module.challenge.activity.ChallengeListActivity;
import com.weiju.ccmall.module.coupon.CouponCenterActivity;
import com.weiju.ccmall.module.coupon.CouponListActivity;
import com.weiju.ccmall.module.coupon.CouponShopActivity;
import com.weiju.ccmall.module.instant.InstantActivity;
import com.weiju.ccmall.module.jkp.IJkpProductService;
import com.weiju.ccmall.module.jkp.JiKaoPuProductDetailActivity;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.module.lottery.LotteryActivity;
import com.weiju.ccmall.module.message.MessageListActivity;
import com.weiju.ccmall.module.notice.NoticeDetailsActivity;
import com.weiju.ccmall.module.notice.NoticeListActivity;
import com.weiju.ccmall.module.order.OrderDetailActivity;
import com.weiju.ccmall.module.order.OrderListActivity;
import com.weiju.ccmall.module.order.RefundDetailActivity;
import com.weiju.ccmall.module.page.CustomPageActivity;
import com.weiju.ccmall.module.page.WebViewJavaActivity;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.module.product.NewUpPriductListActivity;
import com.weiju.ccmall.module.product.SalePriductListActivity;
import com.weiju.ccmall.module.shop.ShopActivity;
import com.weiju.ccmall.module.user.AboutUsActivity;
import com.weiju.ccmall.module.user.FamilyActivity;
import com.weiju.ccmall.module.user.ProfileActivity;
import com.weiju.ccmall.module.world.activity.WorldHomeActivity;
import com.weiju.ccmall.newRetail.Const;
import com.weiju.ccmall.newRetail.activity.CommunityActivity;
import com.weiju.ccmall.newRetail.activity.HiGouDescriptionActivity;
import com.weiju.ccmall.shared.basic.AgentFActivity;
import com.weiju.ccmall.shared.basic.BaseCallback;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Coupon;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.Product;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.SkuPvIds;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.enums.AccountType;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.CartManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.page.PageType;
import com.weiju.ccmall.shared.service.CouponService;
import com.weiju.ccmall.shared.service.ProductService;
import com.weiju.ccmall.shared.service.contract.ILiveService;

import org.greenrobot.eventbus.EventBus;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class EventUtil {
    public static void compileEvent(Context context, String event, String target, boolean isNewTask) {
        compileEvent(context, event, target, isNewTask, "");
    }
    public static void compileEvent(Context context, String event, String target, boolean isNewTask, String orderType) {
        Logger.e("Event: %s\n Target: %s", event, target);
        if ("none".equals(event) || StringUtils.isEmpty(event)) {
            return;
        }
        if ("link".equals(event)) {

            Intent intent = new Intent(context, WebViewJavaActivity.class);
            intent.putExtra("url", target);
            startActivity(context, intent, isNewTask);
            return;
        }
        Log.d("Seven", "ev -> " + event);
        if ("native".equals(event)) {
            compileNative(context, target, isNewTask, orderType);
        }
        if (IJkpProductService.event.equals(event)) {
            compileNative(context, target, isNewTask, orderType, true);
        }
    }
    private static void compileNative(final Context context, String target, boolean isNewTask, String orderType) {
        compileNative(context, target, isNewTask, orderType, false);
    }

    private static void compileNative(final Context context, String target, boolean isNewTask, String orderType, boolean isJkp) {
        final Uri parse = Uri.parse("app://" + target);
        switch (parse.getHost()) {
            case "product":
            case "product-instant":
                if (isJkp) {
                    JiKaoPuProductDetailActivity.start(context, parse.getQueryParameter("skuId"));
                } else {
                    viewProductDetail(context, parse.getQueryParameter("skuId"), isNewTask, orderType);
                }
                break;
            case "product-list":
                Intent intent = new Intent(context, CategoryActivity.class);
                intent.putExtra("categoryId", parse.getQueryParameter("categoryId"));
                startActivity(context, intent, isNewTask);
//                EventBus.getDefault().postSticky(new EventMessage(Event.changeCategory, parse.getQueryParameter("categoryId")));
                break;
            case "order-detail":
                viewOrderDetail(context, parse.getQueryParameter("orderCode"), isNewTask);
                break;
            case "order-list":
                viewOrderList(context, parse.getQueryParameter("type"), isNewTask);
            case "message":
                startActivity(context, new Intent(context, MessageListActivity.class), isNewTask);
                break;
            case "about-us":
                startActivity(context, new Intent(context, AboutUsActivity.class), isNewTask);
                break;
            case "instant":
                viewInstant(context, parse.getQueryParameter("id"), isNewTask);
                break;
            case "coupon":
                startActivity(context, new Intent(context, CouponListActivity.class), isNewTask);
                break;
            case "coupon-center":
                startActivity(context, new Intent(context, CouponCenterActivity.class), isNewTask);
                break;
            case "add-to-cart":
                addProductToCart(context, parse.getQueryParameter("skuId"), parse.getQueryParameter("cost"));
                break;
            case "category":
                viewCategory(context, isNewTask);
                break;
            case "profile":
                startActivity(context, new Intent(context, ProfileActivity.class), isNewTask);
                return;
            case "contact-us":
                CSUtils.start(context);
                // Contact Us;
                return;
            case "get-coupon":
                getCoupon(context, parse.getQueryParameter("couponId"));
                break;
            case "user-center":
                if (UiUtils.checkUserLogin(context)) {
                    viewUserCenter(context, isNewTask);
                }
                break;
            case "custom":
                if (isJkp) {
                    viewCustomPage(context, parse.getQueryParameter("pageId"), isNewTask, PageType.JKP);
                } else {
                    viewCustomPage(context, parse.getQueryParameter("pageId"), isNewTask, PageType.HOME);
                }
                break;
            case "new-products":
                startActivity(context, new Intent(context, NewUpPriductListActivity.class), isNewTask);
                break;
            case "cart":
                startActivity(context, new Intent(context, CartActivity.class), isNewTask);
                break;
            case "lottery":
                ////跳到抽奖
                if (UiUtils.checkUserLogin(context)) {
                    startActivity(context, new Intent(context, LotteryActivity.class), isNewTask);
                }
                break;
            case "sale-product":
                startActivity(context, new Intent(context, SalePriductListActivity.class), isNewTask);
                break;
            case "team-member":
                if (UiUtils.checkUserLogin(context)) {
                    // CCMALL会员
                    startActivity(context, new Intent(context, FamilyActivity.class), isNewTask);
                }
                break;
            case "refund-detail":
                viewRefundDetail(context, parse.getQueryParameter("refundId"), isNewTask);
                break;
            case "profit-list":
                if (UiUtils.checkUserLogin(context)) {
                    // 收入
                    Intent intent2 = new Intent(context, BalanceListActivity.class);
                    intent2.putExtra("type", AccountType.Profit);
                    if (isNewTask) {
                        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                    context.startActivity(intent2);
                }
                break;
            case "balance-list":
                if (UiUtils.checkUserLogin(context)) {
                    // 余额
                    Intent intent2 = new Intent(context, BalanceListActivity.class);
                    intent2.putExtra("type", AccountType.Balance);
                    if (isNewTask) {
                        intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    }
                }
                break;
            case "notice-detail":
                // 公告详情
                viewNoticeDetail(context, parse.getQueryParameter("id"), isNewTask);
                break;
            case "notice-list":
            case "notice":
                // 公告列表
                startActivity(context, new Intent(context, NoticeListActivity.class), isNewTask);
                break;
            case "challenge":
                startActivity(context, new Intent(context, ChallengeListActivity.class), isNewTask);
                break;
            case "community":
                startActivity(context, new Intent(context, CommunityActivity.class), isNewTask);
                break;
            /*case "customPage":
                viewCustomPage(context, parse.getQueryParameter("pageId"), isNewTask);
                break;*/

            /*case "product_detail":
                viewProductDetail(context, parse.getQueryParameter("sku_id"), false);
                break;*/
            case "ccpay":
                viewXinYong(context);
                break;
            /*case "community":
                startActivity(context, new Intent(context, CommunityActivity.class), isNewTask);
                break;*/
            /*case "customPage":
                viewCustomPage(context, parse.getQueryParameter("pageId"), isNewTask);
                break;*/

            /*case "product_detail":
                viewProductDetail(context, parse.getQueryParameter("sku_id"), false);
                break;*/
            /*case "ccpay":
                viewXinYong(context);
                break;*/
            case "hibuy-ShoppingInfo":
                HiGouDescriptionActivity.start(context, false);
//                startActivity(context, new Intent(context, HiGouDescriptionActivity.class), isNewTask);
                break;
            case "hibuy-BestSale":
                EventBus.getDefault().postSticky(new EventMessage(Event.select2Fragment));
                break;
            case "store":
                ShopActivity.start(context, parse.getQueryParameter("storeId"));
                break;
            case "live-list":
                //直播列表
//                if (UiUtils.checkUserLogin(context)) {
//                    startActivity(context, new Intent(context, LiveHomeActivity.class), isNewTask);
                    EventBus.getDefault().post(new EventMessage(Event.goToLiveHome));
                    Activity activity = (Activity) context;
                    if (activity instanceof MainActivity){
                        return;
                    }
                    activity.finish();
//                }
                break;
            case "live-room": {
                //直播房间
                String liveId = Objects.requireNonNull(parse.getQueryParameter("liveId")).trim();
                getLiveDetail(context, liveId, true);
            }
                break;
            case "live-broadcast": {
                // 直播推送
                String liveId = Objects.requireNonNull(parse.getQueryParameter("liveId")).trim();
                getLiveDetail(context, liveId, false);
            }
                break;
            case "globalShopping"://全球仓
                WorldHomeActivity.start(context);
                break;
            default:
                break;
        }
    }

    public static void getLiveDetail(Context context, String liveId, boolean showLoading) {
        if (showLoading) {
            ToastUtil.showLoading(context);
        }
        ILiveService iProductService = ServiceManager.getInstance().createService(ILiveService.class);
        APIManager.startRequest(iProductService.getLive(liveId), new BaseRequestListener<LiveRoom>() {
            @Override
            public void onSuccess(LiveRoom result) {
                ToastUtil.hideLoading();
                LiveRoom liveRoom = new LiveRoom();
                liveRoom.webinarId = result.webinarId;
                liveRoom.status = result.status;
                liveRoom.statusStr = result.statusStr;
                liveRoom.livePassword = result.livePassword;
                liveRoom.pageId = result.pageId;
                liveRoom.memberId = result.memberId;
                liveRoom.liveId = result.liveId;
                LiveManager.elementOnClick(context, liveRoom);
            }

            @Override
            public void onError(Throwable e) {
                super.onError(e);
                ToastUtil.hideLoading();
            }
        },context);

    }

    public static void viewNoticeDetail(Context context, String noticeId, boolean isNewTask) {
        Intent intent = new Intent(context, NoticeDetailsActivity.class);
        intent.putExtra(Config.INTENT_KEY_ID, noticeId);
        startActivity(context, intent, isNewTask);
    }

    /**
     * 跳到退款详情页面
     *
     * @param context
     * @param refundId
     * @param isNewTask
     */
    public static void viewRefundDetail(Context context, String refundId, boolean isNewTask) {
        if (UiUtils.checkUserLogin(context)) {
            Intent intent = new Intent(context, RefundDetailActivity.class);
            intent.putExtra(Config.INTENT_KEY_ID, refundId);
            startActivity(context, intent, isNewTask);
        }
    }

    private static void viewCustomPage(Context context, String pageId, boolean isNewTask, PageType pageType) {
        if (pageId == null || pageId.isEmpty()) {
            return;
        }
        Intent intent = new Intent(context, CustomPageActivity.class);
        intent.putExtra("pageId", pageId);
        intent.putExtra("pageType", pageType.name());
        startActivity(context, intent, isNewTask);
    }

    public static void viewUserCenter(Context context, boolean isNewTask) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("tab", "user-center");
        startActivity(context, intent, isNewTask);
        EventBus.getDefault().post(new EventMessage(Event.viewUserCenter));
    }

    public static void viewNearStoreList(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("tab", "near");
        context.startActivity(intent);
        EventBus.getDefault().post(new EventMessage(Event.viewNearStore));
    }

    private static void getCoupon(Context context, String couponId) {
        if (couponId == null || couponId.isEmpty()) {
            ToastUtil.error("优惠券错误");
            return;
        }
        CouponService.getCoupon(context, couponId);
    }

    private static void viewOrderList(Context context, String type, boolean isNewTask) {
        List<String> types = Arrays.asList("all", "unpay", "paid", "dispatched", "service", "comment");
        if (!types.contains(type)) {
            type = "all";
        }
        Intent intent = new Intent(context, OrderListActivity.class);
        intent.putExtra("type", type);
        startActivity(context, intent, isNewTask);
    }

    public static void viewOrderDetail(Context context, String orderCode, boolean isNewTask) {
        if (orderCode == null || orderCode.isEmpty()) {
            return;
        }
        Intent intent = new Intent(context, OrderDetailActivity.class);
        intent.putExtra("orderCode", orderCode);
        startActivity(context, intent, isNewTask);
    }

    public static void viewOrderDetailBySeller(Context context, String orderCode) {
        if (orderCode == null || orderCode.isEmpty()) {
            return;
        }
        Intent intent = new Intent(context, OrderDetailActivity.class);
        intent.putExtra("orderCode", orderCode);
        intent.putExtra("mode", OrderListActivity.MODE_SELLER);
        context.startActivity(intent);
    }

    public static void viewInstant(Context context, String id, boolean isNewTask) {
        Intent intent = new Intent(context, InstantActivity.class);
        if (!(id == null || id.isEmpty())) {
            intent.putExtra("id", id);
        }
        startActivity(context, intent, isNewTask);
    }

    private static void viewCategory(Context context, boolean isNewTask) {
        context.getSharedPreferences(Const.CLASSIFICATION_TYPE, 0).edit().putString(Const.CLASSIFICATION_TYPE, Const.NORMAL_MODE).commit();
        AgentFActivity.Companion.start(context, CategoryFragment.class, null);

//        Intent intent = new Intent(context, MainActivity.class);
//        intent.putExtra("tab", "category");
//        startActivity(context, intent, isNewTask);
//        EventBus.getDefault().post(new EventMessage(Event.viewCategory));
    }

    public static void viewHome(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("tab", "home");
        context.startActivity(intent);
        EventBus.getDefault().post(new EventMessage(Event.viewHome));
    }

    public static void viewXinYong(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("tab", "xinying");
        context.startActivity(intent);
        EventBus.getDefault().post(new EventMessage(Event.xinyong));
    }


    private static void addProductToCart(final Context context, String skuId, final String amountString) {
        if (skuId == null || skuId.isEmpty()) {
            return;
        }
        ProductService.getSkuInfoById(skuId, new BaseCallback<SkuInfo>() {
            @Override
            public void callback(SkuInfo skuInfo) {
                int amount = 0;
                if (!(amountString == null || amountString.isEmpty())) {
                    amount = Math.max(1, Integer.valueOf(amountString));
                }
                CartManager.addToCart(context, skuInfo, amount, false);
            }
        });
    }

    public static void viewProductDetail(Context context, String skuId, boolean isNewTask) {
        viewProductDetail(context, skuId, isNewTask, "");
    }

    public static void viewProductDetail(Context context, String skuId, boolean isNewTask, String orderType) {
        viewProductDetail(context, skuId, isNewTask, orderType, null);
    }

    public static void viewProductDetail(Context context, String skuId, boolean isNewTask, String orderType, String liveId) {
        if (skuId == null || skuId.isEmpty()) {
            return;
        }
        Intent intent = new Intent(context, NewProductDetailActivity.class);
        intent.putExtra(Key.SKU_ID, skuId);
        intent.putExtra(Const.ORDER_TYPE, orderType);
        if (!TextUtils.isEmpty(liveId)) {
            intent.putExtra("liveId", liveId);
        }
        startActivity(context, intent, isNewTask);
    }

    public static void viewCouponDetail(final Activity context, final Coupon coupon) {
        Date now = new Date();
        if (coupon == null || coupon.limitStartDate == null) {
            // 优惠券数据异常
        } else if (now.before(coupon.limitStartDate)) {
            ToastUtils.showShortToast("该优惠券还未到使用时间");
        } else if (coupon.couponType == 1) { // 店铺优惠券
            ShopActivity.start(context, coupon.storeId);
        } else if (coupon.couponType == 0){ // 产品类型优惠券
            ProductService.getProductInfoById(coupon.productId, new BaseCallback<Product>() {
                @Override
                public void callback(Product product) {
                    if (product.skus.isEmpty()) {
                        compileNative(context, "custom?pageId=" + coupon.storeId, false, "");
                    } else {
                        SkuPvIds skuPvIds = product.skus.get(0);
                        viewProductDetail(context, skuPvIds.skuId, false);
                    }
                }
            });
        } else if (coupon.couponType == 2){ // 全场优惠券，跳转大首页
            viewHome(context);
            context.finish();
        } else if (coupon.couponType == 3) { // 跨店铺优惠券
            CouponShopActivity.start(context, coupon.getTheCouponId());
        }
    }

    public static void startActivity(Context context, Intent intent, boolean isNewTask) {
        if (isNewTask) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }
}
