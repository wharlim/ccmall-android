package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class LiveReportImageUploadItem {
    @SerializedName("imgUrl")
    public String imgUrl;
}
