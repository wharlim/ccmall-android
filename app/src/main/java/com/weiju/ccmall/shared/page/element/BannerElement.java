package com.weiju.ccmall.shared.page.element;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.blankj.utilcode.utils.ScreenUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.shared.page.bean.BasicData;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.util.CarshReportUtils;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;

public class BannerElement extends SimpleDraweeView {

    public BannerElement(Context context, Element element) {
        super(context);
        try {
            final BasicData data = ConvertUtil.json2object(element.data);
            LinearLayout.LayoutParams layoutParams;
            int imageHeight = ConvertUtil.convertHeight(getContext(), 750, element.height);
            if (element.height != 0) {
                layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, imageHeight);
            } else {
                imageHeight = 0;
                layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            }
            setLayoutParams(layoutParams);
            setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventUtil.compileEvent(getContext(), data.event, data.target, false);
                }
            });
            FrescoUtil.setImage(this, data.image, ScreenUtils.getScreenWidth(), imageHeight);
        } catch (Exception e) {
            CarshReportUtils.post(e);
        }
    }
}
