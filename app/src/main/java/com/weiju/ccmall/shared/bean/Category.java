package com.weiju.ccmall.shared.bean;

import com.bigkoo.pickerview.model.IPickerViewData;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Category implements IPickerViewData {
    @SerializedName("categoryId")
    public String id;
    @SerializedName("parentId")
    public String parentId;
    @SerializedName("iconUrl")
    public String icon;
    @SerializedName("categoryName")
    public String name;
    @SerializedName("remark")
    public String remark;
    @SerializedName("createDate")
    public Date createDate;
    public boolean isSelected = Boolean.FALSE;

    @Override
    public String getPickerViewText() {
        return name;
    }
}
