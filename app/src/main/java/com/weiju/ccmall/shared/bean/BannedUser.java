package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @date 2020/2/21.
 */
public class BannedUser {

    /**
     * memberId : 7e93eee22e67ee7dd2f17dc68f29be2e
     * nickName : 135****8784
     * headImage :
     * bannedMessageDate : 2020-02-20 18:08:04
     */

    @SerializedName("memberId")
    public String memberId;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("bannedMessageDate")
    public String bannedMessageDate;
}
