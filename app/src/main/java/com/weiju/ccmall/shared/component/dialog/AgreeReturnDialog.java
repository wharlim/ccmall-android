package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Address;
import com.weiju.ccmall.shared.util.CommonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AgreeReturnDialog extends Dialog {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvPhone)
    TextView tvPhone;
    @BindView(R.id.tvContacts)
    TextView tvContacts;
    @BindView(R.id.tvAddressDetail)
    TextView tvAddressDetail;
    @BindView(R.id.ivDefault)
    ImageView ivDefault;
    private OnDialogClickListener mListener;
    private Address mAddress;

    public AgreeReturnDialog(Context context) {
        super(context, R.style.Theme_Light_Dialog);
//        mAddress = address;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_agree_return);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.CENTER);
        setCanceledOnTouchOutside(false);
//        initView();
    }

    private void initView() {
        ivDefault.setVisibility(mAddress.isDefault ? View.VISIBLE : View.GONE);
        tvContacts.setText(mAddress.contacts);
        tvPhone.setText(mAddress.phone);
        tvAddressDetail.setText(mAddress.getFullAddress());
    }

    @OnClick(R.id.tvConfirm)
    protected void onConfirm() {
        if (mListener != null) {
            mListener.onConfirm(mAddress.addressId);
        }
        dismiss();
    }

    public void setData(Address data) {
        mAddress = data;
        initView();
    }

    @OnClick(R.id.tvCancel)
    protected void onClose() {
        dismiss();
    }

    @OnClick(R.id.tvChangeAddress)
    public void onChangeAddress() {
        if (mListener != null) {
            mListener.onChangeAddress(mAddress.addressId);
        }
    }

    public void setOnDialogClickListener(OnDialogClickListener listener) {
        mListener = listener;
    }

    public interface OnDialogClickListener {
        void onConfirm(String addressId);

        void onChangeAddress(String addressId);
    }

}
