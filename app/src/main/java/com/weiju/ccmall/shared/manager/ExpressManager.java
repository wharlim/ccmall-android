package com.weiju.ccmall.shared.manager;

import android.content.Context;

import com.weiju.ccmall.shared.bean.ExpressCompany;
import com.weiju.ccmall.shared.service.contract.IExpressService;

import java.util.List;

import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

public class ExpressManager {

    public static void checkExpress(Context context, String expressCode, String companyCode) {
        IExpressService service = ServiceManager.getInstance().createService(IExpressService.class);
        service.getExpressDetails(
                companyCode,
                expressCode,
                "1",
                System.currentTimeMillis() + ""
        );

        service.listExpressCompany("").flatMap(new Function<List<ExpressCompany>, ObservableSource<?>>() {
            @Override
            public ObservableSource<?> apply(List<ExpressCompany> expressCompanies) throws Exception {
                return null;
            }
        });
    }
}
