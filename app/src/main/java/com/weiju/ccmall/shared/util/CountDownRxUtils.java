package com.weiju.ccmall.shared.util;

import android.support.annotation.NonNull;
import android.widget.TextView;

import com.blankj.utilcode.utils.TimeUtils;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;

import org.greenrobot.eventbus.EventBus;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/6/8.
 */
public class CountDownRxUtils {

    /**
     * 给 textView 自动倒计时
     * text 需要显示倒计时的 textview
     *
     * @param textView  需要倒计时的 text
     * @param timeCount 倒计时总时间(单位 秒)
     * @param initStr   默认状态下显示的文本
     */
    public static void textViewCountDown(final TextView textView, final int timeCount, final String initStr) {
        Observable
                .interval(0, 1, TimeUnit.SECONDS)
                .take(timeCount)
                .map(new Function<Long, Long>() {
                    @Override
                    public Long apply(@NonNull Long aLong) throws Exception {
                        return timeCount - aLong;

                    }
                })
                .doOnSubscribe(new Consumer<Disposable>() {
                    @Override
                    public void accept(@NonNull Disposable disposable) throws Exception {
                        textView.setEnabled(false);
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Long>() {

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Long aLong) {
                        textView.setText(aLong + "秒");
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        textView.setEnabled(true);
                        textView.setText(initStr);
                    }
                });
    }

    /**
     * @param textView
     * @param timeCount 倒计时总时间，单位秒
     * @param format    显示格式（"还差1人，剩余%s"）
     * @param pattern   剩余时间转换格式（"HH:mm:ss"）
     */
    public static void textViewCountDown(final TextView textView, final int timeCount,
                                         final String format, final String pattern,final String completeStr) {
        Observable
                .interval(0, 1, TimeUnit.SECONDS)
                .take(timeCount)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<Long, Long>() {
                    @Override
                    public Long apply(@NonNull Long aLong) throws Exception {
                        return timeCount - aLong;

                    }
                })
                .subscribe(new Observer<Long>() {

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Long aLong) {
//                        SimpleDateFormat sd = new SimpleDateFormat(pattern, Locale.getDefault());
////                        sd.setTimeZone(TimeZone.getTimeZone("GMT+0"));
//                        String time = sd.format(new Date(aLong * 1000));
                        String time = String.format(Locale.getDefault(), "%02d", aLong / 3600 / 24) + "天"
                                + String.format(Locale.getDefault(), "%02d", aLong / 3600 % 24) + " : "
                                + String.format(Locale.getDefault(), "%02d", aLong / 60 % 60) + " : "
                                + String.format(Locale.getDefault(), "%02d", aLong % 60);
                        textView.setText(String.format(format, time));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        textView.setText(completeStr);
                    }
                });
    }

    /**
     * 显示倒计时 时分秒
     * @param textView  需要倒计时的 text
     * @param expiresDate 结束时间
     * @param format 显示格式
     * @param pattern 剩余时间转换格式
     */
    public static void textViewCountDown(final TextView textView, final String expiresDate , final String format,final String pattern) {
        final long timeCount = ((TimeUtils.string2Millis(expiresDate)) - System.currentTimeMillis()) / 1000;
        Observable
                .interval(0, 1, TimeUnit.SECONDS)
                .take(timeCount)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<Long, Long>() {
                    @Override
                    public Long apply(@NonNull Long aLong) throws Exception {
                        return timeCount - aLong;

                    }
                })
                .subscribe(new Observer<Long>() {

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Long aLong) {
                        long downTime = ((TimeUtils.string2Millis(expiresDate)) - System.currentTimeMillis()) / 1000;
                        long hours = downTime / 3600 ;
                        long minutes = downTime / 60 % 60;
                        long seconds = downTime % 60;
                        String time = String.format(pattern,hours<10?"0"+hours:hours,minutes<10?"0"+minutes:minutes,
                                seconds<10?"0"+seconds:seconds);

                        textView.setText(String.format(format, time));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        textView.setText("开始挑战");
                        EventBus.getDefault().post((new EventMessage(Event.countDownComplete)));
                    }
                });
    }


}
