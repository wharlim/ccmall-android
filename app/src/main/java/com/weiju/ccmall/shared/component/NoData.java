package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.component
 * @since 2017-06-07
 */
public class NoData extends LinearLayout {

    private ImageView mImageView;
    private TextView mTextView;

    public NoData(Context context) {
        super(context);
        initView();
    }

    public NoData(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public NoData(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }


    private void initView() {
        inflate(getContext(), R.layout.cmp_no_data, this);
        mImageView = (ImageView) findViewById(R.id.noDataIcon);
        mTextView = (TextView) findViewById(R.id.noDataLabel);
    }

    public NoData setImgRes(int resId) {
        mImageView.setImageResource(resId);
        return this;
    }

    public NoData setTextView(String str) {
        mTextView.setText(str);
        return this;
    }

}
