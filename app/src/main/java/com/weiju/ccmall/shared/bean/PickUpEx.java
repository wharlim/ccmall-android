package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/8/5 on 9:51
 * @desc ${TODD}
 */
public class PickUpEx {

    /**
     * 单张提货券的金额
     */
    @SerializedName("cost")
    public long cost;
    /**
     * 可用提货券的总数量（与totalRecord一致）
     */
    @SerializedName("num")
    public int num;

    /**
     * 提货券总的使用金额
     */
    @SerializedName("totalCost")
    public long totalCost;

}
