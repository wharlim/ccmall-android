package com.weiju.ccmall.shared.page.element;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.component.TagTextView;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.page.bean.BasicData;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.page.bean.ProductData;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/8/17.
 */
public class ProductGroupElement extends LinearLayout {

    private SimpleDraweeView mIvProduct;
    private TagTextView mTvTitle;
    private TextView mTvPrice;
    private TextView mTvSales;
    private TextView mTvCCM;
    private final IProductService mService;
    private int mImgHeight;
    private View mIvBanjia;

    public ProductGroupElement(Context context, Element element) {
        super(context);
        mService = ServiceManager.getInstance().createService(IProductService.class);
        initView(element);
        initData(element);
    }

    private void initData(Element element) {
        final ProductData productData = ConvertUtil.json2Product(element.data);
        final SkuInfo skuInfo = productData.sku;
        FrescoUtil.setImgMask(mIvProduct, productData.image, skuInfo.status, skuInfo.stock);
        mTvTitle.setText(skuInfo.name);
        mTvTitle.setTags(skuInfo.tags);
        mTvSales.setText(String.format("销量：%s 件", skuInfo.sales));
        mTvCCM.setText(String.format("奖%s%%算率", skuInfo.countRateExc));
        mTvPrice.setText(MoneyUtil.centToYuan¥StrNoZero(skuInfo.groupSkuInfo.groupPrice));
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                goSkuDetails(skuInfo.skuId);
            }
        });
        mIvBanjia.setVisibility(View.GONE);
//        mIvBanjia.setVisibility(skuInfo.isBanjia() ? View.VISIBLE : View.GONE);
    }

    private void initView(final Element element) {
        View view = inflate(getContext(), R.layout.el_product_group_layout, this);
        mIvProduct = (SimpleDraweeView) view.findViewById(R.id.ivProduct);
        element.height = element.height == 0 ? 330 : element.height;
        mImgHeight = ConvertUtil.convertHeight(getContext(), 750, element.height);
        mIvProduct.getLayoutParams().height = mImgHeight;
        mTvTitle = (TagTextView) view.findViewById(R.id.tvTitle);
        mTvPrice = (TextView) view.findViewById(R.id.itemPriceTv);
        mTvSales = (TextView) view.findViewById(R.id.itemSalesTv);
        mTvCCM = (TextView) view.findViewById(R.id.tvCCM);
        view.findViewById(R.id.tvGoGroup).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final BasicData basicData = ConvertUtil.json2object(element.data);
                goSkuDetails(basicData.skuId);
            }
        });
        mIvBanjia = view.findViewById(R.id.ivBanjia);
    }

    public void goSkuDetails(String skuId) {
        Intent intent = new Intent(getContext(), NewProductDetailActivity.class);
        intent.putExtra(Key.SKU_ID, skuId);
        getContext().startActivity(intent);
    }
}
