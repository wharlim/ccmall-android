package com.weiju.ccmall.shared.util;

import android.content.Context;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.shared.component.NoData;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/7/19.
 */
public class RvUtils {

    public static void configRecycleView(Context context, final RecyclerView recyclerView) {
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    public static void configRecycleView(Context context, final RecyclerView recyclerView, BaseQuickAdapter adapter) {
        configRecycleView(context, recyclerView);
        adapter.setEmptyView(new NoData(context));
        recyclerView.setAdapter(adapter);
    }

}
