package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/11/5 on 16:43
 * @desc ${TODD}
 */
public class OrderShipDialog extends Dialog {
    @BindView(R.id.etExpressName)
    protected EditText mEtExpressName;
    @BindView(R.id.etExpressCode)
    protected EditText mEtExpressCode;

    protected OrderShipDialog.OnConfirmListener mConfirmListener;

    public OrderShipDialog(Context context) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_order_ship);
        ButterKnife.bind(this);
    }

    public void setOnConfirmListener(@NonNull OrderShipDialog.OnConfirmListener listener) {
        mConfirmListener = listener;
    }

    public void setExpressCode(String code) {
        mEtExpressCode.setText(code);
    }

    @OnClick(R.id.cancelBtn)
    protected void onClose(View view) {
        dismiss();
    }

    @OnClick(R.id.ivScan)
    protected void scan() {
        if (mConfirmListener != null) {
            mConfirmListener.sacn();
        }
    }

    @OnClick(R.id.confirmBtn)
    protected void onConfirm(View view) {
        String name = mEtExpressName.getText().toString();
        String code = mEtExpressCode.getText().toString();
        if (TextUtils.isEmpty(name)) {
            ToastUtil.error("请输入物流公司");
            return;
        }
        if (TextUtils.isEmpty(code)) {
            ToastUtil.error("请输入物流单号");
            return;
        }
        if (mConfirmListener != null) {
            mConfirmListener.confirm(name, code);
        }
        dismiss();
    }


    public interface OnConfirmListener {
        void confirm(String expressName, String expressCode);

        void sacn();
    }
}
