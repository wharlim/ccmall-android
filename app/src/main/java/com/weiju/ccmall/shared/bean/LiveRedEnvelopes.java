package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2020/1/7 on 17:02
 * @desc
 */
public class LiveRedEnvelopes {

    /**
     * receiveId : 0611c6ffa81d4993b0743e2c781a6baa
     * redEnvelopesId : b8a34dfdf3274f0eb3e202a4d5f6c1ca
     * memberId : 27f32b57d3014afbbf4e4275f86c13a9
     * headImage :
     * nickName : 新用户
     * liveId : cf75431b0f354f259adb603cbbdc2206
     * receiveMoney : 99
     * balanceCode : 1691578608765418
     * createDate : 2020-01-07 15:16:45
     * updateDate : 2020-01-07 15:16:45
     * deleteFlag : 0
     */

    @SerializedName("receiveId")
    public String receiveId;
    @SerializedName("redEnvelopesId")
    public String redEnvelopesId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("liveId")
    public String liveId;
    @SerializedName("receiveMoney")
    public int receiveMoney;
    @SerializedName("balanceCode")
    public String balanceCode;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;

    /**
     * title : 直播间红包
     * sendNum : 10
     * sendMoney : 1000
     * remark :
     * status : 0
     * receiveStatus : 0
     * receiveNum : 0
     */

    @SerializedName("title")
    public String title;
    @SerializedName("sendNum")
    public int sendNum;
    @SerializedName("sendMoney")
    public int sendMoney;
    @SerializedName("remark")
    public String remark;
    @SerializedName("status")
    public int status;
    @SerializedName("receiveStatus")
    public int receiveStatus;
    @SerializedName("receiveNum")
    public int receiveNum;

}
