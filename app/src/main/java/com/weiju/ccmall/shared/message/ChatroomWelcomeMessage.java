package com.weiju.ccmall.shared.message;

import android.annotation.SuppressLint;
import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

/**
 * @author chenyanming
 * @time 2020/1/7 on 15:16
 * @desc
 */
@SuppressLint("ParcelCreator")
@MessageTag(value = "CCM:Chatroom:Welcome", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class ChatroomWelcomeMessage extends MessageContent {
    public ChatroomWelcomeMessage() {
    }

    //名字
    private String nickName;
    //头像
    private String headImage;
    //观众人数
    private int audienceNum;


    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("nickName", nickName);
            jsonObj.put("headImage", headImage);
            jsonObj.put("audienceNum", audienceNum);

        } catch (JSONException e) {
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ChatroomWelcomeMessage(byte[] data) {
        String jsonStr = null;

        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e1) {

        }

        try {
            JSONObject jsonObj = new JSONObject(jsonStr);

            if (jsonObj.has("nickName")) {
                nickName = jsonObj.optString("nickName");
                setNickName(nickName);
            }

            if (jsonObj.has("headImage")) {
                headImage = jsonObj.optString("headImage");
                setHeadImage(headImage);
            }

            if (jsonObj.has("audienceNum")) {
                audienceNum = jsonObj.optInt("audienceNum");
                setAudienceNum(audienceNum);
            }
        } catch (JSONException e) {
        }

    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }


    public void setHeadImage(String headImage) {
        this.headImage = headImage;
    }

    public void setAudienceNum(int audienceNum) {
        this.audienceNum = audienceNum;
    }

    public int getAudienceNum() {
        return audienceNum;
    }

    public String getNickName() {
        return nickName;
    }

    public String getHeadImage() {
        return headImage;
    }

    //给消息赋值。
    public ChatroomWelcomeMessage(Parcel in) {
        nickName = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        headImage = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        audienceNum = ParcelUtils.readIntFromParcel(in);//该类为工具类，消息属性
        //这里可继续增加你消息的属性
    }

    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<ChatroomWelcomeMessage> CREATOR = new Creator<ChatroomWelcomeMessage>() {

        @Override
        public ChatroomWelcomeMessage createFromParcel(Parcel source) {
            return new ChatroomWelcomeMessage(source);
        }

        @Override
        public ChatroomWelcomeMessage[] newArray(int size) {
            return new ChatroomWelcomeMessage[size];
        }
    };

    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     *
     * @return 一个标志位，表明Parcelable对象特殊对象类型集合的排列。
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * 将类的数据写入外部提供的 Parcel 中。
     *
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, nickName);//该类为工具类，对消息中属性进行序列化
        ParcelUtils.writeToParcel(dest, headImage);//该类为工具类，对消息中属性进行序列化
        ParcelUtils.writeToParcel(dest, audienceNum);//该类为工具类，对消息中属性进行序列化
        //这里可继续增加你消息的属性
    }
}
