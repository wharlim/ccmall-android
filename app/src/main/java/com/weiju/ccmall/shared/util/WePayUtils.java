package com.weiju.ccmall.shared.util;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.blankj.utilcode.utils.StringUtils;
import com.tencent.mm.opensdk.modelbiz.WXLaunchMiniProgram;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.module.page.PayWebFragment;
import com.weiju.ccmall.module.pay.PayResultActivity;
import com.weiju.ccmall.module.world.IWorldService;
import com.weiju.ccmall.module.world.activity.WorldOrderDetailActivity;
import com.weiju.ccmall.module.world.entity.CreateWxPayEntity;
import com.weiju.ccmall.shared.basic.AgentFActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.YiBaoPay;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IPayService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * Created by zjm on 2018/3/30.
 */
public class WePayUtils {

    private static final String WX_NOTIFY_URL = BaseUrl.getInstance().getBaseUrl() + "wxnotify/wxapppay";
    private static IWXAPI mWxapi;


    public static IWXAPI initWePay(final Activity context) {
        mWxapi = WXAPIFactory.createWXAPI(context, BuildConfig.WX_APP_ID, true);
        mWxapi.registerApp(BuildConfig.WX_APP_ID);
        return mWxapi;
    }


    public static byte[] callMapToXML(Map map) {
        StringBuffer sb = new StringBuffer();
        sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><bizdata>");
        mapToXMLTest2(map, sb);
        sb.append("</bizdata>");
        try {
            return sb.toString().getBytes("UTF-8");
        } catch (Exception e) {
        }
        return null;
    }

    private static void mapToXMLTest2(Map map, StringBuffer sb) {
        Set set = map.keySet();
        for (Iterator it = set.iterator(); it.hasNext(); ) {
            String key = (String) it.next();
            Object value = map.get(key);
            if (null == value) {
                value = "";
            }
            if (value.getClass().getName().equals("java.util.ArrayList")) {
                ArrayList list = (ArrayList) map.get(key);
                sb.append("<" + key + ">");
                for (int i = 0; i < list.size(); i++) {
                    HashMap hm = (HashMap) list.get(i);
                    mapToXMLTest2(hm, sb);
                }
                sb.append("</" + key + ">");

            } else {
                if (value instanceof HashMap) {
                    sb.append("<" + key + ">");
                    mapToXMLTest2((HashMap) value, sb);
                    sb.append("</" + key + ">");
                } else {
                    sb.append("<" + key + ">" + value + "</" + key + ">");
                }

            }

        }
    }


    public static void payByWeb(final Activity activity, final Order order, final int payType) {
        if (payType == AppTypes.PAY_TYPE.WECHAT) {
            IWXAPI iwxapi = WePayUtils.initWePay(activity);
            if (!iwxapi.isWXAppInstalled()) {
                com.sobot.chat.utils.ToastUtil.showToast(activity, "尚未安装微信");
                return ;
            }
        }
        IPayService service = ServiceManager.getInstance().createService(IPayService.class);
        APIManager.startRequest(
                service.getHiCardPayYiBo(
                        order.orderMain.orderCode,
                        payType + "",
                        null,
                        StringUtil.md5("a70c34cc321f407d990c7a2aa7900729" + order.orderMain.orderCode)
                ),
                new BaseRequestListener<YiBaoPay>(activity) {
                    @Override
                    public void onSuccess(YiBaoPay result) {
                        if (payType == AppTypes.PAY_TYPE.WECHAT) {
                            yibaoWechatPay(activity, result);
                        }
//                        else if(payType == AppTypes.PAY_TYPE.ALI_WEB){
//                            yibaoAliPay(activity,result);
//                        }
                        else {
                            String qrURL = result.payUrl;
                            if (webviewStartUrl(activity, qrURL, order)) {
//                            goPayResultActivity(activity, order);
                            } else {
                                activity.finish();
                                EventUtil.viewOrderDetail(activity, order.orderMain.orderCode, false);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        activity.finish();
                        EventUtil.viewOrderDetail(activity, order.orderMain.orderCode, false);
                    }
                },activity);
    }

    private static void yibaoWechatPay(Activity activity, YiBaoPay result) {
        IWXAPI iwxapi = WePayUtils.initWePay(activity);

        YiBaoPay.PayConfigEntity payConfig = result.payConfig;
        PayReq request = new PayReq();
        request.appId = payConfig.appid;
        request.partnerId = payConfig.partnerid;
        request.prepayId = payConfig.prepayid;
        request.packageValue = payConfig.packagevalue;
        request.nonceStr = payConfig.noncestr;
        request.timeStamp = payConfig.timestamp;
        request.sign = payConfig.sign;
        iwxapi.sendReq(request);
    }

    public static void payByWeChatMini(final Activity activity, final String merchantOrderNo) {
        IWXAPI iwxapi = WePayUtils.initWePay(activity);
        if (!iwxapi.isWXAppInstalled()) {
            com.sobot.chat.utils.ToastUtil.showToast(activity, "尚未安装微信");
            return;
        }
        IWorldService service = ServiceManager.getInstance().createService(IWorldService.class);
        APIManager.startRequest(
                service.createWebhookPay(
                        merchantOrderNo,
                        StringUtil.md5("a70c34cc321f407d990c7a2aa7900729" + merchantOrderNo)
                ),
                new BaseRequestListener<CreateWxPayEntity>(activity) {
                    @Override
                    public void onSuccess(CreateWxPayEntity result) {
                        WeChatMiniPay(activity, result);
                    }

                    @Override
                    public void onError(Throwable e) {
                        super.onError(e);
                        activity.finish();
                        WorldOrderDetailActivity.start(activity, merchantOrderNo);
                    }
                }, activity);
    }

    private static void WeChatMiniPay(Activity activity, CreateWxPayEntity result) {
        IWXAPI api = WePayUtils.initWePay(activity);
        WXLaunchMiniProgram.Req req = new WXLaunchMiniProgram.Req();
        req.userName = result.sourid; // 填小程序原始id
        //拉起小程序页面的可带参路径，不填默认拉起小程序首页，对于小游戏，可以只传入 query 部分，来实现传参效果，如：传入 "?foo=bar"。
        req.path = result.url + "?shopKey=" + result.shopKey + "&request=" + result.request +
                "&sign=" + result.sign + "&debug=" + result.debug;//传0则为正式环境，传1代表测试环境
        req.miniprogramType = WXLaunchMiniProgram.Req.MINIPTOGRAM_TYPE_RELEASE;// 可选打开 开发版，体验版和正式版
        api.sendReq(req);
    }

    public static boolean webviewStartUrl(Activity context, String url, Order order) {
        if (StringUtils.isEmpty(url)) {
            ToastUtil.error("URL数据异常");
            return false;
        }
        goPayResultActivity(context, order.orderMain.orderCode);
        Bundle b = new Bundle();
        b.putString("url", url);
        b.putString("orderCode", order.orderMain.orderCode);
        AgentFActivity.start(context, PayWebFragment.class, b);
//        Intent intent = new Intent();
//        intent.setAction("android.intent.action.VIEW");
//        Uri content_url = Uri.parse(url);
//        intent.setData(content_url);
//        context.startActivity(intent);
        context.finish();
        return true;
    }

    public static void goPayResultActivity(final Activity context, String orderCode) {
        Intent intent = new Intent(context, PayResultActivity.class);
        intent.putExtra("orderCode", orderCode);
        context.startActivity(intent);
    }

}
