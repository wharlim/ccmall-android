package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.newRetail.bean.CheckTimeOutBean;
import com.weiju.ccmall.shared.bean.CommentCount;
import com.weiju.ccmall.shared.bean.GetOrderStatusCount;
import com.weiju.ccmall.shared.bean.MyCommentModel;
import com.weiju.ccmall.shared.bean.Order;
import com.weiju.ccmall.shared.bean.OrderComment;
import com.weiju.ccmall.shared.bean.OrderCount;
import com.weiju.ccmall.shared.bean.OrderProfit;
import com.weiju.ccmall.shared.bean.OrderResponse;
import com.weiju.ccmall.shared.bean.OrderVouchers;
import com.weiju.ccmall.shared.bean.RefundBody;
import com.weiju.ccmall.shared.bean.RefundType;
import com.weiju.ccmall.shared.bean.RefundsOrder;
import com.weiju.ccmall.shared.bean.SkuAmount;
import com.weiju.ccmall.shared.bean.Store;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.body.ReceiveRefundGoodsBody;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface IOrderService {

    @GET("order/getOrderStatusCount")
    Observable<RequestResult<OrderCount>> getOrderCount();

    @GET("order/getOrderStatusCount")
    Observable<RequestResult<OrderCount>> getOrderCount(@Query("type") String type);

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST()
    Observable<RequestResult<OrderResponse>> create(
            @Url String url,
            @Body RequestBody requestBody
    );

    @GET("order/getOrderByOrderCode")
    Observable<RequestResult<Order>> getOrderByCode(@Query("orderCode") String orderCode, @Query("userType") String userType);

    @GET("liveStore/orderDetail")
    Observable<RequestResult<Order>> orderDetail(@Query("orderCode") String orderCode);

    @GET("refundOrder/getOrderRefundList")
    Observable<RequestResult<List<RefundsOrder>>> getOrderRefundList(
            @Query("orderCode") String orderCode
    );

    @GET("refundOrder/getRefundList")
    Observable<RequestResult<PaginationEntity<RefundsOrder, Object>>> getOrderRefundList(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize
    );

    @GET("refundOrder/getRefundList")
    Observable<RequestResult<PaginationEntity<RefundsOrder, Object>>> getOrderRefundList(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize, @Query("productType") int productType, @Query("type") String type
    );

    @GET("storeOrder/getStoreRefundList")
    Observable<RequestResult<PaginationEntity<RefundsOrder, Object>>> getStoreOrderRefundList(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize,
            @Query("type") String type,
            @Query("refundStatus") String refundStatus
    );

    @GET("storeOrder/getStoreRefundList")
    Observable<RequestResult<PaginationEntity<RefundsOrder, Object>>> getStoreRefundList(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize,
            @Query("productType") int productType,
            @Query("refundType") String refundType,
            @Query("refundStatus") String refundStatus
    );

    @GET("storeOrder/getRefundType")
    Observable<RequestResult<List<RefundType>>> getRefundType(@Query("type") String type);

    @GET("order/getAllOrderList")
    Observable<RequestResult<PaginationEntity<Order, Object>>> getAllOrderList(
            @Query("pageOffset") int page, @Query("pageSize") String pageSize, @Query("type") String type
    );

    @GET("order/getOrderList")
    Observable<RequestResult<PaginationEntity<Order, Object>>> getOrderListByStatus(@Query("orderStatus") int status, @Query("pageOffset") int page, @Query("type") String type);

    @GET("channel-orders")
    Observable<RequestResult<PaginationEntity<Order, Object>>> getGiftOrderListByStatus(@Query("status") Integer status, @Query("pageOffset") int page, @Query("pageSize") int pageSize);

    @GET("liveStore/orderList")
    Observable<RequestResult<PaginationEntity<Order, Object>>> orderList(@Query("orderStatus") Integer status, @Query("pageOffset") int page, @Query("pageSize") int pageSize);


    @GET("order/getOrderWaitCommentList")
    Observable<RequestResult<PaginationEntity<OrderComment, Object>>> getOrderWaitCommentList(@Query("pageOffset") int page);

    @GET("order/getOrderWaitCommentList")
    Observable<RequestResult<PaginationEntity<OrderComment, Object>>> getOrderWaitCommentList(@Query("pageOffset") int page, @Query("productType") String productType);

    @FormUrlEncoded
    @POST("order/paymentOrder")
    Observable<RequestResult<Object>> payBalance(@Field("orderCode") String orderCode,
                                                 @Field("password") String password,
                                                 @Field("checkNumber") String captcha,
                                                 @Field("payType") int payType);

    @FormUrlEncoded
    @POST("order/waitPayOrderCheck")
    Observable<RequestResult<Object>> checkOrderToPay(@Field("orderCode") String orderCode);

    @FormUrlEncoded
    @POST("order/cancel")
    Observable<RequestResult<Object>> cancelOrder(@Field("orderCode") String orderCode);

    @FormUrlEncoded
    @POST("order/received")
    Observable<RequestResult<Object>> receiveOrder(@Field("orderCode") String orderCode);

    @POST("storeOrder/receiveRefundGoodsExt")
    Observable<RequestResult<Object>> receiveOrder(
            @Body RefundBody body
    );


    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("refundOrder/apply")
    Observable<RequestResult<Object>> refundOrder(@Body RequestBody body);

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("refundOrder/applyExt")
    Observable<RequestResult<Object>> refundOrderExt(@Body RequestBody body);

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("refundOrder/applyExtEdit")
    Observable<RequestResult<Object>> refundOrderExtEdit(
            @Body RequestBody body
    );

    @FormUrlEncoded
    @POST("refundOrder/saveRefundExpress")
    Observable<RequestResult<Object>> refundExpress(@Field("refundId") String refundId, @Field("expressName") String expressName, @Field("expressCode") String expressCode);

    @FormUrlEncoded
    @POST("refundOrder/cancel")
    Observable<RequestResult<Object>> refundCancel(
            @Field("orderCode") String orderCode,
            @Field("remark") String remark
    );

    /**
     * 取消退款退货申请单
     *
     * @param orderCode
     * @param remark
     * @return
     */
    @FormUrlEncoded
    @POST("refundOrder/cancelExt")
    Observable<RequestResult<Object>> refundCancelExt(
            @Field("refundId") String refundId,
            @Field("remark") String remark
    );

    @GET("storeOrder/getOrderStatusCount")
    Observable<RequestResult<GetOrderStatusCount>> getOrderStatusCount(@Query("type") String type);

    @GET("storeOrder/getOrderList")
    Observable<RequestResult<PaginationEntity<Order, Object>>> getStoreOrderList(
            @Query("orderStatus") int status,
            @Query("pageOffset") int page, @Query("type") String type
    );

    @GET("storeOrder/getOrderList")
    Observable<RequestResult<PaginationEntity<Order, Object>>> getStoreOrderList(
            @Query("orderStatus") int status,
            @Query("pageOffset") int page, @Query("type") String type, @Query("orderCode") String orderCode
    );

    @GET("liveStore/orderSearch")
    Observable<RequestResult<PaginationEntity<Order, Object>>> orderSearch(
            @Query("pageOffset") int page, @Query("orderCode") String orderCode
    );

    @POST("storeOrder/shipOrder")
    @Headers("Content-Type: application/json;charset=UTF-8")
    Observable<RequestResult<Object>> shipOrder(
            @Body RequestBody body
    );

    @FormUrlEncoded
    @POST("liveStore/ship")
    Observable<RequestResult<Object>> ship(
            @FieldMap HashMap<String, Object> params
    );

    @POST("storeOrder/agreeRefundMoneyExt")
    Observable<RequestResult<Object>> refundMoney(
            @Body RefundBody body
    );

    @POST("storeOrder/agreeRefundMoney")
    Observable<RequestResult<Object>> agreeRefundMoney(@Body RefundBody body);

    @POST("storeOrder/agreeRefundGoodsExt")
    Observable<RequestResult<Object>> refundGoods(
            @Body RefundBody body
    );

    @POST("storeOrder/agreeRefundGoods")
    Observable<RequestResult<Object>> agreeRefundGoods(@Body RequestBody Body);

    @POST("storeOrder/rejectRefundGoodsExt")
    Observable<RequestResult<Object>> refuseRefundGoods(
            @Body RefundBody body
    );

    @POST("storeOrder/rejectRefundMoneyExt")
    Observable<RequestResult<Object>> refuseRefundMoney(
            @Body RefundBody body
    );

    @POST("storeOrder/receiveRefundGoods")
    Observable<RequestResult<Object>> receiveRefundGoods(
            @Body ReceiveRefundGoodsBody body
    );

    @GET("refundOrder/getRefundDetail")
    Observable<RequestResult<RefundsOrder>> getRefundDetail(
            @Query("refundId") String refundId, @Query("memberId") String memberId
    );

    @GET("storeOrder/getStoreRefundDetail")
    Observable<RequestResult<RefundsOrder>> getSellerRefundDetail(
            @Query("refundId") String refundId,
            @Query("memberId") String memberId
    );

    @GET
    Observable<RequestResult<PaginationEntity<MyCommentModel, Object>>> getOrderCommentList(
            @Url String url,
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize,
            @Query("memberId") String memberId
    );

    @GET
    Observable<RequestResult<PaginationEntity<MyCommentModel, Object>>> getOrderCommentList(
            @Url String url,
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize,
            @Query("memberId") String memberId, @Query("productType") String productType
    );

    @GET("order/getOrderCommentCount")
    Observable<RequestResult<CommentCount>> getOrderCommentCount();

    @GET("order/getOrderCommentCount")
    Observable<RequestResult<CommentCount>> getOrderCommentCount(@Query("productType") String productType);

    @GET("pay/checkOrderPayStatus")
    Observable<RequestResult<Object>> checkOrderPayStatus(
            @Query("orderCode") String orderCode
    );

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("vouchers/calcOrderVouchersList")
    Observable<RequestResult<OrderVouchers>> calcOrderVouchersList(
            @Body ArrayList<SkuAmount> body
    );

    @POST("order/comfirmPay")
    Observable<RequestResult<Object>> comfirmPay(@Query("orderId") String orderId, @Query("memberId") String memberId);

    @GET("order/getSaleOrderList")
    Observable<RequestResult<PaginationEntity<Order, Object>>> getSaleOrderList(@Query("status") String status,
                                                                                @Query("pageOffset") int page, @Query("type") String type);

    @GET("storeOrder/getStoreGrade")
    Observable<RequestResult<Store>> getStoreGrade();

    @POST("order/checkTimeOut")
    Observable<RequestResult<CheckTimeOutBean>> checkTimeOut(@Query("orderCode") String orderCode);

    @POST("order/giveUpRefund")
    Observable<RequestResult<Object>> giveUpRefund(@Query("orderCode") String orderCode);

    @GET("order/getOrderProfit")
    Observable<RequestResult<OrderProfit>> getOrderProfit(@Query("orderCode") String orderCode);

    @FormUrlEncoded
    @POST("channel-orders/dispatch")
    Observable<RequestResult<Object>> dispatch(@Field("orderCode") String orderCode,
                                               @Field("expressName") String expressName, @Field("expressCode") String expressCode);

}
