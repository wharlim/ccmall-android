package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.bean
 * @since 2017-07-10
 */
public class ProductAuth extends BaseModel {

    @SerializedName("iconUrl")
    public String icon;
    @SerializedName("title")
    public String title;
    @SerializedName("content")
    public String content;
}
