package com.weiju.ccmall.shared.page.element;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.decoration.SpacesItemHorizontalDecoration;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.util.CarshReportUtils;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ViewConstructor")
public class SwiperElement extends LinearLayout {

    private TextView mMoreBtn;

    public SwiperElement(final Context context, final Element element) {
        super(context);
        View view = inflate(getContext(), R.layout.el_swiper_layout, this);
        view.setBackgroundColor(Color.WHITE);
        element.setBackgroundInto(view);

        mMoreBtn = (TextView) view.findViewById(R.id.eleMoreTv);
        if (element.hasMore && !StringUtils.isEmpty(element.title)) {
            mMoreBtn.setVisibility(VISIBLE);
            mMoreBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        EventUtil.compileEvent(context, element.moreLink.event, element.moreLink.target, false);
                    } catch (Exception e) {
                        CarshReportUtils.post(e);
                    }
                }
            });
        } else {
            mMoreBtn.setVisibility(GONE);
        }

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        if (!StringUtils.isEmpty(element.title)) {
            TextView tvTitle = (TextView) view.findViewById(R.id.eleTitleTv);
            tvTitle.setText(element.title);
        } else {
            LinearLayout layoutTop = (LinearLayout) view.findViewById(R.id.layoutTop);
            layoutTop.setVisibility(GONE);
            recyclerView.setPadding(0, SizeUtils.dp2px(15), 0, 0);
        }

        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        final SwiperAdapter swiperAdapter = new SwiperAdapter();
        recyclerView.setAdapter(swiperAdapter);
        recyclerView.addItemDecoration(new SpacesItemHorizontalDecoration(ConvertUtil.dip2px(10), true));

        ArrayList<SkuInfo> skus = ConvertUtil.json2SkuList(element.data);
        swiperAdapter.setItems(skus);
        swiperAdapter.notifyDataSetChanged();
    }

    private class SwiperAdapter extends RecyclerView.Adapter<ViewHolder> {

        private List<SkuInfo> items = new ArrayList<>();

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.el_swiper_item, parent, false));
        }

        @SuppressLint("DefaultLocale")
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final SkuInfo skuInfo = items.get(position);

            FrescoUtil.setSkuImgSmallMask(holder.mThumbIv, skuInfo);

            holder.mTitleTv.setText(skuInfo.name);
            holder.mPriceTv.setText(ConvertUtil.centToCurrency(getContext(), skuInfo.retailPrice));
            holder.mMarkPriceTv.setText(ConvertUtil.centToCurrency(getContext(), skuInfo.marketPrice));
            holder.mTvCCM.setText(String.format("奖%s%%算率", skuInfo.countRateExc));
            TextViewUtil.addThroughLine(holder.mMarkPriceTv);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EventUtil.viewProductDetail(getContext(), skuInfo.skuId, false);
                }
            });
            holder.mIvBanjia.setVisibility(skuInfo.isBanjia() ? VISIBLE : GONE);
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        void setItems(@NonNull List<SkuInfo> items) {
            this.items = items;
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.itemThumbIv)
        SimpleDraweeView mThumbIv;
        @BindView(R.id.itemTitleTv)
        TextView mTitleTv;
        @BindView(R.id.itemPriceTv)
        TextView mPriceTv;
        @BindView(R.id.itemMarkPriceTv)
        TextView mMarkPriceTv;
        @BindView(R.id.ivBanjia)
        ImageView mIvBanjia;
        @BindView(R.id.tvCCM)
        protected TextView mTvCCM;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
