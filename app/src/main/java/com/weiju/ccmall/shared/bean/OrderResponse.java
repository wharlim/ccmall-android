package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.bean
 * @since 2017-06-20
 */
public class OrderResponse {

    @SerializedName("orderCode")
    public String orderCode;

    @SerializedName("levelStr")
    public String levelStr;

    @SerializedName("upLevel")
    public int upLevel;

    @SerializedName("content")
    public String content; // CCM抵扣未授权提示内容
}
