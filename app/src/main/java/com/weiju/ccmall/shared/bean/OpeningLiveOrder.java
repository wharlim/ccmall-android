package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class OpeningLiveOrder {

    /**
     * orderId : 8f6cdfb171f543fd8c601546f55c7d32
     * orderCode : 0421585870357149
     */

    @SerializedName("orderId")
    public String orderId;
    @SerializedName("orderCode")
    public String orderCode;
}
