package com.weiju.ccmall.shared.basic;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.util.UiUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseListFragment extends BaseFragment implements OnRefreshListener {


    @BindView(R.id.recyclerView)
    public RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    public SwipeRefreshLayout mRefreshLayout;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getContentLayoutRes(), container, false);
        ButterKnife.bind(this, view);
        getIntentData();
        initView();
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    public int getContentLayoutRes() {
        return R.layout.activity_base_list2;
    }

    public void getIntentData() {

    }

    public void initView() {

        UiUtils.configRecycleView(mRecyclerView, new LinearLayoutManager(getContext()), false);
        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData(true);
            }
        });
        if (isShowListDivider()) {
            mRecyclerView.addItemDecoration(new ListDividerDecoration(getContext()));
        }
        mRecyclerView.setAdapter(getAdapter());
        configAdapter(getAdapter());

        if (getHeaderViews() != null) {
            getAdapter().setHeaderAndEmpty(true);
            for (int i = 0; i < getHeaderViews().size(); i++) {
                getAdapter().addHeaderView(getHeaderViews().get(i));
            }
        }

        mRecyclerView.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                onListItemClick(adapter, view, position);
            }
        });
        mRecyclerView.addOnItemTouchListener(new OnItemChildClickListener() {
            @Override
            public void onSimpleItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                onListItemClick(adapter, view, position);
            }
        });
    }

    public void initData() {
        getData(true);
    }

    /**
     * 对 adapter 进行一些默认配置
     * 如果不需要，就重写这个方法
     *
     * @param adapter
     * @return
     */
    public BaseQuickAdapter configAdapter(BaseQuickAdapter adapter) {
        if (getEmptyView() != null) {
            adapter.setEmptyView(getEmptyView());
        }
        if (isLoadMore()) {
            adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
                @Override
                public void onLoadMoreRequested() {
                    getData(false);
                }
            }, mRecyclerView);
        }
        getAdapter().setHeaderAndEmpty(true);
        getAdapter().setFooterViewAsFlow(true);
        return adapter;
    }

    /**
     * @return 如果需要自定义空布局，请重写这个，并 return 自己的空布局
     */
    public View getEmptyView() {
        View inflate = View.inflate(getContext(), R.layout.cmp_no_data, null);
        return inflate;
    }

    /**
     * @return 是否显示分割线
     */
    public boolean isShowListDivider() {
        return false;
    }

    /**
     * @return 是否开启加载更多模式
     */
    public boolean isLoadMore() {
        return true;
    }

    /**
     * @return 返回你需要的列表头控件，系统会自动添加到 rv 上面
     */
    public ArrayList<View> getHeaderViews() {
        return null;
    }


    /**
     * @return 这个页面的标题
     */
//    public abstract String getTitleStr();

    /**
     * 列表点击事件
     *
     * @param adapter
     * @param view
     * @param position
     */
    public abstract void onListItemClick(BaseQuickAdapter adapter, View view, int position);

    /**
     * @return 列表的 adapter
     */
    public abstract BaseQuickAdapter getAdapter();

    /**
     * 加载列表数据
     *
     * @param isRefresh 是否是第一页
     */
    public abstract void getData(boolean isRefresh);

    @Override
    public void onRefresh(RefreshLayout refreshLayout) {
        getData(true);
    }
}
