package com.weiju.ccmall.shared.message;

import android.annotation.SuppressLint;
import android.os.Parcel;

import com.weiju.ccmall.shared.constant.AppTypes;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
@SuppressLint("ParcelCreator")
@MessageTag(value = "CCM:Chatroom:Coupon", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class ChatroomCouponMessage extends MessageContent {
    public ChatroomCouponMessage() {
    }

    //优惠券Id
    private String couponId;
    //发送Id
    private String sendId;

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("couponId", couponId);
            jsonObj.put("sendId", sendId);
        } catch (JSONException e) {
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ChatroomCouponMessage(byte[] data) {
        String jsonStr = null;

        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e1) {

        }

        try {
            JSONObject jsonObj = new JSONObject(jsonStr);

            if (jsonObj.has("couponId")) {
                couponId = jsonObj.optString("couponId");
                setCouponId(couponId);
            }

            if (jsonObj.has("sendId")) {
                sendId = jsonObj.optString("sendId");
                setSendId(sendId);
            }
        } catch (JSONException e) {
        }

    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setSendId(String sendId) {
        this.sendId = sendId;
    }

    public String getSendId() {
        return sendId;
    }

    //给消息赋值。
    public ChatroomCouponMessage(Parcel in) {
        couponId = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        sendId = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        //这里可继续增加你消息的属性
//        setRedId(redId);
    }

    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<ChatroomCouponMessage> CREATOR = new Creator<ChatroomCouponMessage>() {

        @Override
        public ChatroomCouponMessage createFromParcel(Parcel source) {
            return new ChatroomCouponMessage(source);
        }

        @Override
        public ChatroomCouponMessage[] newArray(int size) {
            return new ChatroomCouponMessage[size];
        }
    };

    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     *
     * @return 一个标志位，表明Parcelable对象特殊对象类型集合的排列。
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * 将类的数据写入外部提供的 Parcel 中。
     *
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, couponId);//该类为工具类，对消息中属性进行序列化
        ParcelUtils.writeToParcel(dest, sendId);//该类为工具类，对消息中属性进行序列化
        //这里可继续增加你消息的属性
    }
}
