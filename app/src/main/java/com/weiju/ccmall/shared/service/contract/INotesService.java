package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.newRetail.bean.ActiveValue;
import com.weiju.ccmall.newRetail.bean.ActiveValueRule;
import com.weiju.ccmall.newRetail.bean.ShoppingGuidelinesBean;
import com.weiju.ccmall.shared.bean.NoticeDetailsModel;
import com.weiju.ccmall.shared.bean.NoticeListModel;
import com.weiju.ccmall.shared.bean.RulesModel;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface INotesService {

    @GET("notes/getNotes")
    Observable<RequestResult<NoticeDetailsModel>> getNotes(
            @Query("notesId") String id
    );

    @GET("notes/getList")
    Observable<RequestResult<NoticeListModel>> getList(
            @Query("pageSize") int size,
            @Query("pageOffset") int page
    );

    @GET("agreement/getMemberRegister")
    Observable<RequestResult<RulesModel>> getRules();

    @GET("agreement/getShoppingGuidelines")
    Observable<RequestResult<ShoppingGuidelinesBean>> getShoppingGuidelines();

    @GET("agreement/getPrivacyAgreement")
    Observable<RequestResult<RulesModel>> getPrivacyAgreement();

    @GET("agreement/getActiveValueRule")
    Observable<RequestResult<ActiveValueRule>> getActiveValueRule();

    @GET("user/activevalue")
    Observable<RequestResult<ActiveValue>> activevalue();
}
