package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/5/15 on 15:23
 * @desc ${TODD}
 */
public class DayProfit {
    /**
     * msg : 分红已发放
     * money : 2
     * status : 1
     */
    @SerializedName("msg")
    public String msg;
    @SerializedName("money")
    public long money;
    @SerializedName("status")
    public long status;
}
