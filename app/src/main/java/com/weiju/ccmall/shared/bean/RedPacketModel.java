package com.weiju.ccmall.shared.bean;

import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/8/6.
 */
public class RedPacketModel extends BaseModel implements MultiItemEntity {

    /**
     * 还在有效期内，并且未领取的红包
     */
    public static final int NORMAL = 1;
    /**
     * 已经领取的红包
     */
    public static final int ALREADY_RECEIVED = 2;
    /**
     * 过期的红包
     */
    public static final int EXPIRE = 3;

    /**
     * redEnvelopeId : 86d12cb2434b4f65a833580c72a695ea
     * memberId : 47cb0f7d85474882961e03bba620fb36
     * sendType : 0
     * sendTypeStr : 注册送红包
     * title : 注册送红包
     * money : 8800
     * sendStatus : 2
     * sendStatusStr : 已领取
     * expireDate : 2018-08-10 23:59:59
     * createDate : 2018-08-03 11:44:52
     * updateDate : 2018-08-03 11:44:52
     * deleteFlag : 0
     */
    @SerializedName("redEnvelopeId")
    public String redEnvelopeId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("sendType")
    public int sendType;
    @SerializedName("sendTypeStr")
    public String sendTypeStr;
    @SerializedName("title")
    public String title;
    @SerializedName("money")
    public int money;
    @SerializedName("sendStatus")
    public int sendStatus;
    @SerializedName("sendStatusStr")
    public String sendStatusStr;
    @SerializedName("expireDate")
    public String expireDate;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;


    @Override
    public int getItemType() {
        if (money > 0) {
            return ALREADY_RECEIVED;
        }
        long expirTime = TimeUtils.string2Millis(expireDate);
        long nowTimeMills = TimeUtils.getNowTimeMills();
        if (expirTime < nowTimeMills) {
            return EXPIRE;
        }
        return NORMAL;
    }
}
