package com.weiju.ccmall.shared.page.element;

import android.content.Context;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.LiveManager;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.util.CarshReportUtils;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;

/**
 * @author chenyanming
 * @time 2019/11/20 on 18:13
 * @desc ${TODD}
 */
public class LiveElement extends LinearLayout {
    public LiveElement(Context context, Element element) {
        super(context);
        try {
            View view = inflate(getContext(), R.layout.el_live_layout, this);
            element.setBackgroundInto(view);
            final LiveRoom liveData = ConvertUtil.json2Live(element.data);

            ImageView living = view.findViewById(R.id.ivLiving);
            living.setVisibility(liveData.status == 1 ? VISIBLE : GONE);
            SimpleDraweeView liveImage = view.findViewById(R.id.ivAvatar);
            liveData.posterHeight = liveData.posterHeight == 0 ? 340 : liveData.posterHeight;
            int mImgHeight = ConvertUtil.convertHeight(getContext(), 750, liveData.posterHeight);

            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) liveImage.getLayoutParams();
            layoutParams.height = mImgHeight;
            liveImage.setLayoutParams(layoutParams);

            FrescoUtil.setImage(liveImage, liveData.poster);

            TextView title = view.findViewById(R.id.tvTitle);
            title.setText(liveData.title);

            TextView onLine = view.findViewById(R.id.tvOnLine);
            onLine.setText(String.valueOf(liveData.maxOnlineNum));
            onLine.setVisibility(liveData.status == 1 ? VISIBLE : GONE);

            this.setOnClickListener(view1 -> LiveManager.elementOnClick(context, liveData));

        } catch (Exception e) {
            CarshReportUtils.post(e);
        }
    }
}
