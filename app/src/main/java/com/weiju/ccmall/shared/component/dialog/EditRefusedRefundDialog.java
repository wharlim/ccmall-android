package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.util.CommonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditRefusedRefundDialog extends Dialog {

    @BindView(R.id.etMessage)
    EditText mEtMessage;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    private OnDialogClickListener mListener;

    public EditRefusedRefundDialog(Context context) {
        this(context, 0);
    }

    private EditRefusedRefundDialog(Context context, int themeResId) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_refused_refund);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.CENTER);
        setCanceledOnTouchOutside(false);
        initViews();
    }

    private void initViews() {
    }

    @Override
    public void setTitle(@Nullable CharSequence title) {
        tvTitle.setText(title);
    }

    @OnClick(R.id.tvConfirm)
    protected void confirmToChange() {
        if (mListener != null) {
            mListener.onConfirm(mEtMessage.getText().toString());
        }
    }

    @OnClick(R.id.tvCancel)
    protected void onClose() {
        dismiss();
    }

    public void setOnDialogClickListener(OnDialogClickListener listener) {
        mListener = listener;
    }

    public interface OnDialogClickListener {
        void onConfirm(String etStr);
    }

}
