package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.Page;
import com.weiju.ccmall.shared.bean.PageConfig;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.page.bean.Element;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface IPageService {
    @GET("page/home")
    Observable<RequestResult<List<Element>>> getHomePageConfig();

//    @GET("pageConfig/getSecondKill")
//    Observable<RequestResult<List<Element>>> getInstantPageConfig();

    @GET("page/to-be-store")
    Observable<RequestResult<List<Element>>> getToBeShopkeeperPageConfig();

    @GET("page/{pageId}")
    Observable<RequestResult<PageConfig>> getPageConfig(
            @Path("pageId") String pageId
    );

    @GET("pageConfig/getGeneralLable")
    Observable<RequestResult<List<Page>>> getPageList();
    // 集靠谱页面相关接口
    @GET("tkPage/home")
    Observable<RequestResult<List<Element>>> getTkHomePageConfig();

    @GET("tkPage/{pageId}")
    Observable<RequestResult<PageConfig>> getTkPageConfig(
            @Path("pageId") String pageId
    );

    @GET("tkPageConfig/getGeneralLable")
    Observable<RequestResult<List<Page>>> getTkPageList();


    /**
     * 嗨购精选
     */
    @GET("page/happybuy/home")
//    @GET("page/home")
    Observable<RequestResult<List<Element>>> getHappyBuyHome();
    @GET("page/getHibuyCarouselBanner")
    Observable<RequestResult<Element>> getHibuyCarouselBanner();
}
