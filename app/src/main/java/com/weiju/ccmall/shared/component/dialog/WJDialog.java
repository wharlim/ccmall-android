package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.util.CommonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.component
 * @since 2017-07-07
 */
public class WJDialog extends Dialog {

    @BindView(R.id.titleTv)
    protected TextView mTitleTv;
    @BindView(R.id.contentTv)
    protected TextView mContentTv;
    @BindView(R.id.cancelBtn)
    protected TextView mCancelBtn;
    @BindView(R.id.confirmBtn)
    protected TextView mConfirmBtn;

    protected View.OnClickListener mCancelListener;
    protected View.OnClickListener mConfirmListener;
    protected View.OnClickListener mExtraOnClickListeners;
    protected int[] mExtraClickableIds;

    private int layoutRes;

    public WJDialog(Context context) {
        this(context, R.style.Theme_Light_Dialog);
    }

    private WJDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layoutRes == 0 ? R.layout.dialog_confirm : layoutRes);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.CENTER);
        setCanceledOnTouchOutside(true);
        if (mExtraOnClickListeners != null && mExtraClickableIds != null) {
            for (int id :
                    mExtraClickableIds) {
                findViewById(id).setOnClickListener(mExtraOnClickListeners);
            }
        }
    }

    public void setExtraClickListener(int[] ids, View.OnClickListener onClickListener) {
        mExtraClickableIds = ids;
        mExtraOnClickListeners = onClickListener;
    }

    public void setLayoutRes(int layoutRes) {
        this.layoutRes = layoutRes;
    }

    @Override
    public void setTitle(CharSequence charSequence) {
        if(mTitleTv == null)
            mTitleTv = findViewById(R.id.titleTv);
        mTitleTv.setText(charSequence);
        mTitleTv.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTitle(@StringRes int titleId) {
        String string = getContext().getResources().getString(titleId);
        setTitle(string);
    }

    public void hideCancelBtn() {
        if(mCancelBtn == null)
            mCancelBtn = findViewById(R.id.cancelBtn);
        mCancelBtn.setVisibility(View.GONE);
    }

    public void setCancelText(CharSequence charSequence) {
        if(mCancelBtn == null)
            mCancelBtn = findViewById(R.id.cancelBtn);
        mCancelBtn.setText(charSequence);
        setCancelable(true);
    }

    public void setConfirmText(CharSequence charSequence) {
        if(mConfirmBtn == null)
            mConfirmBtn = findViewById(R.id.confirmBtn);
        mConfirmBtn.setText(charSequence);
    }

    public void setConfirmTextColor(int color) {
        if(mConfirmBtn == null) {
            mConfirmBtn = findViewById(R.id.confirmBtn);
        }
        mConfirmBtn.setTextColor(getContext().getResources().getColor(color));
    }

    public void setContentText(CharSequence charSequence) {
        if(mContentTv == null)
            mContentTv = findViewById(R.id.contentTv);
        mContentTv.setText(charSequence);
    }

    @Override
    public void setCancelable(boolean cancelable) {
        super.setCancelable(cancelable);
        mCancelBtn.setVisibility(cancelable ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setOnCancelListener(@Nullable OnCancelListener listener) {
    }

    public void setOnCancelListener(@Nullable View.OnClickListener listener) {
        mCancelListener = listener;
        setCancelable(true);
    }

    public void setOnConfirmListener(@Nullable View.OnClickListener listener) {
        mConfirmListener = listener;
    }

    @OnClick(R.id.cancelBtn)
    protected void onClose(View view) {
        if (mCancelListener == null) {
            dismiss();
        } else {
            mCancelListener.onClick(view);
        }
    }

    @OnClick(R.id.confirmBtn)
    protected void onConfirm(View view) {
        if (mConfirmListener == null) {
            dismiss();
        } else {
            mConfirmListener.onClick(view);
        }
    }
}
