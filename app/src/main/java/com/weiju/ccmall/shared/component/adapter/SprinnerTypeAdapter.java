package com.weiju.ccmall.shared.component.adapter;

import android.graphics.Color;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.PrestoreType;

import java.util.List;

/**
 * @author chenyanming
 * @time 2019/12/17 on 11:37
 * @desc
 */
public class SprinnerTypeAdapter extends BaseQuickAdapter<PrestoreType, BaseViewHolder> {

    private int mPositions = 0;
    private boolean mIsWhiteBg;

    public SprinnerTypeAdapter(@Nullable List<PrestoreType> data) {
        super(R.layout.item_sprinner, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, PrestoreType item) {
        helper.setText(R.id.tvItem, item.name);
        if (mIsWhiteBg) {
            helper.getView(R.id.tvItem).setBackgroundResource(R.drawable.color_sprine_white_gray_selector);
        }else {
            helper.getView(R.id.tvItem).setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int positions) {
        super.onBindViewHolder(holder, positions);
        if (positions == mPositions) {
            holder.getView(R.id.tvItem).setSelected(true);
        } else {
            holder.getView(R.id.tvItem).setSelected(false);
        }
    }

    public void setWhiteSelectBgModel(boolean isWhiteBg){
        mIsWhiteBg = isWhiteBg;
    }

    public void selectPosition(int positions) {
        mPositions = positions;
        notifyDataSetChanged();
    }

    public int getCurrentPosition(){
        return mPositions;
    }
}

