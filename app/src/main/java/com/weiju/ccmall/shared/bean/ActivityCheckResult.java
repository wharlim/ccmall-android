package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class ActivityCheckResult {

    @SerializedName("result")
    public int result;
    @SerializedName("message")
    public String message;

    public boolean isOk() {
        return result == 1;
    }
}
