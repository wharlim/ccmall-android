package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;
import com.weiju.ccmall.shared.constant.AppTypes;

import java.util.ArrayList;
import java.util.List;

public class Product extends BaseModel {
    @SerializedName("productId")
    public String productId;
    @SerializedName("skuId")
    public String skuId;
    @SerializedName("brandId")
    public String brandId;
    @SerializedName("productName")
    public String name;
    @SerializedName("minPrice")
    public long minPrice;
    @SerializedName("maxPrice")
    public long maxPrice;
    @SerializedName("unitMesure")
    public String unitMesure;
    @SerializedName("unitPack")
    public String unitPack;
    @SerializedName("unitRatio")
    public String unitRatio;
    @SerializedName("saleCount")
    public int saleCount;
    @SerializedName("status")
    public int status;
    @SerializedName("stock")
    public int stock;
    @SerializedName("sellBegin")
    public String sellBegin;
    @SerializedName("sellEnd")
    public String sellEnd;
    @SerializedName("countryId")
    public String countryId;
    @SerializedName("info")
    public String desc;
    @SerializedName("content")
    public String content;
    @SerializedName("hasPresent")
    private boolean hasPresent;
    @SerializedName("hasCoupon")
    public boolean hasCoupon;
    @SerializedName("type")
    public int type;
    @SerializedName("viewCount")
    public int viewCount;
    @SerializedName("isShippingFree")
    public boolean isShippingFree;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("country")
    public Country country;
    @SerializedName("thumbUrl")
    public String thumb;
    @SerializedName("store")
    public Store store;
    @SerializedName("properties")
    public List<Property> properties;
    @SerializedName("tags")
    public List<Tag> tags;
    @SerializedName("images")
    public List<String> images = new ArrayList<>();
    @SerializedName("skus")
    public List<SkuPvIds> skus = new ArrayList<>();
    @SerializedName("auths")
    public List<ProductAuth> auths = new ArrayList<>();

    @SerializedName("coverUrl")
    public String coverUrl;
    @SerializedName("mediaUrl")
    public String mediaUrl;


    public boolean isInstant() {
        return type == 3;
    }

    /**
     * extType : 1
     * groupExt : {"activity":{"activityId":"b44c1c23fc1f4f6c8384e7c22ed409e5","title":"测试团购活动","rule":"团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则","orderLifeCycle":24,"joinCount":1,"joinMemberNum":2,"startDate":"2017-10-31 10:47:29","endDate":"2017-11-30 10:47:29","memberCreteDate":"2016-12-01 10:47:29"},"groupSkuList":[{"activityId":"b44c1c23fc1f4f6c8384e7c22ed409e5","productId":"7a90e4080ef94f8ba45a9903063a5633","skuId":"e6af3767684449418af03cd83d6b722a","groupPrice":45,"minBuyNum":1,"maxBuyNum":2,"groupLeaderReturn":0}],"activityInfoList":[{"orderId":"523812a29e034fc39c17a724c0d465b4","memberId":"0d3e5898d0c04bde8afec8adea40179d","headImage":"","nickName":"","activityId":"b44c1c23fc1f4f6c8384e7c22ed409e5","activityStatus":2,"activityStatusStr":"拼团成功","groupCode":"1571509764504472","groupLeaderReturn":0,"expiresDate":"2017-11-02 05:52:20","joinMemberNum":2,"createOrderNum":2,"payOrderNum":2,"payDate":"2017-11-01 19:52:20"},{"orderId":"523812a29e034fc39c17a724c0d465b4","memberId":"0d3e5898d0c04bde8afec8adea40179d","headImage":"","nickName":"","activityId":"b44c1c23fc1f4f6c8384e7c22ed409e5","activityStatus":2,"activityStatusStr":"拼团成功","groupCode":"1571509764504472","groupLeaderReturn":0,"expiresDate":"2017-11-02 05:52:20","joinMemberNum":2,"createOrderNum":2,"payOrderNum":2,"payDate":"2017-11-01 19:52:20"}]}
     */

    /**
     * 是否参与团购 2：参与 5：超级拼团
     */
    @SerializedName("extType")
    public int extType;
    @SerializedName("groupExt")
    public GroupExtEntity groupExt;

    public GroupExtEntity.GroupSkuListEntity getGroupEntity(String skuId) {
        for (Product.GroupExtEntity.GroupSkuListEntity groupSkuListEntity : groupExt.groupSkuList) {
            if (skuId.equals(groupSkuListEntity.skuId)) {
                return groupSkuListEntity;
            }
        }
        return new GroupExtEntity.GroupSkuListEntity();
    }

    public static class GroupExtEntity extends BaseModel {
        /**
         * activity : {"activityId":"b44c1c23fc1f4f6c8384e7c22ed409e5","title":"测试团购活动","rule":"团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则","orderLifeCycle":24,"joinCount":1,"joinMemberNum":2,"startDate":"2017-10-31 10:47:29","endDate":"2017-11-30 10:47:29","memberCreteDate":"2016-12-01 10:47:29"}
         * groupSkuList : [{"activityId":"b44c1c23fc1f4f6c8384e7c22ed409e5","productId":"7a90e4080ef94f8ba45a9903063a5633","skuId":"e6af3767684449418af03cd83d6b722a","groupPrice":45,"minBuyNum":1,"maxBuyNum":2,"groupLeaderReturn":0}]
         * activityInfoList : [{"orderId":"523812a29e034fc39c17a724c0d465b4","memberId":"0d3e5898d0c04bde8afec8adea40179d","headImage":"","nickName":"","activityId":"b44c1c23fc1f4f6c8384e7c22ed409e5","activityStatus":2,"activityStatusStr":"拼团成功","groupCode":"1571509764504472","groupLeaderReturn":0,"expiresDate":"2017-11-02 05:52:20","joinMemberNum":2,"createOrderNum":2,"payOrderNum":2,"payDate":"2017-11-01 19:52:20"},{"orderId":"523812a29e034fc39c17a724c0d465b4","memberId":"0d3e5898d0c04bde8afec8adea40179d","headImage":"","nickName":"","activityId":"b44c1c23fc1f4f6c8384e7c22ed409e5","activityStatus":2,"activityStatusStr":"拼团成功","groupCode":"1571509764504472","groupLeaderReturn":0,"expiresDate":"2017-11-02 05:52:20","joinMemberNum":2,"createOrderNum":2,"payOrderNum":2,"payDate":"2017-11-01 19:52:20"}]
         */

        @SerializedName("activity")
        public ActivityEntity activity;
        @SerializedName("activityBean")
        public ActivityEntity activityBean;
        @SerializedName("initiatorBean")
        public InitiatorEntity initiatorBean;
        @SerializedName("groupSkuList")
        public List<GroupSkuListEntity> groupSkuList;
        @SerializedName("activityInfoList")
        public List<ActivityInfoListEntity> activityInfoList;
        @SerializedName("groupCode")
        public String groupCode;

        public static class  InitiatorEntity extends BaseModel{

            /**
             * orderId : 1ff0eba5c1eb4c89ac21aeda512265c9
             * memberId : d7c5771902814396a672f2c4f05c5db2
             * groupCode : 1781564556250768
             * productId : 5fa97d881fa94801abd5654224fd34e6
             */

            @SerializedName("orderId")
            public String orderId;
            @SerializedName("memberId")
            public String memberId;
            @SerializedName("groupCode")
            public String groupCode;
            @SerializedName("productId")
            public String productId;
        }

        public static class ActivityEntity extends BaseModel {
            /**
             * activityId : b44c1c23fc1f4f6c8384e7c22ed409e5
             * title : 测试团购活动
             * rule : 团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则团购活动规则
             * orderLifeCycle : 24
             * joinCount : 1
             * joinMemberNum : 2
             * startDate : 2017-10-31 10:47:29
             * endDate : 2017-11-30 10:47:29
             * memberCreteDate : 2016-12-01 10:47:29
             */

            @SerializedName("activityId")
            public String activityId;
            @SerializedName("title")
            public String title;
            @SerializedName("rule")
            public String rule;
            @SerializedName("orderLifeCycle")
            public int orderLifeCycle;
            @SerializedName("joinCount")
            public int joinCount;
            @SerializedName("joinMemberNum")
            public int joinMemberNum;
            @SerializedName("startDate")
            public String startDate;
            @SerializedName("endDate")
            public String endDate;
            @SerializedName("memberCreteDate")
            public String memberCreteDate;
            @SerializedName("status")
            public int status;
        }

        public static class GroupSkuListEntity extends BaseModel {
            /**
             * activityId : b44c1c23fc1f4f6c8384e7c22ed409e5
             * productId : 7a90e4080ef94f8ba45a9903063a5633
             * skuId : e6af3767684449418af03cd83d6b722a
             * groupPrice : 45
             * minBuyNum : 1
             * maxBuyNum : 2
             * groupLeaderReturn : 0
             */

            @SerializedName("activityId")
            public String activityId;
            @SerializedName("productId")
            public String productId;
            @SerializedName("skuId")
            public String skuId;
            @SerializedName("groupPrice")
            public long groupPrice;
            @SerializedName("minBuyNum")
            public int minBuyNum;
            @SerializedName("maxBuyNum")
            public int maxBuyNum;
            @SerializedName("groupLeaderReturn")
            public int groupLeaderReturn;
        }

        public static class ActivityInfoListEntity extends BaseModel {
            /**
             * orderId : 523812a29e034fc39c17a724c0d465b4
             * memberId : 0d3e5898d0c04bde8afec8adea40179d
             * headImage :
             * nickName :
             * activityId : b44c1c23fc1f4f6c8384e7c22ed409e5
             * activityStatus : 2
             * activityStatusStr : 拼团成功
             * groupCode : 1571509764504472
             * groupLeaderReturn : 0
             * expiresDate : 2017-11-02 05:52:20
             * joinMemberNum : 2
             * createOrderNum : 2
             * payOrderNum : 2
             * payDate : 2017-11-01 19:52:20
             */

            @SerializedName("orderId")
            public String orderId;
            @SerializedName("memberId")
            public String memberId;
            @SerializedName("headImage")
            public String headImage;
            @SerializedName("nickName")
            public String nickName;
            @SerializedName("activityId")
            public String activityId;
            @SerializedName("activityStatus")
            public int activityStatus;
            @SerializedName("activityStatusStr")
            public String activityStatusStr;
            @SerializedName("groupCode")
            public String groupCode;
            @SerializedName("groupLeaderReturn")
            public int groupLeaderReturn;
            @SerializedName("expiresDate")
            public String expiresDate;
            @SerializedName("joinMemberNum")
            public int joinMemberNum;
            @SerializedName("createOrderNum")
            public int createOrderNum;
            @SerializedName("payOrderNum")
            public int payOrderNum;
            @SerializedName("payDate")
            public String payDate;
        }
    }


}
