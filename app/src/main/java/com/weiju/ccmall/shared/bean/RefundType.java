package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author Ben
 * @date 2020/5/25.
 */
public class RefundType {

    /**
     * values : 买家申请退货
     * key : 0
     */

    @SerializedName("values")
    public String values;
    @SerializedName("key")
    public String key;
}
