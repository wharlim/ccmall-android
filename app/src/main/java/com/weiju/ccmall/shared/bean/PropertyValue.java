package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

/**
 * Created by JayChan on 2017/2/22.
 */

public class PropertyValue extends BaseModel {
    @SerializedName("propertyValueId")
    public String id;
    @SerializedName("propertyValue")
    public String value;

    public String skuId;
    public String stock;
    public boolean isSelected = false;
}
