package com.weiju.ccmall.shared.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.weiju.ccmall.shared.basic.BaseModel;

import java.util.List;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/9/14.
 */
public class ProductActivityModel extends BaseModel implements MultiItemEntity {

    public static final int TEXT = 1;
    public static final int IMG = 2;
    private int mType;
    private SkuInfo.ActivityTagEntity mTagEntity;
    private List<Presents> mPresents;

    public ProductActivityModel(int type) {
        mType = type;
    }

    public ProductActivityModel(int type, SkuInfo.ActivityTagEntity tagEntity) {
        mType = type;
        mTagEntity = tagEntity;
    }

    public ProductActivityModel(int type, List<Presents> presents) {
        mType = type;
        mPresents = presents;
    }

    public SkuInfo.ActivityTagEntity getTagEntity() {
        return mTagEntity;
    }

    public List<Presents> getPresents() {
        return mPresents;
    }

    @Override
    public int getItemType() {
        return mType;
    }
}
