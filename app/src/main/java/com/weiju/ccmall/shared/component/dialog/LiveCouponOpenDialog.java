package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.blankj.utilcode.utils.TimeUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.CouponReceive;
import com.weiju.ccmall.shared.bean.LiveCoupon;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
public class LiveCouponOpenDialog extends Dialog {


    @BindView(R.id.ivAvatar)
    SimpleDraweeView mSimpleDraweeView;
    @BindView(R.id.tvConponTitle)
    TextView mTvConponTitle;
    @BindView(R.id.tvMoney)
    TextView mTvMoney;
    @BindView(R.id.tvDate)
    TextView mTvDate;


    private Activity mActivty;
    private final String mLiveId;
    private final String mSendId;
    private String mCouponId;
    private String mHeadImage;

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);

    public LiveCouponOpenDialog(@NonNull Activity context, String liveId, String sendId, String couponId, String headImage) {
        super(context, R.style.Theme_Light_Dialog);
        mActivty = context;
        mLiveId = liveId;
        mSendId = sendId;
        mCouponId = couponId;
        mHeadImage = headImage;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_coupon_open);
        ButterKnife.bind(this);

        initData();
    }

    private void initData() {
        FrescoUtil.setImage(mSimpleDraweeView, mHeadImage);
        APIManager.startRequest(mService.getLiveCouponById(mCouponId), new BaseRequestListener<LiveCoupon>(mActivty) {
            @Override
            public void onSuccess(LiveCoupon result) {
                mTvConponTitle.setText(result.title);
                mTvMoney.setText(MoneyUtil.centToYuan¥StrNoZero(result.cost));
                String startDate = TimeUtils.millis2String(TimeUtils.string2Millis(result.limitStartDate), "yyyy.MM.dd");
                String endDate = TimeUtils.millis2String(TimeUtils.string2Millis(result.limitEndDate), "yyyy.MM.dd");
                mTvDate.setText(String.format("有效期：%s-%s", startDate, endDate));
            }
        }, getContext());
    }

    @OnClick({R.id.ivClose})
    protected void close() {
        dismiss();
    }

    @OnClick(R.id.ivOpen)
    protected void open() {
        if (UiUtils.checkUserLogin(mActivty)) {
            APIManager.startRequest(mService.receiveLiveCoupon(mSendId, mLiveId), new BaseRequestListener<CouponReceive>(mActivty) {
                @Override
                public void onSuccess(CouponReceive result) {
                    LiveCouponReceiveSuccessDialog dialog = new LiveCouponReceiveSuccessDialog(mActivty, result);
                    dialog.show();
                    dismiss();
                    EventBus.getDefault().post(new EventMessage(Event.couponDialogShow));
                }

                @Override
                public void onError(Throwable e) {
                    super.onError(e);
                    LiveCouponReceiveFailDialog dialog = new LiveCouponReceiveFailDialog(mActivty);
                    dialog.show();
                    dismiss();
                    EventBus.getDefault().post(new EventMessage(Event.couponDialogShow));
                }
            }, getContext());
        }
    }

    @Override
    public void dismiss() {
        EventBus.getDefault().post(new EventMessage(Event.couponDialogDismiss));
        super.dismiss();
    }
}
