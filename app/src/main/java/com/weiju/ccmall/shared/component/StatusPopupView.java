package com.weiju.ccmall.shared.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.PrestoreType;
import com.weiju.ccmall.shared.component.adapter.SprinnerTypeAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author chenyanming
 * @time 2019/12/17 on 14:11
 * @desc
 */
public class StatusPopupView extends FrameLayout {

    @BindView(R.id.rvLeft)
    RecyclerView mRvLeft;
    @BindView(R.id.rvRight)
    RecyclerView mRvRight;

    private SelectListener mSelectListener;

    private String mLeftValue;
    private SprinnerTypeAdapter mLeftAdapter;
    private SprinnerTypeAdapter mRightAdapter;
    private Map<String, List<PrestoreType>> mLeftTypeMap = new HashMap<>();
    private ArrayList<PrestoreType> mRightTypes = new ArrayList<>();

    @SuppressLint("CheckResult")
    public StatusPopupView(@NonNull Context context) {
        super(context);
        View view = inflate(context, R.layout.view_prestore_data_popup, this);
        ButterKnife.bind(this, view);

        initLeftList();
        initRightList();

        List<PrestoreType> list = new ArrayList<>();
        list.add(new PrestoreType("执行中", "0"));
        list.add(new PrestoreType("执行成功", "1"));
        list.add(new PrestoreType("执行失败", "2"));

        List<PrestoreType> list1 = new ArrayList<>();
        list1.add(new PrestoreType("执行中2", "0"));
        list1.add(new PrestoreType("执行成功2", "1"));
        list1.add(new PrestoreType("执行失败2", "2"));


        mLeftTypeMap.put("0", list);
        mLeftTypeMap.put("1", list1);

    }

    private void initRightList() {
        mRvRight.setLayoutManager(new LinearLayoutManager(getContext()));
        mRightAdapter = new SprinnerTypeAdapter(mRightTypes);
        mRvRight.setAdapter(mRightAdapter);
        mRvRight.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                mRightAdapter.selectPosition(position);
                PrestoreType prestoreType = mRightTypes.get(position);
                select(prestoreType.value, prestoreType.name);
            }
        });
    }

    private void initLeftList() {
        mRvLeft.setLayoutManager(new LinearLayoutManager(getContext()));
        ArrayList<PrestoreType> prestoreTypes = new ArrayList<>();
        prestoreTypes.add(new PrestoreType("全部类型", ""));
        prestoreTypes.add(new PrestoreType("收入", "0"));
        prestoreTypes.add(new PrestoreType("支出", "1"));

        mLeftAdapter = new SprinnerTypeAdapter(prestoreTypes);
        mRvLeft.setAdapter(mLeftAdapter);
        mLeftAdapter.setWhiteSelectBgModel(true);
        mRvLeft.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (mLeftAdapter.getCurrentPosition() == position) {
                    return;
                }
                if (position == 0) {
                    select("", "全部类型");
                    mRvRight.setVisibility(View.INVISIBLE);
                } else {
                    mLeftValue = prestoreTypes.get(position).value;
                    mRvRight.setVisibility(View.VISIBLE);
                    mRightTypes.clear();
                    mRightTypes.addAll(mLeftTypeMap.get(mLeftValue));
                }
                mRightAdapter.selectPosition(0);
                mLeftAdapter.selectPosition(position);
            }
        });
    }

    private void select(String type, String text) {
        if (mSelectListener != null) {
            mSelectListener.onSelectType(type, text);
        }
    }

    public void setSelectListener(SelectListener selectListener) {
        mSelectListener = selectListener;
    }

    public interface SelectListener {
        void onSelectType(String type, String text);
    }

}
