package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.cart.CartActivity;
import com.weiju.ccmall.module.live.activity.ChooseLiveGoodsActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.adapter.LiveProductAdapter;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.CartManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.message.ChatroomGoodsMessage;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.ChatroomKit;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/26 on 10:30
 * @desc
 */
public class LiveProductDialog extends Dialog {

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.tvProduceTotal)
    TextView mTvProduceTotal;
    @BindView(R.id.line)
    View mLine;
    @BindView(R.id.tvAddProduct)
    TextView mTvAddProduct;
    @BindView(R.id.ivCart)
    ImageView ivCart;
    private Activity mContext;

    private int mCurrentPage = 1;
    private String mLiveId;
    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);
    private LiveProductAdapter mLiveProductAdapter;

    private boolean mIsAdd;
    private boolean mShopkeeper;
    private boolean mIsCreate;

    public LiveProductDialog(@NonNull Activity context, String liveId, boolean isAdd) {
        this(context, liveId, isAdd, false);
    }

    public LiveProductDialog(@NonNull Activity context, String liveId, boolean isAdd, boolean shopkeeper) {
        this(context, liveId, isAdd, shopkeeper, false);
    }

    public LiveProductDialog(@NonNull Activity context, String liveId, boolean isAdd, boolean shopkeeper, boolean isCreate) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mLiveId = liveId;
        mIsAdd = isAdd;
        mShopkeeper = shopkeeper;
        mIsCreate = isCreate;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_product);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
//        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
//        layoutParams.dimAmount = 0.0f;
//        getWindow().setAttributes(layoutParams);

        initView();
        initData();

    }

    private void initData() {
        getLiveSkus(true);
    }

    private void initView() {
        Log.d("Seven", "isAdad" + mIsAdd);
//        mLine.setVisibility(mIsAdd ? View.GONE : View.VISIBLE);
        mLine.setVisibility(mIsAdd ? View.GONE : View.GONE);
        mTvAddProduct.setVisibility(mIsAdd ? View.GONE : View.VISIBLE);
        ivCart.setVisibility(mIsAdd ? View.VISIBLE : View.GONE);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        if (!mIsCreate) {
            mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getLiveSkus(true);
                }
            });
        }

        mLiveProductAdapter = new LiveProductAdapter();
        mLiveProductAdapter.setIsAdd(mIsAdd);
        mLiveProductAdapter.setIsCreate(mIsCreate);

        View inflate = View.inflate(mContext, R.layout.cmp_no_data, null);
        mLiveProductAdapter.setEmptyView(inflate);

        if (!mIsCreate) {
            mLiveProductAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
                @Override
                public void onLoadMoreRequested() {
                    getLiveSkus(false);
                }
            }, mRecyclerView);
        }

        mLiveProductAdapter.setHeaderAndEmpty(true);
        mLiveProductAdapter.setFooterViewAsFlow(true);

        mRecyclerView.setAdapter(mLiveProductAdapter);

        mLiveProductAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                EventBus.getDefault().post(new EventMessage(Event.openNewActivity));
                SkuInfo item = mLiveProductAdapter.getItem(position);
                if (null != item) {
                    if (!mIsCreate) recordClickEventForGood(item.skuId);
                    EventUtil.viewProductDetail(mContext, item.skuId, false, "", mLiveId);
                }
                Log.d("Seven", "查看商品详情");
            }
        });

        mLiveProductAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                List<SkuInfo> data = mLiveProductAdapter.getData();
                SkuInfo item = data.get(position);
                if (UiUtils.checkUserLogin(mContext) && null != item) {
                    switch (view.getId()) {
                        case R.id.tvExplain:
                            if (!view.isSelected()) {
                                APIManager.startRequest(mILiveService.addLiveSkuId(mLiveId, item.skuId),
                                        new BaseRequestListener<Object>(mContext) {
                                            @Override
                                            public void onSuccess(Object result) {
                                                super.onSuccess(result);
                                                ChatroomGoodsMessage message = new ChatroomGoodsMessage();
                                                ChatroomKit.sendMessage(message, true);
                                                mLiveProductAdapter.setSelectIndex(position);
                                            }
                                        }, mContext);
                            }
                            break;
                        case R.id.ivAddOrDelete:
                            if (mIsAdd) {
                                //添加购物车
                                recordClickEventForGood(item.skuId);
                                CartManager.addToCart(getContext(), item, 1, false, mLiveId);
                            } else {
                                //删除直播商品
                                if (mIsCreate) {
                                    mLiveProductAdapter.remove(position);
                                    EventBus.getDefault().post(new EventMessage(Event.addLiveProduct, getData()));
                                } else {
                                    if (mLiveProductAdapter.getSelectedIndex() == position) {
                                        ToastUtil.error("当前讲解的商品不可删除");
                                        return;
                                    }
                                    removeProduct(item);
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }

            }
        });

    }

    private int mSelectIndex = -1;
    private String mCurrentSkuId;
    private void removeProduct(SkuInfo skuInfo) {
        APIManager.startRequest(mILiveService.removeLiveSkuRelation(mLiveId, skuInfo.skuId),
                new BaseRequestListener<Object>(mContext) {
                    @Override
                    public void onSuccess(Object result) {
                        ToastUtil.success("删除成功");
                        getLiveSkus(true);
                    }
                }, mContext);
    }


    @OnClick(R.id.ivCart)
    protected void cart() {
        if (UiUtils.checkUserLogin(getContext())) {
            EventBus.getDefault().post(new EventMessage(Event.openNewActivity));
            mContext.startActivity(new Intent(getContext(), CartActivity.class));
        }
    }

    @OnClick({R.id.tvAddProduct})
    protected void addProduct() {
        if (mIsCreate) {
            List<SkuInfo> data = getData();
            ArrayList<String> skuIds = new ArrayList<>();
            for (SkuInfo info : data) {
                skuIds.add(info.skuId);
            }
            ChooseLiveGoodsActivity.start(mContext, null, mShopkeeper, true, skuIds);
        } else {
            EventBus.getDefault().post(new EventMessage(Event.openNewActivity));
            ChooseLiveGoodsActivity.start(mContext, mLiveId, mShopkeeper);
        }
    }

    public void setData(List<SkuInfo> skus) {
        mLiveProductAdapter.setNewData(skus);
    }

    public void clearSelectIndex() {
        mLiveProductAdapter.setSelectIndex(-1);
    }

    public List<SkuInfo> getData() {
        return mLiveProductAdapter.getData();
    }

    public void getLiveSkus(boolean isRefresh) {
        if (mIsCreate) return;
        int selectedIndex = mLiveProductAdapter.getSelectedIndex();
        if (selectedIndex != -1){
            SkuInfo info = mLiveProductAdapter.getData().get(selectedIndex);
            mCurrentSkuId = info.skuId;
        }
        if (isRefresh) {
            mCurrentPage = 1;
        } else {
            mCurrentPage++;
        }
        APIManager.startRequest(mILiveService.getLiveSkus(mLiveId, mCurrentPage, 20),
                new BaseRequestListener<PaginationEntity<SkuInfo, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<SkuInfo, Object> result) {
                        mTvProduceTotal.setText(String.format("全部商品 %s", result.total));
                        if (mCurrentPage == 1) {
                            mLiveProductAdapter.setNewData(result.list);
                        } else {
                            mLiveProductAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mLiveProductAdapter.loadMoreEnd();
                        } else {
                            mLiveProductAdapter.loadMoreComplete();
                        }
                        if (selectedIndex != -1) {
                            mSelectIndex = -1;
                            List<SkuInfo> data = mLiveProductAdapter.getData();
                            for (int i = 0; i < data.size(); i++) {
                                String skuId = data.get(i).skuId;
                                if (mCurrentSkuId.equals(skuId)) {
                                    mSelectIndex = i;
                                    break;
                                }
                            }
                            mLiveProductAdapter.setSelectIndex(mSelectIndex);
                        }
                    }
                }, mContext);
    }

    private void recordClickEventForGood(String skuId) {
        APIManager.startRequest(mILiveService.clickShareOrGoods(mLiveId, 2, skuId), new BaseRequestListener<Object>() {
            @Override
            public void onSuccess(Object result) {
                // no op
            }
        }, mContext);
    }
}
