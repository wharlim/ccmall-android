package com.weiju.ccmall.shared.contracts;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.contracts
 * @since 2017-07-06
 */
public interface IRegion {

    String getType();

    String getName();

    String getId();
}
