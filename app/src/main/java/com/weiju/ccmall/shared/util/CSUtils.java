package com.weiju.ccmall.shared.util;

import android.content.Context;

import com.sobot.chat.SobotApi;
import com.sobot.chat.api.model.Information;
import com.weiju.ccmall.shared.bean.User;

/**
 *
 *
 * <p>
 * Created by zjm on 2017/8/6.
 * 客服
 */
public class CSUtils {
    public final static String KEY = "0abe14e42de0489eb36f8622bd30981a";

    public static void start(Context context) {
//        if (!SessionUtil.getInstance().isLogin()) {
//            ToastUtil.error("请先登录");
//            context.startActivity(new Intent(context, LoginActivity.class));
//            return;
//        }
//        User loginUser = SessionUtil.getInstance().getLoginUser();
//        Information info = new Information();
//        info.setAppkey(KEY);
//        info.setUid(loginUser.id);
//        info.setRealname(loginUser.nickname);
//        info.setTel(loginUser.phone);
//        info.setFace(loginUser.avatar);
//        info.setColor("#f51861");
//        SobotApi.startSobotChat(context, info);

//        Intent intent = new Intent(context, WebViewActivity.class);
//        intent.putExtra("url","https://static.meiqia.com/dist/standalone.html?_=t&eid=55468");
//        context.startActivity(intent);

        start(context, "");
    }

    public static void start(Context context, String comment) {
//        User loginUser = SessionUtil.getInstance().getLoginUser();
//        HashMap<String, String> clientInfo = new HashMap<>();
//        if (loginUser != null) {
//            clientInfo.put("name", loginUser.nickname);
//            clientInfo.put("avatar", loginUser.avatar);
//            clientInfo.put("tel", loginUser.phone);
//            clientInfo.put("comment", comment);
//        }
//
//        Intent intent = new MQIntentBuilder(context)
//                .setClientInfo(clientInfo)
//                .build();
//        context.startActivity(intent);


        start(context, comment, null);
    }

    public static void start(Context context, String comment, String serviceId){
        Information info = new Information();
        info.setAppkey("ef0dfc07158045d59434a84d929443ae");
        if(serviceId != null){
            //转接类型(0-可转入其他客服，1-必须转入指定客服)
            info.setTranReceptionistFlag(1);
            //指定客服id
            info.setReceptionistId(serviceId);
        }
        //用户编号
        //注意：uid为用户唯一标识，不能传入一样的值
        User user = SessionUtil.getInstance().getLoginUser();
        if (user != null) {
            info.setUid(user.id);
            //用户昵称，选填
            info.setUname(user.nickname);
            //用户姓名，选填
            info.setRealname(user.userName);
            //用户电话，选填
            info.setTel(user.phone);
            //自定义头像，选填
            info.setFace(user.avatar);
            //用户备注，选填
            info.setRemark(comment);
            info.setShowSatisfaction(true);
        }
        SobotApi.startSobotChat(context, info);
    }

}
