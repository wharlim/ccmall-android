package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/8/24.
 */
public class BindPhoneMsgModel extends BaseModel {

    /**
     * msg : 手机号码已绑定，请更换手机号
     * status : 2
     */

    @SerializedName("msg")
    public String msg;
    /**
     * 0成功 1错误 2已绑定
     */
    @SerializedName("status")
    public int status;
}
