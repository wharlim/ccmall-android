package com.weiju.ccmall.shared.service.contract;

        import com.weiju.ccmall.shared.bean.UploadResponse;
        import com.weiju.ccmall.shared.bean.api.RequestResult;

        import io.reactivex.Observable;
        import okhttp3.MultipartBody;
        import okhttp3.RequestBody;
        import retrofit2.http.Multipart;
        import retrofit2.http.POST;
        import retrofit2.http.Part;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service.contract
 * @since 2017-06-07
 */
public interface IUploadService {

    @Multipart
    @POST("upload/uploadImage")
    Observable<RequestResult<UploadResponse>> uploadImage(@Part MultipartBody.Part body, @Part("version") RequestBody version);
}
