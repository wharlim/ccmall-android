package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.module.groupBuy.GroupBuyModel;
import com.weiju.ccmall.module.order.body.AddCommentBody;
import com.weiju.ccmall.module.shop.bean.ShopItem;
import com.weiju.ccmall.newRetail.bean.HappyBuyStore;
import com.weiju.ccmall.newRetail.bean.OrderRates;
import com.weiju.ccmall.shared.bean.Category;
import com.weiju.ccmall.shared.bean.InstantData;
import com.weiju.ccmall.shared.bean.JoinProduct;
import com.weiju.ccmall.shared.bean.Keyword;
import com.weiju.ccmall.shared.bean.PopupOrderList;
import com.weiju.ccmall.shared.bean.Product;
import com.weiju.ccmall.shared.bean.ProductComment;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.ViewHistory;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface IProductService {
    @GET("user/authorizeCcm")
    Observable<RequestResult<Object>> authorizeCcm(@Query("status") int status);

    @GET("product/skulist")
    Observable<RequestResult<ArrayList<SkuInfo>>> getListBySkuIds(@Query("skuIds") String skuIds);

    @GET("product/skuDetail")
    Observable<RequestResult<SkuInfo>> getSkuById(@Query("skuId") String skuId);

    @GET("channel-products/{memberSkuId}")
    Observable<RequestResult<SkuInfo>> getSkuByMemberSkuId(@Path("memberSkuId") String memberSkuId);

    @GET("product/getNewerSkuDetail")
    Observable<RequestResult<SkuInfo>> getNewerSkuDetail();


    @GET("product/spuDetail")
    Observable<RequestResult<Product>> getDetailById(@Query("spuId") String productId);

    @GET("channel-products/skus")
    Observable<RequestResult<List<SkuInfo>>> getProductsSkus(@Query("memberId") String memberId, @Query("skuIds") String productId);

    @GET("product/skuDetailByProperty")
    Observable<RequestResult<SkuInfo>> getSkuByPropertyValueIds(@Query("productId") String productId, @Query("propertyValueIds") String propertyValueIds);

    @GET("secondKill/getIndexSecondKill")
    Observable<RequestResult<InstantData>> getInstantComponentData(
            @Query("secondKillId") String id
    );

    @GET("product/getSpuListByCategory")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getCategoryProductList(@Query("categoryId") String mCategoryId, @Query("pageOffset") int page);

    @GET("product/getSpuListByCategory")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getCategoryProductList(@Query("categoryId") String mCategoryId, @Query("pageOffset") int page, @Query("productType") String productType);


    @GET("product/searchSkuList")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> search(@Query("skuName") String keyword, @Query("pageOffset") int page);

    @GET("product/searchSkuList")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> search(@Query("skuName") String keyword, @Query("pageOffset") int page, @Query("productType") String productType);

    @GET("productPush/searchSkuList")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> searchPush(@Query("skuName") String keyword, @Query("pageOffset") int page);

    @FormUrlEncoded
    @POST("viewHistory/addViewRecord")
    Observable<Object> addViewRecord(
            @Field("memberId") String memberId,
            @Field("skuId") String skuId
    );

    @POST("product/cancelNewerSku")
    Observable<RequestResult<Object>> cancelNewerSku();

    @GET("viewHistory/getViewHistory")
    Observable<RequestResult<ViewHistory>> getViewHistory();

    @GET("product/getKeyWordList")
    Observable<RequestResult<List<Keyword>>> getHotKeywords();

    @GET("popupOrder/getPopupOrderList")
    Observable<RequestResult<PopupOrderList>> getPopupOrderList(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize
    );

    @POST("productComment/saveProductComment")
    Observable<RequestResult<Object>> saveProductComment(
            @Body AddCommentBody body
    );

    @POST("productComment/addToProductComment")
    Observable<RequestResult<Object>> addToProductComment(
            @Body AddCommentBody body
    );

    @GET("productComment/getProductComment")
    Observable<RequestResult<PaginationEntity<ProductComment, Object>>> getProductComment(
            @Query("productId") String productId,
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize
    );

    @GET("product/getNewUpSkuList")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getNewUpSkuList(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize
    );

    @GET("product/getSaleUpSkuList")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getSaleSkuList(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize
    );

    @GET()
    Observable<RequestResult<GroupBuyModel>> getGroupInfo(
            @Url String url,
            @Query("groupCode") String id
    );


    @GET("productComment/getProductCommentDetail")
    Observable<RequestResult<List<ProductComment>>> getProductCommentDetail(
            @Query("productId") String productId,
            @Query("order1Id") String order1Id,
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize
    );


    @GET("product/getNewRetailSkuList")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getNewRetailSkuList(@Query("pageOffset") int page,
                                                                                     @Query("pageSize") int pageSize, @Query("self") String self);

    @GET("productComment/getNewGiftStoreComment")
    Observable<RequestResult<PaginationEntity<ProductComment, Object>>> getNewGiftStoreComment(@Query("pageOffset") int page,
                                                                                               @Query("pageSize") int pageSize, @Query("commentType") int commentType);


    @POST("productComment/deleteMyProductComment")
    Observable<RequestResult<Object>> deleteMyProductComment(@Query("productId") String productId, @Query("orderCode") String orderCode);


    @GET("product/getUp")
    Observable<RequestResult<Object>> getUp();

    @GET("product/getUpProduct")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getUpProduct();

    @GET("product/getOrderRates")
    Observable<RequestResult<OrderRates>> getOrderRates();

    @GET("product/getUpProduct")
    Observable<RequestResult<HappyBuyStore>> getUpProductHappyBuy();

    @GET("product/getProList")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getProList(@Query("pageOffset") int page,
                                                                            @Query("pageSize") int pageSize, @Query("pushId") int pushId);

    @GET("product/getStore")
    Observable<RequestResult<ShopItem>> getStore(@Query("storeId") String storeId);

    @GET("liveStoreHome/getStore")
    Observable<RequestResult<ShopItem>> getLiveStore(@Query("storeId") String storeId);

    @FormUrlEncoded
    @POST("collect/addStore")
    Observable<RequestResult<Object>> collectStore(@Field("storeId") String storeId);

    @FormUrlEncoded
    @POST("collect/delStore ")
    Observable<RequestResult<Object>> delCollectStore(@Field("storeId") String storeId);

    @GET("category/getStoreParentCategory")
    Observable<RequestResult<PaginationEntity<Category, Object>>> getStoreParentCategory(@Query("storeId") String storeId, @Query("pageOffset") int page, @Query("pageSize") int pageSize);

    @GET("liveStoreHome/getCategoryByStore")
    Observable<RequestResult<PaginationEntity<Category, Object>>> getCategoryByStore(@Query("storeId") String storeId);

    @GET("product/getSpuListByCategoryAndStore")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getSpuListByCategoryAndStore(
            @Query("storeId") String storeId,
            @Query("categoryId") String categoryId,
            @Query("pageOffset") int pageOffset,
            @Query("pageSize") int pageSize
    );

    @GET("liveStoreHome/getLiveStoreSpuListByCategory")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getLiveStoreSpuListByCategory(@Query("storeId") String storeId, @Query("categoryId") String categoryId,
                                                                                               @Query("pageOffset") int page, @Query("pageSize") int pageSize);

    @GET("channel-products")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getMyPackages(
            @Query("pageOffset") int pageOffset,
            @Query("pageSize") int pageSize
    );

    @GET("product/packages")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getProductPackages(
            @Query("keyword") String keyword,
            @Query("pageOffset") int pageOffset,
            @Query("pageSize") int pageSize
    );

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("channel-products/join")
    Observable<RequestResult<Object>> joinProduct(  @Body ArrayList<JoinProduct> body);

    @Headers("Content-Type: application/json;charset=UTF-8")
    @POST("channel-products/edit")
    Observable<RequestResult<Object>> editProduct(  @Body ArrayList<JoinProduct> body);
}
