package com.weiju.ccmall.shared.service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.view.View;

import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.ToastUtils;
import com.umeng.analytics.MobclickAgent;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.AuthPhoneActivity;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.user.BindPhoneActivity;
import com.weiju.ccmall.module.user.LoginActivity;
import com.weiju.ccmall.module.user.SetPasswordActivity;
import com.weiju.ccmall.module.user.SetPayPasswordActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.HasPasswordModel;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.dialog.WJDialog;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.PushManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IUserService;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;

import io.rong.imlib.RongIMClient;

public class UserService {

    public static void login(User user) {
        MobclickAgent.onProfileSignIn(user.id);
        SessionUtil.getInstance().setLoginUser(user);
        LoginActivity.loginTXchat();
    }

    public static void logout(Context context) {
        MobclickAgent.onProfileSignOff();
        SessionUtil.getInstance().logout();
        EventBus.getDefault().post(new EventMessage(Event.logout));
        EventBus.getDefault().post(new EventMessage(Event.viewHome));
        PushManager.setJPushInfo(context, null);
        RongIMClient.getInstance().logout();
    }

    public static void logoutWithoutEvent(Context context) {
        MobclickAgent.onProfileSignOff();
        SessionUtil.getInstance().logout();
        PushManager.setJPushInfo(context, null);
    }

    public static void checkHasPassword(final Activity activity, final HasPasswordListener listener) {
        IUserService service = ServiceManager.getInstance().createService(IUserService.class);
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (StringUtils.isEmpty(loginUser.phone)) {
            ToastUtils.showShortToast("请先绑定手机");
            activity.startActivity(new Intent(activity, BindPhoneActivity.class));
            return;
        }
        APIManager.startRequest(service.hasPassowrd(), new BaseRequestListener<HasPasswordModel>(activity) {
            @Override
            public void onSuccess(HasPasswordModel model) {
                super.onSuccess(model);
                if (model.status) {
                    listener.onHasPassword();
                } else {
                    showNoPasswordDialog(activity);
                }
            }
        },activity);
    }

    public static void checkHasPayPwd(final Activity activity, final HasPasswordListener listener) {
        IUserService service = ServiceManager.getInstance().createService(IUserService.class);
        User loginUser = SessionUtil.getInstance().getLoginUser();
        if (loginUser == null) {
            ToastUtil.error("用户信息为空，请先登录");
            return;
        }
        if (StringUtils.isEmpty(loginUser.phone)) {
            ToastUtil.error("请先绑定手机");
            activity.startActivity(new Intent(activity, BindPhoneActivity.class));
            return;
        }
        if (loginUser.autoAuthStatus != AppTypes.AUTH_STATUS.SUCESS) {
            Intent intent2 = new Intent(activity, AuthPhoneActivity.class);
            SharedPreferences sharedPreferences = activity.getSharedPreferences("authType", 0);
            sharedPreferences.edit().putString("authType", "UserCenter").commit();
            intent2.putExtra(Config.INTENT_KEY_TYPE_NAME, Config.USER.INTENT_KEY_TYPE_AUTH_PHONE);
            activity.startActivity(intent2);
            MyApplication.isShowPayPwd = true;
            return;
        }
        APIManager.startRequest(service.hasPayPwd(), new BaseRequestListener<HasPasswordModel>(activity) {
            @Override
            public void onSuccess(HasPasswordModel model) {
                super.onSuccess(model);
                if (model.status) {
                    listener.onHasPassword();
                } else {
                    showNoPayPwdDialog(activity);
                }
            }
        },activity);

    }

    public static void checkIsLogin(final Activity activity, final IsLoginListener listener) {
        IUserService service = ServiceManager.getInstance().createService(IUserService.class);
        APIManager.startRequest(service.checkIsLogin(), new BaseRequestListener<User>(activity) {
            @Override
            public void onSuccess(User user) {
                super.onSuccess(user);
                    if (!TextUtils.isEmpty(user.id)){
                        listener.isLogin();
                    }else {
                        EventBus.getDefault().post(new EventMessage(Event.goToLogin));
                    }

            }
        },activity);
    }

    private static void showNoPasswordDialog(final Activity activity) {
        final WJDialog dialog = new WJDialog(activity);
        dialog.show();
        dialog.setTitle("您还未设置密码");
        dialog.setContentText("密码可用于登录、转账、提现等，为保证账户安全，请设置密码");
        dialog.setCancelText("返回");
        dialog.setConfirmText("去设置");
        dialog.setOnConfirmListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent(activity, SetPasswordActivity.class);
                activity.startActivity(intent);
            }
        });
    }
    private static void showNoPayPwdDialog(final Activity activity) {
        final WJDialog dialog = new WJDialog(activity);
        dialog.show();
        dialog.setTitle("温馨提示");
        dialog.setContentText("您的账号尚未设置支付密码，请先设置支付密码");
        dialog.setCancelText("取消");
        dialog.setConfirmText("设置支付密码");
        dialog.setConfirmTextColor(R.color.btn_ccm_tab_color);
        dialog.setOnConfirmListener(v -> {
            dialog.dismiss();
            SetPayPasswordActivity.start(activity);
        });
    }

    public interface HasPasswordListener {
        void onHasPassword();
    }

    public interface IsLoginListener{
        void isLogin();
    }
}
