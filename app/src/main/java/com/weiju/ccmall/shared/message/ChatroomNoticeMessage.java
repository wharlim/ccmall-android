package com.weiju.ccmall.shared.message;

import android.annotation.SuppressLint;
import android.os.Parcel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

/**
 * @author chenyanming
 * @date 2020/2/19.
 */
@SuppressLint("ParcelCreator")
@MessageTag(value = "CCM:Chatroom:Live:Notice", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class ChatroomNoticeMessage extends MessageContent {
    public ChatroomNoticeMessage() {
    }

    private String notice;

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("notice", notice);

        } catch (JSONException e) {
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    public ChatroomNoticeMessage(byte[] data) {
        String jsonStr = null;

        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e1) {

        }

        try {
            JSONObject jsonObj = new JSONObject(jsonStr);

            if (jsonObj.has("notice")) {
                notice = jsonObj.optString("notice");
                setNotice(notice);
            }
        } catch (JSONException e) {
        }

    }

    public void setNotice(String notice) {
        this.notice = notice;
    }


    public String getNotice() {
        return notice;
    }


    //给消息赋值。
    public ChatroomNoticeMessage(Parcel in) {
        notice = ParcelUtils.readFromParcel(in);//该类为工具类，消息属性
        //这里可继续增加你消息的属性
    }

    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<ChatroomNoticeMessage> CREATOR = new Creator<ChatroomNoticeMessage>() {

        @Override
        public ChatroomNoticeMessage createFromParcel(Parcel source) {
            return new ChatroomNoticeMessage(source);
        }

        @Override
        public ChatroomNoticeMessage[] newArray(int size) {
            return new ChatroomNoticeMessage[size];
        }
    };

    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     *
     * @return 一个标志位，表明Parcelable对象特殊对象类型集合的排列。
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * 将类的数据写入外部提供的 Parcel 中。
     *
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, notice);//该类为工具类，对消息中属性进行序列化
        //这里可继续增加你消息的属性
    }
}
