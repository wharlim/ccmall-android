package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/8/27.
 */
public class MyCommentModel extends BaseModel {


    @SerializedName("order1Id")
    public String order1Id;
    @SerializedName("orderId")
    public String orderId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("orderCode")
    public String orderCode;
    @SerializedName("skuId")
    public String skuId;
    @SerializedName("productId")
    public String productId;
    @SerializedName("productImage")
    public String productImage;
    @SerializedName("skuName")
    public String skuName;
    @SerializedName("properties")
    public String properties;
    @SerializedName("price")
    public long price;
    @SerializedName("payMoney")
    public long payMoney;
    @SerializedName("quantity")
    public int quantity;
    @SerializedName("lineTotal")
    public int lineTotal;
    @SerializedName("deleteStatus")
    public int deleteStatus;

    /**
     * 0未评价 1已评价 2追加评价
     */
    @SerializedName("commentStatus")
    public int commentStatus;
    @SerializedName("marketPrice")
    public long marketPrice;
    @SerializedName("realPrice")
    public long realPrice;
    @SerializedName("discountCoupon")
    public int discountCoupon;
    @SerializedName("score")
    public int score;
}
