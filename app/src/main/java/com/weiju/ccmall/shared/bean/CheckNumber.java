package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/7/30.
 */
public class CheckNumber {

    /**
     * checkNumber : 3840
     */

    @SerializedName("checkNumber")
    public String checkNumber;
}
