package com.weiju.ccmall.shared.manager;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.util.Log;

import com.tbruyelle.rxpermissions.RxPermissions;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.community.publish.FileUtils;
import com.weiju.ccmall.module.product.ProductQrcodeShowActivity;
import com.weiju.ccmall.shared.Constants;
import com.weiju.ccmall.shared.bean.UploadResponse;
import com.weiju.ccmall.shared.contracts.RequestListener;
import com.weiju.ccmall.shared.service.contract.IUploadService;
import com.weiju.ccmall.shared.util.PermissionsUtils;
import com.weiju.ccmall.shared.util.ToastUtil;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.PicassoEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.functions.Action1;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.manager
 * @since 2017-06-11
 */
public class UploadManager {

    public static void uploadImage(File file, RequestListener<UploadResponse> responseRequestListener,Activity activity) {
        RequestBody fileRequestBody = RequestBody.create(MediaType.parse("image"), file);
        MultipartBody.Part fileBody = MultipartBody.Part.createFormData("file", file.getName(), fileRequestBody);
        RequestBody versionRequestBody = RequestBody.create(MediaType.parse("multipart/form-data"), Constants.API_VERSION);
        IUploadService uploadService = ServiceManager.getInstance().createService(IUploadService.class);
        APIManager.startRequest(uploadService.uploadImage(fileBody, versionRequestBody), responseRequestListener,activity);
    }


    /**
     * 带压缩功能的上传图片
     *
     * @param activity
     * @param uri
     * @param responseRequestListener
     */
    public static void uploadImage(Activity activity, Uri uri, final RequestListener<UploadResponse> responseRequestListener) {
        File file = uri2File(uri, activity);
        Luban.with(activity)
                .load(file)                     //传人要压缩的图片
                .setCompressListener(new OnCompressListener() { //设置回调
                    @Override
                    public void onStart() {
                        responseRequestListener.onStart();
                        // 压缩开始前调用，可以在方法内启动 loading UI
                    }

                    @Override
                    public void onSuccess(File file) {
                        // 压缩成功后调用，返回压缩后的图片文件
                        uploadImage(file, responseRequestListener,activity);
                    }

                    @Override
                    public void onError(Throwable e) {
                        // 当压缩过程出现问题时调用
                        responseRequestListener.onError(e);
                    }
                }).launch();
    }

    public static Flowable<File> getLubanObservable(final Context context, File file) {
        return Flowable.just(file)
                .observeOn(Schedulers.io())
                .map(new Function<File, File>() {
                    @Override
                    public File apply(@NonNull File file) throws Exception {
                        // 同步方法直接返回压缩后的文件
                        return Luban.with(context).load(file).get();
                    }
                })
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static void selectImage(final Activity activity) {
        selectImage(activity, Config.REQUEST_CODE_CHOOSE_IMAGE_SELECT, 1);
    }

    public static void selectImage(final Activity activity, final int request, final int maxSelectable) {
        RxPermissions rxPermissions = new RxPermissions(activity);
        rxPermissions.request(
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        ).subscribe(new Action1<Boolean>() {
            @Override
            public void call(Boolean aBoolean) {
                if (aBoolean) {
                    Matisse.from(activity)
                            .choose(MimeType.ofAll())
                            .countable(true)
                            .maxSelectable(maxSelectable)
                            .imageEngine(new PicassoEngine())
                            .capture(true)
                            .captureStrategy(
                                    new CaptureStrategy(true, "com.weiju.ccmall.fileProvider")
                            )
                            .forResult(request);
                } else {
                    PermissionsUtils.goPermissionsSetting(activity);
                    ToastUtil.error("无法获得必要的权限");
                }
            }
        });
    }


    public static File uri2File(Uri uri, Activity activity) {
        try {
            File file = null;
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor actualimagecursor = activity.managedQuery(uri, proj, null,
                    null, null);
            int actual_image_column_index = actualimagecursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            actualimagecursor.moveToFirst();
            String img_path = actualimagecursor
                    .getString(actual_image_column_index);
            file = new File(img_path);
            return file;
        } catch (Exception e) {
            try {
                // 这段代码运行在主线程可能造成阻塞，but影响不大，因为图片一般比较小。
                final File savedImageFile = ProductQrcodeShowActivity.getSaveFile();
                InputStream is = MyApplication.getContext().getContentResolver().openInputStream(uri);
                com.blankj.utilcode.utils.FileUtils.writeFileFromIS(savedImageFile, is, false);
                return savedImageFile;
            } catch (Exception e1) {
            }
        }
        return null;
    }
}
