package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class OrderCount {
    @SerializedName("waitPay")
    public int waitPay;
    @SerializedName("waitShip")
    public int waitShip;
    @SerializedName("hasShip")
    public int hasShip;
    @SerializedName("hasComplete")
    public int hasComplete;
    @SerializedName("afterSales")
    public int afterSales;
    @SerializedName("waitComment")
    public int waitComment;
}
