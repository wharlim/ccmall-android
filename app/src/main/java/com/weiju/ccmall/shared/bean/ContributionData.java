package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/6/20 on 15:54
 * @desc ${TODD}
 */
public class ContributionData {

    /**
     * point : 0
     * createDate : 2019-06-19
     */

    @SerializedName("point")
    public Float point;
    @SerializedName("createDate")
    public String createDate;
}
