package com.weiju.ccmall.shared.manager;

import android.content.Context;

import com.bigkoo.pickerview.OptionsPickerView;
import com.weiju.ccmall.R;

public class OptionsPickerDialogManage {

    public static OptionsPickerView getOptionsDialog(Context context, OptionsPickerView.OnOptionsSelectListener listener) {
        int color = context.getResources().getColor(R.color.colorAccent);
        OptionsPickerView.Builder builder = new OptionsPickerView.Builder(context, listener);
        builder.setCancelColor(color).setSubmitColor(color);
        return builder.build();
    }


}
