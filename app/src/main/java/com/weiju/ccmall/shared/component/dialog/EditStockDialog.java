package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.PropertyValue;
import com.weiju.ccmall.shared.component.adapter.StockAdapter;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/11/7 on 10:36
 * @desc ${TODD}
 */
public class EditStockDialog extends Dialog {

    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;

    private StockAdapter mStockAdapter = new StockAdapter();
    private OnConfirmListener mOnConfirmListener;

    public EditStockDialog(Context context, OnConfirmListener onConfirmListener) {
        super(context, R.style.Theme_Light_Dialog);
        this.mOnConfirmListener = onConfirmListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_edit_stock);
        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mStockAdapter);
    }

    public void setPropertyValue(List<PropertyValue> list) {
        mStockAdapter.setNewData(list);
    }


    @OnClick(R.id.cancelBtn)
    protected void onClose(View view) {
        dismiss();
    }


    @OnClick(R.id.confirmBtn)
    protected void onConfirm(View view) {
        List<PropertyValue> data = mStockAdapter.getData();
        for (PropertyValue datum : data) {
            if (TextUtils.isEmpty(datum.stock)) {
                ToastUtil.error("请输入全部库存");
                return;
            }
        }
        mOnConfirmListener.onConfirm(data);
        dismiss();
    }

    public interface OnConfirmListener {
        void onConfirm(List<PropertyValue> list);
    }


}
