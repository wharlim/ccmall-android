package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class ExpressCompany {

    /**
     * comCode : kuaijiesudi
     * id :
     * noCount : 3639
     * noPre : 536257
     * startTime :
     */

    @SerializedName("comCode")
    public String comCode;
    @SerializedName("id")
    public String id;
    @SerializedName("noCount")
    public int noCount;
    @SerializedName("noPre")
    public String noPre;
    @SerializedName("startTime")
    public String startTime;
}
