package com.weiju.ccmall.shared.manager;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.blankj.utilcode.utils.ConvertUtils;
import com.weiju.ccmall.R;

public class PopupWindowManage {
    private static PopupWindowManage sInstance;
    private PopupWindow mPopupWindow;
    private Context mContext;
    private RecyclerView mRecyclerView;
    private View mYinYing;


    public PopupWindowDismissListener mPopupWindowDismissListener;

    public static PopupWindowManage getInstance(Context context) {
        sInstance = new PopupWindowManage(context);
        return sInstance;
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private PopupWindowManage(Context context) {
        mContext = context;
        initWindow();
    }

    private void initWindow() {
        mPopupWindow = new PopupWindow(mContext);
        mPopupWindow.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.bg_common_white_normal));
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setFocusable(true);
        mPopupWindow.setTouchable(true);
    }

    public void setBackgroundDrawable(int drawable) {
        mPopupWindow.setBackgroundDrawable(mContext.getResources().getDrawable(drawable));
    }

    public void showWindowNormal(final View anchor, View windowView, int xoff) {
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                if (null != mYinYing) {
                    mYinYing.setVisibility(View.GONE);
                }
                anchor.setSelected(false);
            }
        });
//        mPopupWindow.setWidth(anchor.getWidth());
        mPopupWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
        mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        mPopupWindow.setContentView(windowView);
        mPopupWindow.showAsDropDown(anchor, xoff, 0);
        anchor.setSelected(true);

        if (null != mYinYing) {
            mYinYing.setVisibility(View.VISIBLE);
            mYinYing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }

//        mPopupWindow.setFocusable(false);
    }

    public void showWindow(final View anchor, View windowView) {
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                if (null != mYinYing) {
                    mYinYing.setVisibility(View.GONE);
                }
                anchor.setSelected(false);
            }
        });
        mPopupWindow.setWidth(anchor.getWidth());
        mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        mPopupWindow.setContentView(windowView);
        mPopupWindow.showAsDropDown(anchor);
        anchor.setSelected(true);
      
        if (null != mYinYing) {
            mYinYing.setVisibility(View.VISIBLE);
            mYinYing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });
        }

//        mPopupWindow.setFocusable(false);
    }

    public void showListWindow(View anchor, RecyclerView.Adapter adapter) {
        if (mRecyclerView == null) {
            mRecyclerView = new RecyclerView(mContext);
            RecyclerView.LayoutParams layoutParams = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, 0, ConvertUtils.dp2px(200f));
            mRecyclerView.setLayoutParams(layoutParams);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        }
        mRecyclerView.setAdapter(adapter);
        showWindow(anchor, mRecyclerView);
    }

    public boolean isShowing() {
        return mPopupWindow.isShowing();
    }

    public void dismiss() {
        mPopupWindow.dismiss();
        if (mPopupWindowDismissListener != null) {
            mPopupWindowDismissListener.dismiss();
        }

    }

    public void setYinYing(View yinYing) {
        mYinYing = yinYing;
    }

    public void setOutsideTouchable(boolean outsideTouchable) {
        mPopupWindow.setOutsideTouchable(false);
        mPopupWindow.setFocusable(false);
    }

    public void setPopupWindowDismissListener(PopupWindowDismissListener listener) {
        mPopupWindowDismissListener = listener;
    }

    public interface PopupWindowDismissListener {
        void dismiss();
    }
}
