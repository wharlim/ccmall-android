package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.CartAmount;
import com.weiju.ccmall.shared.bean.CartStore;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service.contract
 * @since 2017-06-09
 */
public interface ICartService {

    @GET("cart/list")
    Observable<RequestResult<List<CartStore>>> getAllList();

    @GET("cart/getAccountList")
    Observable<RequestResult<List<CartStore>>> getListBySkuIds(@Query("skuIds") String skuIds);

    @FormUrlEncoded
    @POST("cart/operateCart")
    Observable<RequestResult<CartAmount>> updateCartItem(@Field("skuId") String skuId, @Field("quantity") int amount);

    @FormUrlEncoded
    @POST("cart/operateCartAdd")
    Observable<RequestResult<CartAmount>> addToCart(@Field("skuId") String skuId, @Field("quantity") int amount,
            @Field("liveId") String liveId);

    @FormUrlEncoded
    @POST("cart/removeSkuId")
    Observable<RequestResult<Object>> removeItem(@Field("skuIds") String skuIds);

    @GET("cart/getCartQuantity")
    Observable<RequestResult<CartAmount>> getTotal();
}
