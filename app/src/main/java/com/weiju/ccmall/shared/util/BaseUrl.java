package com.weiju.ccmall.shared.util;

import android.content.SharedPreferences;

import com.weiju.ccmall.BuildConfig;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.shared.manager.ServiceManager;

public class BaseUrl {


    private static class BaseUrlHolder {
        private static final BaseUrl INSTANCE = new BaseUrl();
    }

    private BaseUrl() {
    }

    public static final BaseUrl getInstance() {
        return BaseUrlHolder.INSTANCE;
    }


    /**
     * 网络环境类型
     * 0 公司测试环境
     * 1 外包测试环境
     * 2 正式环境
     * 3 后台局域网测试环境
     */
    private int networkType = 1;

//    private int buildType = 1;

    //公司测试环境
    public static final int CCMALL_TEST = 0;
    //外包测试环境
    public static final int CCMALL_WAIBAO_TEST = 1;
    //正式环境
    public static final int CCMALL_RELEASE_1 = 2;
    //公司局域网环境
    public static final int CCMALL_LOCAL = 3;
    //用户自定义类型
    public static final int CCMALL_CUSTOMIZE = 4;

    public static final int CCMALL_RELEASE_2 = 5;

    public static final int CCMALL_beta = 6;
    public static final int CCMALL_grayapi = 7;
    public static final int CCMALL_preapi = 8;

    //公司测试环境
//    private String testUrl = "http://219.135.171.167:8090/ccmapi/";
    private String testUrl = "http://47.98.237.215:8085/ccmapi/";
//    private String testUrl = "http://preapi.create-chain.net/ccmapi/";
//    private String testUrl =  "https://testapi.weijuit.com/ccmapi/";

    //外包测试环境
    private String waibaoUrl = "https://testapi.weijuit.com/ccmapi/";
    //正式环境
    private String releaseUrl = "https://api.create-chain.net/ccmapi/";
    //公司局域网环境
    private String localUrl = "http://192.168.254.214:8085/ccmapi/";
    // beta环境
//    private String betaUrl = "http://betaapi.create-chain.net/ccmapi/";
    private String betaUrl = "http://betaapi.create-chain.net:8085/ccmapi/";    //内网
    //    private String betaUrl = "http://47.98.237.215:7085/ccmapi/"; //外网
    // gray环境
    private String grayUrl = "https://grayapi.create-chain.net/ccmapi/";
    // pre环境
//    private String preUrl = "http://preapi.create-chain.net/ccmapi/";
    private String preUrl = "http://preapi.create-chain.net:8085/ccmapi/";  //内网
//    private String preUrl = "http://47.98.237.215:8085/ccmapi/";//外网

    public static final String NETWORK_TYPE = "NETWORK_TYPE";


    private static String baseUrl = "https://testapi.weijuit.com/ccmapi/";


    public void setBaseUrl(String url) {
        networkType = CCMALL_CUSTOMIZE;
        MyApplication.getContext().getSharedPreferences("CUSTOMIZE_URL", 0).edit().putString("CUSTOMIZE_URL", url).commit();
        baseUrl = url;
    }

    public String getBaseUrl() {
        int networkTypeSp = -1;
        if ("release".equals(BuildConfig.BUILD_TYPE)) {
            SharedPreferences sharedPreferences = MyApplication.getContext().getSharedPreferences(NETWORK_TYPE, 0);
            sharedPreferences.edit().putInt(NETWORK_TYPE, CCMALL_RELEASE_1).commit();
            networkTypeSp = CCMALL_RELEASE_1;
        } else {
            networkTypeSp = MyApplication.getInstance().getSharedPreferences(NETWORK_TYPE, 0).getInt(NETWORK_TYPE, -1);
            if (CCMALL_RELEASE_1 == networkTypeSp) {
                SharedPreferences sharedPreferences = MyApplication.getContext().getSharedPreferences(NETWORK_TYPE, 0);
                sharedPreferences.edit().putInt(NETWORK_TYPE, CCMALL_TEST).commit();
                networkTypeSp = CCMALL_TEST;
            }
        }

        switch (networkTypeSp != -1 ? networkTypeSp : getNetworkType()) {
            case CCMALL_TEST:
                baseUrl = testUrl;
                break;
            case CCMALL_WAIBAO_TEST:
                baseUrl = waibaoUrl;
                break;
            case CCMALL_RELEASE_1:
                baseUrl = releaseUrl;
                break;
            case CCMALL_LOCAL:
                baseUrl = localUrl;
                break;
            case CCMALL_beta:
                baseUrl = betaUrl;
                break;
            case CCMALL_grayapi:
                baseUrl = grayUrl;
                break;
            case CCMALL_preapi:
                baseUrl = preUrl;
                break;
            case CCMALL_CUSTOMIZE:
                baseUrl = MyApplication.getInstance().getSharedPreferences("CUSTOMIZE_URL", 0).getString("CUSTOMIZE_URL", "");
                break;
            case CCMALL_RELEASE_2:
                baseUrl = releaseUrl;
                break;
            default:
                break;
        }

        return baseUrl;
    }

    public void setNetWorkType(int newNetWorkType) {
        networkType = newNetWorkType;
        if (CCMALL_RELEASE_1 == newNetWorkType) {
            if ("release".equals(BuildConfig.BUILD_TYPE)) {
                SharedPreferences sharedPreferences = MyApplication.getInstance().getSharedPreferences(NETWORK_TYPE, 0);
                sharedPreferences.edit().putInt(NETWORK_TYPE, networkType).commit();
                return;
            } else {
                SharedPreferences sharedPreferences = MyApplication.getInstance().getSharedPreferences(NETWORK_TYPE, 0);
                sharedPreferences.edit().putInt(NETWORK_TYPE, CCMALL_RELEASE_2).commit();
                return;
            }
        }
        SharedPreferences sharedPreferences = MyApplication.getInstance().getSharedPreferences(NETWORK_TYPE, 0);
        sharedPreferences.edit().putInt(NETWORK_TYPE, networkType).commit();
        ServiceManager.getInstance().baseUrlManagerInterceptor.setNewBaseUrl(getBaseUrl());
        ServiceManager.getInstance().reInitRetrofit2();
    }

    public int getNetworkType() {
        return networkType;
    }

    public String getNetworkTypeString() {
        int networkTypeSp = MyApplication.getInstance().getSharedPreferences(NETWORK_TYPE, 0).getInt(NETWORK_TYPE, -1);
        String netWorkTypeString = "";
        switch (networkTypeSp != -1 ? networkTypeSp : getNetworkType()) {
            case CCMALL_TEST:
                netWorkTypeString = "pre环境（外网）";
                break;
            case CCMALL_RELEASE_1:
            case CCMALL_RELEASE_2:
                netWorkTypeString = "正式环境";
                break;
            case CCMALL_WAIBAO_TEST:
                netWorkTypeString = "testapi.weijuit.com";
                break;
            case CCMALL_beta:
                netWorkTypeString = "beta环境";
                break;
            case CCMALL_grayapi:
                netWorkTypeString = "gray环境";
                break;
            case CCMALL_preapi:
                netWorkTypeString = "pre环境";
                break;
            case CCMALL_CUSTOMIZE:
                netWorkTypeString = MyApplication.getInstance().getSharedPreferences("CUSTOMIZE_URL", 0).getString("CUSTOMIZE_URL", "");
                break;
            default:
                break;

        }
        return netWorkTypeString;
    }


    public String getCCPayHostPost() {
        int networkTypeSp = MyApplication.getInstance().getSharedPreferences(NETWORK_TYPE, 0).getInt(NETWORK_TYPE, -1);
        switch (networkTypeSp != -1 ? networkTypeSp : getNetworkType()) {
            case CCMALL_beta:
                return "betaccpay.create-chain.net";
            case CCMALL_preapi:
                return "preccpay.create-chain.net";
            case CCMALL_RELEASE_1:
            case CCMALL_RELEASE_2:
                return "credit.ccmallv2.create-chain.net";
        }
        return "preccpay.create-chain.net"; // 默认采用测试环境
    }
}
