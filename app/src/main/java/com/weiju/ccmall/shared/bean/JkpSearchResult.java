package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JkpSearchResult {

    /**
     * error : 0
     * msg : 查询成功！
     * search_type : 1
     * is_similar : 0
     * total_results : 7568460
     * result_list : [{"category_id":50009838,"category_name":"大米","commission_rate":"200","coupon_id":"","coupon_info":"","coupon_remain_count":0,"coupon_total_count":0,"item_description":"拍1发2袋","item_id":612420483070,"item_url":"https://detail.tmall.com/item.htm?id=612420483070","level_one_category_id":50016422,"level_one_category_name":"粮油米面/南北干货/调味品","nick":"袁米旗舰店","num_iid":612420483070,"pict_url":"https://img.alicdn.com/bao/uploaded/i4/2206449959356/O1CN010aOMYa2Iz6AxO6vKD_!!0-item_pic.jpg","provcity":"吉林 松原","real_post_fee":"0.00","reserve_price":"199","seller_id":2206449959356,"shop_dsr":49421,"shop_title":"袁米旗舰店","short_title":"","small_images":["https://img.alicdn.com/i2/2206449959356/O1CN01l0gETt2Iz6AxQFqwp_!!2206449959356.jpg","https://img.alicdn.com/i3/2206449959356/O1CN01weIYpP2Iz6Awj7Pnr_!!2206449959356.jpg","https://img.alicdn.com/i4/2206449959356/O1CN01U8v4SE2Iz6Awj9UcZ_!!2206449959356.jpg","https://img.alicdn.com/i2/2206449959356/O1CN01YqnSXs2Iz6AuyGNjY_!!2206449959356.jpg"],"title":"预售3.10号发货薇娅推荐 袁米海水稻严选大米5kg农家大米香米10斤","tk_total_commi":"0","tk_total_sales":"0","user_type":1,"volume":94625,"white_image":"","zk_final_price":"119.9"},{"category_id":50050366,"category_name":"手机数据线","commission_rate":"3000","coupon_amount":"5","coupon_end_time":"2020-02-26","coupon_id":"ade0ff902a8340cab0656950d660feb3","coupon_info":"满18元减5元","coupon_remain_count":94000,"coupon_start_fee":"18","coupon_start_time":"2020-02-12","coupon_total_count":100000,"item_description":"安全快充 稳定传输 不发烫 365天质保换新","item_id":535520474828,"item_url":"https://detail.tmall.com/item.htm?id=535520474828","level_one_category_id":50008090,"level_one_category_name":"3C数码配件","nick":"品胜数码旗舰店","num_iid":535520474828,"pict_url":"https://img.alicdn.com/bao/uploaded/i1/467098491/O1CN01q0kmNZ2CavZzWq46H_!!467098491-0-pixelsss.jpg","provcity":"广东 深圳","real_post_fee":"0.00","reserve_price":"23.9","seller_id":467098491,"shop_dsr":48462,"shop_title":"品胜数码旗舰店","short_title":"品胜iphone6数据线6s苹果器11 ipad","small_images":["https://img.alicdn.com/i3/467098491/O1CN015dQh9M2CavYFJNp31_!!467098491.jpg","https://img.alicdn.com/i4/467098491/O1CN01Vzg9Yz2CavY2lXVjy_!!467098491.jpg","https://img.alicdn.com/i2/467098491/O1CN01omaumz2CavY2aCsXo_!!467098491.jpg","https://img.alicdn.com/i2/467098491/O1CN01xbxyop2CavYFJGVB3_!!467098491.jpg"],"title":"品胜iPhone6数据线6s苹果7plus充电线器11手机XS快充X加长XR单头8p短iPad闪充ios冲电Max旗舰店iphonex正品5s","tk_total_commi":"338184","tk_total_sales":"45959","user_type":1,"volume":94261,"white_image":"https://img.alicdn.com/bao/uploaded/TB1Gdt1aEuF3KVjSZK9XXbVtXXa.png","zk_final_price":"18"},{"category_id":50016845,"category_name":"方便面/拉面/面皮","commission_rate":"200","coupon_id":"","coupon_info":"","coupon_remain_count":0,"coupon_total_count":0,"item_description":"好面汤决定 享受喝汤 一碗见底","item_id":20460056220,"item_url":"https://detail.tmall.com/item.htm?id=20460056220","level_one_category_id":50016422,"level_one_category_name":"粮油米面/南北干货/调味品","nick":"天猫超市","num_iid":20460056220,"pict_url":"https://img.alicdn.com/bao/uploaded/i1/725677994/O1CN01OilKQN28vIjbNhPq9_!!0-item_pic.jpg","provcity":"上海","real_post_fee":"5.00","reserve_price":"45","seller_id":725677994,"shop_dsr":49061,"shop_title":"天猫超市","short_title":"统一汤达人日式豚骨125g*5袋拉面","small_images":["https://img.alicdn.com/i4/725677994/O1CN01T7JIZ028vIjVPbRjL_!!725677994.jpg","https://img.alicdn.com/i1/725677994/TB2_.JBnhPI8KJjSspfXXcCFXXa_!!725677994.jpg","https://img.alicdn.com/i2/725677994/O1CN01iBt3Sg28vIdrHHh4e_!!725677994.jpg","https://img.alicdn.com/i4/725677994/TB2NTwUmZjI8KJjSsppXXXbyVXa_!!725677994.jpg"],"title":"统一汤达人日式豚骨拉面125g*5袋方便面泡面","tk_total_commi":"6286.07","tk_total_sales":"12880","user_type":1,"volume":90411,"white_image":"https://img.alicdn.com/bao/uploaded/TB2UUY0XrAaBuNjt_igXXb5ApXa_!!725677994.png","zk_final_price":"45"},{"category_id":201230507,"category_name":"方便粉丝/米线/螺蛳粉","commission_rate":"200","coupon_amount":"30","coupon_end_time":"2020-02-18","coupon_id":"d60c4207c8564fec86c9ab420e76f73f","coupon_info":"满59元减30元","coupon_remain_count":54000,"coupon_start_fee":"59","coupon_start_time":"2020-02-12","coupon_total_count":100000,"item_description":"地道柳州螺蛳粉 安全放心 远离小作坊","item_id":549906105947,"item_url":"https://detail.tmall.com/item.htm?id=549906105947","level_one_category_id":50016422,"level_one_category_name":"粮油米面/南北干货/调味品","nick":"柳江人家旗舰店","num_iid":549906105947,"pict_url":"https://img.alicdn.com/bao/uploaded/i3/3254991622/O1CN01W2OFDH1Nqv01nnB6Z_!!0-item_pic.jpg","provcity":"广西 柳州","real_post_fee":"0.00","reserve_price":"98","seller_id":3254991622,"shop_dsr":48913,"shop_title":"柳江人家旗舰店","short_title":"柳江人家广西正宗柳州螺蛳粉5包装","small_images":["https://img.alicdn.com/i3/3254991622/O1CN01UGFA0C1NquxzBPT9e_!!3254991622.jpg","https://img.alicdn.com/i1/3254991622/TB2QWVzfuuSBuNjy1XcXXcYjFXa_!!3254991622.jpg","https://img.alicdn.com/i3/3254991622/TB2ZKFCfxSYBuNjSspjXXX73VXa_!!3254991622.jpg","https://img.alicdn.com/i3/3254991622/O1CN01WmPJnk1Nquvw3mQS2_!!3254991622.jpg"],"title":"柳江人家广西正宗柳州螺蛳粉整箱5包装原味螺狮粉特产螺丝粉速食","tk_total_commi":"80343.5","tk_total_sales":"49334","user_type":1,"volume":89703,"white_image":"https://img.alicdn.com/bao/uploaded/TB1RFVVU8LoK1RjSZFuXXXn0XXa.png","zk_final_price":"59.8"},{"category_id":50009839,"category_name":"面粉/食用粉","commission_rate":"200","coupon_id":"","coupon_info":"","coupon_remain_count":0,"coupon_total_count":0,"item_description":"家庭通用  无食品添加剂  优质原粮制作","item_id":557054356640,"item_url":"https://detail.tmall.com/item.htm?id=557054356640","level_one_category_id":50016422,"level_one_category_name":"粮油米面/南北干货/调味品","nick":"天猫超市","num_iid":557054356640,"pict_url":"https://img.alicdn.com/bao/uploaded/i2/725677994/O1CN01TRiMJG28vIjXEF440_!!725677994-0-sm.jpg","provcity":"上海","real_post_fee":"5.00","reserve_price":"59.9","seller_id":725677994,"shop_dsr":49061,"shop_title":"天猫超市","short_title":"金沙河雪花中筋家用白面馒头面粉","small_images":["https://img.alicdn.com/i1/725677994/O1CN01GoEqyw28vIjSl5JYA_!!725677994.jpg","https://img.alicdn.com/i3/725677994/TB2u4apgCtYBeNjSspkXXbU8VXa_!!725677994.jpg","https://img.alicdn.com/i3/725677994/O1CN01B6jIlO28vIirzb87A_!!725677994.jpg","https://img.alicdn.com/i4/725677994/O1CN01JuQqlS28vIiwUTI73_!!725677994.jpg"],"title":"金沙河通用雪花小麦粉中筋家用面粉白面5KG面点面食油条 馒头面粉","tk_total_commi":"7291.02","tk_total_sales":"15815","user_type":1,"volume":86270,"white_image":"https://img.alicdn.com/bao/uploaded/TB2wyQ.gwoQMeJjy0FoXXcShVXa_!!725677994.png","zk_final_price":"19.9"},{"category_id":50016845,"category_name":"方便面/拉面/面皮","commission_rate":"200","coupon_id":"","coupon_info":"","coupon_remain_count":0,"coupon_total_count":0,"item_description":"这酸爽才正宗 年23亿人次 酸爽过瘾","item_id":12426175516,"item_url":"https://detail.tmall.com/item.htm?id=12426175516","level_one_category_id":50016422,"level_one_category_name":"粮油米面/南北干货/调味品","nick":"天猫超市","num_iid":12426175516,"pict_url":"https://img.alicdn.com/bao/uploaded/i3/725677994/O1CN01633fq928vIjXkXw6J_!!0-item_pic.jpg","provcity":"上海","real_post_fee":"5.00","reserve_price":"12.5","seller_id":725677994,"shop_dsr":49061,"shop_title":"天猫超市","short_title":"统一100老坛酸菜121g*5包牛肉面","small_images":["https://img.alicdn.com/i2/725677994/O1CN01arb58G28vIinkvIih_!!725677994.jpg","https://img.alicdn.com/i2/725677994/TB2sq7Lm0rJ8KJjSspaXXXuKpXa_!!725677994.jpg","https://img.alicdn.com/i2/725677994/TB2X3lQnh6I8KJjy0FgXXXXzVXa_!!725677994.jpg","https://img.alicdn.com/i1/725677994/TB2oMtQnh6I8KJjy0FgXXXXzVXa_!!725677994.jpg"],"title":"统一100 老坛酸菜牛肉面121g*5包 代餐方便面泡面速食面夜宵","tk_total_commi":"2965.75","tk_total_sales":"12070","user_type":1,"volume":83227,"white_image":"https://img.alicdn.com/bao/uploaded/TB28d8XcYZnBKNjSZFGXXbt3FXa_!!725677994.png","zk_final_price":"12.5"},{"category_id":50050727,"category_name":"芒果","commission_rate":"250","coupon_id":"","coupon_info":"","coupon_remain_count":0,"coupon_total_count":0,"item_description":"收藏下单 拍下27.9元 精选中大果 爆甜多汁~","item_id":612084422308,"item_url":"https://detail.tmall.com/item.htm?id=612084422308","level_one_category_id":50050359,"level_one_category_name":"水产肉类/新鲜蔬果/熟食","nick":"甘福园旗舰店","num_iid":612084422308,"pict_url":"https://img.alicdn.com/bao/uploaded/i2/3015214310/O1CN01xzToIE1hi1cyhEevo_!!0-item_pic.jpg","provcity":"海南 三亚","real_post_fee":"0.00","reserve_price":"138","seller_id":3015214310,"shop_dsr":46100,"shop_title":"甘福园旗舰店","short_title":"【甘福园】海南贵妃5斤新鲜当季芒果","small_images":["https://img.alicdn.com/i3/3015214310/O1CN01na8EaC1hi1d1pSew3_!!3015214310.jpg","https://img.alicdn.com/i3/3015214310/O1CN01qItqAU1hi1czePJ8a_!!3015214310.jpg","https://img.alicdn.com/i1/3015214310/O1CN01TStCZa1hi1cyBDywU_!!3015214310.jpg","https://img.alicdn.com/i3/3015214310/O1CN01G9NS3D1hi1d1pUnzA_!!3015214310.jpg"],"title":"【甘福园】海南贵妃芒果5斤新鲜当季水果小红金龙整箱10批发包邮","tk_total_commi":"0","tk_total_sales":"0","user_type":1,"volume":82786,"white_image":"https://img.alicdn.com/bao/uploaded/O1CN01WRQgwa1hi1cxKrHef_!!2-item_pic.png","zk_final_price":"29.9"},{"category_id":50150001,"category_name":"草莓","commission_rate":"250","coupon_id":"","coupon_info":"","coupon_remain_count":0,"coupon_total_count":0,"item_description":"精选大果 纯甜多汁 现摘现发 顺丰包邮！","item_id":612287295603,"item_url":"https://detail.tmall.com/item.htm?id=612287295603","level_one_category_id":50050359,"level_one_category_name":"水产肉类/新鲜蔬果/熟食","nick":"甘福园旗舰店","num_iid":612287295603,"pict_url":"https://img.alicdn.com/bao/uploaded/i4/3015214310/O1CN01I2pwpK1hi1d2Y7oRm_!!0-item_pic.jpg","provcity":"辽宁 丹东","real_post_fee":"0.00","reserve_price":"158","seller_id":3015214310,"shop_dsr":46100,"shop_title":"甘福园旗舰店","short_title":"甘福园丹东99草莓3斤新鲜当季牛奶","small_images":["https://img.alicdn.com/i1/3015214310/O1CN01Fr2eXr1hi1czY5ePb_!!3015214310.jpg","https://img.alicdn.com/i2/3015214310/O1CN01Mr3GXt1hi1cxMNoUu_!!3015214310.jpg","https://img.alicdn.com/i2/3015214310/O1CN01Sv8Kfc1hi1cxtG9aN_!!3015214310.jpg","https://img.alicdn.com/i1/3015214310/O1CN01HERNeS1hi1d0LbROK_!!3015214310.jpg"],"title":"甘福园 丹东99牛奶草莓3斤新鲜当季水果四季奶油红颜大草莓5包邮","tk_total_commi":"941.6","tk_total_sales":"793","user_type":1,"volume":79260,"white_image":"https://img.alicdn.com/bao/uploaded/O1CN01036j8o1hi1cxX2k6e_!!2-item_pic.png","zk_final_price":"79.9"},{"category_id":50012587,"category_name":"手机贴膜","commission_rate":"130","coupon_id":"","coupon_info":"","coupon_remain_count":0,"coupon_total_count":0,"item_description":"全玻璃 高清高透 强抗指纹 无白边不碎边","item_id":542177447482,"item_url":"https://detail.tmall.com/item.htm?id=542177447482","level_one_category_id":50008090,"level_one_category_name":"3C数码配件","nick":"柏奈儿旗舰店","num_iid":542177447482,"pict_url":"https://img.alicdn.com/bao/uploaded/i3/2326583143/O1CN01cRRyX01Z5XPzBqOtK_!!0-item_pic.jpg","provcity":"广东 深圳","real_post_fee":"0.00","reserve_price":"12","seller_id":2326583143,"shop_dsr":48464,"shop_title":"柏奈儿旗舰店","short_title":"华为mate30钢化p30p20 nova5pro贴膜","small_images":["https://img.alicdn.com/i2/2326583143/O1CN01FCOj9z1Z5XLEDTZP5_!!2326583143.jpg","https://img.alicdn.com/i1/2326583143/O1CN01arKtoi1Z5XJleVFVj_!!2326583143.jpg","https://img.alicdn.com/i2/2326583143/TB2k7_rGQSWBuNjSszdXXbeSpXa_!!2326583143.jpg","https://img.alicdn.com/i1/2326583143/O1CN01biHacd1Z5XJzQOyAX_!!2326583143.jpg"],"title":"华为mate30钢化膜p30p20贴膜nova5pro荣耀10v10v20全屏8x青春版20s十mate20曲面9i手机play水凝5i/3i/2s覆盖4","tk_total_commi":"28181.6","tk_total_sales":"27448","user_type":1,"volume":76457,"white_image":"https://img.alicdn.com/bao/uploaded/TB1prSoX8iE3KVjSZFMXXbQhVXa.png","zk_final_price":"10.8"},{"category_id":50280002,"category_name":"橙","commission_rate":"500","coupon_amount":"20","coupon_end_time":"2020-02-29","coupon_id":"87d661ddc0dd469d80fdcbcd999db04e","coupon_info":"满29元减20元","coupon_remain_count":93466,"coupon_start_fee":"29","coupon_start_time":"2020-02-09","coupon_total_count":100000,"item_description":"春节不打烊，点击详情领取15元优惠卷","item_id":611207111711,"item_url":"https://detail.tmall.com/item.htm?id=611207111711","level_one_category_id":50050359,"level_one_category_name":"水产肉类/新鲜蔬果/熟食","nick":"桃花会旗舰店","num_iid":611207111711,"pict_url":"https://img.alicdn.com/bao/uploaded/i1/2207307537739/O1CN01Mgwej5272Vjloz7AA_!!0-item_pic.jpg","provcity":"四川 内江","real_post_fee":"0.00","reserve_price":"54.9","seller_id":2207307537739,"shop_dsr":46521,"shop_title":"桃花会旗舰店","short_title":"比爱暖38维c多塔罗科当季新鲜血橙","small_images":["https://img.alicdn.com/i3/2207307537739/O1CN01LoeL3Z272VjhfERit_!!2207307537739.jpg","https://img.alicdn.com/i4/2207307537739/O1CN01UIpIxg272Vjb4xcob_!!2207307537739.jpg","https://img.alicdn.com/i1/2207307537739/O1CN01yYKaaA272VjYXJF2w_!!2207307537739.jpg","https://img.alicdn.com/i3/2207307537739/O1CN01W18Qwt272Vjb83EHU_!!2207307537739.jpg"],"title":"比爱暖38维c多塔罗科血橙当季水果新鲜包邮5斤/10斤红橙应季水果","tk_total_commi":"198376","tk_total_sales":"65733","user_type":1,"volume":74715,"white_image":"","zk_final_price":"54.9"},{"category_id":50010817,"category_name":"化妆/美容工具","commission_rate":"400","coupon_id":"","coupon_info":"","coupon_remain_count":0,"coupon_total_count":0,"item_description":"李佳琦热销推荐，初学者修眉刀5把装","item_id":585093718782,"item_url":"https://detail.tmall.com/item.htm?id=585093718782","level_one_category_id":50010788,"level_one_category_name":"彩妆/香水/美妆工具","nick":"天猫超市","num_iid":585093718782,"pict_url":"https://img.alicdn.com/bao/uploaded/i1/725677994/O1CN01yLi1Fj28vIjcOEcck_!!0-item_pic.jpg","provcity":"上海","real_post_fee":"5.00","reserve_price":"48","seller_id":725677994,"shop_dsr":49061,"shop_title":"天猫超市","short_title":"kai /贝印日本修眉刀女用套装刀片","small_images":["https://img.alicdn.com/i3/725677994/O1CN01YEPLzK28vIjYLaTa2_!!725677994.jpg","https://img.alicdn.com/i3/725677994/O1CN01gbLIgC28vIg0yztoi_!!725677994.jpg","https://img.alicdn.com/i1/725677994/O1CN01HDENdI28vIfx9D4Xn_!!725677994.jpg","https://img.alicdn.com/i3/725677994/O1CN01JbKK3328vIh9gS0XW_!!725677994.jpg"],"title":"kai/贝印日本修眉刀刮眉刀女用初学者套装进口刀片修眉工具5把","tk_total_commi":"6699.5","tk_total_sales":"18916","user_type":1,"volume":72325,"white_image":"https://img.alicdn.com/bao/uploaded/O1CN01YGuFks28vIgGf6uL0_!!2-item_pic.png","zk_final_price":"21.8"},{"category_id":50012546,"category_name":"湿巾","commission_rate":"2000","coupon_amount":"3","coupon_end_time":"2020-02-16","coupon_id":"5e5d3b5c73144d10a25e51567843d268","coupon_info":"满18元减3元","coupon_remain_count":99000,"coupon_start_fee":"18","coupon_start_time":"2020-02-13","coupon_total_count":100000,"item_description":"第二件半价 现货即发","item_id":559680679226,"item_url":"https://detail.tmall.com/item.htm?id=559680679226","level_one_category_id":50014812,"level_one_category_name":"婴童用品","nick":"碧c奥顿专卖店","num_iid":559680679226,"pict_url":"https://img.alicdn.com/bao/uploaded/i1/3087060159/O1CN0102BxDn1D2rYluzidt_!!0-item_pic.jpg","provcity":"浙江 金华","real_post_fee":"0.00","reserve_price":"64","seller_id":3087060159,"shop_dsr":48615,"shop_title":"碧c奥顿专卖店","short_title":"碧c湿巾纸巾婴幼儿 80抽5包大包装","small_images":["https://img.alicdn.com/i1/3087060159/O1CN01GnLsxu1D2rVLToFBi_!!3087060159.jpg","https://img.alicdn.com/i2/TB15019jh6I8KJjSszfYXGZVXXa_M2.SS2","https://img.alicdn.com/i2/TB1abjzjDnI8KJjSszbYXH4KFXa_M2.SS2","https://img.alicdn.com/i2/TB1IcPajnnI8KJjSszgYXI8ApXa_M2.SS2"],"title":"碧c婴儿湿巾纸巾婴幼儿新生宝宝手口屁专用80抽5包大包装特价家用","tk_total_commi":"343448","tk_total_sales":"158344","user_type":1,"volume":70313,"white_image":"https://img.alicdn.com/bao/uploaded/O1CN01SYhEFV1D2rVLItrUT_!!2-item_pic.png","zk_final_price":"18.98"},{"category_id":50018976,"category_name":"洗衣粉","commission_rate":"3000","coupon_amount":"10","coupon_end_time":"2020-02-19","coupon_id":"450d5c5374b0443cb88fa4c158f17973","coupon_info":"满25元减10元","coupon_remain_count":99000,"coupon_start_fee":"25","coupon_start_time":"2020-02-13","coupon_total_count":100000,"item_description":"天然皂粉 深层洁净 不含荧光剂 母婴可用","item_id":591903517605,"item_url":"https://detail.tmall.com/item.htm?id=591903517605","level_one_category_id":50025705,"level_one_category_name":"洗护清洁剂/卫生巾/纸/香薰","nick":"炫衣彩旗舰店","num_iid":591903517605,"pict_url":"https://img.alicdn.com/bao/uploaded/i1/2200780559224/O1CN01DfLnlb2I0dtUdNz2x_!!0-item_pic.jpg","provcity":"河北 石家庄","real_post_fee":"0.00","reserve_price":"25.9","seller_id":2200780559224,"shop_dsr":48577,"shop_title":"炫衣彩旗舰店","short_title":"炫衣彩5斤装椰油促销家庭装皂粉","small_images":["https://img.alicdn.com/i4/2200780559224/O1CN01ziOTle2I0dpPr1HWs_!!0-item_pic.jpg","https://img.alicdn.com/i1/2200780559224/O1CN01SszZWc2I0dpPhMdGt_!!2200780559224.jpg","https://img.alicdn.com/i4/2200780559224/O1CN01HP2XkZ2I0dpPhNZUW_!!2200780559224.jpg","https://img.alicdn.com/i1/2200780559224/O1CN01DYCO292I0dpQDKVo2_!!2200780559224.jpg"],"title":"炫衣彩5斤装椰油皂粉促销家庭装无磷留香深层洁净洗衣粉2.5kg批发","tk_total_commi":"253108","tk_total_sales":"63614","user_type":1,"volume":68967,"white_image":"https://img.alicdn.com/bao/uploaded/O1CN0194agOw2I0dpNehNHR_!!2-item_pic.png","zk_final_price":"25.9"},{"category_id":50012546,"category_name":"湿巾","commission_rate":"2000","coupon_id":"","coupon_info":"","coupon_remain_count":0,"coupon_total_count":0,"item_description":"不连抽、无酒精、无香味、无荧光剂、不掉毛","item_id":583668859654,"item_url":"https://detail.tmall.com/item.htm?id=583668859654","level_one_category_id":50014812,"level_one_category_name":"婴童用品","nick":"棉嫂旗舰店","num_iid":583668859654,"pict_url":"https://img.alicdn.com/bao/uploaded/i2/2455564892/O1CN01gG92ZL1m0a8VOPt0s_!!0-item_pic.jpg","provcity":"山东 济宁","real_post_fee":"0.00","reserve_price":"39.9","seller_id":2455564892,"shop_dsr":48211,"shop_title":"棉嫂旗舰店","short_title":"棉嫂湿巾纸巾手口屁幼儿5大包装","small_images":["https://img.alicdn.com/i2/2455564892/O1CN01SXgfwW1m0a72kqaUp_!!2455564892.jpg","https://img.alicdn.com/i1/2455564892/O1CN01XzrWlO1m0a6WnQ0cZ_!!2455564892.jpg","https://img.alicdn.com/i3/2455564892/O1CN019clYab1m0a6VQsPkM_!!2455564892.jpg","https://img.alicdn.com/i1/2455564892/O1CN01Gtx66w1m0a2Km1EwO_!!2455564892.jpg"],"title":"棉嫂湿巾纸巾婴儿手口屁专用幼儿新生儿宝宝80抽5大包装特价家用","tk_total_commi":"3234.63","tk_total_sales":"6673","user_type":1,"volume":66368,"white_image":"https://img.alicdn.com/bao/uploaded/O1CN01rnPEFC1m0a8YrxHXc_!!2-item_pic.png","zk_final_price":"14.9"},{"category_id":123188007,"category_name":"百香果","commission_rate":"250","coupon_id":"","coupon_info":"","coupon_remain_count":0,"coupon_total_count":0,"item_description":"果园现摘 精选红果 有坏包赔 收藏送开果器","item_id":544684932914,"item_url":"https://detail.tmall.com/item.htm?id=544684932914","level_one_category_id":50050359,"level_one_category_name":"水产肉类/新鲜蔬果/熟食","nick":"梓芸农业旗舰店","num_iid":544684932914,"pict_url":"https://img.alicdn.com/bao/uploaded/i1/2930289273/O1CN0153coBT2IN5IoQB94g_!!0-item_pic.jpg","provcity":"云南 德宏","real_post_fee":"0.00","reserve_price":"24.8","seller_id":2930289273,"shop_dsr":47708,"shop_title":"梓芸农业旗舰店","short_title":"","small_images":["https://img.alicdn.com/i3/2930289273/O1CN01lb2WwL2IN5Ir16cKz_!!2930289273.jpg","https://img.alicdn.com/i4/2930289273/O1CN01bZDs2v2IN5ITpWyHc_!!0-item_pic.jpg","https://img.alicdn.com/i2/2930289273/O1CN01HgW20t2IN5Ck7KWar_!!2930289273.jpg","https://img.alicdn.com/i4/2930289273/TB2kSg4aYZnBKNjSZFhXXc.oXXa_!!2930289273.png"],"title":"广西新鲜现摘百香果大果5斤水果西番莲鸡蛋果10精装红果当季整箱","tk_total_commi":"27025.8","tk_total_sales":"9061","user_type":1,"volume":65067,"white_image":"","zk_final_price":"13.8"}]
     */

    @SerializedName("error")
    public String error;
    @SerializedName("msg")
    public String msg;
    @SerializedName("search_type")
    public String searchType;
    @SerializedName("is_similar")
    public String isSimilar;
    @SerializedName("total_results")
    public int totalResults;
    @SerializedName("result_list")
    public List<ResultListBean> resultList;

    @SerializedName("data")
    public ResultListBean data;

    public List<SkuInfo> getSkuProducts() {
        ArrayList<SkuInfo> ret = new ArrayList<>();
        for (ResultListBean item: resultList) {
            ret.add(item.toSkuInfo());
        }
        return ret;
    }

    public static class ResultListBean {

        public SkuInfo toSkuInfo() {
            SkuInfo skuInfo = new SkuInfo();
            skuInfo.originId = numIid;
            skuInfo.name = title;
            skuInfo.marketPrice = (long)(zkFinalPrice * 100);
            skuInfo.retailPrice = (long)((zkFinalPrice - couponAmount) * 100);
            skuInfo.costPrice = (long)(couponAmount * 100);
            skuInfo.thumb = pictUrl;
            return skuInfo;
        }

        public void parseCouponAmountFromCouponInfo() {
            try {
                String reg = "满(\\d+)元减(\\d+)元";
                Pattern pattern = Pattern.compile(reg);
                Matcher matcher = pattern.matcher(couponInfo);
                if (matcher.find()) {
                    couponAmount = Integer.parseInt(matcher.group(2));
                }
            } catch (Exception e) {
            }
        }
        /**
         * category_id : 50009838
         * category_name : 大米
         * commission_rate : 200
         * coupon_id :
         * coupon_info :
         * coupon_remain_count : 0
         * coupon_total_count : 0
         * item_description : 拍1发2袋
         * item_id : 612420483070
         * item_url : https://detail.tmall.com/item.htm?id=612420483070
         * level_one_category_id : 50016422
         * level_one_category_name : 粮油米面/南北干货/调味品
         * nick : 袁米旗舰店
         * num_iid : 612420483070
         * pict_url : https://img.alicdn.com/bao/uploaded/i4/2206449959356/O1CN010aOMYa2Iz6AxO6vKD_!!0-item_pic.jpg
         * provcity : 吉林 松原
         * real_post_fee : 0.00
         * reserve_price : 199
         * seller_id : 2206449959356
         * shop_dsr : 49421
         * shop_title : 袁米旗舰店
         * short_title :
         * small_images : ["https://img.alicdn.com/i2/2206449959356/O1CN01l0gETt2Iz6AxQFqwp_!!2206449959356.jpg","https://img.alicdn.com/i3/2206449959356/O1CN01weIYpP2Iz6Awj7Pnr_!!2206449959356.jpg","https://img.alicdn.com/i4/2206449959356/O1CN01U8v4SE2Iz6Awj9UcZ_!!2206449959356.jpg","https://img.alicdn.com/i2/2206449959356/O1CN01YqnSXs2Iz6AuyGNjY_!!2206449959356.jpg"]
         * title : 预售3.10号发货薇娅推荐 袁米海水稻严选大米5kg农家大米香米10斤
         * tk_total_commi : 0
         * tk_total_sales : 0
         * user_type : 1
         * volume : 94625
         * white_image :
         * zk_final_price : 119.9
         * coupon_amount : 5
         * coupon_end_time : 2020-02-26
         * coupon_start_fee : 18
         * coupon_start_time : 2020-02-12
         */

        @SerializedName("category_id")
        public int categoryId;
        @SerializedName("category_name")
        public String categoryName;
        @SerializedName("commission_rate")
        public String commissionRate;
        @SerializedName("coupon_id")
        public String couponId;
        @SerializedName("coupon_info")
        public String couponInfo;
        @SerializedName("coupon_remain_count")
        public int couponRemainCount;
        @SerializedName("coupon_total_count")
        public int couponTotalCount;
        @SerializedName("item_description")
        public String itemDescription;
        @SerializedName("item_id")
        public String itemId;
        @SerializedName("item_url")
        public String itemUrl;
        @SerializedName("level_one_category_id")
        public int levelOneCategoryId;
        @SerializedName("level_one_category_name")
        public String levelOneCategoryName;
        @SerializedName("nick")
        public String nick;
        @SerializedName("num_iid")
        public String numIid;
        @SerializedName("pict_url")
        public String pictUrl;
        @SerializedName("provcity")
        public String provcity;
        @SerializedName("real_post_fee")
        public String realPostFee;
        @SerializedName("reserve_price")
        public String reservePrice;
        @SerializedName("seller_id")
        public long sellerId;
        @SerializedName("shop_dsr")
        public int shopDsr;
        @SerializedName("shop_title")
        public String shopTitle;
        @SerializedName("short_title")
        public String shortTitle;
        @SerializedName("title")
        public String title;
        @SerializedName("tk_total_commi")
        public String tkTotalCommi;
        @SerializedName("tk_total_sales")
        public String tkTotalSales;
        @SerializedName("user_type")
        public int userType;
        @SerializedName("volume")
        public int volume;
        @SerializedName("white_image")
        public String whiteImage;
        @SerializedName("zk_final_price")
        public float zkFinalPrice;
        @SerializedName("coupon_amount")
        public float couponAmount;
        @SerializedName("coupon_end_time")
        public String couponEndTime;
        @SerializedName("coupon_start_fee")
        public String couponStartFee;
        @SerializedName("coupon_start_time")
        public String couponStartTime;
        @SerializedName("small_images")
        public List<String> smallImages;
    }
}
