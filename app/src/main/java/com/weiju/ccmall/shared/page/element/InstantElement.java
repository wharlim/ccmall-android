package com.weiju.ccmall.shared.page.element;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.blankj.utilcode.utils.SizeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.instant.MsgInstant;
import com.weiju.ccmall.module.instant.adapter.InstantAdapter2;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.constant.Key;
import com.weiju.ccmall.shared.decoration.SpacesItemDecoration;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.util.ConvertUtil;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/3/22.
 */
public class InstantElement extends FrameLayout {

    private final RecyclerView mListRv;
    private ArrayList<SkuInfo> mSkuInfos = new ArrayList<>();
    private InstantAdapter2 mAdapter = new InstantAdapter2(mSkuInfos);


    public InstantElement(Context context, Element element) {
        super(context);

        View view = inflate(getContext(), R.layout.el_product_layout, this);
        element.setBackgroundInto(view);

        mListRv = (RecyclerView) view.findViewById(R.id.eleListRv);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        mListRv.setLayoutManager(linearLayoutManager);
        mListRv.setNestedScrollingEnabled(false);
        mListRv.setAdapter(mAdapter);
        mListRv.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                SkuInfo item = mAdapter.getItem(position);
                if (item!=null){
                    goProductDetail(item.skuId, false);

                }
            }
        });
        mListRv.addItemDecoration(new SpacesItemDecoration(SizeUtils.dp2px(10)));
        mAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                SkuInfo item = mAdapter.getItem(position);
                if (item!=null){
                    goProductDetail(item.skuId, true);

                }
            }
        });
        ArrayList<SkuInfo> skus = ConvertUtil.json2SkuList(element.data);
        mSkuInfos.addAll(skus);
        mAdapter.notifyDataSetChanged();
    }

    private void goProductDetail(String skuId, boolean isBuy) {
        Intent intent = new Intent(getContext(), NewProductDetailActivity.class);
        intent.putExtra(Key.SKU_ID, skuId);
        getContext().startActivity(intent);
        if (isBuy) {
            EventBus.getDefault().postSticky(new MsgInstant(MsgInstant.ACTION_BUY));
        }
    }

}
