package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author chenyanming
 * @time 2019/12/21 on 17:57
 * @desc
 */
public class PullUrl implements Serializable {


    /**
     * rtmpUrl_lld : rtmp://live-pull.weijuit.cn/qqmp/d41a6d34b2714e8ca778556d8e5dd69d_lld?auth_key=1577104662-0-0-0d0097e6d1975c8649b483ae823e290f
     * m3u8Url_lud : http://live-pull.weijuit.cn/qqmp/d41a6d34b2714e8ca778556d8e5dd69d_lud.m3u8?auth_key=1577104662-0-0-199729eace5dd0e61f5b59f2838439cb
     * rtmpUrl : rtmp://live-pull.weijuit.cn/qqmp/d41a6d34b2714e8ca778556d8e5dd69d?auth_key=1577104662-0-0-300d509cc573305ad96337c8da51ef39
     * rtmpUrl_lsd : rtmp://live-pull.weijuit.cn/qqmp/d41a6d34b2714e8ca778556d8e5dd69d_lsd?auth_key=1577104662-0-0-eee064e5ba08280bd8261afb392fab0b
     * m3u8Url_lsd : http://live-pull.weijuit.cn/qqmp/d41a6d34b2714e8ca778556d8e5dd69d_lsd.m3u8?auth_key=1577104662-0-0-864ce7b6d83185d0a6ad4d431f21f6f6
     * m3u8Url_lld : http://live-pull.weijuit.cn/qqmp/d41a6d34b2714e8ca778556d8e5dd69d_lld.m3u8?auth_key=1577104662-0-0-73ac245c6183329cfe2e2bda481d33bb
     * rtmpUrl_lud : rtmp://live-pull.weijuit.cn/qqmp/d41a6d34b2714e8ca778556d8e5dd69d_lud?auth_key=1577104662-0-0-3f806b1f5f0fe44f962aca54a6d20191
     * m3u8Url_lhd : http://live-pull.weijuit.cn/qqmp/d41a6d34b2714e8ca778556d8e5dd69d_lhd.m3u8?auth_key=1577104662-0-0-f0138901ddb2e3d4924ea5ca8b0f9b8d
     * rtmpUrl_lhd : rtmp://live-pull.weijuit.cn/qqmp/d41a6d34b2714e8ca778556d8e5dd69d_lhd?auth_key=1577104662-0-0-0b36edf2f259c87fef714bebb2cf2516
     * m3u8Url : http://live-pull.weijuit.cn/qqmp/d41a6d34b2714e8ca778556d8e5dd69d.m3u8?auth_key=1577104662-0-0-fc17eb3d7ffe0447bd321bb465668793
     */

    /**
     * 流畅
     */
    @SerializedName("rtmpUrl_lld")
    public String rtmpUrlLld;
    @SerializedName("m3u8Url_lud")
    public String m3u8UrlLud;
    @SerializedName("rtmpUrl")
    public String rtmpUrl;
    /**
     * 高清
     */
    @SerializedName("rtmpUrl_lsd")
    public String rtmpUrlLsd;
    @SerializedName("m3u8Url_lsd")
    public String m3u8UrlLsd;
    @SerializedName("m3u8Url_lld")
    public String m3u8UrlLld;
    /**
     * 超清
     */
    @SerializedName("rtmpUrl_lud")
    public String rtmpUrlLud;
    @SerializedName("m3u8Url_lhd")
    public String m3u8UrlLhd;
    @SerializedName("rtmpUrl_lhd")
    public String rtmpUrlLhd;
    @SerializedName("m3u8Url")
    public String m3u8Url;
}
