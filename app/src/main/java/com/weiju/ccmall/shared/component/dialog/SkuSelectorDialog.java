package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.StringUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.flexbox.FlexboxLayout;
import com.google.common.base.Joiner;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.NewProductDetailActivity;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.Product;
import com.weiju.ccmall.shared.bean.Property;
import com.weiju.ccmall.shared.bean.PropertyValue;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.SkuPvIds;
import com.weiju.ccmall.shared.component.NumberField;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.contracts.OnValueChangeLister;
import com.weiju.ccmall.shared.decoration.ListDividerDecoration;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.CartManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.IProductService;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SkuSelectorDialog extends Dialog {

    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.selectedTv)
    protected TextView mSelectedTv;
    @BindView(R.id.priceTv)
    protected TextView mPriceTv;
    @BindView(R.id.thumbIv)
    protected SimpleDraweeView mThumbIv;
    @BindView(R.id.stockTv)
    protected TextView mStockTv;
    @BindView(R.id.buyNowBtn)
    protected TextView mBuyNowBtn;
    @BindView(R.id.addToCartBtn)
    protected TextView mAddToCartBtn;
    @BindView(R.id.confirmBtn)
    protected TextView mConfirmBtn;
    @BindView(R.id.numberField)
    protected NumberField mNumberField;

    protected Product product;
    @BindView(R.id.closeBtn)
    ImageView mCloseBtn;
    @BindView(R.id.layoutBotttom)
    LinearLayout mLayoutBotttom;
    @BindView(R.id.tvBottomGreyText)
    TextView mTvBottomGreyText;

    private SkuInfo skuInfo;
    private Map<Property, PropertyValue> mSelected = new HashMap<>();
    private IProductService mProductService;
    private int mAction = -1;
    private NewProductDetailActivity mActivity;
    private int mAmount = 1;
    private OnSelectListener mSelectListener;
    private String mGroupCode;
    private boolean mIsShowBottomText = true;

    private String orderType;
    private String liveId;

    public SkuSelectorDialog(Context context, Product product, SkuInfo sku) {
        this(context, 0);
        this.product = product;
        this.skuInfo = sku;
    }

    public SkuSelectorDialog(Context context, Product product, SkuInfo sku, int action,
                             NewProductDetailActivity activity) {
        this(context, product, sku, action, activity, "");
    }

    public SkuSelectorDialog(Context context, Product product, SkuInfo sku, int action,
                             NewProductDetailActivity activity, String orderType) {
        this(context, 0);
        this.product = product;
        this.skuInfo = sku;
        mAction = action;
        mActivity = activity;
        this.orderType = orderType;
    }

    public void setLiveId(String liveId) {
        this.liveId = liveId;
    }


    private SkuSelectorDialog(Context context, int themeResId) {
        super(context, R.style.Theme_Light_Dialog);
    }

    public SkuSelectorDialog(Context context, Product mProduct, SkuInfo mSkuInfo, int action) {
        this(context, mProduct, mSkuInfo);
        mAction = action;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_product_sku_selector);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        setCanceledOnTouchOutside(true);
        initViews();
        mProductService = ServiceManager.getInstance().createService(IProductService.class);
        setBottomViews();
    }

    private void setBottomViews() {
        mTvBottomGreyText.setVisibility(View.GONE);
        mConfirmBtn.setVisibility(View.GONE);
        mAddToCartBtn.setVisibility(View.GONE);
        mBuyNowBtn.setVisibility(View.GONE);

        if (mActivity != null && !StringUtils.isEmpty(mActivity.getBottomGreyText())) {
            mTvBottomGreyText.setVisibility(View.VISIBLE);
            mTvBottomGreyText.setText(mActivity.getBottomGreyText());
        } else if (!StringUtils.isEmpty(getBottomGreyText())) {
            mTvBottomGreyText.setVisibility(View.VISIBLE);
            mTvBottomGreyText.setText(getBottomGreyText());
        } else if (mAction != -1) {
            mConfirmBtn.setVisibility(mIsShowBottomText  ? View.VISIBLE : View.GONE);
        } else {
            mAddToCartBtn.setVisibility(mIsShowBottomText ? View.VISIBLE : View.GONE);
            mBuyNowBtn.setVisibility(mIsShowBottomText ? View.VISIBLE : View.GONE);
        }
    }

    private void setSkuInfo(SkuInfo sku) {
        this.skuInfo = sku;
        FrescoUtil.setImage(mThumbIv, skuInfo.thumb);
        mStockTv.setText(String.format("库存 %s 件", skuInfo.stock));
        if (product.extType == 2 && (mAction == AppTypes.SKU_SELECTOR_DIALOG.ACTION_CREATE_GROUP || mAction == AppTypes.SKU_SELECTOR_DIALOG.ACTION_JOIN_GROUP)) {
            Product.GroupExtEntity.GroupSkuListEntity groupEntity = product.getGroupEntity(skuInfo.skuId);
            mPriceTv.setText(MoneyUtil.centToYuan¥StrNoZero(groupEntity.groupPrice));
//            mNumberField.setValues(mAmount, groupEntity.minBuyNum, groupEntity.maxBuyNum);
        } else {
            mPriceTv.setText(MoneyUtil.centToYuan¥StrNoZero(skuInfo.retailPrice));
        }
        mNumberField.setValues(mAmount, 1, skuInfo.productType == 9 || product.extType == 5 ? 1 : skuInfo.stock);
//        mNumberField.setValues(mAmount, 1, skuInfo.productType == 9 ? 1 : skuInfo.stock);
        setBottomViews();
    }


    private void initViews() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        linearLayoutManager.setAutoMeasureEnabled(true);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        TagFlowAdapter tagFlowAdapter = new TagFlowAdapter();
        mRecyclerView.setAdapter(tagFlowAdapter);
        mRecyclerView.addItemDecoration(new ListDividerDecoration(getContext()));
        setSkuInfo(skuInfo);
        initSelected();
        mNumberField.setOnChangeListener(new OnValueChangeLister() {
            @Override
            public void changed(int value) {
                mAmount = value;
            }
        });
//        if (!mIsSale) {
//            mLayoutBotttom.setVisibility(View.GONE);
//        }
    }

    private void initSelected() {
        String pvIds = "";
        for (SkuPvIds skuPvIds : product.skus) {
            if (skuPvIds.skuId.equals(skuInfo.skuId)) {
                pvIds = skuPvIds.pvIds;
                break;
            }
        }
        if (pvIds.isEmpty()) {
            return;
        }
        for (Property property : product.properties) {
            for (PropertyValue value : property.values) {
                value.isSelected = pvIds.contains(value.id);
                if (value.isSelected) {
                    mSelected.put(property, value);
                    selectTags(false);
                }
            }
        }
    }

    @OnClick(R.id.buyNowBtn)
    protected void buyNow() {
        CartManager.buyNow(mActivity, getContext(), skuInfo, mAmount, null, orderType, liveId);
    }

    @OnClick(R.id.addToCartBtn)
    protected void addToCart() {
        if (skuInfo.productType == 18) {
            CartManager.buyNow(mActivity, getContext(), skuInfo, mAmount, null, orderType, liveId);
        } else
            CartManager.addToCart(getContext(), skuInfo, mAmount, true, liveId);
    }

    @OnClick(R.id.confirmBtn)
    protected void onConfirm() {
        switch (mAction) {
            case AppTypes.SKU_SELECTOR_DIALOG.ACTION_CART:
                addToCart();
                break;
            case AppTypes.SKU_SELECTOR_DIALOG.ACTION_BUY:
            case AppTypes.SKU_SELECTOR_DIALOG.ACTION_CREATE_SUPER_GROUP:
                buyNow();
                break;
            case AppTypes.SKU_SELECTOR_DIALOG.ACTION_CREATE_GROUP:
                CartManager.groupBuy(getContext(), skuInfo, mAmount);
                break;
            case AppTypes.SKU_SELECTOR_DIALOG.ACTION_JOIN_GROUP:
                CartManager.joinGroup(getContext(), skuInfo, mAmount, mGroupCode);
                break;
            default:
        }
    }


    @OnClick(R.id.closeBtn)
    protected void onClose() {
        dismiss();
    }


    private void selectTags(boolean loadSkuInfo) {
        if (mSelected.keySet().size() == product.properties.size()) {
            ArrayList<String> strings = new ArrayList<>();
            ArrayList<String> ids = new ArrayList<>();
            for (PropertyValue value : mSelected.values()) {
                ids.add(value.id);
                strings.add("“" + value.value + "”");
            }
            mSelectedTv.setText("已选：" + Joiner.on(", ").join(strings));
            if (loadSkuInfo) {
                loadSkuInfoByPropertyValueIds(ids);
            }
        } else {
            mSelectedTv.setText("请选择规格");
        }
    }

    private void loadSkuInfoByPropertyValueIds(ArrayList<String> ids) {
        APIManager.startRequest(mProductService.getSkuByPropertyValueIds(product.productId, Joiner.on(",").join(ids)), new BaseRequestListener<SkuInfo>() {
            @Override
            public void onSuccess(SkuInfo result) {
                updateSkuById(result.skuId);
            }

            @Override
            public void onError(Throwable e) {
            }
        }, getContext());
    }

    private void updateSkuById(String skuId) {
        APIManager.startRequest(mProductService.getSkuById(skuId), new BaseRequestListener<SkuInfo>() {
            @Override
            public void onSuccess(SkuInfo result) {
                if (mSelectListener != null) {
                    mSelectListener.onSelectSku(result);
                }
                setSkuInfo(result);
            }
        }, getContext());
    }

    public void setSelectListener(OnSelectListener selectListener) {
        mSelectListener = selectListener;
    }

    public void setGroupCode(String groupCode) {
        mGroupCode = groupCode;
    }

    public void showBottomText(boolean isShowBottomText) {
        mIsShowBottomText = isShowBottomText;
        if (mIsShowBottomText) {
            mConfirmBtn.setVisibility(View.GONE);
            mAddToCartBtn.setVisibility(View.GONE);
            mBuyNowBtn.setVisibility(View.GONE);
        }
    }

    public String getBottomGreyText() {
        if (skuInfo == null || product == null) {
            return null;
        }
        String greyText = null;
        if (skuInfo.status == 0) { //下架产品
            greyText = "已下架";
        } else if (skuInfo.stock <= 0) {
            greyText = "已告罄";
        }
        return greyText;
    }

    private class TagFlowAdapter extends RecyclerView.Adapter<ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(getLayoutInflater().inflate(R.layout.item_product_sku_tag_group_layout, parent, false));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final Property property = product.properties.get(position);
            holder.titleTv.setText(String.format("请选择%s", property.name));
            holder.tagContainer.removeAllViews();
            for (final PropertyValue value : property.values) {
                View view = getLayoutInflater().inflate(R.layout.sku_tag_item_layout, null);
                final TextView tagView = (TextView) view.findViewById(R.id.tagTv);
                tagView.setText(value.value);
                tagView.setSelected(value.isSelected);
                holder.tagContainer.addView(view);
                tagView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        for (PropertyValue propertyValue : property.values) {
                            propertyValue.isSelected = propertyValue.id.equals(value.id);
                        }
                        mSelected.put(property, value);
                        notifyDataSetChanged();
                        selectTags(true);
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            if (null == product) {
                return 0;
            }
            return product.properties == null ? 0 : product.properties.size();
        }
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.titleTv)
        protected TextView titleTv;
        @BindView(R.id.tagContainer)
        protected FlexboxLayout tagContainer;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface OnSelectListener {
        void onSelectSku(SkuInfo skuInfo);
    }

}
