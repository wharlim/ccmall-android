package com.weiju.ccmall.shared.util;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.weiju.ccmall.R;

import java.util.ArrayList;
import java.util.List;

/**
 * @author chenyanming
 * @time 2019/4/29 on 14:16
 * @desc ${TODD}
 */
public class ChartUtils {

    public static final int[] LINE_FILL_COLORS = {
            0x80000000, 0xffb90100, Color.rgb(235, 228, 248)
    };
    public static final int[] LINE_COLORS = {
            Color.rgb(140, 210, 118), Color.rgb(159, 143, 186), Color.rgb(233, 197, 23)
    };//绿色，紫色，黄色

    private static int main_blue = 0xFF6DC4CE;

    public static void setLinesChart(Context context, LineChart lineChart, final List<String> xAxisValue, final List<String> yAxisValue, final List<List<Long>> yXAxisValues, int[] lineColors) {
        //去掉周围的线
        lineChart.setDrawBorders(false);
        //好像没什么作用
        lineChart.setDrawGridBackground(true);
        lineChart.setGridBackgroundColor(Color.TRANSPARENT);
        //设置
           lineChart.setTouchEnabled(true);
        lineChart.setDragEnabled(false);
        lineChart.setScaleEnabled(false);
        lineChart.setPinchZoom(false);
        lineChart.setHighlightPerDragEnabled(false);
        lineChart.setData(generateLineData(context, yXAxisValues, lineColors));
        //设置没用数据和图标信息
        lineChart.setNoDataText("");
        lineChart.setDescription(null);
        //设置x轴
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setEnabled(true);
        xAxis.setDrawAxisLine(true);
        xAxis.setDrawGridLines(false);
        //设置轴线和网格线的颜色和宽度
        xAxis.setAxisLineColor(ContextCompat.getColor(context, R.color.black));
        xAxis.setAxisLineWidth(1);
        xAxis.setAxisLineColor(ContextCompat.getColor(context, R.color.color_e6));
        xAxis.setGridLineWidth(0.5f);
        xAxis.setLabelCount(4);
        xAxis.setTextColor(context.getResources().getColor(R.color.color_99));
        xAxis.setTextSize(10);
        xAxis.setCenterAxisLabels(false);
        xAxis.setAvoidFirstLastClipping(true);
        //设置x轴的值
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float v) {
                int position = (int) v;
                Log.v("value",position+"  ");
                if (position <xAxisValue.size() && position >= 0) {
                    return xAxisValue.get(position);
                }
                return "" + v;
            }
        });
        //设置右边边的为null
        YAxis rightAxis = lineChart.getAxisRight();
        rightAxis.setDrawAxisLine(false);
        rightAxis.setDrawGridLines(false);
        rightAxis.setDrawLabels(false);
        rightAxis.setAxisMinimum(0);
        rightAxis.setStartAtZero(true);

        YAxis leftAxis = lineChart.getAxisLeft();
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setDrawGridLines(true);
        leftAxis.setDrawAxisLine(false);
        leftAxis.enableGridDashedLine(10, 10, 10);
        leftAxis.setLabelCount(4,true);
        leftAxis.setSpaceTop(0f);
        leftAxis.setSpaceBottom(0f);
        leftAxis.setAxisLineColor(context.getResources().getColor(R.color.color_e6));
        leftAxis.setTextColor(context.getResources().getColor(R.color.color_99));
        leftAxis.setGridColor(context.getResources().getColor(R.color.color_e6));
        leftAxis.setAxisMinimum(0);
        leftAxis.setTextSize(10);
        leftAxis.setGridLineWidth(0.5f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setGranularityEnabled(false);

        Legend legend = lineChart.getLegend();
        legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        legend.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        legend.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        legend.setDrawInside(false);
        //legend被禁用
        legend.setEnabled(false);
   /*     legend.setDirection(Legend.LegendDirection.LEFT_TO_RIGHT);
        legend.setForm(Legend.LegendForm.SQUARE);
        legend.setTextSize(12f);*/
        lineChart.animateX(500);

        int hightIndex = yXAxisValues.get(0).size();
        lineChart.highlightValue(new Highlight(hightIndex,0,0));

    }


    private static LineData generateLineData(Context context, List<List<Long>> lineValues, int[] lineColors) {
        List<List<Entry>> entriesList = new ArrayList<>();
        if (lineValues != null) {
            for (int i = 0; i < lineValues.size(); ++i) {
                ArrayList<Entry> entries = new ArrayList<>();
                for (int j = 0, n = lineValues.get(i).size(); j < n; j++) {
                    Entry entry = new Entry(j, lineValues.get(i).get(j));
//                    entry.setData("" + lineValues.get(i).get(j));
                    entries.add(entry);
                }
                entriesList.add(entries);
            }
        }
        return generateLineData(context, entriesList, lineColors, 0);
    }

    private static LineData generateLineData(Context context, List<List<Entry>> entriesList, int[] lineColors, int what) {
        List<ILineDataSet> dataSets = new ArrayList<>();
        if (entriesList != null) {
            for (int i = 0; i < entriesList.size(); ++i) {
                LineDataSet lineDataSet = new LineDataSet(entriesList.get(i), "");
                lineDataSet.setValues(entriesList.get(i));
                if (lineColors != null) {
                    lineDataSet.setColor(context.getResources().getColor(lineColors[i]));
                    lineDataSet.setCircleColor(LINE_FILL_COLORS[1]);
//                    lineDataSet.setCircleColorHole(LINE_FILL_COLORS[1]);
                    lineDataSet.setDrawFilled(false);
                } else {
                    lineDataSet.setColor(LINE_FILL_COLORS[i % 3]);
                    lineDataSet.setCircleColor(LINE_COLORS[i % 3]);
//                    lineDataSet.setCircleColorHole(Color.WHITE);
                    lineDataSet.setDrawFilled(false);

                }
                lineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
                lineDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
                lineDataSet.setHighlightEnabled(true);
                lineDataSet.setHighLightColor(LINE_FILL_COLORS[0]);
                lineDataSet.setValueTextColor(main_blue);
                lineDataSet.setDrawCircleHole(false);
                lineDataSet.setLineWidth(1f);
                lineDataSet.setDrawValues(false);
                lineDataSet.setDrawCircles(false);
                lineDataSet.setMode(LineDataSet.Mode.LINEAR);
                dataSets.add(lineDataSet);
            }
        }

        LineData lineData = new LineData(dataSets);
        lineData.setValueTextSize(10f);
        return lineData;
    }

}
