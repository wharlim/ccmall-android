package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author chenyanming
 * @time 2019/12/24 on 17:22
 * @desc
 */
public class RecordUrl implements Serializable {

    /**
     * recordId : 5e8bbafa401c42b5a3acc97124573479
     * liveId : 97fc0f7934754b7f9d2819ae59ba6ae5
     * recordUrl : https://sgcoss.weijuit.com/record/qqmp/97fc0f7934754b7f9d2819ae59ba6ae5/2019-12-24-16-59-13_2019-12-24-17-10-19.mp4
     * duration : 666.311
     * startTime : 2019-12-24 16:59:13
     * stopTime : 2019-12-24 17:10:19
     * createDate : 2019-12-24 17:14:00
     * deleteFlag : 0
     */

    @SerializedName("recordId")
    public String recordId;
    @SerializedName("liveId")
    public String liveId;
    @SerializedName("recordUrl")
    public String recordUrl;
    @SerializedName("duration")
    public String duration;
    @SerializedName("startTime")
    public String startTime;
    @SerializedName("stopTime")
    public String stopTime;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
}
