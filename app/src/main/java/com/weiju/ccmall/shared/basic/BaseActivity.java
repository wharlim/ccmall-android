package com.weiju.ccmall.shared.basic;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.orhanobut.logger.Logger;
import com.qmuiteam.qmui.util.QMUIStatusBarHelper;
import com.umeng.analytics.MobclickAgent;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.shared.bean.api.RequestResult;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.HeaderLayout;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.Timber;
import com.weiju.ccmall.shared.util.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.reflect.Method;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


abstract public class BaseActivity extends FragmentActivity {

    protected FrameLayout mBaseContentLayout;
    public HeaderLayout mHeaderLayout;
    protected InputMethodManager mInputMethodManager;
    private SwipeRefreshLayout mLayoutRefresh;
    private Dialog mProgressDialog;
    private CompositeDisposable compositeSubscription;
    private Disposable mDisposable;
    protected View mBarPading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        requestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        QMUIStatusBarHelper.setStatusBarLightMode(this);
//        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mInputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (isNeedLogin() && !SessionUtil.getInstance().isLogin()) {
            EventBus.getDefault().post(new EventMessage(Event.goToLogin));
            ToastUtil.error(Config.NET_MESSAGE.NO_LOGIN);
        }
    }

    protected boolean isNeedLogin() {
        return false;
    }

    public SwipeRefreshLayout getLayoutRefresh() {
        return mLayoutRefresh;
    }

    public void setLayoutRefresh(SwipeRefreshLayout layoutRefresh) {
        mLayoutRefresh = layoutRefresh;
    }

    public Dialog getProgressDialog() {
        if (mProgressDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.loading_layout, null);
            mProgressDialog = new Dialog(this, R.style.LoadingDialog);
            mProgressDialog.setContentView(view, new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            mProgressDialog.setCancelable(true);
        }
        return mProgressDialog;
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
//        Bugtags.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
//        Bugtags.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ToastUtil.hideLoading();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        unSubscribe();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void subscribeMessageWithEventBus(EventMessage message) {
        switch (message.getEvent()) {
            case toastErrorMessage:
                ToastUtil.error((String) message.getData());
                break;
            case hideLoading:
                ToastUtil.hideLoading();
                break;
            case showLoading:
                ToastUtil.showLoading(this);
                break;
            case networkConnected:
                Logger.e("网络已连接");
                break;
            case networkDisconnected:
                Logger.e("网络已断开");
                break;
            case showAlert:
                new AlertDialog.Builder(this)
                        .setCancelable(false)
                        .setTitle("原生弹窗")
                        .setPositiveButton("确定", null)
                        .setMessage((String) message.getData())
                        .create().show();
                break;
            default:
                break;
        }
    }


    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.base_layout);
        initBaseViews();
        getLayoutInflater().inflate(layoutResID, mBaseContentLayout, true);
    }

    @Override
    public void setContentView(View view) {
        super.setContentView(R.layout.base_layout);
        initBaseViews();
        mBaseContentLayout.addView(view);
    }

    private void initBaseViews() {
        mBaseContentLayout = (FrameLayout) findViewById(R.id.baseContentLayout);
        mHeaderLayout = (HeaderLayout) findViewById(R.id.headerLayout);
        mBarPading = findViewById(R.id.barPading);
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle(title);
        mHeaderLayout.setTitle(title);
    }

    @Override
    public void setTitle(int titleId) {
        super.setTitle(titleId);
        mHeaderLayout.setTitle(titleId);
    }

    public void setBarPadingHeight(int height) {
        mBarPading.getLayoutParams().height = height;
//        mBarPading.setMinimumHeight(height);
    }

    public void setBarPadingColor(int color) {
        mBarPading.setBackgroundColor(color);
    }

    public void setTitleNoLine() {
        mHeaderLayout.setNoLine();
    }


    public void setLeftBlack() {
        mHeaderLayout.setLeftDrawable(R.mipmap.icon_back_black);
        mHeaderLayout.setOnLeftClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void showHeader() {
        mHeaderLayout.show();
    }

    public void showHeader(String title, boolean isBlack) {
        showHeader();
        setTitle(title);
        if (isBlack) {
            setLeftBlack();
        }
    }

    public HeaderLayout getHeaderLayout() {
        return mHeaderLayout;
    }

    // 网络请求封装
    public void execute(Observable observable, BaseSubscriber subscriber) {
        observable = observable
                .map(new HttpResultFunc())
                .throttleFirst(500, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        observable.subscribe(subscriber);
        addSubscribe(subscriber.getDisposable());
    }


    public static class HttpResultFunc<T> implements Function<RequestResult<T>, T> {
        @Override
        public T apply(@NonNull RequestResult<T> result) throws Exception {
            if (result.isSuccess()) {
                return result.data;
            } else if (result.isNotLogin()) {
                EventBus.getDefault().post(new EventMessage(Event.goToLogin));
                throw new RuntimeException(Config.NET_MESSAGE.NO_LOGIN);
            } else {
                throw new RuntimeException(result.message);
            }
        }
    }

    public void addSubscribe(Disposable disposable) {
        if (compositeSubscription == null) {
            compositeSubscription = new CompositeDisposable();
        }
        compositeSubscription.add(disposable);
    }

    public void unSubscribe() {
        if (compositeSubscription != null) {
            compositeSubscription.clear();
        }
    }

    /**
     * 切换横竖屏， Android8.0有bug，要特殊处理
     * ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE（0）横屏
     * ActivityInfo.SCREEN_ORIENTATION_PORTRAIT（1）竖屏
     */
    private void requestedOrientation(int orientation){
        Timber.INSTANCE.e("Build.VERSION.SDK_INT = " + Build.VERSION.SDK_INT);
        if(Build.VERSION.SDK_INT == Build.VERSION_CODES.O){
            try {
                Method method = Activity.class.getDeclaredMethod("convertFromTranslucent");
                method.setAccessible(true);
                method.invoke(this);
            } catch (Exception e) {
                Timber.INSTANCE.e(e.getMessage());
            }
        }
        setRequestedOrientation(orientation);
    }


}