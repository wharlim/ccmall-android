package com.weiju.ccmall.shared.bean;

import com.blankj.utilcode.utils.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.constant.AppTypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.bean
 * @since 2017-06-20
 */
public class OrderProfit implements Serializable {
    @SerializedName("storeId")
    public String storeId;
    @SerializedName("storeName")
    public String storeName;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("userName")
    public String userName;
    @SerializedName("sobotId")
    public String sobotId;
    @SerializedName("orderMain")
    public OrderMain orderMain;
    @SerializedName("refundOrder")
    public RefundOrder refundOrder;
    @SerializedName("groupInfo")
    public GroupInfoEntity groupInfo;
    @SerializedName("store")
    public StoreUser storeUser;
    @SerializedName("upMember")
    public UpMember upMember;

    @SerializedName("vipDto")
    public VipDto vipDto;

    @SerializedName("memberProfitBean")
    public MemberProfitBean memberProfitBean;
    @SerializedName("orderAssociatedHeadImage")
    public String orderAssociatedHeadImage;

    /**
     * @return 是否在订单显示团购信息
     */
    public boolean isShowGroupOrderStatus() {
        return (orderMain.status == 2) && (groupInfo != null) && (groupInfo.activityStatus == AppTypes.GROUP_BUY.STATUS_WAIT_COMPLETE) && orderMain.orderType != 2;
    }

    public boolean isGroupOrder() {
        return (groupInfo != null) && !StringUtils.isEmpty(groupInfo.activityId) && orderMain.isPay && orderMain.orderType != 2;
    }


    public static class GroupInfoEntity implements Serializable {
        /**
         * orderId : 523812a29e034fc39c17a724c0d465b4
         * memberId : 0d3e5898d0c04bde8afec8adea40179d
         * headImage :
         * nickName :
         * activityId : b44c1c23fc1f4f6c8384e7c22ed409e5
         * activityStatus : 1
         * activityStatusStr : 拼团中
         * groupCode : 1571509764504472
         * groupLeaderReturn : 0
         * expiresDate : 2017-11-02 05:52:20
         * joinMemberNum : 2
         * createOrderNum : 1
         * payOrderNum : 1
         * payDate : 2017-11-01 19:52:20
         */

        @SerializedName("orderId")
        public String orderId;
        @SerializedName("memberId")
        public String memberId;
        @SerializedName("headImage")
        public String headImage;
        @SerializedName("nickName")
        public String nickName;
        @SerializedName("activityId")
        public String activityId;
        @SerializedName("activityStatus")
        public int activityStatus;
        @SerializedName("activityStatusStr")
        public String activityStatusStr;
        @SerializedName("groupCode")
        public String groupCode;
        @SerializedName("groupLeaderReturn")
        public int groupLeaderReturn;
        @SerializedName("expiresDate")
        public String expiresDate;
        @SerializedName("joinMemberNum")
        public int joinMemberNum;
        @SerializedName("createOrderNum")
        public int createOrderNum;
        @SerializedName("payOrderNum")
        public int payOrderNum;
        @SerializedName("payDate")
        public String payDate;
    }


    public static class OrderMain implements Serializable {

        @SerializedName("orderId")
        public String orderId;
        @SerializedName("memberId")
        public String memberId;
        @SerializedName("orderCode")
        public String orderCode;
        @SerializedName("orderStatus")
        public int status;
        /**
         * 2 超级拼团
         */
        @SerializedName("orderType")
        public int orderType;
        @SerializedName("contact")
        public String contact;
        @SerializedName("phone")
        public String phone;
        @SerializedName("province")
        public String province;
        @SerializedName("city")
        public String city;
        @SerializedName("district")
        public String district;
        @SerializedName("detail")
        public String detail;
        @SerializedName("totalMoney")
        public long totalMoney;
        @SerializedName("totalWeight")
        public int totalWeight;
        @SerializedName("freight")
        public long freight;
        @SerializedName("discountCoupon")
        public long discountCoupon;
        @SerializedName("isPay")
        public boolean isPay;
        @SerializedName("payType")
        public int payType;
        @SerializedName("payMoney")
        public int payMoney;
        @SerializedName("payDate")
        public String payDate;
        @SerializedName("payRemark")
        public String payRemark;
        @SerializedName("isReceived")
        public int isReceived;
        @SerializedName("receivedDate")
        public String receivedDate;
        @SerializedName("orderFrom")
        public int orderFrom;
        @SerializedName("buyerRemark")
        public String buyerRemark;
        @SerializedName("sellerRemark")
        public String sellerRemark;
        @SerializedName("expressId")
        public int expressId;
        @SerializedName("expressName")
        public String expressName;
        @SerializedName("expressCode")
        public String expressCode;
        @SerializedName("expressType")
        public String expressType;
        @SerializedName("shipDate")
        public String shipDate;
        @SerializedName("createDate")
        public String createDate;
        @SerializedName("deleteFlag")
        public int deleteFlag;
        @SerializedName("orderStatusStr")
        public String orderStatusStr;
        @SerializedName("payTypeStr")
        public String payTypeStr;
        @SerializedName("productMoney")
        public long productMoney;
        @SerializedName("score")
        public long score;
        @SerializedName("ticket")
        public long ticket;
        @SerializedName("coin")
        public long coin;
        @SerializedName("goldenMemberInfo")
        public String goldenMemberInfo;
        @SerializedName("goldenTicket")
        public long goldenTicket;
        @SerializedName("useGoldenTicket")
        public long useGoldenTicket;
        /*@SerializedName("orderType")
        public int orderType;*/

        @SerializedName("giveUpRefund")
        public int giveUpRefund;//0默认，1：已放弃，已提交过

        @SerializedName("payLog")
        public String payLog;

        @SerializedName("paymentNumber")
        public String paymentNumber;

        /**
         * discountRate : 60
         * discountMoney : 15040
         */

        @SerializedName("discountRate")
        public int discountRate;
        @SerializedName("discountMoney")
        public long discountMoney;
        /**
         * totalProductMoney : 72200
         */

        @SerializedName("totalProductMoney")
        public long totalProductMoney;

        @SerializedName("transactionFee")
        public int transactionFee;

        @SerializedName("serviceFee")
        public long serviceFee;

        @SerializedName("lessTime")
        public long lessTime;
        /**
         * 0:未支付
         * 1：下级支付
         * 2：上级支付
         */
        @SerializedName("payTag")
        public int payTag;

        @SerializedName("nickName")
        public String nickName;

        public String getFullAddress() {
            return province + city + district + detail;
        }

    }


    public static class MemberProfitBean implements Serializable {

        @SerializedName("profitId")
        public String profitId;

        @SerializedName("sortIndex")
        public int sortIndex;

        @SerializedName("memberId")
        public String memberId;

        @SerializedName("type")
        public int type;

        @SerializedName("orderId")
        public String orderId;

        @SerializedName("orderMemberId")
        public String orderMemberId;

        @SerializedName("orderCode")
        public String orderCode;

        @SerializedName("orderMoney")
        public String orderMoney;

        @SerializedName("profitMoney")
        public int profitMoney;
        @SerializedName("profitTime")
        public String profitTime;

        @SerializedName("unfreezeSumMoney")
        public String unfreezeSumMoney;
        @SerializedName("availableMoney")
        public String availableMoney;
        @SerializedName("status")
        public int status;//status -1 代表未付款，0 冻结，1解冻
        @SerializedName("createDate")
        public String createDate;
        @SerializedName("unfreezeDate")
        public String unfreezeDate;
    }


    public static class StoreUser implements Serializable {
        @SerializedName("storeId")
        public String storeId;
        @SerializedName("phone")
        public String phone;
        @SerializedName("alipayImage")
        public String alipayImage;
        @SerializedName("alipayAccount")
        public String alipayAccount;
        @SerializedName("userName")
        public String userName;
        @SerializedName("realname")
        public String realName;
        @SerializedName("alipayImageCode")
        public String alipayImageCode;
        @SerializedName("nickName")
        public String nickName;
        @SerializedName("alipayReceiver")
        public String alipayReceiver;
        /*@SerializedName("alipayImageCode")
        public String alipayImageCode;*/
        @SerializedName("displayStatus")
        public int displayStatus;
        @SerializedName("memberId")
        public String memberId;
        @SerializedName("storeName")
        public String storeName;

    }


    public static class RefundOrder implements Serializable {
        public int refundType;
        public int refundStatus;
        public String refundReason;
        public String refundRemark;
        public long refundMoney;
        public long applyRefundMoney;
        public String refundGoodsExpressName;
        public String refundGoodsExpressCode;
        public List<String> refundGoodsImage = new ArrayList<>();
        public String refundId;
    }

    public long canRefundMoney(int refundType) {
        if (refundType == 1) {
            return orderMain.payMoney - orderMain.freight;
        } else {
            return orderMain.payMoney;
        }
    }

    public static class UpMember implements Serializable {
        @SerializedName("memberId")
        public String memberId;
        @SerializedName("headImage")
        public String headImage;
        @SerializedName("nickName")
        public String nickName;
        @SerializedName("phone")
        public String phone;
        @SerializedName("vipType")
        public int vipType;

    }


    public static class VipDto implements Serializable {
        @SerializedName("mVip")
        public int mVip;

        @SerializedName("upVip")
        public int upVip;
    }
}
