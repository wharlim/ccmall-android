package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.weiju.ccmall.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TranslationUpgradeDialog extends Dialog {

    @BindView(R.id.tvActivate)
    protected TextView tvActivate;

    @BindView(R.id.tvCurrentLevel)
    TextView mTvCurrentLevel;
    @BindView(R.id.tvLatestLevel)
    TextView mTvLatestLevel;
    @BindView(R.id.tvNextLevel)
    TextView mTvNextLevel;

    public TranslationUpgradeDialog(Context context) {
        this(context, R.style.Theme_Light_Dialog);
    }

    private TranslationUpgradeDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_translation_upgrade);
        ButterKnife.bind(this);
//        CommonUtil.initDialogWindow(getWindow(), Gravity.CENTER);
//        setCanceledOnTouchOutside(true);
    }


    public void setCurrentLevelText(CharSequence charSequence) {
        if (mTvCurrentLevel == null)
            mTvCurrentLevel = findViewById(R.id.tvCurrentLevel);
        mTvCurrentLevel.setText(charSequence);
    }

    public void setLatestLevelText(CharSequence charSequence) {
        if (mTvLatestLevel == null)
            mTvLatestLevel = findViewById(R.id.tvLatestLevel);
        mTvLatestLevel.setText(charSequence);
    }

    public void setNextLevelText(CharSequence charSequence) {
        if (mTvNextLevel == null)
            mTvNextLevel = findViewById(R.id.tvNextLevel);
        mTvNextLevel.setText(charSequence);
    }

    private View.OnClickListener mConfirmListener;

    public void setOnConfirmListener(@Nullable View.OnClickListener listener) {
        mConfirmListener = listener;
    }

    @OnClick(R.id.tvClose)
    protected void onClose(View view) {
        dismiss();
    }

    @OnClick(R.id.tvActivate)
    protected void onConfirm(View view) {
        if (mConfirmListener == null) {
            dismiss();
        } else {
            mConfirmListener.onClick(view);
        }
    }
}
