package com.weiju.ccmall.shared.basic;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.util.ConvertUtil;

import net.lucode.hackware.magicindicator.MagicIndicator;
import net.lucode.hackware.magicindicator.ViewPagerHelper;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.CommonNavigator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.CommonNavigatorAdapter;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.abs.IPagerTitleView;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.indicators.LinePagerIndicator;
import net.lucode.hackware.magicindicator.buildins.commonnavigator.titles.SimplePagerTitleView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 上面一个 tab，下面一个 viewpager 的 Fragment
 */
public abstract class BaseTopTabActivity extends BaseActivity {

    @BindView(R.id.barPading)
    View mBarPading;
    @BindView(R.id.magicIndicator)
    MagicIndicator mMagicIndicator;
    @BindView(R.id.viewPager)
    ViewPager mViewPager;
    public FragmentPagerAdapter mFragmentPagerAdapter;
    public CommonNavigatorAdapter mNavigatorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_top_tab);
        ButterKnife.bind(this);
        getIntentData();
        initView();
        initData();
    }

    public void initData() {

    }

    public void getIntentData() {

    }

    public void initView() {
        final String[] titles = getTitles();
        final List<BaseFragment> fragments = getFragments();

        mFragmentPagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }

            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return titles[position];
            }
        };
        mViewPager.setOffscreenPageLimit(fragments.size());
        mViewPager.setCurrentItem(0);
        mViewPager.setAdapter(mFragmentPagerAdapter);

        mNavigatorAdapter = new CommonNavigatorAdapter() {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public IPagerTitleView getTitleView(Context context, final int index) {
                SimplePagerTitleView titleView = new SimplePagerTitleView(context);
                titleView.setText(titles[index]);
                titleView.setNormalColor(getTabTextNoramalColor());
                titleView.setSelectedColor(getTabTextSelectColor());
                titleView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mViewPager.setCurrentItem(index);
                    }
                });
                titleView.setPadding(0, 0, 0, 0);
                titleView.setTextSize(14);
                return titleView;
            }

            @Override
            public IPagerIndicator getIndicator(Context context) {
                LinePagerIndicator indicator = new LinePagerIndicator(context);
                indicator.setColors(getIndicatorColor());
                indicator.setLineHeight(ConvertUtil.dip2px(2));
                return indicator;
            }
        };
        CommonNavigator commonNavigator = new CommonNavigator(this);
        commonNavigator.setLeftPadding(ConvertUtil.dip2px(20));
        commonNavigator.setRightPadding(ConvertUtil.dip2px(20));
        commonNavigator.setAdapter(mNavigatorAdapter);
        commonNavigator.setAdjustMode(true);
        mMagicIndicator.setNavigator(commonNavigator);
        ViewPagerHelper.bind(mMagicIndicator, mViewPager);
    }

    public int getIndicatorColor() {
        return getResources().getColor(R.color.red);
    }

    public int getTabTextNoramalColor() {
        return getResources().getColor(R.color.default_text_color);
    }

    public int getTabTextSelectColor() {
        return getResources().getColor(R.color.red);
    }

    public abstract String[] getTitles();

    public abstract List<BaseFragment> getFragments();
}
