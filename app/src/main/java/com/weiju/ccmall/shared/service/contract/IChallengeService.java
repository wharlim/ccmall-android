package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.Challenge;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author chenyanming
 * @time 2019/1/29 on 11:08
 * @desc 挑战
 */
public interface IChallengeService {
    @GET("challenge/activityList")
    Observable<RequestResult<PaginationEntity<Challenge, Object>>> getActivityList(@Query("pageOffset") int page, @Query("pageSize") int pageSize);

    @GET("challenge/checkChallenge")
    Observable<RequestResult<Challenge>> checkChallenge(@Query("activityId") String activityId);

    @GET("challenge/getChallenge")
    Observable<RequestResult<Challenge>> getChallenge(@Query("challengeId") String challengeId);

    @FormUrlEncoded
    @POST("challenge/join")
    Observable<RequestResult<Challenge>> joinChallenge(
            @Field("activityId") String skuId
    );


    @GET("challenge/logList")
    Observable<RequestResult<PaginationEntity<Challenge, Object>>> getLogList(@Query("pageOffset") int page, @Query("pageSize") int pageSize);


    @GET("challenge/getPkChallengeList")
    Observable<RequestResult<List<Challenge>>> getPkChallengeList(@Query("activityId") String activityId,
                                                                  @Query("pkMinScore") int pkMinScore, @Query("pkMaxScore") int pkMaxScore);

    @FormUrlEncoded
    @POST("challenge/addPkChallenge")
    Observable<RequestResult<Challenge>> addPkChallenge(
            @Field("activityId") String skuId, @Field("pkScore") int pkScore
    );

    @FormUrlEncoded
    @POST("challenge/joinPkChallenge")
    Observable<RequestResult<Challenge>> joinPkChallenge(
            @Field("pkChallengeId") String pkChallengeId, @Field("pkMemberId") String pkMemberId
    );

    @FormUrlEncoded
    @POST("challenge/cancelPkChallenge")
    Observable<RequestResult<Object>> cancelPkChallenge(
            @Field("challengeId") String challengeId);

}
