package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveCoupon;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.LiveRedLoadMoreView;
import com.weiju.ccmall.shared.component.adapter.LiveCouponListAdapter;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;


import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @date 2020/2/21.
 */
public class LiveCouponListDialog extends Dialog {

    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;
    @BindView(R.id.tvTitle)
    TextView mTvTitle;

    private final Activity mContext;
    private final String mLiveId;
    private final LiveUser mLiveUser;


    public static int COUPON_PUSH = 0;
    public static int COUPON_PLAY = 1;
    private int mType;

    private LiveCouponListAdapter mLiveCouponListAdapter = new LiveCouponListAdapter();
    private int mCurrentPage = 1;

    private ILiveService mService = ServiceManager.getInstance().createService(ILiveService.class);

    public LiveCouponListDialog(Activity context, String liveId, LiveUser liveUser, int type) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mLiveId = liveId;
        mLiveUser = liveUser;
        mType = type;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_coupon_list);
        ButterKnife.bind(this);
        initView();
        getLiveCouponList();
    }

    private void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mLiveCouponListAdapter);

        View inflate = View.inflate(mContext, R.layout.cmp_no_data, null);
        inflate.findViewById(R.id.noDataLabel).setVisibility(View.GONE);
        mLiveCouponListAdapter.setEmptyView(inflate);
        mLiveCouponListAdapter.setType(mType);

        mTvTitle.setText(mType == LiveCouponListDialog.COUPON_PUSH ? "优惠券领取记录":"优惠券列表");

        mLiveCouponListAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentPage++;
                getLiveCouponList();
            }
        }, mRecyclerView);

        mLiveCouponListAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                LiveCoupon item = mLiveCouponListAdapter.getItem(position);
                if (null == item) {
                    return;
                }
                dismiss();

                if (mType == COUPON_PUSH) {
                    LiveCouponReceiveDetailDialog liveCouponReceiveDetailDialog = new LiveCouponReceiveDetailDialog(mContext, mLiveId, item.couponId, item.sendId);
                    liveCouponReceiveDetailDialog.show();
                    EventBus.getDefault().post(new EventMessage(Event.couponDialogShow));
                } else {
                    if (item.status == 0 && item.receiveStatus == 0) {
                        //未领取完，用户还没领取,去领取
                        LiveCouponOpenDialog openDialog = new LiveCouponOpenDialog(mContext, mLiveId, item.sendId, item.couponId,mLiveUser.headImage);
                        openDialog.show();
                        EventBus.getDefault().post(new EventMessage(Event.couponDialogShow));
                    } else if (item.receiveStatus == 1) {
                        //领取过该优惠券，查看详情
                        LiveCouponReceiveDetailDialog detailDialog = new LiveCouponReceiveDetailDialog(mContext, mLiveId, item.couponId, item.sendId);
                        detailDialog.show();
                        EventBus.getDefault().post(new EventMessage(Event.couponDialogShow));
                    }
                }

            }
        });

        mLiveCouponListAdapter.setHeaderAndEmpty(true);
        mLiveCouponListAdapter.setFooterViewAsFlow(true);
        mLiveCouponListAdapter.setLoadMoreView(new LiveRedLoadMoreView());

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mCurrentPage = 1;
                getLiveCouponList();
            }
        });

//        List<LiveCoupon> list = new ArrayList<>();
//        for (int i = 0; i < 10; i++) {
//            list.add(new LiveCoupon());
//        }
//        mLiveCouponListAdapter.setNewData(list);
    }

    private void getLiveCouponList() {
        APIManager.startRequest(mService.getLiveCouponList(mLiveId, mCurrentPage, 20),
                new BaseRequestListener<PaginationEntity<LiveCoupon, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<LiveCoupon, Object> result) {
                        if (mCurrentPage == 1) {
                            mLiveCouponListAdapter.setNewData(result.list);
                        } else {
                            mLiveCouponListAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mLiveCouponListAdapter.loadMoreEnd();
                        } else {
                            mLiveCouponListAdapter.loadMoreComplete();
                        }
                    }
                }, mContext);
    }

    @OnClick(R.id.ivClose)
    protected void close() {
        dismiss();
    }


    @Override
    public void dismiss() {
        EventBus.getDefault().post(new EventMessage(Event.couponDialogDismiss));
        super.dismiss();
    }
}
