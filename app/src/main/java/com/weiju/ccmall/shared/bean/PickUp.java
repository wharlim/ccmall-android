package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author chenyanming
 * @time 2019/8/2 on 10:59
 * @desc ${TODD}
 */
public class PickUp implements Serializable {
    /**
     * 提货卷ID
     */
    @SerializedName("vouchersId")
    public String vouchersId;
    /**
     * 提货卷标题
     */
    @SerializedName("title")
    public String title;
    /**
     * 提货卷图标
     */
    @SerializedName("iconUrl")
    public String iconUrl;
    /**
     * 提货卷金额
     */
    @SerializedName("cost")
    public long cost;
    /**
     * 提货卷数量
     */
    @SerializedName("num")
    public int num;

    /**
     * 用户的提货券ID
     */
    @SerializedName("goodsId")
    public String goodsId;
    /**
     * 提货券编号
     */
    @SerializedName("goodsCode")
    public String goodsCode;
    /**
     * 提货券是否可用（0未使用 1已使用）
     */
    @SerializedName("status")
    public int status;
    /**
     * 使用日期
     */
    @SerializedName("useDate")
    public String useDate;
    /**
     *过期时间
     */
    @SerializedName("expireDate")
    public String expireDate;
    /**
     * 订单编号
     */
    @SerializedName("orderCode")
    public String orderCode;
    /**
     * 赠送/收到的时间
     */
    @SerializedName("transferDate")
    public String transferDate;
    /**
     * 来自哪个用户
     */
    @SerializedName("inMemberId")
    public String inMemberId;
    /**
     * 来自哪个用户（手机）
     */
    @SerializedName("inMemberPhone")
    public String inMemberPhone;
    /**
     * 赠送给哪个用户
     */
    @SerializedName("outMemberId")
    public String outMemberId;
    /**
     * 赠送给哪个用户（手机）
     */
    @SerializedName("outMemberPhone")
    public String outMemberPhone;

    public boolean isSelected;

}
