package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.adapter.LiveUserAdapter;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.CommonUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author chenyanming
 * @time 2019/12/23 on 17:08
 * @desc
 */
public class LiveUserDialog extends Dialog {
    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;
    private Context mContext;

    private int mCurrentPage = 1;
    private String mLiveId;
    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);
    private LiveUserAdapter mLiveUserAdapter;

    public LiveUserDialog(@NonNull Context context, String liveId) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mLiveId = liveId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_user);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.dimAmount = 0.0f;
        getWindow().setAttributes(layoutParams);

        initView();
        initData();

    }

    private void initData() {
        getLiveAudienceList();
    }

    private void initView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(new EventMessage(Event.liveUserRefresh));
                mCurrentPage = 1;
                getLiveAudienceList();
            }
        });

        mLiveUserAdapter = new LiveUserAdapter();


        View inflate = View.inflate(mContext, R.layout.cmp_no_data, null);
        mLiveUserAdapter.setEmptyView(inflate);

        mLiveUserAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                mCurrentPage++;
                getLiveAudienceList();
            }
        }, mRecyclerView);

        mLiveUserAdapter.setHeaderAndEmpty(true);
        mLiveUserAdapter.setFooterViewAsFlow(true);

        mRecyclerView.setAdapter(mLiveUserAdapter);

    }

    private void getLiveAudienceList() {
        APIManager.startRequest(mILiveService.getLiveAudienceList(mLiveId, mCurrentPage, 20),
                new BaseRequestListener<PaginationEntity<LiveUser, Object>>(mRefreshLayout) {
                    @Override
                    public void onSuccess(PaginationEntity<LiveUser, Object> result) {
                        if (mCurrentPage == 1) {
                            mLiveUserAdapter.setNewData(result.list);
                        } else {
                            mLiveUserAdapter.addData(result.list);
                        }
                        if (result.page >= result.totalPage) {
                            mLiveUserAdapter.loadMoreEnd();
                        } else {
                            mLiveUserAdapter.loadMoreComplete();
                        }

                    }
                }, mContext);
    }

}
