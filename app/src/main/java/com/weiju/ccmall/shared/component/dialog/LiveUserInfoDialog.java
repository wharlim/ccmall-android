package com.weiju.ccmall.shared.component.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.SessionUtil;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/25 on 10:06
 * @desc
 */
public class LiveUserInfoDialog extends Dialog {

    private final Activity mContext;
    @BindView(R.id.ivAvatar)
    SimpleDraweeView mIvAvatar;
    @BindView(R.id.tvName)
    TextView mTvName;
    @BindView(R.id.tvFollow)
    TextView mTvFollow;
    @BindView(R.id.tvLikeNum)
    TextView mTvLikeNum;
    @BindView(R.id.tvFollowNum)
    TextView mTvFollowNum;
    @BindView(R.id.tvFansNum)
    TextView mTvFansNum;
    private LiveUser mLiveUser;

    private FollowOnClickListener mFollowOnClickListener;

    public LiveUserInfoDialog(@NonNull Activity context, FollowOnClickListener followOnClickListener) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mFollowOnClickListener = followOnClickListener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_user_info);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.dimAmount = 0.0f;
        getWindow().setAttributes(layoutParams);
    }

    public void setLiveUser(LiveUser liveUser) {
        mLiveUser = liveUser;
        FrescoUtil.setImage(mIvAvatar, liveUser.headImage);
        mTvName.setText(liveUser.nickName);
        mTvFollow.setText(liveUser.followStatus == 0 ? "关注" : "取消关注");
        mTvFollow.setSelected(liveUser.followStatus == 0);
        mTvLikeNum.setText(String.valueOf(liveUser.likeNum));
        mTvFollowNum.setText(String.valueOf(liveUser.followNum));
        mTvFansNum.setText(String.valueOf(liveUser.fansNum));
        User loginUser = SessionUtil.getInstance().getLoginUser();
        mTvFollow.setVisibility(null != loginUser && loginUser.id.equals(liveUser.memberId) ? View.GONE : View.VISIBLE);
    }


    @OnClick(R.id.tvFollow)
    protected void follow() {
        if (mFollowOnClickListener != null) {
            mFollowOnClickListener.follow();
        }
    }

    public interface FollowOnClickListener {
        void follow();
    }

}
