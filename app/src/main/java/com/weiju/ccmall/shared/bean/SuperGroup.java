package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

/**
 * @author chenyanming
 * @time 2019/8/14 on 11:34
 * @desc ${TODD}
 */
public class SuperGroup extends BaseModel {

    /**
     * activityBean : {"activityId":"minim","title":"Lorem officia velit ut elit","rule":"do ex","startDate":"sit ullamco","endDate":"eu ad aliquip","status":6.541330376575178E7}
     * productBean : {"productId":"eu","productName":"exercitation tempor cupidatat","status":-7.260696502473336E7,"thumbUrl":"nisi ipsum consequat laboris voluptate"}
     * groupInfo : {"groupCode":"eiusmod cupidatat elit ullamco cillum","num":"incididunt labore Lorem"}
     */

    @SerializedName("activityBean")
    public Product.GroupExtEntity.ActivityEntity activityBean;
    @SerializedName("productBean")
    public Product productBean;
    @SerializedName("groupInfo")
    public GroupInfoEntity groupInfo;

    public static class GroupInfoEntity {
        /**
         * groupCode : eiusmod cupidatat elit ullamco cillum
         * num : incididunt labore Lorem
         */

        @SerializedName("groupCode")
        public String groupCode;
        @SerializedName("num")
        public long num;
    }
}
