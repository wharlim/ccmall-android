package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.PrestoreType;
import com.weiju.ccmall.shared.component.adapter.SprinnerTypeAdapter;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author chenyanming
 * @time 2019/12/17 on 11:36
 * @desc
 */
public class PrestoreDatePopupView extends FrameLayout {

    @BindView(R.id.rvLeft)
    RecyclerView mRvLeft;
    @BindView(R.id.rvRight)
    RecyclerView mRvRight;

    private SelectListener mSelectListener;

    private String mYear;
    private SprinnerTypeAdapter mLeftAdapter;
    private SprinnerTypeAdapter mRightdapter;

    public PrestoreDatePopupView(@NonNull Context context) {
        super(context);
        View view = inflate(context, R.layout.view_prestore_data_popup, this);
        ButterKnife.bind(this, view);

        initLeftList();
        initRightList();
    }

    private void initRightList() {
        mRvRight.setLayoutManager(new LinearLayoutManager(getContext()));
        ArrayList<PrestoreType> prestoreTypes = new ArrayList<>();
//        prestoreTypes.add(new PrestoreType("全年", "00"));
        for (int i = 1; i <= 12; i++) {
            String name = String.format("%02d月", i);
            String value = String.format("%02d", i);
            prestoreTypes.add(new PrestoreType(name, value));
        }

        mRightdapter = new SprinnerTypeAdapter(prestoreTypes);
        mRvRight.setAdapter(mRightdapter);
        mRvRight.setVisibility(VISIBLE);

        mRvRight.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                mRightdapter.selectPosition(position);
                select(mYear + "-" + prestoreTypes.get(position).value);
            }
        });
    }

    private void initLeftList() {
        mRvLeft.setLayoutManager(new LinearLayoutManager(getContext()));
        ArrayList<PrestoreType> prestoreTypes = new ArrayList<>();
//        prestoreTypes.add(new PrestoreType("不限年份", ""));

        int initYear = TimeUtils.string2Date("2017", "yyyy").getYear();
        int currentYear = new Date().getYear();
        LogUtils.e("initYear" + initYear);
        LogUtils.e("currentYear" + currentYear);
        for (int i = currentYear - initYear; i > 0; i--) {
            prestoreTypes.add(new PrestoreType(String.valueOf(2017 + i), String.valueOf(2017 + i)));
        }
        mYear = prestoreTypes.get(0).value;

        mLeftAdapter = new SprinnerTypeAdapter(prestoreTypes);
        mRvLeft.setAdapter(mLeftAdapter);
        mLeftAdapter.setWhiteSelectBgModel(true);
        mRvLeft.addOnItemTouchListener(new OnItemClickListener() {
            @Override
            public void onSimpleItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (mLeftAdapter.getCurrentPosition() == position) {
                    return;
                }
//                if (position == 0) {
//                    select("");
//                    mRvRight.setVisibility(View.INVISIBLE);
//                } else {
//                    mYear = prestoreTypes.get(position).value;
//                    mRvRight.setVisibility(View.VISIBLE);
//                }
                mYear = prestoreTypes.get(position).value;
                mRvRight.setVisibility(View.VISIBLE);
                mRightdapter.selectPosition(0);
                mLeftAdapter.selectPosition(position);
            }
        });
    }

    private void select(String month) {
        if (mSelectListener != null) {
            String text;
            if (StringUtils.isEmpty(month)) {
                text = "不限年份";
            } else {
                String[] items = month.split("-");
                String yearText = items[0] + "年";
                String monthText = "";
                String temp = items[1];
                if (!temp.equals("00")) {
                    monthText = Integer.valueOf(temp) + "月";
                }
                text = yearText + monthText;
            }
            mSelectListener.onSelectDate(month, text);
        }
    }

    public void setSelectListener(SelectListener selectListener) {
        mSelectListener = selectListener;
    }

   public interface SelectListener {
        void onSelectDate(String month, String text);
    }
}
