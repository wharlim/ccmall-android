package com.weiju.ccmall.shared.bean;

/**
 * @author chenyanming
 * @time 2019/12/11 on 16:39
 * @desc
 */
public class XinYongUser {
    public int drawableTop;
    public String title;
    public String desc;

    public XinYongUser(int drawableTop, String title, String desc) {
        this.drawableTop = drawableTop;
        this.title = title;
        this.desc = desc;
    }
}
