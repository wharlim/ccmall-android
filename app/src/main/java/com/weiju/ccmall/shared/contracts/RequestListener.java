package com.weiju.ccmall.shared.contracts;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.contracts
 * @since 2017-03-10
 */
public interface RequestListener<E> {
    void onStart();

    void onSuccess(E result);

    void onError(Throwable e);

    void onComplete();
}
