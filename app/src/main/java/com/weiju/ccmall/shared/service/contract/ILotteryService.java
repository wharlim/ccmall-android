package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.module.xysh.bean.XyshNotice;
import com.weiju.ccmall.shared.bean.LotteryActivityModel;
import com.weiju.ccmall.shared.bean.LotteryWinner;
import com.weiju.ccmall.shared.bean.LuckDraw;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ILotteryService {
    @GET("luckDraw/getActivity")
    Observable<RequestResult<LotteryActivityModel>> getActivity();

    @GET("luckDraw/getLuckDraw")
    Observable<RequestResult<LuckDraw>> getLuckDraw(
            @Query("activityId") String id
    );

    @GET("luckDraw/getLuckDrawHistory")
    Observable<RequestResult<PaginationEntity<LotteryWinner, Object>>> getWinnerList(
            @Query("pageSize") int pageSize,
            @Query("pageOffset") int page
    );

    @POST("luckDraw/acceptPrize")
    @FormUrlEncoded
    Observable<RequestResult<Object>> acceptPrize(
            @Field("drawId") String drawId,
            @Field("addressId") String addressId
    );

    // 获取信用生活公告
    @GET("bulletin/getCcpayBulletin")
    Observable<RequestResult<XyshNotice>> getCcpayBulletin();
}
