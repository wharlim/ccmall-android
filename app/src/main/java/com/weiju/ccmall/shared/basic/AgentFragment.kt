package com.weiju.ccmall.shared.basic

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.weiju.ccmall.R
import com.weiju.ccmall.shared.bean.api.RequestResult
import com.weiju.ccmall.shared.manager.APIManager
import com.weiju.ccmall.shared.manager.ServiceManager
import io.reactivex.Observable

typealias OnSuccess<T> = (postCode: Int, t: T) -> Unit

open class AgentFragment : Fragment() {

    private var loading: FrameLayout? = null

    protected open fun layoutId(): Int {
        return 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var v = view
        if (v == null) {
            val layoutId = layoutId()
            if (layoutId != 0)
                v = inflater.inflate(layoutId, container, false)
        }
        val parent = v?.parent as? ViewGroup?
        parent?.removeView(v)
        return v
    }

    protected fun showLoading() {
        if (view == null)
            return
        if (loading?.childCount != 2) {
            val group = view!!.parent as ViewGroup
            val index = group.indexOfChild(view)
            group.removeViewAt(index)
            if (loading == null) {
                val lp = view!!.layoutParams as ViewGroup.MarginLayoutParams
                loading = FrameLayout(context)
                val lp1 = ViewGroup.MarginLayoutParams(lp.width, lp.height)
                lp1.leftMargin = lp.leftMargin
                lp1.topMargin = lp.topMargin
                lp1.rightMargin = lp.rightMargin
                lp1.bottomMargin = lp.bottomMargin
                loading!!.layoutParams = lp1
                layoutInflater.inflate(R.layout.loading_layout, loading)
            }
            group.addView(loading, index)
            loading!!.addView(view, 0)
        }
    }

    protected fun hideLoading() {
        if (view == null)
            return
        if (loading?.childCount == 2) {
            val group = loading!!.parent as ViewGroup
            val index = group.indexOfChild(loading)
            group.removeViewAt(index)
            val view = loading!!.getChildAt(0)
            loading!!.removeView(view)
            group.addView(view, index)
        }
    }

    fun <T> request(onSuccess: OnSuccess<T>, request: Observable<RequestResult<T>>, postCode: Int = 0, isShowLoading: Boolean = true){
        APIManager.startRequest(request, object : BaseRequestListener<T>(){

            override fun onSt() {
                if (isShowLoading)
                    showLoading()
            }

            override fun onS(result: T) {
                if(isShowLoading)
                    hideLoading()
                onSuccess(postCode, result)
            }

            override fun onE(e: Throwable?) {
                if(isShowLoading)
                    hideLoading()
                onError(postCode, e)
            }
        }, null)
    }

    open fun onError(postCode: Int, e: Throwable?) {

    }

    protected fun <V> service(c: Class<V>): V{
        return ServiceManager.getInstance().createService<V>(c)
    }

    protected fun <V> service2(c: Class<V>): V{
        return ServiceManager.getInstance().createService2<V>(c)
    }
}
