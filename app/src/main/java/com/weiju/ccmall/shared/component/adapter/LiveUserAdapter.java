package com.weiju.ccmall.shared.component.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.util.FrescoUtil;


/**
 * @author chenyanming
 * @time 2019/12/23 on 17:31
 * @desc
 */
public class LiveUserAdapter extends BaseQuickAdapter<LiveUser, BaseViewHolder> {
    public LiveUserAdapter() {
        super(R.layout.item_live_user);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveUser item) {
        FrescoUtil.setImageSmall(helper.getView(R.id.ivAvatar), item.headImage);
        helper.setText(R.id.tvName, item.nickName);
        helper.setText(R.id.tvFans,String.format("%s粉丝",item.fansNum));
    }
}
