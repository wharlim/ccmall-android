package com.weiju.ccmall.shared.basic;

public interface BaseCallback<T> {

    void callback(T data);
}
