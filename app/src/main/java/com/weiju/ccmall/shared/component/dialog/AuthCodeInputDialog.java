package com.weiju.ccmall.shared.component.dialog;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.InputType;

public class AuthCodeInputDialog extends TitleContentEditDialog {
    public AuthCodeInputDialog(Context context, String title, String message, String etHide, OnConfirmListener listener) {
        super(context, title, message, etHide, listener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mEtMessage.setMaxLines(1);
        mEtMessage.setInputType(InputType.TYPE_CLASS_NUMBER);
        mEtMessage.setFilters(new InputFilter[]{new InputFilter.LengthFilter(6)});

        mConfirmBtn.setTextColor(Color.parseColor("#EBB95B"));
    }
}
