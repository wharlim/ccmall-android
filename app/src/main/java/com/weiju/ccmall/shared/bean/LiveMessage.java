package com.weiju.ccmall.shared.bean;

/**
 * @author chenyanming
 * @time 2020/1/6 on 11:17
 * @desc
 */
public class LiveMessage {

    public String name;
    public String content;
    public String headImage;
    public boolean showAvatar;
    public boolean isWelcome;
    public boolean  isNotice;
    public String memberId;

}
