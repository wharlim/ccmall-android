package com.weiju.ccmall.shared.service;

import android.content.Context;
import android.content.Intent;

import com.sobot.chat.SobotApi;
import com.sobot.chat.api.enumtype.SobotChatTitleDisplayMode;
import com.sobot.chat.api.model.Information;
import com.weiju.ccmall.module.auth.Config;
import com.weiju.ccmall.module.user.LoginActivity;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.util.SessionUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.service
 * @since 2017-07-12
 */
public class CsService {

    public static void startSobot(Context context, String storeName) {
        if (!SessionUtil.getInstance().isLogin()) {
            ToastUtil.error(Config.NET_MESSAGE.NO_LOGIN);
            context.startActivity(new Intent(context, LoginActivity.class));
            return;
        }
        Information info = new Information();
        //info.setColor("#f51861");
        //用户编号
        //注意：uid为用户唯一标识，不能传入一样的值
        User user = SessionUtil.getInstance().getLoginUser();
        info.setUid(user.openId);
        //用户昵称，选填
        info.setUname(user.nickname);
        //用户姓名，选填
        info.setRealname(user.userName);
        //用户电话，选填
        info.setTel(user.phone);
        //自定义头像，选填
        info.setFace(user.avatar);
        //用户性别 0.男，1.女，3.未知
        //info.setSex("3");
        //用户备注，选填
        info.setRemark("");
        //对话页标题，选填
        info.setVisitTitle("");
        //对话页路径，选填
        info.setVisitUrl("");
        SobotApi.setChatTitleDisplayMode(context, SobotChatTitleDisplayMode.ShowFixedText, storeName);
    }
}
