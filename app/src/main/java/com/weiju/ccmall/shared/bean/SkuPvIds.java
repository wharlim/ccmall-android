package com.weiju.ccmall.shared.bean;

import com.google.common.collect.Sets;
import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

import java.util.Collections;
import java.util.HashSet;


public class SkuPvIds extends BaseModel {
    @SerializedName("skuId")
    public String skuId;
    @SerializedName("propertyValueIds")
    public String pvIds;

    public boolean isMatch(String ids) {
        return Sets.difference(new HashSet<>(Collections.singletonList(pvIds)), new HashSet<>(Collections.singleton(ids))).isEmpty();
    }
}
