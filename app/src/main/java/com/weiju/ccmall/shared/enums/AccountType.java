package com.weiju.ccmall.shared.enums;

import java.io.Serializable;

/**
 * @author chenyanming
 * @time 2019/1/6 on 17:28
 * @desc 账户类型
 */
public enum AccountType implements Serializable {

    Balance("余额"),
    CCM("CCM"),
    CB("C券"),
    Profit("收入"),
    ShopMoney("购物券"),
    GoldenTicket("金券"),
    PSP("PSP值"),
    TSP("TSP值"),
    PCP("PCP值"),
    PickUpCoupon("提货券");


    // name
    private final String name;

    public String getName() {
        return name;
    }

    AccountType(String name) {
        this.name = name;
    }


//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(final Parcel dest, final int flags) {
//        dest.writeInt(ordinal());
//    }
//
//    public static final Creator<AccountType> CREATOR = new Creator<AccountType>() {
//        @Override
//        public AccountType createFromParcel(final Parcel source) {
//            return AccountType.values()[source.readInt()];
//        }
//
//        @Override
//        public AccountType[] newArray(final int size) {
//            return new AccountType[size];
//        }
//    };
}
