package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.PickUp;
import com.weiju.ccmall.shared.bean.PickUpEx;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.VouchersTransfer;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author chenyanming
 * @time 2019/8/2 on 10:57
 * @desc ${TODD}
 */
public interface IPickUpService {

    @GET("vouchers/getSumList")
    Observable<RequestResult<PaginationEntity<PickUp, Object>>> getSumList(@Query("pageOffset") int page,
                                                                           @Query("pageSize") int pageSize);

    @GET("vouchers/getUnusedList")
    Observable<RequestResult<PaginationEntity<PickUp, PickUpEx>>> getUnusedList(@Query("pageOffset") int page,
                                                                                @Query("pageSize") int pageSize,
                                                                                @Query("vouchersId") String vouchersId);

    @GET("vouchers/getUsedList")
    Observable<RequestResult<PaginationEntity<PickUp, Object>>> getUsedList(@Query("pageOffset") int page,
                                                                            @Query("pageSize") int pageSize);

    @GET("product/getSkuListByVouchersId")
    Observable<RequestResult<PaginationEntity<SkuInfo, Object>>> getSkuListByVouchersId(@Query("pageOffset") int page,
                                                                                        @Query("pageSize") int pageSize,
                                                                                        @Query("vouchersId") String vouchersId);

    @GET("vouchers/queryListByType")
    Observable<RequestResult<PaginationEntity<PickUp, Object>>> queryListByType(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize,
            @Query("type") int type);

    @GET("vouchers/queryList")
    Observable<RequestResult<PaginationEntity<PickUp, PickUpEx>>> queryList(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize);

    @FormUrlEncoded
    @POST("vouchers/transfer")
    Observable<RequestResult<VouchersTransfer>> transfer(
            @Field("payeePhone") String payeePhone,
            @Field("password") String password,
            @Field("checkNumber") String checkNumber,
            @Field("goodsCodes") String goodsCode
    );
}
