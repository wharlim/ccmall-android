package com.weiju.ccmall.shared.bean;

import android.text.TextUtils;

import com.blankj.utilcode.utils.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.NewerConfigEntity;
import com.weiju.ccmall.shared.constant.AppTypes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SkuInfo implements Serializable {
    public boolean isSelected;

    @SerializedName(value = "originId")
    public String originId;

    public boolean isJpkProduct() {
        return !TextUtils.isEmpty(originId);
    }

    @SerializedName(value = "skuId", alternate = "goodsId")
    public String skuId;
    @SerializedName("memberSkuId")
    public String memberSkuId;
    @SerializedName("productId")
    public String productId;
    @SerializedName(value = "skuName", alternate = "title")
    public String name;
    @SerializedName("productName")
    public String productName;
    @SerializedName(value = "intro", alternate = "introduce")
    public String desc;
    @SerializedName("stock")
    public int stock;
    @SerializedName(value = "retailPrice", alternate = "couponPrice")
    public long retailPrice;
    @SerializedName(value = "marketPrice", alternate = "originPrice")
    public long marketPrice;
    @SerializedName("shippingPrice")
    public long shippingPrice;
    @SerializedName("price")
    public long price;
    @SerializedName("saleCount")
    public long sales;
    @SerializedName("weight")
    public long weight;
    @SerializedName("quantity")
    public int quantity;
    @SerializedName("discountStatus")
    public int discountStatus;
    @SerializedName(value = "thumbUrl", alternate = "image")
    public String thumb;
    @SerializedName("storeId")
    public String storeId;
    @SerializedName("sobotId")
    public String sobotId;
    @SerializedName("storeName")
    public String storeName;
    @SerializedName("hasCoupon")
    public int hasCoupon;
    @SerializedName("properties")
    public String properties;
    @SerializedName("collectStatus")
    public int isFav;
    @SerializedName("images")
    public List<String> images;
    @SerializedName("presents")
    public List<Presents> presents = new ArrayList<>();
    @SerializedName("tags")
    public List<Tag> tags = new ArrayList<>();
    @SerializedName(value = "costPrice", alternate = "quanPrice")
    public long costPrice;
    @SerializedName("coin")
    public long coin;
    @SerializedName("countRate")
    public long countRate;
    @SerializedName("countRateExc")
    public String countRateExc;
    @SerializedName("anchorDistributionProfit")
    public String anchorDistributionProfit;
    public String getAnchorDistributionProfit() {
        if (TextUtils.isEmpty(anchorDistributionProfit)) {
            return "0";
        }
        return anchorDistributionProfit;
    }
    public double countRateExc() {
        double ret = 0;
        try {
            ret = Double.valueOf(countRateExc);
        } catch (Exception e) {}
        return ret;
    }
    @SerializedName("ticket")
    public long ticket;
    /**
     * 2 礼包
     */
    @SerializedName("productType")
    public int productType;
    @SerializedName("goldenTicket")
    public long goldenTicket;
    @SerializedName("distributionProfit")
    public long distributionProfit;
    @SerializedName("skuEarnings")
    public String skuEarnings;
    @SerializedName("storeOwnerMemberId")
    public String storeOwnerMemberId;

    public boolean isLiveStoreProduct() {
        return productType == 28;//直播商品
    }

    /**
     * buyScore : 1
     */
    @SerializedName("buyScore")
    public int buyScore;
    /**
     * vipTypePrices : [{"vipType":4,"price":12000},{"vipType":3,"price":14000},{"vipType":2,"price":16000},{"vipType":1,"price":18000},{"vipType":0,"price":20000}]
     */
    @SerializedName("vipTypePrices")
    public List<CartItem.VipTypePricesEntity> vipTypePrices;
    /**
     * currentVipTypePrice : 15000
     */
    @SerializedName("currentVipTypePrice")
    public long currentVipTypePrice;
    @SerializedName("totalSaleCount")
    public long totalSaleCount;
    /**
     * 0：已下架
     */
    @SerializedName("status")
    public int status;
    /**
     * 0、默认，1跨境购产品
     * 2、团购
     * 3、会员特卖
     * 5、超级团购
     */
    @SerializedName("extType")
    public int extType;
    @SerializedName("hasDifferentPrice")
    public int hasDifferentPrice;
    @SerializedName("groupSkuInfo")
    public GroupSkuInfoEntity groupSkuInfo;

    /**
     * 分享最大价
     */
    @SerializedName("maxPrice")
    public long maxPrice;
    /**
     * 分享最低价
     */
    @SerializedName("minPrice")
    public long minPrice;
    @SerializedName("spec")
    public String spec;
    /**
     * 1:跨境购
     */
    public int isCross;
    @SerializedName("userTypeStr")
    public String userTypeStr;
    @SerializedName("sellBegin")
    public String sellBegin;
    @SerializedName("sellEnd")
    public String sellEnd;
    @SerializedName("halfSkuInfo")
    public HalfSkuInfoEntity halfSkuInfo;
    @SerializedName("activityTag")
    public List<ActivityTagEntity> activityTag;

    @SerializedName("type")
    public int type;
    @SerializedName("newerConfig")
    public NewerConfigEntity newerConfig;

    @SerializedName("createProDate")
    public String createProDate;

    @SerializedName("orderCode")
    public String orderCode;

    @SerializedName("lessTime")
    public long lessTime;

    @SerializedName("pushId")
    public int pushId;

    @SerializedName("consumeCcm")
    public long consumeCcm; // 兑换现金消耗的CCM

    @SerializedName("exchangeMoney")
    public long exchangeMoney; // 消耗CCM兑换的金额

    @SerializedName("activityBrief")
    public List<String> activityBrief;

    // 密码pi商品
    @SerializedName("skuCat")
    public int skuCat;

    public boolean isCatDefault() {
        return skuCat == 1;
    }
    public boolean isCatPi() {
        return skuCat == 2;
    }


    public static class GroupSkuInfoEntity implements Serializable {
        /**
         * activityId : b44c1c23fc1f4f6c8384e7c22ed409e5
         * productId : 7a90e4080ef94f8ba45a9903063a5633
         * skuId : e6af3767684449418af03cd83d6b722a
         * groupPrice : 45
         * minBuyNum : 1
         * maxBuyNum : 2
         * groupLeaderReturn : 0
         */

        @SerializedName("activityId")
        public String activityId;
        @SerializedName("productId")
        public String productId;
        @SerializedName("skuId")
        public String skuId;
        @SerializedName("groupPrice")
        public long groupPrice;
        @SerializedName("minBuyNum")
        public int minBuyNum;
        @SerializedName("maxBuyNum")
        public int maxBuyNum;
        @SerializedName("groupLeaderReturn")
        public int groupLeaderReturn;

    }

    public long getTypePrice(int vipType) {
        long price = retailPrice;
        if (vipTypePrices != null && vipTypePrices.size() > 0) {
            for (CartItem.VipTypePricesEntity vipTypePrice : vipTypePrices) {
                if (vipTypePrice.vipType == vipType) {
                    price = vipTypePrice.price;
                }
            }
        }
        return price;
    }

    /**
     * @return vip 会员返利价
     */
    public long getVipRefundPrice() {
        return getTypePrice(AppTypes.FAMILY.MEMBER_ZUNXIANG) - getTypePrice(AppTypes.FAMILY.MEMBER_TIYAN);
    }

    /**
     * @return 是否显示成为店主的 view
     */
    public boolean isShowVipView() {
        return hasDifferentPrice == 1;
    }

    public static class HalfSkuInfoEntity implements Serializable {
        /**
         * activityId : 964a037c80a24e738f6401eb686cadd5
         * title : 半价活动
         * startDate : 2018-09-13 10:11:16
         * endDate : 2019-03-09 10:11:16
         */

        @SerializedName("activityId")
        public String activityId;
        @SerializedName("title")
        public String title;
        @SerializedName("startDate")
        public String startDate;
        @SerializedName("endDate")
        public String endDate;
    }

    public static class ActivityTagEntity implements Serializable {
        @SerializedName("type")
        public int type;
        @SerializedName("iconUrl")
        public String iconUrl;
        @SerializedName("title")
        public String title;
        @SerializedName("content")
        public String content;

        public String getActivityText(boolean singLine) {
            if (StringUtils.isEmpty(content)) {
                return title;
            } else {
                return content;
            }
//            return singLine ? title : content;
        }

        public String getTypeActivityText() {
            String typeStr = "";
            switch (type) {
                case 0:
                    typeStr = "多买优惠：";
                    break;
                case 1:
                    typeStr = "限购：";
                    break;
                default:
                    typeStr = "满减：";
                    break;
            }
            return typeStr + getActivityText(true);
        }
    }

    public boolean isBanjia() {
//        return true;
        return extType == 4;
    }
}
