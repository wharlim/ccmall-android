package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.contracts.IRegion;

import java.io.Serializable;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.bean
 * @since 2017-07-06
 */
public class RegionProvince implements Serializable, IRegion {

    @SerializedName("provinceId")
    public String id;
    @SerializedName("province")
    public String name;

    @Override
    public String getType() {
        return "province";
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getId() {
        return id;
    }
}
