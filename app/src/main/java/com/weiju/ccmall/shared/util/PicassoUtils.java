package com.weiju.ccmall.shared.util;

import android.widget.ImageView;

import com.blankj.utilcode.utils.StringUtils;
import com.squareup.picasso.Picasso;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/9/14.
 */
public class PicassoUtils {

    public static void loadImage(ImageView imageView, String imgUrl) {
        if (StringUtils.isEmpty(imgUrl)) {
            imageView.setImageResource(R.drawable.default_image);
        } else {
            Picasso.with(MyApplication.getInstance().getApplicationContext()).load(imgUrl).error(R.drawable.default_image).into(imageView);
        }
    }

    public static void loadImage(ImageView imageView, String imgUrl, int width, int height) {
        String ossImageUrl = getOssImageUrl(imgUrl, width, height);
        loadImage(imageView, ossImageUrl);
    }


    private static String getOssImageUrl(String imageUrl, int imgWidth, int height) {
        if (!imageUrl.contains("http://static")) {
            return imageUrl;
        } else {
            return String.format("%s?x-oss-process=image/resize,m_lfit,limit_0,w_%d,h_%d/quality,Q_85", imageUrl, imgWidth, height);
        }
    }
}
