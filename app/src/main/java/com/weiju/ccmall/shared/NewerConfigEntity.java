package com.weiju.ccmall.shared;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author chenyanming
 * @time 2019/2/25 on 16:07
 * @desc  新人专区产品状态
 */
public class NewerConfigEntity implements Serializable {

    /**
     * nextBuyDate :
     * locked : false
     * payByMoney : true
     */

    @SerializedName("nextBuyDate")
    public String nextBuyDate;
    @SerializedName("locked")
    public boolean locked;
    @SerializedName("payByMoney")
    public boolean payByMoney;
    @SerializedName("canBuy")
    public boolean canBuy;
}
