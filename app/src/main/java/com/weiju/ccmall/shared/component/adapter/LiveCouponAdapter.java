package com.weiju.ccmall.shared.component.adapter;


import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.LiveCoupon;
import com.weiju.ccmall.shared.util.MoneyUtil;


/**
 * @author chenyanming
 * @date 2020/2/20.
 */
public class LiveCouponAdapter extends BaseQuickAdapter<LiveCoupon, BaseViewHolder> {

    public LiveCouponAdapter() {
        super(R.layout.item_live_coupon);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveCoupon item) {
        helper.setText(R.id.tvMoney, MoneyUtil.centToYuan¥StrNoZero(item.cost));
        helper.setText(R.id.tvContent, item.title);
        helper.setText(R.id.tvDate, String.format("有效期：%s", item.limitEndDate));
        helper.setText(R.id.tvStock, String.format("库存%s", item.stock));
    }
}
