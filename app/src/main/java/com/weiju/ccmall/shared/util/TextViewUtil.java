package com.weiju.ccmall.shared.util;

import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ScaleXSpan;
import android.view.View;
import android.widget.TextView;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.TimeUtils;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.NewerConfigEntity;
import com.weiju.ccmall.shared.bean.Challenge;
import com.weiju.ccmall.shared.bean.Tag;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;

public class TextViewUtil {

    public static void addThroughLine(TextView tv) {
        tv.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        tv.getPaint().setAntiAlias(true);
    }

    public static void setTagColorTitle(TextView tvTitle, String content, String tag) {
        String str = "<font color='#f51861'>" + tag + "</font><font color='#333333'>" + content + "</font>";
        tvTitle.setText(android.text.Html.fromHtml(str));
    }

    private static SpannableStringBuilder createTagSpan(String tag, float textSize) {
        tag = " " + tag + " ";
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(tag);
        spannableStringBuilder.setSpan(
                new IconTextSpan(MyApplication.getInstance().getApplicationContext(), R.color.red, tag),
                spannableStringBuilder.length() - tag.length(),
                spannableStringBuilder.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        );
        return spannableStringBuilder;
    }

    public static void setTagTitle(TextView tvTitle, String title, List<Tag> tags) {
        if (tags == null || tags.size() == 0) {
            tvTitle.setText(title);
            LogUtils.e("没有 tag");
        } else {
            LogUtils.e("有 tag");
            SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
            for (Tag tag : tags) {
                spannableStringBuilder.append(createTagSpan(tag.name, ConvertUtil.dip2px(11)));
            }
            if (com.blankj.utilcode.utils.StringUtils.isEmpty(title)) {
                spannableStringBuilder.append(tvTitle.getText().toString());
            } else {
                spannableStringBuilder.append(title);
            }
            tvTitle.setText(spannableStringBuilder);
        }
    }

    public static void setNewerBuyDate(Context context, TextView textView, NewerConfigEntity newerConfigEntity, String format1, String format2, boolean isDetail){
        if (newerConfigEntity==null){
            textView.setVisibility(View.GONE);
            return;
        }
        if (!newerConfigEntity.canBuy&& !isDetail){
            textView.setBackgroundResource(R.drawable.ic_product_sale_black_two);
            textView.setText("不可购买");
            return;
        }

        if (TextUtils.isEmpty(newerConfigEntity.nextBuyDate)){
            textView.setVisibility(View.GONE);
            return;
        }

        long currentTimeMillis = System.currentTimeMillis();
        long nextBuyDateL = TimeUtils.string2Millis(newerConfigEntity.nextBuyDate, "yyyy-MM-dd");
        long millisUntilFinished = nextBuyDateL - currentTimeMillis;
        millisUntilFinished /= 1000;
        long hours = millisUntilFinished / 3600 ;
        if (hours>24){
            textView.setText(String.format(Locale.getDefault(), format1,newerConfigEntity.nextBuyDate ));
            if (isDetail){
                textView.setBackgroundColor(context.getResources().getColor(R.color.text_black));
            }else {
                textView.setBackgroundResource(R.drawable.ic_product_sale_black_two);
            }
        }else {

//            long minutes = millisUntilFinished / 60 % 60;
//            long seconds = millisUntilFinished % 60;
//            textView.setText(String.format(Locale.getDefault(), format2,hours,minutes,seconds));
            textViewCountDown(textView,millisUntilFinished,newerConfigEntity,isDetail);

            if (isDetail){
                textView.setBackgroundColor(context.getResources().getColor(R.color.red));
            }else {
                textView.setBackgroundResource(R.drawable.ic_product_sale_red);
            }
        }
    }


    public  static void textViewCountDown(final TextView textView, final long timeCount, final NewerConfigEntity newerConfigEntity, final boolean isDetail) {
        if (timeCount<=0){
            textView.setVisibility(View.GONE);
            return;
        }
        Observable
                .interval(0, 1, TimeUnit.SECONDS)
                .take(timeCount)
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<Long, Long>() {
                    @Override
                    public Long apply(@NonNull Long aLong) throws Exception {
                        return timeCount - aLong;

                    }
                })
                .subscribe(new Observer<Long>() {

                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull Long aLong) {
                        long downTime = ((TimeUtils.string2Millis(newerConfigEntity.nextBuyDate,"yyyy-MM-dd")) - System.currentTimeMillis()) / 1000;
                        long hours = downTime / 3600%24;
                        long minutes = downTime / 60 % 60;
                        long seconds = downTime % 60;

                        String time = String.format("%s:%s:%s", hours < 10 ? "0" + hours : hours, minutes < 10 ? "0" + minutes : minutes,
                                seconds < 10 ? "0" + seconds : seconds);

                        if (isDetail){
//                            String  format = String.format("<font color=\"#ffffff\" size=\"14sp\">该商品为新人专区商品,</font> " +
//                                            "<font color=\"#ffffff\"  size=\"12sp\"> 还剩</font> <font color=\"#ffffff\"  size=\"14sp\"> %s " +
//                                            "</font> <font color=\"#ffffff\"  size=\"12sp\"> 可购买 </font>",
//                                    time);
                            String format=String.format("该商品为新人专区商品,还剩 %s 可购买",time);
                            SpannableString spannableString = new SpannableString(format);
                            spannableString.setSpan(new AbsoluteSizeSpan(24),11,13, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            spannableString.setSpan(new AbsoluteSizeSpan(24),format.length()-3,format.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                         //   textView.setText(Html.fromHtml(format));
                            textView.setText(spannableString);
                        }else {
                            textView.setText(String.format("还剩%s可购买",
                                    time));
                        }
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {
                        textView.setVisibility(View.GONE);
                    }
                });
    }
}
