package com.weiju.ccmall.shared.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.orhanobut.logger.Logger;
import com.weiju.ccmall.module.product.ProductQrcodeShowActivity;
import com.weiju.ccmall.shared.basic.BaseWebViewClient;
import com.weiju.ccmall.shared.service.JavascriptService;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Locale;

import top.zibin.luban.Luban;

public class WebViewUtil {

    public static WebView createNewWebView(Context context) {
        return createNewWebView(context, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface", "JavascriptInterface"})
    public static WebView createNewWebView(Context context, ViewGroup.LayoutParams layoutParams) {
        WebView webView = new WebView(context);
        webView.setLayoutParams(layoutParams);
        WebSettings settings = webView.getSettings();
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setDisplayZoomControls(false);
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        settings.setLoadWithOverviewMode(true);
        webView.addJavascriptInterface(new JavascriptService(), "bridge");
        webView.setWebViewClient(new BaseWebViewClient());
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return true;
            }
        });
        return webView;
    }

    public static void configWebView(WebView webView) {
        WebSettings settings = webView.getSettings();
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setDisplayZoomControls(false);
        settings.setSupportZoom(false);
        settings.setBuiltInZoomControls(false);
        settings.setLoadWithOverviewMode(true);
        settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        webView.addJavascriptInterface(new JavascriptService(), "bridge");
        webView.setWebViewClient(new BaseWebViewClient());
        webView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return true;
            }
        });
    }

    public static BaseWebViewClient getOverrideAllUrlLoadingClient() {
        return new BaseWebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }
        };
    }

    public static void clearWebViewResource(@NonNull WebView webView) {
        Logger.e("清除 WebView 资源");
        webView.removeAllViews();
        ((ViewGroup) webView.getParent()).removeView(webView);
        webView.setTag(null);
        webView.clearHistory();
        webView.destroy();
    }

    public static boolean compileUri(Uri uri) {
        Logger.e("Request Host:" + uri.getHost());
        return true;
    }

    public static void loadDataToWebView(WebView webView, String content) {
        loadDataToWebView(webView, content, null);
    }

    public static void loadDataToWebView(WebView webView, String content, String baseUrl) {
        content = String.format("<!DOCTYPE html><html lang=\"zh-CN\"><head><meta name=\"viewport\" content=\"width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no\">" +
                "<style>body{padding:0;margin:0;display:flex;flex-direction:column;align-items:stretch;}" +
                "img{display:block;width:100%% !important;height:auto !important;}table{width:100%% !important;height:auto !important;}" +
                "</style></head><body>%s</body></html>", content);
        webView.loadDataWithBaseURL(baseUrl, content, "text/html", "UTF-8", null);
    }

    public static void loadDataToWebViewForJkpDetail(WebView webView, String content, String baseUrl) {
        content = String.format("<!DOCTYPE html><html lang=\"zh-CN\"><head><meta name=\"viewport\" content=\"width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no\">" +
                "<style>body{padding:0;margin:0;display:flex;flex-direction:column;align-items:stretch;}" +
                "img{display:block;width:100%% !important;height:auto !important;}table{width:100%% !important;height:auto !important;}" +
                "div{width:100%% !important;}" +
                "</style></head><body>%s</body></html>", content);
        webView.loadDataWithBaseURL(baseUrl, content, "text/html", "UTF-8", null);
    }

    public static File shotWebView(WebView webView) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                // Android5.0以上
                float scale = webView.getScale();
                int width = webView.getWidth();
                int height = (int) (webView.getContentHeight() * scale + 0.5);
                Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);
                webView.draw(canvas);

                // 保存图片
                return savePicture(webView.getContext(), bitmap);
            } else {
                // Android5.0以下
                Picture picture = webView.capturePicture();
                int width = picture.getWidth();
                int height = picture.getHeight();
                if (width > 0 && height > 0) {
                    Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);
                    picture.draw(canvas);

                    // 保存图片
                    return savePicture(webView.getContext(), bitmap);
                }
                return null;
            }
        } catch (OutOfMemoryError oom) {
            ToastUtil.error("截图失败!");
            return null;
        }
    }

    /**
     * 保存图片
     *
     * @param context
     * @param bitmap
     */
    public static File savePicture(final Context context, Bitmap bitmap) {

        final File savedImageFile = ProductQrcodeShowActivity.getSaveFile();
        ProductQrcodeShowActivity.saveBitmapFile(bitmap, savedImageFile, false);

        return savedImageFile;
    }
}
