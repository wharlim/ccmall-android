package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.CouponReceive;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.util.MoneyUtil;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
public class LiveCouponReceiveFailDialog   extends Dialog {

    public LiveCouponReceiveFailDialog(@NonNull Context context) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_coupon_receive_fail);
        ButterKnife.bind(this);
    }


    @OnClick({R.id.ivClose})
    protected void close() {
        dismiss();
    }

    @Override
    public void dismiss() {
        EventBus.getDefault().post(new EventMessage(Event.couponDialogDismiss));
        super.dismiss();
    }
}
