package com.weiju.ccmall.shared.component;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.common.AdvancedCountdownTimer;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.component
 * @since 2017-03-10
 */
public class CaptchaBtn extends AppCompatTextView {

    private AdvancedCountdownTimer countdownTimer;

    private long mCountTime = 60000;
    private long mInterval = 200;
    private String mDefaultText = "获取验证码";

    public CaptchaBtn(Context context) {
        super(context);
    }

    public CaptchaBtn(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CaptchaBtn(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CaptchaBtn);
        mCountTime = typedArray.getInt(R.styleable.CaptchaBtn_cb_count_time, (int) mCountTime);
        mInterval = typedArray.getInt(R.styleable.CaptchaBtn_cb_interval, (int) mInterval);
        mDefaultText = attrs.getAttributeValue(android.R.attr.text);
        typedArray.recycle();
    }

    public void stop() {
        setText(mDefaultText);
        setEnabled(true);
        setClickable(true);
        countdownTimer.cancel();
    }

    public void start() {
        setText(String.format("%s s", mCountTime / 1000));
        setEnabled(false);
        setClickable(false);
        countdownTimer = new AdvancedCountdownTimer(mCountTime, mInterval) {

            @Override
            public void onTick(long millisUntilFinished, int percent) {
                setText(String.format("%s s", millisUntilFinished / 1000));
                if (millisUntilFinished / 1000 == 0) {
                    onFinish();
                }
            }

            @Override
            public void onFinish() {
                stop();
            }
        }.start();
    }
}
