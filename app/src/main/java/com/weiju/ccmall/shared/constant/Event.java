package com.weiju.ccmall.shared.constant;

/**
 * 事件枚举，用于 EventBus
 * Created by JayChan on 2016/12/14.
 */
public enum Event {
    /**
     * EvenBus事件
     */
    wxLoginCancel,
    wxLoginSuccess,
    loginSuccess,
    goToLogin,
    newRetailPaySuccess,//嗨购确认支付

    confirmUpgrade,//
    isReadHigou,//已经阅读嗨购须知

    select2Fragment,

    hideLoading,
    toastErrorMessage,
    networkDisconnected,
    networkConnected,
    showAlert,
    inviterUpdate,
    showLoading,
    cartAmountUpdate,
    updateAvatar,
    updateNickname,
    selectAddress,
    changeCategory,
    viewCategory,
    viewNearStore,
    viewCart,
    viewUserCenter,
    selectCartItem,
    alipayResponse,
    regionSelect,
    deleteAddress, saveAddress, cancelOrder, finishOrder, selectImage2Upload, refundOrder, paySuccess, refundExpressSubmit, commentFinish, transferSuccess, viewHome, xinyong, logout, createOrderSuccess, selectLotteryAddress, acceptPrizeSuccess, sendSelectDialog, PUBLISH_NEW, addCommond, addSupport, cancelSupport, PUBLISH_EDIT_FINISH, countDownComplete, joinChallenge, exchSuccess, setOAuthTokenSuccess, onKeyDown,
    goMain, transToGoldSuccess, challengeTrans, joinProduct, delectProduct, orderChange, bankChange,
    liveRoomChange, askPermissions, liveUserRefresh, addLiveProduct, liveLike, dialogDismiss, applyCard, deleteRepayPlan, goToLoginRongIM,authChannelSuccess,
    liveReportSuccess, couponDialogDismiss, sendCouponSuccess,loginError, deleteLive,couponDialogShow,openNewActivity,
    selectImage2UploadHeader, selectImage2UploadFooter, selectClassify, signApplySuccess, ApplyPaySuccess, liveStoreProductUpdate,
    goToLiveHome, liveHomeMessageUpdate, worldAddOrderSuccess, creditLifeUpgradeSuccess,
    chooseLiveGoods, taobaoAuthorize, startLiveSuccess, isEditLoginPwdSuccess

}
