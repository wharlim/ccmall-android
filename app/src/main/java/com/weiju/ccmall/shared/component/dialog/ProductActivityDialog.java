package com.weiju.ccmall.shared.component.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.blankj.utilcode.utils.SizeUtils;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.product.adapter.ProductActivityAdapter;
import com.weiju.ccmall.shared.bean.ProductActivityModel;
import com.weiju.ccmall.shared.decoration.SpacesItemDecoration;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/9/14.
 */
public class ProductActivityDialog extends BottomSheetDialog {

    @BindView(R.id.ivClose)
    ImageView mIvClose;
    @BindView(R.id.rvActivity)
    RecyclerView mRvActivity;

    public ProductActivityDialog(@NonNull Context context) {
        super(context);
        setContentView(R.layout.dialog_product_activity);
        ButterKnife.bind(this);
    }

    public void setData(ArrayList<ProductActivityModel> activityModels) {
        ProductActivityAdapter productActivityAdapter = new ProductActivityAdapter(activityModels, true);
        mRvActivity.setLayoutManager(new LinearLayoutManager(getContext()));
        mRvActivity.addItemDecoration(new SpacesItemDecoration(SizeUtils.dp2px(10)));
        mRvActivity.setAdapter(productActivityAdapter);
    }

    @OnClick(R.id.ivClose)
    public void onViewClicked() {
        dismiss();
    }
}
