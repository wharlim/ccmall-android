package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 *
 *
 * <p>
 * Created by zjm on 2018/8/28.
 */
public class GuideModel {


    @SerializedName("images")
    public List<String> images;
}
