package com.weiju.ccmall.shared.util;

import android.content.Context;
import android.content.res.Resources;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.StringUtils;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.CartItem;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.bean.NoticeListModel;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.page.bean.BasicData;
import com.weiju.ccmall.shared.page.bean.ProductData;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ConvertUtil {

    public static int dip2px(float dip) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dip * scale + 0.5f);
    }

    public static int dip2px(int dip) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dip * scale + 0.5f);
    }

    public static int px2dip(int pixel) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (pixel / scale + 0.5f);
    }

    public static int convertHeight(Context context, int width, int height) {
        return (int) (CommonUtil.getScreenWidth(context) * 1.0f / width * height);
    }

    public static double cent2yuan(long cent) {
        BigDecimal bigDecimal = BigDecimal.valueOf(cent);
        return bigDecimal.divide(BigDecimal.valueOf(100)).doubleValue();
    }

    /**
     * 智能显示价格 将分转成元
     *
     * @param skuInfo
     * @return 22.2
     */
    public static double cent2yuan(SkuInfo skuInfo) {
        long price = skuInfo.retailPrice;
        if (SessionUtil.getInstance().isLogin()) {
            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (loginUser.isShopkeeper()) {
                price = skuInfo.retailPrice;
            } else {
                price = skuInfo.marketPrice;
            }
        }
        return cent2yuan(price);
    }

    /**
     * @param skuInfo
     * @return
     */
    public static long cent2CurrentCent(CartItem skuInfo) {
        long price = skuInfo.retailPrice;
        if (SessionUtil.getInstance().isLogin()) {
            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (loginUser.isShopkeeper()) {
                price = skuInfo.retailPrice;
            } else {
                price = skuInfo.marketPrice;
            }
        }
        return price;
    }

    /**
     * 将分转换成元，结尾删掉 .00
     *
     * @param cent 230  300
     * @return 2.30 3
     */
    public static String cent2yuanNoZero(long cent) {
//        return String.format(Locale.getDefault(), "%.2f", cent2yuan(cent)).replace(".00", "");
        if (cent == 0) {
            return "0";
        }
        String centStr = String.valueOf(cent);
        if (centStr.endsWith("00")) {
            return cleanZero(String.valueOf(cent / 100));
        } else {
            return cleanZero(String.format("%.2f", cent2yuan(cent)));
        }
    }

    /**
     * 根据店主或者普通成员，智能显示价格
     *
     * @param context
     * @param skuInfo
     * @return ¥12.2
     */
    public static String centToCurrency(Context context, SkuInfo skuInfo) {
        if (skuInfo == null) {
            return "";
        }
        long price = skuInfo.retailPrice;
        if (SessionUtil.getInstance().isLogin()) {
            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (loginUser.isShopkeeper()) {
                price = skuInfo.retailPrice;
            } else {
                price = skuInfo.marketPrice;
            }
        }
        return cleanZero(centToCurrency(context, price));
    }

    /**
     * 根据店主或者普通成员，智能显示价格
     *
     * @param context
     * @param skuInfo
     * @return
     */
    public static String centToCurrency(Context context, CartItem skuInfo) {
        long price = skuInfo.retailPrice;
        if (SessionUtil.getInstance().isLogin()) {
            User loginUser = SessionUtil.getInstance().getLoginUser();
            if (loginUser.isShopkeeper()) {
                price = skuInfo.retailPrice;
            } else {
                price = skuInfo.marketPrice;
            }
        }
        return price==0?"¥0":cleanZero(centToCurrency(context, price));
    }

    /**
     * 将分转化成元，加上¥符号
     *
     * @param context
     * @param cent    2212
     * @return ¥22.12
     */
    public static String centToCurrency(Context context, long cent) {
        if (cent == 0) {
            return "0";
        }
        if (cent < 0) {
            return cleanZero("-" + centToCurrency(context, -cent));
        }
        if (context == null) {
            return cleanZero(String.format("¥%.2f", ConvertUtil.cent2yuan(cent)));
        }
        return cleanZero(context.getResources().getString(R.string.currency) + String.format(context.getResources().getString(R.string.format_money), ConvertUtil.cent2yuan(cent)));
    }

    /**
     * @param context
     * @param cent
     * @return
     */
    public static String centToCurrencyNoZero(Context context, long cent) {
        if (cent == 0) {
            return "0";
        }
        return cleanZero(centToCurrency(context, cent).replace(".00", ""));
    }

    /**
     * 将分转换成元长度对应位数的 ?
     *
     * @param cent 12345
     * @return ???
     */
    public static String cent2QM(long cent) {
        String yuan = String.valueOf(cent / 100);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < yuan.length(); i++) {
            sb.append("?");
        }
        return sb.toString();
    }

    /**
     * 将分转换成 店主价：？？？
     *
     * @param cent
     * @return
     */
    public static String cent2ShopkeeperPrice(long cent) {
        return "店主价：" + cent2QM(cent);
    }

    public static ArrayList<BasicData> json2DataList(JsonElement json) {
        Type type = new TypeToken<ArrayList<BasicData>>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public static BasicData json2object(JsonElement json) {
        Type type = new TypeToken<BasicData>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public static ArrayList<String> json2StringList(JsonElement json) {
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public static SkuInfo json2Sku(JsonElement json) {
        Type type = new TypeToken<SkuInfo>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public static ProductData json2Product(JsonElement json) {
        Type type = new TypeToken<ProductData>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public static ArrayList<SkuInfo> json2SkuList(JsonElement json) {
        Type type = new TypeToken<ArrayList<SkuInfo>>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public static ArrayList<NoticeListModel.DatasEntity> json2NoticeList(JsonElement json) {
        Type type = new TypeToken<ArrayList<NoticeListModel.DatasEntity>>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }

    public static Map<String, String> objToMap(Object object) throws JSONException {
        Map<String, String> data = new HashMap<>();
        JSONObject jsonObject = new JSONObject(new Gson().toJson(object));
        Iterator ite = jsonObject.keys();
        while (ite.hasNext()) {
            String key = ite.next().toString();
            String value = jsonObject.get(key).toString();
            data.put(key, value);
        }
        return data;
    }

    public static String maskPhone(String phone) {
        return phone.substring(0, 3) + "****" + phone.substring(7);
    }

    public static String convertWeight(long weight) {
        if (weight == 0) {
            return "无";
        }
        if (weight < 1000) {
            return weight + "克";
        }
        return String.format("%.2f克", weight / 1000f);
    }

    /**
     * 输入元单位的字符串转换成对应的 long 分
     *
     * @param moneyStr 12.34
     * @return
     */
    public static long stringMoney2Long(String moneyStr) {
        if (StringUtils.isEmpty(moneyStr)) {
            return 0;
        }
        long money1 = (long) (Double.parseDouble(moneyStr) * 100);
        double moneyDouble = Double.parseDouble(moneyStr);
        moneyDouble = moneyDouble > 0 ? moneyDouble + 0.005 : 0;
        long money2 = new Double(moneyDouble * 100).longValue();
        LogUtils.e("输入" + moneyStr + " 输出：" + money2 + "  元函数：" + money1 + "     double" + moneyDouble);
        return money2;
    }

    public static long yuan2cent(String money) {
        return 100L * Long.parseLong(money);
    }

    public static String memberType2TypeStr(int memberType) {
        String memberTypeStr = "";
        switch (memberType) {
            case AppTypes.FAMILY.MEMBER_NORMAL:
                memberTypeStr = "普通会员";
                break;
            case AppTypes.FAMILY.MEMBER_ZUNXIANG:
                memberTypeStr = "尊享会员";
                break;
            case AppTypes.FAMILY.MEMBER_JINKA:
                memberTypeStr = "金卡会员";
                break;
            case AppTypes.FAMILY.MEMBER_TIYAN:
                memberTypeStr = "铂金会员";
                break;
            case AppTypes.FAMILY.MEMBER_ZHUANYING:
                memberTypeStr = "钻石会员";
                break;
        }
        return memberTypeStr;
    }

    /**
     * @param idCard 将身份证号中间改成*
     * @return
     */
    public static String idCard2xing(String idCard) {
        return new StringBuffer().append(idCard.substring(0, 4)).append("**********").append(idCard.substring(14)).toString();
    }

    private static String cleanZero(String money){
        if (money.equals("0")) {
            return money;
        }
        if(money.endsWith(".0")){
            return money.replace(".0", "");
        }else if (money.endsWith(".00")){
            return money.replace(".00", "");
        }else if(money.contains(".") && money.endsWith("0")){
            return money.substring(0,money.length()-1);
        }
        return money;
    }

    public static LiveRoom json2Live(JsonElement json) {
        Type type = new TypeToken<LiveRoom>() {
        }.getType();
        return new Gson().fromJson(json, type);
    }
}
