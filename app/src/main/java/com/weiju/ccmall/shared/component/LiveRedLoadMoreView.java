package com.weiju.ccmall.shared.component;

import com.chad.library.adapter.base.loadmore.LoadMoreView;
import com.weiju.ccmall.R;

/**
 * @author chenyanming
 * @time 2020/1/8 on 18:07
 * @desc
 */
public class LiveRedLoadMoreView extends LoadMoreView {

    @Override public int getLayoutId() {
        return R.layout.red_view_load_more;
    }

    @Override protected int getLoadingViewId() {
        return R.id.load_more_loading_view;
    }

    @Override protected int getLoadFailViewId() {
        return R.id.load_more_load_fail_view;
    }

    @Override protected int getLoadEndViewId() {
        return R.id.load_more_load_end_view;

    }
}
