package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.basic.BaseRequestListener;
import com.weiju.ccmall.shared.bean.LiveRedEnvelopes;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.manager.APIManager;
import com.weiju.ccmall.shared.manager.ServiceManager;
import com.weiju.ccmall.shared.service.contract.ILiveService;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.UiUtils;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.weiju.ccmall.shared.component.dialog.LiveRedListDialog.PLAY;

/**
 * @author chenyanming
 * @time 2020/1/8 on 14:06
 * @desc
 */
public class LiveRedOpenDialog extends Dialog {


    @BindView(R.id.ivOpen)
    ImageView mIvOpen;
    @BindView(R.id.tvMoney)
    TextView mTvMoney;
    @BindView(R.id.layoutMoney)
    LinearLayout mLayoutMoney;
    @BindView(R.id.layoutNoMoney)
    RelativeLayout mLayoutNoMoney;
    private String mLiveId;
    private Context mContext;
    private ILiveService mILiveService = ServiceManager.getInstance().createService(ILiveService.class);

    private String mRedEnvelopesId;

    private boolean mDialogDismiss = true;

    private LiveUser mLiveUser;

    public LiveRedOpenDialog(Context context, String liveId, String redEnvelopesId, LiveUser liveUser) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
        mLiveId = liveId;
        mRedEnvelopesId = redEnvelopesId;
        mLiveUser = liveUser;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_live_red_open);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.ivOpen)
    protected void receiveRedEnvelopes() {
        if (UiUtils.checkUserLogin(mContext)) {
            APIManager.startRequest(mILiveService.receiveRedEnvelopes(mLiveId, mRedEnvelopesId),
                    new BaseRequestListener<LiveRedEnvelopes>() {
                        @Override
                        public void onSuccess(LiveRedEnvelopes result) {
                            mIvOpen.setVisibility(View.GONE);
                            if (result.status != 2) {
                                mLayoutMoney.setVisibility(View.VISIBLE);
                                mTvMoney.setText(MoneyUtil.centToYuanStrNoZero(result.receiveMoney));
                            } else {
                                mLayoutNoMoney.setVisibility(View.VISIBLE);
                            }

                        }
                    }, mContext);
        }
    }

    @OnClick(R.id.tvGoDetail)
    protected void goDetail() {
        mDialogDismiss = false;
        LiveRedDetailDialog liveRedDetailDialog = new LiveRedDetailDialog(mContext, mLiveId, mRedEnvelopesId, PLAY, mLiveUser);
        liveRedDetailDialog.show();
        dismiss();
    }

    @Override
    public void dismiss() {
        if (mDialogDismiss) {
            EventBus.getDefault().post(new EventMessage(Event.dialogDismiss));
        }
        super.dismiss();
    }

    @OnClick(R.id.ivClose)
    protected void onClose(View view) {
        dismiss();
    }

}
