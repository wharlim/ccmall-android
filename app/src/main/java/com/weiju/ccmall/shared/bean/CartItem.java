package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.constant.AppTypes;

import java.util.ArrayList;
import java.util.List;

public class CartItem {

    @SerializedName("skuId")
    public String skuId;
    @SerializedName("skuName")
    public String name;
    @SerializedName("stock")
    public int stock;
    @SerializedName("retailPrice")
    public long retailPrice;
    @SerializedName("marketPrice")
    public long marketPrice;
    @SerializedName("shippingPrice")
    public long shippingPrice;
    @SerializedName("quantity")
    public int amount;
    @SerializedName("hasCoupon")
    public int hasCoupon;
    @SerializedName("thumbUrl")
    public String thumb;
    @SerializedName("properties")
    public String properties;
    @SerializedName("presents")
    public List<CartItem> presents = new ArrayList<>();
    @SerializedName("coin")
    public long coin;
    @SerializedName("countRate")
    public long countRate;
    @SerializedName("countRateExc")
    public String countRateExc;
    public double countRateExc() {
        double ret = 0;
        try {
            ret = Double.valueOf(countRateExc);
        } catch (Exception e) {}
        return ret;
    }
    @SerializedName("ticket")
    public long ticket;
    /**
     * 11 金券产品
     */
    @SerializedName("productType")
    public int productType;

    public boolean isLiveStoreProduct() {
        return productType == 28;//直播商品
    }

    @SerializedName("goldenTicket")
    public long goldenTicket;

    public boolean isSelected = false;
    /**
     * discountStatus : 0
     */

    @SerializedName("discountStatus")
    public int discountStatus;
    /**
     * vipTypePrices : [{"vipType":4,"price":12000},{"vipType":3,"price":14000},{"vipType":2,"price":16000},{"vipType":1,"price":18000},{"vipType":0,"price":20000}]
     */
    @SerializedName("vipTypePrices")
    public List<VipTypePricesEntity> vipTypePrices;
    /**
     * currentVipTypePrice : 22000
     */

    @SerializedName("currentVipTypePrice")
    public long currentVipTypePrice;
    /**
     * buyScore : 1
     */
    @SerializedName("buyScore")
    public int buyScore;
    /**
     * productId : 71b97ea19ea542a59afacbce49cbf256
     */

    @SerializedName("productId")
    public String productId;

    @SerializedName("liveId")
    public String liveId;

    /**
     * 0、默认，1跨境购产品
     */
    @SerializedName("extType")
    public int extType;
    /**
     * 1:跨境购
     */
    public int isCross;
    public long tempPrice;
    @SerializedName("activityTag")
    public List<SkuInfo.ActivityTagEntity> activityTag;

    public static class VipTypePricesEntity {
        /**
         * vipType : 4
         * price : 12000
         */
        @SerializedName("vipType")
        public int vipType;
        @SerializedName("price")
        public long price;
        @SerializedName("vipTypeStr")
        public String vipTypeStr;
    }

    /**
     * 计算积分时用到的价格
     */
    public long scorePrice;

    public long getTypePrice(int vipType) {
        long price = retailPrice;
        if (vipType != AppTypes.FAMILY.MEMBER_NORMAL && vipTypePrices != null) {
            for (VipTypePricesEntity vipTypePrice : vipTypePrices) {
                if (vipTypePrice.vipType == AppTypes.FAMILY.MEMBER_ZUNXIANG) {
                    price = vipTypePrice.price;
                }
            }
        }
        return price;
    }

    /**
     * @return 会员折扣的钱(需要自己判断当前会员等级不是普通会员)
     */
    public long getVipRadioMoney() {
        return retailPrice - getTypePrice(AppTypes.FAMILY.MEMBER_ZUNXIANG);
    }

    /**
     * @return 是否参加第二件半价活动
     */
    public boolean isHalfActivity() {
        return extType == 4;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof CartItem) {
            return ((CartItem) obj).skuId.equals(skuId);
        }
        return obj instanceof SkuInfo && ((SkuInfo) obj).skuId.equals(skuId);
    }
}
