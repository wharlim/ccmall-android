package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JkpOriginalProduct implements Serializable {

    /**
     * hasCoupon : 0
     * couponClickUrl : https://s.click.taobao.com/t?e=m%3D2%26s%3DXbjYr4n49Y9w4vFB6t2Z2ueEDrYVVa64K7Vc7tFgwiHLWlSKdGSYDmA1yfj825FYRitN3%2FurF3xdD3SmLuWTiT5AZvfHZZIWp4L4fdjAJN0z8FnweTKoIR8X7G7Q37Ba6xeVzMRVCyZn8kltdrFlY4we6%2FtGg2%2FRjN4f8DSxNxv%2FVeV2fAquMKfYt%2FNWsG39ElmsmErZ7l5gFuPUbGAoYhmmndTeAzsKGh6bzQC5b8hTCMAoQuwRAJLDyC6kgFFbxg5p7bh%2BFbQ%3D&union_lens=lensId:0b582879_0c59_1703f7bc7cc_7a4b&xId=26OC0rVnekUQadcHAhoYzS9UWeqyaU8F4AOkpUNHd8pf2xwwLkZfOtLEJjxnaVnnENbycaxXcaWvOmTpIIA2EZ&relationId=2109679916
     * couponEndTime :
     * couponInfo :
     * couponStartTime :
     * couponRemainCount : 0
     * couponTotalCount : 0
     * couponType : 0
     * commissionRate : 2.00
     * numIid : 596946937030
     * zkFinalPrice : 6800
     * reservePrice : 6800
     * title : 耐磨加厚pe防水美容食品餐饮厨房家务耐用超薄紧薄款100只均码
     * tbkPwd : ￥p0lX1V5iue3￥
     * couponShortUrl : https://s.click.taobao.com/cr2FEpv
     * smallImages : https://img.alicdn.com/i2/3337398782/O1CN01Iwo4ZD2EkCpj24mdH_!!3337398782.jpg,https://img.alicdn.com/i1/3337398782/O1CN01WhLVd92EkCppNZw9b_!!3337398782.jpg,https://img.alicdn.com/i2/3337398782/O1CN016pGPDY2EkCpkwsuLV_!!3337398782.jpg,https://img.alicdn.com/i2/3337398782/O1CN01stlnay2EkCpmyo7ng_!!3337398782.jpg
     * pictUrl : https://img.alicdn.com/bao/uploaded/i1/3337398782/O1CN01jkk7WE2EkCpmmnbJ5_!!0-item_pic.jpg
     * nick : 惠媳妇旗舰店
     * catName : 家庭/个人清洁工具
     * catLeafName : 家务手套
     */

    @SerializedName("hasCoupon")
    public int hasCoupon;
    public boolean isValidity() {
        return hasCoupon > 0;
    }
    @SerializedName("couponClickUrl")
    public String couponClickUrl;
    @SerializedName("couponEndTime")
    public String couponEndTime;
    @SerializedName("couponInfo")
    public String couponInfo;
    @SerializedName("couponStartTime")
    public String couponStartTime;
    @SerializedName("couponRemainCount")
    public int couponRemainCount;
    @SerializedName("couponTotalCount")
    public int couponTotalCount;
    @SerializedName("couponType")
    public int couponType;
    @SerializedName("commissionRate")
    public String commissionRate;
    @SerializedName("numIid")
    public long numIid;
    @SerializedName("zkFinalPrice")
    public int zkFinalPrice;
    @SerializedName("reservePrice")
    public int reservePrice;
    @SerializedName("title")
    public String title;
    @SerializedName("tbkPwd")
    public String tbkPwd;
    @SerializedName("couponShortUrl")
    public String couponShortUrl;
    @SerializedName("smallImages")
    public String smallImages;
    @SerializedName("pictUrl")
    public String pictUrl;
    @SerializedName("nick")
    public String nick;
    @SerializedName("catName")
    public String catName;
    @SerializedName("catLeafName")
    public String catLeafName;

    @SerializedName("forecastProfit")
    public int forecastProfit; // 预估收益

    @SerializedName("inviteCode")
    public String inviteCode; // 邀请码

    public int getCouponAmountFromCouponInfo() {
        int couponAmount = 0;
        try {
            String reg = "满(\\d+)元减(\\d+)元";
            Pattern pattern = Pattern.compile(reg);
            Matcher matcher = pattern.matcher(couponInfo);
            if (matcher.find()) {
                couponAmount = Integer.parseInt(matcher.group(2));
            }
        } catch (Exception e) {
        }
        return couponAmount;
    }

    public int getFullAmountFromCouponInfo() {
        int couponAmount = 0;
        try {
            String reg = "满(\\d+)元减(\\d+)元";
            Pattern pattern = Pattern.compile(reg);
            Matcher matcher = pattern.matcher(couponInfo);
            if (matcher.find()) {
                couponAmount = Integer.parseInt(matcher.group(1));
            }
        } catch (Exception e) {
        }
        return couponAmount;
    }
}
