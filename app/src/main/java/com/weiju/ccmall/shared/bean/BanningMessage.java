package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @date 2020/2/20.
 */
public class BanningMessage  {


    /**
     * bannedMessageStatus : true
     */

    @SerializedName("bannedMessageStatus")
    public boolean bannedMessageStatus;
}
