package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/8/5 on 11:05
 * @desc ${TODD}
 */
public class Vouchers {
    @SerializedName("goodsId")
    public String goodsId;

    public Vouchers(String goodsId) {
        this.goodsId = goodsId;
    }
}
