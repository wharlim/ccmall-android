package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/2/22 on 17:52
 * @desc 贡献值
 */
public class Contribution {

    /**
     * psp : {"value":0,"minFillValue":100}
     * tsp : {"value":0,"minFillValue":300}
     * tse : {"value":0,"minFillValue":3}
     * tsl : {"value":0,"minFillValue":1}
     */

    @SerializedName("psp")
    public ContributionType psp;
    @SerializedName("tsp")
    public ContributionType tsp;
    @SerializedName("tse")
    public ContributionType tse;
    @SerializedName("tsl")
    public ContributionType tsl;
    @SerializedName("pcp")
    public ContributionType pcp;


}
