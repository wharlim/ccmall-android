package com.weiju.ccmall.shared.util;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.utils.LogUtils;
import com.blankj.utilcode.utils.SizeUtils;
import com.blankj.utilcode.utils.TimeUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.orhanobut.logger.Logger;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.PopupOrderList;

/**
 * 自定义Toast工具类
 * Created by JayChan on 2016/10/13.
 */
public class ToastUtil {

    private static Dialog mProgressDialog;

    public static void error(String message) {
        show(message, R.color.error_text_color, R.drawable.toast_error_bg);
        Logger.i("Toast Error Message: " + message);
    }

    public static void success(String message) {
        show(message, R.color.success_text_color, R.drawable.toast_success_bg);
        Logger.i("Toast Success Message: " + message);
    }

    private static void show(String message, @ColorRes int textColor, @DrawableRes int backgroundRes) {
        Context context = MyApplication.getInstance().getApplicationContext();
        LinearLayout layout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.toast_layout, null);
        layout.setBackgroundResource(backgroundRes);
        TextView messageTv = (TextView) layout.findViewById(R.id.toastMessageTv);
        messageTv.setTextColor(context.getResources().getColor(textColor));
        messageTv.setText(message);
        Toast toast = new Toast(context);
        toast.setGravity(Gravity.TOP, 0, 6);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        toast.show();
    }

    @SuppressLint("InflateParams")
    public static void showLoading(Context context) {
        showLoading(context, false);
    }

    public static void showLoading(Context context, boolean cancelable) {
        if (context == null) {
            return;
        }
        if (null == mProgressDialog && null != context) {
            View view = LayoutInflater.from(context).inflate(R.layout.loading_layout, null);
            mProgressDialog = new Dialog(context, R.style.LoadingDialog);
            mProgressDialog.setContentView(view, new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            mProgressDialog.setCancelable(cancelable);
        }
        if (null != mProgressDialog && !mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    public static void hideLoading() {
        if (null != mProgressDialog) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
            mProgressDialog = null;
        }
    }

    public static void showOrderToast(Context context, PopupOrderList.DatasEntity datasEntity) {
        if (datasEntity == null || context == null) {
            return;
        }
        context = context.getApplicationContext();
        Toast toast = new Toast(context);
        View inflate = View.inflate(context, R.layout.toast_order, null);
        SimpleDraweeView simpleDraweeView = (SimpleDraweeView) inflate.findViewById(R.id.ivImg);
        FrescoUtil.setImageSmall(simpleDraweeView, datasEntity.headImage);
        TextView tvName = (TextView) inflate.findViewById(R.id.tvName);
        String time = TimeUtils.getFitTimeSpan(TimeUtils.string2Millis(datasEntity.createDate), TimeUtils.getNowTimeMills(), 4);
        if (time == null) {
            return;
        }
        LogUtils.e(datasEntity.createDate + "   " + datasEntity.nickName + "  差时间" + time);
        tvName.setText("最新订单来自 " + datasEntity.nickName + " ，" + time + "之前");
        toast.setView(inflate);
        toast.setDuration(Toast.LENGTH_SHORT);
        int x = SizeUtils.dp2px(22);
        int y = SizeUtils.dp2px(90);
        toast.setGravity(Gravity.TOP | Gravity.LEFT, x, y);
        toast.show();
    }

    public static Dialog getProgressDialog() {
        return mProgressDialog;
    }
}
