package com.weiju.ccmall.shared.util;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetManager;
import android.net.Uri;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.weiju.ccmall.MyApplication;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.Gift;
import com.weiju.ccmall.shared.bean.LiveUser;
import com.weiju.ccmall.shared.bean.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;

import io.rong.imlib.model.UserInfo;

/**
 * Created by duanliuyi on 2018/5/10.
 */

/*数据接口
 *
 * 由于本demo没有App Server，用户信息，聊天室信息 等全部通过DataInterface的接口返回，目前都是写死的数据。 开发者可以修改这些接口，去自己的app server取数据。
 * */
public class DataInterface {

    /*appkey   需要改成开发者自己的appKey*/
//    public static String appKey = "8luwapkv84sdl";


    /*是否登录*/
    private static boolean loginStatus = false;

    public static boolean isLoginStatus() {
        return loginStatus;
    }

    public static void setLoginStatus(boolean loginStatus) {
        DataInterface.loginStatus = loginStatus;
    }

    /*是否禁言*/
    private static boolean banStatus = false;

    public static boolean isBanStatus() {
        return banStatus;
    }

    public static void setBanStatus(boolean banStatus) {
        DataInterface.banStatus = banStatus;
    }

    static private ArrayList<UserInfo> userInfoList;
    static private ArrayList<LiveUser> userModes;


    public static ArrayList<LiveUser> getUserModes() {
        return userModes;
    }

    static public void initUserInfo(UserInfo userInfo) {
        userInfoList = new ArrayList<>();
        userInfoList.add(userInfo);
    }

    /*
     * 获取用户信息
     * */
    public static UserInfo getUserInfo(String userid) {
        for (int i = 0; i < userInfoList.size(); i++) {
            if (userInfoList.get(i).getUserId().equals(userid)) {
                return userInfoList.get(i);
            }
        }
        return null;
    }


    /*根据roomId获取房间在线成员列表*/
    public static ArrayList<UserInfo> getUserList(String roomId) {
        ArrayList<UserInfo> userInfos = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            userInfos.add(userInfoList.get(i));
        }
        return userInfos;
    }


    /*获取礼物列表*/
    public static ArrayList<Gift> getGiftList() {
        ArrayList<Gift> gifts = new ArrayList<>();
        String[] giftNames = new String[]{"蛋糕", "气球", "花儿", "项链", "戒指"};
        int[] giftRes = new int[]{R.drawable.gift_cake, R.drawable.gift_ballon, R.drawable.gift_flower, R.drawable.gift_necklace, R.drawable.gift_ring};

        for (int i = 0; i < giftNames.length; i++) {
            Gift gift = new Gift();
            gift.setGiftId("GiftId_" + (i + 1));
            gift.setGiftName(giftNames[i]);
            gift.setGiftRes(giftRes[i]);
            gifts.add(gift);
        }
        return gifts;
    }


    /*获取礼物名*/
    public static String getGiftNameById(String giftId) {
        switch (giftId) {
            case "GiftId_1":
                return "蛋糕";
            case "GiftId_2":
                return "气球";
            case "GiftId_3":
                return "花儿";
            case "GiftId_4":
                return "项链";
            case "GiftId_5":
                return "戒指";
        }
        return null;
    }

    /*根据giftId获取礼物信息*/
    public static Gift getGiftInfo(String giftId) {
        ArrayList<Gift> gifts = getGiftList();
        for (int i = 0; i < gifts.size(); i++) {
            if (gifts.get(i).getGiftId().equals(giftId)) {
                return gifts.get(i);
            }
        }
        return null;
    }


    /*生成随机数*/
    public static int getRandomNum(int max) {
        Random r = new Random();
        return r.nextInt(max);
    }

    public static Uri getUri(Context context, int res) {
        Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
                + context.getResources().getResourcePackageName(res) + "/"
                + context.getResources().getResourceTypeName(res) + "/"
                + context.getResources().getResourceEntryName(res));

        return uri;
    }


    public static String getJson(String fileName, Context context) {
        //将json数据变成字符串
        StringBuilder stringBuilder = new StringBuilder();
        try {
            //获取assets资源管理器
            AssetManager assetManager = context.getAssets();
            //通过管理器打开文件并读取
            BufferedReader bf = new BufferedReader(new InputStreamReader(
                    assetManager.open(fileName)));
            String line;
            while ((line = bf.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }
}
