package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.alivc.live.pusher.AlivcLivePusher;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.live.fragment.LivePushFragment;
import com.weiju.ccmall.shared.util.CommonUtil;
import com.weiju.ccmall.shared.util.SharedPreferenceUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/20 on 17:51
 * @desc
 */
public class LiveMeiYanDialog extends Dialog {
    @BindView(R.id.seekbar)
    SeekBar mSeekbar;
    @BindView(R.id.ivMoPi)
    ImageView mIvMoPi;
    @BindView(R.id.ivMeiBai)
    ImageView mIvMeiBai;
    @BindView(R.id.ivHongRun)
    ImageView mIvHongRun;
    @BindView(R.id.ivSaiHong)
    ImageView mIvSaiHong;
    @BindView(R.id.ivShouLian)
    ImageView mIvShouLian;
    @BindView(R.id.ivXiaBa)
    ImageView mIvXiaBa;
    @BindView(R.id.ivDaYan)
    ImageView mIvDaYan;

    @BindView(R.id.ivClose)
    ImageView mIvClose;
    @BindView(R.id.tvClose)
    TextView mTvClose;

    private AlivcLivePusher mAlivcLivePusher = null;
    private Context mContext;


    public LiveMeiYanDialog(@NonNull Context context) {
        super(context, R.style.Theme_Light_Dialog);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_meiyan);
        ButterKnife.bind(this);
        CommonUtil.initDialogWindow(getWindow(), Gravity.BOTTOM);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        layoutParams.dimAmount = 0.0f;
        getWindow().setAttributes(layoutParams);


        initView();

    }

    private void initView() {
        mIvMoPi.setSelected(true);
        mSeekbar.setOnSeekBarChangeListener(onSeekBarChangeListener);

        mIvMoPi.setSelected(true);
        setSeekBarProgress(SharedPreferenceUtils.getBuffing(mContext));
    }

    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            try {
                if (mIvMoPi.isSelected()) {
                    mAlivcLivePusher.setBeautyBuffing(mSeekbar.getProgress());
                    SharedPreferenceUtils.setBuffing(mContext, progress);
                } else if (mIvMeiBai.isSelected()) {
                    mAlivcLivePusher.setBeautyWhite(mSeekbar.getProgress());
                    SharedPreferenceUtils.setWhiteValue(mContext, progress);
                } else if (mIvHongRun.isSelected()) {
                    mAlivcLivePusher.setBeautyRuddy(mSeekbar.getProgress());
                    SharedPreferenceUtils.setRuddy(mContext, progress);
                } else if (mIvSaiHong.isSelected()) {
                    mAlivcLivePusher.setBeautyCheekPink(mSeekbar.getProgress());
                    SharedPreferenceUtils.setCheekPink(mContext, progress);
                } else if (mIvShouLian.isSelected()) {
                    mAlivcLivePusher.setBeautySlimFace(mSeekbar.getProgress());
                    SharedPreferenceUtils.setSlimFace(mContext, progress);
                } else if (mIvXiaBa.isSelected()) {
                    mAlivcLivePusher.setBeautyShortenFace(mSeekbar.getProgress());
                    SharedPreferenceUtils.setShortenFace(mContext, progress);
                } else if (mIvDaYan.isSelected()) {
                    mAlivcLivePusher.setBeautyBigEye(mSeekbar.getProgress());
                    SharedPreferenceUtils.setBigEye(mContext, progress);
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };


    public void setSeekBarProgress(int progress) {
        mSeekbar.setProgress(progress);
    }

    public void setAlivcLivePusher(AlivcLivePusher alivcLivePusher) {
        this.mAlivcLivePusher = alivcLivePusher;
    }



    @OnClick({R.id.tvClose, R.id.ivClose})
    protected void close() {
        if (mAlivcLivePusher != null) {
            try {
                boolean selected = mIvClose.isSelected();
                mIvClose.setSelected(!selected);
                mTvClose.setText(!selected ? "开启" : "关闭");
                mTvClose.setSelected(!selected);

                mAlivcLivePusher.setBeautyOn(selected);

                SharedPreferenceUtils.setBeautyOn(mContext, !selected);
            } catch (IllegalArgumentException | IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }


    @OnClick({R.id.layoutMoPi, R.id.layoutMeiBai, R.id.layouHongRun, R.id.layoutSaiHong,
            R.id.layoutShouLian, R.id.layoutXiaBa, R.id.layoutDaYan})
    protected void meiyan(View view) {
        mIvMoPi.setSelected(false);
        mIvMeiBai.setSelected(false);
        mIvHongRun.setSelected(false);
        mIvSaiHong.setSelected(false);
        mIvShouLian.setSelected(false);
        mIvXiaBa.setSelected(false);
        mIvDaYan.setSelected(false);
        switch (view.getId()) {
            case R.id.layoutMoPi:
                mIvMoPi.setSelected(true);
                setSeekBarProgress(SharedPreferenceUtils.getBuffing(mContext));
                break;
            case R.id.layoutMeiBai:
                mIvMeiBai.setSelected(true);
                setSeekBarProgress(SharedPreferenceUtils.getWhiteValue(mContext));
                break;
            case R.id.layouHongRun:
                mIvHongRun.setSelected(true);
                setSeekBarProgress(SharedPreferenceUtils.getRuddy(mContext));
                break;
            case R.id.layoutSaiHong:
                mIvSaiHong.setSelected(true);
                setSeekBarProgress(SharedPreferenceUtils.getCheekpink(mContext));
                break;
            case R.id.layoutShouLian:
                mIvShouLian.setSelected(true);
                setSeekBarProgress(SharedPreferenceUtils.getSlimFace(mContext));
                break;
            case R.id.layoutXiaBa:
                mIvXiaBa.setSelected(true);
                setSeekBarProgress(SharedPreferenceUtils.getShortenFace(mContext));
                break;
            case R.id.layoutDaYan:
                mIvDaYan.setSelected(true);
                setSeekBarProgress(SharedPreferenceUtils.getBigEye(mContext));
                break;
            default:
        }
    }

    @OnClick(R.id.tvReBuild)
    protected void reBuild() {
        mAlivcLivePusher.setBeautyBuffing(40);
        SharedPreferenceUtils.setBuffing(mContext, 40);
        mAlivcLivePusher.setBeautyWhite(70);
        SharedPreferenceUtils.setWhiteValue(mContext, 70);
        mAlivcLivePusher.setBeautyRuddy(40);
        SharedPreferenceUtils.setRuddy(mContext, 40);
        mAlivcLivePusher.setBeautyCheekPink(15);
        SharedPreferenceUtils.setCheekPink(mContext, 15);
        mAlivcLivePusher.setBeautySlimFace(40);
        SharedPreferenceUtils.setSlimFace(mContext, 40);
        mAlivcLivePusher.setBeautyShortenFace(50);
        SharedPreferenceUtils.setShortenFace(mContext, 50);
        mAlivcLivePusher.setBeautyBigEye(30);
        SharedPreferenceUtils.setBigEye(mContext, 30);

        int progress = 40;

        if (mIvMeiBai.isSelected()) {
            progress = 70;
        } else if (mIvSaiHong.isSelected()) {
            progress = 15;
        } else if (mIvXiaBa.isSelected()) {
            progress = 50;
        } else if (mIvDaYan.isSelected()) {
            progress = 30;
        }

        setSeekBarProgress(progress);

    }

    @OnClick(R.id.tvBack)
    protected void back() {
        dismiss();
    }
}
