package com.weiju.ccmall.shared.service.contract;

import com.weiju.ccmall.shared.bean.Category;
import com.weiju.ccmall.shared.bean.api.PaginationEntity;
import com.weiju.ccmall.shared.bean.api.RequestResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ICategoryService {
    @GET("category/getParentCategory")
    Observable<RequestResult<PaginationEntity<Category, Object>>> getTopCategory(@Query("pageOffset") int page);

    @GET("category/getChildCategory")
    Observable<RequestResult<PaginationEntity<Category, Object>>> getCategory(
            @Query("parentCategoryId") String categoryId,
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize
    );

    @GET("category/getParentCategory")
    Observable<RequestResult<PaginationEntity<Category, Object>>> getPushTopCategory(
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize
    );

    @GET("category/getChildCategory")
    Observable<RequestResult<PaginationEntity<Category, Object>>> getPushChildCategory(
            @Query("parentCategoryId") String categoryId,
            @Query("pageOffset") int page,
            @Query("pageSize") int pageSize
    );
}
