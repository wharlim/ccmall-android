package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;

import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.constant.AppTypes;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanmin
 * @date 2019/6/22
 * @desc
 */
public class CheckPasswordDialog extends Dialog {
    @BindView(R.id.etPassword)
    protected EditText mEtPassword;

    protected OnConfirmListener mConfirmListener;

    public CheckPasswordDialog(Context context) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_check_password);
        ButterKnife.bind(this);
    }

    public void setOnConfirmListener(@NonNull OnConfirmListener listener) {
        mConfirmListener = listener;
    }

    @OnClick(R.id.cancelBtn)
    protected void onClose(View view) {
        dismiss();
    }

    @OnClick(R.id.confirmBtn)
    protected void onConfirm(View view) {
        if (mConfirmListener != null) {
            mConfirmListener.confirm(mEtPassword.getText().toString());
        }
        dismiss();
    }

    public interface OnConfirmListener {
        void confirm(String password);
    }
}
