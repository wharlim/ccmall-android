package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.bean
 * @since 2017-06-09
 */
public class SkuAmount implements Serializable {
    @SerializedName("skuId")
    public String skuId;
    @SerializedName("quantity")
    public int amount = 1;

    @SerializedName("liveId")
    public String liveId;

    public SkuAmount() {
    }

    public SkuAmount(String skuId, int amount) {
        this.skuId = skuId;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "SkuAmount{" +
                "skuId='" + skuId + '\'' +
                ", amount=" + amount +
                '}';
    }
}
