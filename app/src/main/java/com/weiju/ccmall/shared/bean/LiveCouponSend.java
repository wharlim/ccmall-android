package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @date 2020/2/22.
 */
public class LiveCouponSend {

    /**
     * sendId : 0158824bf9bc42c5b1a971dd52dbc47a
     * couponId : 7dad1585889d496bb64a7addddebf36c
     * liveId : dae42bcc1c0e463faec0adefe3ef2fc3
     * memberId : af8d785b384ad9281286275095141f15
     * title : 直播间优惠券
     * sendNum : 10
     * remark :
     * status : 0
     * receiveStatus : 0
     * balanceCode : 0211582435875511
     * receiveNum : 1
     * createDate : 2020-02-20 22:21:39
     * updateDate : 2020-02-20 22:31:08
     * deleteFlag : 0
     */

    @SerializedName("sendId")
    public String sendId;
    @SerializedName("couponId")
    public String couponId;
    @SerializedName("liveId")
    public String liveId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("title")
    public String title;
    @SerializedName("sendNum")
    public int sendNum;
    @SerializedName("remark")
    public String remark;
    @SerializedName("status")
    public int status;
    @SerializedName("receiveStatus")
    public int receiveStatus;
    @SerializedName("balanceCode")
    public String balanceCode;
    @SerializedName("receiveNum")
    public int receiveNum;
    @SerializedName("createDate")
    public String createDate;
    @SerializedName("updateDate")
    public String updateDate;
    @SerializedName("deleteFlag")
    public int deleteFlag;
    @SerializedName("cost")
    public long cost;
    @SerializedName("couponTitle")
    public String couponTitle;
}
