package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

/**
 * @author chenyanming
 * @time 2019/1/29 on 11:13
 * @desc
 */
public class Challenge {


    /**
     * activityId : b07f5ac0d0e84f879c85b51f5fcd2fe8
     * type : 1
     * lifeCycle : 72
     * target : V3
     * score : 3000
     * expiresDate :
     * activityStatus : 0
     * activityStatusStr : 未挑战
     */

    @SerializedName("activityId")
    public String activityId;
    @SerializedName("type")
    /**
     * 挑战类型 0销售挑战 1升级挑战  2，3，4的为PK挑战
     */
    public int type;
    @SerializedName("lifeCycle")
    public int lifeCycle;
    @SerializedName("target")
    public String target;
    @SerializedName("score")
    public int score;
    @SerializedName("expiresDate")
    public String expiresDate;
    /**
     * 挑战状态 0未挑战 1挑战中2挑战成功 3挑战失败
     */
    @SerializedName("activityStatus")
    public int activityStatus;
    @SerializedName("activityStatusStr")
    public String activityStatusStr;


    /**
     * challengeId :
     * memberId : 1049901ba4644a368e849d2c60c0fe53
     * startDate :
     * endDate :
     */

    @SerializedName("challengeId")
    public String challengeId;
    @SerializedName("memberId")
    public String memberId;
    @SerializedName("startDate")
    public String startDate;
    @SerializedName("endDate")
    public String endDate;
    /**
     * pkStatus : 1
     * pkDetail : {"challengeId":"610452284e8844d2858386a27d7c32a7","pkScore":20,"memberId":"67e7258cc6cb411e8978f92b7c41f05c","userName":"","nickName":"68","headImage":"http://testimg.beautysecret.cn/G1/M00/00/03/cjcLYVmULpaANAOKAANdia7CY2E731.jpg","idType":1,"pkChallengeId":"94c25e293e5c435584b77f416e2b382c","pkMemberId":"31ca6e58263d468caad89e3457f823ca","pkUserName":"4068","pkNickName":"一个忧伤的胖子","pkHeadImage":"http://wx.qlogo.cn/mmopen/vi_32/r7l5z7myibviatYzOjq58OeGgk5Zxt6vMYuibO4bj1IIHpUwZ0bOh0EjyAXMuL40wAneicK0gYjGrYVgicBLBs22sbw/0","pkStartDate":"2019-06-25 11:19:33","startPoint":0,"endPoint":0,"pkStartPoint":0,"pkEndPoint":0,"winMemberId":"","createDate":"2019-06-25 11:19:36","updateDate":"2019-06-25 11:19:36","deleteFlag":0}
     */

    @SerializedName("pkStatus")
    public int pkStatus;
    @SerializedName("pkDetail")
    public PkDetailEntity pkDetail;
    /**
     * userName : 叶子林
     * headImage : http://thirdwx.qlogo.cn/mmopen/vi_32/iaBYC9wCzV4MCNTflex8iavFYsRqSYxy0ibdicBPzUicZicJCMK9OCibrkMyd8QrZhE2eiahp3fBTmKRCg3iavlxpGnADkw/132
     * nickName : 新用户
     * pkUserName :
     * pkNickName :
     * pkHeadImage :
     * pkScore : 20
     */

    @SerializedName("userName")
    public String userName;
    @SerializedName("headImage")
    public String headImage;
    @SerializedName("nickName")
    public String nickName;
    @SerializedName("pkUserName")
    public String pkUserName;
    @SerializedName("pkNickName")
    public String pkNickName;
    @SerializedName("pkHeadImage")
    public String pkHeadImage;
    @SerializedName("pkScore")
    public int pkScore;
    @SerializedName("minScore")
    public int minScore;
    @SerializedName("maxScore")
    public int maxScore;


    public String getTypeString() {
        switch (type) {
            case 2:
                return "PSP";
            case 3:
                return "TSP";
            case 4:
                return "TSE";
            default:
                return "";
        }
    }



}
