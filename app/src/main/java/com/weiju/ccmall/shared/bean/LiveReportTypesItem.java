package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

public class LiveReportTypesItem {

    /**
     * reportTitleListId : b468a2fd83334838af417bb92yyf0000
     * reportTitle : 测试1
     */

    @SerializedName("reportTitleListId")
    public String reportTitleListId;
    @SerializedName("reportTitle")
    public String reportTitle;
}
