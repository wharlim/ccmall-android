package com.weiju.ccmall.shared.common;

import android.content.Context;
import android.view.View;

import com.bigkoo.convenientbanner.holder.Holder;
import com.blankj.utilcode.utils.ScreenUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.shared.bean.LiveRoom;
import com.weiju.ccmall.shared.page.bean.BasicData;
import com.weiju.ccmall.shared.util.FrescoUtil;

/**
 * @author JayChan <voidea@foxmail.com>
 * @version 1.0
 * @package com.weiju.ccmall.shared.common
 * @since 2017-06-09
 */
public class CarouselItemViewHolder<T> implements Holder<T> {

    private SimpleDraweeView simpleDraweeView;
    private int mScreenWidth;

    @Override
    public View createView(Context context) {
        simpleDraweeView = new SimpleDraweeView(context);
        mScreenWidth = ScreenUtils.getScreenWidth();
        return simpleDraweeView;
    }

    @Override
    public void UpdateUI(Context context, int position, T data) {
        if (data instanceof String) {
            FrescoUtil.setImage(simpleDraweeView, String.valueOf(data), mScreenWidth, 0);
        } else if (data instanceof BasicData) {
            FrescoUtil.setImage(simpleDraweeView, ((BasicData) data).image, mScreenWidth, 0);
        }else if (data instanceof LiveRoom) {
            FrescoUtil.setImage(simpleDraweeView, ((LiveRoom) data).image);
        }
    }

}