package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * @author chenyanming
 * @time 2019/8/15 on 10:49
 * @desc ${TODD}
 */
public class SuperMatch {

    /**
     * matchBean : {"matchId":"est voluptate","groupCode":"nostrud","successDate":"et aliquip sint dolor cupidatat"}
     * memberList : [{"memberId":"elit sed et eu laborum","inviteCode":-7.077857429038374E7,"headImage":"reprehenderit proident","phone":"ut la","nickName":"ut veniam","wechat":"qui ex magna","inviteMemberType":-9.687313342623578E7,"inviteMemberTypeStr":"ipsum nisi aliquip","xwsWechatOpenId":"dolore sint magna","wechatUnionId":"irure","isStore":-5495602.538764238,"userName":"sit fugiat tempor aliqua","qq":"aliqua dolor amet","identityCard":"est","memberType":9.427974338375208E7,"memberTypeStr":"fugiat consect","authStatus":-9.169000557404253E7,"authStatusStr":"amet","signinStatus":7.440273580666265E7,"storeMemberId":"nostrud ea anim est dolore","storeType":-1.8668033353099704E7,"storeTypeStr":"sunt irure","vipType":-9295712.649458438,"vipTypeStr":"consectetur","vipMemberXwsStatus":6844452.504953489,"xdsWechatOpenId":"velit","hasDeposit":8.66723309612591E7,"hidePrestoredStatus":8.156824957558522E7,"zhAuthStatus":5.134511596453041E7,"zhTeamCount":8.071594937778091E7,"autoAuthStatus":9.363795123499689E7,"password":"dolore amet consectetur dolor","salt":"cillum","freezeType":4.4910678574312E7,"freezeDate":"culpa dolor","freezeReason":"aliquip adipisicing do","referrerMemberId":"laborum Duis id consectetur","createDate":"pariatur","deleteFlag":5.130028646705428E7,"proxyMemberId":"do dolor qui cillum","hasDepositStr":"ipsum tempor pariatur"}]
     */

    @SerializedName("matchBean")
    public MatchBeanEntity matchBean;
    @SerializedName("memberList")
    public List<User> memberList;

    public static class MatchBeanEntity {
        /**
         * matchId : est voluptate
         * groupCode : nostrud
         * successDate : et aliquip sint dolor cupidatat
         */

        @SerializedName("matchId")
        public String matchId;
        @SerializedName("groupCode")
        public String groupCode;
        @SerializedName("successDate")
        public String successDate;
    }
}
