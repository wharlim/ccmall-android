package com.weiju.ccmall.shared.page.element;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.blankj.utilcode.utils.SizeUtils;
import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.category.adapter.RoundBackgroundColorSpan;
import com.weiju.ccmall.module.jkp.JiKaoPuProductDetailActivity;
import com.weiju.ccmall.shared.basic.BaseCallback;
import com.weiju.ccmall.shared.bean.SkuInfo;
import com.weiju.ccmall.shared.bean.Tag;
import com.weiju.ccmall.shared.bean.event.EventMessage;
import com.weiju.ccmall.shared.component.TagTextView;
import com.weiju.ccmall.shared.component.TagViews;
import com.weiju.ccmall.shared.constant.Event;
import com.weiju.ccmall.shared.decoration.SpacesItemDecoration;
import com.weiju.ccmall.shared.manager.CartManager;
import com.weiju.ccmall.shared.page.PageType;
import com.weiju.ccmall.shared.page.bean.Element;
import com.weiju.ccmall.shared.service.ProductService;
import com.weiju.ccmall.shared.util.ConvertUtil;
import com.weiju.ccmall.shared.util.EventUtil;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.MoneyUtil;
import com.weiju.ccmall.shared.util.TextViewUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProductElement extends LinearLayout {

    private final RecyclerView mListRv;
    private ProductAdapter mProductAdapter;
    private PageType pageType = PageType.HOME;

    public ProductElement(Context context, Element element) {
        super(context);
        View view = inflate(getContext(), R.layout.el_product_layout, this);
        view.setBackgroundColor(Color.parseColor("#ececf2"));
        element.setBackgroundInto(view);
        mListRv = (RecyclerView) view.findViewById(R.id.eleListRv);
        mListRv.setScrollContainer(false);
        mListRv.requestDisallowInterceptTouchEvent(true);

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mListRv.getLayoutParams();
        if (element.columns == 1) {
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            linearLayoutManager.setAutoMeasureEnabled(true);
            linearLayoutManager.setSmoothScrollbarEnabled(false);
            mListRv.setLayoutManager(linearLayoutManager);
            mListRv.addItemDecoration(new SpacesItemDecoration(ConvertUtil.dip2px(10)));
            layoutParams.setMargins(0, 0, 0, 0);
            mListRv.setLayoutParams(layoutParams);
        } else {
            GridLayoutManager gridLayoutManager = new GridLayoutManager(context, element.columns);
            gridLayoutManager.setAutoMeasureEnabled(true);
            gridLayoutManager.setSmoothScrollbarEnabled(false);
            mListRv.setLayoutManager(gridLayoutManager);
            int margin = ConvertUtil.dip2px(2.5f);
            layoutParams.setMargins(margin, 0, margin, 0);
            mListRv.setLayoutParams(layoutParams);
        }
        mListRv.setNestedScrollingEnabled(false);
        mListRv.setScrollContainer(false);
        ArrayList<SkuInfo> skuIds = ConvertUtil.json2SkuList(element.data);
        mProductAdapter = new ProductAdapter(skuIds, element.columns);
        mListRv.setAdapter(mProductAdapter);
//        setSkuIds(skuIds, element.columns);
    }

    private void setSkuIds(ArrayList<String> skuIds, final int columns) {
        ProductService.getListBySkuIds(skuIds, new BaseCallback<ArrayList<SkuInfo>>() {

            @Override
            public void callback(ArrayList<SkuInfo> data) {
                mProductAdapter = new ProductAdapter(data, columns);
                mListRv.setAdapter(mProductAdapter);
            }
        });
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void updatePrice(EventMessage message) {
        if (message.getEvent().equals(Event.loginSuccess) || message.getEvent().equals(Event.logout)) {
            mProductAdapter.notifyDataSetChanged();
        }
    }

    private class ProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private final ArrayList<SkuInfo> items;
        private final int columns;

        ProductAdapter(ArrayList<SkuInfo> items, int columns) {
            this.items = items;
            this.columns = columns;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            if (viewType == 2) {
                return new ProductColumn2ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.el_product_column2_item, parent, false));
            } else if (viewType == 3) {
                return new ProductColumn3ViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.el_product_column3_item, parent, false));
            } else {
                return new ProductViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.el_product_column_item, parent, false));
            }
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            holder.setIsRecyclable(false);
            final SkuInfo skuInfo = items.get(position);
            if (columns == 2) {
                ProductColumn2ViewHolder column2ViewHolder = (ProductColumn2ViewHolder) holder;
                column2ViewHolder.setProduct(skuInfo);
                column2ViewHolder.marginTopView.setVisibility(position < 2 ? View.GONE : View.VISIBLE);
            } else if (columns == 3) {
                ProductColumn3ViewHolder column3ViewHolder = (ProductColumn3ViewHolder) holder;
                column3ViewHolder.setProduct(skuInfo);
                column3ViewHolder.marginTopView.setVisibility(position < 3 ? View.GONE : View.VISIBLE);
            } else {
                ProductViewHolder viewHolder = ((ProductViewHolder) holder);
                viewHolder.setProduct(skuInfo);
                viewHolder.mTagViews.setData(null);
                viewHolder.itemTitleTv.setTags(skuInfo.tags);
                viewHolder.itemTitleTv.setText(skuInfo.name);
            }
            holder.itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (skuInfo.isJpkProduct()) {
                        JiKaoPuProductDetailActivity.start(getContext(), skuInfo);
                    } else {
                        EventUtil.viewProductDetail(getContext(), skuInfo.skuId, false);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return items.size();
        }

        @Override
        public int getItemViewType(int position) {
            return columns;
        }
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemTitleTv)
        protected TagTextView itemTitleTv;
        @BindView(R.id.itemPriceTv)
        protected TextView itemPriceTv;
        @BindView(R.id.itemSalesTv)
        protected TextView itemSalesTv;
        @BindView(R.id.itemThumbIv)
        protected SimpleDraweeView itemThumbIv;
        @BindView(R.id.itemMarkPriceTv)
        protected TextView itemMarkPriceTv;
        @BindView(R.id.addToCartBtn)
        protected ImageView addToCartBtn;
        @BindView(R.id.tagViews)
        protected TagViews mTagViews;
        @BindView(R.id.ivBanjia)
        protected ImageView mBanjiaView;
        @BindView(R.id.tvCCM)
        protected TextView mTvCCM;
        @BindView(R.id.tvSaleTime)
        protected TextView mTvSaleTime;

        ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setProduct(final SkuInfo product) {
            FrescoUtil.setSkuImgSmallMask(itemThumbIv, product);
            itemTitleTv.setTitle(product.name);
            mTagViews.setData(product.tags);
//            itemTitleTv.setTags(product.tags);
            itemSalesTv.setText(String.format("销量：%s 件", product.totalSaleCount));
            itemPriceTv.setText(MoneyUtil.centToYuan¥StrNoZero(product.retailPrice));
//            if (3 > 2 || SessionUtil.getInstance().getLoginUser().isShopkeeper()) {
//            itemMarkPriceTv.setVisibility(VISIBLE);
            itemMarkPriceTv.setText(MoneyUtil.centToYuan¥StrNoZero(product.marketPrice));
            mTvCCM.setText(String.format("奖%s%%算率", product.countRateExc));
            TextViewUtil.addThroughLine(itemMarkPriceTv);
//            } else {
//                itemMarkPriceTv.setVisibility(GONE);
//            }
            addToCartBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    CartManager.addToCart(getContext(), product, 1, false);
                }
            });

            mBanjiaView.setVisibility(product.isBanjia() ? VISIBLE : GONE);

            mTvSaleTime.setVisibility(product.type == 9 && product.newerConfig != null &&
                    (!product.newerConfig.canBuy || product.newerConfig.locked) ? View.VISIBLE : View.GONE);
            TextViewUtil.setNewerBuyDate(getContext(), mTvSaleTime, product.newerConfig, "%s可购买",
                    "还剩%d:%d:%d可购买", false);
        }
    }

    class ProductColumn2ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.ivBanjia)
        protected ImageView mBanjiaView;
        @BindView(R.id.itemTitleTv)
        protected TextView itemTitleTv;
        @BindView(R.id.itemSalesTv)
        protected TextView itemSalesTv;
        @BindView(R.id.itemPriceTv)
        protected TextView itemPriceTv;
        @BindView(R.id.itemThumbIv)
        protected SimpleDraweeView itemThumbIv;
        @BindView(R.id.marginTopView)
        protected View marginTopView;
        @BindView(R.id.itemMarkPriceTv)
        protected TextView itemMarkPriceTv;
        @BindView(R.id.tvCCM)
        protected TextView mTvCCM;
        @BindView(R.id.tvSaleTime)
        protected TextView mTvSaleTime;

        ProductColumn2ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setTag(SpannableStringBuilder title, List<Tag> tags) {
            for (Tag tag: tags) {
                title.insert(0, tag.name);
                int start = 0;
                int end = tag.name.length();
                //稍微设置标签文字小一点
                title.setSpan(new RelativeSizeSpan(0.9f), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                //设置圆角背景
                title.setSpan(new RoundBackgroundColorSpan(getContext().getResources().getColor(R.color.red), Color.WHITE), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }

        public void setTag(LinearLayout layout, List<Tag> tags) {
            int padding = ConvertUtil.dip2px(3);

            int dividers = SizeUtils.dp2px(5);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 0, dividers, 0);

            layout.removeAllViews();
            if (tags == null) {
                Tag tag = new Tag();
                tag.name = "  ";
                tags = Arrays.asList(tag);
            }

//            if (tags != null) {
            int length = 0;
            for (Tag tag : tags) {
                TextView textView = new TextView(getContext());
                textView.setLayoutParams(layoutParams);
                textView.setTextSize(12);
                textView.setTextColor(getResources().getColor(R.color.white));
                textView.setBackgroundResource(R.drawable.btn_bg_red);
                textView.setText(tag.name);
                textView.setMaxLines(1);
                textView.setEllipsize(TextUtils.TruncateAt.END);
                textView.setPadding(padding, 0, padding, 0);

                length += tag.name.length();
                if (length > 10) {
                    break;
                }

                layout.addView(textView);
//                }
            }
        }

        void setProduct(SkuInfo product) {
            if (product.isJpkProduct()) {
                FrescoUtil.setImageSmall(itemThumbIv, product.thumb);
            } else {
                FrescoUtil.setSkuImgMaskSmall(itemThumbIv, product);
            }
            SpannableStringBuilder ssb = new SpannableStringBuilder();
            if (product.name != null) {
                ssb.append(product.name);
            }
            setTag(ssb, product.tags);
            itemTitleTv.setText(ssb);
//            setTag(mTagViews, product.tags);
            itemSalesTv.setText(String.format("销量：%s件", product.totalSaleCount));
            itemPriceTv.setText(MoneyUtil.centToYuan¥StrNoZero(product.retailPrice));
            itemMarkPriceTv.setText(MoneyUtil.centToYuan¥StrNoZero(product.marketPrice));
            if (product.isJpkProduct()) {
                mTvCCM.setText(String.format("%s元优惠券", MoneyUtil.centToYuan¥StrNoZero(product.costPrice)));
            } else {
                mTvCCM.setText(String.format("奖%s%%算率", product.countRateExc));
            }
            TextViewUtil.addThroughLine(itemMarkPriceTv);
            mBanjiaView.setVisibility(product.isBanjia() ? VISIBLE : GONE);
            mTvSaleTime.setVisibility(product.type == 9 && product.newerConfig != null &&
                    (!product.newerConfig.canBuy || product.newerConfig.locked) ? View.VISIBLE : View.GONE);
            TextViewUtil.setNewerBuyDate(getContext(), mTvSaleTime, product.newerConfig, "%s可购买",
                    "还剩%d:%d:%d可购买", false);
        }


    }

    class ProductColumn3ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemTitleTv)
        protected TextView itemTitleTv;
        @BindView(R.id.itemPriceTv)
        protected TextView itemPriceTv;
        @BindView(R.id.itemThumbIv)
        protected SimpleDraweeView itemThumbIv;
        @BindView(R.id.marginTopView)
        protected View marginTopView;
        @BindView(R.id.itemMarkPriceTv)
        protected TextView itemMarkPriceTv;
        @BindView(R.id.ivBanjia)
        protected ImageView mBanjiaView;
        @BindView(R.id.tvCCM)
        protected TextView mTvCCM;

        ProductColumn3ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void setProduct(SkuInfo product) {
            FrescoUtil.setSkuImgSmallMask(itemThumbIv, product);
            itemTitleTv.setText(product.name);
            itemPriceTv.setText(MoneyUtil.centToYuan¥StrNoZero(product.retailPrice));

            itemMarkPriceTv.setText(MoneyUtil.centToYuan¥StrNoZero(product.marketPrice));
            TextViewUtil.addThroughLine(itemMarkPriceTv);
//            itemMarkPriceTv.setVisibility(INVISIBLE);
            mTvCCM.setText(String.format("奖%s%%算率", product.countRateExc));
            mBanjiaView.setVisibility(product.isBanjia() ? VISIBLE : GONE);
        }
    }
}
