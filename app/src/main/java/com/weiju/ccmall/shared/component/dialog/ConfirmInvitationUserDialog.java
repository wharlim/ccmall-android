package com.weiju.ccmall.shared.component.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.weiju.ccmall.R;
import com.weiju.ccmall.shared.bean.User;
import com.weiju.ccmall.shared.constant.AppTypes;
import com.weiju.ccmall.shared.util.FrescoUtil;
import com.weiju.ccmall.shared.util.StringUtil;
import com.weiju.ccmall.shared.util.ToastUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/5/9 on 16:59
 * @desc ${TODD}
 */
public class ConfirmInvitationUserDialog  extends Dialog {


    private User mUser;

    @BindView(R.id.avatarIv)
    protected SimpleDraweeView mAvatarIv;
    @BindView(R.id.nameTv)
    protected TextView mNameTv;
    @BindView(R.id.phoneTv)
    protected TextView mPhoneTv;

    protected View.OnClickListener mConfirmListener;

    public ConfirmInvitationUserDialog(@NonNull Context context, User user) {
        super(context);
        mUser = user;
    }

    private ConfirmInvitationUserDialog(Context context, int themeResId) {
        super(context, R.style.Theme_Light_Dialog);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_confirm_invitation_user);
        ButterKnife.bind(this);
        FrescoUtil.setImage(mAvatarIv, mUser.avatar);
        mNameTv.setText(String.format("%s", mUser.nickname));
        StringBuilder phoneSB = new StringBuilder(mUser.phone);
        String phone = phoneSB.replace(3, 7, "****").toString();
        mPhoneTv.setText(phone);
    }

    public void setOnConfirmListener(@NonNull View.OnClickListener listener) {
        mConfirmListener = listener;
    }

    @OnClick(R.id.cancelBtn)
    protected void onClose(View view) {
        dismiss();
    }

    @OnClick(R.id.confirmBtn)
    protected void onConfirm(View view) {
        if (mConfirmListener != null) {
            mConfirmListener.onClick(view);
        }
        dismiss();
    }
}
