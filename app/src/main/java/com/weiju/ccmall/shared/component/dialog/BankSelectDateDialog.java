package com.weiju.ccmall.shared.component.dialog;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.weiju.ccmall.R;
import com.zyyoona7.wheel.WheelView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author chenyanming
 * @time 2019/12/12 on 17:00
 * @desc
 */
public class BankSelectDateDialog extends BottomSheetDialog {

    @BindView(R.id.wheelView)
    WheelView mWheelView;
    @BindView(R.id.tvTtile)
    TextView mTvTitle;

    private OnConfirmListener mOnConfirmListener;
    private String mTtile;

    private int maxDay = 31;

    public BankSelectDateDialog(@NonNull Context context,
                                OnConfirmListener onConfirmListener,
                                String title) {
        super(context, R.style.Theme_Light_Dialog);
        mOnConfirmListener = onConfirmListener;
        mTtile = title;
    }

    public BankSelectDateDialog(@NonNull Context context,
                                OnConfirmListener onConfirmListener,
                                String title, int maxDay) {
        this(context, onConfirmListener, title);
        this.maxDay = maxDay;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_bank_select_date);
        ButterKnife.bind(this);

        Window window = getWindow();
        if (null != window) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

        if (!TextUtils.isEmpty(mTtile)) {
            mTvTitle.setText(mTtile);
        }
        initData();
    }

    private void initData() {
        List<Integer> list = new ArrayList<>();
        for (int i = 1; i <= maxDay; i++) {
            list.add(i);
        }
        mWheelView.setData(list);

    }

    @OnClick(R.id.cancel)
    protected void cancelSelect() {
        dismiss();
    }

    @OnClick(R.id.confirm)
    protected void confirm() {
        mOnConfirmListener.confirm((Integer) (mWheelView.getSelectedItemData()));
        dismiss();
    }

    public interface OnConfirmListener {
        void confirm(Integer date);
    }
}
