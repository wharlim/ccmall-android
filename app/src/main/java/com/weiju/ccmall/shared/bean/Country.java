package com.weiju.ccmall.shared.bean;

import com.google.gson.annotations.SerializedName;
import com.weiju.ccmall.shared.basic.BaseModel;

public class Country extends BaseModel {

    /**
     * countryName : 国内现货
     * flag : http://flyimg.kangerys.com/G1/M00/00/1A/eEzAjVj8cAuARm3UAAAev4GaHv8221.jpg
     */

    @SerializedName("countryName")
    public String countryName;
    @SerializedName("flag")
    public String flag;
}
