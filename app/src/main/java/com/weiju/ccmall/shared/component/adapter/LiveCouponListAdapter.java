package com.weiju.ccmall.shared.component.adapter;


import com.blankj.utilcode.utils.TimeUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.weiju.ccmall.R;
import com.weiju.ccmall.module.face.widget.ITimeViewBase;
import com.weiju.ccmall.shared.bean.LiveCoupon;
import com.weiju.ccmall.shared.component.dialog.LiveCouponListDialog;
import com.weiju.ccmall.shared.util.MoneyUtil;


/**
 * @author chenyanming
 * @date 2020/2/21.
 */
public class LiveCouponListAdapter extends BaseQuickAdapter<LiveCoupon, BaseViewHolder> {

    private int mType;

    public LiveCouponListAdapter() {
        super(R.layout.item_live_coupon_list);
    }

    @Override
    protected void convert(BaseViewHolder helper, LiveCoupon item) {
        helper.setText(R.id.tvMoney, MoneyUtil.centToYuan¥StrNoZero(item.cost));
        helper.setText(R.id.tvContent, item.couponTitle);
        helper.setText(R.id.tvDate, mType == LiveCouponListDialog.COUPON_PUSH  ?
                TimeUtils.millis2String(TimeUtils.string2Millis(item.createDate), "HH:mm"):
                String.format("有效期：%s-%s",TimeUtils.millis2String(TimeUtils.string2Millis(item.limitStartDate), "yyyy.MM.dd"),
                        TimeUtils.millis2String(TimeUtils.string2Millis(item.limitEndDate), "yyyy.MM.dd")   ));
        helper.setVisible(R.id.ivCheckPush, mType == LiveCouponListDialog.COUPON_PUSH || item.receiveStatus == 1);
        helper.setVisible(R.id.ivCheckOpen, mType == LiveCouponListDialog.COUPON_PLAY && item.receiveStatus == 0);
        helper.setVisible(R.id.ivNoData, mType == LiveCouponListDialog.COUPON_PLAY && item.status == 1 && item.receiveStatus == 0);

    }

    public void setType(int type) {
        mType = type;
    }
}
