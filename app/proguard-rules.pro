# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/JayChan/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}
-keepattributes SourceFile, LineNumberTable
-keep class com.chad.library.adapter.** {
*;
}
-keep public class * extends com.chad.library.adapter.base.BaseQuickAdapter
-keep public class * extends com.chad.library.adapter.base.BaseViewHolder
-keepclassmembers public class * extends com.chad.library.adapter.base.BaseViewHolder {
     <init>(android.view.View);
}

-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public enum com.bumptech.glide.load.resource.bitmap.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}


-keep public class * extends android.webkit.WebChromeClient

-keepattributes Signature
    -keep class sun.misc.Unsafe { *; }
    -keep class com.taobao.** {*;}
    -keep class com.alibaba.** {*;}
    -keep class com.alipay.** {*;}
    -dontwarn com.taobao.**
    -dontwarn com.alibaba.**
    -dontwarn com.alipay.**
    -keep class com.ut.** {*;}
    -dontwarn com.ut.**
    -keep class com.ta.** {*;}
    -dontwarn com.ta.**
    -keep class org.json.** {*;}
    -keep class com.ali.auth.**  {*;}

     -dontwarn com.tencent.bugly.**
    -keep public class com.tencent.bugly.**{*;}


    -keepattributes Exceptions,InnerClasses

    -keepattributes Signature

#     //RongCloud SDK
    -keep class io.rong.** {*;}
    -keep class cn.rongcloud.** {*;}
    -keep class * implements io.rong.imlib.model.MessageContent {*;}
    -dontwarn io.rong.push.**
    -dontnote com.xiaomi.**
    -dontnote com.google.android.gms.gcm.**
    -dontnote io.rong.**

#     //VoIP
    -keep class io.agora.rtc.** {*;}

#    //红包
    -keep class com.google.gson.** { *; }
    -keep class com.uuhelper.Application.** {*;}
    -keep class net.sourceforge.zbar.** { *; }
    -keep class com.google.android.gms.** { *; }
    -keep class com.alipay.** {*;}
    -keep class com.jrmf360.rylib.** {*;}

    -ignorewarnings
